<?php

/* required files and their download locations:
 * FPDF 1.6: http://www.fpdf.org/ (then go to downloads)
 * FPDI 1.3.1: http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/
 * FPDI_TPL: http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/
 */

// Include the main TCPDF library and TCPDI.
require_once('tcpdf/tcpdf.php');
require_once('tcpdf/tcpdi.php');

// Create new PDF document.
$pdf = new TCPDI(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// Add a page from a PDF by file path.
$pdf->AddPage();
$pdf->setSourceFile('/path/to/file-to-import.pdf');
$idx = $pdf->importPage(1);
$pdf->useTemplate($idx);

$pdfdata = file_get_contents('/path/to/other-file.pdf'); // Simulate only having raw data available.
$pagecount = $pdf->setSourceData($pdfdata); 
for ($i = 1; $i <= $pagecount; $i++) { 
    $tplidx = $pdf->importPage($i);
    $pdf->AddPage();
    $pdf->useTemplate($tplidx); 
}
?>