<?php
class Doc_model extends CI_Model
{
	/*function validate()
	{
		$this->db->where('reg_regno', $this->input->post('reg_regno'));
		
		//$this->db->where('reg_pass', md5( $this->input->post('reg_pass') )); // code commented for black-box testing by preeti on 26th mar 14
		
		$this->db->where('reg_pass', $this->input->post('reg_pass_encode') ); // code added for black-box testing by preeti on 26th mar 14
		
		$this->db->where('reg_status', 'c');
		
		$res = $this->db->get('register');
		
		if( $res->num_rows == 1 )
		{
			foreach($res->result() as $row)
			{			
				return $row;	
			}		
			
		}
		else 
		{
			return FALSE;	
		}
		
	}*///  code commented by preeti on 28th mar 14 for black-box testing
	
	
	// code added by preeti on 28th mar 14 for black-box testing
	
	public function get_visitor()
	{
	
  $res=$this->db->get('visitor');
  $rows=$res->result();
  $vno=$rows[0]->vno;
 $newvno=$vno+1;
  $data=array('vno'=>$newvno);
 // $this->db->where('id',1);
  $this->db->update('visitor',$data);
  
  return $vno; 
  
	
	}
	
	
	function validate()
	{
		$this->db->select('*');
		
       	$this->db->from('register');
		
		$this->db->where('reg_regno', $this->input->post('reg_regno'));
		
		$this->db->where('reg_status', 'c');
		
		$res = $this->db->get();
				
		if( $res->num_rows > 0 )
		{
			
			$res_arr = $res->result();
			
			$reg_pass = $res_arr[0]->reg_pass; // returns the md5 of otp
			
			// create a combined md5 string
			
			// get the salt from the previous page
			
			//$salt = $this->input->post('salt'); // commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('user_salt'); // added by preeti on 22nd apr 14 for manual testing
			
			$en_password = $this->input->post('reg_pass_encode');
			
			$pass_to_match = md5( $reg_pass.$salt );
			
			if( $en_password === $pass_to_match  )
			{
				return $res_arr[0];			
			}
			else 
			{
				return FALSE;
			}			
			
		}
		else 
		{
			return FALSE;	
		}
		
	}
	
		
	// below function added by preeti on 26th apr 14	
		
	function add_login( ) 
	{
		$data=array();
		
		$data['logu_action'] = 'login';
		
		$data['logu_time'] = date('Y-m-d H:i:s');
		
		$data['logu_ip'] = $this->input->ip_address();
		
		$data['logu_regno']= $this->session->userdata('regno') ;
							
		$this->db->insert('login_user', $data);
			
	}
	
	// below function added by preeti on 26th apr 14	
			
	function add_logout(  )
	{
		$data=array();
		
		$data['logu_action'] = 'logout';
		
		$data['logu_time'] = date('Y-m-d H:i:s');
		
		$data['logu_ip'] = $this->input->ip_address();
		
		$data['logu_regno']= $this->session->userdata('regno') ;
							
		$this->db->insert('login_user', $data);
	}


	// below function added by preeti on 26th apr 14	
		
	function add_pass( $action ) 
	{
		$data=array();
		
		$data['logu_action'] = $action;
		
		$data['logu_time'] = date('Y-m-d H:i:s');
		
		$data['logu_ip'] = $this->input->ip_address();
		
		$data['logu_regno']= $this->session->userdata('regno') ;
							
		$this->db->insert('login_user', $data);
			
	}
	
	
	
	
	// below function added by preeti on 26th apr 14	
		
	function add_logfail( $regno ) 
	{
		$data=array();
		
		$data['logu_action'] = 'logfail';
		
		$data['logu_time'] = date('Y-m-d H:i:s');
		
		$data['logu_ip'] = $this->input->ip_address();
		
		$data['logu_regno']= $regno ;	
						
		$this->db->insert('login_user', $data);
			
	}
	
	
	function get_user_numb() 
	{
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN logondetails1 log ON reg.reg_regno = log.logondetails1_u_regno
		
		 
		
		
			
		WHERE reg_status = 'c' ";

		
		
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}
	
	function get_form_last_date()
	{
		$res_last = $this->db->get('last_date');
		
		$res_arr_last = $res_last->result();
		
		$ld_date = $res_arr_last[0]->ld_date;
		
		return $ld_date;
	
	}
	
	// below function added by preeti on 21st may 14
	
	function get_form_start_date()
	{
		$res_last = $this->db->get('last_date');
		
		$res_arr_last = $res_last->result();
		
		$ld_start = $res_arr_last[0]->ld_start;
		
		return $ld_start;
	
	}
	
	function check_otp_validity( $reg_id )
	{
		$current = date('Y-m-d H:i:s');
		
		$this->db->select('reg_otp_end');
		
		$this->db->from('register');
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		if( $current <= $res_arr[0]->reg_otp_end )
		{ 
			return TRUE;
		}
		else 
		{
			return FALSE;	
		}		
		
	}
	
	/* check the time limit to confirm registration from the last_date table */
	
	// below function modified by preeti on 5th mar 14
	
	function check_confirm_validity( $reg_id )
	{
		// get the creation date for this reg_id from register table
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->get('register');
		
		$res_arr = $res->result();
		
		$reg_creation = $res_arr[0]->reg_createdon;
		
		// get the number of validity days from last_date table 
		
		$res_last = $this->db->get('last_date');
		
		$res_arr_last = $res_last->result();
		
		$ld_validity = $res_arr_last[0]->ld_validity;
	
	
		//get the difference in days between the creation date and the current date
		
		$reg_creation = new DateTime( $reg_creation );
		
		$current_date = new DateTime( date('Y-m-d') );// current date
		
		$diff = date_diff( $current_date, $reg_creation );
		
		/*$diff = $diff->format('%R%a');
		
		$diff = str_replace('+', '', $diff);*/ 
		
		$diff = $diff->d;		
		
		// if the days exceed the valid num of days then disallow else allow
		
		if( $diff > $ld_validity )
		{
			return FALSE;
		}
		else 
		{
			return TRUE;	
		}
		
	}
	
	/* to check the existence of registration num and OTP in the DB when the
	
	 * status is incomplete */
	
	/*function validate_otp()  
	{
		$this->db->where('reg_regno', $this->input->post('reg_regno'));
		
		//$this->db->where('reg_otp', $this->input->post('reg_otp') );// code commented by preeti on 26th mar 14 for black-box testing
		
		// below code added by preeti on 26th mar 14 for black-box testing, this otp contains the salted md5 value
		
		$this->db->where('reg_otp', $this->input->post('reg_otp_encode') );
		
		$this->db->where('reg_status', 'i' );
		
		$res = $this->db->get('register');
		
		if( $res->num_rows == 1 )
		{
			foreach($res->result() as $row)
			{
				$reg_id = $row->reg_id;
				
				return $reg_id;	
			}			
			
		}
		else 
		{
			return FALSE;	
		}
		
	}*/ //  code commented by preeti on 28th mar 14 for black-box testing 
	
	
	// below code added by preeti on 28th mar 14 for black-box testing
	
	function validate_otp()  
	{
			
		$this->db->select('reg_id, reg_otp');
		
        $this->db->from('register');
		
		$this->db->where('reg_regno', $this->input->post('reg_regno'));
		
		$this->db->where('reg_status', 'i' );
		
		$res = $this->db->get();
		
		if( $res->num_rows > 0 )
		{
			
			$res_arr = $res->result();
			
			$reg_otp = $res_arr[0]->reg_otp; // returns the md5 of otp
			
			// create a combined md5 string
			
			// get the salt from the previous page
			
			//$salt = $this->input->post('salt'); // commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('confirm_salt'); // added by preeti on 22nd apr 14 for manual testing
			
			$en_password = $this->input->post('reg_otp_encode');
			
			$pass_to_match = md5( $reg_otp.$salt );
			
			if( $en_password === $pass_to_match  )
			{
				// return th reg_id	
				
				$reg_id = $res_arr[0]->reg_id; // returns the md5 of otp
				
				return $reg_id;			
			}
			else 
			{
				return FALSE;
			}					
			
		}
		else 
		{
			return FALSE;	
		}
		
	}
	
	
	function get_name( $doc_id ) // returns the name
	{
		$sql = " SELECT CONCAT(doc_fname, ' ', doc_lname) name FROM doctor 
		
		WHERE doc_id = ? ";
		
		$res = $this->db->query( $sql, array($doc_id) );
		
		foreach($res->result() as $row )
		{
			$name = $row->name;
		
			return $name;
		}
		
	}
	
	function get_qual()
	{
		$res = $this->db->query("SELECT qual_id, qual_name 
		
		FROM qualification ORDER BY qual_name ASC ");
		
		return $res->result();		
	}
	
	function get_stream()
	{
		$res = $this->db->query("SELECT stream_id, stream_name 
		
		FROM stream ORDER BY stream_name ASC ");
		
		return $res->result();		
	}
	
	function get_college()
	{
		$res = $this->db->query("SELECT coll_id, coll_name	 
		
		FROM college ORDER BY coll_name	 ASC ");
		
		return $res->result();		
	}
	
	/*function insert_doc( $photo_path, $resume_path )
	{
		// first check if the username already exist
		
		$data = array();
		
		$data['doc_username'] = $this->input->post('doc_username');
		
		$data['doc_fname'] = $this->input->post('doc_fname');
		
		$data['doc_lname'] = $this->input->post('doc_lname');
		
		$data['doc_lname'] = $this->input->post('doc_lname');
		
		$data['doc_mobile'] = $this->input->post('doc_mobile');
		
		$data['doc_email'] = $this->input->post('doc_email');
		
		$data['doc_dob'] = calen_to_db($this->input->post('doc_dob'));
		
		$data['doc_qual'] = $this->input->post('doc_qual');
		
		$data['doc_stream'] = $this->input->post('doc_stream');
		
		$data['doc_college'] = $this->input->post('doc_college');
		
		$data['doc_photo'] = $photo_path;
		
		$data['doc_resume'] = $resume_path;
		
		$data['doc_status'] = 'i'; // at the first step the status would be inactive
		
		$data['doc_createdon'] = date('Y-m-d H:i:s');
		
		$data['doc_updatedon'] = date('Y-m-d H:i:s');
		
		$res = $this->db->insert('doctor', $data);
		
		if($res)
		{
			$id = $this->db->insert_id();
			
			return $id;	
		}
		else
		{
			return FALSE;
		}		
	
	}*/
	
	function get_otp_hours()
	{
		$this->db->select('ld_otp_hours');

		$this->db->from('last_date');
		
		$res = $this->db->get();
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0]->ld_otp_hours;	
		}		
		
	}
	
	function update_only_otp( $reg_id, $reg_otp, $otp_hours )
	{
		$data = array();
		
		// start, code added by preeti on 28th mar 14 for black-box testing
				
		$reg_otp = md5( $reg_otp ); // this becomes the md5 value
		
		// end, code added by preeti on 28th mar 14 for black-box testing
		
		
		$data['reg_otp'] = $reg_otp;
		
		$current_date = date('Y-m-d H:i:s');
		
		$data['reg_otp_start'] = $current_date;
		
		
		
		$date = new DateTime( $current_date );

		$date->add(new DateInterval('PT'.$otp_hours.'H'));

		$data['reg_otp_end'] = $date->format('Y-m-d H:i:s'); 
		
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->update('register', $data);
		
		return $res;	
	}
	
	function update_otp( $reg_id, $reg_regno, $reg_otp, $otp_hours )
	{
		$data = array();
		
		$data['reg_regno'] = $reg_regno;
		
		// start, code added by preeti on 28th mar 14 for black-box testing
		
		$reg_otp = md5( $reg_otp ); // this becomes the md5 value
		
		// end, code added by preeti on 28th mar 14 for black-box testing
		
		$data['reg_otp'] = $reg_otp;
		
		$data['reg_otp_duration'] = $otp_hours;
		
		
		
		$current_date = date('Y-m-d H:i:s');
		
		$data['reg_otp_start'] = $current_date; // set the OTP start date
		
		
		$date = new DateTime( $current_date );

		$date->add(new DateInterval('PT'.$otp_hours.'H'));

		$data['reg_otp_end'] = $date->format('Y-m-d H:i:s'); // set the OTP end date
		
		
		
		$this->db->where('reg_id', $reg_id);
				
		$res = $this->db->update('register', $data);
		
		return $res;	
	}
	
	function insert_doc( )
	{
		$data = array();
		
		$data['reg_fname'] = $this->input->post('reg_fname');
		
		$data['reg_mname'] = $this->input->post('reg_mname');
		
		$data['reg_lname'] = $this->input->post('reg_lname');
		
		$data['reg_mobile'] = $this->input->post('reg_mobile');
		
		$data['reg_email'] = $this->input->post('reg_email');
		
		$data['reg_status'] = 'i'; // at this stage the registration is surely incomplete
		
		$data['reg_createdon'] = date('Y-m-d H:i:s');
		
		$res = $this->db->insert('register', $data);
		
		if($res)
		{
			$id = $this->db->insert_id();
			
			return $id;	
		}
		else
		{
			return FALSE;
		}		
	
	}
	

	function get_item_name( $doc_id, $item_name )
	{
		$this->load->helper('file');	
			
		if($item_name == 'photo')
		{
			$sql = " SELECT doc_photo FROM doctor WHERE doc_id = ? ";
		
			$res = $this->db->query($sql, array($doc_id));
			
			if( $res->num_rows() > 0 )
			{
				$res_arr = $res->result();
			
				return $res_arr[0]->doc_photo;	
			}		
				
				
		}
		else if($item_name == 'resume')
		{
			$sql = " SELECT doc_resume FROM doctor WHERE doc_id = ? ";
		
			$res = $this->db->query($sql, array($doc_id));
			
			$res_arr = $res->result();
			
			return $res_arr[0]->doc_resume;	
				
		}		
		
	}

	
	function update_doc( $doc_id, $photo_path = '', $resume_path = '' )
	{
		$data = array();
				
		$data['doc_fname'] = $this->input->post('doc_fname');
		
		$data['doc_lname'] = $this->input->post('doc_lname');
		
		$data['doc_lname'] = $this->input->post('doc_lname');
		
		$data['doc_mobile'] = $this->input->post('doc_mobile');
		
		$data['doc_email'] = $this->input->post('doc_email');
		
		$data['doc_dob'] = calen_to_db($this->input->post('doc_dob'));
		
		$data['doc_qual'] = $this->input->post('doc_qual');
		
		$data['doc_stream'] = $this->input->post('doc_stream');
		
		$data['doc_college'] = $this->input->post('doc_college');
		
		if( !empty( $photo_path ) )
		{
			$data['doc_photo'] = $photo_path;
		}
		
		if( !empty( $resume_path ) )
		{
			$data['doc_resume'] = $resume_path;
		}
		
		
		$data['doc_updatedon'] = date('Y-m-d H:i:s');
		
		$this->db->where('doc_id', $doc_id);
		
		$res = $this->db->update('doctor', $data);
		
		if($res)
		{
			return TRUE;					
		}
		else
		{
			return FALSE;
		}
	}


	function check_duplicate(  )
	{
		$doc_username = $this->input->post('doc_username');
		
		$sql = " SELECT doc_username FROM doctor 
		
		WHERE doc_username = ? ";
		
		$res = $this->db->query( $sql, array($doc_username) );
		
		if( $res->num_rows > 0 ) // duplicasy is there
		{
			return FALSE;	
		}
		else 
		{
			return TRUE;	
		}
		
	}
	
	function check_duplicate_mobile(  )
	{
		$reg_mobile = $this->input->post('reg_mobile');
		
		$sql = " SELECT reg_mobile FROM register 
		
		WHERE reg_mobile = ? ";
		
		$res = $this->db->query( $sql, array($reg_mobile) );
		
		if( $res->num_rows > 0 ) // duplicasy is there
		{
			return FALSE;	
		}
		else 
		{
			return TRUE;	
		}
		
	}
	
	function check_duplicate_email(  )
	{
		$reg_email = $this->input->post('reg_email');
		
		$sql = " SELECT reg_email FROM register 
		
		WHERE reg_email = ? ";
		
		$res = $this->db->query( $sql, array($reg_email) );
		
		if( $res->num_rows > 0 ) // duplicasy is there
		{
			return FALSE;	
		}
		else 
		{
			return TRUE;	
		}
		
	}
	
	function get_num_records()
	{
		$res = $this->db->get('doctor');
		
		return $res->num_rows();	
		
	}
	
		
	/*function get_detail( $doc_id )
	{
		$sql = " SELECT doc.doc_fname, doc.doc_lname, doc.doc_mobile, doc.doc_dob,doc.doc_username,
		
		doc.doc_qual, doc.doc_stream, doc.doc_college, doc.doc_resume,
		
		doc.doc_photo, qual.qual_name, st.stream_name, coll.coll_name ,
		
		doc.doc_email
		
		FROM doctor doc
		
		JOIN qualification qual ON doc.doc_qual = qual.qual_id
		
		JOIN stream st ON doc.doc_stream = st.stream_id
		
		JOIN college coll ON doc.doc_college = coll.coll_id
		
		WHERE doc_id = ? ";
		
		$res = $this->db->query($sql, array($doc_id));
		
		if( $res->num_rows() > 0 )
		{
			foreach($res->result() as $obj)
			{
				return $obj;
			}	
		}	
		
	}*/
	
	function get_detail( $reg_id )
	{
		$this->db->select('reg_email, reg_mobile');
		
		$this->db->from('register');
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		return $res_arr[0];
	}
	
	function check_email()
	{
		$reg_email = $this->input->post('reg_email');
		
		$qry = " SELECT reg_id FROM register 
		
		WHERE reg_email = ? ";
		
		$res = $this->db->query( $qry, array( $reg_email ) );
		
		if( $res->num_rows() == 1 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->reg_id;				
		}
		else 
		{
			return FALSE;
		}
	}
	
	// below function added by preeti on 25th apr 14
	
	function get_mobile( $reg_id )
	{
		$qry = " SELECT reg_mobile FROM register 
		
		WHERE reg_id = ? ";
		
		$res = $this->db->query( $qry, array( $reg_id ) );
		
		if( $res->num_rows() == 1 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->reg_mobile;				
		}
		else 
		{
			return FALSE;
		}
	}
	
	// below function added by preeti on 3rd apr 14
	
	function random_num( $num = 32 ) // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, $num);
		
		return 	$random;
	}
	
	
	// below function added by preeti on 3rd apr 14
	
	function random_only_num( $length ) 
	{ 
		$random= "";
	
		srand((double)microtime()*1000000);
		
		$data = "0123456789";
	
		for($i = 0; $i < $length; $i++) 
		{ 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}
		
		$random = substr($random, 0, $length);
	
		return $random; 
	}
	
	function new_password( $reg_id )
	{
		//$new_pass = 'passup'.$reg_id;	 // line commented by preeti on 3rd apr 14
		
		// start, below line added by preeti on 3rd apr 14
		
		$random_num = $this->random_num( 4 );
		
		//$new_pass = 'Passup@'.$random_num; // code commented by preeti on 3rd apr 14
		
		$new_pass = 'Passup@'.$this->random_num(2).$this->random_only_num(2); // code added by preeti on 3rd apr 14
		
		// end, below line added by preeti on 3rd apr 14
		
		$reg_pass = md5( $new_pass );	
		
		$qry = " UPDATE register 
		
		SET reg_pass = ?
		
		WHERE reg_id = ? ";
		
		$res = $this->db->query( $qry, array( $reg_pass, $reg_id ) );
		
		if( $res )
		{
			return $new_pass;
		}		
		
	}
	
	function ajax_check()
	{
		$doc_username = $this->input->post('doc_username');
		
		$sql = " SELECT doc_username FROM doctor 
		
		WHERE doc_username = ? ";
		
		$res = $this->db->query( $sql, array($doc_username) );
		
		return $res->num_rows();
				
	}
	
	// code commented by preeti on 11th apr 14 for black-box testing
	
	/*function ajax_check_mobile()
	{
		$reg_mobile = $this->input->post('reg_mobile');
		
		$sql = " SELECT reg_mobile FROM register 
		
		WHERE reg_mobile = ? ";
		
		$res = $this->db->query( $sql, array( $reg_mobile ) );
		
		return $res->num_rows();
				
	}*/
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	function ajax_check_mobile()
	{
		$reg_mobile = $this->input->post('reg_mobile');
		
		if( $this->only_num( $reg_mobile ) )
		{
			$sql = " SELECT reg_mobile FROM register 
		
			WHERE reg_mobile = ? ";
			
			$res = $this->db->query( $sql, array( $reg_mobile ) );
			
			return $res->num_rows();	
		}
		else 
		{
			return 'invalid';
		}		
				
	}
	
	// code commented by preeti on 11th apr 14 for black-box testing
	
	/*function ajax_check_email()
	{
		$reg_email = $this->input->post('reg_email');
		
		$sql = " SELECT reg_email FROM register 
		
		WHERE reg_email = ? ";
		
		$res = $this->db->query( $sql, array( $reg_email ) );
		
		return $res->num_rows();
				
	}*/
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	function ajax_check_email()
	{
		$reg_email = $this->input->post('reg_email');
		
		if( $this->alpha_at_num_space( $reg_email ) )
		{
			$sql = " SELECT reg_email FROM register 
		
			WHERE reg_email = ? ";
			
			$res = $this->db->query( $sql, array( $reg_email ) );
			
			return $res->num_rows();
			
		}
		else 
		{
			return 'invalid';
		}	
				
	}
	
	function activate()
	{
		$doc_id = $this->input->post('doc_id');
		
		$doc_username = trim( $this->input->post('doc_username') ) ;
		
		$qry = " SELECT * FROM doctor WHERE
		
		doc_id = ? AND doc_username = ? AND doc_status = ? ";
		
		$res = $this->db->query( $qry, array( $doc_id, $doc_username, 'i' ) );
		
		if( $res->num_rows() == 1 )
		{
			
			$password = 'pass'.$doc_id;
			
			$data = array(
			
			'doc_password' => md5($password),
			
			'doc_status' => 'a' );
			
			$this->db->where('doc_id', $doc_id);
			
			$res1 = $this->db->update('doctor', $data);	
		
			if($res1)
			{
				//return TRUE;
				
				return $doc_id; // return the id number
			}
			else 
			{
				return FALSE;	
			}	
			
		}
		else 
		{
			return FALSE;
		}	
		
	}
	
	function update_doc_password()
	{
		$data = array();
		
		$regno = $this->input->post('regno');
		
		// below code commented for black-box testing by preeti on 26th mar 14
		
		//$data['reg_pass'] = md5( $this->input->post('reg_pass') );
		
		// below, code added for black-box testing by preeti on 26th mar 14
		
		$data['reg_pass'] = $this->input->post('reg_pass_encode');
		
		$this->db->where('reg_regno', $regno);
		
		$res = $this->db->update('register', $data);
		
		if($res)
		{
			return TRUE;					
		}
		else
		{
			return FALSE;
		}
	}
	
	/*function check_old_password( $regno )
	{
		// below code commented for black-box testing by preeti on 26th mar 14	
		
		//$old_password = $this->input->post( 'opassword' );
		
		// below code added for black-box testing by preeti on 26th mar 14
		
		$old_password = $this->input->post( 'opassword_encode' );
		
		$qry = " SELECT reg_id FROM register 
		
		WHERE reg_regno = ? AND reg_pass = ? ";
		
		// below code commented for black-box testing by preeti on 26th mar 14
		
		//$res = $this->db->query( $qry , array( $regno, md5( $old_password ) ) );
		
		// below code added for black-box testing by preeti on 26th mar 14
		
		$res = $this->db->query( $qry , array( $regno, $old_password ) );
		
		
		if( $res->num_rows() == 1 )
		{
			return TRUE;
		}
		else
		{
			return FALSE;			
		}
		
	}*/ //  code commented by preeti on 28th mar 14 for black-box testing
	
	// code added by preeti on 28th mar 14 for black-box testing
	
	function check_old_password( $regno )
	{
		//$salt = $this->input->post( 'salt' ); // commented by preeti on 22nd apr 14 for manual testing
		
		$salt = $this->session->userdata('user_salt'); // added by preeti on 22nd apr 14 for manual testing
		
		$old_password = $this->input->post( 'opassword_encode' ); // this is the salted md5 value
		
		$qry = " SELECT reg_id, reg_pass FROM register 
		
		WHERE reg_regno = ? ";		
		
		$res = $this->db->query( $qry , array( $regno ) );		
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
			
			$reg_pass = $res_arr[0]->reg_pass;
			
			
			$result_pass = md5( $reg_pass.$salt );
			
			if( $result_pass === $old_password )
			{
				return TRUE;	
			}
			else
			{
				return FALSE;
			}	
			
		}
		else
		{
			return FALSE;			
		}
		
	}
	

	function get_all_not()
	{
		$qry = " SELECT * FROM notification 
		
		ORDER BY not_id DESC"; // change in query by preeti on 19th feb 14
		
		$res = $this->db->query( $qry );

		return $res->result();
	}
	
	function get_not_detail( $not_id )
	{
		$qry = " SELECT * FROM notification 
		
		WHERE not_id = ? ";
		
		$res = $this->db->query( $qry , array( 'not_id' => $not_id ) );

		$res_arr = $res->result();

		return $res_arr[0];

	}
	
	function get_result()
	{
		$qry = " SELECT * FROM result 
		
		ORDER BY res_id DESC "; // change in query by preeti on 19th feb 14
		
		$res = $this->db->query( $qry );

		$res_arr = $res->result();

		return $res_arr;

	}
	
	function get_regno( $reg_id )// modified by preeti on 27th feb 14
	{
		$qry = " SELECT reg_regno FROM register 
		
		WHERE reg_id = ? ";
		
		$res = $this->db->query( $qry , array( 'reg_id' => $reg_id  ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0]->reg_regno;	
		}
		else 
		{
			return;	
		}		
		
	}
	
	function check_regno_user( $regno )
	{
		$qry = " SELECT u_id FROM user WHERE u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $regno  ) );
		
		if( $res->num_rows() == 1 )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;	
		}
	}
	
	function get_new_name_file( $new_u_regno )
	{
		$this->db->select('new_name_proof');	
			
		$this->db->from('new_name');	
			
		$this->db->where('new_u_regno', $new_u_regno);
		
		$res = $this->db->get();
		
		if( $res->num_rows() )
		{
			$res_arr = $res->result();
		
			return $res_arr[0]->new_name_proof;	
		}
		
		return;
	}
	
	
	function save_user1( $u_photo_file, $u_dob_proof_file,  $new_name_proof_file = '' )
	{
		// if the registration number is set in the hidden field
		
		$regno = ''; 
		
		if( $this->input->post('reg_id') != '' )
		{
			$reg_id = $this->input->post('reg_id');	
			
			$regno = $this->get_regno( $reg_id );			
		}
		
		$data['u_regno'] = $regno;	// set the regno in user table same as in register table	
		
		
		$data['u_fname'] = $this->input->post('u_fname');
		
		$data['u_mname'] = $this->input->post('u_mname');
		
		$data['u_lname'] = $this->input->post('u_lname');
		
		
		$data['u_hindi_name'] = $this->input->post('u_hindi_name');
		
		$data['u_photo'] = $u_photo_file;
		
		$res_new = TRUE;
		
		if( $this->input->post('u_is_name_change') == 'y' ) // if only the name is changed, then there will be a proof of that
		{
			$data['u_is_name_change'] = $this->input->post('u_is_name_change');
			
			$data_new['new_u_regno'] = $regno;
			
			$data_new['new_fname'] = $this->input->post('new_fname');
			
			$data_new['new_mname'] = $this->input->post('new_mname');
			
			$data_new['new_lname'] = $this->input->post('new_lname');
			
			if( $new_name_proof_file != '' )
			{
				$data_new['new_name_proof'] = $new_name_proof_file;	
			}		
						
			$res_del = $this->db->delete('new_name', array( 'new_u_regno' => $regno ));
	
			// now insert the new values
			
			if( $res_del )
			{
				$res_new = $this->db->insert('new_name', $data_new);	
			}
			else 
			{
				$res_new = '';
			}				
			
		}
		else 
		{
			$data['u_is_name_change'] = 'n';
			
			// get the filename from new_name table
			
			$new_name_proof = $this->doc_model->get_new_name_file( $regno );
			
			
			// delete the file from the uploads/new_name folder
			
			if( $new_name_proof !='' )
			{
				$new_name_proof_url = 'uploads/new_name/'.$new_name_proof;
			
				if( file_exists( $new_name_proof_url ) )
				{
					unlink( $new_name_proof_url );
				}
				
				
				// delete from the new_name table where new_u_regno is this
				
				$this->db->delete('new_name', array('new_u_regno' => $regno ));	
				
			}
			
			
		}
		
		$data['u_father_fname'] = $this->input->post('u_father_fname');
		
		$data['u_father_mname'] = $this->input->post('u_father_mname');
		
		$data['u_father_lname'] = $this->input->post('u_father_lname');
		
		$data['u_sex'] = $this->input->post('u_sex');
		
		$dob = calen_to_db( $this->input->post('u_dob')) ;
		
		$birth_date = new DateTime( $dob );
		
		//$today = new DateTime('00:00:00');// todays date
		
		$current_year = date('Y');
		
		$dec31_date = $current_year.'-12-31'; // Y-m-d format of the current year
		
		$dec31 = new DateTime( $dec31_date );
		
		$diff = date_diff( $birth_date , $dec31);
		
		$data['u_dob'] = $dob ;
		
		$data['u_dob_proof'] = $u_dob_proof_file; // dob proof 
		
		$data['u_age_year'] = $diff->y ;
		
		$data['u_age_month'] = $diff->m ;
		
		$data['u_age_day'] = $diff->d ;
		
		$data['u_nationality'] = $this->input->post('u_nationality');
		
		$data['u_is_married'] = $this->input->post('u_is_married');
		
		$data['u_updatedon'] = date('Y-m-d H:i:s');
		
		
		// update in user table as the record is already there
		
		$this->db->where('u_regno', $regno);
			
		$res_user = $this->db->update('user', $data);	
			
		
		
		$res_sp = TRUE;	
	
		
		if( $this->input->post('u_is_married') == 'y' )
		{
			$data_sp['sp_u_regno'] =  $regno ;
			
			$data_sp['sp_nation'] =  $this->input->post('sp_nation') ;
			
			$data_sp['sp_mar_date'] =  calen_to_db( $this->input->post('sp_mar_date') ) ;
			
			$data_sp['sp_name'] =   $this->input->post('sp_name') ;
			
			$data_sp['sp_occup'] =   $this->input->post('sp_occup') ;
			
			$data_sp['sp_ssc_applied'] =   $this->input->post('sp_ssc_applied') ;
			
			if( $this->input->post('sp_citizenship_date') != '' )
			{
				$data_sp['sp_citizenship_date'] = calen_to_db( $this->input->post('sp_citizenship_date')) ;
			}
			
			// first delete the prevous record related to this regno from spouse table 
			
			//and then add the new record its like updating
			
			$res_del = $this->db->delete('spouse', array( 'sp_u_regno' => $regno ));
	
			// now insert the new values
			
			if( $res_del )
			{
				$res_sp = $this->db->insert('spouse', $data_sp);	
			}
			else 
			{
					$res_sp = '';
			}		
			
		}
		else if( $this->input->post('u_is_married') == 'n' ) // else if added by preeti on 3rd mar 14
		{
			$res_del = $this->db->delete('spouse', array( 'sp_u_regno' => $regno ));
	    }
		
		if( $res_user && $res_sp && $res_new )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;
		}		
	}

	/*function save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
	$u_attempt_cert1_file,  $u_dd_copy_file, $u_intern_cert_file, $u_mbbs_cert_file, 
	
	$u_pg_cert_file )*/ /* lines commented and below lines added by preeti on 24th feb 14 */
	
	function save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
	$u_attempt_cert1_file,  $u_dd_copy_file, $u_mbbs_cert_file, 
	
	$u_intern_cert_file = '', $u_pg_cert1_file = '' , $u_pg_cert2_file = '', $u_pg_cert3_file = '' )
	{
		$regno = ''; 
		
		if( $this->input->post('reg_id') != '' )
		{
			$reg_id = $this->input->post('reg_id');
			
			if( !$this->only_num( $reg_id ) ) // if block added by preeti on 11th apr 14 for black-box testing
			{
				return;
			}	
			
			$regno = $this->get_regno( $reg_id );			
		}
		else 
		{
			return;	
		}
		
		$data['u_grad_from'] = $this->input->post( 'u_grad_from' );
		
	
		$data['u_pmrc_num'] = $this->input->post('u_pmrc_num');
		
		$data['u_pmrc_issue_off'] = $this->input->post('u_pmrc_issue_off');
		
		$data['u_pmrc_reg_date'] = calen_to_db( $this->input->post('u_pmrc_reg_date')) ;
			
		$data['u_pmrc_copy'] = $u_pmrc_copy_file;
		
		$data['u_basic_qual'] = $this->input->post('u_basic_qual');
		
		
		$data['u_adm_date'] = calen_to_db( $this->input->post('u_adm_date'));
		
		$data['u_passing_date'] = calen_to_db( $this->input->post('u_passing_date'));
		
		$data['u_mbbs_year'] = substr( $data['u_passing_date'], 0, 4 );
		
		$data['u_num_attempt'] = $this->input->post('u_num_attempt');
		
		$data['u_num_attempt1'] = $this->input->post('u_num_attempt1');
		
		$data['u_attempt_cert'] = $u_attempt_cert_file;
		
		$data['u_attempt_cert1'] = $u_attempt_cert1_file;
		
		$data['u_college'] = $this->input->post('u_college');
		
		$data['u_univ'] = $this->input->post('u_univ');
		
		$data['u_college_recog'] =  $this->input->post('u_college_recog') ;
		
		$res_pg =  TRUE;
	
		if( $this->input->post('u_is_pg') == 'y' )
		{
			$data['u_is_pg'] =  'y';
			
			$data_pg['pg_cert'] = $u_pg_cert1_file;
			
			$data_pg['pg_u_regno'] = $regno;
			
			$data_pg['pg_degree'] = $this->input->post('pg_degree');
			
			$data_pg['pg_subject'] = $this->input->post('pg_subject');
			
			$data_pg['pg_college'] = $this->input->post('pg_college');
			
			$data_pg['pg_univ'] = $this->input->post('pg_univ');
			
			$data_pg['pg_year'] = $this->input->post('pg_year');
			
			$data_pg['pg_mci_recog'] = $this->input->post('pg_mci_recog');
			
			$data_pg['pg_order'] = 1;
			
			$res_del = $this->db->delete('post_grad', array( 'pg_u_regno' => $regno ));
	
			// now insert the new values
			
			if( $res_del )
			{
				$res_pg = $this->db->insert('post_grad', $data_pg);	
			}
			
			if( $this->input->post('add_pg1') == 'y' )
			{
				unset( $data_pg );
				
				$data_pg['pg_cert'] = $u_pg_cert2_file;
			
				$data_pg['pg_u_regno'] = $regno;
				
				$data_pg['pg_degree'] = $this->input->post('pg_degree1');
				
				$data_pg['pg_subject'] = $this->input->post('pg_subject1');
				
				$data_pg['pg_college'] = $this->input->post('pg_college1');
				
				$data_pg['pg_univ'] = $this->input->post('pg_univ1');
				
				$data_pg['pg_year'] = $this->input->post('pg_year1');
				
				$data_pg['pg_mci_recog'] = $this->input->post('pg_mci_recog1');
				
				$data_pg['pg_order'] = 2;
				
				$res_pg = $this->db->insert('post_grad', $data_pg);	
			}
			
			if( $this->input->post('add_pg2') == 'y' )
			{
				unset( $data_pg );
				
				$data_pg['pg_cert'] = $u_pg_cert3_file;
			
				$data_pg['pg_u_regno'] = $regno;
				
				$data_pg['pg_degree'] = $this->input->post('pg_degree2');
				
				$data_pg['pg_subject'] = $this->input->post('pg_subject2');
				
				$data_pg['pg_college'] = $this->input->post('pg_college2');
				
				$data_pg['pg_univ'] = $this->input->post('pg_univ2');
				
				$data_pg['pg_year'] = $this->input->post('pg_year2');
				
				$data_pg['pg_mci_recog'] = $this->input->post('pg_mci_recog2');
				
				$data_pg['pg_order'] = 3;
				
				$res_pg = $this->db->insert('post_grad', $data_pg);	
			}				
				
		}
		//else // else commented by preeti on 3rd mar 14
		else if( $this->input->post('u_is_pg') == 'n' )// else if added by preeti on 3rd mar 14
		{
			$res_del = $this->db->delete('post_grad', array( 'pg_u_regno' => $regno ));
			
			$data['u_is_pg'] =  'n';		
		}
	
		
		
		$data['u_dd_num'] = $this->input->post('u_dd_num');
		
		$data['u_dd_date'] = calen_to_db( $this->input->post('u_dd_date') ) ;
		
		$data['u_dd_bank'] = $this->input->post('u_dd_bank');
		
		$data['u_dd_copy'] = $u_dd_copy_file;
		
		$data['u_intern_complete'] = $this->input->post('u_intern_complete' );
		
		if( $this->input->post('u_intern_complete') == 'y' )
		{
			$data['u_intern_date'] = calen_to_db( $this->input->post('u_intern_date') );
			
			$data['u_intern_date_likely'] = '';
		}
		else
		{
			$data['u_intern_date_likely'] = calen_to_db( $this->input->post('u_intern_date_likely') );
			
			$data['u_intern_date'] = '';		
		}
		
		
		if( $u_intern_cert_file != '' )// if added by preeti on 24th feb 14
		{
			$data['u_intern_cert'] = $u_intern_cert_file; 	
		}
		else 
		{
			$data['u_intern_cert'] = '';	
		}
		
		
		
		$data['u_mbbs_cert'] = $u_mbbs_cert_file; // line added by preeti on 19th feb 14
		
		//$data['u_pg_cert'] = $u_pg_cert_file; // line added by preeti on 19th feb 14		
		
		
		$data['u_updatedon'] = date('Y-m-d H:i:s');	
		
		
		$this->db->where('u_regno', $regno);
			
		$res_user = $this->db->update('user', $data);	
		
		
		if( $res_user && $res_pg )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;
		}		
	}
	

	function show_user_data1( $u_regno ) // to show saved data on the first page of registration
	{
		$qry = " SELECT * FROM user u
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $u_regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}		
				
	}	

	/*function show_user_data2( $u_regno ) // to show saved data on the second page of registration
	{
		$qry = " SELECT * FROM user u
		
		JOIN register reg ON u.u_regno = reg.reg_regno
		
		LEFT JOIN postal post ON u.u_regno = post.post_u_regno
		
		LEFT JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN previous_ssc prev ON u.u_regno = prev.ps_u_regno
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $u_regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}		
				
	}*/
	
	
	
	/*function show_user_data2( $u_regno ) // to show saved data on the second page of registration
	{
		$qry = " SELECT * FROM user u
		
		JOIN register reg ON u.u_regno = reg.reg_regno
		
		LEFT JOIN post_grad post ON u.u_regno = post.pg_u_regno		
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $u_regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}		
				
	}	*/ // commented and below lines added by preeti on 25th feb 14
	
	
	function show_user_data2( $u_regno ) // to show saved data on the second page of registration
	{
		$qry = " SELECT * FROM user u
		
		JOIN register reg ON u.u_regno = reg.reg_regno
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $u_regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}		
				
	}
	
	function get_pg_data( $u_regno )
	{
		$this->db->select('*');
		
		$this->db->from('post_grad');
		
		$this->db->where('pg_u_regno', $u_regno);
		
		$this->db->order_by('pg_order', 'ASC');
		
		$res = $this->db->get();
		
		if( $res->num_rows > 0 )
		{
			return $res->result();
		}
		else
		{
			return FALSE;	
		}
	}
	
	function save_user3( $u_sign_file, $ce_emp_noc_file = '', $ps_rel_order_file = '' )
	{
		// if the registration number is set in the hidden field
		
		$regno = '';		
		
		if( $this->input->post('reg_id') != '' )
		{
			$reg_id = $this->input->post('reg_id');	
			
			$regno = $this->get_regno( $reg_id );			
		}
		else 
		{
			return;	
		}
		
		
		$res_emp = TRUE;
		
		if( $this->input->post('u_current_emp') == 'y' ) 
		{
			$data['u_current_emp'] = 'y';
			
			$data_ce['ce_u_regno'] = $regno;
			
			$data_ce['ce_employer'] = $this->input->post('ce_employer');
			
			$data_ce['ce_emp_noc'] = $ce_emp_noc_file;	
			
			
			$res_del = $this->db->delete('current_employer', array( 'ce_u_regno' => $regno ));
	
			if( $res_del )
			{
				$res_emp = $this->db->insert('current_employer', $data_ce);	
			}
			else 
			{
				$res_emp = '';
			}				
			
		}
		else 
		{
			$data['u_current_emp'] = 'n';	
		}
		
		
		$data['u_ncc_cert'] = $this->input->post('u_ncc_cert');
		
		$data['u_hobbies'] = $this->input->post('u_hobbies');
		
		$data['u_pref1'] = $this->input->post('u_pref1');
		
		$data['u_pref2'] = $this->input->post('u_pref2');
		
		$data['u_pref3'] = $this->input->post('u_pref3');	
	
		
		
		$res_ps = TRUE;
		
		if( $this->input->post('u_previous_ssc') == 'y' ) 
		{
			$data['u_previous_ssc'] = 'y';
			
			
			$data_ps['ps_u_regno'] = $regno;
			
			$data_ps['ps_com_date'] = calen_to_db( $this->input->post('ps_com_date') );
			
			$data_ps['ps_rel_date'] = calen_to_db( $this->input->post('ps_rel_date') );
			
			$data_ps['ps_rel_order'] = $ps_rel_order_file;
			
					
			$res_del = $this->db->delete('previous_ssc', array( 'ps_u_regno' => $regno ));
	
			// now insert the new values
				
			if( $res_del )
			{
				// insert in the post_grad table
				
				$res_ps = $this->db->insert('previous_ssc', $data_ps);	
			}
			else 
			{
				$res_ps = '';
			}				
			
		}
		else 
		{
			$data['u_previous_ssc'] = 'n';	
		}

		// insert in postal table 
		
		$data_post['post_address'] = $this->input->post('post_address');
		
		$data_post['post_state'] = $this->input->post('post_state');
		
		$data_post['post_pin'] = $this->input->post('post_pin');
		
		$data_post['post_tel'] = $this->input->post('post_tel');
		
		$data_post['post_u_regno'] = $regno;
		
		
		$res_del = $this->db->delete('postal', array( 'post_u_regno' => $regno ));
	
		if( $res_del )
		{
			$res_post = $this->db->insert('postal', $data_post);	
		}
		else 
		{
			$res_post = '';
		}
		
		
		
		// insert in permanent table
		
		$data_perm['perm_address'] = $this->input->post('perm_address');
		
		$data_perm['perm_state'] = $this->input->post('perm_state');
		
		$data_perm['perm_pin'] = $this->input->post('perm_pin');
		
		$data_perm['perm_tel'] = $this->input->post('perm_tel');
		
		$data_perm['perm_u_regno'] = $regno;
		
		
		$res_del = $this->db->delete('permanent', array( 'perm_u_regno' => $regno ));
	
		if( $res_del )
		{
			$res_perm = $this->db->insert('permanent', $data_perm);	
		}
		else 
		{
			$res_perm = '';
		}

		
		// below lines added by preeti on 11th mar 14
		
		if( $this->input->post('perm_as_post') != '' )
		{
			$data['u_perm_as_post'] = 'y';	
		}
		else 
		{
			$data['u_perm_as_post'] = 'n';
		}
		
		
		$data['u_sign'] = $u_sign_file;
		
		$data['u_updatedon'] = date('Y-m-d H:i:s');		
		
		$this->db->where('u_regno', $regno);
			
		$res_user = $this->db->update('user', $data);	
		
		
		if( $res_user && $res_emp && $res_ps && $res_post &&  $res_perm)
		{
			// update the status in the register table
			
			$data_up['reg_status'] = 'c';
			
			$this->db->where('reg_regno', $regno);
			
			$res_up = $this->db->update('register', $data_up);
			
			if( $res_up )
			{
				return TRUE;	
			}
			else 
			{
				return FALSE;	
			}			
			
		}
		else 
		{
			return FALSE;
		}		
	}	
	
	function show_user_data3( $u_regno ) // to show saved data on the second page of registration
	{
		$qry = " SELECT * FROM user u
		
		JOIN register reg ON u.u_regno = reg.reg_regno
		
		LEFT JOIN postal post ON u.u_regno = post.post_u_regno
		
		LEFT JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno
		
		LEFT JOIN previous_ssc ssc ON u.u_regno = ssc.ps_u_regno
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( 'u_regno' => $u_regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}		
				
	}

	function get_reg_detail( $reg_id )
	{
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->get('register');
		
		$res_arr = $res->result();
		
		return $res_arr[0];
	}
	
	function update_reg_pass( $reg_id, $reg_pass )
	{
		// below code commented for black-box testing by preeti on 26th mar 14	
			
		//$data['reg_pass'] =  md5( $reg_pass );
		
		// start, code added for black-box testing by preeti on 26th mar 14
		
		$reg_pass = md5( $reg_pass );
		
		$data['reg_pass'] =  $reg_pass;
		
		// end, code added for black-box testing by preeti on 26th mar 14
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->update('register', $data);
		
		if( $res )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;	
		}
	}
	
	function save_user_reg( $u_regno )
	{
		$this->db->where('u_regno', $u_regno);
		
		$res = $this->db->get('user');
		
		if( $res->num_rows > 0 )
		{
			return TRUE;
		}
		else
		{
			$data['u_regno'] = $u_regno;
		
			$data['u_updatedon'] = date('Y-m-d H:i:s');
		
			$res = $this->db->insert('user', $data);
		
			return $res;	
		}	
		
	}
	
	function check_regno( $reg_regno )
	{
		
		
		$this->db->where('reg_regno', $reg_regno);
		
		$this->db->where('reg_status', 'i');
		
		$res = $this->db->get('register');
		
		if( $res->num_rows() == 1 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->reg_id;
		}	
		else 
		{
			return FALSE;	
		}
	}
	
	function check_today_otp( $reg_regno )
	{
		$this->db->where('reg_regno', $reg_regno);
		
		$res = $this->db->get('register');
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
			
			$obj = $res_arr[0];
			
			$reg_otp_start = $obj->reg_otp_start;
			
			$todays_date = date('Y-m-d');
			
			// check if previous otp generation was done today only
			
			if( strpos( $reg_otp_start , $todays_date) !== FALSE ) 
			{
				return FALSE;
			}
			else 
			{
				return TRUE;	
			}
		}
	}
	
	function get_last_regnum()
	{
		$this->db->select_max('value');
		
		$this->db->from('regnum');
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		return $res_arr[0]->value;
		
	}

	function add_regnum( $regnum )
	{
		$data['value'] =  $regnum;
		
		$res = $this->db->insert('regnum', $data);
		
		if( $res )
		{
			// delete all the previous values before the regnum
			
			$this->db->where('value <', $regnum);
			
			$this->db->delete('regnum');
		}
		
		return $res;
	}
	
	function get_last_date()
	{
		$this->db->select('*'); // modified by preeti on 2nd apr 14
		
		$this->db->from('last_date');
		
		$this->db->order_by('ld_id', 'DESC');
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		return $res_arr[0];
			
	}
	
	function get_reg_creation( $reg_id )
	{
		$this->db->select('reg_createdon');
		
		$this->db->from('register');
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		return $res_arr[0]->reg_createdon;
		
	}
	
	function get_state_name( $state_id )
	{
		$this->db->select('state_name');
		
		$this->db->from('state');
		
		$this->db->where('state_id', $state_id);
		
		$res = $this->db->get();
		
		if( $res->num_rows() > 0  )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->state_name;
		}
		
		return ;
	}
	
	function get_user_detail( $regno )
	{
		$this->db->select('reg_email, reg_mobile');
		
		$this->db->from('register');
		
		$res = $this->db->get();
		
		$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN post_grad grad ON u.u_regno = grad.pg_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( $regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}
		else 
		{
			return ;	
		}		
		
	}
	
	function get_admin_email()
	{
		$this->db->select('admin_email');
		
		$res = $this->db->get('admin', 1, 0);	
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->admin_email;
		}
		else 
		{
			return FALSE;	
		}
	}
	
	/*function get_user_name( $regno ) // function added by preeti on 20th feb 14
	{
		$this->db->select('u_fname, u_mname, u_lname');	
		
		$this->db->from('user');
		
		$this->db->where('u_regno', $regno);
		
		$res = $this->db->get();
		
		$res_arr = $res->result();
		
		return $res_arr[0]->u_fname.' '.$res_arr[0]->u_mname.' '.$res_arr[0]->u_lname;
	}*/ // commented by preeti on 27th feb 14
	
	function get_user_name( $regno )  // added by preeti on 27th feb 14
	{
				
		// below query modified by preeti on 3rd mar 14	
			
		$qry = " SELECT reg.reg_regno, u.u_is_name_change , u.u_fname, u.u_mname, u.u_lname,
		
		new.new_fname , new.new_mname, new.new_lname
		
		FROM register reg 
		
		JOIN user u ON reg.reg_regno = u. u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		WHERE reg.reg_regno = '".$regno."'";
		
		$res = $this -> db -> query( $qry );

		$res_arr = $res -> result();

		$obj = $res_arr[0];
		
		if( $obj->u_is_name_change == 'y' )
		{
			$name = $obj->new_fname.' '.$obj->new_mname.' '.$obj->new_lname;	
		}
		else 
		{
			$name = $obj->u_fname.' '.$obj->u_mname.' '.$obj->u_lname;	
		}	

		return $name;
	}
	
	// below function added by preeti on 4th mar 14
	
	function get_reg_data( $reg_id )
	{
		$this->db->select('reg_regno,reg_otp,reg_status');
		
		$this->db->from('register');
		
		$this->db->where('reg_id', $reg_id);
		
		$res = $this->db->get();
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0];
		}
		
		return 0;
	}
	
	// below function added by preeti on 2nd apr 14
	
	function get_dob( $reg_id )
	{
		$qry = " SELECT u.u_dob FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno 
		
		WHERE reg.reg_id = ?";
		
		$res = $this->db->query( $qry, array( 'reg_id' => $reg_id ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
			
			return $res_arr[0]->u_dob;
		}
		
			
	}
	
	
	function alpha_and_space($str) // only for alpha and space
	{
	    if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}

	// code added by preeti on 11th apr 14 for black-box testing

	function alpha_at_space($str) // only for alpha and space and @
	{
		 //if( ! preg_match("/^([-a-z@_ ])+$/i", $str) )
		 if( ! preg_match("/^([-a-z_@\. ])+$/i", $str) )
		 {
		    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
					
		    	return FALSE;
		 }
		else 
		{
			return TRUE;	
		}	
			
	}
	
	// code added by preeti on 11th apr 14 for black-box testing

	function alpha_at_num_space($str) // only for alpha and space and @ and number
	{
		 //if( ! preg_match("/^([-a-z@_ ])+$/i", $str) )
		 if( ! preg_match("/^([-a-z0-9_@\. ])+$/i", $str) )
		 {
		    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
					
		    	return FALSE;
		 }
		else 
		{
			return TRUE;	
		}	
			
	}

	// code added by preeti on 11th apr 14 for black-box testing

	function alpha_num_dash($str) // for alpha-numeric with space, dash, underscore, forward slash
    {
        return ( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $str))? FALSE : TRUE;    
	}
	
	// code added by preeti on 11th apr 14 for black-box testing

	function only_num($str) // for numeric
    {
        return ( ! preg_match("/^([0-9])+$/i", $str))? FALSE : TRUE;    
	}
	
	
	// code added by preeti on 22nd apr 14 for manual testing
	
	function check_old_new_pass_same( $old_password, $new_password )// old pass is salted md5 value and password is md5 value 
	{
		$salt = $this->session->userdata('user_salt');			
			
		$result_pass = md5( $new_password.$salt );
			
		if( $result_pass === $old_password )
		{
			return TRUE; 	
		}
		else 
		{
			return FALSE;	
		}
	}
		
	
}
