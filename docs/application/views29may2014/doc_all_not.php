<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Notification List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script src="<?php echo base_url(); ?>js/jquery.min.js"></script> <!-- added by preeti on 20th feb 14 -->

<script src="<?php echo base_url(); ?>js/marquee.js"></script> <!-- added by preeti on 20th feb 14 -->




</head>

<body>
<!-- right and left box added by vijay -->


	<?php $this->load->view('includes/sideblock'); ?>



<!-- right and left box added by vijay -->

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>

    <div id="changing">

      <div class="contentsquaresmall">

<!-- CODE ADDED BY VIJAY -->
<div style="background-color:#4B4B4B; text-align:center; font-weight:bold; color:#FFFFFF; height:auto;">
    <u>JOIN ARMY MEDICAL CORPS
AS SHORT SERVICE COMMISSIONED (SSC) OFFICER FOR 
A PROMISING AND CHALLENGING CAREER </u>
</div>
<!-- CODE ADDED BY VIJAY -->
		  	
          	<h2 align="center" style="margin-left:0px; float:none"><u>NOTIFICATION LIST</u></h2>     	
          
          
          <div >
          	
          	<!--<marquee behavior="scroll" direction="up" scrollamount="2" height="100" width="350">-->
          	
          	<!-- above line commented below line added by preeti on 20th feb 14 -->
          	
          	<marquee behavior="scroll" direction="up" scrollamount="1">
          	
          <?php
          foreach( $record as $val )
		  {	  	
			if( $val->not_type == 'c' )
			{		  	
				$url = base_url().'doc/not/'.$val->not_id;
          ?>
                <!-- below line modified by preeti on 3rd mar 14 --> 
                   
          		<div class="not_left"> <a href="<?php echo $url; ?>"><?php echo strtoupper( $val->not_title ) ; ?> - <?php echo db_to_calen( $val->not_date ); ?></a> </div>

		<?php		  
			}
			else if( $val->not_type == 'l' )
			{
				$url = base_url().'uploads/not/'.$val->not_link;
			?>	
			
				<!-- below line modified by preeti on 3rd mar 14 -->
			
				<div class="not_left"> <a target="_blank" href="<?php echo $url; ?>"><?php echo strtoupper( $val->not_title ) ; ?> - <?php echo db_to_calen( $val->not_date ); ?></a> </div>
			
				
			
			<?php
			}
		  
		  }		  
		  ?>
		  
		  </marquee>
		  
		  </div>

        </div>      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<!-- script tag added by preeti on 20th feb 14 -->

<script>
	
	$('marquee').marquee('pointer').mouseover(function () {
  $(this).trigger('stop');
}).mouseout(function () {
  $(this).trigger('start');
}).mousemove(function (event) {
  if ($(this).data('drag') == true) {
    this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
  }
}).mousedown(function (event) {
  $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
}).mouseup(function () {
  $(this).data('drag', false);
});
	
</script>

</body>

</html>