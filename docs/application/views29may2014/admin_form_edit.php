<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Edit Email Format</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>asset/ckeditor/ckeditor.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin'); ?>
	
	<div id="changing">

      <div class="cksquaresmall">
      	
      	<?php
          	
          		echo form_open_multipart('admin/form_update');
					
		?>			
		
			
			<div class="heading">Edit Email Format</div>
			
			<div class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
			
			<div class="collect-signup">
				
				<div class="left"><span class="star">*</span>Mandatory Field</div>
			
				<div class="right">&nbsp;</div>		
		
			</div> 
			
			
		
		<div class="collect-signup">
			
			<div class="left"><label for="form_title">Title<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" name="form_title" id="form_title" 
				
				value="<?php echo $record->form_title; ?>" />
									
			</div>		
			
			</div>
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_subject">Subject<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" name="form_subject" id="form_subject" 
				
				value="<?php echo $record->form_subject; ?>" />
									
			</div>		
			
			</div>
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_content">Email Body<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >&nbsp;</div>		
			
			</div>	
			
			
			<div>
			
			<div style="width: 500px;clear: both;padding:5px;margin:5px;" >
				
				<textarea cols="60" id="form_content" name="form_content" rows="23"><?php echo str_replace("<br />", "\n", $record->form_content); ?></textarea>
								
			</div>
			
			</div>	

		
			
			
			<div class="collect-signup">
				
				<div class="left">&nbsp;</div>
			
			<div class="right"><input type="hidden" name="form_id" value="<?php echo $record->form_id; ?>" />
					
					<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
					<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
					
					<?php echo form_submit('sub', 'Submit'); ?> </div>		
		
			</div>         	
          
          <?php						
				echo form_close();
          	
          	?>

        </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>