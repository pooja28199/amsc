<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Call Letter</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme">
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />


<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script>





</head>

<body>
<!-- right and left box added by vijay -->
<?php $this->load->view('includes/sideblock'); ?>


<!-- right and left box added by vijay -->

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>

    <div id="changing">

      <div class="contentsquaresmall">


		  	
          	<h2 align="center" style="margin-left:0px; float:none;"><u>Call Letter</u></h2>     	
          
		  <?php if($errmsg!='')
		  {
		  ?>
		  <p align="center">
		  <?php
		  echo $errmsg;
		  
		  ?>
		  </p>
		  <?php
		  }
		  
		  ?>
		  
          <p align="center">Please enter your registration number and date of birth in the specified format and click submit to download your call letter.</p>
          	<?php
          	
          	echo form_open('cletter/download');
					
		?>	
          <table align="center" cellspacing="5" cellpadding="5">
          <tr>
          
          <td><label>Registrtion number</label>
          </td>
          <td><input type="text" name="regno" placeholder="201406SSCXXXX" /></td>
          
          </tr>
          
          
          <tr>
          
          <td><label>Date of Birth</label>
          </td>
          <td>
          <input type="text" readonly="readonly" name="dob" id="dob" value="" placeholder="dd/mm/yyyy" />
				
				<span  style="display: none;">
				
					<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
				</span>
         
          
         
          </td>
          
          </tr>
          <tr>
          <td>
          </td>
          <td>
          <input type="submit" value="Submit" />
          </td>
          </tr>
          </table>
          <?php
		 echo form_close();
		  ?>
          
          <script type="text/javascript">

			$('#dob').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
		

</script>
          <div >
          	
          
          	
		  
		  </div>

        </div>      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<!-- script tag added by preeti on 20th feb 14 -->



</body>

</html>