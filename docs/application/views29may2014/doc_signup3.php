<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Registration</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup3.js"></script> 

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
	
	<div id="changing">

      <div class="regsquaresmall">
      	
      	<div class="heading">REGISTRATION STEP - 3</div>
      	
      	<div class="error" id="status">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
      	
      	<form id="upload_form" action="<?php echo base_url(); ?>doc/save3" method="post" enctype="multipart/form-data">

		<!-- commented by preeti on 23rd apr 14 -->      	
      	
      	<!--<div class="left-col"><a class="link" href="<?php echo base_url(); ?>doc/doc_signup2/<?php echo $reg_id; ?>">Back to Step 2</a></div>-->
			
		<div class="sub-form">EMPLOYMENT DETAILS</div>
			
			<!-- below form modified by preeti on 11th mar 14 -->
			
			<div class="bind-col">
			
			<div class="left-col"><label>Are you Employed ?<span class="error">*</span></label></div>
			
			<div class="right-col">

      				Yes<input type="radio" name="u_current_emp" value="y" <?php if($record->u_current_emp == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="u_current_emp" value="n" <?php if($record->u_current_emp == 'n' ){ echo "checked"; } ?> />

			</div>
			
			</div>

      	
      		<div id="emp_div">
      			

			<div class="bind-col">
			
			<div class="left-col"><label>Current Employer<span class="error">*</span></label></div>
			
			<div class="right-col">

				<!-- below line modified by preeti on 21st apr 14 for manual testing -->

				<input <?php echo 'autocomplete="off"'; ?> style="width:150px;" type="text" value="<?php echo $record->ce_employer; ?>"  name="ce_employer" id="ce_employer" />
      				
			</div>
			
			</div>

      		
      		
      		<?php
		    if( $record->ce_emp_noc != '' )
			{
				$url = base_url().'uploads/emp_noc/'.$record->ce_emp_noc;
			?>
			
				<!-- id added to the below div by preeti on 3rd mar 14 -->
			
				<div class="bind-col" id="noc_upload">
				
				<div class="left-col"><label>Uploaded NOC<span class="error">*</span></label></div>
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 7th mar 14 -->
	
					<a target="_blank" title="Click Here" class="" href="<?php echo $url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>
	      				
				</div>
				
				</div>		      		
			      		
		    <?php
			}	
		    ?>
		    
			<div class="bind-col">
				
				<div class="left-col"><label>Upload No Objection Certificate <span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<input type="file" name="ce_emp_noc" id="ce_emp_noc" />
	      				
	      			<br />
	      			
	      			<span class="small">Type: pdf / Max Size: 200KB</span>	
	      				
				</div>
				
			</div>		    
	
	  		</div> <!-- emp_div ends here -->
	  		
	  		<div class="clear"></div>
	  		
	  		<div class="sub-form">OTHER DETAILS</div>
      	
      	
    		<div class="bind-col">
				
				<div class="left-col"><label>NCC Training Certificate </label></div>
				
				<div class="right-col">
	
					<select name="u_ncc_cert" id="u_ncc_cert" style="width: 150px;">
      					
      					<!-- below options text changed by preeti on 3rd mar 14 -->
      					
      					<option value="">None</option>
      					
      					<option <?php if( $record->u_ncc_cert == 'a' ){ echo "selected"; } ?> value="a">A</option>
      					
      					<option <?php if( $record->u_ncc_cert == 'b' ){ echo "selected"; } ?> value="b">B</option>
      					
      					<option <?php if( $record->u_ncc_cert == 'c' ){ echo "selected"; } ?> value="c">C</option>
      					
      				</select>
	      				
				</div>
				
			</div>	  		
    
    
    		<div class="bind-col-photo">
				
				<div class="left-col"><label>Hobbies<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<textarea name="u_hobbies" id="u_hobbies" rows="3" cols="16"><?php echo $record->u_hobbies; ?></textarea>
					<br /><span class="small">(Comma Separated)</span>
	      				
				</div>
				
			</div>		    
	  		
    
    		<div class="bind-col">
				
				<div class="left-col"><label>First Preference of Service<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<select style="width: 150px;" name="u_pref1" id="u_pref1">
      					
      					<option value="">Select</option>
      					
      					<option <?php if( $record->u_pref1 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
      					
      					<option <?php if( $record->u_pref1 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
      					
      					<option <?php if( $record->u_pref1 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>
      					
      				</select>
	      				
				</div>
				
			</div>		    
	
      		
      	<div class="bind-col">
				
				<div class="left-col"><label>Second Preference of Service<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<select style="width: 150px;" name="u_pref2" id="u_pref2">      					
      					
      					<option value="">Select</option>
      					
      					<?php
      					if( $record->u_pref2 != '' )
						{
      					?>
      					
	      					<option <?php if( $record->u_pref2 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
	      					
	      					<option <?php if( $record->u_pref2 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
	      					
	      					<option <?php if( $record->u_pref2 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>
	      					
      					<?php
						}
      					?>
      					
      				</select>
	      				
				</div>
				
			</div>		    
	
      	
      	  	<div class="bind-col">
				
				<div class="left-col"><label>Third Preference of Service<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<select style="width: 150px;" name="u_pref3" id="u_pref3">      					
      					
      					<option value="">Select</option>

						<?php
      					if( $record->u_pref3 != '' )
						{
      					?>
      					
      					<option <?php if( $record->u_pref3 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
      					
      					<option <?php if( $record->u_pref3 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
      					
      					<option <?php if( $record->u_pref3 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>
      					
      					<?php
						}
      					?>
      					
      				</select>
						      				
				</div>
				
			</div>	
			
			<div class="clear"></div>
			
			<div class="sub-form">PREVIOUS AMC SERVICE</div>		
      	
      	    <div class="bind-col">
				
				<div class="left-col"><label>Have you worked in AMC service before ?<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					Yes<input type="radio" name="u_previous_ssc" value="y" <?php if( $record->u_previous_ssc == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="u_previous_ssc" value="n" <?php if( $record->u_previous_ssc == 'n' ){ echo "checked"; } ?> />
											      				
				</div>
				
			</div>	
  
      	     
      	    <div id="ssc_div">
      	    	
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Date Of Commission <span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<input readonly="readonly" value="<?php if( $record->ps_com_date != '0000-00-00' ){ echo db_to_calen( $record->ps_com_date ) ; } ?>" name="ps_com_date" id="ps_com_date" size="14" type="text" />
      			
      				<span  style="display: none;">
				
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	      			
      			
											      				
				</div>
				
			</div>	
  
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Date Of Release<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<input readonly="readonly" name="ps_rel_date" value="<?php if( $record->ps_rel_date != '0000-00-00' ){ echo db_to_calen( $record->ps_rel_date ) ; } ?>" id="ps_rel_date" size="14" type="text" />
      			
      				<span  style="display: none;">
				
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	 
												      				
				</div>
				
			</div>	
  
  			<?php
      		
      		if( $record->ps_rel_order != '' )
			{
				$url = base_url().'uploads/prev_rel_order/'.$record->ps_rel_order;
			
			?>	
      		   
      		  <!-- id added to the below div by preeti on 3rd mar 14 --> 
      		   
    		<div class="bind-col" id="rel_upload">
				
				<div class="left-col"><label>Uploaded copy of release order<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<!-- below line modified by preeti on 7th mar 14 -->
					
					<a class="" title="Click Here" target="_blank" href="<?php echo $url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>	 
												      				
				</div>
				
			</div>	
  
      	    
      	    <?php
			}
      		
      		?>
      		
      		<div class="bind-col">
				
				<div class="left-col"><label>Upload a copy of release order <span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<input name="ps_rel_order" id="ps_rel_order" type="file" />	
					
					<br />
					
					<span class="small">Type: pdf / Max Size: 200KB</span> 
												      				
				</div>
				
			</div>   
      	    	
      	    </div> <!-- ssc_div ends here -->
      	     
      	    <div class="clear"></div> 
      	     
      	    <div class="sub-form">ADDRESS DETAILS</div> 
      	      
      	     
      	    <div class="bind-col">
				
				<div class="left-col"><label>Mobile</label></div>
				
				<div class="right-col">
	
					<?php echo $record->reg_mobile; ?>	 
												      				
				</div>
				
			</div>   
    
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Email</label></div>
				
				<div class="right-col">
	
					<?php echo $record->reg_email; ?>	 
												      				
				</div>
				
			</div>   
      	
      	    <div class="clear"></div>  	
      	      	
      	    <div class="subsub-form">POSTAL ADDRESS</label></div> 
      	    
      	    <div class="bind-col-photo">
				
				<div class="left-col"><label>Address<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<textarea name="post_address" id="post_address" rows="3"  cols="16"><?php echo $record->post_address; ?></textarea>
												      				
				</div>
				
			</div>   	
      	      	
      	     
      	    <div class="bind-col">
				
				<div class="left-col"><label>State<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<select style="width: 150px;"  name="post_state" id="post_state">
	      			
	      				<option value="">Select</option>
	      				
	      				<?php
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>
	      				
	      					<option <?php if( $record->post_state == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>      				
	      				      				
	      			</select>
												      				
				</div>
				
			</div>  	
      	      	
      	      
      	    <div class="bind-col">
				
				<div class="left-col"><label>Pincode<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
	
					<input <?php echo 'autocomplete="off"'; ?> maxlength="6" name="post_pin" value="<?php echo $record->post_pin; ?>" id="post_pin" type="text" />
												      				
				</div>
				
			</div>  	  	
    
    		<!-- label changed by preeti on 27th feb 14 -->
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Alternate Contact No. (if any)</label></div>
				
				<div class="right-col">
	
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
	
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->post_tel; ?>" name="post_tel" id="post_tel" />
												      				
				</div>
				
			</div> 
			
			
			<div class="clear"></div>  	
      	      	
      	    <div class="subsub-form">PERMANENT ADDRESS
      	    	
      	    &nbsp;
      	    
      	    <!-- below line modified by preeti on 11th mar 14 -->
      	    	
      	    <input type="checkbox" value="y" name="perm_as_post" id="perm_as_post" <?php if( $record->u_perm_as_post == 'y' ){ echo "checked"; } ?> />
      	    
      	    Same as Postal Address
      	    
      	    </div> 
      	     	  	
    		<div class="bind-col-photo">
				
				<div class="left-col"><label>Address<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<textarea name="perm_address" id="perm_address" rows="3" cols="16"><?php echo $record->perm_address; ?></textarea>
												      				
				</div>
				
			</div> 
			
    
    		<div class="bind-col">
				
				<div class="left-col"><label>State<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<select style="width: 150px;" name="perm_state" id="perm_state">
	      			
	      				<option value="">Select</option>
	      				
	      				<?php
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>	      				
	      					<option <?php if( $record->perm_state == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>       				
	      				      				
	      			</select>
												      				
				</div>
				
			</div> 
	
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Pincode<span class="error">*</span></label></div>
				
				<div class="right-col">
	
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
	
					<input <?php echo 'autocomplete="off"'; ?>  maxlength="6" name="perm_pin" value="<?php echo $record->perm_pin; ?>" id="perm_pin" type="text" />
																	      				
				</div>
				
			</div> 
	
			<!-- label changed by preeti on 27th feb 14 -->
    
    		<div class="bind-col">
				
				<div class="left-col"><label>Alternate Contact No. (if any )</label></div>
				
				<div class="right-col">
	
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
	
					<input <?php echo 'autocomplete="off"'; ?>  type="text" value="<?php echo $record->perm_tel; ?>" name="perm_tel" id="perm_tel" />
																	      				
				</div>
				
			</div> 
	
      	    <?php
      		
      		if( $record->u_sign != '' )
			{
				$url = base_url().'uploads/sign/'.$record->u_sign;
			
			?>	      		
			
				<div class="bind-col-photo">
				
					<div class="left-col"><label>Uploaded Signature</label></div>
					
					<div class="right-col"><img title="Signature" src="<?php echo $url; ?>"   /></div>
					
				</div>
			
			<?php
			}
      		?> 
      		
      		<div class="bind-col">
				
					<div class="left-col"><label>Upload Signature </label></div>
					
					<div class="right-col"><input type="file" name="u_sign" id="u_sign" /><br /><span class="small">width:2 inch(144-240px)/height:1 inch(48-144px)/type:gif,jpg,png,jpeg/size:100KB</span>
						
					</div>
					
			</div> 	
			
			
			<div class="bind-col-photo">
				
				<div class="left-col"><label>Declaration</label></div>
					
				<div class="right-col">
						
						I hereby solemnly declare that all statements made by me in &nbsp; the application are true and correct to the best of my knowledge and belief. &nbsp; At any stage, if information furnished by me is found to be false or incorrect &nbsp; I will be liable for disciplinary action or termination of service as deemed fit.
					
				</div>
					
			</div>
			
			<div class="clear"></div>
			
			<div class="clear"></div>
			
			<div class="clear"></div>
			
			
			<div class="bind-col">
				
				<div class="left-col">
				
					<input type="hidden" name="reg_id" id="reg_id" value="<?php echo $reg_id; ?>" />
      				
      				<input type="hidden" name="ce_emp_noc_hid" id="ce_emp_noc_hid" value="<?php echo $record->ce_emp_noc; ?>" />
      				
      				<input type="hidden" name="ps_rel_order_hid" value="<?php echo $record->ps_rel_order; ?>" id="ps_rel_order_hid" />
      				
      				<input type="hidden" name="u_sign_hid" value="<?php echo $record->u_sign; ?>" id="u_sign_hid" />
      				
      				<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
					
					<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
					<input type="hidden" name="confirm_random" id="confirm_random" value="<?php echo $confirm_random; ?>" />
					
					<!-- below line added by preeti on 23rd apr 14 for manual testing -->
				
					<input type="hidden" name="sequence_token" id="sequence_token" value="<?php echo $sequence_token; ?>" /> 
					
									
				</div>
					
				<div class="right-col">
				
					<input type="submit" id="save" name="save" value="Save" />		
						
				</div>
					
			</div>
			
      	      	
      	 <?php							
				echo form_close();          	
         ?>

      </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script type="text/javascript">

			$('#ps_com_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#ps_rel_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
						
								
</script>

 </body>

</html>