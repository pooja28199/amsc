<?php

$data_left['current_page'] = $this->uri->segment(3);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Set Last Date</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin', $data_left); ?>
	
	<div id="changing">

      <div class="cksquaresmall">
      	
      	<?php
          	
          		echo form_open_multipart('admin/save_date');
					
		?>	
			
			<div class="heading">Set Last Date for Registration</div>
			
			<div class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo strip_tags( validation_errors() );
				}
				else 
				{
					echo ( $errmsg );	
				}
				
				?>
				
			</div>
			
			<div class="collect-signup">
				
				<div class="left"><span class="star">*</span>Mandatory Field</div>
			
				<div class="right">&nbsp;</div>		
		
			</div>
			
			<!-- below div added by preeti on 2nd apr 14 -->
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_title">Opening Date<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<input type="text" readonly="readonly" name="ld_start" id="ld_start" value="<?php if( $this->input->post('ld_start') != '' ){ echo $this->input->post('ld_start') ; }else{ echo $ld_start; } ?>" />
				
				<span  style="display: none;">
				
					<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
				</span>
									
			</div>		
			
			</div>
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_title">Last Date<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<input type="text" readonly="readonly" name="ld_date" id="ld_date" value="<?php if( $this->input->post('ld_date') != '' ){ echo $this->input->post('ld_date') ; }else{ echo $ld_date; } ?>" />
				
				<span  style="display: none;">
				
					<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
				</span>
									
			</div>		
			
			</div>
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_subject">Validity Days<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="ld_validity" id="ld_validity" value="<?php if( $this->input->post('ld_validity') != '' ){ echo $this->input->post('ld_validity') ; }else{ echo $ld_validity; } ?>" />
									
			</div>		
			
			</div>
			
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="form_subject">OTP Validity ( in Hours )<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="ld_otp_hours" id="ld_otp_hours" value="<?php if( $this->input->post('ld_otp_hours') != '' ){ echo $this->input->post('ld_otp_hours') ; }else{ echo $ld_otp_hours; } ?>" />
									
			</div>		
			
			</div>
			
			
			
			<div class="collect-signup">
				
				<div class="left">&nbsp;</div>
			
			<div class="right">
				
				<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
				<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
				
					<?php echo form_submit('sub', 'Submit'); ?> 
					
			</div>		
		
			</div> 
			
			
			<div class="collect-signup">
				
				<div class="error"><br /><br />Note: Last Date sets the Registration Last date <br /> Validity Days sets the number of days for the New User to Confirm Registration
					
					<br /> OTP Validity ( in Hours ) sets the validity of OTP in hours
					
				</div>		
		
			</div> 
			
			     
		
		  <?php			
				echo form_close();
          	
          	?>

        </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script type="text/javascript">

			$('#ld_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			//below code added by preeti on 2nd apr 14 for black-box testing 
			
			$('#ld_start').datepick({showOnFocus: false, showTrigger: '#calImg1'});

</script>

</body>

</html>