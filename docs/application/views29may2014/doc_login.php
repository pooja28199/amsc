<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Login</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<!-- below scripts are added by preeti on 26th mar 14 for black-box testing -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the password typed 
			
			var pass = $('#reg_pass').val();
			
			if( pass != '' )
			{
				// below lines modified by preeti on 28th mar 14 
			
				//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
				
				var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
				
				pass = md5( pass );
				
				var result = md5( pass + salt );
				
				// set the value of the hidden field
				
				$('#reg_pass_encode').val(result);
				
				// clear the field
				
				$('#reg_pass').val('');	
			}
			
			
		});
		
	});
	
</script>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>

    <div id="changing">
    	
    	

      <div class="regsquaresmall">

		<div class="heading">Login</div> <!-- Line added by preeti on 25th mar 14 -->

          <p>
          	
          	<span class="error">
          		
          		<!-- below lines modified by preeti on 25th mar 14 for black-box testing  -->
          		
          		<?php 
          		
          		if( validation_errors() )
				{
					echo strip_tags( validation_errors() ) ;
				}
				else 
				{
					echo $errmsg;	
				}          		 
          		
          		?>	
          		
          		
          		</span>
          	
          	<?php
          	
          		echo form_open('doc/validate');
				
			?>	
				
				<!--
					
					echo form_label('Reg. No.&nbsp;', 'reg_regno');
				
				// below code modified by preeti on 21st apr 14 for manual testing
				
				$att = array(
				
					'name' => 'reg_regno',
					
					'id' => 'reg_regno',
					
					'value' => $this->input->post('reg_regno'),
					
					'autocomplete' => 'off'
				
				);
				
				
				
				
				echo form_input( $att ); // code added by preeti on 21st apr 14 for manual testing
				
				echo form_label('Password', 'reg_pass');
				
				
				
				$pass_att = array(
				
					'name' => 'reg_pass',
					
					'id' => 'reg_pass',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 24th mar 14 
							
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Login');// code added by preeti on 26th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 26th mar 14 for black-box testing
								
				echo anchor('doc/signup', 'New Registration', array( 'class' => 'user' ));
				
				echo anchor('doc/forgot', 'Forgot Password', array( 'class' => 'pass' ));
				-->
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="reg_regno">Reg. No.<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" <?php echo  'autocomplete="off"'; ?> name="reg_regno" id="reg_regno" value="<?php echo $this->input->post('reg_regno'); ?>" />
						
					</div>
	
				</div>
				
	
				<div class="collect-signup">
			
					<div class="left"><label for="reg_pass">Password<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="password" <?php echo  'autocomplete="off"'; ?> name="reg_pass" id="reg_pass" />
						
					</div>
	
				</div>
	
				
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
				
				
				<div class="collect-signup">			
				
				
					<div class="right">
						
						<input type="hidden" name="reg_pass_encode" id="reg_pass_encode" />
				
						<input type="submit" id="sub" name="sub" value="Login" />				
											
					</div>
					
					<div class="right">
						
						<?php echo anchor('doc/signup', 'New Registration', array( 'class' => 'user' )); ?>
						
						<?php echo anchor('doc/forgot', 'Forgot Password', array( 'class' => 'pass' )); ?>
						
					</div>
		
				</div>
				
				<?php
					
				echo form_close();	
          	
          	?>           	 	
          	
          </p>

        </div>      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>