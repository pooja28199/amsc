<?php

$current_page = $this->uri->segment(2);

?>
<div id="mainleft">

      <div id="contmenu">

        <div id="menu">

          <ul>

            <li <?php if( $current_page == 'home' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/home">Home</a></li>
            
            <li <?php if( $current_page == 'preview' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/preview">Profile Preview</a></li>
            
            <li <?php if( $current_page == 'doprint' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/doprint">Generate PDF</a></li>
            
            <li <?php if( $current_page == 'doc_password' || $current_page == 'update_doc_password' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/doc_password">Change Password</a></li>
            
            <!-- below line modified by preeti on 22nd apr 14 for manual testing -->
            
            <li><a href="<?php echo base_url(); ?>doc/logout/<?php echo $this->session->userdata('random'); ?>">Log Out</a></li>

          </ul>

        </div>

      </div>		

    </div>
