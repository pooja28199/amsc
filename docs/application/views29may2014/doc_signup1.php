<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Registration</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE=8" ><!-- line added by preeti on 23rd apr 14 for ie 8 compatibility -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup1.js"></script> 

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
	
	<div id="changing">

      <div class="regsquaresmall">
      	
      	<div class="heading">REGISTRATION STEP - 1</div>
			
			<!-- below form modified by preeti on 11th mar 14 -->
			
			<div class="error" id="status">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					if( is_array( $errmsg ) && COUNT( $errmsg ) > 0 )
					{
						foreach( $errmsg as $val )
						{
							echo $val;
						}						
					}
					else 
					{
						echo ( $errmsg );	
					}					
				}
				
				?>
				
			</div>
      	
      	<form id="upload_form" action="<?php echo base_url(); ?>doc/save1" method="post" enctype="multipart/form-data">
		
		<div class="form">
			
			
			<div class="sub-form">APPLICANT'S INFORMATION</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Registration Number</label></div>
			
			<div class="right-col"><?php echo $record->u_regno; ?></div>
			
			</div>
			
			<div class="bind-col">
			
				<div class="left-col"><label>First Name<span class="error">*</span>
					
					</label></div>
				
				<!-- below text changed by preeti on 27th feb 14 -->
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?> name="u_fname" id="u_fname" value="<?php echo $record->u_fname; ?>" type="text" />
					<br />
					<span class="small">Same Name as in 10th/Matric Certificate.</span>					
				</div>
				
			</div>
			
			<div class="bind-col">
			
				<div class="left-col"><label>Middle Name</label></div>
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?> name="u_mname" id="u_mname" value="<?php echo $record->u_mname; ?>" type="text" />
					
				</div>
			
			</div>
			
			
			<div class="bind-col">
			
				<!-- below code modified by preeti on 2nd apr 14 for black-box testing -->
			
				<div class="left-col"><label>Last Name</label></div>
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<div <?php echo 'autocomplete="off"'; ?> class="right-col"><input name="u_lname" id="u_lname" value="<?php echo $record->u_lname; ?>" type="text" /></div>
				
			</div>
			
			
			
			
			<?php
      		
      		if( $record->u_photo != '' )
			{
				$photo_url = base_url().'uploads/photo/'.$record->u_photo;     		
      		?>
			
			<div class="bind-col-photo">
			
				<div class="left-col"><label>Uploaded Photo</label></div>
				
				<div class="right-col"><img src="<?php echo $photo_url; ?>" height="60" width="60" /></div>
				
			</div>
			
			<?php
			}
      		?>
			
			<!-- text changed by preeti on 27th feb 14 -->
			<div class="bind-col">
			
				<div class="left-col"><label>Photo <span class="error">*</span>
					
					 
					
				</label></div>
				
				<div class="right-col"><input type="file" name="u_photo" id="u_photo" />
					
					<!-- below line modified by preeti on 15th may 14 -->
					<span class="small">color only,Type:gif,jpg,png,jpeg,Size:100KB,<br/>Width:1 inch(48-144px),Height: 1 inch(48-144px)</span>
					
				</div>
			
			</div>
			
			<div class="clear"></div>
			
			<div class="bind-col">
			
				<div class="left-col"><label>Have you changed your Name after Matriculation ?<span class="error">*</span></label></div>
				
				<div class="right-col">
					
					Yes<input type="radio" name="u_is_name_change" value="y" <?php if( $record->u_is_name_change == 'y' ){ echo "checked";  } ?> />&nbsp;
	      				
	      			No<input type="radio" name="u_is_name_change" value="n" <?php if( $record->u_is_name_change == 'n' ){ echo "checked";  } ?> />
	      			
	      		</div>
						
			</div>
			
			<div id="name_div">
			
			<!-- label changed by preeti on 27th feb 14 -->
			
			<div class="bind-col">
			
				<div class="left-col"><label>New First Name<span class="error">*</span></label></div>
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?> name="new_fname" value="<?php echo $record->new_fname; ?>"  id="new_fname" type="text" /></div>
			
			</div>
			
			<!-- label changed by preeti on 27th feb 14 -->
				
			<div class="bind-col">	
				
				<div class="left-col"><label>New Middle Name</label></div>
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?>  name="new_mname" value="<?php echo $record->new_mname; ?>" id="new_mname" type="text" /></div>
			
			</div>
			
			<!-- label changed by preeti on 27th feb 14 -->
			
			<div class="bind-col">
			
			<!--below code modified by preeti on 2nd apr 14 for black-box testing -->
			
				<div class="left-col"><label>New Last Name</label></div>
				
				<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="new_lname" id="new_lname" value="<?php echo $record->new_lname; ?>" />
					
				</div>
			
			</div>
			
			
				<?php
				
			    if( $record->new_name_proof != '' )
				{
					$url = base_url().'uploads/new_name/'.$record->new_name_proof;
			
				?>	
				
				<div class="bind-col">
				
					<div class="left-col"><label>Uploaded Copy of New Name<span class="error">*</span></label></div>
					
					<!-- below line modified by preeti on 7th mar 14 -->
					
					<div class="right-col"><a title="click to View" target="_blank" href="<?php echo $url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a></div>
					
				</div>
				
				<?php
			
				}
				?>
			
				<div class="bind-col">
			
					<div class="left-col"><label>Upload a Proof of New Name <span class="error">*</span></label></div>
					
					<div class="right-col"><input name="new_name_proof" id="new_name_proof" type="file" /><br /><span class="small"> Type: pdf / Max Size: 200KB </span></div>
				
				</div>
				
				<div class="clear"></div>
			
			</div> <!-- name_div ends here -->
			<div class="bind-col">
			
				<div class="left-col"><label>Name (Hindi)<span class="error">*</span></label></div>
				
				<div class="right-col">
					
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
					
					<input <?php echo 'autocomplete="off"'; ?> type="text"  value="<?php echo $record->u_hindi_name; ?>" name="u_hindi_name" id="u_hindi_name" /></div>
			
			</div>
			
			<div class="bind-col">
			
				<div class="left-col">&nbsp;</div>
				
				<div class="right-col"><a class="link" target="_blank" href="http://www.google.com/inputtools/try/" >Hindi Typing</a></div>
			
			</div>
			
			<div class="bind-col">
			
				<div class="left-col"><label>Gender<span class="error">*</span></label></div>
					
				<div class="right-col">
					
					<select style="width: 150px;" name="u_sex" id="u_sex">
		      			
		      				<option value="">Select</option>
		      				
		      				<option <?php if( $record->u_sex == 'm' ){ echo "selected"; } ?> value="m">Male</option>
		      				
		      				<option <?php if( $record->u_sex == 'f' ){ echo "selected"; } ?> value="f">Female</option>
		      				
		      			</select>
					
				</div>
				
			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Date Of Birth<span class="error">*</span></label></div>
				
			<div class="right-col">
			
			<!--below code modified by preeti on 2nd apr 14 for black-box testing -->
			
			<!-- below line modified by preeti on 21st apr 14 for manual testing -->
			
			<input name="u_dob" id="u_dob" readonly="readonly" value="<?php if( $record->u_dob != '0000-00-00' ){ echo db_to_calen( $record->u_dob ); }?>" type="text" />
      				
      				<span  style="display: none;">
				
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	     				
				
			</div>
			
			</div>
			
			
			<?php
      		
      		if( $record->u_dob_proof != '' )
			{
				$dob_url = base_url().'uploads/dob/'.$record->u_dob_proof;
      		?>

			<div class="bind-col">
			
			<div class="left-col"><label>Uploaded DOB Certificate<span class="error">*</span></label></div>
			
			<!-- below file modified by preeti on 7th mar 14 -->
			
			<div class="right-col"><a title="click to View" target="_blank"  href="<?php echo $dob_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a></div>
			
			
			</div>
			
			<?php
      		}
      		?>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Date Of Birth Certificate<span class="error">*</span></label></div>
			
			<div class="right-col">
				
				<input name="u_dob_proof" readonly="readonly" id="u_dob_proof" type="file" /><br /><span class="small"> Type - pdf, Max Size - 200KB </span>
				
			</div>
			
			</div>
			
			<!-- nationality moved up by preeti on 4th mar 14 -->
			
			<div class="bind-col">
			
			<div class="left-col"><label>Nationality<span class="error">*</span></label></div>
			
			<div class="right-col">
			
			<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> name="u_nationality" value="<?php echo $record->u_nationality; ?>" id="u_nationality" type="text" />
				
			</div>
			
			</div>
			
			
			<div class="clear"></div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Father's First Name<span class="error">*</span></label></div>
			
			
			
			<div class="right-col">
			
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_fname; ?>"  name="u_father_fname" id="u_father_fname" />
			
				<!-- below message added by preeti on 27th feb 14 -->
				
				<span class="small">Type the first name without the salutation like Mr. or Sh. etc</span>
			</div>
			
			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Father's Middle Name</label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_mname; ?>" name="u_father_mname" id="u_father_mname" />
				
			</div>
			
			</div>
			
			<div class="bind-col">
			
			<!--below code modified by preeti on 2nd apr 14 for black-box testing -->
			
			<div class="left-col"><label>Father's Last Name</label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_lname; ?>" name="u_father_lname" id="u_father_lname" />
				
			</div>
			
			</div>
			
			<div class="sub-form">SPOUSE INFORMATION</div>
			
			
			<div class="bind-col">
			
			<div class="left-col"><label>Are you Married ?<span class="error">*</span></label></div>
			
			<div class="right-col">
				
     				Yes<input type="radio" name="u_is_married" value="y" <?php if( $record->u_is_married == 'y' ){ echo "checked"; }  ?> />&nbsp;
     				
     				No<input type="radio" name="u_is_married" value="n" <?php if( $record->u_is_married == 'n' ){ echo "checked"; }  ?> />
     				
			</div>
			
			</div>

			<div id="mar_div">
			
			<div class="bind-col">
			
			<div class="left-col"><label>Date Of Marriage<span class="error">*</span></label></div>
			
			<div class="right-col">
				
					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
      				<input <?php echo 'autocomplete="off"'; ?> name="sp_mar_date" readonly="readonly" id="sp_mar_date" value="<?php if( $record->sp_mar_date != '0000-00-00' ){ echo db_to_calen( $record->sp_mar_date ) ; } ?>" type="text" />
      				
      				<span  style="display: none;">
				
						<img id="calImg2" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	

				
			</div>
			
			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Spouse Name<span class="error">*</span></label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
      			<input <?php echo 'autocomplete="off"'; ?> name="sp_name"  value="<?php echo $record->sp_name; ?>" id="sp_name" type="text" />
				
			</div>
			
			</div>
		
			<div class="bind-col">
		
			<div class="left-col"><label>Spouse Occupation<span class="error">*</span></label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
      			<input <?php echo 'autocomplete="off"'; ?> name="sp_occup" value="<?php echo $record->sp_occup; ?>"  id="sp_occup" type="text" />
				
			</div>
			
			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Spouse Nationality<span class="error">*</span></label></div>
			
			<div class="right-col">
				
      					<select style="width: 150px;" name="sp_nation" id="sp_nation">
	      				
	      				<option value="">Select</option>
	      				
	      				<option <?php if( $record->sp_nation == 'i' ){ echo "selected"; }; ?> value="i">Indian</option>
	      				
	      				<option <?php if( $record->sp_nation == 'f' ){ echo "selected"; }; ?> value="f">Foreign</option>
	      				
	      			</select>
				
			</div>

			</div>			
			
			
			<div id="f_citi" class="bind-col">
			
			<div class="left-col"><label>Date of acquiring Indian Citizenship </label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
   				<input <?php echo 'autocomplete="off"'; ?> name="sp_citizenship_date" readonly="readonly" id="sp_citizenship_date" value="<?php if( $record->sp_citizenship_date != '0000-00-00' ){ echo db_to_calen( $record->sp_citizenship_date ) ;} ?>" type="text" />
      				
      				<span  style="display: none;">
				
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	

				<br /><span class="small"> if foreigner</span>
				
			</div>
			
			</div>
			
			
			<div class="bind-col">
			
			<div class="left-col"><label>Whether applied for grant of SSC in AMC ?<span class="error">*</span></label></div>
			
			<div class="right-col">
				
      				Yes<input type="radio" name="sp_ssc_applied" value="y" <?php if( $record->sp_ssc_applied == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="sp_ssc_applied" value="n" <?php if( $record->sp_ssc_applied == 'n' ){ echo "checked"; } ?> />

				
			</div>
			
			</div>
			
			
			
		</div> <!-- mar_div ends here-->
			
			
			<div class="bind-col">
			
			<div class="left-col">
				
				<input type="hidden" name="reg_id" id="reg_id" value="<?php echo $reg_id; ?>" />

					<input type="hidden" name="u_photo_hid" value="<?php echo $record->u_photo; ?>" id="u_photo_hid" />					
      				
      				<input type="hidden" name="new_name_proof_hid" value="<?php echo $record->new_name_proof; ?>" id="new_name_proof_hid" />
      				
      				<input type="hidden" name="u_dob_proof_hid" value="<?php echo $record->u_dob_proof; ?>" id="u_dob_proof_hid" />
      				
      				<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
				
					<!-- below line added by preeti on 21st apr 14 for manual testing -->
						
					<input type="hidden" name="confirm_random" id="confirm_random" value="<?php echo $confirm_random; ?>" />
				
					<!-- below line added by preeti on 23rd apr 14 for manual testing -->
				
					<input type="hidden" name="sequence_token" id="sequence_token" 
					
					value="<?php echo $sequence_token; ?>" />
				
				
					<input type="submit" name="save_e" id="save_e" value="Save & Exit" />				
				
			</div>
			
			<div class="right-col">
								
				<input type="submit" name="save_c" id="save_c" value="Save & Continue" />
				
			</div>
			
			</div>
			
		</div>	 <!-- form div ends here -->
		
		
		 <?php							
				echo form_close();
          	
         ?>	
			
		</div>
      	
      	

      </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>



<script type="text/javascript">

			$('#u_dob').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#sp_citizenship_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			
			$('#sp_mar_date').datepick({showOnFocus: false, showTrigger: '#calImg2'});

</script>

 </body>

</html>