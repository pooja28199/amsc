<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Registration Success</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
    
    <div id="changing">

      <div class="loginsquaresmall">

          <p><h2><?php echo $errmsg; ?></h2></p>

        </div>

      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>



</body>

</html>