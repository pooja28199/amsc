<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: User's List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />



<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">
<div class="listsquaresmall2">
     
			<!-- below heading changed by preeti on 7th apr 14 -->

          <p><h2>Marking Sheet</h2></p>
          
          <?php
          
          
          if( is_array($records)  && COUNT( $records ) == 0  )
		  {
		  ?>	
		  	<span>No Users Found !</span>
		  <?php
		  }
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>         
          <form id="f1" action="<?php echo base_url(); ?>admin/searchnew1" method="post">
          		
          		<table>
          			
          			<!--<tr>
          				
          				<td align="center"><b>Select Regnos from and to </b></td>
          			</tr>-->
          			
          			
          			<tr>
          				<td>
          					
          					<span class="txt-label">From (Reg. No.)</span>
          					         					
          				</td>
          					
          				<td>
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="from"  id="from" />
          					
          				</td>	
          					
          				<td>
          					
          					<span class="txt-label">To (Reg. No.)</span>         					
          					
          				</td>
          				
          				
          				<td>         					
          						
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->	
          						
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="to"  id="to" />
          					
          				</td>
          			
          			</tr>
          			
          			<tr>
          				
          				<td valign="top" colspan="4">
          					
          				<input style="margin-left: 210px;margin-top:20px; " type="submit" name="sub" value="Search" />
          				
          				</td>
          				          				
          			</tr>
          			
          			</table></form>         
           		
          <?php
          
          $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
          $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$i = $uri_arr['offset'] + 1;
			
			$offset = $uri_arr['offset'] ;
				
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	  // add a link to download as excel	
			  
			  $files=array();	 
				
			  $from = $this->input->post('from');
			
			  if( $this->input->post( 'from' ) )
			  {
				$from = $this->input->post( 'from' );	
					
			  }
			  else
			  {
			  	$from =0;
			  }
			  
			  // below code added by preeti on 11th apr 14 for black-box testing
			  
			  if( ( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $from)) )
			  {
			  		$from =0;
			  }		  
			 
			  array_push($files,$from);  
		
		      $to = $this->input->post('to');
			
			  if( $this->input->post( 'to' ) )
			  {
				$from = $this->input->post( 'to``' );	
					
			  }
			  else
			  {
			  	$to =0;
			  }
			  
			  // below code added by preeti on 11th apr 14 for black-box testing
			  
			  if( ( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $to)) )
			  {
			  		$to =0;
			  }
			 
		      array_push($files,$to);
			  
			  
		
					  $url1 = base_url().'admin/file_pdf6';	
			  
			  
			  $co = implode("-", $files);
			  
			  
			 // echo $co;
			   $url221 = base_url().'admin/file_pdf4';	
			  
			if( $from||$to)
			{
			  	$encoded = urlencode( $from); // encoded keyword
				
			  	$url221 = base_url().'admin/file_pdf4/'.$co;
				 
			}
			
				$url211 = base_url().'admin/dozipallapp';
				//$url2=base_url().'Member_con/topdf1';
			 $this->table->add_row('<a class="link" href="'.$url221.'">Print Marking Sheet</a>', '<a class="link" href="'.$url211.'">zipall</a>', '&nbsp;','&nbsp;', '&nbsp;'
			  
			  );
	
  			// add the list heading for columns	
				
		$this->table->add_row(
		
			  '<span class="col-label">S.No</span>', 
			  
			  '<span class="col-label">Select</span>',
			  
			  '<span class="col-label">Name</span>', 
			  
			  '<span class="col-label">Reg No.</span>', 
			  
			  '<span class="col-label">Mobile</span>', 
			  
			  '&nbsp');	
			  
	          foreach( $records as $row )
	          {
	          	 if( $row->u_is_name_change == 'y' )
				{
					$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
				}
				else 
				{
					$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
				}
				         	
				
	          	$this->table->add_row(
	          	
					'<span class="col-data">'.$i.'</span>',
					
					"<input type='radio' name='Typ' id='Typ' value='".$row->reg_regno."'>",
					
					'<span class="col-data">'.strtoupper( $name ).'</span>',// modified by preeti on 28th feb 14
					
					'<span class="col-data">'.$row->reg_regno.'</span>',
					
					'<span class="col-data">'.$row->reg_mobile.'</span>',
					
					"<a class='link' href='".base_url()."admin/doc_password/".$row->reg_id."'>Edit Password</a>" 				
					
				);
				
				$i++;
	        }
	
			echo form_open('admin/submit'); 
	// below line added by preeti on 4th mar 14
			
			echo  $this->pagination->create_links();	
			
			?>
			
			<!-- below div added by preeti on 4th mar 14 -->
			
			<div class="clear"></div>
	
			<?php
	
			echo $this->table->generate();
	 ?>
	  
	  <table>
	  	
	  	<tr>
	  		
	  		<td><input type="submit" name="sbm" id="pn" value="Print all Docs" /></td>
	 	
	 		<td><input type="submit" name="sbm" id="dz"  value="Download Zip" /></td>
	 		
	 		<td><input type="submit" name="sbm" id="pp" value="Print Preview" /></td> 
	 		
	 		<td><input type="submit" name="sbm" id="gp" value="Generate Pdf" /></td>
	 		
	 		<td><!--<input type="submit" name="sbm" id="del" value="Delete" />--></td>
	 		
	 	</tr>
	 	
	 </table> 				
	
				<?php
	
				echo  $this->pagination->create_links();			
				
				echo form_close();
		}
				  
		?>         

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#start_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#end_date').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>