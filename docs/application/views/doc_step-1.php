<?php $this->load->view('template/header'); ?>
<?php //$this->load->view('template/left_user_out'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup1.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup3.js"></script>


<style>
.basic-grey{margin:0 0 -15px;}
#fullright{width:100%;margin-bottom:0;padding-bottom:0;}
#header,#main{width:100%;max-width: 960px;}
th label.subsub-form,th label.sub-form{margin:10px 0;text-align:left;text-decoration:underline;}
.heading{margin-bottom: 10px;}
.form td a.link{}
.basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select
{
	width:100%;
	border-radius: 4px;
	max-width: 245px;
	margin-bottom: 5px;
}
tr{}
td{width: 33.3%;vertical-align: middle;}
img.trigger{display:inline-block;}
table{width:100%;}
.basic-grey .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 8px 20px;
    text-align: center;	
	cursor:pointer;
}
div.warnings{
	margin-bottom:20px;
	border:none !important;
	text-align:left;
}
div.warnings p{
	display: block;
    font-size: 11px;
    line-height: 14px;
    margin: 0;
}
#loading div{top:15% !important;}
.opaque{opacity:0.1 !important;}
label span.error{
	color: red !important;
    margin-left: 2px;
}
.basic-grey textarea{height:50px;border-radius: 4px;}
.font12{font-size:12px;}
</style>

<form id="upload_form" class="basic-grey" action="<?php echo base_url(); ?>doc/save"  method="post" enctype="multipart/form-data">

<section id="personal">
<div id="fullright">
	<div class="regsquaresmall">     	
      	<div class="heading">APPLICANT'S INFORMATION</div>
			
			<div class="error warnings" id="status">
				
				<?php 
				
				if( validation_errors() )
				{
					echo 'hero';
					//echo (validation_errors());
				}
				else 
				{
					if( is_array( $errmsg ) && COUNT( $errmsg ) > 0 )
					{
						foreach( $errmsg as $val )
						{
							echo $val;
						}						
					}
					else 
					{
						echo ( $errmsg );	
					}					
				}
				
				?>
				
			</div>
      	
		
		<div class="form">
				
		<table>
			
			<tr>
				<td><label>Registration Number</label></td>
				<td><strong><?php echo $record->u_regno; ?></strong></td>
				<td><span class="u_regno validate">&nbsp;</span></td>
			</tr>
			
			<!--<tr>
				<td colspan="3">&nbsp;</td>
			</tr>-->
			
			<tr>
				<td>
					<label>Full Name<span class="error">*</span></label>					
				</td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="u_fname" id="u_fname" value="<?php echo $record->u_fname; ?>" type="text" />					
					<span class="small">Name exactly as in 10th/Matric Certificate.</span>					
					<!--<label>Middle Name</label>-->
					
				</td>
				<td>
					<input name="u_mname" value="" type="hidden" />
					<input name="u_lname" value="" type="hidden" />
					<!--<label>Last Name</label>-->					
				</td>
			</tr>
					
			<!--
			
			<tr>
				<td><label>First Name<span class="error">*</span></label></td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="u_fname" id="u_fname" value="<?php echo $record->u_fname; ?>" type="text" />					
					<span class="small">Same Name as in 10th/Matric Certificate.</span>
				</td>
				<td><span class="u_fname validate">&nbsp;</span></td>
			</tr>
		
			<tr>
				<td><label>Middle Name</label></td>
				<td><input <?php echo 'autocomplete="off"'; ?> name="u_mname" id="u_mname" value="<?php echo $record->u_mname; ?>" type="text" />
				</td>
				<td><span class="u_mname validate">&nbsp;</span></td>
			</tr>
			
			<tr>
				<td><label>Last Name</label></td>
				<td><input <?php echo 'autocomplete="off"'; ?> name="u_lname" id="u_lname" value="<?php echo $record->u_lname; ?>" type="text" />
				</td>
				<td><span class="u_lname validate">&nbsp;</span></td>
			</tr> -->
				
			<tr>
				<td><label>Have you changed your Name after Matriculation ?<span class="error">*</span></label></td>
				<td>
					Yes<input type="radio" name="u_is_name_change" value="y" <?php if( $record->u_is_name_change == 'y' ){ echo "checked";  } ?> />&nbsp;
	      				
	      			No<input type="radio" name="u_is_name_change" value="n" <?php if( $record->u_is_name_change == 'n' ){ echo "checked";  } ?> />
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			
			<div id="name_div">
			<table>
			<tr class="name_div">
				<td>
					<label>Changed Name ( Full )<span class="error">*</span></label>					
				</td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="new_fname" value="<?php echo $record->new_fname; ?>"  id="new_fname" type="text" />
				</td>	
				<td>
					<input name="new_mname" value="" type="hidden" />
					<input name="new_lname" value="" type="hidden" />
				</td>
				<!--<td>
					<label>New Middle Name</label>
					<input <?php echo 'autocomplete="off"'; ?>  name="new_mname" value="<?php echo $record->new_mname; ?>" id="new_mname" type="text" />
				</td>
				<td>
					<label>New Last Name</label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="new_lname" id="new_lname" value="<?php echo $record->new_lname; ?>" />
				</td>-->
			</tr>
						
			<tr class="name_div">
			<td>
				<label>New Name Certificate No</label>
				<input name="name_cft_no" id="name_cft_no" type="text" value="<?php echo $record->name_cft_no;?>" />
			</td>
			<td>
				<label>Issuing Office</label>
				<input name="name_cft_office" id="name_cft_office" value="<?php echo $record->name_cft_office;?>" type="text" />
			</td>
			<td>
				<label>Date</label>
				<input name="name_cft_date" id="name_cft_date" readonly="readonly" value="<?php if( $record->name_cft_date != '0000-00-00' ){ echo db_to_calen( $record->name_cft_date ) ; } ?>" type="text" />
			</td>
			</tr>
			</table>
			</div>
			
			<table>
			<!--<tr>
				<td><label>Name (Hindi)<span class="error">*</span></label></td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> type="text"  value="<?php echo $record->u_hindi_name; ?>" name="u_hindi_name" id="u_hindi_name" /></br>
					<a class="link" target="_blank" href="http://www.google.com/inputtools/try/" >Hindi Typing</a>
				</td>
				<td>&nbsp;</td>
			</tr>-->
			
			<tr>
				<td><label>Gender<span class="error">*</span></label></td>
				<td>
					<select name="u_sex" id="u_sex">
		      			
						<option value="">Select</option>
						
						<option <?php if($record->u_sex =='m'){ echo "selected"; } ?> value="m">Male</option>
						
						<option <?php if($record->u_sex =='f'){ echo "selected"; } ?> value="f">Female</option>
		      				
		      		</select>					
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<?php
      		
      		if( $record->u_photo != '' )
			{
				$photo_url = base_url().'uploads/photo/'.$record->u_photo;     		
      		?>
			
			<tr>
				<td><label>Uploaded Photo</label></td>
				<td><img src="<?php echo $photo_url; ?>" height="60" width="60" /></td>
				<td>&nbsp;</td>
			</tr>
						
			<?php } ?>
			
			<tr>
				<td><label>Photo <span class="error">*</span></label></td>
				<td colspan="2">
					<input type="file" name="u_photo" id="u_photo" />
					<span class="small">color only, Type:gif,jpg,png,jpeg Size:100KB, Width:1 inch(48-144px), Height: 1 inch(48-144px)</span>
				</td>
			</tr>
			
			<tr>
				<td><label>Date Of Birth<span class="error">*</span></label></td>
				<td>
					<input name="u_dob" id="u_dob" readonly="readonly" value="<?php if( $record->u_dob != '0000-00-00' ){ echo db_to_calen( $record->u_dob ); }?>" type="text" />      				
      				<span  style="display: none;">
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>	
				</td>
				<td>&nbsp;</td>
			</tr>
					
			
			<?php
      		
      		if( $record->u_dob_proof != '' )
			{
				$dob_url = base_url().'uploads/dob/'.$record->u_dob_proof;
      		?>

			<tr>
				<td><label>Uploaded DOB Certificate<span class="error">*</span></label></td>
				<td>
					<a title="click to View" target="_blank"  href="<?php echo $dob_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>	
				</td>
				<td>&nbsp;</td>
			</tr>
						
			<?php } ?>
			
			<tr>
				<td><label>Date Of Birth Certificate<span class="error">*</span></label></td>
				<td>
					<input name="u_dob_proof" readonly="readonly" id="u_dob_proof" type="file" />
					<span class="small"> Type - pdf, Max Size - 200KB </span>
				</td>
				<td>&nbsp;</td>
			</tr>
						
			<tr>
				<td><label>Nationality<span class="error">*</span></label></td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="u_nationality" value="<?php echo $record->u_nationality; ?>" id="u_nationality" type="text" />
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>
					<label>Father's Full Name<span class="error">*</span></label>					
				</td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_fname; ?>"  name="u_father_fname" id="u_father_fname" />
					<span class="small">Full name without the salutation like Mr. or Sh. etc</span>
				</td>
				<td>
					<input type="hidden" value="" name="u_father_mname" />
					<input type="hidden" value="" name="u_father_lname" />
				</td>
				<!--<td>
					<label>Father's Middle Name</label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_mname; ?>" name="u_father_mname" id="u_father_mname" />
				</td>
				<td>
					<label>Father's Last Name</label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_father_lname; ?>" name="u_father_lname" id="u_father_lname" />
				</td>-->
			</tr>
			
			<tr>
				<th colspan="3"><label class="subsub-form">SPOUSE INFORMATION</label></th>
			</tr>
			
			
			<tr>
				<td><label>Are you Married ?<span class="error">*</span></label></td>
				<td>
     				Yes<input type="radio" name="u_is_married" value="y" <?php if( $record->u_is_married == 'y' ){ echo "checked"; }  ?> />&nbsp;  				
     				No<input type="radio" name="u_is_married" value="n" <?php if( $record->u_is_married == 'n' ){ echo "checked"; }  ?> />    				
				</td>	

				<td>&nbsp;</td>
			</tr>
			</table>
			
			<div id="mar_div">
			<table>
			<tr class="mar_div">		
				<td><label>Date Of Marriage<span class="error">*</span></label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="sp_mar_date" readonly="readonly" id="sp_mar_date" value="<?php if( $record->sp_mar_date != '0000-00-00' ){ echo db_to_calen( $record->sp_mar_date ) ; } ?>" type="text" />
					<span  style="display: none;">
						<img id="calImg2" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>	
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr class="mar_div">				
				<td>
					<label>Spouse Name<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> name="sp_name"  value="<?php echo $record->sp_name; ?>" id="sp_name" type="text" />
				</td>				
				<td>
					<label>Spouse Occupation<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> name="sp_occup" value="<?php echo $record->sp_occup; ?>" id="sp_occup" type="text" />
				</td>
				<td>
					<label>Spouse Nationality<span class="error">*</span></label>
					<select name="sp_nation" id="sp_nation">
						<option value="">Select</option>
						<option <?php if( $record->sp_nation == 'i' ){ echo "selected"; }; ?> value="i">Indian</option>
						<option <?php if( $record->sp_nation == 'f' ){ echo "selected"; }; ?> value="f">Foreign</option>
					</select> 
				</td>
			</tr>
					
			<tr id="f_citi" class="mar_div">			
				<td><label>Date of acquiring Indian Citizenship </label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="sp_citizenship_date" readonly="readonly" id="sp_citizenship_date" value="<?php if( $record->sp_citizenship_date != '0000-00-00' ){ echo db_to_calen( $record->sp_citizenship_date ) ;} ?>" type="text" />
					<span  style="display: none;">
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>	
					<span class="small"> if foreigner</span>				
				</td>
				<td>&nbsp;</td>
			</tr>
			
			
			<tr class="mar_div">			
				<td><label>Whether Spouse applied for grant of SSC in AMC/Serving In AMC?<span class="error">*</span></label></td>		
				<td>			
					Yes<input type="radio" name="sp_ssc_applied" value="y" <?php if( $record->sp_ssc_applied == 'y' ){ echo "checked"; } ?> />&nbsp;  				
					No<input type="radio" name="sp_ssc_applied" value="n" <?php if( $record->sp_ssc_applied == 'n' ){ echo "checked"; } ?> />				
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			</div>
				
	</div>
    
</div>
</div>
</section>

<section id="professional">

<div id="fullright">
	<div class="regsquaresmall">
    <div class="heading">EDUCATIONAL DETAILS</div>
		
      	<div class="form">			
		<table>
			<tr>
				<th colspan="3"><label class="subsub-form">GRADUATION DETAILS</label></th>			
			</tr>
			
			<!-- edited by sanket -->
			 <!-- <tr>
				<td><label>Have you completed your MBBS Degree<span class="error">*</span></label></td>
				<td>
					Yes<input type="radio" id="yes_mbbs_cmp" name="u_complete_mbbs"  value="y" <?php if( $record->u_complete_mbbs == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" id="no_mbbs_cmp" name="u_complete_mbbs"  value="n" <?php if( $record->u_complete_mbbs == 'n' ){ echo "checked"; } ?>  />
				</td>
			</tr>  --> 
			<!-- edited by sanket -->
			 
			<!-- <tr>	
						
				<td id="likelyhead"><label>Likely date Of passing of final MBBS Part II<span class="error">*</span></label></td>		
				<td id="testo">
					<input name="u_likely_mbbs" readonly="readonly" value="<?php if( $record->u_likely_mbbs != '0000-00-00' ){ echo db_to_calen( $record->u_likely_mbbs ) ; } ?>"  id="u_likely_mbbs" type="text" />
					<span  style="display: none;">
						<img id="calImglk" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>
					
				<td>&nbsp;</td>
				
			</tr> -->
			 			
			<!-- edited by sanket end here -->
			
			<tr id="">			
				<td><label>Graduated from<span class="error">*</span></label></td>			
				<td>
					Foreign<input type="radio" id="foreign_grad" name="u_grad_from" <?php if( $record->u_grad_from == 'f' ){ echo "checked"; } ?> value="f" />&nbsp;      				
      				India<input type="radio" id="indian_grad" name="u_grad_from" <?php if( $record->u_grad_from == 'i' ){ echo "checked"; } ?> value="i"  />
				</td>	
				<td>&nbsp;</td>
			</tr>

			

			<tr id="basic_qual">			
				<td><label>Basic Qualification<span class="error">*</span></label></td>			
				<td>
					<input <?php if($record->u_basic_qual) echo 'disabled="disabled"'; ?> <?php echo 'autocomplete="off"'; ?> value="<?php echo $record->u_basic_qual; ?>" id="u_basic_qual" type="text" />
					<input type="hidden" id="u_basic_qual_hidden" name="u_basic_qual" value="<?php echo $record->u_basic_qual; ?>" />
					<span class="small">Minimum MBBS/Equivalent is Mandatory</span>    				      				
				</td>
				<td>&nbsp;</td>
			</tr>	
					
			
			<tr>			
				<td><label>Date Of Admission<span class="error">*</span></label></td>		
				<td>
					<input name="u_adm_date" readonly="readonly" value="<?php if( $record->u_adm_date != '0000-00-00' ){ echo db_to_calen( $record->u_adm_date ) ; } ?>"  id="u_adm_date" type="text" />
					<span  style="display: none;">
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>
				</td>
				<td>&nbsp;</td>
			</tr>			
			
			<tr id="date_finalmbbs">		
				<td><label>Date Of Passing Final MBBS<span class="error">*</span></label></td>		
				<td>
					<input type="text" readonly="readonly" value="<?php if( $record->u_passing_date != '0000-00-00' ){ echo db_to_calen( $record->u_passing_date ) ; } ?>" name="u_passing_date" id="u_passing_date" />
					<span  style="display: none;">
						<img id="calImg2" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>	
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>			
				<td>
					<label>No. Of Attempts for passing Final MBBS Exam (Part I)<span class="error">*</span></label>
				</td>			
				<td>
					<select name="u_num_attempt" id="u_num_attempt">
						<option value="">Select</option>
						<?php for( $i = 1; $i <=2 ; $i++ ) { ?>		
						<option <?php if( $record->u_num_attempt == $i ){ echo "selected";  } ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>      					
						</select>   				
						<span class="small">Max Allowed Attempt: 2</span>
				</td>
				<td>&nbsp;</td><td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>
					<label>Attempt Certificate No(Part I)<span class="error">*</span></label>
					<input name="u_attempt_cert_no" id="u_attempt_cert_no" type="text" value="<?php echo $record->u_attempt_cert1_no;?>" />
				</td>
				<td>
					<label>Issuing Office<span class="error">*</span></label>
					<input name="u_attempt_cert_office" id="u_attempt_cert_office" value="<?php echo $record->u_attempt_cert1_office;?>" type="text" />
				</td>
				<td>
					<label>Date<span class="error">*</span></label>
					<input name="u_attempt_cert_date" id="u_attempt_cert_date" readonly="readonly" value="<?php if( $record->u_attempt_cert1_date != '0000-00-00' ){ echo db_to_calen( $record->u_attempt_cert1_date ) ; } ?>" type="text" />
				</td>
			</tr>
			<div id="">
				<tr id="">			
					<td>
						<label>No. Of Attempts for passing Final MBBS Exam (Part II)<span class="error">*</span></label>
					</td>			
					<td>
						<select name="u_num_attempt1" id="u_num_attempt1">      					
	      					<option value="">Select</option>    					
	      					<?php for( $i = 1; $i <=2 ; $i++ ) { ?>		
							<option <?php if( $record->u_num_attempt1 == $i ){ echo "selected";  } ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>								
							<?php } ?>     					
	      				</select>
	      				<span class="small">Max Allowed Attempt: 2</span>
					</td>
					<td>&nbsp;</td>
				</tr>
				
	      		<tr id="">
					<td>
						<label>Attempt Certificate No(Part II)<span class="error">*</span></label>
						<input name="u_attempt_cert2_no" id="u_attempt_cert2_no" type="text" value="<?php echo $record->u_attempt_cert2_no;?>" />
					</td>
					<td>
						<label>Issuing Office<span class="error">*</span></label>
						<input name="u_attempt_cert2_office" id="u_attempt_cert2_office" value="<?php echo $record->u_attempt_cert2_office;?>" type="text" />
					</td>
					<td>
						<label>Date<span class="error">*</span></label>
						<input name="u_attempt_cert2_date" id="u_attempt_cert2_date" readonly="readonly" value="<?php if( $record->u_attempt_cert2_date != '0000-00-00' ){ echo db_to_calen( $record->u_attempt_cert2_date ) ; } ?>" type="text" />
					</td>
				</tr>
			</div>
	
			<tr>
				<td><label>Medical College<span class="error">*</span></label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_college; ?>" name="u_college" id="u_college" />	
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>				
				<td><label>State<span class="error">*</span></label></td>				
				<td>	
					<select name="medical_state" id="medical_state">	      			
	      				<option value="">Select</option>	      				
	      				<?php
						
						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>	      				
	      					<option <?php if( $record->medical_state == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>       					      				      				
	      			</select>												      				
				</td>
				<td>&nbsp;</td>
			</tr> 
			
			<tr>
				<td><label>University<span class="error">*</span></label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_univ; ?>" name="u_univ" id="u_univ" />	
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<!--   -->
			
			<tr>
				<td colspan="2">
					<label>Whether Institution is Owned by Govt?<span class="error">*</span></label>
				</td>
				<td>
      				Yes<input type="radio" name="u_college_gov" value="y" <?php if( $record->u_college_gov == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="u_college_gov" value="n" <?php if( $record->u_college_gov == 'n' ){ echo "checked"; } ?> />
				</td> 
			</tr>
						
			<!--   -->
			
			<tr>
				<td><label>College Recognized by MCI<span class="error">*</span></label></td>
				<td>
					Yes<input type="radio" name="u_college_recog" value="y" <?php if( $record->u_college_recog == 'y' ){ echo "checked"; } ?> />&nbsp;
					No<input type="radio" name="u_college_recog" value="n" <?php if( $record->u_college_recog == 'n' ){ echo "checked"; } ?> />    
				</td> 
				<td>&nbsp;</td>
      		</tr> 

			<tr id="mbbs_certi_no">
				<td>
					<label>MBBS Certificate No<span class="error">*</span></label>
					<input name="mbbs_ctf_no" id="mbbs_ctf_no" type="text" value="<?php echo $record->mbbs_ctf_no;?>" />
				</td>
				<td>
					<label>Issuing Office<span class="error">*</span></label>
					<input name="mbbs_ctf_office" id="mbbs_ctf_office" value="<?php echo $record->mbbs_ctf_office;?>" type="text" />
				</td>
				<td>
					<label>Date<span class="error">*</span></label>
					<input name="mbbs_ctf_date" id="mbbs_ctf_date" readonly="readonly" value="<?php if( $record->mbbs_ctf_date != '0000-00-00' ){ echo db_to_calen( $record->mbbs_ctf_date ) ; } ?>" type="text" />
				</td>
			</tr>
			
			<tr>
				<td><label>Have you completed your Internship?<span class="error">*</span></label></td>
				<td>
      				Yes<input type="radio" name="u_intern_complete" value="y" <?php if( $record->u_intern_complete == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="u_intern_complete" value="n" <?php if( $record->u_intern_complete == 'n' ){ echo "checked"; } ?> />     				      				
				</td> 
				<td>&nbsp;</td>
			</tr>
			</table>
			
			<div id="intern_div">
			<table>
			<tr class="intern_div">	
				<td><label>Internship Completion Date<span class="error">*</span></label></td>				
				<td>
					<input type="text" readonly="readonly" value="<?php if( $record->u_intern_date != '0000-00-00' ){ echo db_to_calen( $record->u_intern_date ) ; } ?>" name="u_intern_date" id="u_intern_date" />
	      				
	      			<span  style="display: none;">
						<img id="calImg3" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">									
					</span>		
				</td>	
				<td>&nbsp;</td>
			</tr>
		
			<tr class="intern_div">
				<td>
					<label>Internship Certificate No</label>
					<input name="intern_ctf_no" id="intern_ctf_no" type="text" value="<?php echo $record->intern_ctf_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="intern_ctf_office" id="intern_ctf_office" value="<?php echo $record->intern_ctf_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="intern_ctf_date" id="intern_ctf_date" readonly="readonly" value="<?php if( $record->intern_ctf_date != '0000-00-00' ){ echo db_to_calen( $record->intern_ctf_date ) ; } ?>" type="text" />
				</td>
			</tr>
			</table>
			</div>
			
			<table>
			<tr id="intern_like_div">		
				<td><label>Internship Completion Date (Likely)<span class="error">*</span></label></td>
				<td>
					<input type="text" readonly="readonly" value="<?php if( $record->u_intern_date_likely != '0000-00-00' ){ echo db_to_calen( $record->u_intern_date_likely ) ; } ?>" name="u_intern_date_likely" size="15" id="u_intern_date_likely" />
	      			<span  style="display: none;">
						<img id="calImg5" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">
					</span>	
				</td>
				<td>&nbsp;</td>				
			</tr> 
						
			<!-- start, lines moved from above to here by preeti on 12th mar 14 -->
			
			<!-- We have to start from here  -->

			<tr id="">
				<th colspan="3"><label class="subsub-form">PMRC DETAILS</label></th>	
			</tr>

			<tr id="">
				<td>
					<label>Permanent/Provisional Medical Registration Number<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> name="u_pmrc_num" value="<?php if( $record->u_pmrc_num != '0' ){ echo $record->u_pmrc_num; } ?>" id="u_pmrc_num" type="text" />
				</td>		
				<td style="vertical-align:bottom;">
					<label>Issuing Office<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="u_pmrc_issue_off" value="<?php if( $record->u_pmrc_issue_off != '0' ){ echo $record->u_pmrc_issue_off; } ?>" id="u_pmrc_issue_off" />
				</td>
				<td style="vertical-align:bottom;">
					<label>Date Of Registration<span class="error">*</span></label>
					<input type="text" readonly="readonly" name="u_pmrc_reg_date" value="<?php if( $record->u_pmrc_reg_date != '0000-00-00' ){ echo db_to_calen( $record->u_pmrc_reg_date ) ; } ?>" id="u_pmrc_reg_date" />
					<span  style="display: none;">		
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">								
					</span>
				</td>
			</tr>

			<tr id="">			
				<th colspan="3"><label class="subsub-form">POST GRADUATION DETAILS</label></th>
			</tr>
			
			<tr id="">
				<td><label>Are you a Post Graduate ?<span class="error">*</span></label></td>			
				<td>
					Yes<input type="radio" name="u_is_pg" value="y" <?php if( $record->u_is_pg == 'y' ){ echo "checked"; } ?> />&nbsp;
					No<input type="radio" name="u_is_pg" value="n" <?php if( $record->u_is_pg == 'n' ){ echo "checked"; } ?> />     				      				
				</td>
				<td>&nbsp;</td>
			</tr>
			</table>
			
			<div id="pg_div">
			<table>
			<tr class="pg_div">
				<th colspan="3"><label class="subsub-form">FIRST</label></th>
			</tr>
			
			<tr class="pg_div">
				<td><label>PG Qualification<span class="error">*</span></label></td>		
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_degree" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ) {echo $record_pg[0]->pg_degree;} ?>" id="pg_degree" type="text" />      				
				</td> 
				<td>&nbsp;</td>
			</tr>

			<tr class="pg_div">
				<td><label>Subject<span class="error">*</span></label></td>		
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_subject" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ) {echo $record_pg[0]->pg_subject;} ?>" id="pg_subject" type="text" />      				
				</td> 
				<td>&nbsp;</td>
			</tr>
			
			<tr class="pg_div">
				<td><label>College<span class="error">*</span></label></td>		
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_college" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ) {echo $record_pg[0]->pg_college;} ?>" id="pg_college" type="text" />      				
				</td> 
				<td>&nbsp;</td>
			</tr>
			
			<tr class="pg_div">
				<td>
					<label>State<span class="error">*</span></label>
					<select name="pg_medical_state1" id="pg_medical_state1">	      			
	      				<option value="">Select</option>	      				
	      				<?php
						
						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>
	      				
	      					<option <?php if( $record->pg_medical_state1 == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>      					      				      				
	      			</select>
				</td>
				<td>
					<label>University<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ) {echo $record_pg[0]->pg_univ;} ?>" id="pg_univ" />
				</td> 
				<td>
					<label>Year Of Passing<span class="error">*</span></label>
					<select name="pg_year" id="pg_year">  					
						<option value="">Select</option>
						<?php for( $i = 2018; $i >=1950 ; $i-- ) { ?>		
									
						<option <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
									
						<?php } ?>      					
					</select> 
				</td>
			</tr>
			
			<tr class="pg_div">		
				<td><label>College Recognized by MCI<span class="error">*</span></label></td>				
				<td>
					Yes<input type="radio" name="pg_mci_recog" value="y" <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
	      			No<input type="radio" name="pg_mci_recog" value="n" <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> />
	      				      				
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr class="pg_div">	
				<td>
					<label>First PG Certificate No</label>
					<input name="firstpg_ctf_no" id="firstpg_ctf_no" type="text" value="<?php echo $record->firstpg_ctf_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="firstpg_ctf_office" id="firstpg_ctf_office" value="<?php echo $record->firstpg_ctf_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="firstpg_ctf_date" id="firstpg_ctf_date" readonly="readonly" value="<?php if( $record->firstpg_ctf_date != '0000-00-00' ){ echo db_to_calen( $record->firstpg_ctf_date ) ; } ?>" type="text" />
				</td>
			</tr>
			
     		<tr id="" class="pg_div">    		
				<td><label>If you have any other MCI recognized PG Degree / Diploma</label></td>	
				<td>
					Yes<input <?php if($cnt_pg>1){ echo "checked"; } ?> type="radio" value="y" name="add_pg1" />
							
					No<input type="radio" value="n" name="add_pg1" />
					<span class="small"> You can add max Two more PG Qualifications </span>				
				</td> 
				<td>&nbsp;</td>
     		</tr>
     		</table>
			
			<div id="add_pg1_div">
     		<table> <!-- first innner add_pg1_div starts here -->
			<tr>
				<th colspan="3"><label class="subsub-form">SECOND</label></th>
			</tr>
			
			<tr>
				<td><label>PG Qualification<span class="error">*</span></label></td>					
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_degree1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ) {echo $record_pg[1]->pg_degree;} ?>" id="pg_degree1" type="text" />      				
				</td>
				<td>&nbsp;</td>
			</tr>

			<tr>		
				<td><label>Subject<span class="error">*</span></label></td>					
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_subject1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ) {echo $record_pg[1]->pg_subject;} ?>" id="pg_subject1" type="text" />      				
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>		
				<td><label>College<span class="error">*</span></label></td>					
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_college1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ) {echo $record_pg[1]->pg_college;} ?>" id="pg_college1" type="text" />     				
				</td>
				<td>&nbsp;</td>
			</tr>

			<tr>	
				<td>
					<label>State<span class="error">*</span></label>
					<select name="pg_medical_state2" id="pg_medical_state2">	      			
	      				<option value="">Select</option>	      				
	      				<?php
						
						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>
	      				
	      					<option <?php if( $record->pg_medical_state2 == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>      					      				      				
	      			</select>
				</td>					
				<td>
					<label>University<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ) {echo $record_pg[1]->pg_univ;} ?>" id="pg_univ1" />
				</td>
				<td>
					<label>Year Of Passing<span class="error">*</span></label>
					<select name="pg_year1" id="pg_year1">
						<option value="">Select</option>
						<?php for( $i = 2016; $i >=1950 ; $i-- ) { ?>		
							
							<option <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>	
							
						<?php } ?>		      					
					</select>
				</td>
			</tr>
				
			<tr>
				<td><label>College Recognized by MCI<span class="error">*</span></label></td>
				<td>
					Yes<input type="radio" name="pg_mci_recog1" value="y" <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
		      		No<input type="radio" name="pg_mci_recog1" value="n" <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> />
		      	</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>
					<label>Second PG Certificate No</label>
					<input name="secondpg_ctf_no" id="secondpg_ctf_no" type="text" value="<?php echo $record->secondpg_ctf_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="secondpg_ctf_office" id="secondpg_ctf_office" value="<?php echo $record->secondpg_ctf_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="secondpg_ctf_date" id="secondpg_ctf_date" readonly="readonly" value="<?php if( $record->secondpg_ctf_date != '0000-00-00' ){ echo db_to_calen( $record->secondpg_ctf_date ) ; } ?>" type="text" />
				</td>
			</tr>
		
	     	<tr id="add_pg2_addmore">
				<td><label>If you have any other MCI recognized PG Degree / Diploma</label></td>
				<td>
					Yes<input <?php if(  $cnt_pg >2 ){ echo "checked"; } ?> type="radio" value="y" name="add_pg2" />
					No<input type="radio" value="n" name="add_pg2" />						
				</td> 
				<td>&nbsp;</td>
	     	</tr>   			
     		
     		</table> <!-- inner first add_pg1_div ends here -->
     		</div>
			
			<div id="add_pg2_div">
     		<table> <!-- second innner add_pg2_div starts here -->

			<tr>
				<th colspan="3"><label class="subsub-form">THIRD</label></th>	
			</tr>	
			
			<tr>	
				<td><label>PG Qualification<span class="error">*</span></label></td>
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_degree2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ) {echo $record_pg[2]->pg_degree;} ?>" id="pg_degree2" type="text" />      				
				</td> 
				<td>&nbsp;</td>
			</tr>
	
			<tr>	
				<td><label>Subject<span class="error">*</span></label></td>				
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_subject2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ) {echo $record_pg[2]->pg_subject;} ?>" id="pg_subject2" type="text" />      				
				</td> 
				<td>&nbsp;</td>
			</tr>
			
			<tr>	
				<td><label>College<span class="error">*</span></label></td>				
				<td>
					<input <?php echo 'autocomplete="off"'; ?> name="pg_college2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ) {echo $record_pg[2]->pg_college;} ?>" id="pg_college2" type="text" />     				
				</td> 
				<td>&nbsp;</td>
			</tr>
	
			<tr>
				<td>
					<label>State<span class="error">*</span></label>
					<select name="pg_medical_state3" id="pg_medical_state3">	      			
	      				<option value="">Select</option>	      				
	      				<?php
						
						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>
	      				
	      					<option <?php if( $record->pg_medical_state3 == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>      					      				      				
	      			</select>
				</td>			
				<td>
					<label>University<span class="error">*</span></label>
					<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ) {echo $record_pg[2]->pg_univ;} ?>" id="pg_univ2" />
				</td> 
				<td>
					<label>Year Of Passing<span class="error">*</span></label>
					<select name="pg_year2" id="pg_year2">
						<option value="">Select</option>
						<?php for( $i = 2016; $i >=1950 ; $i-- ) { ?>											
							<option <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>		      					
					</select> 
				</td>
			</tr>
				
			<tr>			
				<td><label>College Recognized by MCI<span class="error">*</span></label></td>				
				<td>
					Yes<input type="radio" name="pg_mci_recog2" value="y" <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
					No<input type="radio" name="pg_mci_recog2" value="n" <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> /> 		      				      				
				</td>
				<td>&nbsp;</td>
			</tr>
				
			<tr>
				<td>
					<label>Third PG Certificate No</label>
					<input name="thirdpg_ctf_no" id="thirdpg_ctf_no" type="text" value="<?php echo $record->thirdpg_ctf_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="thirdpg_ctf_office" id="thirdpg_ctf_office" value="<?php echo $record->thirdpg_ctf_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="thirdpg_ctf_date" id="thirdpg_ctf_date" readonly="readonly" value="<?php if( $record->thirdpg_ctf_date != '0000-00-00' ){ echo db_to_calen( $record->thirdpg_ctf_date ) ; } ?>" type="text" />
				</td>
			</tr>
			
     		</table> <!-- inner second add_pg2_div ends here -->
     		</div>
     		
		</div><!-- pg_div ends-->
     		
		<!-- form div close here -->		
      	
		 
      </div>     

</div>
</div>

</section>

<section id="employment">

	<div id="fullright">	
	<div class="regsquaresmall">
      	
      	<div class="heading">EMPLOYMENT DETAILS</div>     	
      	 
		<table>
		
			<tr id="are_emp">			
				<td><label>Are you Employed ?<span class="error">*</span></label></td>
				<td>
      				Yes<input type="radio" name="u_current_emp" value="y" <?php if($record->u_current_emp == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="u_current_emp" value="n" <?php if($record->u_current_emp == 'n' ){ echo "checked"; } ?> />
				</td>
				<td>&nbsp;</td>
			</tr>
		
		</table>
		
		<div id="emp_div">
      	<table>
		
			<tr>			
				<td><label>Current Employer<span class="error">*</span></label></td>		
				<td>
					<input <?php echo 'autocomplete="off"'; ?> style="width:150px;" type="text" value="<?php echo $record->ce_employer; ?>"  name="ce_employer" id="ce_employer" />  				
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>			
				<td colspan="2"><label>Is your Employment is under any BOND with State Govt/Central Govt/Govt of Union Territory<span class="error">*</span></label></td>		
				<td>
					Yes<input type="radio" name="ce_bond" value="y" <?php if($record->ce_bond == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="ce_bond" value="n" <?php if($record->ce_bond == 'n' ){ echo "checked"; } ?> />				
				</td>
			</tr>

			<tr>
				<td>
					<label>No Objection Certificate No</label>
					<input name="noc_cft_no" id="noc_cft_no" type="text" value="<?php echo $record->noc_cft_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="noc_cft_office" id="noc_cft_office" value="<?php echo $record->noc_cft_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="noc_cft_date" id="noc_cft_date" readonly="readonly" value="<?php if( $record->noc_cft_date != '0000-00-00' ){ echo db_to_calen( $record->noc_cft_date ) ; } ?>" type="text" />
				</td>
			</tr>
			
	  	</table> <!-- emp_div ends here -->
		</div>
		
	  	<table>
	  		
			<tr>
				<th colspan="3"><label class="sub-form">OTHER DETAILS</label></th>
			</tr>
      	
    		<tr>
				<td><label>NCC Training Certificate </label></td>			
				<td>	
					<select name="u_ncc_cert" id="u_ncc_cert">    					
      				<option value="">None</option>
      				<option <?php if( $record->u_ncc_cert == 'a' ){ echo "selected"; } ?> value="a">A</option>
      				<option <?php if( $record->u_ncc_cert == 'b' ){ echo "selected"; } ?> value="b">B</option> 
      				<option <?php if( $record->u_ncc_cert == 'c' ){ echo "selected"; } ?> value="c">C</option>     					
      				</select>	      				
				</td>
				<td>&nbsp;</td>
			</tr>	  		
    
    
    		<tr>				
				<td><label>Hobbies<span class="error">*</span></label></td>				
				<td>
					<textarea name="u_hobbies" id="u_hobbies" rows="3" cols="16"><?php echo $record->u_hobbies; ?></textarea>
					<span class="small">(Comma Separated)</span>	      				
				</td>
				<td>&nbsp;</td>
			</tr>		    
	  		 
    		<tr>			
				<td><label>First Preference of Service<span class="error">*</span></label></td>			
				<td>	
					<select name="u_pref1" id="u_pref1">
      					
      					<option value="">Select</option> 					
      					<option <?php if( $record->u_pref1 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
      					
      					<option <?php if( $record->u_pref1 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
      					
      					<option <?php if( $record->u_pref1 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>     					
      				</select>	      				
				</td>
				<td>&nbsp;</td>
			</tr>		    
      		
			<tr>	
				<td><label>Second Preference of Service<span class="error">*</span></label></td>				
				<td>	
					<select name="u_pref2" id="u_pref2">      					  					
      					<option value="">Select</option>
      					
      					<?php
      					if( $record->u_pref2 != '' )
						{
      					?>
						
      					<option <?php if( $record->u_pref2 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
	      				<option <?php if( $record->u_pref2 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
	      				<option <?php if( $record->u_pref2 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>
	      					
      					<?php } ?>    					
      				</select>	      				
				</td>
				<td>&nbsp;</td>
			</tr>		    
 	
      	  	<tr>				
				<td><label>Third Preference of Service<span class="error">*</span></label></td>				
				<td>
					<select name="u_pref3" id="u_pref3">      					
      				<option value="">Select</option>
					<?php if( $record->u_pref3 != '' ) { ?>
      					
					<option <?php if( $record->u_pref3 == 'arm' ){ echo "selected"; } ?> value="arm">Army</option>
					
					<option <?php if( $record->u_pref3 == 'nav' ){ echo "selected"; } ?> value="nav">Navy</option>
					
					<option <?php if( $record->u_pref3 == 'air' ){ echo "selected"; } ?> value="air">Air Force</option>
      					
      				<?php } ?>    					
      				</select>						      				
				</td>
				<td>&nbsp;</td>
			</tr>	
			<table>
			<tr id="prev_amc_service">
				<th colspan="3"><label class="sub-form">PREVIOUS AMC SERVICE</label></th>
			</tr>			
      	
      	    <tr id="prev_amc_service1">			
				<td><label>Have you worked in AMC service before?<span class="error">*</span></label></td>				
				<td>	
					Yes<input type="radio" name="u_previous_ssc" value="y" <?php if( $record->u_previous_ssc == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="u_previous_ssc" value="n" <?php if( $record->u_previous_ssc == 'n' ){ echo "checked"; } ?> />											      				
				</td>
				<td>&nbsp;</td>
			</tr>	
			</table>
      	     
			<div id="ssc_div"> 
      	    <table>  

			<tr>			
				<td>
					<label>PERSNO(SSC NO)<span class="error">*</span></label>
					<input type="text" name="ps_persno" value="<?php echo $record->ps_persno; ?>" />
				</td>								
				<td>
					<label>Date Of Commission <span class="error">*</span></label>					
					<input readonly="readonly" value="<?php if( $record->ps_com_date != '0000-00-00' ){ echo db_to_calen( $record->ps_com_date ) ; } ?>" name="ps_com_date" id="ps_com_date" size="14" type="text" />     			
      				<span  style="display: none;">				
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">								
					</span>	      			      												
				</td>
				<td>
					<label>Date Of Release<span class="error">*</span></label>	
					<input readonly="readonly" name="ps_rel_date" value="<?php if( $record->ps_rel_date != '0000-00-00' ){ echo db_to_calen( $record->ps_rel_date ) ; } ?>" id="ps_rel_date" size="14" type="text" />     			
      				<span  style="display: none;">				
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" class="trigger">								
					</span>	 												      				
				</td>	
			</tr>	
  
			
			<tr>
				<td>
					<label>Release Certificate No</label>
					<input name="rel_cft_no" id="rel_cft_no" type="text" value="<?php echo $record->rel_cft_no;?>" />
				</td>
				<td>
					<label>Issuing Office</label>
					<input name="rel_cft_office" id="rel_cft_office" value="<?php echo $record->rel_cft_office;?>" type="text" />
				</td>
				<td>
					<label>Date</label>
					<input name="rel_cft_date" id="rel_cft_date" readonly="readonly" value="<?php if( $record->rel_cft_date != '0000-00-00' ){ echo db_to_calen( $record->rel_cft_date ) ; } ?>" type="text" />
				</td>
			</tr>
						    	
      	    </table>
			</div>
			 <!-- ssc_div ends here -->
      	     
			<table> 
      	    
			<tr id="">
				<th colspan="3"><label class="sub-form">PREVIOUS ATTENDANCE</label></th> 
      	    </tr> 
			
			<tr id="">			
				<td>
					<label>Have you attended AMC SSC interview previously?<span class="error">*</span></label>
				</td>				
				<td colspan="2">	
					Yes<input type="radio" name="u_previous_unfit" value="y" <?php if( $record->u_previous_unfit == 'y' ){ echo "checked"; } ?> />&nbsp;      				
      				No<input type="radio" name="u_previous_unfit" value="n" <?php if( $record->u_previous_unfit == 'n' ){ echo "checked"; } ?> />											      				
				</td>
			</tr>
	
			<tr id="u_previous_unfit_y">			
				<td>
					<label>Which Year(s)?<span class="error">*</span></label>
				</td>				
				<td colspan="2">	
					<input type="text" name="u_previous_unfit_year" value="<?php echo $record->u_previous_unfit_year; ?>" />
					<span class="small">( Comma seperated )</span>
				</td>
			</tr>
			
			<tr>
				<th colspan="3"><label class="sub-form">ADDRESS DETAILS</label></th> 
      	    </tr> 
      	     
      	    <tr>			
				<td><label>Mobile</label></td>				
				<td><?php echo $record->reg_mobile; ?></td>		
				<td>&nbsp;</td>
			</tr>   
    
    		<tr>			
				<td><label>Email</label></td>			
				<td><?php echo $record->reg_email; ?></td>
				<td>&nbsp;</td>
			</tr>   
      	
			<tr>
				<th colspan="3"><label class="subsub-form">POSTAL ADDRESS (FOR CORRESPONDENCE)</label></th> 
			</tr>
      	    
      	    <tr>			
				<td><label>Address<span class="error">*</span></label></td>				
				<td>	
					<textarea name="post_address" id="post_address" rows="3"  cols="16"><?php echo $record->post_address; ?></textarea>												      				
				</td>
				<td>&nbsp;</td>
			</tr>   	
      	      	
      	     
      	    <tr>				
				<td><label>State<span class="error">*</span></label></td>				
				<td>
					<select name="post_state" id="post_state">	      			
	      				<option value="">Select</option>	      				
	      				<?php

						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>
	      				
	      					<option <?php if( $record->post_state == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>      					      				      				
	      			</select>												      				
				</td>
				<td>&nbsp;</td>
			</tr>  	
      	      	      
      	    <tr>				
				<td><label>Pincode<span class="error">*</span></label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> maxlength="6" name="post_pin" value="<?php echo $record->post_pin; ?>" id="post_pin" type="text" />
												      				
				</td>
				<td>&nbsp;</td>
			</tr>  	  	
    
    		<tr>				
				<td><label>Alternate Contact No. (if any)</label></td>			
				<td>
					<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->post_tel; ?>" name="post_tel" id="post_tel" />												      				
				</td>
				<td>&nbsp;</td>
			</tr> 
			
			<tr>
      	    <th colspan="2"><label class="subsub-form">PERMANENT RESIDENTIAL ADDRESS</label></th>
      	    <th style="font-size:12px">	
				<input type="checkbox" value="y" name="perm_as_post" id="perm_as_post" <?php if( $record->u_perm_as_post == 'y' ){ echo "checked"; } ?> />  Same as Correspondence Address
      	    </th>
			</tr>
			 
    		<tr>			
				<td><label>Address<span class="error">*</span></label></td>				
				<td>
					<textarea name="perm_address" id="perm_address" rows="3" cols="16"><?php echo $record->perm_address; ?></textarea>												      				
				</td>
				<td>&nbsp;</td>
			</tr> 
			
    		<tr>				
				<td><label>State<span class="error">*</span></label></td>				
				<td>	
					<select name="perm_state" id="perm_state">	      			
	      				<option value="">Select</option>	      				
	      				<?php
						
						$this->db->order_by('state_id', 'ASC');
	      				$res = $this->db->get('state');
						
						foreach( $res->result() as $val )
						{						
	      				?>	      				
	      					<option <?php if( $record->perm_state == $val->state_id ){ echo "selected"; } ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
	      				
	      				<?php
						}
	      				?>       					      				      				
	      			</select>												      				
				</td>
				<td>&nbsp;</td>
			</tr> 
	
    		<tr>				
				<td><label>Pincode<span class="error">*</span></label></td>				
				<td>
					<input <?php echo 'autocomplete="off"'; ?>  maxlength="6" name="perm_pin" value="<?php echo $record->perm_pin; ?>" id="perm_pin" type="text" />
																	      				
				</td>
				<td>&nbsp;</td>
			</tr> 
	
    		<tr>			
				<td><label>Alternate Contact No. (if any )</label></td>				
				<td>
					<input <?php echo 'autocomplete="off"'; ?>  type="text" value="<?php echo $record->perm_tel; ?>" name="perm_tel" id="perm_tel" />
																	      				
				</td>
				<td>&nbsp;</td>				
			</tr> 
	
			<tr>			
				<th colspan="3"><label>Declaration</label>					
				<p class="font12">
					I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect &nbsp; I will be liable for disciplinary action or termination of service as deemed fit.					
				</p>	
				</th>
			</tr>
				
			<tr>
				<td colspan="3" style="text-align:center;"><input type="submit" id="save" class="button" name="save" value="Save" /></td>
			</tr>
			</table>
			
			<div class="hidden_values">
			<input type="hidden" name="u_photo_hid" value="<?php echo $record->u_photo; ?>" id="u_photo_hid" />	
			<input type="hidden" name="new_name_proof_hid" value="<?php echo $record->new_name_proof; ?>" id="new_name_proof_hid" />						
			<input type="hidden" name="u_dob_proof_hid" value="<?php echo $record->u_dob_proof; ?>" id="u_dob_proof_hid" />
			<input type="hidden" name="u_pmrc_copy_hid" value="<?php echo $record->u_pmrc_copy; ?>" id="u_pmrc_copy_hid" />
			<input type="hidden" name="u_attempt_cert_hid" value="<?php echo $record->u_attempt_cert; ?>" id="u_attempt_cert_hid" />
			<input type="hidden" name="u_attempt_cert1_hid" value="<?php echo $record->u_attempt_cert1; ?>" id="u_attempt_cert1_hid" />					
			<input type="hidden" name="u_intern_cert_hid" value="<?php echo $record->u_intern_cert; ?>" id="u_intern_cert_hid" />
      		<input type="hidden" name="u_mbbs_cert_hid" value="<?php echo $record->u_mbbs_cert; ?>" id="u_mbbs_cert_hid" />
      		<input type="hidden" name="pg_cert1_hid" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != ''  ){ echo $record_pg[0]->pg_cert; }?>" id="pg_cert1_hid" />
      		<input type="hidden" name="pg_cert2_hid" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != ''  ){ echo $record_pg[1]->pg_cert; }?>" id="pg_cert2_hid" />
      		<input type="hidden" name="pg_cert3_hid" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != ''  ){ echo $record_pg[2]->pg_cert; }?>" id="pg_cert3_hid" />
      		<input type="hidden" name="u_dd_copy_hid" value="<?php echo $record->u_dd_copy; ?>" id="u_dd_copy_hid" />
      		<input type="hidden" name="pg_count" id="pg_count" value="<?php echo $cnt_pg; ?>" />
			<input type="hidden" name="reg_id" id="reg_id" value="<?php echo $reg_id; ?>" />
      		<input type="hidden" name="ce_emp_noc_hid" id="ce_emp_noc_hid" value="<?php echo $record->ce_emp_noc; ?>" />
      		<input type="hidden" name="ps_rel_order_hid" value="<?php echo $record->ps_rel_order; ?>" id="ps_rel_order_hid" />
      		<input type="hidden" name="u_sign_hid" value="<?php echo $record->u_sign; ?>" id="u_sign_hid" />
      		<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
			<input type="hidden" name="confirm_random" id="confirm_random" value="<?php echo $confirm_random; ?>" />
			<input type="hidden" name="sequence_token" id="sequence_token" value="<?php echo $sequence_token; ?>" />
			</div>

	</div>		
</div>		
			
</section>
</form>

<script type="text/javascript">

			$('#ps_com_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#noc_cft_date').datepick({showOnFocus: false, showTrigger: '#calImg'});

			$('#u_likely_mbbs').datepick({showOnFocus: false, showTrigger: '#calImglk'});
			
			$('#rel_cft_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#ps_rel_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});

			$('#u_pmrc_reg_date').datepick({showOnFocus: false, showTrigger: '#calImg'});

			$('#u_adm_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			
			$('#u_passing_date').datepick({showOnFocus: false, showTrigger: '#calImg2'});
			
			$('#u_intern_date').datepick({showOnFocus: false, showTrigger: '#calImg3'});
			
			$('#u_dd_date').datepick({showOnFocus: false, showTrigger: '#calImg4'});
			
			
			$('#u_attempt_cert_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			$('#u_attempt_cert2_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			$('#intern_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
		
		
			$('#mbbs_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			//$('#pmrc_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#firstpg_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#secondpg_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#thirdpg_ctf_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#u_intern_date_likely').datepick({showOnFocus: false, showTrigger: '#calImg5'});

			$('#u_dob').datepick({showOnFocus: false, showTrigger: '#calImg'});
		
			$('#name_cft_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#sp_citizenship_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			
			$('#sp_mar_date').datepick({showOnFocus: false, showTrigger: '#calImg2'});
			

</script>

<?php $this->load->view('template/footer'); ?>