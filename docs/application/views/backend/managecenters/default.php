<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
<title>Admin :: Manage Interview Centers</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script>
<script language="JavaScript" src="<?php echo base_url(); ?>js/icenters.js"></script>

<style type="text/css" media="screen">
.sbtn{margin-bottom:0 !important;}
td,th{text-align:center;}
</style>

</head>
<body>

<div id="container">
<?php $this->load->view('includes/header_admin'); ?>
<div id="main">
	<?php $this->load->view('includes/left_admin'); ?>   
    <div id="changing">
	<div class="listsquaresmall2">
        <p><h2>Manage Interview Centers</h2></p>
        
		<?php if( !empty( $errmsg ) ) { ?>        
          	<span><?php echo $errmsg; ?></span>          
        <?php } ?>         
          
           		<form action="<?php echo base_url(); ?>backend/managecenters/addCenter" method="post">
          		
				<table cellspacing="5" cellpadding="5" align="center"> 
				
                <tr>
				<th>Add New Center</th>
				<td>
				<input type="text" size="5" name="code" id="code" value="" placeholder="Code" />
				</td>
				<td>
				<input type="text" name="label" id="label" value="" placeholder="Title" />
				</td>
				<td>
				<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
				<input class="sbtn" type="submit" value="Add" name="submit" /></td>
				</tr>
				
				</table>
		
          	</form>     

			<div class="clear"></div> 			
			<div class="clear"></div>
			
			
			<table style="width:85%;" align="center"> 
			<tbody>
			<tr>
			<th>&nbsp;</th>
			<th>Code</th>
			<th>Name</th> 
			<th>&nbsp;</th>
			</tr>
			<?php 
			
			if(!empty($records))
			{
				$i=1;
				
				foreach($records as $record)
				{
					echo '<tr id="center_'.$record->id.'"><td>'.$i.'</td><td>'.$record->code.'</td><td>'.$record->label.'</td><td><button onclick="return del('.$record->id.');">Delete</button></td></tr>';
					$i++;
				}				
			}
			else
			{
				echo '<tr><td colspan="4">Nothing to Display</td></tr>';
			}
					
			
			?>
			</table>
			</table>
			
			

			
		<div class="clear"></div>
    </div>     
	</div>
</div>

<?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>
</html>