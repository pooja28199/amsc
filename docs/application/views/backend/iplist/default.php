<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Manage IP</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />



<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">
<div class="listsquaresmall2">
     

          <p><h2>Manage IP List</h2></p>
          
          <?php
          
          
          if( is_array($records)  && COUNT( $records ) == 0  )
		  {
		  ?>	
		  	<span style="display:none">No Users Found !</span>
		  <?php
		  }
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>     
		  
		  <br />
Current IP : <?php echo $_SERVER['REMOTE_ADDR'];?>
<br />

		      
          
           		<form action="<?php echo base_url(); ?>backend/iplist/addip" method="post">
          		
				<table cellspacing="5" cellpadding="5" align="center">
				
                
               
				
				
				
				
                <tr>
				<td>IP Address </td>
				<td>
				<input type="text"  name="ip" value=""  />
				</td>
				</tr>
                
                
                
                <tr>
				<td></td>
				<td><input type="submit" value="Add" name="submit" /></td>
				</tr>
				</table>
				
          			
          		
          	</form>         
          
        
          				
          
           		
          <?php
          
          $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
          $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$i = $uri_arr['offset'] + 1;
			
			$offset = $uri_arr['offset'] ;
				
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	  // add a link to download as excel	
			  
			  $files=array();	 
				
				// below code commented by preeti on 9th apr 14 
				
			  $keyword = $this->input->post('keyword'); 
			
			  if( $this->input->post( 'keyword' ) )
			  {
				$keyword = $this->input->post( 'keyword' );	
					
			  }
			  else
			  {
			  	$keyword =0;
			  }
			   			 
			  array_push($files,$keyword);  
		
		      $gender = '';
		
			  if( $this->input->post( 'gender' ) )
			  {
				$gender = $this->input->post(  'gender' );
							
			  }
			  else
			  {
			  	$gender =0;
					
			  }
		 
		      array_push($files,$gender);
			  
			  $state = '';
				
			  if( $this->input->post( 'state' ) )
			  {
				$state = $this->input->post( 'state' );
							
			  }
			  else
			  {
			  	$state =0;
					
			  }
		      
		    array_push($files,$state);
			  
			$start_date = '';
			
			if( $this->input->post( 'start_date' ) )
			{
				$start_date = $this->input->post( 'start_date' );
						$start_date=date("m.d.Y", strtotime($start_date));
						
			}
			else
			{
				$start_date ='0000.00.00';
					
			}
		 
		 	array_push($files,$start_date);
		
			$end_date = '';
		
			if( $this->input->post( 'end_date' ) )
			{
				$end_date = $this->input->post( 'end_date' );
					$end_date=date("m.d.Y", strtotime($end_date));	
			}
			else {
				$end_date ='0000.00.00';
			}
			
			array_push($files,$end_date);
			
			$start_age = '';
		
			if( $this->input->post( 'start_age' ) )
			{
				$start_age = $this->input->post( 'start_age' );
						
			}
			else
			{
				$start_age =0;
					
			}
		
			array_push($files,$start_age);
			
			$end_age = '';
			
			if( $this->input->post( 'end_age' ) )
			{
				$end_age = $this->input->post( 'end_age' );
						
			}
			else
			{
				$end_age =0;
					
			}
		
			array_push($files,$end_age);
		
		
		    $url1 = base_url().'admin/file_pdf6';	
			  
			  
			  $co = implode("-", $files);
			  
						  
			 // echo $co;
			 $url1 = base_url().'admin/file_pdf6/';
			 
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword ); // encoded keyword
				
			  	//$url1 = base_url().'admin/file_pdf/'.$keyword.'/'.$gender.'/'.$state.'/'.$start_date.'/'.$end_date.'/'.$start_age.'/'.$end_age;
				//$url1 = base_url().'admin/file_pdf4('.$keyword.','.$gender.','.$state.','.$start_date.','.$end_date.','.$start_age.','.$end_age.')';
				$url1 = base_url().'admin/file_pdf6/'.$co; 
			 }
			 
			 $url31 = base_url().'admin/file_xls12';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url31 = base_url().'admin/file_xls12/'.$co;
				 
			 }
			 
			 $url21 = base_url().'admin/file_pdf5';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url21 = base_url().'admin/file_pdf5/'.$co;
				 
			 }
			 
			 $url221 = base_url().'admin/file_pdf4';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url221 = base_url().'admin/file_pdf4/'.$co;
				 
			 }
			
			 $url211 = base_url().'admin/dozipallapp';
			
				//$url2=base_url().'Member_con/topdf1';
			
		
	
  			// add the list heading for columns	
				
			$this->table->add_row(
			
			'<span class="col-label">S.No</span>', 
			
			'<span class="col-label">IP</span>', 
			  
			 
			
			'<span class="col-label">Action</span>'
			
			
			);	
			  
	        foreach( $records as $row )
	        {
	          	
				         	
				
	          	$this->table->add_row(
	          	
					'<span class="col-data">'.$i.'</span>',
				 
				'<span class="col-data">'.$row->ip.'</span>',
				
					
					
				
					
		"			
					  <a class='link' href='".base_url()."backend/iplist/deleteip/".$row->id."'>Delete</a>" 					
					
				);
				
				$i++;
	        }
	
			echo form_open('admin/submit'); 
	// below line added by preeti on 4th mar 14
			
			echo  $this->pagination->create_links();	
			
			?>
			
			<!-- below div added by preeti on 4th mar 14 -->
			
			<div class="clear"></div>
	
			<?php
	$tmpl = array (
                    'table_open'          => '<table border="0" cellpadding="4" width="100%" cellspacing="0">'

              );

$this->table->set_template($tmpl); 
			echo $this->table->generate();
	 ?>
	  
	 			
	
				<?php
	
				echo  $this->pagination->create_links();			
				
				echo form_close();
		}
				  
		?>         

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#idate').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			//$('#to').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>