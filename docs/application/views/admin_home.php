<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Home</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

   <?php $this->load->view('includes/left_admin'); ?>

    <div id="changing">

      <div class="loginsquaresmalls" style="height:auto;">

          <p><h2><center>Welcome to the Admin Panel</center></h2></p>
<br style="clear:both;" />
<div style="padding:10px;">

Today Total Registrations : <?php echo $today_total;?>

<br />
Today Pending Registrations : <?php echo $today_pending;?>
<br />
<b style="color:#030">Today Confirmed Registrations : <?php echo $today_confirmed;?></b>
<br />

<br />
<br />

Total Registrations : <?php echo $total;?>

<br />
Pending Registrations : <?php echo $pending;?>
<br />
Confirmed Registrations : <?php echo $confirm;?>
<br />
<br />

Total Male Candidates (<?php echo $tmale;?>)  = <?php echo $pgmale;?>(PG) + <?php echo $mbbsmale ?>(MBBS)
<br />
Total Female Candidates (<?php echo $tfemale;?>) = <?php echo $pgfemale;?>(PG) + <?php echo $mbbsfemale ?>(MBBS)
<br />
<br />


<table align="center" style="width:100%;">

<tr>
<th align="center" colspan="2"><h2>::State Wise Registrations::</h2></th>
</tr>

<tr>
<th align="left">State</th>
<th align="center">Confirmed Registrations</th>
<!--<th align="center">Pending Registrations</th>-->
</tr>

<?php if(!empty($records)) {
		
	$total=0;	
		
	foreach($records as $record)
	{
		$state_name=$record['state_name'];
		$num_confirm=$record['num_confirm'];
		$num_pending=$record['num_pending'];
		
		echo '<tr>';
		echo '<td>'.$state_name.'</td>';
		echo '<td align="center">'.$num_confirm.'</td>';
		//echo '<td align="center">'.$num_pending.'</td>';
		echo '</tr>';
		
		$total=$total+$num_confirm;
		
	}
		
} ?>

<tr>
<td>&nbsp;</td>
<th align="center">Total : <?php echo $total ?></th>
</tr>

<!-- <?php 

$new = 'select reg_regno from register where pay_status ="1" LIMIT 2200';

$res = mysql_query($new);
while($ft = mysql_fetch_array($res)){
	echo $ft['reg_regno'].'<br>';
}







 ?> -->

</table>


</div>

        </div>

      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>