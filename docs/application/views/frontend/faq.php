 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
 <div id="fullright">
 <div class="contentsquaresmall boldtext">

			

          <p>
          	
          <h1>FREQUENTLY ASKED QUESTIONS</h1>     	
          <h4>PLEASE READ THE INSTRUCTIONS FOR SUBMITTING ONLINE APPLICATION CAREFULLY FOR DETAILS ON SIZE AND TYPE OF DOCUMENTS REQUIRED FOR UPLOADING AT THE TIME OF APPLICATION PROCESS.</h4>
          <!--<h4>
         Important: A copy of the application form generated online at the end of registration along with the Demand Draft is required to reach this office within 10 days of the closing of registration. If an applicant has NCC certificate (A/B/C) and that has been mentioned while submitting the form then a copy of the same should be attached while forwarding the Demand Draft and printed application form. 
          </h4>-->
          </p>
          
          <p>
 
<h3>Frequently Asked Questions</h3>
<p>
Q.	What all documents do I need to have when I am applying online and what format and size is acceptable for the application?<br />
A.	The relevant details are given in the Notifications under the subject "INSTRUCTIONS FOR SUBMITTING ONLINE APPLICATION". The relevant scanned documents have to be with-in the limits as specified in the instructions. Any documents not following the size/type as specified will not allow the applicant to proceed further.  
</p>
<p>
Q.	Do I need a valid and operational mobile number and email for registration?<br />
A.  Yes, registration process cannot start without a valid email and mobile number. The One Time Password (OTP) will be sent to your registered mobile number. The OTP is required for confirmation of preliminary process and proceeding to the first step of registration form. The registered email will be used for communicating the dates of interviews and other updates/details if any relating to the applicant.  
</p>
<p>
Q.	Do I need to save the OTP and Registration Number for further use?<br />
A.	Yes. The OTP is valid for 48 hours and the <u>confirmation for registrations has to be done within 48 hours of receipt of OTP</u>. If the OTP has expired a fresh OTP can be generated online. Registration number once allotted is permanent and cannot be changed, so it needs to be saved for further use/login and correspondence. 
</p>
<p>
Q.	What if I have entered the wrong mobile number/email id?<br />
A.	The mobile number once entered is stored in the database. One cannot register again with the same mobile number or email id. 
</p>
<p>
Q.	I have changed my name after class 10th. What do I need to do?<br />
A.	Please upload a copy of authority for change of name (e.g. News paper advertisement, Affidavit) and bring all the relevant documents in original and an attested copy at the time of interview for verification. 
</p>
<p>
Q.	Do I need to type my name in Hindi during the application process?<br />
A.	Yes. At the time of registration process the link for Hindi typing is given. Click the link and you will be directed to Google Hindi typing tool. Please select Hindi language from the left side menu of the typing tool and start typing your name by using the English keyboard as it is spelt in Hindi. Select from the options shown by the typing tool and when your name is typed full please copy and paste in the text box provided on the form.
</p>
<p>
Q.	Can I apply if my age as on 31 Dec of this year is more than 45 years?<br />
A.	No. Applicant should be less than 45 years as on 31 Dec of the year.
</p>
<p>
Q.	Can I use Mr / Shri / Dr. or any other salutations with my father's name?<br />
A.	No. Salutations are not required while entering the father's name in the form. 
</p>
<p>
Q.	I am a foreign National, can I apply?<br />
A.	No, only candidates having Indian Nationality can apply.
</p>
<p>
Q.	My spouse has a Foreign Nationality, can I apply?<br />
A.	Yes, provided the spouse has applied for Indian Nationality. 
</p>
<p>
Q.	I am a Medical Graduate from a country outside India, can I apply?<br />
A.	Yes. Foreign graduates having Indian Nationality can apply provided they have a valid Permanent Medical Registration Certificate issued by Medical Council of India. 
</p>
<p>
Q.	I am an Indian National and graduated from India and I have only the provisional Medical registration Certificate. Can I apply and upload the same in lieu of Permanent Medical Registration Certificate (PMRC)?<br />
A.	Yes. However, the Permanent Medical Registration Certificate needs to be sent to this office as soon as it is issued. 
</p>
<p>
Q.	I am a Foreign Medical Graduate and I have a Permanent Registration certificate issued by MCI. What should I write in the basic qualification in the form?<br />
A.	The basic qualification is the degree which has been awarded by the Foreign Medical Institute for eg. MD(PHYSICIAN). 
</p>
<p>
Q.	Date of passing MBBS is the date on which I cleared the final MBBS exam or the date of completion of Internship?<br />
A.	The date of passing MBBS is the date on which the candidate cleared his/her final MBBS exams.
</p>
<p>
Q.	I have passed final MBBS-Part I in 1st attempt and final MBBS-Part II in 2nd attempt, can I apply?<br />
A.	Yes. Applicant should have passed Part-I and Part-II of the final MBBS exam in maximum of 2 attempts each.
</p>
<p>
Q.	I have the attempt certificate in which passing of Part-I and Part-II of the final MBBS exam has been mentioned in the same certificate. Can I upload the same certificate at both the places for upload?<br />
A.	Yes. If the attempt certificate mentions both the Parts of final MMBS exams in the same certificate, it needs to be uploaded at both the places.
</p>

<p>
Q.	I am a Post Graduate (MD/MS/Diploma). Can I apply?<br />
A.	Yes. The relevant degrees and subjects need to be filled in the form. A maximum of three (03) Post Graduate qualifications can be entered. If more than three (03) qualifications are there, then the relevant copies and original can also be brought to the interview for verification. 
</p>
<p>
Q.	I got the Demand Draft made from the bank and the issue date is before the date of opening of registrations. Will my application be considered with that Demand Draft?<br />
A.	No. The date of issue of Demand Draft has to be between the date of opening and date of closing of Registration Process. 
</p>
<p>
Q.	Can my Postal address be different from my Permanent address?<br />
A.	Yes. 
</p>



	

          
          
          
          </p>

 
        </div>      
 </div>
 
 <?php $this->load->view('template/footer'); ?>
 