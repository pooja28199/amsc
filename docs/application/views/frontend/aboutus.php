 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
 <div id="fullright">
 <div class="contentsquaresmall boldtext">

			

          <p>
          	
          	<h1>Know Us : A way of life second to none</h1>     	
          
          </p>
          <style>
		  .firstltr:first-letter{ font-size: 3em;}
		  #lpara{ text-align:justify; margin-right:260px;}
          #rpara{text-align:justify; margin-left:240px;}
          #rrpara{text-align:justify; margin-left:260px;}
          
          </style>
           <img style="float:right;" width="250" src="<?php echo base_url();?>asset/images/aboutus/1.1.JPG" />
 
		  <p id="lpara" class="firstltr">
        The Army Medical Corps is amongst one of the best options available to medical graduates of our country where there is an opportune professional environment of an exceptional order blended with high degree of professionalism, dignity and self esteem. It offers a golden opportunity to be a part of world's finest service and get trained not only to be an officer and a gentleman but also a highly skilled professional. Army Medical Corps promises both professional and personal growth at every stage of the career. In addition to the professional growth and development, the adventure and extra-curricular activities in the service ensure an all round development essential for today's world. Apart from attractive pay and perks, service offers the best in life style and professional growth.
          
          
          </p>


 <br class="clear" />
 
 
           <img style="float:left;" width="250" src="<?php echo base_url();?>asset/images/aboutus/1.2.JPG" />
 
 <p id="rrpara">
The young medical graduate is often unaware of how to join the Army Medical Corps and all it has to offer. The aim here is to give you a glimpse on the opportunities available to doctors for all round development of their personality, scope for continuous professional enhancement and the perks and privileges of serving in Army Medical Corps. 
</p>

<br class="clear" />

<h1>National Contribution with a Global Imprint</h1>

 <img style="float:right;" width="250" src="<?php echo base_url();?>asset/images/aboutus/2.1.JPG" />
 
		  <p id="lpara">
          Armed Forces Medical Services is the first tri-service (Army, Navy and Air Force) organization and one of the largest organized medical services in the country. It has state of the art tertiary care hospitals and speciality centres of excellence. It provides medical support to the Armed Forces during war as well as comprehensive health care to all service personnel, ex-servicemen and their dependents during peace. Army Medical Corps provides medical aid during natural calamities both at national and international levels. 
          
          
          </p>


 <br class="clear" />
 
<h1>Prestigious & Professionally Satisfying Career</h1>

  <img style="float:left;" width="250" src="<?php echo base_url();?>asset/images/aboutus/3.1.JPG" />
 

 <ul id="rpara">
 <li>The officer on commissioning (as Capt/Equivalent) shall be employed as per service requirement in any part of the country or abroad in Army, Navy or Air Force.</li>
 <li>
The service offers courses/training on the basis of performance in both national and international institutions. 

 </li>
 <li>
Provision for Post Graduation/study leave in all specialities/super specialities in premier institutes. 
 </li>
  
 <li>
Brighter prospects to avail Post Graduation as ex-serviceman both in service as well as civil hospitals and institutions.  
 </li>
 
 <li>
Deputation abroad in various UN Peace Keeping Missions. 
</li>
 
 </ul>

<br class="clear" />



<h1>Life..which money can not provide</h1>


<img style="float:right;" width="250" src="<?php echo base_url();?>asset/images/aboutus/3.3.JPG" />
 
		  <ul id="lpara">
          
<li>Quality professional work with adequate growth and satisfaction.</li>
<li>Unparalleled pride of serving mankind and motherland.</li>
<li>All round personality development required of a leader in all walks of life.</li>
<li>Free medical treatment for self and dependents.</li>
<li>Canteen (CSD) facilities.</li>
<li>Membership of premier clubs and Institutes</li>
<li>Physical and mental fitness through sports and adventure.</li>
<li>Career spanning the entire nation.</li>
<li>Leave travel concession (LTC), concessional air and rail travel and free railway warrants for self and dependents.</li>
<li>Bonhomie & camaraderie unique to Services.</li>
<li>Ample avenues for self & spouse for contribution to society.</li>

          
          </ul>
 

 <br class="clear" />
 



<br class="clear" />
<br class="clear" />
        </div>      
 </div>
 
 <?php $this->load->view('template/footer'); ?>
 