 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
 
 <style>
 .contact{font-size:13px;}
 .organogram h2{font-weight:bold;font-size:16px;margin:0;color:#663333;}
 .organogram h3{font-weight:bold;font-size:14px;margin:0;color:#663333;}
 </style>
 
 <div id="fullright">
 <div class="contentsquaresmall organogram">

	<p><h1>ORGANOGRAM</h1></p>
	
	<h3 align="center" style="font-style:italic;">:: ORGANISATION STRUCTURE AT OFFICE OF DGAFMS ::</h3>
	<br/>
	
	<table width="100%" cellspacing="0" cellpadding="3" align="center">	
		<tr>
			<td colspan="2" align="center">
				<p class="acenter">
					<img class="cimage" src="<?php echo base_url();?>asset/images/dg.jpg" />
					<!-- <h2>(LT GEN M K UNNI PVSM, AVSM, VSM, PHS)</h2> -->
					<h2>(LT GEN BIPIN PURI VSM, PHS)</h2>
					<h3>{DGAFMS}</h3>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<p class="acenter">
					 <img class="cimage" src="<?php echo base_url();?>asset/images/DG_org.png" /> 
					<!-- <img class="cimage" src="<?php echo base_url();?>asset/images/DG_org.png" /> -->
					<h2>(LT GEN R S GREWAL,VSM)</h2>
					<h3>{DG (ORG & PERS) AFMS}</h3>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<p class="acenter">
					<!-- <img class="cimage" src="<?php echo base_url();?>asset/images/AVM RK RANYAL.png" /> -->
					<img class="cimage" src="<?php echo base_url();?>asset/images/" />
					<h2></h2>
					<h3>{Addl DGAFMS(HR)}</h3>
				</p>
			</td>
		</tr>
		
		
		<tr>
			<td colspan="2" align="center">
				<p class="acenter">
					<img class="cimage" src="<?php echo base_url();?>asset/images/BRIG B SRIDHAR.png" />
					<h2>(BRIG B SRIDHAR)</h2>
					<h3>{Dy DGAFMS(HR)}</h3>
				</p>
			</td>
		</tr>
		<tr>
			<td align="center">
				<p class="acenter">
					<img class="cimage" src="<?php echo base_url();?>asset/images/A S YADAV.png" />
					<h2>(Mr A S YADAV)</h2>
					<h3>{Director AFMS(P-I)}</h3>
				</p>
			</td>
			<td align="center">
				<p class="acenter">
					<img class="cimage" src="<?php echo base_url();?>asset/images/U S LATWAL.png" />
					<h2>(Mr U S LATWAL)</h2>
					<h3>{Director AFMS(P-II)}</h3>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<p class="acenter">
					<img class="cimage" src="<?php echo base_url();?>asset/images/V K SINGH.png" />
					<h2>(Mr V K SINGH)</h2>
					<h3>{Dy Director AFMS}</h3>
				</p>
			</td>
		</tr>
	</table> 

	<br><br>
	
    </div>      
 </div>
 
 <?php $this->load->view('template/footer'); ?>
 