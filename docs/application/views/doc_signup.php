 <?php $this->load->view('template/header'); ?>



<!--
#outPopUp{
     position:absolute;
     width:300px;
     height:200px;
     z-index:15;
     top:50%;
     left:50%;
     margin:-100px 0 0 -150px;
     background:red;
}
#mydiv {
    position:fixed;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; /*set to a negative number 1/2 of your height*/
    margin-left: -15em; /*set to a negative number 1/2 of your width*/
    border: 1px solid #ccc;
    background-color: #f3f3f3;
}
 down vote accepted
	

The easy way, if you have a fixed width and height:

#divElement{
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -50px;
    margin-left: -50px;
    width: 100px;
    height: 100px;
}​
-->


<style>

.basic-grey .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
}
.basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select
{
	width:100%;
	max-width: 245px;
	margin-bottom: 5px;
}
td{width: 33.3%;}
div.warnings{
	margin-bottom:20px;
	text-align:left;
}
div.warnings span{
	display:block;
	font-size: 11px;
}
.basic-grey label
{
	font-size: 14px;
	font-weight:bold;
}
</style>

<?php $this->load->view('template/left_user_out'); ?>

<div id="fullright">

<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<div class="regsquaresmall">
<?php
          	$attributes = array('class' => 'basic-grey');

          		echo form_open_multipart('doc/add_doc',$attributes);		
					
		?>	
<h1>New Registration</h1>
<!--Please check links below before applying
<br />

<ul>
<li><a href="http://amcsscentry.gov.in/uploads/not/AVT_NEW.pdf" target="_blank">ADVERTISEMENT SSC 2014 </a></li>
<li><a href="http://amcsscentry.gov.in/uploads/not/INSTRUCTIONS_FOR_FILLING_UP_THE_FORM_final1.pdf" target="_blank">INSTRUCTIONS FOR FILLING THE ONLINE FORM </a></li>
<li><a target="_blank" href="http://amcsscentry.gov.in/faq">FAQ</a></li> 
</ul>-->

 	<div class="error warnings">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					$explode=explode('.',$errmsg);
					
					foreach($explode as $val)
					{
						if($val)
						echo '<span>* '.$val.'.</span>';
					}
					//echo ( str_replace('.','.</br>',$errmsg) );	
				}				 
				
				?>
				
			</div>
			
			
			<?php /* 
			
			<div class="collect-signup">
			
			<div class="left"><label for="reg_fname">First Name<span class="star">*</span></label></div>
			
			<div class="right" >
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_fname" 
				
				id="reg_fname" value="<?php echo $reg_fname; ?>" />
									
			</div>		
			
			</div>
			
			
			
			<div class="collect-signup" >
			
			<div class="left"><label for="reg_mname">Middle Name</label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_mname" 
				
				value="<?php echo $reg_mname; ?>" />
				
			</div>
	
			</div>
			
			<div class="collect-signup">
			
			<!--  below line modified by preeti on 2nd apr 14 for black-box testing -->
			
			<div class="left"><label for="reg_lname">Last Name</label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_lname" 
				
				value="<?php echo $reg_lname; ?>" />
				
			</div>
	
			</div>
	
			*/ ?>
	
			
			<table style="width:100%;">
			
			<tr>			
				<!--<td><span class="star">*</span> Mandatory Field</td>-->			
				<td  colspan="3">
					<strong>Note: Fields marked with asterisk(*) are Mandatory.</strong>
				</td>				
			</tr> 
			
			<tr>						
				<td colspan="3">&nbsp;</td>				
			</tr>
			
			<tr>		
				<td><label for="reg_email">Email <span class="star">*</span></label></td>			
				<td>
					<input type="text" name="reg_email" id="reg_email" autocomplete="off" value="<?php echo $reg_email; ?>" />
					<div id="div_email">			
						<span class="ajax-msg" id="msg_span_email"></span>					
					</div>				
				</td>
				<td>&nbsp;</td>
			</tr>
	
			
			<tr>			
				<td><label for="reg_aadhar">Aadhar Number <span class="star">*</span></label></td>		
				<td>
					<input type="text" name="reg_aadhar" id="reg_aadhar" autocomplete="off" value="<?php echo $reg_aadhar; ?>" />
					<div id="div_reg_aadhar">
						<span class="ajax-msg" id="msg_span_reg_aadhar"></span>	
					</div>
				</td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>						
				<th colspan="3">OR</th>				
			</tr>
			
			<tr>			
				<td><label for="reg_pan">PAN Number <span class="star">*</span></label></td>		
				<td>
					<input type="text" name="reg_pan" id="reg_pan" autocomplete="off" value="<?php echo $reg_pan; ?>" />
					<div id="div_reg_pan">
						<span class="ajax-msg" id="msg_span_reg_pan"></span>	
					</div>
				</td>
				<td>&nbsp;</td>
			</tr>
	
			
			<tr>			
				<td>
					<label for="reg_mobile">Mobile <span class="star">*</span></label>
					<span class="small">(+91)</span>
				</td>
				
				<td>
					<input type="text" maxlength="10" name="reg_mobile" autocomplete="off" id="reg_mobile" value="<?php echo $reg_mobile; ?>" />
					<div id="div_mobile">
						<span class="ajax-msg" id="msg_span_mobile"></span>
					</div>
				</td>	
				<td align="right">
					<span class="small">
					<b>( Mobile no. should be of 10-digit )</b>
					</span>
				</td>
			</tr>
						
			<tr>			
				<td><label for="captcha">Captcha <span class="star">*</span></label></td>			
				<td>				
					<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />				
				</td>
				<td align="center"><?php echo $cap_img; ?></td>
			</tr>
			
			<tr>						
				<td colspan="3">&nbsp;</td>				
			</tr>
			
			<tr>				
				<td>
					&nbsp;
					<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
					<input type="hidden" name="icenter" id="icenter" value="<?php echo $icenter; ?>" />
				</td>			
				<td colspan="2">
					<?php echo form_submit(array('class'=>'button','name'=>'sub'), 'Submit'); ?>			
				</td>         	
			</tr>
			</table>
			
			<?php echo form_close(); ?>
			
	</div>
</div>

<!--
<br id="clear" />

</div>
<div id="footer" style="color:#fff">
<table cellpadding="0" cellspacing="0" width="100%">
<tr>
<td>
Best View on Chrome,Firefox. Best Resolution:1024*768.
</td>
<td align="right">
Copyright &copy; 2014, Powered by NIC, Delhi
</td>
</tr>
</table>
<center>Copyright &copy; 2014, Powered by NIC, Delhi</center> 
</div>

</body>
</html>
-->

 <?php $this->load->view('template/footer'); ?>

