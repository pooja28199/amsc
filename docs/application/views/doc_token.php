<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Registration Step 2</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <div id="mainleft">

      <div id="contmenu">

        <div id="menu">

          <ul>

            <li><a href="<?php echo base_url(); ?>doc/index">Login</a></li>          

          </ul>

        </div>

      </div>

    </div>

	
	<div id="changing">

		<div class="tokensquaresmall">

		<h2>Registration Step - 2 </h2>

			<span class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo strip_tags( $errmsg );	
				}				 
				
				?>
				
			</span>

          <p>
          	
          	
          	
          	
          	<?php          	
          		
          	
          		echo form_open('doc/register');
				
				echo form_label('Username', 'doc_username');
				
				// below code modified by preeti on 21st apr 14 for manual testing
				
				$att = array(
				
					'name' => 'doc_username',
					
					'id' => 'doc_username',
					
					'autocomplete' => 'off'
				
				);
				
				//echo form_input('doc_username', '');	//line commented by preeti on 21st apr 14 for manual testing
				
				echo form_input( $att ); //line added by preeti on 21st apr 14 for manual testing			
								
				echo form_label('Token ', 'doc_id');
				
				echo form_input('doc_id', '');
								
				echo form_submit('sub', 'Submit');
							
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>      

      
    </div>

	
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


</body>

</html>