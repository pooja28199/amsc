<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">


<html>
  <head>
     <title>All Documents Printout</title>
<style type="text/css">
    @media print 
    {
        .dontprint{display:none} 
    }
</style>
<!-- <script language="JavaScript" src="<?php echo base_url(); ?>js/pdfobject.js"></script> -->
<script type="text/javascript" language="JavaScript">

    function printIframePdf(){
        window.frames["printf"].focus();
        try {
            window.frames["printf"].print();
        }
        catch(e){
            window.print();
            console.log(e);
        }
    }
    function printObjectPdf() {
        try{            
            document.getElementById('idPdf').Print();
        }
        catch(e){
            printIframePdf();
            console.log(e);
        }
    }

    function idPdf_onreadystatechange() {
        if (idPdf.readyState === 4)
            setTimeout(printObjectPdf, 1000);
    }
</script>
</head>
<body>
<div class="dontprint" >
    <form><!-- <input type="button" onClick="printObjectPdf()" class="btn" value="Print"/>--></form>
</div>
<?php
$base_url	= base_url().'/uploads/merged/rs.pdf';
?>
<iframe id="printf" name="printf" src="<?php echo $base_url; ?>" frameborder="0" width="440" height="580" style="width: 440px; height: 580px;display: none;"></iframe>
<object id="idPdf" onreadystatechange="idPdf_onreadystatechange()"
    width="100%" height="650"  type="application/pdf"
    data="<?php echo $base_url; ?>">
    <embed src="<?php echo $base_url; ?>" height="650" width="100%"  type="application/pdf">
    </embed>
    <span>PDF plugin is not available.</span>
</object>
</body>
</html> 