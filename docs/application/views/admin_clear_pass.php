<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Clear DB</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>


<!-- below scripts added by preeti on 26th mar 14 for black-box testing -->


<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the old password typed 
			
			var pass;
			
			var result;
			
			//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
			
			var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
			
			pass = $('#clear_pass').val();
			
			if( pass != '' )
			{
				result = md5( md5( pass ) + salt );
			
				// set the value of the hidden field
				
				$('#clear_pass_encode').val(result);
				
				// clear the field
				
				$('#clear_pass').val('');
			}		
			
			
		});
		
	});
	
</script>



</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>

    <div id="changing">

      <!--<div class="loginsquaresmall">-->

		<!-- above line commented and below added by preeti on 5th mar 14 -->

		<div class="emailsquaresmall">

		<div class="heading">DB Password</div>

			        
          <?php
          echo form_open('admin/clear_verify_pass');
		  ?>
          
          <table style="margin-left: 50px;">
          	
          	<tr>
          		
          		<td colspan="2">
          		
          		<span>
          			
          			
          			
          			<?php 
				
				// below code modified by preeti on 26th mar 14 
				
				if( validation_errors() )
				{
					echo strip_tags(validation_errors());
				}
				else 
				{
					echo $errmsg;	
				}				 
				
				?>
          			
          		</span>
          		
          		</td>
          	
          	</tr>
		  	
		  	<tr>
		  		
		  		<td style="vertical-align: top;color:#993300;">Clear DB Password</td>
		  		
		  		<td>
		  			
		  			<!-- below line modified by preeti on 21st apr 14 for manual testing  -->
		  			
		  			<input type="password" <?php echo 'autocomplete="off"'; ?> name="clear_pass" id="clear_pass" />
		  			
		  			<!-- below code added for black-box testing by preeti on 26th mar 14 -->
		  			
		  			<input type="hidden" name="clear_pass_encode" id="clear_pass_encode" />
		  			
		  			<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
					<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
					  			
		  		</td>
		  				  		
		  	</tr>
		  	
		  	<tr>
		  		
		  		<!--<td colspan="2" >
		  			
		  			<input type="submit" name="sub" value="Submit" />
		  			
		  		</td>-->
		  				  		
		  	</tr>
		  	
		  </table>
		  
		  <?php
		  
		  $sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Submit');// code added by preeti on 26th mar 14 for black-box testing
				
		  echo form_submit( $sub_att );// code modified by preeti on 26th mar 14 for black-box testing
				
		  
          echo form_close();
          ?> 	

        </div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script>
	
	$('#proceed').click(function(){
		
		var choice = confirm("Are you sure you want to clear the complete User related Information from Database ? ") ;
		
		if( choice )
		{
			return true;
		}
		
		return false;
				
	});
	
</script>

</body>

</html>