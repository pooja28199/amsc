 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>

<script src="<?php echo base_url(); ?>js/jquery.min.js"></script> <!-- added by preeti on 20th feb 14 -->

<script src="<?php echo base_url(); ?>js/marquee.js"></script> <!-- added by preeti on 20th feb 14 -->




<div id="fullright">
	<div class="contentsquaresmall">
		<p>
          	<h1>RESULTS</h1>     	
        </p>
		
		<?php if(empty($record)){ ?>
		  
        <p>
			<b class="error">PLEASE CHECK THIS PAGE  AFTER THE INTERVIEWS</b>
        </p>
		  
		<?php } ?>
		  
        <!--<marquee behavior="scroll" direction="up" scrollamount="2" height="100" width="350">-->
         
        <!-- above line commented below line added by preeti on 20th feb 14 -->
         
        <marquee behavior="scroll" direction="up" scrollamount="1">
         
		<ul>
		
		<?php
          
		foreach( $record as $val )
		{
		$url = base_url().'uploads/result/'.$val->res_file_path;
        
		?>
          
          	<!-- below line modified by preeti on 3rd mar 14 -->
          
			<li>
				<div class="not_left">
					<b>
						<a target="_BLANK" href="<?php echo $url; ?>">
							<?php echo strtoupper( $val->res_title ) ; ?> - <?php echo db_to_calen( $val->res_date ); ?>
						</a>
					</b>
				</div>
			</li>
			
		<?php } ?>	

		</ul>
			
		</marquee>

        </div>      

<script>
	
	$('marquee').marquee('pointer').mouseover(function () {
  $(this).trigger('stop');
}).mouseout(function () {
  $(this).trigger('start');
}).mousemove(function (event) {
  if ($(this).data('drag') == true) {
    this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
  }
}).mousedown(function (event) {
  $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
}).mouseup(function () {
  $(this).data('drag', false);
});
	
</script>
</div>



 <?php $this->load->view('template/footer'); ?>
