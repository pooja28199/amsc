<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Admin Audit Trail</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>-->


<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>



</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

      <div class="listsquaresmall">

          <p><h2>Admin Audit Trail</h2></p>
          
          <?php
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
          
          
          if($this->uri->segment(3) != '' )
		  {
		  	$i = $this->uri->segment(3) + 1;	
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
         $this->table->add_row(
		 
		 '<font class="col-label">S.No</font>', 
		 
		 '<font class="col-label">ID</font>', 
		 
		 '<font class="col-label">Username</font>', 
		 
		 '<font class="col-label">IP Address</font>',
		 
		 '<font class="col-label">Date and Time</font>', 
		 
		 '<font class="col-label">Action Performed</font>', 
		 
		 '<font class="col-label">Status</font>'
		 
		 );
		 
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		  
          foreach( $records as $row )
          {
          	
			$action =  $row->loga_action;
			
			$act = '';
			
			$status = '';
			
			switch( $action )
			{
				case 'login': $act = 'Login';
			
						$status = 'Success';
						
						break; 
						
				case 'logout': $act = 'Logout';
			
						$status = 'Success';
						
						break;
						
				case 'logfail': $act = 'Login';
			
						$status = 'Failure';
						
						break;
						
				case 'passchange': $act = 'Password Change';
			
						$status = 'Success';
						
						break;
						
				case 'passchange': $act = 'Password Change';
			
						$status = 'Failure';
						
						break;
						
				case 'default': $act = '-';
			
						$status = '-';
																
			}         	
			
          	$this->table->add_row(
          	
				'<font class="col-data">'.$i.'</font>',
				
				'<font class="col-data">'.$row->loga_id.'</font>',
			
				'<font class="col-data">'.$row->loga_user.'</font>' , 
				
				'<font class="col-data">'.$row->loga_ip.'</font>' ,
				
				'<font class="col-data">'.show_date_time( $row->loga_time ).'</font>' ,
				
				'<font class="col-data">'.strtoupper( $act ).'</font>' ,
				
				'<font class="col-data">'.strtoupper( $status ).'</font>' 
									
			);
			
			$i++;
          }

          	echo form_open('admin/audit_admin_del');
			
			// below line added by preeti on 5th mar 14
			
			echo  $this->pagination->create_links();	
					
			?>
					
			<!-- below div added by preeti on 5th mar 14 -->
					
			<div class="clear"></div>
			
			<?php
          
			echo $this->table->generate();
		?>	
			<div class="clear"></div>
			
			<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
			
			
			
			<div class="clear"></div>
			
		<?php	
			echo  $this->pagination->create_links();
			
			echo form_close();
		}
		else
		{
		?>	
			
			
			<p><span>No Record Found !</span></p>		
		
		<?php
		}		
		  
		?>         

        </div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script>
	
	$('#check_all').click( function()
    {    
    	if($(this).is(':checked'))
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', true);
				
				//$(this).attr('checked','checked');
						
			});			
		}
		else
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', false);
				
				//$(this).removeAttr('checked');
						
			});
		}
  			
  	});
  	
  	$('#del').click( function()
    {
    	var ischecked = false;
    	
    	$(':checkbox').each(function()
		{
			if( $(this).is(':checked') )
			{
				if( !ischecked )
				{
					ischecked = true;	
				}		
							
			}
			
		});
		
		if( !ischecked )
		{
			// below text changed by preeti on 5th mar 14
			
			alert(" Please select at least one checkbox to delete ");
						
			return false;
		}
		else
		{
			return true;
		}
    	
   	});
	
</script>


</body>

</html>