<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: User's List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />



<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
	#pagination{text-align:center;}
	.listsquaresmall2 form{margin-bottom:20px;}	
	.listsquaresmall2{height:auto !important;}
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">
<div class="listsquaresmall2">
     

          <p><h2>List of Approved Users</h2></p>
          
          <?php
          
          
          if( is_array($records)  && COUNT( $records ) == 0  )
		  {
		  ?>	
		  	<span>No Users Found !</span>
		  <?php
		  }
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>         
          
           		<form action="<?php echo base_url(); ?>admin/doc_list2" method="post">
          		
          		<table>
          			
          			<tr>
          				
          				<td valign="top"><span class="txt-label">Keyword</span></td>
          				
          				<!--<td valign="top"><input type="text" name="keyword" value="<?php if( $this->input->post('keyword') != '' ){ echo $this->input->post('keyword'); }else{ echo $keyword; }?>" id="keyword" /></td>-->
          				
          				<!-- above line commented and below added by preeti on 9th apr 14 for black-box testing -->
          				
          				<td valign="top">
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="keyword" value="<?php echo $keyword;?>" id="keyword" />
          					
          				</td>
          				
          				 <td valign="top" colspan="2"><span class="txt-label">( Name, Reg No., Email, Mobile )</span></td>          				         				
          				
          			</tr>
          			
          			<tr>
          				
          				<td valign="top"><span class="txt-label">Gender</span></td>
          				
          				<td valign="top">
          					
          					<select name="gender" id="gender">
          						
          						<option value="">Select</option>
          						
          						<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          						
          						<option <?php if( $this->input->post('gender')  != ''  && $this->input->post('gender') == 'm'  ){ echo "selected"; }else if( $keyword == 'm' ){ echo "selected"; } ?> value="m">Male</option>
          						
          						<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          						
          						<option <?php if( $this->input->post('gender')  != '' && $this->input->post('gender') == 'f'  ){ echo "selected"; }else if( $keyword == 'f' ){ echo "selected"; }  ?> value="f">Female</option>
          						
          					</select>
          					
          				</td>
          				
          				<td valign="top"><span class="txt-label">State</span></td>
          				
          				<td valign="top">
          					
          					<select style="width: 150px;" name="state" id="state">
          						
          					  <option value="">Select</option>
			      				
			      				<?php
			      				$res_st = $this->db->get('state');
								
								foreach( $res_st->result() as $val )
								{						
			      				?>
			      				
			      					<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
			      				
			      					<option <?php if( $this->input->post('state') != '' && $this->input->post('state') == $val->state_id  ){ echo "selected"; }else if( $state == $val->state_id ){ echo "selected"; }  ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
			      				
			      				<?php
								}
			      				?>      				
			      				      				
	      			
          						
          					</select>
          					
          				</td>          				
          				
          			</tr>
          			
          			<tr>
          				
          				<td valign="top"><span class="txt-label">Start Date</span></td>
          				
          				<td valign="top">
          					
          							<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          					
          		          			<input size="15" readonly="readonly" name="start_date" id="start_date" value="<?php if( $this->input->post('start_date') != '' ){ echo $this->input->post('start_date'); }else { echo db_to_calen( $start_date ) ; }?>" type="text" />
          				
          					<span  style="display: none;">
				
								<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
							</span>	
          					
          				</td>
          				
          				<td valign="top"><span class="txt-label">End Date</span></td>
          				
          				<td valign="top">
          					
          					<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          					
          					<input size="15" readonly="readonly" name="end_date" value="<?php if( $this->input->post('end_date')  != '' ){ echo $this->input->post('end_date'); }else { echo db_to_calen( $end_date ); }?>" id="end_date" type="text" />
          				
		
          					<span  style="display: none;">
				
								<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
							</span>	
          					
          				</td>
          				         				
          			</tr>
          			
          			<tr>
          				
          				<td valign="top"><span class="txt-label">Start Age</span></td>
          				
          				<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          				
          				<td valign="top">
          					
          					<!--<input name="start_age" id="start_age" type="text" value="<?php if( $this->input->post('start_age') != '' ){ echo $this->input->post('start_age'); }else{ echo $start_age; }?>" />-->
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> name="start_age" maxlength="3" id="start_age" type="text" value="<?php echo $start_age;?>" />
          					
          				</td>
          				
          				<td valign="top"><span class="txt-label">End Age</span></td>
          				
          				<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          				
          				<td valign="top">
          					
          					<!--<input name="end_age" id="end_age" type="text" value="<?php if( $this->input->post('end_age')  != '' ){ echo $this->input->post('end_age'); }else{ echo $end_age; }?>" />-->
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> name="end_age" maxlength="3" id="end_age" type="text" value="<?php echo $end_age;?>" />
          						
          						
          					</td>          				
          				
          			</tr>
          			
          			<tr>
                    <tr>
          				
          				<td><span class="txt-label">From (Reg No.)</span>
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="from"  id="from" /></td>
          				
          				<td><span class="txt-label">To (Reg No.)</span>
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="to"  id="to" /></td>
          			          				          			
          			</tr>
          				
          				<td valign="top" colspan="4">
          					
          					<input style="margin-left: 180px;margin-top:20px; " type="submit" name="sub" value="Search" />
          					
          				</td>
          				          				
          			</tr>
          			
          		</table>        		
          		
          	</form>         
          
        
          				
          
           		
          <?php
          
          $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
          $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$i = $uri_arr['offset'] + 1;
			
			$offset = $uri_arr['offset'] ;
				
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	  // add a link to download as excel	
			  
			  $files=array();	 
				
				// below code commented by preeti on 9th apr 14 
				
			  $keyword = $this->input->post('keyword'); 
			
			  if( $this->input->post( 'keyword' ) )
			  {
				$keyword = $this->input->post( 'keyword' );	
					
			  }
			  else
			  {
			  	$keyword =0;
			  }
			   			 
			  array_push($files,$keyword);  
		
		      $gender = '';
		
			  if( $this->input->post( 'gender' ) )
			  {
				$gender = $this->input->post(  'gender' );
							
			  }
			  else
			  {
			  	$gender =0;
					
			  }
		 
		      array_push($files,$gender);
			  
			  $state = '';
				
			  if( $this->input->post( 'state' ) )
			  {
				$state = $this->input->post( 'state' );
							
			  }
			  else
			  {
			  	$state =0;
					
			  }
		      
		    array_push($files,$state);
			  
			$start_date = '';
			
			if( $this->input->post( 'start_date' ) )
			{
				$start_date = $this->input->post( 'start_date' );
						$start_date=date("m.d.Y", strtotime($start_date));
						
			}
			else
			{
				$start_date ='0000.00.00';
					
			}
		 
		 	array_push($files,$start_date);
		
			$end_date = '';
		
			if( $this->input->post( 'end_date' ) )
			{
				$end_date = $this->input->post( 'end_date' );
					$end_date=date("m.d.Y", strtotime($end_date));	
			}
			else {
				$end_date ='0000.00.00';
			}
			
			array_push($files,$end_date);
			
			$start_age = '';
		
			if( $this->input->post( 'start_age' ) )
			{
				$start_age = $this->input->post( 'start_age' );
						
			}
			else
			{
				$start_age =0;
					
			}
		
			array_push($files,$start_age);
			
			$end_age = '';
			
			if( $this->input->post( 'end_age' ) )
			{
				$end_age = $this->input->post( 'end_age' );
						
			}
			else
			{
				$end_age =0;
					
			}
		
			array_push($files,$end_age);
		
		
		    $url1 = base_url().'admin/file_pdf6';	
			  
			  
			  $co = implode("-", $files);
			  
						  
			 // echo $co;
			 $url1 = base_url().'admin/file_pdf6/';
			 
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword ); // encoded keyword
				
			  	//$url1 = base_url().'admin/file_pdf/'.$keyword.'/'.$gender.'/'.$state.'/'.$start_date.'/'.$end_date.'/'.$start_age.'/'.$end_age;
				//$url1 = base_url().'admin/file_pdf4('.$keyword.','.$gender.','.$state.','.$start_date.','.$end_date.','.$start_age.','.$end_age.')';
				$url1 = base_url().'admin/file_pdf6/'.$co; 
			 }
			 
			 $url31 = base_url().'admin/file_xls12';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url31 = base_url().'admin/file_xls12/'.$co;
				 
			 }
			 
			 $url21 = base_url().'admin/file_pdf5';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url21 = base_url().'admin/file_pdf5/'.$co;
				 
			 }
			 
			 $url221 = base_url().'admin/file_pdf4';	
			  
			 if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			 {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url221 = base_url().'admin/file_pdf4/'.$co;
				 
			 }
			
			 $url211 = base_url().'admin/dozipallapp';
			
				//$url2=base_url().'Member_con/topdf1';
			$urlexpdf= base_url().'admin/expdf';
			 $this->table->add_row(
			 
			 '<a class="link" href="'.$url1.'">Print</a>', 
			 
			 '<a class="link" href="'.$url21.'">Export</a>',
			 
			 '<a class="link" href="'.$url31.'">Export Excel</a>', 
			 
			  '<a class="link" href="'.$urlexpdf.'">Export PDF</a>',
			 
			 '<a class="link" href="'.$url211.'">zipall</a>', '&nbsp;','&nbsp;', '&nbsp;'
			  
			 );
	
  			// add the list heading for columns	
				
			$this->table->add_row(
			
			'<span class="col-label">S.No</span>', 
			
			'<span class="col-label">Select</span>',
			
			'<span class="col-label">Name</span>', 
			  
			'<span class="col-label">Reg No.</span>', 
			
			'<span class="col-label">Mobile</span>', 
			
			'&nbsp'
			
			);	
			  
	        foreach( $records as $row )
	        {
	          	if( $row->u_is_name_change == 'y' )
				{
					$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
				}
				else 
				{
					$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
				}
				         	
				
	          	$this->table->add_row(
	          	
					'<span class="col-data">'.$i.'</span>',
				
					"<input type='radio' name='Typ' id='Typ' value='".$row->reg_regno."'>
					<input type='hidden' name='regno[]'  value='".$row->reg_regno."'>
					
					
					",
					
					'<span class="col-data">'.strtoupper( $name ).'</span>',// modified by preeti on 28th feb 14
					
					'<span class="col-data">'.$row->reg_regno.'</span>',
					
					'<span class="col-data">'.$row->reg_mobile.'</span>',
					
					"<a class='link' href='".base_url()."admin/doc_password/".$row->reg_id."'>Edit Password</a>" 					
					
				);
				
				$i++;
	        }
	$attributes=array("target"=>"_blank");
	
			echo form_open('admin/submit',$attributes); 
	// below line added by preeti on 4th mar 14
			
			echo  $this->pagination->create_links();	
			
			?>
			
			<!-- below div added by preeti on 4th mar 14 -->
			
			<div class="clear"></div>
	
			<?php
	
			echo $this->table->generate();
	 ?>
	  
	  <table>
	  	
	  	<tr>
	  		<td><input type="submit" name="sbm"  value="Print all" /></td>
	  		<td><input type="submit" name="sbm" id="pn" value="Print all Docs" /></td>
	 	
	 		<td><input type="submit" name="sbm" id="dz"  value="Download Zip" /></td>
	 		
	 		<td><input type="submit" name="sbm" id="pp" value="Print Preview" /></td> 
	 		
	 		<td><input type="submit" name="sbm" id="gp" value="Generate Pdf" /></td>
	 		
	 		<td><!--<input type="submit" name="sbm" id="del" value="Delete" />--></td>
	 		
	 	</tr>
	 	
	 </table> 				
	
				<?php
	
				echo  $this->pagination->create_links();			
				
				echo form_close();
		}
				  
		?>         
 
        </div>     </div>

    </div>

  
  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#start_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#end_date').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>