 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme">
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script>


<style>
.buttonx{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
	text-shadow:none;
	text-decoration:none;
	border: medium none;
    color: #fff;
}
.phead
{
	font-weight:bold;
	font-size:13px;
}
</style>



<div id="fullright">


      <div class="contentsquaresmall">


		  	
          	<h1>Offer Letter</h1>     	
          
		  <?php if($errmsg!='')
		  {
		  ?>
		  <p align="center" style="color:#FF0000; font-weight:bold">
		  <?php
		  echo strtoupper($errmsg);
		  
		  ?>
		  </p>
		  <?php
		  }
		  
		  ?>
		  
		  <h3 align="left" style="color:#FF0000;">
			<i>OFFER LETTER OF AMB FIT CANDIDATES FOR THE INTERVIEWS HELD AT INHS ASVINI, MUMBAI.</i>
		  </h3>
		  
          <p class="phead" align="center">Please enter your registration number and date of birth in the specified format and click submit to download your offer letter.</p>
          	<?php
          	
          	echo form_open('odletter/download');
					
		?>	
          <table align="center" cellspacing="5" cellpadding="5">
          <tr>
          
          <td><label><b>Registrtion number</b></label>
          </td>
          <td><input type="text" name="regno" placeholder="<?php echo date('Y') ?>XXSSCXXXX" /></td>
          
          </tr>
          
          
          <tr>
          
          <td><label><b>Date of Birth</b></label>
          </td>
          <td>
          <input type="text" readonly="readonly" name="dob" id="dob" value="" placeholder="dd/mm/yyyy" />
				
				<span  style="display: none;">
				
					<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
				</span>
         
          
         
          </td>
          
          </tr>
          <tr>
          <td>
          </td>
          <td>
          <input class="buttonx" type="submit" value="Submit" />
          </td>
          </tr>
          </table>
          <?php
		 echo form_close();
		  ?>
          
          <script type="text/javascript">

			$('#dob').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
		

</script>
          <div >
          	
          
          	
		  
		  </div>

        </div>      

    </div>

 
  <?php $this->load->view('template/footer'); ?>
