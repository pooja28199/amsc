<!DOCTYPE html>
<html>
<head>
<title>ARMY MEDICAL CORPS</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" >
<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>asset/images/amc-favicon.png"/>
<link href="<?php echo base_url(); ?>asset/css/style.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="loading" style="display:none;position:fixed;margin:0;left:auto;right:auto; height:100%; width:100%; opacity:0.7; text-align:center; background-color:#ccc; z-index:999" align="center">
<div style=" top:30%;
     left:30%;
    position:absolute; z-index:9999;opacity:0.7;">
<img src="<?php echo base_url();?>asset/images/please_wait.gif" align="center" />
</div>
</div>
<div id="main">



<!--<div id="tophead">
</div>-->
<div id="header">
        <?php $a=base_url().'/images/dgafms.gif';
        
		$b=base_url().'/images/AMC.gif'; ?>
        
        <img src="<?php echo base_url();?>asset/images/header.png" width="960" height="100%" />
        
<table width="100%" style="display:none">
<tr>
<td>
<img src="<?php echo $a;?>" width="95" />
</td>
<td align="center">
<b id="heading1">Armed Forces Medical Services</b>
<br />

<b id="heading1">सशस्त्र सेना चिकित्सा सेवा </b>

<br />
<b id="heading2">Army Medical Corps</b>
<br />

<b id="heading2"> सेना चिकित्सा कोर  </b>



</td>
<td align="right">
<img src="<?php echo $b;?>" width="95" />

</td>
</tr>
</table>
</div>



<div id="wrap">
<?php
$current_page = $this->uri->segment(2);
if($current_page=="")
$current_page = $this->uri->segment(1);
?>

<ul class="navbar">
<li <?php if( $current_page == 'index' || $current_page == '' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>">Home</a></li>
<li <?php if( $current_page == 'aboutus'  ){ echo "class='active'"; } ?> ><a  href="<?php echo base_url(); ?>aboutus.php">About Us</a></li>


           	
			<!--
			<li <?php if( $current_page == 'signup' || $current_page == 'add_doc' ){ echo "style='background-color: #787878;'";  } ?> ><a href="#">Registration</a>
            <ul>
            
            <?php
            
            $time_header = date('d-m-Y H:i:s'); // line added by preeti on 2nd jul 14
			
			if( $time_header < '04-07-2014 00:00:00' )// if block added by preeti on 2nd jul 14
			{
            
            ?>
            
            <li <?php if( $current_page == 'signup' || $current_page == 'add_doc' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/signup">New&nbsp;Registration</a></li>
            
            <?php
			}
            ?>
            
            <?php
            
            $time_conf = date('d-m-Y H:i:s'); // line added by preeti on 10th jul 14
			
			if( $time_conf < '11-07-2014 00:00:00' )// if block added by preeti on 10th jul 14
			{
            
            ?>
            
            <li <?php if( $current_page == 'confirm' || $current_page == 'validate_otp' || $current_page == 'final_saved' || $current_page == 'doc_saved' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/confirm">Confirm&nbsp;Registration</a></li>
            
            <?php
			}
            ?>
            
			</ul>
			</li>
			
			
            <li <?php if( $current_page == 'login' || $current_page == 'validate' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/login">Login</a></li>
			
			<li style="display:none;" <?php if( $current_page == 'cletter' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>cletter">Call Letter</a></li>  
            -->         

 <li  <?php if( $current_page == 'faq' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>faq">FAQ</a></li>         

 <li <?php if( $current_page == 'result' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>faq/attestation">Attestation Form</a></li>  
 
 <li  <?php if( $current_page == 'organogram' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>organogram">Organogram</a></li> 
 
 <li <?php if( $current_page == 'enquiry' ){ echo "class='active'";  } ?> ><a href="<?php echo base_url(); ?>enquiry">Feedback</a></li> 
 
 <li <?php if( $current_page == 'contactus' ){ echo "class='active'"; } ?> ><a href="<?php echo base_url(); ?>contactus">Contact Us</a></li> 
 
 <?php /* if($this->session->userdata('regno')!='' && $this->session->userdata('is_logged_in')==TRUE){ ?>  	<li>
			<a href="<?php echo base_url(); ?>doc/logout/<?php echo $this->session->userdata('random'); ?>">Log Out</a>
		</li>		
 <?php } */ ?>
 
 </ul>

 
</div>