<?php

$current_page = $this->uri->segment(2);

$res=$this->db->get('last_date');
$rows=$res->result();
$last_date=$rows[0];

?><div id="left">

<h1>Navigation</h1>
        <div id="menu">

          <ul>

            <li <?php if( $current_page == 'home' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/home">Home</a></li>
            
            <li <?php if( $current_page == 'preview' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/preview">Profile Preview</a></li>
            
			<li style="background:red;"><a target="_blank" href="<?php echo base_url(); ?>faq/attestation">Attestation Form</a></li>
			
            <li <?php if( $current_page == 'doprint' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/doprint">Generate PDF</a></li>
            
			<?php if($last_date->showcletter=='1') { ?>
			<li <?php if( $current_page == 'cletter' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>cletter">Generate Call Letter</a></li>
			<?php } ?>
			
            <li <?php if( $current_page == 'doc_password' || $current_page == 'update_doc_password' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>doc/doc_password">Change Password</a></li>
            
            <!-- below line modified by preeti on 22nd apr 14 for manual testing -->
            
            <li><a href="<?php echo base_url(); ?>doc/logout/<?php echo $this->session->userdata('random'); ?>">Log Out</a></li>

          </ul>

        </div>

      </div>		

    

