<?php

$data_left['current_page'] = $this->uri->segment(3);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Set Last Date</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin', $data_left); ?>
	
	<div id="changing">

      <div class="cksquaresmall">
      	
      	<?php
          	
          		echo form_open_multipart('admin/save_date');
					
		?>	
			
			<div class="heading">Set Last Date for Registration</div>
			
			<div class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo strip_tags( validation_errors() );
				}
				else 
				{
					echo ( $errmsg );	
				}
				
				?>
				
			</div>
						
			<table align="center" style="width:85%;" cellpadding="5">
			<tbody>
			
			<tr>				
				<th align="left" colspan="2"><span class="star">*</span>Mandatory Field</th>		
			</tr>
			
			<!-- below div added by preeti on 2nd apr 14 -->
			
			<tr>			
				<td><label>Opening Date<span class="star">*</span></label></td>			
				<td>				
					<input type="text" readonly="readonly" name="ld_start" id="ld_start" value="<?php if( $this->input->post('ld_start') != '' ){ echo $this->input->post('ld_start') ; }else{ echo $ld_start; } ?>" />
					
					<span  style="display: none;">				
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">								
					</span>									
				</td>					
			</tr>			
			
			<tr>			
				<td><label>Last Date<span class="star">*</span></label></td>			
				<td>
					<input type="text" readonly="readonly" name="ld_date" id="ld_date" value="<?php if( $this->input->post('ld_date') != '' ){ echo $this->input->post('ld_date') ; }else{ echo $ld_date; } ?>" />
					<span  style="display: none;">
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
					</span>									
				</td>					
			</tr>
			
			<tr>
			
				<td><label>Interview Center<span class="star">*</span></label></td>				
				<td>					
					<select name="icenter" id="icenter">
					<option value="">--- SELECT ---</option>
					<?php 
					
					foreach($icenters as $icentr)
					{
						if($icentr->code==$icenter)
						echo '<option value="'.$icentr->code.'" selected="selected">'.$icentr->label.'</option>';
						else
						echo '<option value="'.$icentr->code.'">'.$icentr->label.'</option>';
					}
					
					?>
					</select>									
				</td>		
			
			</tr>
			
			<tr>			
				<td><label>Last Internship Date<span class="star">*</span></label></td>			
				<td>
					
					<input type="text" readonly="readonly" name="lintern_date" id="lintern_date" value="<?php if( $this->input->post('lintern_date') != '' ){ echo $this->input->post('lintern_date') ; }else{ echo $lintern_date; } ?>" />
										
				</td>					
			</tr>
			
			<tr>			
				<td><label>Check AGE criteria as on<span class="star">*</span></label></td>			
				<td>
					<input type="text" readonly="readonly" name="age_check" id="age_check" value="<?php if( $this->input->post('age_check') != '' ){ echo $this->input->post('age_check') ; }else{ echo $age_check; } ?>" />
				</td>					
			</tr>
		
			<tr>
				<td><label>Validity Days<span class="star">*</span></label></td>				
				<td>					
					<input type="text" <?php echo 'autocomplete="off"'; ?> name="ld_validity" id="ld_validity" value="<?php if( $this->input->post('ld_validity') != '' ){ echo $this->input->post('ld_validity') ; }else{ echo $ld_validity; } ?>" />										
				</td>					
			</tr>
	
			<tr>		
				<td><label>OTP Validity ( in Hours )<span class="star">*</span></label></td>		
				<td>
					<input type="text" <?php echo 'autocomplete="off"'; ?> name="ld_otp_hours" id="ld_otp_hours" value="<?php if( $this->input->post('ld_otp_hours') != '' ){ echo $this->input->post('ld_otp_hours') ; }else{ echo $ld_otp_hours; } ?>" />									
				</td>					
			</tr>
		
			<tr>			
				<td><label>Show New Registration<span class="star">*</span></label></td>			
				<td>
					<select name="showregister">
					<option <?php if($showregister=='0') echo "selected=selected";?> value="0">Yes</option>
					<option value="1" <?php if($showregister=='1') echo "selected=selected";?>>No</option>
					</select>								
				</td>		
			
			</tr>			

			<tr>		
				<td><label>Show Confirm<span class="star">*</span></label></td>				
				<td>
					<select name="showconfirm">
					<option <?php if($showconfirm=='0') echo "selected=selected";?> value="0">Yes</option>
					<option value="1" <?php if($showconfirm=='1') echo "selected=selected";?>>No</option>
					</select>								
				</td>					
			</tr>
			
			<tr>		
				<td><label>Show Call Letter<span class="star">*</span></label></td>				
				<td>
					<select name="showcletter">
					<option <?php if($showcletter=='0') echo "selected=selected";?> value="0">No</option>
					<option value="1" <?php if($showcletter=='1') echo "selected=selected";?>>Yes</option>
					</select>								
				</td>					
			</tr>
			
			<tr>		
				<td><label>Show Offer Letter<span class="star">*</span></label></td>				
				<td>
					<select name="showoletter">
					<option <?php if($showoletter=='0') echo "selected=selected";?> value="0">No</option>
					<option value="1" <?php if($showoletter=='1') echo "selected=selected";?>>Yes</option>
					</select>								
				</td>					
			</tr>

			<tr>			
				<td><label>Maintanance Mode<span class="star">*</span></label></td>		
				<td>
					<select name="m_mode">
					<option <?php if($m_mode=='0') echo "selected=selected";?> value="0">No</option>
					<option value="1" <?php if($m_mode=='1') echo "selected=selected";?>>Yes</option>				
					</select>								
				</td>					
			</tr>
			
			<tr>			
				<td><label>Cost<span class="star">*</span></label></td>		
				<td>
				<input type="text" name="cost" value="<?php echo $cost;?>" />									
				</td>					
			</tr>
			
			<tr>			
				<td><label>Call Letter Date<span class="star">*</span></label></td>				
				<td>					
					<input type="text" readonly="readonly" name="cletter_date" id="cletter_date" value="<?php if( $this->input->post('cletter_date') != '' ){ echo $this->input->post('cletter_date') ; }else{ echo $cletter_date; } ?>" />					
					<span  style="display: none;">					
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">									
					</span>										
				</td>					
			</tr>
         
            <tr>			
				<td><label>Confirm Reg Date<span class="star">*</span></label></td>			
				<td>				
					<input type="text" readonly="readonly" name="cnf_date" id="cnf_date" value="<?php if( $this->input->post('cnf_date') != '' ){ echo $this->input->post('cnf_date') ; }else{ echo $cnf_date; } ?>" />					
					<span  style="display: none;">					
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">									
					</span>									
				</td>					
			</tr>
			
			<tr>			
				<td><label>Interview Venue<span class="star">*</span></label>
				
				</td>			
				<td>				
					<textarea name="ivenue"><?php if( $this->input->post('ivenue') != '' ){ echo $this->input->post('ivenue') ; }else{ echo $ivenue; } ?></textarea>
					<br>
					<span style="font-size:12px">e.g Command Hospital (Eastern Command),<br>Alipore, Kolkata-700027</span>
				</td>					
			</tr>
			
			</tbody>
			</table>
			
			<div class="collect-signup">
				
				<div class="left">&nbsp;</div>
			
			<div class="right">
				
				<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
				<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
				
					<?php echo form_submit('sub', 'Submit'); ?> 
					
			</div>		
		
			</div> 
			
			
			<div class="collect-signup">
				
				<div class="error"><br /><br />Note: Last Date sets the Registration Last date <br /> Validity Days sets the number of days for the New User to Confirm Registration
					
					<br /> OTP Validity ( in Hours ) sets the validity of OTP in hours
					
				</div>		
		
			</div> 
			
			     
		
		  <?php			
				echo form_close();
          	
          	?>

        </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script type="text/javascript">

			$('#ld_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			$('#lintern_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			$('#age_check').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			//below code added by preeti on 2nd apr 14 for black-box testing 
			
			$('#ld_start').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			
			$('#cletter_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			$('#cnf_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});

</script>

</body>

</html>