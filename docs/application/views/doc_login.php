 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the password typed 
			
			var pass = $('#reg_pass').val();
			
			if( pass != '' )
			{
				// below lines modified by preeti on 28th mar 14 
			
				//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
				
				var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
				
				pass = md5( pass );
				
				var result = md5( pass + salt );
				
				// set the value of the hidden field
				
				$('#reg_pass_encode').val(result);
				
				// clear the field
				
				$('#reg_pass').val('');	
			}
			
			
		});
		
	});
	
</script>


<style>
.basic-grey .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 6px 15px;
    text-align: center;	
	cursor:pointer;
	text-shadow:none;
	text-decoration:none;
}
div.warnings{
	margin-bottom:20px;
	margin-top:20px;
	text-align:left;
}
div.warnings span{
	display:block;
	font-size: 11px;
}
.basic-grey label
{
	font-size: 14px;
}
td{width: 33.3%;}

.basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select
{
	width:100%;
	max-width: 245px;
	margin-bottom: 5px;
}

.basic-grey input[type="password"] {
    border: 1px solid #dadada;
    color: #888;
    font: 12px/12px Georgia,"Times New Roman",Times,serif;
    height: 24px;
    margin-right: 6px;
    margin-top: 2px;
    outline: 0 none;
    padding: 3px 3px 3px 5px;
	width:100%;
}
.basic-grey .link.button{
	font-size: 12px;
    padding: 5px 10px;
	display:inline-block;
	}
</style>



<div id="fullright">





<div class="regsquaresmall">

		<div class="heading">Login Page</div> <!-- Line added by preeti on 25th mar 14 -->

          <p>
          	
          	<div class="error warnings">
          		
          		<!-- below lines modified by preeti on 25th mar 14 for black-box testing  -->
          		
          		<?php 
          		
          		if( validation_errors() )
				{
					echo strip_tags( validation_errors() ) ;
				}
				else 
				{
					$explode=explode('.',$errmsg);
					
					foreach($explode as $val)
					{
						if($val)
						echo '<span>* '.$val.'.</span>';
					}
					//echo $errmsg;	
				}          		 
          		
          		?>	
          		
          		
          	</div>
          	
          	<?php
          		$attributes = array('class' => 'basic-grey');
          		echo form_open('doc/validate',$attributes);
				
			?>	
				
				<!--
					
					echo form_label('Reg. No.&nbsp;', 'reg_regno');
				
				// below code modified by preeti on 21st apr 14 for manual testing
				
				$att = array(
				
					'name' => 'reg_regno',
					
					'id' => 'reg_regno',
					
					'value' => $this->input->post('reg_regno'),
					
					'autocomplete' => 'off'
				
				);
				
				
				
				
				echo form_input( $att ); // code added by preeti on 21st apr 14 for manual testing
				
				echo form_label('Password', 'reg_pass');
				
				
				
				$pass_att = array(
				
					'name' => 'reg_pass',
					
					'id' => 'reg_pass',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 24th mar 14 
							
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Login');// code added by preeti on 26th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 26th mar 14 for black-box testing
								
				echo anchor('doc/signup', 'New Registration', array( 'class' => 'user' ));
				
				echo anchor('doc/forgot', 'Forgot Password', array( 'class' => 'pass' ));
				-->
				<?php $y=date('Y'); ?>
				
				<table style="width:100%;">
				<tr>
			
					<td>
						<label><b>Registration Number <span class="star">*</span></b></label>
					</td>
					
					<td>					
						<input type="text"   placeholder="XXXXXXSSCXXXX" <?php echo  'autocomplete="off"'; ?> name="reg_regno" id="reg_regno" value="<?php echo $this->input->post('reg_regno'); ?>" />						
					</td>
					<td>&nbsp;</td>	
				</tr>
				<tr>			
					<td>	
						<label><b>Password<span class="star">*</span></b></label>
					</td>					
					<td>						
						<input type="password" <?php echo  'autocomplete="off"'; ?> name="reg_pass" id="reg_pass" />						
					</td>
					<td>&nbsp;</td>	
				</tr>
				<tr>			
					<td>
						<label for="captcha"><b>Captcha<span class="star">*</span></b></label>
					</td>
					
					<td>
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</td>
					<td align="right"><?php echo $cap_img; ?>		</td>
	
				</tr>
				
				
				<tr>			
					
					<td>&nbsp;</td>
					<td>
						
						<input type="hidden" name="reg_pass_encode" id="reg_pass_encode" />
				
						<input type="submit" id="sub" name="sub" class="button" value="Login" />				
											
					</td>
					<td>&nbsp;</td>
				</tr>	
				
				<tr>
					<td>&nbsp;</td>
					<td colspan="2" align="center">
						
						<?php echo anchor('doc/signup', 'New Registration', array( 'class' => 'user link button' )); ?>
						
						<?php echo anchor('doc/forgot', 'Forgot Password', array( 'class' => 'pass link button' )); ?>
						
					</td>
		
				</tr>
				</table>
				<?php
					
				echo form_close();	
          	
          	?>           	 	
          	
          </p>

        </div>
		
		
</div>


 <?php $this->load->view('template/footer'); ?>


		