<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/left_user_out'); ?>


<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<style>
.regsquaresmall .left { width:140px;}
.button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
	text-shadow:none;
	text-decoration:none;
	border: medium none;
    color: #fff;
}
div.warnings{
	margin-bottom:20px;
	margin-top:20px;
	text-align:left;
}
div.warnings span{
	display:block;
	font-size: 11px;
}
.basic-grey label
{
	font-size: 14px;
}
</style>

	<div id="fullright">
		<div class="regsquaresmall">
			
		<div class="heading">Forgot Password</div>

		
			<span  >
          		
          	<?php 
          	     	
			if( validation_errors() )
			{
				echo strip_tags( validation_errors() ) ;
			}
			else 
			{
				echo strip_tags( $errmsg );	
			}			 
          	
          	?>
          	
          	</span>

          <p style="margin-top:30px;">
          	         	
          	<?php
          	
          		echo form_open('doc/send_password');
				
				?>
				
				<!--echo form_label('Email', 'reg_email');
				
				
				
				// below code added by preeti on 21st apr 14 for manual testing
				
				$arr = array(
				
				'name' => 'reg_email',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);				
				
				echo form_input( $arr ); // code added by preeti on 21st apr 14 for manual testing
				
				//echo form_input('reg_email', ''); // code commented by preeti on 21st apr 14 for manual testing
				
				echo form_submit('sub', 'Submit');-->
				
				<table align="center" cellspacing="15">
				<tbody>
				<tr>
				<th><label for="reg_email">Email<span class="star">*</span></label></th>
				<td><input type="text" name="reg_email" id="reg_email" <?php echo 'autocomplete="off"'; ?> /></td>
				</tr>
				<tr>
				<th>&nbsp;</th>
				<td><?php echo $cap_img; ?></td>
				</tr>
				<tr>
				<th><label for="captcha">Captcha<span class="star">*</span></label></th>
				<td><input type="text" autocomplete="off" name="captcha" id="captcha" value="" /></td>
				</tr>
				<tr>
				<th>&nbsp;</th>
				<td><input type="submit" class="button" value="Submit" name="sub" /></td>
				</tr>				
				</tbody>
				</table>
				
				<!--<div class="collect-signup" >
			
				<div class="left"><label for="reg_email">Email<span class="star">*</span></label></div>
				
				<div class="right" >
					
					<input type="text" name="reg_email" id="reg_email" <?php echo 'autocomplete="off"'; ?> />
					
				</div>		
				
				</div>	
				
				
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
				
				
				<div class="collect-signup">
			
					<div class="left">&nbsp;</div>
					
					<div class="right">
						
						<input type="submit" class="button" value="Submit" name="sub" />
						
					</div>
	
				</div>
				-->
							
				<?php
				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>      

      
    </div>

	
 
<?php $this->load->view('template/footer'); ?>
