<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class faq extends CI_Controller
{	

	public function index($errmsg='')
	{	
		$this->load->view('frontend/faq');
	}
	
	public function attestation($errmsg='')
	{	
		$this->load->view('frontend/attestation');
	}

}	