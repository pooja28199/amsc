<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class organogram extends CI_Controller
{	

	public function __CONSTRUCT()
	{
		parent::__CONSTRUCT();
	}
	 
	public function index($errmsg='')
	{	
		
		$data=array();
		$data['errmsg'] = urldecode( $errmsg );		
		$this->load->view('frontend/organogram',$data);	
				
	}
		
}	