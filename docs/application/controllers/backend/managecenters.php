<?php

class Managecenters extends CI_Controller
{

public $vjdb;

public function __CONSTRUCT()
{	
	parent::__CONSTRUCT();
	$this->load->model('backend/managecenters_model');
	$this->load->model('cletter_model');
	$this->load->library('vj'); 
	$this->vjdb=$this->vj->loadclass('db'); 
	
	/* if( !$this->check_session() )
	{
		redirect('admin/index');		
		exit;
	} */
	
}

public function index($errmsg='')
{
	$data=array(); 
	$data['errmsg'] = urldecode( $errmsg );	
	$data['records']=$this->managecenters_model->getCenters(); 
	$this->load->view('backend/managecenters/default',$data);
}

public function addCenter()
{	
	$text=$this->input->post('text');
	$code=$this->input->post('code');
	$this->managecenters_model->addCenter($text,strtoupper($code));
}

public function deleteCenter()
{	
	$id=$this->input->post('id');
	$this->managecenters_model->deleteCenter($id);
}

}


?>
