<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	
class cletter extends CI_Controller
{	

	public function __CONSTRUCT()
	{
		
		
		parent::__CONSTRUCT();
		
		
			

		$this->load->model('cletter_model');
		$this->load->model('doc_model');
	
    
		

	}
	 
	 
 
	
	public function index($errmsg='')
	{	
		
	$last_date=$this->doc_model->get_last_date();
	$cletter=$last_date->showcletter;	
	
	if($cletter==0)
	redirect('/');

	$data=array();
    $data['errmsg'] = urldecode( $errmsg );
	
	$this->load->view('cletter',$data);	
		
		
	}
	public function download()
	{
		
	$data['errmsg']  = '';
	$last_date=$this->doc_model->get_last_date();
	$icenter=$last_date->icenter;
	
	$this->form_validation->set_rules('regno', 'Registration Number', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		
		if($this->form_validation->run()==TRUE)
		{
		$regno=$this->input->post('regno');
		$dob=$this->input->post('dob');
		
		$u_dob=calen_to_db($dob);
		$query = $this->db->query("SELECT user.* FROM user JOIN register ON user.u_regno=register.reg_regno WHERE user.u_regno='$regno' AND user.u_dob='$u_dob' AND register.icenter='$icenter' AND register.reg_status='c' AND register.pay_status=1");
		$result=$query->result();
		
		if($query->num_rows==0)
		{
			$data['errmsg']="Please Enter the same Registration Number & Date of Birth as mentioned in the form.";
			$errmsg = $data['errmsg'];		
			$errmsg = urlencode( $errmsg );
			redirect('cletter/index/'.$errmsg);
		}
		
		
		$res = $this->cletter_model->generatecall($regno,$dob);
		if($res)
		{
		
		redirect('cletter/index/abc');
		}
		else
		{
		$data['errmsg']="Either your registration number and date of birth do not match or your application has awaiting approval (Due to non receipt of demand draft). Please try again later.";
		$errmsg = $data['errmsg'];		
		$errmsg = urlencode( $errmsg );
		redirect('cletter/index/'.$errmsg);
		}
		
		}
		else
		{
		$data['errmsg']="REGISTRTION NUMBER AND DATE OF BIRTH FIELD IS REQUIRED.";
		$errmsg = $data['errmsg'];		
		$errmsg = urlencode( $errmsg );
		redirect('cletter/index/'.$errmsg);
		}
		
		
		
	}
	
	
}	