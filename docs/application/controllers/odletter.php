<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	
class odletter extends CI_Controller
{	

	public function __CONSTRUCT()
	{
		
		
		parent::__CONSTRUCT();
		
		
			

		$this->load->model('odletter_model');
		$this->load->model('doc_model');
	
    
		

	}
	 
	 
 
	
	public function index($errmsg='')
	{	
	
	$last_date=$this->doc_model->get_last_date();
	$oletter=$last_date->showoletter;	
	
	if($oletter==0)
	redirect('/');
	
	$data=array();
    $data['errmsg'] = urldecode( $errmsg );
	
	$this->load->view('odletter',$data);	
		
		
	}
	public function download()
	{
		
	$data['errmsg']  = '';


	$this->form_validation->set_rules('regno', 'Registration Number', 'required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'required');
		
		if($this->form_validation->run()==TRUE)
		{
		$regno=$this->input->post('regno');
		$dob=$this->input->post('dob');
		

		$res = $this->odletter_model->generatecall($regno,$dob);
		if($res)
		{
		
		redirect('odletter/index/abc');
		}
		else
		{
		$data['errmsg']="Either your registration number and date of birth do not match or your application has awaiting approval (Due to non receipt of demand draft). Please try again later.";
		$errmsg = $data['errmsg'];		
		$errmsg = urlencode( $errmsg );
		redirect('odletter/index/'.$errmsg);
		}
		
		}
		else
		{
		$data['errmsg']="REGISTRTION NUMBER AND DATE OF BIRTH FIELD IS REQUIRED.";
		$errmsg = $data['errmsg'];		
		$errmsg = urlencode( $errmsg );
		redirect('odletter/index/'.$errmsg);
		}
		
		
		
	}
	
	
}	