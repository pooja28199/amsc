<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doc extends CI_Controller
{
	//code added by Pavan on 31-03-2014
	function stopXSS() 
	{
		// prevent XSS on $_GET, $_POST and $_COOKIE
		foreach ($_GET as $gkey => &$gval) {
		if(is_array($gval)) { // allow for checkboxes!
		foreach ($gval as $gkey2 => &$gval2) {
		$gval2 = htmlspecialchars($gval2);
		}
		} else {
		$gval = htmlspecialchars($gval);
		}
		}
		foreach ($_POST as $pkey => &$pval) {
		if(is_array($pval)) { // allow for checkboxes!
		foreach ($pval as $pkey2 => &$pval2) {
		$pval2 = htmlspecialchars($pval2);
		}
		} else {
		$pval = htmlspecialchars($pval);
		}
		}
		foreach ($_COOKIE as $ckey => &$cval) {
		$cval = htmlspecialchars($cval);
		}
	}
	public function __CONSTRUCT()
	{
		parent::__CONSTRUCT();
		
		$this->load->model('doc_model');
		
		// below code added by preeti on 25th mar 14 for black-box testing 
		
		$this->load->helper('my_helper');
    	
    	no_cache();
		
		
		// below code written by preeti on 31st mar 14 for testing
		
		/*echo "<pre>";
		
		print_r( $this->session->all_userdata() );
		
		echo "</pre>";*/
			
	}
	 
	// parameter added by preeti on 25th apr 14 
	 
	public function login( $errmsg = '' )
	{
		//$errmsg = '';// commented by preeti on 25th apr 14 
		
		if( !($this->check_session()) )
		{
			//$data['errmsg'] = '';// commented by preeti on 25th apr 14 
			
			// added by preeti on 24th apr 14 for manual testing		
			
			if( $errmsg )
			{
				$errmsg = urldecode( $errmsg );			
			}	
			
			$data['errmsg'] = $errmsg;// added by preeti on 25th apr 14
			
			$data['salt'] = $this->random_num(); // code added by preeti on 28th mar 14 for black-box testing
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
							
			$sess_arr = array(
							
				'user_salt' => $data['salt'] );
							
				$this->session->set_userdata( $sess_arr );
									
			// end, code added by preeti on 22nd apr 14 for manual testing	
			
			
			
			
			// start, code added by preeti on 21st apr 14 for manual testing
			
			
			$this->load->helper('captcha'); 
			
			$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     //'img_path' => FCPATH.'captcha/', // above commented and this added by preeti on 13th may 14
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		    $cap = create_captcha($vals);
				
		  		
		    /*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	); */  // commented by preeti on 22nd apr 14 for manual testing
				
			// start, added by preeti on 22nd apr 14 for manual testing	
				
			$data['captcha_time'] = $cap['time'];
			
			$data['ip_address'] = $this->input->ip_address();
			
			$data['word'] = $cap['word'];	
		  		
			// end, added by preeti on 22nd apr 14 for manual testing	
				
		  	$this->session->set_userdata($data);
		  		
		  	$data['cap_img']=$cap['image'];	
			
			
			// end, code added by preeti on 21st apr 14 for manual testing
						
			$this->load->view('doc_login', $data);	
		}
		else 
		{
			$this->home();	
		}			
		
	}
	
	public function index()
	{		
		$errmsg = '';
		
		if( !($this->check_session()) )
		{
			$data = array('errmsg' => '');	
			
			$data['record'] = $this->doc_model->get_all_not(); // contains all rows 
			$data['vno']=$this->doc_model->get_visitor();
			$this->load->view('doc_all_not', $data);	
		}
		else 
		{
			$this->home();	
		}			
		
	}
	
	public function not( )
	{
		$errmsg = '';
		
		$not_id = $this->uri->segment(3);
		
		if( !($this->check_session()) && $not_id != ''  )
		{
			$data = array('errmsg' => '');	
			
			$data['record'] = $this->doc_model->get_not_detail( $not_id );
			
			$this->load->view('doc_not', $data);	
		}
		else 
		{
			$this->home();	
		}			
		
	}
	
	/*public function signup()
	{
		// get all the qualification values from the DB
		
		$data = array();
		
		$data['errmsg'] = '';
		
		$data['qual_records'] = $this->doc_model->get_qual();
		
		$data['stream_records'] = $this->doc_model->get_stream();
		
		$data['college_records'] = $this->doc_model->get_college();
		
		$this->load->view('doc_signup', $data);	
		
	}*/
	
	// below function commented by preeti on 21st apr 14 as it is not being used anywhere
	
	/*public function edit()
	{
		if( !$this->check_session() )
		{
			redirect('doc/index');
			
			exit;
		}		
			
		// get all the qualification values from the DB
		
		$data = array();
		
		$data['errmsg'] = '';
		
		$data['doc_id'] = $this->session->userdata('doc_id');
		
		$data['record'] = $this->doc_model->get_detail( $data['doc_id'] );
		
		$data['qual_records'] = $this->doc_model->get_qual();
		
		$data['stream_records'] = $this->doc_model->get_stream();
		
		$data['college_records'] = $this->doc_model->get_college();
		
		$this->load->view('doc_edit', $data);	
		
	}*/		

	public function home()
	{
		if( $this->check_session() )
		{
			$data = array();
			
			//$name = $this->session->userdata('name');
			
			$regno = $this->session->userdata('regno'); // above line commented and line added by preeti on 20th feb 14
			
			$name = $this->doc_model->get_user_name( $regno ); // line added by preeti on 20th feb 14
			
			$data['name'] = strtoupper( $name ); 		 
						
			$this->load->view('doc_home', $data);	
		}
		else 
		{
			redirect('doc/index');
		}			
		
	}
	
	/*public function validate()
	{
		$result = $this->doc_model->validate();// model has been included in the constructor function
		
		if($result)// $result holds the row object
		{
			$reg_regno = $result->reg_regno;
			
			$reg_id = $result->reg_id;
				
			$name = $result->reg_fname.' '.$result->reg_mname.' '.$result->reg_lname;			
				
			// first set the session variables and then	
				
			// go to the home page
				
			$data = array(
				
				'regno' => $reg_regno,
				
				'is_logged_in' => TRUE, 
				
				'name' => $name,
				
				'reg_id' => $reg_id,
				
				);
				
			$this->session->set_userdata( $data );
				
			redirect('doc/home');			
		}
		else 
		{
			// show the error message	
		
			$data = array('errmsg' => 'Login Credentials are Incorrect');
		
			$this->load->view('doc_login', $data);
		}		
		
	}*/ // commented by preeti on 25th mar 14 for black-box testing
	
	
	// added/modified by preeti on 25th mar 14 for black-box testing
	
	public function validate()
	{
		$this->form_validation->set_rules('reg_regno', 'Reg. No.', 'trim|required|alpha_numeric');
		
		//$this->form_validation->set_rules('reg_pass', 'Password', 'required|alpha_dash');// code commented for black-box testing by preeti on 26th mar 14
		
		$this->form_validation->set_rules('reg_pass_encode', 'Password', 'required');// code added for black-box testing by preeti on 26th mar 14
		
		// below line added by preeti on 21st apr 14 for manual testing
		
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		
		if( $this->form_validation->run() == TRUE )
		{			
			$result = $this->doc_model->validate();// model has been included in the constructor function
			
			if($result)// $result holds the row object
			{
				$reg_regno = $result->reg_regno;
				
				$reg_id = $result->reg_id;
					
				$name = $result->reg_fname.' '.$result->reg_mname.' '.$result->reg_lname;			
					
				// first set the session variables and then	
					
				// go to the home page
				
				/*$data = array(
					
					'regno' => $reg_regno,
					
					'is_logged_in' => TRUE, 
					
					'name' => $name,
					
					'reg_id' => $reg_id,
					
					);*/  // code commented by preeti on 31st mar 14 for black-box testing
					
					
				// start, code added by preeti on 31st mar 14 for black-box testing
		
				$sessid = '';
				
				while (strlen($sessid) < 32)
				{
					$sessid .= mt_rand(0, mt_getrandmax());
				}	
				
				// To make the session ID even more secure we'll combine it with the user's IP
				
				$sessid .= $this->input->ip_address();
		
				$session_id = md5(uniqid($sessid, TRUE)) ; 
				
				// end, code added by preeti on 31st mar 14 for black-box testing	
				
				$data = array(
					
					'regno' => $reg_regno,
					
					'is_logged_in' => TRUE, 
					
					'name' => $name,
					
					'reg_id' => $reg_id,
					
					'random' => $this->random_num(), // line added by preeti on 21st apr 14 for manual testing
					
					'session_id' => $session_id );	
				
				$this->session->set_userdata( $data );
				
				// end, code added by preeti on 31st mar 14 for black-box testing
				
				// below line  added by preeti on 26th apr 14	
				
				$this->doc_model->add_login( );				
						
					
				redirect('doc/home');			
			}
			else // above else commented and this added by preeti on 25th apr 14
			{
				// below line added by preeti on 26th apr 14
				
				$reg_regno = $this->input->post('reg_regno') ;
				
				$this->doc_model->add_logfail( $reg_regno );
								
				// start, code added by preeti on 21st apr 14 for manual testing				
					
				$errmsg = 'Login Credentials are Incorrect';				
				
				// below code added by preeti on 24th apr 14 for manual testing
						
				$errmsg = urlencode( $errmsg );
				
				redirect('doc/login/'.$errmsg);
				
			}	
		
		}
		/*else 
		{
			
			// start, code added by preeti on 21st apr 14 for manual testing				
				
			$this->load->helper('captcha'); 
			
			$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
			 );
		  		
		  	$cap = create_captcha($vals);				
		  		
		  	$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);
		  		
		  	$this->session->set_userdata($data);
		  		
		  	$data['cap_img']=$cap['image'];	
				
				
			// end, code added by preeti on 21st apr 14 for manual testing
				
			
			// the validation errors will appear from the view page
			
			$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
			
			
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
							
			$sess_arr = array(
							
				'user_salt' => $data['salt'] );
							
				$this->session->set_userdata( $sess_arr );
									
			// end, code added by preeti on 22nd apr 14 for manual testing	
			
			
			$this->load->view('doc_login', $data);
				
		}*/ // code commented and below added by preeti on 25th apr 14	
		else if( validation_errors( ) ) 
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('doc/login/'.$errmsg);
						
			exit;					
		}
					
		
	}


	public function validate_otp()
	{
		$this->form_validation->set_rules('reg_regno', 'Registration Number', 'trim|required|alpha_numeric|xss_clean');// code modified on 27th mar 14 by preeti for black-box testing
		
		//$this->form_validation->set_rules('reg_otp', 'OTP', 'trim|required|xss_clean'); // code commented by preeti on 26th mar 14 for black-box testing

		$this->form_validation->set_rules('reg_otp_encode', 'OTP', 'required'); // code added by preeti on 26th mar 14 for black-box testing
				
		$data['errmsg'] = '';
		
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		if( $this->form_validation->run() == TRUE )
		{						
			$reg_id = $this->doc_model->validate_otp(); // reg_id is returned from the model function
		
			if( $reg_id )
			{
				// check if the registration confirmation is happening within the allowed time limit
				
				$res_valid = $this->doc_model->check_confirm_validity( $reg_id );
				
				if( $res_valid )
				{
					// check the time of the OTP , if in the range then allow the login
					
					// else disallow
					
					$res_otp = $this->doc_model->check_otp_validity( $reg_id );
					
					if( $res_otp )
					{
						// add a record in the user table with the registration number
				
						$regno = $this->doc_model->get_regno( $reg_id );
						
						$res = $this->doc_model->save_user_reg( $regno );				
						
						// redirect to the first step screen
						
						if( $res )
						{
							// start, below code added by preeti on 4th mar 14	
								
							// set the session variable to check for is_logged_in
							
							$confirm_data = array(
                   			
                   			'is_confirm_logged'  => TRUE,
                   
                   			'confirm_regno'     => $regno,
                   			
							'confirm_regid'     => $reg_id,
							
							//'confirm_otp' => trim( $this->input->post('reg_otp') ) ); // code commented for black-box testing by preeti on 26th mar 14 
                   			
                   			'confirm_random'     => $this->random_num(), // code added by preeti on 21st apr 14 for manual testing
                  			
                   			'confirm_otp' => trim( $this->input->post('reg_otp_encode') ) ); // code added for black-box testing by preeti on 26th mar 14
                   			
							
							
							$this->session->set_userdata( $confirm_data );
							
							// end, below code added by preeti on 4th mar 14
							
							redirect('doc/signup1/'.$reg_id);	
						}		
					}
					else 
					{
						//$data['errmsg'] = " Your OTP is expired now. Regenerate the OTP to Confirm Registration ";
					
						// above code commented and below added by preeti on 25th apr 14 
					
						$errmsg = " Your OTP is expired now. Regenerate the OTP to Confirm Registration ";
					
						$errmsg = urlencode( $errmsg );
																				
						redirect('doc/confirm/'.$errmsg);
					
					}								
					
				}
				else 
				{
					
					//$data['errmsg'] = 'Your Registration Time has been exceeded.Please register again.';
					
					// above code commented and below added by preeti on 25th apr 14 
					
					$errmsg = 'Your Registration Time has been exceeded.Please register again.';
					
					$title = "Confirm Registration";
					
					$errmsg = urlencode( $errmsg );
																				
					$title = urlencode( $title );
																							
					redirect('doc/show_message_out/'.$title.'/'.$errmsg);
					
				}		
											
			}
			else 
			{
				// below text changed by preeti on 28th mar 14	
			
				//$data = array('errmsg' => 'Incorrect Username/OTP');	
				
				// above line commented and below added by preeti on 23rd apr 14 for manual testing	
				
				//$data['errmsg'] = 'Incorrect Username/OTP';	
				
				// above code commented and below added by preeti on 25th apr 14 
					
				$errmsg = 'Incorrect Username or OTP';	
					
				$errmsg = urlencode( $errmsg );
																				
				
																							
				redirect('doc/confirm/'.$errmsg);
			}
			
		}
		else if( validation_errors( ) ) // else added by preeti on 25th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('doc/confirm/'.$errmsg);
						
			exit;					
		}


	   // start, code added by preeti on 21st apr 14 for manual testing	

	   $this->load->helper('captcha'); 
			
	   $vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		$cap = create_captcha($vals);
				
		  		
		/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);*/
		  		
		// start,above line commented and below added by preeti on 23rd apr 14 for manual testing		
				
		
		$data['captcha_time'] = $cap['time'];
		
		$data['ip_address'] = $this->input->ip_address();
		     	
		$data['word'] = $cap['word'];     	
		     	
		// end,above line commented and below added by preeti on 23rd apr 14 for manual testing		
				
		$this->session->set_userdata($data);
		  		
		$data['cap_img']=$cap['image'];	


		// end, code added by preeti on 21st apr 14 for manual testing

		$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
								
		$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
		$this->session->set_userdata( $sess_arr );
										
		// end, code added by preeti on 22nd apr 14 for manual testing	
						
			
		$this->load->view('doc_confirm', $data);		
		
	}

	public function signup( )
	{
		// get the last date for form submit from the last_date table
		
		// and accordingly allow or disallow
		
		// start, code added by preeti on 3rd apr 14
		
		$this->load->helper('captcha'); 
		
		$vals = array(
	     
	     'img_path' => './captcha/',
	     
	     'img_url' => base_url().'captcha/'
	     
		 );
  		
  		$cap = create_captcha($vals);
  		
  		$data = array(
     	
     	'captcha_time' => $cap['time'],
     	
     	'ip_address' => $this->input->ip_address(),
     	
     	'word' => $cap['word']
     	);
  		
  		$this->session->set_userdata($data);
  		
  		$data['cap_img']=$cap['image'];
  
  		// end, code added by preeti on 3rd apr 14
		
		
		
		
		$ld_date = $this->doc_model->get_form_last_date();
		
		$ld_start = $this->doc_model->get_form_start_date();// line added by preeti on 21st may 14
		
		$current_date = date('Y-m-d');
		
		//if( $ld_date < $current_date )
		if( $ld_date < $current_date || $ld_start > $current_date ) // above line commented and this line added by preeti on 21st may 14
		{
			// below code added / modified by preeti on 21st may 14	
							
			if( $ld_start > $current_date )
			{
				$title = "Registration not yet started";
			
				$errmsg = "The Registration process is not open yet. Please refer to the advertisement for details";
				
			}					
			
			
			if( $ld_date < $current_date )
			{
				$title = "Last Date is Over";
			
				$errmsg = "The Last Date for filling up of the form is over.";
				
			}
			
			$errmsg = urlencode( $errmsg );
																				
			$title = urlencode( $title );
																				
			redirect('doc/show_message_out/'.$title.'/'.$errmsg);
					
			
		}
		else 
		{
			
			$arr = $this->uri->uri_to_assoc(3);
			
			$fname = urldecode( $arr['fname'] );
			
			$mname = urldecode( $arr['mname'] );
			
			$lname = urldecode( $arr['lname'] );
			
			$email = urldecode( $arr['email'] );
			
			$mobile = urldecode( $arr['mobile'] );
			
			$errmsg = urldecode( $arr['errmsg'] );
				
			
			//$data['errmsg'] = '';
			
			$data['errmsg'] = $errmsg; // above line commented and this added by preeti on 28th apr 14
		
			// start, added by preeti on 28th apr 14
						
			$data['reg_fname'] = $fname; 
			
			$data['reg_mname'] = $mname;
			
			$data['reg_lname'] = $lname;
			
			$data['reg_mobile'] = $mobile;
			
			$data['reg_email'] = $email;
			
			
			
			// end, added by preeti on 28th apr 14
		
			// if there is a data saved in the user table corresponding to this regno
			// then show that data prefilled in the form
			
			//$regno = $this->doc_model->get_regno( $reg_id );
			
			//$data['record'] = $this->doc_model->show_user_data1( $regno ); 
			
			$this->load->view('doc_signup', $data);	
			
		}
		
					
	}
	
	
	// below function added by preeti on 4th mar 14
	
	public function signup1( $reg_id )
	{
		
		// check for the session variable value 
		
		if( $this->session->userdata('is_confirm_logged') == TRUE 
		
		&& $this->session->userdata('confirm_regno') != '' 
		
		&& $this->session->userdata('confirm_otp') != ''  
		
		&& $this->session->userdata('confirm_random') != '' )// confirm_random condition added by preeti on 21st apr 14
		{
			// check for the regno and otp if set in the session variable
			
			// if set check if the same session value matches the db value 
			
			//for the corresponding reg id and must have the reg_status incomplete(i)
		
		
			$res_reg = $this->doc_model->get_reg_data( $reg_id );
			
			// verify the otp by combining with salt and match with the session value
			
			if( $res_reg )
			{				
				if( $res_reg->reg_status == 'i' )
				{
					// below  code commented by preeti on 28th mar 14 for black-box testing
					
					/*if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $res_reg->reg_otp == $this->session->userdata('confirm_otp') )*/
					
					// start, code added by preeti on 28th mar 14 for black-box testing
					
					$en_pass = $this->session->userdata('confirm_otp');
					
					$salt = $this->session->userdata('confirm_salt');
					
					$result_pass = md5( $res_reg->reg_otp.$salt  );
					
					if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $result_pass == $this->session->userdata('confirm_otp') )// end, code added by preeti on 28th mar 14 for black-box testing
					{
						$data['reg_id'] = $reg_id;
			
						$data['errmsg'] = '';
						
						// if there is a data saved in the user table corresponding to this regno
						// then show that data prefilled in the form
						
						$regno = $this->doc_model->get_regno( $reg_id );
						
						$data['record'] = $this->doc_model->show_user_data1( $regno ); 
						
						// below line added by preeti on 21st apr 14 for manual testing
						
						$data['confirm_random'] = $this->session->userdata( 'confirm_random' );
						
						// start, code added by preeti on 23rd apr 14
						
						$sequence_token = $this->random_num();
						
						$sess_arr = array(
						
						'sequence_token' => $sequence_token
						
						);
						
						$data['sequence_token'] = $sequence_token;// the value to be set in a hidden field
						
						$this->session->set_userdata( $sess_arr );
						
						// end, code added by preeti on 23rd apr 14
						
						$this->load->view('doc_signup1', $data);			
					}
					else // else block added by preeti on 23rd apr 14 
					{
						// clear all session related vars and redirect 
						
						// unset the session variable values for registration no. and the OTP
			
						$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
						);
				
						$this->session->unset_userdata($array_items);
						
						$this->session->sess_destroy();	// added by preeti on 24th apr 14 for manual testing				
						
						
						redirect('doc/confirm');
					}
					
				}
				else if( $res_reg->reg_status == 'c' )
				{
					$data['errmsg'] = 'Your Registration is complete';
					
					// start, code added by preeti on 21st apr 14 for manual testing
					
					$this->load->helper('captcha'); 
			
					$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			  		$cap = create_captcha($vals);
					
			  		
			  		$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			  		$this->session->set_userdata($data);
			  		
			  		$data['cap_img']=$cap['image'];	
						
					
					// end, code added by preeti on 21st apr 14 for manual testing
				
					$data['salt'] = $this->random_num();	// code added by preeti on 22nd apr 14 for manual testing	
			
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					// add the same value in the session variable
									
					$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
					// end, code added by preeti on 22nd apr 14 for manual testing	
					
				
					$this->load->view('doc_confirm', $data);
				}
			
			}
			else 
			{			
				redirect('doc/confirm');
			}	
			 
		}
		else 
		{
			// start, code added by preeti on 21st apr 14 for manual testing
					
			/*$this->load->helper('captcha'); 
			
			$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					);
			  		
		    $cap = create_captcha($vals);
			
			$data['captcha_time'] = $cap['time'];
			
			$data['ip_address'] = $this->input->ip_address();
			
			$data['word'] = $cap['word'];     
			  		
			$this->session->set_userdata($data);
			  		
			$data['cap_img']=$cap['image'];	
						
					
			// end, code added by preeti on 21st apr 14 for manual testing
				
			
			
			$data['errmsg'] = 'Please fill this form and submit';
			
			$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
								
			$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
			// end, code added by preeti on 22nd apr 14 for manual testing	
						
			
			$this->load->view('doc_confirm', $data);*/	
			
			// above code commented and below added by preeti on 21st may 14
			
			$errmsg = 'Please fill this form and submit';
			
			$errmsg = urlencode( $errmsg );
			
			redirect('doc/confirm/'.$errmsg);
		}
		
					
	}
	
	public function check_session()
	{
		if(  $this->session->userdata('regno') != ''  && $this->session->userdata('is_logged_in') == TRUE )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;	
		}
	}
	
	function randomPrefix() 
	{ 
	
		/*$random= "";
	
		srand((double)microtime()*1000000);
	
		$data = "AbcDE123IJKLMN67QRSTUVWXYZ"; 
	
		$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz"; 
		
		$data .= "0FGH45OP89";
	
		for($i = 0; $i < $length; $i++) 
		{ 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}*/
		
		$random = substr(md5(rand()), 0, 6);
	
		return $random; 
	}
	
	function generate_regnum() 
	{
		// get the last insert id from regnum table
		
		// if no record found then start with 1001 and add that in the db
		
		$res = $this->doc_model->get_last_regnum();		
		
		if( $res )
		{
			$regnum = $res;
			
			$regnum = (int) $res + 1;
		}
		else 
		{
			$regnum = '1001';		
		}
			
		$this->doc_model->add_regnum( $regnum );// add in the db the last used regnum
		
		// create the regnum string , format is like this YYYYMMSSC1001 , mm added on 17th feb 14
		
		$regnum_str = date('Y').date('m').'SSC'.$regnum;
		
		return $regnum_str;
		
	} 
	
	
	// below code added on 27th mar 14 by preeti for black-box testing
	
	function alpha_dash_space($str) // only for alpha and space
	{
	    if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alphabet characters.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	
	// below code added on 28th apr 14 by preeti for black-box testing
	
	function alpha_num_dash($str) // for alpha-numeric with space, dash, underscore, forward slash
	{
	    //if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    if( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_num_dash', 'The %s field may only contain valid characters like alpha-numeric, dash, underscore, slash etc.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	
	 // below code added by preeti on 3rd apr 14
	 	
	 public function check_captcha()
	 {
	  	
		  $expiration = time()-7200; // Two hour limit
		  
		  $cap=$this->input->post('captcha');
		  
		  if( strtoupper( $this->session->userdata('word') ) == strtoupper( $cap ) 
		  
		   AND $this->session->userdata('ip_address')== $this->input->ip_address()
		  
		   AND $this->session->userdata('captcha_time')> $expiration)
		  {
		  		  
		   	return true;
		  
		  }
		  
		  else
		  {  
			   $this->form_validation->set_message('check_captcha', 'Security number does not match.');
			  
			   return false;
		  }
	 
	 }
	
	
	public function add_doc()
	{
		$errmsg = '';
		
		// below lines modified by preeti on 27th mar 14, for black-box testing 
		
		//$this->form_validation->set_rules('reg_fname', 'First Name', 'trim|required|alpha|xss_clean');// code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('reg_fname', 'First Name', 'trim|required|callback_alpha_dash_space');// code added on 27th mar 14 by preeti for black-box testing
		
		if( $this->input->post('reg_mname') != '' )
		{
			$this->form_validation->set_rules('reg_mname', 'Middle Name', 'trim|alpha|callback_alpha_dash_space');
		}
		
		 
		// below lines modified by preeti on 2nd apr 14
		
		if( $this->input->post('reg_lname') != '' )
		{
			$this->form_validation->set_rules('reg_lname', 'Last Name', 'trim|required|alpha|callback_alpha_dash_space');
		}
		
		// below  line modified by preeti on 2nd apr 14 for black-box testing
		
		$this->form_validation->set_rules('reg_mobile', 'Mobile', 'trim|required|integer|max_length[10]|min_length[10]|xss_clean');
		
		$this->form_validation->set_rules('reg_email', 'Email', 'trim|required|valid_email|xss_clean');

		// below line added by preeti on 3rd apr 14		
		
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		
		if( $this->form_validation->run() == TRUE )
		{
			$data = array();
		
			$is_valid = true; // to check mobile and email validity
			
			if( ! $this->doc_model->check_duplicate_mobile() )
			{
				$is_valid = FALSE;	
					
				$errmsg .= " Mobile already exist <br />";	
			}
					
			if( ! $this->doc_model->check_duplicate_email() )
			{
				$is_valid = FALSE;					
					
				$errmsg .= " Email already exist <br />";	
			}					
					
			if( $is_valid )
			{
				$reg_id = $this->doc_model->insert_doc( ); // this returns the reg_id
				
				//$reg_id_len = strlen( $reg_id );
				
				//$otp_len = 12 - (int) $reg_id_len;// length of prefix for OTP number
				
				$reg_regno = $this->generate_regnum();
				
				$reg_otp = $this->randomPrefix(  );
				
				$reg_otp = $reg_otp.$reg_id;
				
				// get the otp validity in hours from the last_date table
		
				$otp_hours = '8'; // default value 		
		
				if( $this->doc_model->get_otp_hours())
				{
					$otp_hours = $this->doc_model->get_otp_hours(); 
				}
										
				$res = $this->doc_model->update_otp( $reg_id, $reg_regno, $reg_otp, $otp_hours );
					
				if( $res )
				{
					// send sms with the token to the mobile specified
							
					$msg = "Hello, your Reg No. is ".$reg_regno." and OTP is ".$reg_otp." and it is valid for ".$otp_hours." hours. you can also check your details on registered email address."; // line edited by rahul on 18th june 2014.
							
					$msg_encoded = urlencode($msg);
							
					$dest_num = $this->input->post('reg_mobile');
											
					//$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_encoded."&mnumber=".$dest_num."&signature=NICSUP";

					// 23rd may 14
					
					$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_encoded."&mnumber=".$dest_num."&signature=NICSUP";
					
					
		
					//$res_sms = file_get_contents( $url ); 					 
						
					$res_sms = $this->send_sms_curl( $msg_encoded, $dest_num );	//23rd may 14
						
						
					if( $res_sms )
					{
						// send an email with the link to proceed further
						
						// below line added by preeti on 25th apr 14 for manual testing
						
						$msg = "Hello, your Reg No. is ".$reg_regno." and OTP is ".$reg_otp." and it is valid for ".$otp_hours." hours."; // line edited by Rahul on 18th june 2014.
						
						$msg .= "Your login details are also sent to your Registered Mobile.";
						
						$msg .= " You can confirm your registration by visiting this link - ".base_url().'doc/confirm'; 
							
						$reg_email = $this->input->post('reg_email');
								
						$config = array(
			
								'protocol' => 'smtp',
									
								'smtp_host' => 'relay.nic.in',
									
								'smtp_port' => 25,
									
								'mailtype' => 'html',
				        
			        			'newline' => '\r\n',
				        
			        			'charset' => 'utf-8' //default charset				
								
						); 
							
						/*$config = Array(
		    
				    'protocol' => 'smtp',
				    
				    'smtp_host' => 'ssl://smtp.googlemail.com',
				    
				    'smtp_port' => 465,
				    
				    'smtp_user' => 'rahejapreetiemail@gmail.com',
				    
				    'smtp_pass' => '',
				    
				    'mailtype'  => 'html', 
				    
				    'charset'   => 'iso-8859-1'
					
					);*/ // commented by preeti on 16th may 14		
								
						$this->load->library('email', $config);
								
						$this->email->set_newline("\r\n");
						
						// get the admin email id to set the from email id
						
						$admin_email = $this->doc_model->get_admin_email();
						
						if( !$admin_email || $admin_email == '' )
						{
							$admin_email = "dirafmsp-mod@nic.in";// mail id changed by preeti on 16th may 14	
						}
							
						$this->email->from($admin_email, 'AMC');
								
						$this->email->to( $reg_email );		
								
						$this->email->subject('Registration OTP');
								
						$this->email->message($msg);
								
						if($this->email->send()) 
						{
							
							// get the validity days from last_date table
							
							$res_ld = $this->doc_model->get_last_date();
									
							$ld_date = $res_ld->ld_date; // last date of registration
							
							$ld_validity = $res_ld->ld_validity; // no. of days from today to complete registration		
									
							$errmsg = " Please check your Registered Mobile and email address for the Registration number and OTP. To complete your registration follow the Confirm Registration link. You must complete your registration within ". // line edited by rahul on 18th june 2014.
							
							//$ld_validity." days or before ".db_to_calen( $ld_date ) ." whichever is earlier ";	
							
							$ld_validity." days or before ".str_replace('/', '-', db_to_calen( $ld_date ) ) ." whichever is earlier ";
						
							/*$data['errmsg'] = $errmsg;
							
							$this->load->view('doc_success', $data);
							
							return;	*/	
							
							// above lines commented and below lines added by preeti on 25th apr 14 for manual testing
			
			
							$title = "New Registration";
							
							$errmsg = urlencode( $errmsg );
																	
																								
							$title = urlencode( $title );
							
							redirect('doc/show_message_out/'.$title.'/'.$errmsg);					
								
						}
						else 
						{
							$errmsg = "Registration Link Couldnot be sent on Email. ";
							
							// below code added by preeti on 25th apr 14 for manual testing
						
							$errmsg = urlencode( $errmsg );
							
							//redirect('doc/signup/'.$errmsg);
							
							redirect('doc/signup/errmsg/'.$errmsg); // above code commented and this added by preeti on 13th may 14
							
						}														
								
					}
					else 
					{
						$errmsg = "Token Couldnot be sent on Mobile.";
						
						// below line to be remove din the production server. added by preeti on 15th may 14
						
						//$errmsg .= "Hello, your Reg No. is ".$reg_regno." and OTP is ".$reg_otp." and it is valid for ".$otp_hours." hours.Please save it somewhere";
						
						// below code added by preeti on 25th apr 14 for manual testing
						
						$errmsg = urlencode( $errmsg );
							
						//redirect('doc/signup/'.$errmsg);
						
						redirect('doc/signup/errmsg/'.$errmsg); // above code commented and this added by preeti on 13th may 14
					}									
													
				}
				else 
				{
					$errmsg = " Error in registration ";
					
					// below code added by preeti on 25th apr 14 for manual testing
						
					$errmsg = urlencode( $errmsg );
							
					//redirect('doc/signup/'.$errmsg);
					
					redirect('doc/signup/errmsg/'.$errmsg); // above code commented and this added by preeti on 13th may 14
				}
							
			}
			else // else added by preeti  on 11th apr 14
			{
				$this->load->helper('captcha'); 
			
				$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		  		$cap = create_captcha($vals);
				
		  		
		  		$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);
		  		
		  		$this->session->set_userdata($data);
		  		
		  		$data['cap_img']=$cap['image'];	
			}				
			
		}
		/*else // code added by preeti on 3rd apr 14
		{
			// start, code added by preeti on 3rd apr 14
		
			$this->load->helper('captcha'); 
			
			$vals = array(
		     
		     'img_path' => './captcha/',
		     
		     'img_url' => base_url().'captcha/'
		     
			 );
	  		
	  		$cap = create_captcha($vals);
			
	  		
	  		$data = array(
	     	
	     	'captcha_time' => $cap['time'],
	     	
	     	'ip_address' => $this->input->ip_address(),
	     	
	     	'word' => $cap['word']
	     	);
	  		
	  		$this->session->set_userdata($data);
	  		
	  		$data['cap_img']=$cap['image'];		
	  
	  		// end, code added by preeti on 3rd apr 14
				
		}*/ // commented by preeti on 28th apr 14	
		else if( validation_errors( ) ) // else added by preeti on 28th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			//redirect('doc/signup/'.$errmsg);
			
			$url = '';
			
			if( $this->input->post('reg_fname') != ''  )
			{
				$url .= '/fname/'.urlencode( $this->input->post('reg_fname') );
			}
			
			if( $this->input->post('reg_mname') != ''  )
			{
				$url .= '/mname/'.urlencode( $this->input->post('reg_mname') );
			}
			
			if( $this->input->post('reg_lname') != ''  )
			{
				$url .= '/lname/'.urlencode( $this->input->post('reg_lname') );
			}
			
			
			if( $this->input->post('reg_mobile') != ''  )
			{
				$url .= '/mobile/'.urlencode( $this->input->post('reg_mobile') );
			}
			
			
			if( $this->input->post('reg_email') != ''  )
			{
				$url .= '/email/'.urlencode( $this->input->post('reg_email') );
			}
			
			
			if( $errmsg != ''  )
			{
				$url .= '/errmsg/'.$errmsg;
			}
			
			
			redirect('doc/signup'.$url);
						
			exit;					
		}
		$data['errmsg'] = $errmsg;
		
		$this->load->view('doc_signup', $data);
	}
	
	

	public function logout()
	{
		// below if added by preeti on 22nd apr 14 for manual testing
		
		if( $this->uri->segment(3) == $this->session->userdata('random') )
		{			
			// below code added by preeti on 26th apr 14
		
			$this->doc_model->add_logout( );
		
			$data = array(
				
				'regno' => '',
				
				'random' => '', // added by preeti on 21st apr 14 for manual testing
				
				'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
				
				'user_salt' => '', // added by preeti on 22nd apr 14 for manual testing
				
				'is_logged_in' => '');
				
				$this->session->unset_userdata($data);
				
				// delet cookie ci_session
				
				// line added by preeti on 22nd apr 14 for manual testing
				
				setcookie( "ci_session", '', time()-(60*60*24*30) );
				
			
			// start, code added by preeti on 31st mar 14 for black-box testing
			
			$sessid = '';
			
			while (strlen($sessid) < 32)
			{
				$sessid .= mt_rand(0, mt_getrandmax());
			}	
			
			// To make the session ID even more secure we'll combine it with the user's IP
			$sessid .= $this->input->ip_address();
	
			$data = array(
								'session_id'	=> md5(uniqid($sessid, TRUE))							
								); 
			
			$this->session->set_userdata($data);
			
			// end, code added by preeti on 31st mar 14 for black-box testing
				
			//redirect(base_url()); // line commented by preeti on 25th mar 14 for black-box testing
				
			// below code added by preeti on 25th mar 14 for black-box testing
			
			$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing
			
			redirect( 'doc/login' ); 
				
			exit;
		}	
		
	}
	
	
	
	
	
	// function to update the values in the DB from the edit form
	
	public function update_doc()
	{
		if( !$this->check_session() )
		{
			redirect('doc/index');
			
			exit;
		}		
			
		
		$errmsg = '';
		
		$this->form_validation->set_rules('doc_fname', 'First Name', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('doc_lname', 'Last Name', 'trim|required|xss_clean'); 
		
		$this->form_validation->set_rules('doc_mobile', 'Mobile', 'trim|required|integer|max_length[10]|xss_clean');
		
		$this->form_validation->set_rules('doc_email', 'Email', 'trim|required|valid_email|xss_clean');
		
		$this->form_validation->set_rules('doc_dob', 'Date Of Birth', 'required');
		
		$this->form_validation->set_rules('doc_qual', 'Qualification', 'required');
		
		$this->form_validation->set_rules('doc_stream', 'Specialization', 'required');
		
		$this->form_validation->set_rules('doc_college', 'College', 'required');
		
		if( $this->form_validation->run() == TRUE )
		{
			
			$data = array();
		
			$photo_file_name = '';
			
			$resume_file_name = '';
			
			$previous_photo = ''; // in order to delete the previous files
		
			$previous_resume = '';// in order to delete the previous files
			
			$doc_id = $this->session->userdata('doc_id');
			
			$this->load->library('upload');
			
			
			if( isset($_FILES['doc_photo']) && $_FILES['doc_photo']['size'] > 0  )
			{
				
				$config['upload_path'] = './uploads/photo/';
				
				$config['allowed_types'] = 'gif|jpeg|jpg|png';
				
				$config['max_size'] = '100'; // in kb
				
				$config['min_width'] = '48'; // in pixel = 2 inch (as median)
				
				$config['max_width'] = '144'; // in pixel = 2 inch (as median)
				
				$config['min_height'] = '48'; // in pixel = 1 inch (as median)
				
				$config['max_height'] = '144'; // in pixel = 1 inch (as median)
				
				
			
				
				
				$this->upload->initialize($config);
				
				if(! $this->upload->do_upload('doc_photo') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$photo_upload_arr = $this->upload->data();
			
					$photo_file_name = $photo_upload_arr['file_name'];
				}				
			
			}
			
			
			if( isset( $_FILES['doc_resume'] ) && $_FILES['doc_resume']['size'] > 0   )
			{
				$config['upload_path'] = './uploads/resume/';
			
				$config['allowed_types'] = 'doc|docx|pdf';
				
				$config['max_size'] = '100'; // in kb
				
				$config['max_width'] = '0'; // in pixel
				
				$config['max_height'] = '0'; // in pixel
				
				$this->upload->initialize($config);
				
				
				if(! $this->upload->do_upload('doc_resume') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$resume_upload_arr = $this->upload->data();
					
					$resume_file_name = $resume_upload_arr['file_name'];
				}	
				
			}
			
			if( $photo_file_name !='' )
			{
					// delete the previous photo from the uploads/photo folder
					
					$previous_photo = $this->doc_model->get_item_name( $doc_id, 'photo' );// get the previous file name
									
			}
				
			if( $resume_file_name !='' )
			{
					// delete the previous resume from the uploads/resume folder
					
					$previous_resume = $this->doc_model->get_item_name( $doc_id, 'resume' );// get the previous file name
									
			}

			// shoot an update query here
			
			$res = $this->doc_model->update_doc( $doc_id, $photo_file_name, $resume_file_name );	
			
			if($res)
			{
				// delete the previous file names from the folders of photo as well as resume
				
				
				if( $photo_file_name !='' )
				{
					// delete the previous photo from the uploads/photo folder
					
					$photo_url = "uploads/photo/".$previous_photo;
					
					if(file_exists($photo_url))
					{
						unlink($photo_url);
					}
					
				}
				
				if( $resume_file_name !='' )
				{
					// delete the previous resume from the uploads/resume folder
					
					$resume_url = "uploads/resume/".$previous_resume;
					
					if(file_exists($resume_url))
					{
						unlink($resume_url);
					}
					
				}
				
					
		
				$errmsg = "Profile Updated Successfully ";
			}
			else 
			{
				$errmsg = "Profile couldn't be Updated";
			}			
			
		}
		
		
		$data['errmsg'] = $errmsg;
		
		$data['doc_id'] = $this->session->userdata('doc_id');
		
		$data['record'] = $this->doc_model->get_detail( $data['doc_id'] );
		
		$data['qual_records'] = $this->doc_model->get_qual();
		
		$data['stream_records'] = $this->doc_model->get_stream();
		
		$data['college_records'] = $this->doc_model->get_college();
		
		$this->load->view('doc_edit', $data);
	}

	public function get_pdf_html( $res , $where = '' ) // parameter added by preeti on 13th may 14
	{
		//$image_src = base_url()."uploads/photo/".$res->u_photo;
		
		// above line commented and below lines added by preeti on 13th may 14
		
		if( $where == 'pdf' )
		{
			$image_src = FCPATH."uploads/photo/".$res->u_photo; 	
		}
		else 
		{
			$image_src = base_url()."uploads/photo/".$res->u_photo; 	
		}
		
		
		
		// .image-col property modified by preeti on 25th feb 14
		
		$html = "<style>
		
		.pdf{height:auto;width:100%;font-size:10px;}
		
		.left-col{float:left;width:40%;}
		
		.right-col{float:left;width:40%;}
		
		.image-col{float:right;width:20%; }
		#profimg{position:absolute;right:0;}
		
		</style>
		
		<div class='pdf'>
		
		<h3>Form Details</h3>
		
		<div class='left-col' >Registration No.</div>
		
		<div class='right-col' >".$res->reg_regno."</div>
		
		<div class='image-col' id='profimg' ><img src='".$image_src."' /></div>
		
		
		<div class='left-col' >Registration Date</div>
		
		<div class='right-col' >".db_to_calen( $res->u_updatedon )."</div>";
		
		if( $res->u_is_name_change == 'y' )
		{
			$html .= "<div class='left-col' >Name</div>
				
			<div class='right-col' >".strtoupper( $res->new_fname.' '.$res->new_mname.' '.$res->new_lname ) ."</div>";		
		}
		else 
		{
			$html .= "<div class='left-col' >Name (as in Matric Certificate)</div>
		
			<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";
		}
		
		$html .= "<div class='left-col' >Name (in Hindi)</div>
		
		<div class='right-col' >".$res->u_hindi_name."</div>
		
		
		
		<div class='left-col' >Name Changed</div>
				
		<div class='right-col' >".( ($res->u_is_name_change == 'y')?'YES':'NO' )."</div>";
				
			
		if( $res->u_is_name_change == 'y' )
		{
				
				$html .= "<div class='left-col' >Old Name (as in Matric Certificate)</div>
				
				<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";		
				
		}
			
			if( $res->u_sex == 'm' )
			{
				$gender = 'MALE';
			}
			else
			{
				$gender = 'FEMALE';
			} 
			
			$html .= "<div class='left-col' >Gender</div>
				
				<div class='right-col' >".$gender."</div>";	
			
			$u_dob = $res->u_dob;
						
			
			$html .= " <div class='left-col' >Date Of Birth</div>
				
				<div class='right-col' >".db_to_calen( $u_dob ) ."</div>";	
			
			$html .= " <div class='left-col' >Age (As on 31st Dec)</div>
				
				<div class='right-col' >".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</div>";
			
			
			// below lines added by preeti on 19th feb 14
			
			$html .= " <div class='left-col' >Father's Name</div>
				
				<div class='right-col' >".strtoupper( $res->u_father_fname.' '.$res->u_father_mname.' '.$res->u_father_lname )."</div>";
			
			$html .= " <div class='left-col' >Mobile</div>
				
				<div class='right-col' >".$res->reg_mobile."</div>";
				
			$email = str_replace('@', '[at]', $res->reg_email);// code added by preeti on 25th mar 14 for black-box testing	
			
			$email = str_replace('.', '[dot]', $email);// code added by preeti on 25th mar 14 for black-box testing
			
			$html .= "<div class='left-col' >Email</div>
				
				<div class='right-col' >".strtoupper( $email ) ."</div>";	// code modified by preeti on 25th mar 14 for black-box testing		
		
			$html .= " <div class='left-col' >Nationality</div>
				
				<div class='right-col' >".strtoupper( $res->u_nationality ) ."</div>";
			
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
	
						
			
			$html .= "<div class='left-col' >Are you Married ?</div>
				
				<div class='right-col' >".$u_is_married."</div>";
			
			if( $res->u_is_married == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Marriage</div>
				
				<div class='right-col' >".db_to_calen( $res->sp_mar_date ) ."</div>";
			
				
				$html .="<div class='left-col' >Spouse Name</div>
				
				<div class='right-col' >".strtoupper( $res->sp_name ) ."</div>";
				
				
				$html .= "<div class='left-col' >Occupation</div>
				
				<div class='right-col' >".strtoupper( $res->sp_occup ) ."</div>";
				
				if( $res->sp_nation == 'f' )
				{
					$sp_nation = 'FOREIGN';
				}
				else
				{
					$sp_nation = 'INDIAN';
				}	
				
				$html .= "<div class='left-col' >Nationality</div>
				
				<div class='right-col' >".$sp_nation ."</div>";
				
				if( $res->sp_citizenship_date != '0000-00-00' )
				{
				
					$html .= "<div class='left-col' >Citizenship acquiring Date(if foreigner)</div>
					
					<div class='right-col' >".db_to_calen( $res->sp_citizenship_date )."</div>";
				
				}
				
				if( $res->sp_ssc_applied == 'y' )
				{
					$sp_ssc_applied = 'YES';
				}
				else
				{
					$sp_ssc_applied = 'NO';
				}	
				
				$html .= "<div class='left-col' >Applied for grant of SSC in AMC</div>
					
					<div class='right-col' >".$sp_ssc_applied."</div>					
					";	
				
			}

			if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIA';
			}
			
			$html .= "<div class='left-col' >Graduated From</div>
					
					<div class='right-col' >".$u_grad_from."</div>";					
					
					$html .= "<div class='left-col' >PMRC Medical Registration Number</div>
					
					<div class='right-col' >".strtoupper( $res->u_pmrc_num )."</div>";
					
					$html .= "<div class='left-col' >Date of PMRC Registration</div>
					
					<div class='right-col' >". db_to_calen( $res->u_pmrc_reg_date ) ."</div>";
					
					
					$html .= "<div class='left-col' >PMRC Issuing Office</div>
					
					<div class='right-col' >". strtoupper( $res->u_pmrc_issue_off ) ."</div>";
					
					
					$html .= "<div class='left-col' >Basic Educational Qualification</div>
					
					<div class='right-col' >".strtoupper( $res->u_basic_qual )."</div>					
					";	
					
				
					$html .= "<div class='left-col' >Date of Admission to MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_adm_date ) ."</div>";
				
					// below line modified by preeti on 25th feb 14, text corrected
				
					$html .= "<div class='left-col' >Date Of Passing MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_passing_date ) ."</div>";
					
					// below lines modified by preeti on 25th feb 14
				
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part I)</div>
					
					<div class='right-col' >". $res->u_num_attempt ."</div>";
					
					// below lines added by preeti on 25th feb 14
					
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part II)</div>
					
					<div class='right-col' >". $res->u_num_attempt1 ."</div>";
				
					$html .= "<div class='left-col' >MBBS College/University</div>
					
					<div class='right-col' >". strtoupper( $res->u_college.' / '.$res->u_univ ) ."</div>";
					
					
					if( $res->u_intern_complete == 'y' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date ) ;
					
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >YES</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date</div>
					
						<div class='right-col' >".$intern_date."</div>";
					}
					else if( $res->u_intern_complete == 'n' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date_likely ) ;
						
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >NO</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date (Likely)</div>
					
						<div class='right-col' >".$intern_date."</div>";
					}
					
					if( $res->u_college_recog == 'y' )
					{
						$u_college_recog = 'YES';
					}
					else
					{
						$u_college_recog = 'NO';
					}
			
					
					$html .= "<div class='left-col' >MBBS college Recg. by MCI</div>
					
					<div class='right-col' >". $u_college_recog ."</div>
					
					";
			
			if( $res->u_is_pg == 'y' )
			{
				$u_is_pg = 'YES';
			}
			else
			{
				$u_is_pg = 'NO';
			}
			
			
			$html .= "<div class='left-col' >Are you a Post Graduate ?</div>
					
					<div class='right-col' >". $u_is_pg ."</div> ";
					
			if( $res->u_is_pg == 'y' )
			{
				
				$regno = $res->u_regno; // line added by preeti on 25th feb 14
		
				// get post_grad details
				
				$res_pg = $this->doc_model->get_pg_data( $regno );
				
				$pg_qual = '';
				
				$pg_arr = array();
				
				if( $res_pg )
				{
					if( is_array( $res_pg ) && COUNT( $res_pg ) > 0 )
					{
						// below lines added by preeti on 27th feb 14
							
						$pg_cnt = '';
						
						if(COUNT( $res_pg ) > 1)
						{
							$pg_cnt = 1;
						}
						
						foreach( $res_pg as $val )
						{
							$html .= "<div class='left-col' >PG Qualification$pg_cnt (Subject) </div>
					
							<div class='right-col' >". strtoupper( $val->pg_degree.'('.$val->pg_subject.')') ."</div>
					
							";
					
							$html .= "<div class='left-col' >College / University </div>
					
							<div class='right-col' >". strtoupper( $val->pg_college.'/'.$val->pg_univ ) ."</div>
					
							";
					
							$html .= "<div class='left-col' >Year Of Passing</div>
					
							<div class='right-col' >". $val->pg_year ."</div>
					
							";
					
							if( $val->pg_mci_recog == 'y' )
							{
								$pg_mci_recog = 'YES';
							}
							else
							{
								$pg_mci_recog = 'NO';
							}	
					
							$html .= "<div class='left-col' >PG College Recognized by MCI</div>
								
								<div class='right-col' >". $pg_mci_recog ."</div>
								
								";
								
							if( is_numeric( $pg_cnt ) )
							{
								$pg_cnt++;
							}		
								
								
						}
									
									
					}
				}				
							
			}
					
			$html .= "<div class='left-col' >DD Number</div>
					
					<div class='right-col' >". $res->u_dd_num ."</div>
					
					";	
			
			
			$html .= "<div class='left-col' >DD Date</div>
					
					<div class='right-col' >". db_to_calen( $res->u_dd_date )  ."</div>
					
					";	
			
			$html .= "<div class='left-col' >Payable Bank</div>
					
					<div class='right-col' >". strtoupper( $res->u_dd_bank )  ."</div>
					
					";
					
			if( $res->u_current_emp == 'y' )
			{
				$u_current_emp = 'YES';
			}
			else
			{
				$u_current_emp = 'NO';
			}		
			
			$html .= "<div class='left-col' >Are you Employed ?</div>
					
					<div class='right-col' >". $u_current_emp  ."</div>
					
					";
					
			if( $res->u_current_emp == 'y'  )
			{
				$html .= "<tr>
				
					<div class='left-col' >Current Employer</div>
					
					<div class='right-col' >".strtoupper( $res->ce_employer )  ."</div>
					
					";
			}
			
			// below if else added by preeti on 3rd mar 14
			
			if( $res->u_ncc_cert == '' )
			{
				$u_ncc_cert = 'NONE';
			}		
			else
			{
				$u_ncc_cert = strtoupper( $res->u_ncc_cert );
			}
			
			// below code modified by preeti on 3rd mar 14		
			
			$html .= "<div class='left-col' >NCC Training Certificate</div>
					
					<div class='right-col' >". $u_ncc_cert  ."</div>
					
					";
			
			$html .= "<div class='left-col' >Hobbies</div>
					
					<div class='right-col' >". strtoupper( $res->u_hobbies )  ."</div>
					
					";
					
			if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >First Preference of Service</div>
					
					<div class='right-col' >". $u_pref1  ."</div>
					
					";
			
			
			
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Second Preference of Service</div>
					
					<div class='right-col' >". $u_pref2  ."</div>
					
					";
			
			
			if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Third Preference of Service</div>
					
					<div class='right-col' >". $u_pref3  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				$u_previous_ssc = 'YES';
			}
			else
			{
				$u_previous_ssc = 'NO';
			}		
					
			$html .= "<div class='left-col' >Have you worked in AMC service before ? </div>
					
					<div class='right-col' >". $u_previous_ssc  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Commission</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_com_date )  ."</div>
					
					";
				
				$html .= "<div class='left-col' >Date Of Release</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_rel_date )  ."</div>
					
					";
				
			}			
			
			$state_post = $this->doc_model->get_state_name( $res->post_state );
			
			$state_perm = $this->doc_model->get_state_name( $res->perm_state );		
									
			
			/*$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. 'Tel(Landline) - '.$res->post_tel) ."</div>
					
					";				
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. 'Tel(Landline) - '. $res->perm_tel ) ."</div>
					 
					";	*/ //lines commented by preeti on 28th feb 14		 
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->post_tel != '' )
			{
				$post_tel = $res->post_tel;	
			}
			else 
			{
				$post_tel = 'NONE';
			}
			
			
			$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. ' Alt. Tel - '.$post_tel) ."</div>
					
					";									
			
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->perm_tel != '' )
			{
				$perm_tel = $res->perm_tel;	
			}
			else 
			{
				$perm_tel = 'NONE';
			}
			
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. ' Alt. Tel - '. $perm_tel ) ."</div>
					
					";	
					
			//$u_sign_url = base_url().'uploads/sign/'.$res->u_sign;		
				
			
			// above line commented and below lines added by preeti on 13th may 14
		
			if( $where == 'pdf' )
			{
				$u_sign_url = FCPATH.'uploads/sign/'.$res->u_sign;	
			}
			else 
			{
				$u_sign_url = base_url().'uploads/sign/'.$res->u_sign; 	
			}		
				
					
			$html .= "<div class='left-col' >Declaration</div>
					
					<div class='right-col' >I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.</div>
					
					<div class='image-col' ><img src='".$u_sign_url."' /></div>
					
					";		
		
		$html .= "</div>
		
		";
		
		return $html;
	}
	

	public function doprint()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		
		$regno = $this->session->userdata('regno');   
		    
		$res = $this->doc_model->get_user_detail( $regno );   
		 
		//$image_src = base_url()."/uploads/photo/".$res->u_photo; // commented by preeti on 19th feb 14
		    
		$html = $this->get_pdf_html( $res, 'pdf' ); // second parameter added by preeti on 13th may 14
			
		$this->_gen_pdf($html);                  
    }
	
	private function _gen_pdf($html,$paper='A4')
    {
        $this->load->library('mpdf/mpdf');                
        
        //$mpdf=new mPDF('utf-8',$paper);
        
        $mpdf=new mPDF('hi',$paper); // for hindi characters
        
        $mpdf->WriteHTML($html);
        
        $mpdf->Output('profile.pdf', 'D');
    }
    
    public function preview()
	{
		if( $this->check_session() )
		{
			$data = array();
			
			$regno = $this->session->userdata('regno');   
		    
		 	$res = $this->doc_model->get_user_detail( $regno );   
		 
		 	$html = $this->get_pdf_html( $res );		 	
		 	
		 	$data['html'] = $html;			
						
			$this->load->view('doc_preview', $data);	
		}
		else 
		{
			redirect('doc/index');
		}	
		
	}
	
	public function create_otp()
	{
		$errmsg = '';
		
		// below line modified by preeti on 25th mar 14 for black-box testing
		
		$this->form_validation->set_rules('reg_regno', 'Registration No.', 'trim|required|alpha_numeric|xss_clean');
		
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		
		if( $this->form_validation->run() == TRUE )
		{
			
			// first check in the register table if this registration number exists
			
			// if not then show the message registration no. doesnt exist
			
			$reg_regno = $this->input->post('reg_regno');	
			
			$reg_id = $this->doc_model->check_regno( $reg_regno );// it returns the reg_id
			
			if( $reg_id )
			{
				// if it exists then check the date i.e reg_otp_start if it is in this day only
			
				$res_today = $this->doc_model->check_today_otp( $reg_regno );
			
				// then disallow and show a message saying you can regenerate tomorrow
			
				// else generate the new otp and send it to the mobile and email
				
				if( $res_today )
				{
					//$reg_id_len = strlen( $reg_id );
					
					//$otp_len = 12 - (int) $reg_id_len;// length of prefix for OTP number
					
					$reg_otp = $this->randomPrefix(  );
					
					$reg_otp = $reg_otp.$reg_id;
					
					// get the otp validity in hours from the last_date table
		
					$otp_hours = '8'; // default value 		
		
					if( $this->doc_model->get_otp_hours())
					{
						$otp_hours = $this->doc_model->get_otp_hours(); 
					}
										
					$res = $this->doc_model->update_only_otp( $reg_id, $reg_otp, $otp_hours );
					
					if( $res )
					{
						$msg = "Hello, your new OTP is ".$reg_otp." and it is valid for ".$otp_hours." hours. you can also check your details on registered email address."; // line edited by rahul on 18th june 2014.
							
						$encoded_msg = urlencode($msg);
						
						$obj = $this->doc_model->get_detail( $reg_id );
						
						
						
						$dest_num = $obj->reg_mobile;
							
						//$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$encoded_msg."&mnumber=".$dest_num."&signature=NICSUP";
	
						// 23rd may 14
					
						$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$encoded_msg."&mnumber=".$dest_num."&signature=NICSUP";
							
						//$res_sms = file_get_contents( $url );
						
						$res_sms = $this->send_sms_curl( $encoded_msg, $dest_num );	//23rd may 14
						
						$reg_email = $obj->reg_email;
								
						$config = array(
			
								'protocol' => 'smtp',
									
								'smtp_host' => 'relay.nic.in',
									
								'smtp_port' => 25,
									
								'mailtype' => 'html',
				        
			        			'newline' => '\r\n',
				        
			        			'charset' => 'utf-8' //default charset				
								
							);
							
						/*$config = Array(
		    
				    'protocol' => 'smtp',
				    
				    'smtp_host' => 'ssl://smtp.googlemail.com',
				    
				    'smtp_port' => 465,
				    
				    'smtp_user' => 'rahejapreetiemail@gmail.com',
				    
				    'smtp_pass' => '',
				    
				    'mailtype'  => 'html', 
				    
				    'charset'   => 'iso-8859-1'
					
					);	*/ // commented by preeti on 16th apr 14	
								
						$this->load->library('email', $config);
								
						$this->email->set_newline("\r\n");
						
						// get the admin email id to set the from email id
						
						$admin_email = $this->doc_model->get_admin_email();
												
						if( !$admin_email || $admin_email == '' )
						{
							$admin_email = "dirafmsp-mod@nic.in";// mail changed by preeti on 16th may 14	
						}
								
						$this->email->from($admin_email, 'AMC');
								
						$this->email->to( $reg_email );		
								
						$this->email->subject('Registration OTP');
						
						// below line added by preeti on 25th apr 14 for manual testing
						
						$msg = "Hello, your new OTP is ".$reg_otp." and it is valid for ".$otp_hours." hours "; // line edited by Rahul on 18th june 2014.
						
						$msg .= " and the new OTP has also been sent to your Registered Mobile and it is valid for ".$otp_hours." hours ";
								
						$this->email->message($msg);
								
						$res_email = $this->email->send(); 
						
						if( $res_sms || $res_email )
						{
							$errmsg = " Your New OTP has been sent to your Registered ";
							
							if( $res_sms )
							{
								$errmsg .= " Mobile";
								$errmsg .= " and Email Address"; // line added by rahul on 18th june 2014.	
							}
							
							/*if( $res_email )
							{
								$errmsg .= " Email";	
							}*/ // line commented by preeti on 25th apr 14 for manual testing
							
							// below code added by preeti on 26th mar 14 for black-box testing
							
							// below code is to be removed when in production
							
							//$errmsg .= ". Your OTP is ".$reg_otp; 
							
							// below code added by preeti on 22nd apr 14 for manual testing
									
							$errmsg = urlencode( $errmsg );
																				
							$title = urlencode( "New OTP" );
																				
							redirect('doc/show_message_out/'.$title.'/'.$errmsg);
							
							 
						}						
						 
					}				
				
				}
				else 
				{
					$errmsg = "You can generate an OTP only once a day";
					
					// below code added by preeti on 22nd apr 14 for manual testing
									
					$errmsg = urlencode( $errmsg );
																				
					$title = urlencode( "New OTP" );
																				
					redirect('doc/show_message_out/'.$title.'/'.$errmsg);
				}	
					
			}
			else 
			{
				$errmsg = "This Registration No. doesnt exist";
				
				// below code added by preeti on 22nd apr 14 for manual testing
									
				$errmsg = urlencode( $errmsg );
																				
				$title = urlencode( "New OTP" );
																				
				redirect('doc/show_message_out/'.$title.'/'.$errmsg);
			}			
			
			  
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('doc/new_otp/'.$errmsg);
						
			exit;					
		}
		

		// below captcha code added by preeti on 21st apr 14 for manual testing
		
		$this->load->helper('captcha'); 
			
		$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		$cap = create_captcha($vals);
				
		  		
		$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);
		  		
		$this->session->set_userdata($data);
		  		
		$data['cap_img']=$cap['image'];	
		
		
		
		$data['errmsg'] = $errmsg;	
			
		$this->load->view('doc_otp', $data);
			
	} 
	
	// below function commented by preeti on 21st apr 14 for manual testing
	
	/*public function new_otp()
	{
		$data['errmsg'] = '';	
			
		$this->load->view('doc_otp', $data);
				
	}*/
	
	// below function commented by preeti on 21st apr 14 for manual testing
	
	public function new_otp( $errmsg = '' )
	{
		$this->load->helper('captcha'); 
			
		$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		$cap = create_captcha($vals);
				
		  		
		/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);*/ 
		     	
		// above code commented and below added by preeti on 25th apr 14     	
		  		
		$data['captcha_time'] = $cap['time'];
		
		$data['ip_address'] = $this->input->ip_address();     	
		     	
		$data['word'] = $cap['word'];		     	
		     	
		$this->session->set_userdata($data);
		  		
		$data['cap_img'] = $cap['image'];		
		
		//$data['errmsg'] = '';	
			
		// above code commented and below added by preeti on 25th apr 14	
			
		// added by preeti on 24th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );			
		}	
			
		$data['errmsg'] = $errmsg;	
			
		$this->load->view('doc_otp', $data);
				
	}
	
	// parameter added by preeti on 25th apr 14
	
	public function forgot( $errmsg = '' )
	{
		// start , code added by preeti on 21st apr 14 for manual testing
		
		$this->load->helper('captcha'); 
			
		$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		$cap = create_captcha($vals);
				
		  		
		/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);*/ 
				
		// above code commented and below code added by preeti on 25th apr 14
		
		$data['captcha_time'] = $cap['time'];
		     	
		$data['ip_address'] = $this->input->ip_address();
		     	
		$data['word'] = $cap['word'];		
		  		
		$this->session->set_userdata($data);
		  		
		$data['cap_img']=$cap['image'];	
		
		// end , code added by preeti on 21st apr 14 for manual testing
		
		//$data['errmsg'] = '';	// commented by preeti on 25th apr 14
			
		// below code added by preeti on 25th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );			
		}	
			
		$data['errmsg'] = $errmsg;	
			
		$this->load->view('doc_forgot', $data);
				
	}
	
	public function token()
	{
		$data['errmsg'] = '';	
			
		$this->load->view('doc_token', $data);
				
	}
	
	public function send_password()
	{
		// this function will change the password 
		//and send the updated password
		
		$data['errmsg'] = '';
		
		$this->form_validation->set_rules('reg_email', 'Email', 'trim|required|valid_email|xss_clean');

		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		if( $this->form_validation->run() == TRUE )
		{			
			$reg_email = $this->input->post('reg_email');	
				
			$reg_id = $this->doc_model->check_email(); // function returns the doc_id
			
			if($reg_id) // email id found
			{
				// update the password and return
				
				$new_pass = $this->doc_model->new_password( $reg_id );
				
				if( $new_pass )
				{
					// start, code added by preeti on 25th apr 14	
											
					// send the password as sms on the mobile					
					
					$msg = "Your new password is : ".$new_pass;	

					$msg_mobile = urlencode($msg);
								
					$dest_num = $this->doc_model->get_mobile( $reg_id );
												
					//$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_mobile."&mnumber=".$dest_num."&signature=NICSUP";
	
					// 23rd may 14
					
					$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_mobile."&mnumber=".$dest_num."&signature=NICSUP";
						
					//$res_sms = file_get_contents( $url );
					
					$res_sms = $this->send_sms_curl( $msg_mobile, $dest_num );	//23rd may 14
				
					// end, code added by preeti on 25th apr 14
					
					// send this new password in the email
						
					$reg_email = $this->input->post('reg_email'); 					
														
					$config = array(
			
						'protocol' => 'smtp',
						
						'smtp_host' => 'relay.nic.in',
						
						'smtp_port' => 25,
						
						'mailtype' => 'html',
	        
	        			'newline' => '\r\n',
	        
	        			'charset' => 'utf-8' //default charset				
					
					);
					
					/*$config = Array(
		    
				    'protocol' => 'smtp',
				    
				    'smtp_host' => 'ssl://smtp.googlemail.com',
				    
				    'smtp_port' => 465,
				    
				    'smtp_user' => 'rahejapreetiemail@gmail.com',
				    
				    'smtp_pass' => '',
				    
				    'mailtype'  => 'html', 
				    
				    'charset'   => 'iso-8859-1'
					
					);	*/ // commented by preeti on 16th may 14
					
					$this->load->library('email', $config);
					
					$this->email->set_newline("\r\n");
					
					// get the admin email id to set the from email id
						
					$admin_email = $this->doc_model->get_admin_email();
											
					if( !$admin_email || $admin_email == '' )
					{
						$admin_email = "dirafmsp-mod@nic.in"; // mail id changed by preeti on 16th may 14	
					}
					
					
					$this->email->from($admin_email, 'AMC');
					
					$this->email->to( $reg_email );		
					
					$this->email->subject('New Password');
					
					$data['new_pass'] = $new_pass;
					
					$msg = $this->load->view( 'email_format', $data, TRUE);
					
					$this->email->message($msg);
					
					if( $this->email->send() )
					{
						//$data['errmsg'] = 'Password sent to your mail';
						
						$data['errmsg'] = 'Password sent to your Registered Mobile';// above line commented and this line added by preeti on 25th apr 14 for manual testing
						
						// below code added by preeti on 22nd apr 14 for manual testing
						
						$errmsg = $data['errmsg'];
									
						$errmsg = urlencode( $errmsg );
																			
						$title = urlencode( "Forgot Password" );
																			
						redirect('doc/show_message_out/'.$title.'/'.$errmsg);					
						
					}		
					else
					{
						//show_error($this->email->print_debugger());
							
						$data['errmsg'] = 'Password couldnot be sent ';
						
						// below code added by preeti on 22nd apr 14 for manual testing
						
						$errmsg = $data['errmsg'];
									
						$errmsg = urlencode( $errmsg );
																			
						$title = urlencode( "Forgot Password" );
																			
						redirect('doc/show_message_out/'.$title.'/'.$errmsg);					
						
					}
											 
				}
				else 
				{
					// new password not sent
					
					$data['errmsg'] = 'Password couldnot be reset!';
					
					// below code added by preeti on 22nd apr 14 for manual testing
						
					$errmsg = $data['errmsg'];
									
					$errmsg = urlencode( $errmsg );
																			
					$title = urlencode( "Forgot Password" );
																			
					redirect('doc/show_message_out/'.$title.'/'.$errmsg);		
						
				}
				
			}
			else 
			{
				$data['errmsg'] = "Email doesn't Exist !";	
				
				// below code added by preeti on 22nd apr 14 for manual testing
						
				$errmsg = $data['errmsg'];
									
				$errmsg = urlencode( $errmsg );
																			
				$title = urlencode( "Forgot Password" );
																			
				redirect('doc/show_message_out/'.$title.'/'.$errmsg);		
			}
				
		}
		else if( validation_errors( ) ) // else added by preeti on 25th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('doc/forgot/'.$errmsg);
						
			exit;					
		}

		$this->load->helper('captcha'); 
			
		$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		$cap = create_captcha($vals);
				
		  		
		/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);*/
		
		// above code commented and below added by preeti on 25th apr 14 		
		
		$data['captcha_time'] = $cap['time'];
		
		$data['ip_address'] = $this->input->ip_address();
		
		$data['word'] = $cap['word'];
				
		  		
		$this->session->set_userdata($data);
		  		
		$data['cap_img']=$cap['image'];	// captcha code added by preeti on 21st apr 14 for manual testing		
			
		$this->load->view('doc_forgot', $data);		
				
	}

	/*public function register()
	{
		$errmsg = '';
		
		$this->form_validation->set_rules('doc_id', 'Token', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('doc_username', 'Username', 'trim|required|xss_clean');
		
		if( $this->form_validation->run() == TRUE )
		{
			$doc_id = $this->doc_model->activate( ); // token will be used to fetch the record from table doctor
					
			if( $doc_id )
			{
				$pass = 'pass'.$doc_id;
				
				// send the serial number and the password on user mobile and email
				
				// get the mobile and email from the db
				
				$result = $this->doc_model->get_detail( $doc_id );
				
				$doc_mobile = $result->doc_mobile;
				
				$doc_email = $result->doc_email;
				
				$msg = "Registration Successful. Your Serial Number is ".$doc_id." and Password is ".$pass;	

				$msg_mobile = urlencode($msg);
							
				$dest_num = $doc_mobile;
							
				$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_mobile."&mnumber=".$dest_num."&signature=NICSUP";

				//$res_sms = file_get_contents( $url );// commented for black-box testing
				
				$res_sms = TRUE;// added for black-box testing
							
				// send an email with the link to proceed further
							
				$config = array(
			
					'protocol' => 'smtp',
									
					'smtp_host' => 'relay.nic.in',
									
					'smtp_port' => 25,
									
					'mailtype' => 'html',
				        
					'newline' => '\r\n',
				        
					'charset' => 'utf-8' //default charset				
								
				);
								
				$this->load->library('email', $config);
								
				$this->email->set_newline("\r\n");
				
				// get the admin email id to set the from email id
						
				$admin_email = $this->doc_model->get_admin_email();
										
				if( !$admin_email || $admin_email == '' )
				{
					$admin_email = "preeti.nhq@nic.in";	
				}
								
				$this->email->from($admin_email, 'AMC');
								
				$this->email->to( $doc_email );		
								
				$this->email->subject('Registration Successful');
								
				$this->email->message($msg);
								
				//if($this->email->send()) // line commented for black-box testing
				
				if( TRUE )// line added for black-box testing
				{
									
					$errmsg = "Congratulations ! You have registered successfully. Please check your Email for login details";	
																	
					$data['errmsg'] = $errmsg;
							
					$this->load->view('doc_success', $data);
							
					return;		
								
				}			
		
			}
			else 
			{
				$data['errmsg'] = 'Token and Username didnt match in our records';	
			}
		}
		else
		{
			$data['errmsg'] = '';
			
			$this->load->view('doc_token', $data);		
		}
				
			
	}*/

	// parameter added by preeti on 24th apr 14 for manual testing
	
	public function doc_password( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('doc/index');
			
			exit;
		}		
			
		$regno = $this->session->userdata('regno');
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$random = $this->session->userdata('random');			
		
		// added by preeti on 24th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );			
		}
			
		//$data['errmsg'] = '';
		
		$data['errmsg'] = $errmsg; // above line commented and this line added by preeti on 24th apr 14
		
		$data['regno'] = $regno;
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['random'] = $random;	
		
		$data['salt'] = $this->random_num();// code added by preeti on 28th mar 14 for black-box testing
		
		
		// start, code added by preeti on 22nd apr 14 for manual testing
						
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'user_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
			
		$this->load->view('doc_password', $data);
	}
	
	// below function is added by preeti on 22nd apr 14 for manual testing
	
	public function show_message( $title = '',  $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('doc/index');
			
			exit;
		}				
		
		$data['title'] = urldecode( $title );
		
		$data['errmsg'] = urldecode( $errmsg );
		
		$this->load->view('doc_msg', $data);
	}
	
	// below function is added by preeti on 22nd apr 14 for manual testing
	
	public function show_message_out( $title = '',  $errmsg = '' )
	{
		$data['title'] = urldecode( $title );
		
		$data['errmsg'] = urldecode( $errmsg );
		
		$this->load->view('doc_msg', $data);
	}		
	
	public function update_doc_password()
	{
		if( !$this->check_session() )
		{
			redirect('doc/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$regno = $this->session->userdata('regno');
		
		// start, below lines modified by preeti on 26th mar 14 for black-box testing
		
		$this->form_validation->set_rules('opassword_encode', 'Old Password', 'required');
		
		$this->form_validation->set_rules('reg_pass_encode', 'Password', 'required');
		
		$this->form_validation->set_rules('cpassword_encode', 'Confirm Password', 'required|matches[reg_pass_encode]'); 
		
		// end, lines modified by preeti on 26th mar 14 for black-box testing
		
		$this->form_validation->set_message('matches', 'The two passwords do not match');
		
		if( $this->form_validation->run() == TRUE )
		{
			// below if added by preeti on 21st apr 14 for manual testing
							
			if( $this->input->post('random') != ''  && strlen( $this->input->post('random') ) == 32   
			
			&& $this->session->userdata('random') != ''   && ( $this->input->post('random') == $this->session->userdata('random') ) )  
			{
				if( $this->doc_model->check_old_password( $regno ) ) // if old password is verified
				{
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					$old_password = $this -> input -> post('opassword_encode');// hashed md5 value
							
					$new_password = $this -> input -> post('reg_pass_encode'); // only md5 value
					
					
					if( !$this->doc_model->check_old_new_pass_same( $old_password, $new_password ) )// end, code added by preeti on 22nd apr 14 for manual testing
					{
					
						// shoot an update query here
			
						$res = $this->doc_model->update_doc_password();	
						
						if($res)
						{
							// below lines added by preeti on 26th apr 14
							
							// add pass change status in login_user table
							
							$this->doc_model->add_pass( 'passchange' );
							
							$errmsg = "Password Updated Successfully ";
							
							// below code added by preeti on 22nd apr 14 for manual testing
									
							$errmsg = urlencode( $errmsg );
																				
							$title = urlencode( "Change Password" );
																				
							redirect('doc/show_message/'.$title.'/'.$errmsg);
							
						}
						else 
						{
							// below lines added by preeti on 26th apr 14
							
							// add pass change status in login_user table
							
							$this->doc_model->add_pass( 'passchangefail' );
							
							
							$errmsg = "Password couldn't be Updated";
							
							// below code added by preeti on 22nd apr 14 for manual testing
									
							$errmsg = urlencode( $errmsg );
																				
							$title = urlencode( "Change Password" );
																				
							redirect('doc/show_message/'.$title.'/'.$errmsg);
						}	
					
					}
					else 
					{
							$errmsg = "New Password cannot be same as the Old Password";
							
							// below code added by preeti on 24th apr 14 for manual testing
						
							$errmsg = urlencode( $errmsg );
													
							redirect('doc/doc_password/'.$errmsg);
							
					}	
					
				}
				else 
				{
					$errmsg = "Old Password is Incorrect ";	
					
					// below code added by preeti on 24th apr 14 for manual testing
						
					$errmsg = urlencode( $errmsg );
													
					redirect('doc/doc_password/'.$errmsg);
				}
			
			}
			else 
			{
				$errmsg = "Password couldn't be Updated";
				
				// below code added by preeti on 22nd apr 14 for manual testing
									
				$errmsg = urlencode( $errmsg );
																				
				$title = urlencode( "Change Password" );
																				
				redirect('doc/show_message/'.$title.'/'.$errmsg);
			}				
			
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('doc/doc_password/'.$errmsg);
						
			exit;					
		}
		
		$data['random'] = $this->session->userdata('random');		
		
		
		$data['errmsg'] = $errmsg;
		
		$data['regno'] = $regno;
		
		
		
		
		// start, code added by preeti on 22nd apr 14 for manual testing
		
		$data['salt'] = $this->random_num();// code added by preeti on 28th mar 14 for black-box testing
						
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'user_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		$this->load->view('doc_password', $data);
	}

	public function result( )
	{
		$errmsg = '';
		
		if( !($this->check_session()) )
		{
			$data = array('errmsg' => '');	
			
			$data['record'] = $this->doc_model->get_result();
			
			$this->load->view('doc_res', $data);	
		}
		else 
		{
			$this->home();	
		}			
		
	}
	
	// below code added by preeti on 28th mar 14 for black-box testing
	
	/*function random_num() // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, 32);
		
		return 	$random;
	}*/ // commented by preeti on 2nd apr 14
	
	
	// below function added by preeti on 2nd apr 14
	
	function random_num( $num = 32 ) // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, $num);
		
		return 	$random;
	}
	
	// below function added by preeti on 3rd apr 14
	
	function random_only_num( $length ) 
	{ 
		$random= "";
	
		srand( (double) microtime()*1000000 );
		
		$data = "0123456789";
	
		for($i = 0; $i < $length; $i++) 
		{ 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}
		
		$random = substr($random, 0, $length);
	
		return $random; 
	}
	
	
	// parameter added by preeti on 25th apr 14 
	
	public function confirm( $errmsg = '' )
	{
		// check if the user is logged in for the confirm registration send directly to step1 page
		
		
		if( $this->session->userdata('is_confirm_logged') == TRUE 
		
		&& $this->session->userdata('confirm_regno') != '' 
		
		&& $this->session->userdata('confirm_otp') != ''  )
		{
			$reg_id = $this->session->userdata('confirm_regid');
						
			redirect('doc/signup1/'.$reg_id);
		}
		else 
		{
			
			// get the last date for form submit from the last_date table
		
			// and accordingly allow or disallow
			
			$ld_date = $this->doc_model->get_form_last_date();
			
			$ld_start = $this->doc_model->get_form_start_date();// line added by preeti on 21st may 14
			
			
			$current_date = date('Y-m-d');
			
			//if( $ld_date < $current_date )
			if( $ld_date < $current_date || $ld_start > $current_date  ) // above line commented and this added by preeti on 21st may 14
			{
				/*$title = "Last Date Over";
				
				$errmsg = "The Last Date for filling up of the form is over.";*/
				
				// above line commented and below added by preeti on 21st may 14				
					
				if( $ld_start > $current_date )
				{
					$title = "Registration not yet started";
				
					$errmsg = "The Registration process is not open yet. Please refer to the advertisement for details";
					
				}					
				
				
				if( $ld_date < $current_date )
				{
					$title = "Last Date is Over";
				
					$errmsg = "The Last Date for filling up of the form is over.";
					
				}
				
				$errmsg = urlencode( $errmsg );
																				
				$title = urlencode( $title );
																				
				redirect('doc/show_message_out/'.$title.'/'.$errmsg);
				
			}
			else 
			{
				
				// start, code added by preeti on 21st apr 14 for manual testing
				
				
				$this->load->helper('captcha'); 
			
				$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		  		$cap = create_captcha($vals);
				
		  		
		  		/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     	);*/
		     	
		     	// above code commented and below added by preeti on 23rd apr 14
		     	
		     	$data['captcha_time'] = $cap['time'];
				
				$data['ip_address'] = $this->input->ip_address();
				
				$data['word'] = $cap['word'];
				
				
		  		
		  		$this->session->set_userdata($data);
		  		
		  		$data['cap_img']=$cap['image'];	
				
				
				// end, code added by preeti on 21st apr 14 for manual testing
				
				
			
				//$errmsg = ''; // commented by preeti on 24th apr 14 for manual testing		
				
				
				// added by preeti on 25th apr 14 for manual testing		
			
				if( $errmsg )
				{
					$errmsg = urldecode( $errmsg );			
				}
				
					
				//$data['errmsg'] = '';	
				
				// above line commented and below line added by preeti on 25th apr 14
								
				$data['errmsg'] = $errmsg;
				
				$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
				
				// start, code added by preeti on 22nd apr 14 for manual testing
				
				// add the same value in the session variable
								
				$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
				// end, code added by preeti on 22nd apr 14 for manual testing	
								
					
				$this->load->view('doc_confirm', $data);			
				
			}	
			
		}		
		
	}
	
	function do_upload( $file_element_name , $upload_loc )
    {
    	$config['upload_path'] = './'.$upload_loc.'/'; // path wrt root where we wish to upload our file
       
        $config['allowed_types'] = 'pdf';
       
	    $config['max_size'] = '200';// in KB
		
		/*$config['max_width']  = '189';
       
	    $config['max_height']  = '189';
		
		$config['min_width'] = '76'; // in pixel
			
		$config['min_height'] = '76'; // in pixel*/
			
		
		$status = '';
		
		$this->load->library('upload', $config);
		
		$this->upload->initialize($config);

        if ( ! $this->upload->do_upload( $file_element_name ))
        {
            $error = array('error' => $this->upload->display_errors());

            $status = $error;                        
        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
			
			$upload_arr = $this->upload->data();
					
			$file_name = $upload_arr['file_name'];

            $status = ':'.$file_name; // to retrieve the filename search for the : if its present  
        }		
		
		return $status;
    }
	
	
	function do_upload_image( $file_element_name , $upload_loc )
    {
    	$config['upload_path'] = './'.$upload_loc.'/'; // path wrt root where we wish to upload our file
       
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
       
	    $config['max_size'] = '100';// in KB
	    
	    // below lines modified by preeti on 15th may 14
       
	    $config['min_width'] = '48'; // 48 px = half inch
	    
	    $config['max_width']  = '144'; // 48 px = half inch
       
	    
	    $config['min_height'] = '48'; // 144 px = one and half inch
	    
	    $config['max_height']  = '144';// 144 px = one and half inch
		
		
			
		
		
		$status = '';
		
		$this->load->library('upload', $config);
		
		$this->upload->initialize($config);

        if ( ! $this->upload->do_upload( $file_element_name ))
        {
            $error = array('error' => $this->upload->display_errors());

            $status = $error;                        
        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
			
			$upload_arr = $this->upload->data();
					
			$file_name = $upload_arr['file_name'];

            $status = ':'.$file_name; // to retrieve the filename search for the : if its present  
        }		
		
		return $status;
    }

	function do_upload_sign( $file_element_name , $upload_loc )
    {
    	$config['upload_path'] = './'.$upload_loc.'/'; // path wrt root where we wish to upload our file
       
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
       
	    $config['max_size'] = '100';// in KB
       
		// below configs modified by preeti on 15th may 14       
       
	    $config['min_width'] = '144'; // 144 px =  1 and half inch
	    
	    $config['max_width']  = '240'; // 240 px = 2 and half inch
       
	    $config['min_height'] = '48'; // 48 px =  half inch
	   
	    $config['max_height']  = '144'; // 144 px = 1 and half inch
		
				
		$status = '';
		
		$this->load->library('upload', $config);
		
		$this->upload->initialize($config);

        if ( ! $this->upload->do_upload( $file_element_name ))
        {
            $error = array('error' => $this->upload->display_errors());

            $status = $error;                        
        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
			
			$upload_arr = $this->upload->data();
					
			$file_name = $upload_arr['file_name'];

            $status = ':'.$file_name; // to retrieve the filename search for the : if its present  
        }		
		
		return $status;
    }
	
	
	/*public function save1() // save the first form fields
	{
		// save and show the exit page
			
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$u_dob_proof_file = '';
		
		$new_name_proof_file = '';
		
		$u_photo_file = '';
			
		$this->form_validation->set_rules('u_fname', 'First Name', 'trim|required');
		
		$this->form_validation->set_rules('u_lname', 'Last Name', 'trim|required');
			
		$this->form_validation->set_rules('u_is_name_change', 'Name Changed', 'required');
		
		if( $this->input->post( 'u_is_name_change' ) == 'y' )
		{
			
			$this->form_validation->set_rules('new_fname', 'New First Name', 'trim|required');
			
			$this->form_validation->set_rules('new_lname', 'New Last Name', 'trim|required');
									
		}
		
		// order of hindi name is changed by preeti on 28th feb 14	
		
		$this->form_validation->set_rules('u_hindi_name', 'Present Name( Hindi ) ', 'trim|required');
		
		$this->form_validation->set_rules('u_sex', "Gender", 'trim|required');		
		
		$this->form_validation->set_rules('u_dob', "Date Of Birth", 'trim|required');
		
		$this->form_validation->set_rules('u_nationality', "Nationality", 'trim|required');
		
		$this->form_validation->set_rules('u_father_fname', "Father's First Name", 'trim|required');
		
		$this->form_validation->set_rules('u_father_lname', "Father's Last Name", 'trim|required');
				
		$this->form_validation->set_rules('u_is_married', "Married", 'required');
		
		if( $this->input->post( 'u_is_married' ) == 'y' )
		{
			$this->form_validation->set_rules('sp_mar_date', 'Date Of Marriage', 'trim|required');
			
			$this->form_validation->set_rules('sp_name', 'Spouse Name', 'trim|required');
			
			$this->form_validation->set_rules('sp_occup', 'Spouse Occupation', 'trim|required');
			
			$this->form_validation->set_rules('sp_nation', 'Spouse Nationality', 'trim|required');
			
			$this->form_validation->set_rules('sp_ssc_applied', 'Spouse Applied for SSC', 'required');
		}		
		
		
		if( $this->form_validation->run() == TRUE )
		{
			// mandatory uploads - photo
			
			// optional uploads - copy of new name if name changed is checked
			
			// mandatory uploads - dob certificate
			
			
			$file_element_name = "u_photo";
					
			$upload_loc = 'uploads/photo'; 
				
			$res = $this->do_upload_image( $file_element_name, $upload_loc ); 	
				
			$errmsg = $res; 
			
			if( is_array( $errmsg ))
			{
				foreach( $errmsg as $key=>$val )
				{
					$errmsg[$key] = "For the Photo : ".$val; 
				}
			}	
					
			if( !is_array( $errmsg ))
			{
				if( strpos( $errmsg, ':' ) !== FALSE )
				{
					$u_photo_file = str_replace(':', '', $errmsg);							
							
					$errmsg = ''; // all is well						
							
				}	
			}
			else
			{
				if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
				&& ($this->input->post( 'u_photo_hid' ) != '' ))					
				{
					$u_photo_file = $this->input->post( 'u_photo_hid' );
						
					$errmsg = ''; // all is well							
							
				}						
						
			}

			if( !is_array( $errmsg ) && $errmsg == '' )
			{
				if( $this->input->post( 'u_is_name_change' ) == 'y' ) // when name changed option is checked
				{
						
					$file_element_name = "new_name_proof";
						
					$upload_loc = 'uploads/new_name'; 
					
					$res = $this->do_upload( $file_element_name, $upload_loc ); 	
					
					$errmsg = $res; 
					
					if( is_array( $errmsg ))
					{
						foreach( $errmsg as $key=>$val )
						{
							$errmsg[$key] = "For the New Name Proof : ".$val; 
						}
					}		
						
					if( !is_array( $errmsg ))
					{
						if( strpos( $errmsg, ':' ) !== FALSE )
						{
							$new_name_proof_file = str_replace(':', '', $errmsg);							
								
							$errmsg = ''; // all is well						
								
						}	
					}
					else
					{
							if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'new_name_proof_hid' ) != '' ))					
							{
								$new_name_proof_file = $this->input->post( 'new_name_proof_hid' );
							
								$errmsg = ''; // all is well							
								
							}						
							
					}				
					
				}
			}

			if( !is_array( $errmsg ) && $errmsg == '' )
			{
			
				//if( $this->input->post( 'u_is_name_change' ) == 'n' ) 
				{
					// upload the dob certificate
					
					$file_element_name = "u_dob_proof";
				
					$upload_loc = 'uploads/dob'; // location to upload dob proof
					
					$res = $this->do_upload( $file_element_name, $upload_loc ); // whether there is an error or filename( for success)	
					
					$errmsg = $res; // either it will have an error message or a filename with : prefixed	
					
					if( is_array( $errmsg ))
					{
						foreach( $errmsg as $key=>$val )
						{
							$errmsg[$key] = "For the DOB Proof : ".$val; 
						}
					}
					
					if( !is_array( $errmsg )) // if there is no error
					{
						if( strpos( $errmsg, ':' ) !== FALSE )
						{
							$u_dob_proof_file = str_replace(':', '', $errmsg);	
							
							$errmsg = ''; // all is well							
									
						}	
					}	
					else // if error 
					{
						// in case of edit form, when no file is browsed
						
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
							
							&& ($this->input->post( 'u_dob_proof_hid' ) != '' ))
						{
							
								$u_dob_proof_file = $this->input->post( 'u_dob_proof_hid' );
								
								$errmsg = ''; // all is well					
						}						
			
					}				
					
				}

			}			
											
		}
		else 
		{
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			$res_ins = $this->doc_model->save_user1( $u_photo_file, $u_dob_proof_file,  $new_name_proof_file );	
							
			if( $res_ins )
			{
				// check which button was clicked
			
				$save_e = $this->input->post('save_e');
			
				$save_c = $this->input->post('save_c');
			
				if( $save_e != '' )
				{
					echo 'exit';
				}
				
				if( $save_c != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}			
		
	}*/  // code commented by preeti on 1st apr 14 for black-box testing
	
	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_dob( $str ) // age shud be in 17 - 45
	{
		
		// change the calendar format to db date format and 
		
		$cal_arr = explode('/', $str ); // date is in format dd/mm/YYYY
		
		
		
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		$date_31dec = date('Y').'-12-31'; // 31st dec of the current year
		
		$oDateNow = new DateTime( $date_31dec );
		
		$oDateBirth = new DateTime( $sDateBirth );

		$oDateIntervall = $oDateNow->diff($oDateBirth);

 		$year = $oDateIntervall->y;
		
		if( intval( $year ) >= 17 && intval( $year ) <= 45 )
	    {
	    	return TRUE;
	    }
		else 
		{
			$this->form_validation->set_message('check_dob', 'Your age must be in between 17 to 45 as on 31st Dec');
				
	    	return FALSE;
			
		}	
		
	}
	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	/*function check_dom( $str ) // marriage date should be atleast 21
	{
		
		// get the dob from dob field
		
		$dob = $this->input->post('u_dob');
	
	
		
		
		// change the calendar format to db date format and 
		
		$cal_arr = explode('/', $dob ); // date is in format dd/mm/YYYY
		
		
		
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		
		// change the calendar format to db date format and 
		
		$cal_arr = explode('/', $str ); // date is in format dd/mm/YYYY
		
		
		
		$mar_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		$date_31dec = date('Y').'-12-31'; // 31st dec of the current year
		
		$oDateNow = new DateTime( $mar_date );
		
		$oDateBirth = new DateTime( $sDateBirth );

		$oDateIntervall = $oDateNow->diff($oDateBirth);

 		$year = $oDateIntervall->y;
		
		if( intval( $year ) >= 21 )
	    {
	    	return TRUE;
	    }
		else 
		{
			$this->form_validation->set_message('check_dom', 'Your Marriage date is assumed to be equal to or greater than 21 yrs from date of birth');
				
	    	return FALSE;
			
		}	
		
	}*/ // code commented by preeti on 4th apr 14
	
	
	// code added by preeti on 4th apr 14
	
	function check_dom( $str ) // marriage date should be atleast 21
	{
		
		// get the dob from dob field
		
		$dob = $this->input->post('u_dob');
	
	
		
		
		// change the calendar format to db date format and 
		
		$cal_arr = explode('/', $dob ); // date is in format dd/mm/YYYY
		
		
		
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		
		// change the calendar format to db date format and 
		
		$cal_arr = explode('/', $str ); // date is in format dd/mm/YYYY
		
		
		
		$mar_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		$date_31dec = date('Y').'-12-31'; // 31st dec of the current year
		
		$oDateNow = new DateTime( $mar_date );
		
		$oDateBirth = new DateTime( $sDateBirth );

		$oDateIntervall = $oDateNow->diff($oDateBirth);

 		$year = $oDateIntervall->y;
		
		
		$gender = $this->input->post('u_sex'); 
		
		if( $gender == 'm' )
		{
			if( intval( $year ) >= 21 )
			{
				return TRUE;	
			}
			else 
			{
				$this->form_validation->set_message('check_dom', 'Your Marriage date is assumed to be equal to or greater than 21 yrs from date of birth');
				
	    		return FALSE;
			}
		}
		else if( $gender == 'f'  )
		{
			if( intval( $year ) >= 18 )
			{
				return TRUE;	
			}
			else 
			{
				$this->form_validation->set_message('check_dom', 'Your Marriage date is assumed to be equal to or greater than 18 yrs from date of birth');
				
	    		return FALSE;
			}
		}	
		
	}
	
	
	
	// below code added by preeti on 1st apr 14 for black-box testing
	
	public function save1() // save the first form fields
	{
		// save and show the exit page
			
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$u_dob_proof_file = '';
		
		$new_name_proof_file = '';
		
		$u_photo_file = '';
			
		$this->form_validation->set_rules('u_fname', 'First Name', 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
		
		// start, code added by preeti on 2nd apr 14 for black-box testing
		
		if(  $this->input->post( 'u_lname' ) )
		{
			$this->form_validation->set_rules('u_lname', 'Last Name', 'trim|callback_alpha_and_space');	
		}
		
		// end, code added by preeti on 2nd apr 14 for black-box testing
		
		
			
		$this->form_validation->set_rules('u_is_name_change', 'Name Changed', 'required');
		
		/*$this->form_validation->set_rules('u_pmrc_num', 'Permanent Medical Registration Number', 'required'); //line modified by Vijay Kumar on 17th Jun 14
		
		$pmrc_no = $this->input->post('u_pmrc_num'); 
		$sql = "select * from user where u_pmrc_num = '$pmrc_no' ";
			$qry = $this->db->query($sql);
			if ($query->num_rows() == 0)
			{
				$errmsg = "Duplicate PMRC Number";
				
			}       //line modified by Vijay Kumar on 17th Jun 14*/
		
		if( $this->input->post( 'u_is_name_change' ) == 'y' )
		{
			
			$this->form_validation->set_rules('new_fname', 'New First Name', 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
			
			// below code modified by preeti on 2nd apr 14 for black-box testing
			
			if( $this->input->post( 'new_lname' ) )
			{
				$this->form_validation->set_rules('new_lname', 'New Last Name', 'trim|callback_alpha_and_space');// line modified by preeti on 28th apr 14
			}		
									
		}
		
		// order of hindi name is changed by preeti on 28th feb 14	
		
		$this->form_validation->set_rules('u_hindi_name', 'Present Name( Hindi ) ', 'trim|required');
		
		$this->form_validation->set_rules('u_sex', "Gender", 'trim|required');		
		
		$this->form_validation->set_rules('u_dob', "Date Of Birth", 'trim|required|callback_check_dob');
		
		$this->form_validation->set_rules('u_nationality', "Nationality", 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
		
		$this->form_validation->set_rules('u_father_fname', "Father's First Name", 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
		
		// start, code added by preeti on 2nd apr 14 for black-box testing
		
		if( $this->input->post( 'u_father_lname' ) )
		{
			$this->form_validation->set_rules('u_father_lname', "Father's Last Name", 'trim|callback_alpha_and_space');
		}
		
		// end, code added by preeti on 2nd apr 14 for black-box testing
		
		
				
		$this->form_validation->set_rules('u_is_married', "Married", 'required');
		
		if( $this->input->post( 'u_is_married' ) == 'y' )
		{
			$this->form_validation->set_rules('sp_mar_date', 'Date Of Marriage', 'trim|required|callback_check_dom');
			
			$this->form_validation->set_rules('sp_name', 'Spouse Name', 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
			
			$this->form_validation->set_rules('sp_occup', 'Spouse Occupation', 'trim|required|callback_alpha_and_space');
			
			$this->form_validation->set_rules('sp_nation', 'Spouse Nationality', 'trim|required');
			
			// start, code added by preeti on 2nd apr 14 for black-box testing
			
			if( $this->input->post('sp_nation') == 'f' )
			{
				$this->form_validation->set_rules('sp_citizenship_date', 'Date of acquiring Indian Citizenship', 'trim|required');
			}
			
			// end, code added by preeti on 2nd apr 14 for black-box testing
			
			$this->form_validation->set_rules('sp_ssc_applied', 'Spouse Applied for SSC', 'required');
		}		
		
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('confirm_random') != ''  && strlen( $this->input->post('confirm_random') ) == 32   
			
			&& $this->session->userdata('confirm_random') != ''   && ( $this->input->post('confirm_random') == $this->session->userdata('confirm_random') ) ) 
			{			
			
			// mandatory uploads - photo
			
			// optional uploads - copy of new name if name changed is checked
			
			// mandatory uploads - dob certificate
			
			
			$file_element_name = "u_photo";
					
			$upload_loc = 'uploads/photo'; 
				
			$res = $this->do_upload_image( $file_element_name, $upload_loc ); 	
				
			$errmsg = $res; 
			
			if( is_array( $errmsg ))
			{
				foreach( $errmsg as $key=>$val )
				{
					$errmsg[$key] = "For the Photo : ".$val; 
				}
			}	
					
			if( !is_array( $errmsg ))
			{
				if( strpos( $errmsg, ':' ) !== FALSE )
				{
					$u_photo_file = str_replace(':', '', $errmsg);							
							
					$errmsg = ''; // all is well						
							
				}	
			}
			else
			{
				if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
				&& ($this->input->post( 'u_photo_hid' ) != '' ))					
				{
					$u_photo_file = $this->input->post( 'u_photo_hid' );
						
					$errmsg = ''; // all is well							
							
				}						
						
			}

			if( !is_array( $errmsg ) && $errmsg == '' )
			{
				if( $this->input->post( 'u_is_name_change' ) == 'y' ) // when name changed option is checked
				{
						
					$file_element_name = "new_name_proof";
						
					$upload_loc = 'uploads/new_name'; 
					
					$res = $this->do_upload( $file_element_name, $upload_loc ); 	
					
					$errmsg = $res; 
					
					if( is_array( $errmsg ))
					{
						foreach( $errmsg as $key=>$val )
						{
							$errmsg[$key] = "For the New Name Proof : ".$val; 
						}
					}		
						
					if( !is_array( $errmsg ))
					{
						if( strpos( $errmsg, ':' ) !== FALSE )
						{
							$new_name_proof_file = str_replace(':', '', $errmsg);
							
							
							
							// start, code added by preeti on 1st apr 14 for black-box testing
						
							// read the file and verify if it is actually a pdf file
								
							// if it is not a pdf file then delete the file and show the message as file not pdf
								
							$path = 'uploads/new_name/'.$new_name_proof_file;
								
							$this->load->helper('file');
								
							$string = read_file( $path );
						
							if( $string )
							{
								if( strpos( $string , '%PDF' ) !== FALSE )
								{
									// proceed with the saving of the data
																	
									// shoot an update query here
							
									$errmsg = ''; // all is well												
										
								}
								else 
								{
									// delete the uploaded file, dont proceed and show the error message
										
									$file_path = 'uploads/new_name/'.$new_name_proof_file;
										
									if(file_exists( $file_path )) 
									{						
										unlink( $file_path );
									}
										
									$errmsg = "For the New Name Proof : Uploaded File is not valid ";
										
								}
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/new_name/'.$new_name_proof_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the New Name Proof : Uploaded File is not valid ";		
							}						
								
								
							// end, code added by preeti on 1st apr 14 for black-box testing									
								
						}	
					}
					else
					{
							if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'new_name_proof_hid' ) != '' ))					
							{
								$new_name_proof_file = $this->input->post( 'new_name_proof_hid' );
							
								$errmsg = ''; // all is well							
								
							}						
							
					}				
					
				}
			}

			if( !is_array( $errmsg ) && $errmsg == '' )
			{
			
				//if( $this->input->post( 'u_is_name_change' ) == 'n' ) 
				{
					// upload the dob certificate
					
					$file_element_name = "u_dob_proof";
				
					$upload_loc = 'uploads/dob'; // location to upload dob proof
					
					$res = $this->do_upload( $file_element_name, $upload_loc ); // whether there is an error or filename( for success)	
					
					$errmsg = $res; // either it will have an error message or a filename with : prefixed	
					
					if( is_array( $errmsg ))
					{
						foreach( $errmsg as $key=>$val )
						{
							$errmsg[$key] = "For the DOB Proof : ".$val; 
						}
					}
					
					if( !is_array( $errmsg )) // if there is no error
					{
						if( strpos( $errmsg, ':' ) !== FALSE )
						{
							$u_dob_proof_file = str_replace(':', '', $errmsg);
							
							
							
							// start, code added by preeti on 1st apr 14 for black-box testing
						
							// read the file and verify if it is actually a pdf file
								
							// if it is not a pdf file then delete the file and show the message as file not pdf
								
							$path = 'uploads/dob/'.$u_dob_proof_file;
								
							$this->load->helper('file');
								
							$string = read_file( $path );
						
							if( $string )
							{
								if( strpos( $string , '%PDF' ) !== FALSE )
								{
									// proceed with the saving of the data
																	
									// shoot an update query here
							
									$errmsg = ''; // all is well												
										
								}
								else 
								{
									// delete the uploaded file, dont proceed and show the error message
										
									$file_path = 'uploads/dob/'.$u_dob_proof_file;
										
									if(file_exists( $file_path )) 
									{						
										unlink( $file_path );
									}
										
									$errmsg = "For the DOB Proof : Uploaded File is not valid ";
										
								}
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/dob/'.$u_dob_proof_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the DOB Proof : Uploaded File is not valid ";		
							}						
								
								
							// end, code added by preeti on 1st apr 14 for black-box testing												
									
						}	
					}	
					else // if error 
					{
						// in case of edit form, when no file is browsed
						
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
							
							&& ($this->input->post( 'u_dob_proof_hid' ) != '' ))
						{
							
								$u_dob_proof_file = $this->input->post( 'u_dob_proof_hid' );
								
								$errmsg = ''; // all is well					
						}						
			
					}				
					
				}

			}

			} // block added by preeti on 21st apr 14 for manual testing
			else // block added by preeti on 21st apr 14 for manual testing
			{
				$errmsg = 'Values couldnot be saved';
			}					
											
		}
		else 
		{
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			$res_ins = $this->doc_model->save_user1( $u_photo_file, $u_dob_proof_file,  $new_name_proof_file );	
							
			if( $res_ins )
			{
				// check which button was clicked
			
				$save_e = $this->input->post('save_e');
			
				$save_c = $this->input->post('save_c');
			
				if( $save_e != '' )
				{
					echo 'exit';
				}
				
				if( $save_c != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}			
		
	}
	
	

	public function doc_saved() // when saved partially
	{
		//if(  $this->session->userdata('sequence_token') == $this->uri->segment('4') ) // if/else block added by preeti on 23rd apr 14
		
		if(  $this->session->userdata('sequence_token') != '' && $this->session->userdata('sequence_token') != '' && ( $this->session->userdata('sequence_token') == $this->uri->segment('4') ) ) // above line commented and this line added by preeti on 25th apr 14
		{			
			// start, code added by preeti on 4th mar 14
			
			// unset the session variable values for registration no. and the OTP
			
			$array_items = array(
			
			'is_confirm_logged' => '', 
			
			'confirm_regno' => '', 
			
			'confirm_otp' => '', 
			
			'confirm_regid' => '', 
			
			'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
			
			'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
			
			'sequence_token' => '', // added by preeti on 23rd apr 14 for manual testing
			
			'confirm_salt' => '');
	
			$this->session->unset_userdata($array_items);
			
			$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing
			
			// end, code added by preeti on 4th mar 14	
				
			$reg_id = $this->uri->segment(3);
			
			$reg_createdon = $this->doc_model->get_reg_creation( $reg_id );
			
			$res_ld = $this->doc_model->get_last_date();
										
			$ld_date = $res_ld->ld_date; // last date of registration
								
			$ld_validity = $res_ld->ld_validity; // no. of days from today to complete registration		
			
			
			
			$today = new DateTime('00:00:00');// todays date
			
			$createdon = new DateTime( $reg_createdon );
			
			$diff = date_diff( $createdon , $today); // $diff is an object
			
			$reg_days = $diff->d ; // days passed since registered as a new user
			
			$reg_days_left = $ld_validity;
			
			if( $reg_days > 0 )
			{
				$reg_days_left = (int) $reg_days_left - (int) $reg_days;		
			}
			
			$data['errmsg'] = "Form Data Saved Successfully. You can complete the form by revisiting the 'Confirm Registration' link again within ".$reg_days_left." Days.";
				
			$this->load->view('doc_saved', $data);	
		}
		else // else block added by preeti on 23rd apr 14 
		{
			// clear all session related vars and redirect 
						
			// unset the session variable values for registration no. and the OTP
			
			$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
						);
				
			$this->session->unset_userdata($array_items);
			
			$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing					
						
			redirect('doc/confirm');							
		
		}		
		
	}
	
	
	public function doc_signup2( $reg_id ) // to show the second page of the registration
	{
		// check for the session variable value 	
		
		if( $this->session->userdata('is_confirm_logged') == TRUE 
		
		&& $this->session->userdata('confirm_regno') != '' 
		
		&& $this->session->userdata('confirm_otp') != '' 
		
		&& $this->session->userdata('confirm_random') != '' 
		
		&& ( $this->session->userdata('sequence_token') != '' && $this->uri->segment('4') != '' && ( $this->session->userdata('sequence_token') == $this->uri->segment('4') ) )  ) // line modified by preeti on 25th apr 14  
		{
			// check for the regno and otp if set in the session variable
			
			// if set check if the same session value matches the db value 
			
			//for the corresponding reg id and must have the reg_status incomplete(i)
		
		
			$res_reg = $this->doc_model->get_reg_data( $reg_id );
			
			if( $res_reg )
			{
				if( $res_reg->reg_status == 'i' )
				{
					
					/*if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $res_reg->reg_otp == $this->session->userdata('confirm_otp') )*/ //  code commented by preeti on 28th mar 14 for black-box testing
					// start, code added by preeti on 28th mar 14 for black-box testing
					
					$en_pass = $this->session->userdata('confirm_otp');
					
					$salt = $this->session->userdata('confirm_salt');
					
					$result_pass = md5( $res_reg->reg_otp.$salt  );
					
					if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $result_pass == $this->session->userdata('confirm_otp') )// end, code added by preeti on 28th mar 14 for black-box testing
					{
						// if all is well show the page
						
						
						$data['errmsg'] = '';
		
						$reg_id = $this->uri->segment(3);
						
						$regno = $this->doc_model->get_regno( $reg_id );
						
						$data['record'] = $this->doc_model->show_user_data2( $regno ); 
						
						// get the pg data as well
						
						$record_pg_arr = $this->doc_model->get_pg_data( $regno );
						
						if( $record_pg_arr )
						{
							$data['record_pg'] = $record_pg_arr;
							
							$data['cnt_pg']	= COUNT( $record_pg_arr );
						}
						else 
						{
							$data['record_pg'] = '';
							
							$data['cnt_pg']	= 0;
						}
						
						$data['reg_id'] = $reg_id;
						
						$data['confirm_random'] = $this->session->userdata('confirm_random'); // below line added by preeti on 21st apr 14 for manual testing
						
						
						// start, code added by preeti on 23rd apr 14 for manual testing
						
						$sequence_token = $this->random_num();
						
						$sess_arr = array(
						
						'sequence_token' => $sequence_token
						
						);
						
						$data['sequence_token'] = $sequence_token;// the value to be set in a hidden field
						
						$this->session->set_userdata( $sess_arr );
						
						// end, code added by preeti on 23rd apr 14 for manual testing
						
						
						$this->load->view('doc_signup2', $data);
									
					}
					else // else block added by preeti on 23rd apr 14 
					{
						// clear all session related vars and redirect 
						
						// unset the session variable values for registration no. and the OTP
			
						$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
						);
				
						$this->session->unset_userdata($array_items);
						
						$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing					
						
						redirect('doc/confirm');					
		
					}
					
				}
				else if( $res_reg->reg_status == 'c' )
				{
					// start, added on 21st apr 14 for manual testing					
					
					$this->load->helper('captcha'); 
			
					$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			  		$cap = create_captcha($vals);
					
			  		
			  		$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			  		$this->session->set_userdata($data);
			  		
			  		$data['cap_img']=$cap['image'];	
					
					// end, added on 21st apr 14 for manual testing
					
					
					$data['errmsg'] = 'Your Registration is complete';
				
					$data['salt'] = $this->random_num();	// code added by preeti on 22nd apr 14 for manual testing	
			
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					// add the same value in the session variable
									
					$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
					// end, code added by preeti on 22nd apr 14 for manual testing	
					
									
				
					$this->load->view('doc_confirm', $data);
				}
			
			}
			else 
			{
				
				// start, added on 21st apr 14 for manual testing					
					
				/*$this->load->helper('captcha'); 
			
				$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			  	$cap = create_captcha($vals);
					
			  		
			  	$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			  	$this->session->set_userdata($data);
			  		
			  	$data['cap_img']=$cap['image'];	
					
				// end, added on 21st apr 14 for manual testing
				
				$data['errmsg'] = '';
				
				$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
			
				// start, code added by preeti on 22nd apr 14 for manual testing
				
				// add the same value in the session variable
								
				$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
				// end, code added by preeti on 22nd apr 14 for manual testing	
						
			
				$this->load->view('doc_confirm', $data);*/
				
				redirect('doc/confirm');
			}	
			 
		}
		else 
		{
			// start, added by preeti on 23rd apr 14 for manual testing
			
			$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', 
						
						'ci_session' => '', 
						
						'confirm_salt' => '',
						
						'sequence_token' => '' );
				
			$this->session->unset_userdata($array_items);
			
			$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing
			
			//redirect('doc/confirm');
			
			// above line commented and below added by preeti on 21st may 14
			
			$errmsg = 'Please fill this form and submit';
			
			$errmsg = urlencode( $errmsg );
			
			redirect('doc/confirm/'.$errmsg);
		}
		
			
	}
	
	public function att_check( $num )
	{
		if( $num )
		{
		
			if( intval( $num ) > 2 )
			{
				$this->form_validation->set_message('att_check', "No. of attempts should not be more than 2 for Final MBBS Exam ( Part I ) .");
			
				return FALSE;
			}
			else 
			{
				return TRUE;
			}
		}
		else 
		{
			$this->form_validation->set_message('att_check', "No. of attempts for Final MBBS Exam ( Part I ) field is required.");	
		
			return FALSE;
		
		}
		
	}	
	
	public function att_check_two( $num )
	{
		if( $num )
		{
			if( intval( $num ) > 2 )
			{
				$this->form_validation->set_message('att_check_two', "No. of attempts should not be more than 2 for Final MBBS Exam ( Part II ).");
			
				return FALSE;
			}
			else 
			{
				return TRUE;	
			}
		}
		else 
		{
			$this->form_validation->set_message('att_check_two', "No. of attempts Part II field is required.");	
		
			return FALSE;
		}
		
	}
	
	// below code commented by preeti on 1st apr 14 for black-box testing
	
	/*public function save2() // save the second page
	{
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$u_pmrc_copy_file = '';
		
		$u_attempt_cert_file = ''; // for part I
		
		$u_attempt_cert1_file = ''; // for part II
		
		$u_dd_copy_file = '';
		
		$u_intern_cert_file = '';
		
		$u_mbbs_cert_file = '';
		
		$u_pg_cert1_file = '' ; 
		
		$u_pg_cert2_file = ''; 
		
		$u_pg_cert3_file = '';
		
		$this->form_validation->set_rules('u_grad_from', "Graduate From", 'required');
	
		$this->form_validation->set_rules('u_basic_qual', "Basic Qualification", 'trim|required');
		
		
		$this->form_validation->set_rules('u_adm_date', "Date Of Admission", 'trim|required');
		
		$this->form_validation->set_rules('u_passing_date', "Date Of Passing", 'trim|required');
		
		//$this->form_validation->set_rules('u_num_attempt', "No. of Attempts (Part I)", 'trim|required');
		
		
		$this->form_validation->set_rules('u_num_attempt', 'No. of Attempts for Final MBBS Exam (Part I)', 'trim|required|callback_att_check');
		
		
		//$this->form_validation->set_rules('u_num_attempt1', "No. of Attempts (Part II)", 'trim|required');
		
		$this->form_validation->set_rules('u_num_attempt1', 'No. of Attempts for Final MBBS Exam (Part II)', 'trim|required|callback_att_check_two');
		
		$this->form_validation->set_rules('u_college', "Medical College", 'trim|required');
		
		$this->form_validation->set_rules('u_univ', "University", 'trim|required');
		
		
		$this->form_validation->set_rules('u_intern_complete', "Internship completed", 'trim|required');
		
		
		if( $this->input->post('u_intern_complete') == 'y' )
		{
			$this->form_validation->set_rules('u_intern_date', "Internship Completion Date", 'trim|required');
		}
		else if( $this->input->post('u_intern_complete') == 'n' )
		{
			$this->form_validation->set_rules('u_intern_date_likely', "Likely Internship Completion Date", 'trim|required');
		}
		
		

		$this->form_validation->set_rules('u_college_recog', "College Recognized by MCI", 'trim|required');
		
		
		$this->form_validation->set_rules('u_pmrc_num', "PMRC Medical Registration Number", 'trim|required');	
		
		$this->form_validation->set_rules('u_pmrc_issue_off', "Issuing Office", 'trim|required');
		
		$this->form_validation->set_rules('u_pmrc_reg_date', "Date Of Registration", 'trim|required');
		
		
		
		
		$this->form_validation->set_rules('u_is_pg', "Are you a Post Graduate", 'trim|required');
		
		if( $this->input->post('u_is_pg') == 'y' )
		{
			$this->form_validation->set_rules('pg_degree', "PG Qualification", 'trim|required');
			
			$this->form_validation->set_rules('pg_subject', "Subject", 'trim|required');
			
			$this->form_validation->set_rules('pg_college', "College", 'trim|required');
			
			$this->form_validation->set_rules('pg_univ', "University", 'trim|required');
			
			$this->form_validation->set_rules('pg_year', "Year of Passing", 'trim|required');
			
			$this->form_validation->set_rules('pg_mci_recog', "College Recognized by MCI", 'required');
		
		
			if( $this->input->post('add_pg1') == 'y' )
			{
				// validation on all the fields				
				
				$this->form_validation->set_rules('pg_degree1', "PG Qualification (PG second)", 'trim|required');
			
				$this->form_validation->set_rules('pg_subject1', "Subject (PG second)", 'trim|required');
				
				$this->form_validation->set_rules('pg_college1', "College (PG second)", 'trim|required');
				
				$this->form_validation->set_rules('pg_univ1', "University (PG second)", 'trim|required');
				
				$this->form_validation->set_rules('pg_year1', "Year of Passing (PG second)", 'trim|required');
				
				$this->form_validation->set_rules('pg_mci_recog1', "College Recognized by MCI (PG second)", 'required');
				
			}
			
			if( $this->input->post('add_pg2') == 'y' )
			{
				// validation on all the fields				
				
				$this->form_validation->set_rules('pg_degree2', "PG Qualification (PG third)", 'trim|required');
			
				$this->form_validation->set_rules('pg_subject2', "Subject (PG third)", 'trim|required');
				
				$this->form_validation->set_rules('pg_college2', "College (PG third)", 'trim|required');
				
				$this->form_validation->set_rules('pg_univ2', "University (PG third)", 'trim|required');
				
				$this->form_validation->set_rules('pg_year2', "Year of Passing (PG third)", 'trim|required');
				
				$this->form_validation->set_rules('pg_mci_recog2', "College Recognized by MCI (PG third)", 'required');
				
			}
		
		
		}
		
		$this->form_validation->set_rules('u_dd_num', "DD Number", 'trim|required');
		
		$this->form_validation->set_rules('u_dd_date', "DD Date", 'trim|required');
		
		$this->form_validation->set_rules('u_dd_bank', "Payable Bank", 'trim|required');
		
		
				
		if( $this->form_validation->run() == TRUE )
		{
			// mandatory uploads - Copy of PMRC certificate
			
			// mandatory uploads - attempt certificate part I
			
			// mandatory uploads - attempt certificate part II
			
			
			// optional uploads - internship certificate , if internship is complete only then
			
			// mandatory uploads - MBBS certificate
			
			// OPTIONAL uploads - PG certificate
			
			// if add more pg - PG certificate
			
			// if add more pg - PG certificate
			
			// mandatory uploads - dd certificate
			
			
			$file_element_name = "u_pmrc_copy";
					
			$upload_loc = 'uploads/pmrc'; 
				
			$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
			$errmsg = $res;
			
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $key=>$val )
				{
					$errmsg[$key] = "For the Permanent Medical Registration Certificate/Provisional : ".$val; 
				}	
			}
			 	
					
			if( !is_array( $errmsg ))
			{
				if( strpos( $errmsg, ':' ) !== FALSE )
				{
					$u_pmrc_copy_file = str_replace(':', '', $errmsg);							
							
					$errmsg = ''; // all is well						
							
				}	
			}
			else
			{
				if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
				&& ($this->input->post( 'u_pmrc_copy_hid' ) != '' ))					
				{
					$u_pmrc_copy_file = $this->input->post( 'u_pmrc_copy_hid' );
						
					$errmsg = ''; // all is well							
							
				}						
						
			}
			
			// upload attempt certificate for part I 
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_attempt_cert";
					
				$upload_loc = 'uploads/attempt1'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Attempt Certificate Part I : ".$val; 
					}
				
				} 	
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_attempt_cert_file = str_replace(':', '', $errmsg);							
							
						$errmsg = ''; // all is well						
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_attempt_cert_hid' ) != '' ))					
						{
							$u_attempt_cert_file = $this->input->post( 'u_attempt_cert_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}
			
			
			// upload attempt certificate for part II
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_attempt_cert1";
					
				$upload_loc = 'uploads/attempt2'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Attempt Certificate Part II : ".$val; 
					} 	
				}
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_attempt_cert1_file = str_replace(':', '', $errmsg);							
							
						$errmsg = ''; // all is well						
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_attempt_cert1_hid' ) != '' ))					
						{
							$u_attempt_cert1_file = $this->input->post( 'u_attempt_cert1_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}
			
			
			// upload internship certificate , only if internship is complete
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' && ( $this->input->post('u_intern_complete') == 'y' ) ) 
			{
				$file_element_name = "u_intern_cert";
			
				$upload_loc = 'uploads/intern'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
				
					foreach( $errmsg as $key=>$val )
					{
						// below line modified by preeti on 5th mar 14
						
						$errmsg[$key] = "For the Internship Completion Certificate : ".$val; 
					}
				
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_intern_cert_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_intern_cert_hid' ) != '' ))
					{
						
							$u_intern_cert_file = $this->input->post( 'u_intern_cert_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			
			// upload mbbs passing certificate
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' ) 
			{
				$file_element_name = "u_mbbs_cert";
			
				$upload_loc = 'uploads/mbbs'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the MBBS Certificate : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_mbbs_cert_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_mbbs_cert_hid' ) != '' ))
					{
						
							$u_mbbs_cert_file = $this->input->post( 'u_mbbs_cert_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			// upload PG passing certificate
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y') 
			{
				$file_element_name = "pg_cert";
			
				$upload_loc = 'uploads/pg/pg1'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG first) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert1_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert1_hid' ) != '' ))
					{
						
							$u_pg_cert1_file = $this->input->post( 'pg_cert1_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	


			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y' &&  $this->input->post('add_pg1') == 'y' ) 
			{
				$file_element_name = "pg_cert1";
			
				$upload_loc = 'uploads/pg/pg2'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG second) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert2_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert2_hid' ) != '' ))
					{
						
							$u_pg_cert2_file = $this->input->post( 'pg_cert2_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	


			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y' &&  $this->input->post('add_pg2') == 'y' ) 
			{
				$file_element_name = "pg_cert2";
			
				$upload_loc = 'uploads/pg/pg3'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG third) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert3_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert3_hid' ) != '' ))
					{
						
							$u_pg_cert3_file = $this->input->post( 'pg_cert3_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			
			if( $errmsg == '' ) 
			{
				// upload the dob certificate
				
				$file_element_name = "u_dd_copy";
			
				$upload_loc = 'uploads/dd'; // location to upload dob proof
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); // whether there is an error or filename( for success)	
				
				$errmsg = $res; // either it will have an error message or a filename with : prefixed	
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the DD Copy : ".$val; 
					}
				} 
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_dd_copy_file = str_replace(':', '', $errmsg);	
						
						$errmsg = ''; // all is well							
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_dd_copy_hid' ) != '' ))
					{
						
							$u_dd_copy_file = $this->input->post( 'u_dd_copy_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}			
											
		}
		else 
		{
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			// below lines modified by preeti on 19th feb 14
			
			//$res_ins = $this->doc_model->save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
			//$u_attempt_cert1_file , $u_dd_copy_file, $u_intern_cert_file, $u_mbbs_cert_file, 
			
			//$u_pg_cert_file );	// lines commented and below added by preeti on 24th feb 14 
							
			$res_ins = $this->doc_model->save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
			$u_attempt_cert1_file , $u_dd_copy_file, $u_mbbs_cert_file, 
			
			$u_intern_cert_file , $u_pg_cert1_file , $u_pg_cert2_file , $u_pg_cert3_file );				
							
			if( $res_ins )
			{
				// check which button was clicked
			
				$save_e = $this->input->post('save_e');
			
				$save_c = $this->input->post('save_c');
			
				if( $save_e != '' )
				{
					echo 'exit';
				}
				
				if( $save_c != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}			
		
	}*/
	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_adm( $str ) // age for admission must be between 17 to 30
	{
		
		$reg_id = $this->input->post('reg_id');
		
		// get the dob from db
		
		$dob = $this->doc_model->get_dob( $reg_id );		
		
		if( $dob )
		{
			
			// change the calendar format to db date format and 
			
			$cal_arr = explode('/', $dob ); // date is in format dd/mm/YYYY			
			
			$sDateBirth = $dob;			
			
			// change the calendar format to db date format and 
			
			$cal_arr = explode('/', $str ); // date is in format dd/mm/YYYY			
			
			$adm_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
			
			$oDateNow = new DateTime( $adm_date );
			
			$oDateBirth = new DateTime( $sDateBirth );
	
			$oDateIntervall = $oDateNow->diff($oDateBirth);
	
	 		$year = $oDateIntervall->y;
			
			if( intval( $year ) >= 17 && intval( $year ) <= 30 )
		    {
		    	return TRUE;
		    }
			else 
			{
				$this->form_validation->set_message('check_adm', 'Your Admission date is assumed to be between 17 to 30 yrs from date of birth');
					
		    	return FALSE;
				
			}			
			
		}			
		
	}


	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_passing( $str ) // age for passing wrt d.o. admn must be between 4 to 10
	{
		
		$adm_date = $this->input->post('u_adm_date');
		
			
		$cal_arr = explode('/', $adm_date ); 			
			
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];		
			
		
			
		$cal_arr = explode('/', $str ); 		
			
		$passing_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
			
		
		
		
		
		$oDateNow = new DateTime( $passing_date );
			
		$oDateBirth = new DateTime( $sDateBirth );
	
		$oDateIntervall = $oDateNow->diff($oDateBirth);
	
	 	$year = $oDateIntervall->y;
			
		if( intval( $year ) >= 4 && intval( $year ) <= 10 )
		{
		    return TRUE;
		}
		else 
		{
			$this->form_validation->set_message('check_passing', 'Your Passing date is assumed to be between 4 to 10 yrs from date of admission');
					
		    return FALSE;
				
		}				
		
	}
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_intern( $str ) // age for passing wrt d.o. admn must be between 4 to 10
	{
		
		$passing_date = $this->input->post('u_passing_date');
		
			
		$cal_arr = explode('/', $passing_date ); 			
			
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];		
			
		
		
			
		$cal_arr = explode('/', $str ); 		
			
		$intern_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];	
		
		
		
		$oDateNow = new DateTime( $intern_date );
			
		$oDateBirth = new DateTime( $sDateBirth );
	
		$oDateIntervall = $oDateNow->diff($oDateBirth);
	
	 	$year = $oDateIntervall->y;
		
		$invert = $oDateIntervall->invert; // if in negative then it is 0
		
		$month = $oDateIntervall->m; 
		
		$day = $oDateIntervall->d;
			
		if( intval( $year ) > 0 && $invert == 1 )
		{
		    return TRUE;
		}
		else if( intval( $year ) == 0 )
		{
			if( $invert == 0 )
			{
				$this->form_validation->set_message('check_intern', 'The value of %s field is assumed to be after the date of passing');
					
		    	return FALSE;	
			}
			else 
			{
				if( $month > 0 )
				{
					return TRUE;
				}
				else 
				{
					if( $day > 0 )
					{
						return TRUE;
					}
					else 
					{
						$this->form_validation->set_message('check_intern', 'The value of %s field is assumed to be after the date of passing');
						
						return FALSE;
					}		
				}
			}						
			
		}	
		else
		{
			$this->form_validation->set_message('check_intern', 'The value of %s field is assumed to be after the date of passing');
					
		    return FALSE;
		}			
		
	}

	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_pg( $str ) // passing year must be greater than the year of passing
	{
		
		$passing_date = $this->input->post('u_passing_date');
		
		$cal_arr = explode('/', $passing_date ); 		
			
		$year = $cal_arr[2];	
		
		
		if( $str > $year )
		{	
			return TRUE;
		}	
		else
		{
			$this->form_validation->set_message('check_pg', 'The value of %s field is assumed to be after the year of passing MBBS');
					
		    return FALSE;
		}			
		
	}
	
	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_dd( $str ) // chcek dd date from db
	{
		
		$obj = $this->doc_model->get_last_date();
		
		$start_date = $obj->ld_start;
		
		
		
		$start_time = strtotime( $start_date );
		
		$end_date = $obj->ld_date;
		
		$end_time = strtotime( $end_date );
		
		
		$dd_date = $this->input->post('u_dd_date');
		
		$cal_arr = explode('/', $dd_date ); // date is in format dd/mm/YYYY
		
		
		
		$dd_date = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];
		
		$dd_time = strtotime( $dd_date );		
		
		
		if( $dd_time >= $start_time && $dd_time <= $end_time )
		{	
			return TRUE;
		}	
		else
		{
			$this->form_validation->set_message('check_dd', 'The value of %s field is not in between '.db_to_calen( $start_date ).' and '.db_to_calen( $end_date ));
					
		    return FALSE;
		}			
		
	}
	
	
	// below function added by preeti on 1st apr 14 for black-box testing
	
	public function save2() // save the second page
	{
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$u_pmrc_copy_file = '';
		
		$u_attempt_cert_file = ''; // for part I
		
		$u_attempt_cert1_file = ''; // for part II
		
		$u_dd_copy_file = '';
		
		$u_intern_cert_file = '';
		
		$u_mbbs_cert_file = '';
		
		$u_pg_cert1_file = '' ; 
		
		$u_pg_cert2_file = ''; 
		
		$u_pg_cert3_file = '';
		
		$this->form_validation->set_rules('u_grad_from', "Graduate From", 'required');
	
		$this->form_validation->set_rules('u_basic_qual', "Basic Qualification", 'trim|required|callback_alpha_and_space');
		
		
		// below line modified by preeti on 2nd apr 14
		
		$this->form_validation->set_rules('u_adm_date', "Date Of Admission", 'trim|required|callback_check_adm');
		
		// below line modified by preeti on 2nd apr 14
		
		$this->form_validation->set_rules('u_passing_date', "Date Of Passing", 'trim|required|callback_check_passing');
		
		//$this->form_validation->set_rules('u_num_attempt', "No. of Attempts (Part I)", 'trim|required');
		
		
		$this->form_validation->set_rules('u_num_attempt', 'No. of Attempts for Final MBBS Exam (Part I)', 'trim|required|callback_att_check');
		
		
		//$this->form_validation->set_rules('u_num_attempt1', "No. of Attempts (Part II)", 'trim|required');
		
		$this->form_validation->set_rules('u_num_attempt1', 'No. of Attempts for Final MBBS Exam (Part II)', 'trim|required|callback_att_check_two');
		
		$this->form_validation->set_rules('u_college', "Medical College", 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
		
		$this->form_validation->set_rules('u_univ', "University", 'trim|required|callback_alpha_and_space');// line modified by preeti on 28th apr 14
		
		
		$this->form_validation->set_rules('u_intern_complete', "Internship completed", 'trim|required');
		
		
		if( $this->input->post('u_intern_complete') == 'y' )
		{
			// below line modified by preeti on 2nd apr 14
				
			$this->form_validation->set_rules('u_intern_date', "Internship Completion Date", 'trim|required|callback_check_intern');
		}
		else if( $this->input->post('u_intern_complete') == 'n' )
		{
			// below line modified by preeti on 2nd apr 14	
				
			$this->form_validation->set_rules('u_intern_date_likely', "Likely Internship Completion Date", 'trim|required|callback_check_intern');
		}
		
		

		$this->form_validation->set_rules('u_college_recog', "College Recognized by MCI", 'trim|required');
		
		
		$this->form_validation->set_rules('u_pmrc_num', "PMRC Medical Registration Number", 'trim|required|callback_alpha_num_dash');	
		
		$this->form_validation->set_rules('u_pmrc_issue_off', "Issuing Office", 'trim|required|callback_alpha_and_space');
		
		$this->form_validation->set_rules('u_pmrc_reg_date', "Date Of Registration", 'trim|required|callback_check_intern');
		
		
		
		
		$this->form_validation->set_rules('u_is_pg', "Are you a Post Graduate", 'trim|required');
		
		if( $this->input->post('u_is_pg') == 'y' )
		{
			$this->form_validation->set_rules('pg_degree', "PG Qualification", 'trim|required|callback_alpha_and_space');
			
			$this->form_validation->set_rules('pg_subject', "Subject", 'trim|required|callback_alpha_and_space');
			
			$this->form_validation->set_rules('pg_college', "College", 'trim|required|callback_alpha_and_space');
			
			$this->form_validation->set_rules('pg_univ', "University", 'trim|required|callback_alpha_and_space');
			
			// below line modified by preeti on 2nd apr 14
			
			$this->form_validation->set_rules('pg_year', "Year of Passing (PG)", 'trim|required|callback_check_pg');
			
			$this->form_validation->set_rules('pg_mci_recog', "College Recognized by MCI", 'required');
		
		
			if( $this->input->post('add_pg1') == 'y' )
			{
				// validation on all the fields				
				
				$this->form_validation->set_rules('pg_degree1', "PG Qualification (PG second)", 'trim|required|callback_alpha_and_space');
			
				$this->form_validation->set_rules('pg_subject1', "Subject (PG second)", 'trim|required|callback_alpha_and_space');
				
				$this->form_validation->set_rules('pg_college1', "College (PG second)", 'trim|required|callback_alpha_and_space');
				
				$this->form_validation->set_rules('pg_univ1', "University (PG second)", 'trim|required|callback_alpha_and_space');
				
				// below line modified by preeti on 2nd apr 14
				
				$this->form_validation->set_rules('pg_year1', "Year of Passing (PG second)", 'trim|required|callback_check_pg');
				
				$this->form_validation->set_rules('pg_mci_recog1', "College Recognized by MCI (PG second)", 'required');
				
			}
			
			if( $this->input->post('add_pg2') == 'y' )
			{
				// validation on all the fields				
				
				$this->form_validation->set_rules('pg_degree2', "PG Qualification (PG third)", 'trim|required|callback_alpha_and_space');
			
				$this->form_validation->set_rules('pg_subject2', "Subject (PG third)", 'trim|required|callback_alpha_and_space');
				
				$this->form_validation->set_rules('pg_college2', "College (PG third)", 'trim|required|callback_alpha_and_space');
				
				$this->form_validation->set_rules('pg_univ2', "University (PG third)", 'trim|required|callback_alpha_and_space');
				
				// below line modified by preeti on 2nd apr 14
				
				$this->form_validation->set_rules('pg_year2', "Year of Passing (PG third)", 'trim|required|callback_check_pg');
				
				$this->form_validation->set_rules('pg_mci_recog2', "College Recognized by MCI (PG third)", 'required');
				
			}
		
		
		}
		
		$this->form_validation->set_rules('u_dd_num', "DD Number", 'trim|required|integer|min_length[6]|max_length[6]');
		
		// below code modified by preeti on 2nd apr 14 for black-box testing
		
		$this->form_validation->set_rules('u_dd_date', "DD Date", 'trim|required|callback_check_dd');
		
		$this->form_validation->set_rules('u_dd_bank', "Payable Bank", 'trim|required|callback_alpha_and_space');
		
		
		
				
		if( $this->form_validation->run() == TRUE )
		{
			
			// below if added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('confirm_random') != ''  && strlen( $this->input->post('confirm_random') ) == 32   
			
			&& $this->session->userdata('confirm_random') != ''   && ( $this->input->post('confirm_random') == $this->session->userdata('confirm_random') ) ) 
			{	
		
			
			
			// mandatory uploads - Copy of PMRC certificate
			
			// mandatory uploads - attempt certificate part I
			
			// mandatory uploads - attempt certificate part II
			
			
			// optional uploads - internship certificate , if internship is complete only then
			
			// mandatory uploads - MBBS certificate
			
			// OPTIONAL uploads - PG certificate
			
			// if add more pg - PG certificate
			
			// if add more pg - PG certificate
			
			// mandatory uploads - dd certificate
			
			
			$file_element_name = "u_pmrc_copy";
					
			$upload_loc = 'uploads/pmrc'; 
				
			$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
			$errmsg = $res;
			
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $key=>$val )
				{
					$errmsg[$key] = "For the Permanent Medical Registration Certificate/Provisional : ".$val; 
				}	
			}
			 	
					
			if( !is_array( $errmsg ))
			{
				if( strpos( $errmsg, ':' ) !== FALSE )
				{
					$u_pmrc_copy_file = str_replace(':', '', $errmsg);	
					
					
					
					// start, code added by preeti on 1st apr 14 for black-box testing
						
					// read the file and verify if it is actually a pdf file
								
					// if it is not a pdf file then delete the file and show the message as file not pdf
								
					$path = 'uploads/pmrc/'.$u_pmrc_copy_file;
								
					$this->load->helper('file');
								
					$string = read_file( $path );
						
					if( $string )
					{
						if( strpos( $string , '%PDF' ) !== FALSE )
						{
							// proceed with the saving of the data
																	
							// shoot an update query here
							
							$errmsg = ''; // all is well												
										
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/pmrc/'.$u_pmrc_copy_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the Permanent Medical Registration Certificate/Provisional : Uploaded File is not valid ";
										
						}
					}
					else 
					{
						// delete the uploaded file, dont proceed and show the error message
										
						$file_path = 'uploads/pmrc/'.$u_pmrc_copy_file;
										
						if(file_exists( $file_path )) 
						{						
							unlink( $file_path );
						}
										
						$errmsg = "For the Permanent Medical Registration Certificate/Provisional : Uploaded File is not valid ";		
					}						
								
								
					// end, code added by preeti on 1st apr 14 for black-box testing								
							
				}	
			}
			else
			{
				if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
				&& ($this->input->post( 'u_pmrc_copy_hid' ) != '' ))					
				{
					$u_pmrc_copy_file = $this->input->post( 'u_pmrc_copy_hid' );
						
					$errmsg = ''; // all is well							
							
				}						
						
			}
			
			// upload attempt certificate for part I 
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_attempt_cert";
					
				$upload_loc = 'uploads/attempt1'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Attempt Certificate Part I : ".$val; 
					}
				
				} 	
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_attempt_cert_file = str_replace(':', '', $errmsg);	
						
						
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/attempt1/'.$u_attempt_cert_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/attempt1/'.$u_attempt_cert_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the Attempt Certificate Part I : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/attempt1/'.$u_attempt_cert_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the Attempt Certificate Part I : Uploaded File is not valid ";		
						}						
								
								
						// end, code added by preeti on 1st apr 14 for black-box testing										
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_attempt_cert_hid' ) != '' ))					
						{
							$u_attempt_cert_file = $this->input->post( 'u_attempt_cert_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}
			
			
			// upload attempt certificate for part II
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_attempt_cert1";
					
				$upload_loc = 'uploads/attempt2'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Attempt Certificate Part II : ".$val; 
					} 	
				}
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_attempt_cert1_file = str_replace(':', '', $errmsg);							
							
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/attempt2/'.$u_attempt_cert1_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/attempt2/'.$u_attempt_cert1_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the Attempt Certificate Part II : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/dob/'.$u_dob_proof_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the Attempt Certificate Part II : Uploaded File is not valid ";		
						}						
								
								
						// end, code added by preeti on 1st apr 14 for black-box testing											
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_attempt_cert1_hid' ) != '' ))					
						{
							$u_attempt_cert1_file = $this->input->post( 'u_attempt_cert1_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}
			
			
			// upload internship certificate , only if internship is complete
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' && ( $this->input->post('u_intern_complete') == 'y' ) ) 
			{
				$file_element_name = "u_intern_cert";
			
				$upload_loc = 'uploads/intern'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
				
					foreach( $errmsg as $key=>$val )
					{
						// below line modified by preeti on 5th mar 14
						
						$errmsg[$key] = "For the Internship Completion Certificate : ".$val; 
					}
				
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_intern_cert_file = str_replace(':', '', $errmsg);				
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/intern/'.$u_intern_cert_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/intern/'.$u_intern_cert_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the Internship Completion Certificate : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/intern/'.$u_intern_cert_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the Internship Completion Certificate : Uploaded File is not valid ";		
						}								
								
						// end, code added by preeti on 1st apr 14 for black-box testing												
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_intern_cert_hid' ) != '' ))
					{
						
							$u_intern_cert_file = $this->input->post( 'u_intern_cert_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			
			// upload mbbs passing certificate
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' ) 
			{
				$file_element_name = "u_mbbs_cert";
			
				$upload_loc = 'uploads/mbbs'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{				
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the MBBS Certificate : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_mbbs_cert_file = str_replace(':', '', $errmsg);	
						
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/mbbs/'.$u_mbbs_cert_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/mbbs/'.$u_mbbs_cert_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the MBBS Certificate : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/mbbs/'.$u_mbbs_cert_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the MBBS Certificate : Uploaded File is not valid ";		
						}						
								
								
						// end, code added by preeti on 1st apr 14 for black-box testing											
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_mbbs_cert_hid' ) != '' ))
					{
						
							$u_mbbs_cert_file = $this->input->post( 'u_mbbs_cert_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			// upload PG passing certificate
			
			// below if  added by preeti on 19th feb 14
			
			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y') 
			{
				$file_element_name = "pg_cert";
			
				$upload_loc = 'uploads/pg/pg1'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG first) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert1_file = str_replace(':', '', $errmsg);	
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/pg/pg1/'.$u_pg_cert1_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/pg/pg1/'.$u_pg_cert1_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the PG Certificate (PG first) : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/pg/pg1/'.$u_pg_cert1_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the PG Certificate (PG first) : Uploaded File is not valid ";		
						}							
								
						// end, code added by preeti on 1st apr 14 for black-box testing											
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert1_hid' ) != '' ))
					{
						
							$u_pg_cert1_file = $this->input->post( 'pg_cert1_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	


			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y' &&  $this->input->post('add_pg1') == 'y' ) 
			{
				$file_element_name = "pg_cert1";
			
				$upload_loc = 'uploads/pg/pg2'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG second) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert2_file = str_replace(':', '', $errmsg);	
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/pg/pg2/'.$u_pg_cert2_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/pg/pg2/'.$u_pg_cert2_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the PG Certificate (PG second) : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/pg/pg2/'.$u_pg_cert2_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the PG Certificate (PG second) : Uploaded File is not valid ";		
						}							
								
						// end, code added by preeti on 1st apr 14 for black-box testing								
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert2_hid' ) != '' ))
					{
						
							$u_pg_cert2_file = $this->input->post( 'pg_cert2_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	


			if( $errmsg == '' &&  $this->input->post('u_is_pg') == 'y' &&  $this->input->post('add_pg2') == 'y' ) 
			{
				$file_element_name = "pg_cert2";
			
				$upload_loc = 'uploads/pg/pg3'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the PG Certificate (PG third) : ".$val; 
					}
				} 	
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_pg_cert3_file = str_replace(':', '', $errmsg);	
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/pg/pg3/'.$u_pg_cert3_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/pg/pg3/'.$u_pg_cert3_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the PG Certificate (PG third) : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/pg/pg3/'.$u_pg_cert3_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the PG Certificate (PG third) : Uploaded File is not valid ";		
						}							
								
						// end, code added by preeti on 1st apr 14 for black-box testing									
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'pg_cert3_hid' ) != '' ))
					{
						
							$u_pg_cert3_file = $this->input->post( 'pg_cert3_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}	
			
			
			if( $errmsg == '' ) 
			{
				// upload the dob certificate
				
				$file_element_name = "u_dd_copy";
			
				$upload_loc = 'uploads/dd'; // location to upload dob proof
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); // whether there is an error or filename( for success)	
				
				$errmsg = $res; // either it will have an error message or a filename with : prefixed	
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the DD Copy : ".$val; 
					}
				} 
				
				if( !is_array( $errmsg )) // if there is no error
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_dd_copy_file = str_replace(':', '', $errmsg);	
						
						// start, code added by preeti on 1st apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
								
						// if it is not a pdf file then delete the file and show the message as file not pdf
								
						$path = 'uploads/dd/'.$u_dd_copy_file;
								
						$this->load->helper('file');
								
						$string = read_file( $path );
						
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																	
								// shoot an update query here
							
								$errmsg = ''; // all is well												
										
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
										
								$file_path = 'uploads/dd/'.$u_dd_copy_file;
										
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
										
								$errmsg = "For the DD Copy : Uploaded File is not valid ";
										
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
										
							$file_path = 'uploads/dd/'.$u_dd_copy_file;
										
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
										
							$errmsg = "For the DD Copy : Uploaded File is not valid ";		
						}						
								
								
						// end, code added by preeti on 1st apr 14 for black-box testing								
								
					}	
				}	
				else // if error 
				{
					// in case of edit form, when no file is browsed
					
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
						&& ($this->input->post( 'u_dd_copy_hid' ) != '' ))
					{
						
							$u_dd_copy_file = $this->input->post( 'u_dd_copy_hid' );
							
							$errmsg = ''; // all is well					
					}						
		
				}				
				
			}

		  } // block added by preeti on 21st apr 14 for manual testing
		  else // block added by preeti on 21st apr 14 for manual testing
		  {
			$errmsg = 'Values couldnot be saved';
		  }		
											
		}
		else 
		{			
			
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			// below lines modified by preeti on 19th feb 14
			
			/*$res_ins = $this->doc_model->save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
			$u_attempt_cert1_file , $u_dd_copy_file, $u_intern_cert_file, $u_mbbs_cert_file, 
			
			$u_pg_cert_file );	*/ /* lines commented and below added by preeti on 24th feb 14 */
							
			$res_ins = $this->doc_model->save_user2( $u_pmrc_copy_file, $u_attempt_cert_file,
			
			$u_attempt_cert1_file , $u_dd_copy_file, $u_mbbs_cert_file, 
			
			$u_intern_cert_file , $u_pg_cert1_file , $u_pg_cert2_file , $u_pg_cert3_file );				
							
			if( $res_ins )
			{
				// check which button was clicked
			
				$save_e = $this->input->post('save_e');
			
				$save_c = $this->input->post('save_c');
			
				if( $save_e != '' )
				{
					echo 'exit';
				}
				
				if( $save_c != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}			
		
	}
	

	public function doc_signup3( $reg_id ) // show the third page of the registration
	{
		// check for the session variable value 
		
		if( $this->session->userdata('is_confirm_logged') == TRUE 
		
		&& $this->session->userdata('confirm_regno') != '' 
		
		&& $this->session->userdata('confirm_otp') != ''  
		
		&& $this->session->userdata('confirm_random') != '' 
		
		&& ( $this->session->userdata('sequence_token') != '' && 
		
		$this->session->userdata('sequence_token') != '' && 
		
		( $this->session->userdata('sequence_token') == $this->uri->segment('4') ) )  ) // line modified by preeti on 25th apr 14 for manual testing
		{
			// check for the regno and otp if set in the session variable
			
			// if set check if the same session value matches the db value 
			
			//for the corresponding reg id and must have the reg_status incomplete(i)
		
		
			$res_reg = $this->doc_model->get_reg_data( $reg_id );
			
			if( $res_reg )
			{
				if( $res_reg->reg_status == 'i' )
				{
					/*if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $res_reg->reg_otp == $this->session->userdata('confirm_otp') )*///  code commented by preeti on 28th mar 14 for black-box testing
					
					// start, code added by preeti on 28th mar 14 for black-box testing
					
					$en_pass = $this->session->userdata('confirm_otp');
					
					$salt = $this->session->userdata('confirm_salt');
					
					$result_pass = md5( $res_reg->reg_otp.$salt  );
					
					if( $res_reg->reg_regno == $this->session->userdata('confirm_regno') 
					
					&& $result_pass == $this->session->userdata('confirm_otp') )// end, code added by preeti on 28th mar 14 for black-box testing
					{
						// if all is well then show the page here
						
						$data['errmsg'] = '';
		
						$reg_id = $this->uri->segment(3);
						
						$regno = $this->doc_model->get_regno( $reg_id );
						
						$data['record'] = $this->doc_model->show_user_data3( $regno );
						
						$data['reg_id'] = $reg_id;
						
						$data['confirm_random'] = $this->session->userdata('confirm_random'); // below line added by preeti on 21st apr 14 for manual testing
						
						// start, code added by preeti on 23rd apr 14
						
						$sequence_token = $this->random_num();
						
						$sess_arr = array(
						
						'sequence_token' => $sequence_token
						
						);
						
						$data['sequence_token'] = $sequence_token;// the value to be set in a hidden field
						
						$this->session->set_userdata( $sess_arr );
						
						// end, code added by preeti on 23rd apr 14
													
						$this->load->view('doc_signup3', $data);			
					}
					else // else block added by preeti on 23rd apr 14 
					{
						// clear all session related vars and redirect 
						
						// unset the session variable values for registration no. and the OTP
			
						$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
						);
				
						$this->session->unset_userdata($array_items);
						
						$this->session->sess_destroy();	// added by preeti on 24th apr 14 for manual testing					
						
						redirect('doc/confirm');					
		
					}
					
				}
				else if( $res_reg->reg_status == 'c' )
				{
					// start, added on 21st apr 14 for manual testing					
					
					$this->load->helper('captcha'); 
			
					$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			  		$cap = create_captcha($vals);
					
			  		
			  		$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			  		$this->session->set_userdata($data);
			  		
			  		$data['cap_img']=$cap['image'];	
					
					// end, added on 21st apr 14 for manual testing
					
					
					$data['errmsg'] = 'Your Registration is complete';
				
				
					$data['salt'] = $this->random_num();	// code added by preeti on 22nd apr 14 for manual testing	
			
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					// add the same value in the session variable
									
					$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
					// end, code added by preeti on 22nd apr 14 for manual testing	
					
				
				
					$this->load->view('doc_confirm', $data);
				}
			
			}
			else 
			{
				// start, added on 21st apr 14 for manual testing					
					
				/*$this->load->helper('captcha'); 
			
				$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			  	$cap = create_captcha($vals);
					
			  		
			  	$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			  	$this->session->set_userdata($data);
			  		
			  	$data['cap_img']=$cap['image'];	
					
				// end, added on 21st apr 14 for manual testing
				
				$data['errmsg'] = '';
				
				$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
			
				// start, code added by preeti on 22nd apr 14 for manual testing
				
				// add the same value in the session variable
								
				$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
				// end, code added by preeti on 22nd apr 14 for manual testing	
						
			
				$this->load->view('doc_confirm', $data);*/
				
				redirect('doc/confirm');
			}	
			 
		}
		else 
		{
			// unset the session variable values for registration no. and the OTP
			
			$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
			);
				
			$this->session->unset_userdata($array_items);
			
			$this->session->sess_destroy();	// added by preeti on 24th apr 14 for manual testing	
			
			/*// start, added on 21st apr 14 for manual testing					
					
		    $this->load->helper('captcha'); 
			
		    $vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
					 );
			  		
			$cap = create_captcha($vals);
					
			  		
			$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			     	);
			  		
			$this->session->set_userdata($data);
			  		
			$data['cap_img']=$cap['image'];	
					
			// end, added on 21st apr 14 for manual testing
			
			$data['errmsg'] = 'Please fill this form and submit';
			
			$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
								
			$sess_arr = array(
								
					'confirm_salt' => $data['salt'] );
								
			$this->session->set_userdata( $sess_arr );
										
			// end, code added by preeti on 22nd apr 14 for manual testing	
						
			
			$this->load->view('doc_confirm', $data);*/	
			
			// above lines commented and below added by preeti on 21st may 14
			
			$errmsg = 'Please fill this form and submit';
			
			$errmsg = urlencode( $errmsg );
			
			redirect('doc/confirm/'.$errmsg);
		}	
			
	}

	/*public function save3() // save the third page
	{
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$ce_emp_noc_file = '';
		
		$ps_rel_order_file = '';
		
		$u_sign_file = '';
		
		if( $this->input->post('u_current_emp') == 'y' )
		{
			$this->form_validation->set_rules('ce_employer', "Current Employer", 'trim|required');
		}
			
		//$this->form_validation->set_rules('u_ncc_cert', "NCC Training Certificate", 'trim|required');
		
		$this->form_validation->set_rules('u_hobbies', "Hobbies", 'trim|required');
		
		$this->form_validation->set_rules('u_pref1', "First Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_pref2', "Second Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_pref3', "Third Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_previous_ssc', "Have you worked in AMC service before", 'required');
		
		if( $this->input->post( 'u_previous_ssc' ) == 'y' )
		{
			$this->form_validation->set_rules('ps_com_date', 'Date Of Commission', 'trim|required');
			
			$this->form_validation->set_rules('ps_rel_date', 'Date Of Release', 'trim|required');
			
		}
		
		$this->form_validation->set_rules('post_address', 'Address', 'trim|required');
		
		$this->form_validation->set_rules('post_state', "State", 'trim|required');
		
		$this->form_validation->set_rules('post_pin', "Pincode", 'trim|required');
		
		// below line commented by preeti on 27th feb 14
		
		//$this->form_validation->set_rules('post_tel', "Telephone", 'trim|required');
		
		$this->form_validation->set_rules('perm_address', "Permanent Address", 'trim|required');
		
		$this->form_validation->set_rules('perm_state', "State", 'trim|required');
		
		$this->form_validation->set_rules('perm_pin', "Pincode", 'trim|required');
		
		// below line commented by preeti on 27th feb 14
		
		//$this->form_validation->set_rules('perm_tel', "Telephone", 'trim|required');
		
		if( $this->form_validation->run() == TRUE )
		{
			// optional uploads - Copy of NOC from current employer
			
			// optional uploads - copy of release order
			
			// mandatory uploads - signature
			
			if( $this->input->post('u_current_emp') == 'y' )
			{
				$file_element_name = "ce_emp_noc";
					
				$upload_loc = 'uploads/emp_noc'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res;
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the NOC : ".$val; 
					}
				}
				
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$ce_emp_noc_file = str_replace(':', '', $errmsg);							
								
						$errmsg = ''; // all is well						
								
					}	
				}
				else
				{
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
					&& ($this->input->post( 'ce_emp_noc_hid' ) != '' ))					
					{
						$ce_emp_noc_file = $this->input->post( 'ce_emp_noc_hid' );
							
						$errmsg = ''; // all is well							
								
					}						
							
				}				 	
				
			}
			
			if( $errmsg == '' && $this->input->post('u_previous_ssc') == 'y' ) 
			{
					
				$file_element_name = "ps_rel_order";
					
				$upload_loc = 'uploads/prev_rel_order'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Release Order : ".$val; 
					}
				}	
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$ps_rel_order_file = str_replace(':', '', $errmsg);							
							
						$errmsg = ''; // all is well						
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'ps_rel_order_hid' ) != '' ))					
						{
							$ps_rel_order_file = $this->input->post( 'ps_rel_order_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
				}				
				
			}
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_sign";
					
				$upload_loc = 'uploads/sign'; 
				
				$res = $this->do_upload_sign( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						// below line modified by preeti on 5th mar 14
						
						$errmsg[$key] = "For the Signature : ".$val; 
					}
				}	
					
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_sign_file = str_replace(':', '', $errmsg);							
							
						$errmsg = ''; // all is well						
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_sign_hid' ) != '' ))					
						{
							$u_sign_file = $this->input->post( 'u_sign_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}				
											
		}
		else 
		{
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			$res_ins = $this->doc_model->save_user3( $u_sign_file, $ce_emp_noc_file, $ps_rel_order_file );	
							
			if( $res_ins )
			{
				$save = $this->input->post('save');
		
				if( $save != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}		
		
	}*/ //  code commented by preeti on 2nd apr 14 for black-box testing

	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function check_amc( $str ) // date must be before the current date
	{
		
		$cal_arr = explode('/', $str ); 			
			
		$sDateBirth = $cal_arr[2].'-'.$cal_arr[1].'-'.$cal_arr[0];		
		
		
		$current = date('Y-m-d');
		
		$oDateNow = new DateTime( $current );
			
		$oDateBirth = new DateTime( $sDateBirth );
	
		$oDateIntervall = $oDateNow->diff($oDateBirth);
	
	 	$year = $oDateIntervall->y;
		
		$invert = $oDateIntervall->invert; // if in negative then it is 0
		
		$month = $oDateIntervall->m; 
		
		$day = $oDateIntervall->d;
			
		if( intval( $year ) > 0 && $invert == 1 )
		{
		    return TRUE;
		}
		else if( intval( $year ) == 0 )
		{
			if( $invert == 0 )
			{
				$this->form_validation->set_message('check_amc', 'The value of %s field is assumed to be before the current date');
					
		    	return FALSE;	
			}
			else 
			{
				if( $month > 0 )
				{
					return TRUE;
				}
				else 
				{
					if( $day > 0 )
					{
						return TRUE;
					}
					else 
					{
						$this->form_validation->set_message('check_amc', 'The value of %s field is assumed to be before the current date');
						
						return FALSE;
					}		
				}
			}						
			
		}	
		else
		{
			$this->form_validation->set_message('check_amc', 'The value of %s field is assumed to be before the current date');
					
		    return FALSE;
		}	
		
	}
	
	
	// below code added by preeti on 2nd apr 14 for black-box testing

	public function save3() // save the third page
	{
		// validate the input values
			
		$errmsg = ''; 
		
		$file_name = '';
		
		$ce_emp_noc_file = '';
		
		$ps_rel_order_file = '';
		
		$u_sign_file = '';
		
		if( $this->input->post('u_current_emp') == 'y' )
		{
			$this->form_validation->set_rules('ce_employer', "Current Employer", 'trim|required|callback_alpha_and_space');
		}
			
		//$this->form_validation->set_rules('u_ncc_cert', "NCC Training Certificate", 'trim|required');
		
		$this->form_validation->set_rules('u_hobbies', "Hobbies", 'trim|required|callback_alpha_and_space');
		
		$this->form_validation->set_rules('u_pref1', "First Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_pref2', "Second Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_pref3', "Third Preference of Service", 'trim|required');
		
		$this->form_validation->set_rules('u_previous_ssc', "Have you worked in AMC service before", 'required');
		
		if( $this->input->post( 'u_previous_ssc' ) == 'y' )
		{
			// below line modified by preeti on 2nd apr 14
			
			$this->form_validation->set_rules('ps_com_date', 'Date Of Commission', 'trim|required|callback_check_amc');
			
			$this->form_validation->set_rules('ps_rel_date', 'Date Of Release', 'trim|required|callback_check_amc');
			
		}
		
		//$this->form_validation->set_rules('post_address', 'Address', 'trim|required|callback_alpha_num_dash');
		
		// above line commented and below line added by preeti on 22nd may 14 
		
		$this->form_validation->set_rules('post_address', 'Address', 'trim|required|callback_alpha_address');
		
		$this->form_validation->set_rules('post_state', "State", 'trim|required');
		
		// below line modified by preeti on 4th apr 14
		
		$this->form_validation->set_rules('post_pin', "Postal Address Pincode", 'trim|required|integer|min_length[6]|max_length[6]');
		
		// below line added by preeti on 28th apr 14
		
		if( $this->input->post('post_tel') != ''  )
		{
			$this->form_validation->set_rules('post_tel', "Telephone", 'trim|required|callback_alpha_num_dash');
						
		}
		
		
		
		//$this->form_validation->set_rules('perm_address', "Permanent Address", 'trim|required|callback_alpha_num_dash');
		
		// above line commented and below line added by preeti on 22nd may 14
		
		$this->form_validation->set_rules('perm_address', "Permanent Address", 'trim|required|callback_alpha_address');
		
		$this->form_validation->set_rules('perm_state', "State", 'trim|required');
		
		// below line modified by preeti on 4th apr 14
		
		$this->form_validation->set_rules('perm_pin', "Permanent Address Pincode", 'trim|required|integer|min_length[6]|max_length[6]');
		
		// below line commented by preeti on 27th feb 14
		
		if( $this->input->post('perm_tel') != ''  )
		{
			$this->form_validation->set_rules('perm_tel', "Telephone", 'trim|required|callback_alpha_num_dash');
		}
		
		if( $this->form_validation->run() == TRUE )
		{
			// below if added by preeti on 21st apr 14 for manual testing	
				
			if( $this->input->post('confirm_random') != ''  && strlen( $this->input->post('confirm_random') ) == 32   
			
			&& $this->session->userdata('confirm_random') != ''   && ( $this->input->post('confirm_random') == $this->session->userdata('confirm_random') ) ) 
			{			
			
			// optional uploads - Copy of NOC from current employer
			
			// optional uploads - copy of release order
			
			// mandatory uploads - signature
			
			if( $this->input->post('u_current_emp') == 'y' )
			{
				$file_element_name = "ce_emp_noc";
					
				$upload_loc = 'uploads/emp_noc'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res;
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the NOC : ".$val; 
					}
				}
				
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$ce_emp_noc_file = str_replace(':', '', $errmsg);							
								
						
						// start, code added by preeti on 2nd apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
									
						// if it is not a pdf file then delete the file and show the message as file not pdf
									
						$path = 'uploads/emp_noc/'.$ce_emp_noc_file;
									
						$this->load->helper('file');
									
						$string = read_file( $path );
							
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																		
								// shoot an update query here
								
								$errmsg = ''; // all is well												
											
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
											
								$file_path = 'uploads/emp_noc/'.$ce_emp_noc_file;
											
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
											
								$errmsg = "For the NOC : Uploaded File is not valid ";
											
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
											
							$file_path = 'uploads/emp_noc/'.$ce_emp_noc_file;
											
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
											
							$errmsg = "For the NOC : Uploaded File is not valid ";		
						}						
									
									
						// end, code added by preeti on 2nd apr 14 for black-box testing	
						
												
								
					}	
				}
				else
				{
					if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
						
					&& ($this->input->post( 'ce_emp_noc_hid' ) != '' ))					
					{
						$ce_emp_noc_file = $this->input->post( 'ce_emp_noc_hid' );
							
						$errmsg = ''; // all is well							
								
					}						
							
				}				 	
				
			}
			
			if( $errmsg == '' && $this->input->post('u_previous_ssc') == 'y' ) 
			{
					
				$file_element_name = "ps_rel_order";
					
				$upload_loc = 'uploads/prev_rel_order'; 
				
				$res = $this->do_upload( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						$errmsg[$key] = "For the Release Order : ".$val; 
					}
				}	
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$ps_rel_order_file = str_replace(':', '', $errmsg);							
							
						
						
						// start, code added by preeti on 2nd apr 14 for black-box testing
						
						// read the file and verify if it is actually a pdf file
									
						// if it is not a pdf file then delete the file and show the message as file not pdf
									
						$path = 'uploads/prev_rel_order/'.$ps_rel_order_file;
									
						$this->load->helper('file');
									
						$string = read_file( $path );
							
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																		
								// shoot an update query here
								
								$errmsg = ''; // all is well												
											
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
											
								$file_path = 'uploads/prev_rel_order/'.$ps_rel_order_file;
											
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
											
								$errmsg = "For the Release Order : Uploaded File is not valid ";
											
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
											
							$file_path = 'uploads/prev_rel_order/'.$ps_rel_order_file;
											
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
											
							$errmsg = "For the Release Order : Uploaded File is not valid ";		
						}						
									
									
						// end, code added by preeti on 2nd apr 14 for black-box testing	
							
												
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'ps_rel_order_hid' ) != '' ))					
						{
							$ps_rel_order_file = $this->input->post( 'ps_rel_order_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
				}				
				
			}
			
			if( $errmsg == '' ) 
			{
					
				$file_element_name = "u_sign";
					
				$upload_loc = 'uploads/sign'; 
				
				$res = $this->do_upload_sign( $file_element_name, $upload_loc ); 	
				
				$errmsg = $res; 
				
				if( is_array( $errmsg ))
				{
					foreach( $errmsg as $key=>$val )
					{
						// below line modified by preeti on 5th mar 14
						
						$errmsg[$key] = "For the Signature : ".$val; 
					}
				}	
					
					
				if( !is_array( $errmsg ))
				{
					if( strpos( $errmsg, ':' ) !== FALSE )
					{
						$u_sign_file = str_replace(':', '', $errmsg);							
							
						$errmsg = ''; // all is well						
							
					}	
				}
				else
				{
						if( (strpos( $errmsg['error'], 'You did not select a file to upload.') !== FALSE ) 
					
					&& ($this->input->post( 'u_sign_hid' ) != '' ))					
						{
							$u_sign_file = $this->input->post( 'u_sign_hid' );
						
							$errmsg = ''; // all is well							
							
						}						
						
				}				
				
			}

			} // block added by preeti on 21st apr 14 for manual testing
			else // block added by preeti on 21st apr 14 for manual testing
			{
				$errmsg = 'Values couldnot be saved';
			}				
											
		}
		else 
		{
				if( validation_errors() )
				{
					$errmsg = validation_errors();
				}
		}
		
		if( $errmsg != ''  )
		{
			if( is_array( $errmsg ) )
			{
				foreach( $errmsg as $val )
				{
					echo $val.'<br />';
				}	
			}
			else
			{
				echo $errmsg;
			}		
			
		}
		else 
		{
			// save the values in the db
			
			$res_ins = $this->doc_model->save_user3( $u_sign_file, $ce_emp_noc_file, $ps_rel_order_file );	
							
			if( $res_ins )
			{
				$save = $this->input->post('save');
		
				if( $save != '' )
				{
					echo 'continue';
				}	
			}
			else 
			{
				$errmsg = 'Values couldnot be saved';
				
				echo $errmsg;
			}
			
							
		}		
		
	}



	public function final_saved() // when saved completely
	{
		// below line mdoified by preeti on 25th apr 14
					
		if(  $this->session->userdata('sequence_token') != '' && $this->session->userdata('sequence_token') != '' && ( $this->session->userdata('sequence_token') == $this->uri->segment('4') ) ) // if/else block added by preeti on 23rd apr 14
		{
			
			// start, code added by preeti on 4th mar 14
			
			// unset the session variable values for registration no. and the OTP
			
			$array_items = array(
			
			'is_confirm_logged' => '', 
			
			'confirm_regno' => '', 
			
			'confirm_otp' => '', 
			
			'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
			
			'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
			
			'sequence_token' => '', // added by preeti on 23rd apr 14 for manual testing
			
			'confirm_salt' => '');
	
			$this->session->unset_userdata($array_items);
			
			$this->session->sess_destroy();	// added by preeti on 24th apr 14 for manual testing
				
			// end, code added by preeti on 4th mar 14	
				
			// send an email and sms( mobile ) with the registration number to the user
			
			$reg_id = $this->uri->segment(3);
			 
			$reg_obj = $this->doc_model->get_reg_detail( $reg_id ); 
			 
			//$reg_pass = 'pass'.$reg_id; // commented by preeti on 2nd apr 14
			
			
			$random_num = $this->random_num(4);
			
			//$reg_pass = 'Pass@'.$this->random_num(4); // commented by preeti on 3rd apr 14
			
			$reg_pass = 'Pass@'.$this->random_num(2).$this->random_only_num(2); // added by preeti on 3rd apr 14  
			
			// update the password in the db
			
			$this->doc_model->update_reg_pass( $reg_id, $reg_pass );
			
			$msg = "Hello, your Reg No. is ".$reg_obj->reg_regno.", Your password is ".$reg_pass;
								
			$encoded_msg = urlencode($msg);
								
			$dest_num = $reg_obj->reg_mobile;
								
			//$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$encoded_msg."&mnumber=".$dest_num."&signature=NICSUP";
	
			// 23rd may 14
					
			$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$encoded_msg."&mnumber=".$dest_num."&signature=NICSUP";
					
	
			//$res_sms = file_get_contents( $url );
			
			$res_sms = $this->send_sms_curl( $encoded_msg, $dest_num );	//23rd may 14 
			
				
			// send an email with the link to proceed further
								
				$reg_email = $reg_obj->reg_email;
									
				$config = array(
				
					'protocol' => 'smtp',
										
					'smtp_host' => 'relay.nic.in',
										
					'smtp_port' => 25,
										
					'mailtype' => 'html',
					        
					'newline' => '\r\n',
					        
					'charset' => 'utf-8' //default charset				
									
				);
				
				
				/*$config = Array(
			    
					    'protocol' => 'smtp',
					    
					    'smtp_host' => 'ssl://smtp.googlemail.com',
					    
					    'smtp_port' => 465,
					    
					    'smtp_user' => 'rahejapreetiemail@gmail.com',
					    
					    'smtp_pass' => '',
					    
					    'mailtype'  => 'html', 
					    
					    'charset'   => 'iso-8859-1'
						
						);*/// commented by preeti on 16th may 14	
				
									
				$this->load->library('email', $config);
									
				$this->email->set_newline("\r\n");
				
				// get the admin email id to set the from email id
							
				$admin_email = $this->doc_model->get_admin_email();
										
				if( !$admin_email || $admin_email == '' )
				{
					$admin_email = "dirafmsp-mod@nic.in"; // mail id changed by preeti on 16th may 14	
				}
									
				$this->email->from($admin_email, 'AMC');
									
				$this->email->to( $reg_email );		
									
				$this->email->subject('Registration Success');
				
				// below message added by preeti on 25th apr 14 for manual testing
				
				$msg = "Your Login Details have been sent to your Registered Mobile.";
									
				$this->email->message($msg);
									
				$this->email->send();	
				
				// below code commented for black-box testing by preeti on 26th mar 14
				
				//$data['errmsg'] = "Form Submitted succesfully. Check your email/mobile for your Registration No. and Password and keep it safe for future reference "; 
				
				// start, code commented for black-box testing by preeti on 26th mar 14
				
				$errmsg = "Form Submitted succesfully. Check your email/mobile for your Registration No. and Password and keep it safe for future reference ";
				
				//$errmsg .= "reg no. is ".$reg_obj->reg_regno." password is ".$reg_pass;
				
				$data['errmsg'] = $errmsg;
				
				// end, code commented for black-box testing by preeti on 26th mar 14
				
				$this->load->view('doc_saved', $data);
				
		}
		else // else block added by preeti on 23rd apr 14 
		{
			
			// clear all session related vars and redirect 
						
			// unset the session variable values for registration no. and the OTP
			
			$array_items = array(
						
						'is_confirm_logged' => '', 
						
						'confirm_regno' => '', 
						
						'confirm_otp' => '', 
						
						'confirm_regid' => '', 
						
						'confirm_random' => '', // added by preeti on 21st apr 14 for manual testing
						
						'ci_session' => '', // added by preeti on 22nd apr 14 for manual testing
						
						'confirm_salt' => '',
						
						'sequence_token' => '' // added by preeti on 23rd apr 14 for manual testing
						
						);
				
			$this->session->unset_userdata($array_items);	
			
			$this->session->sess_destroy();	// added by preeti on 24th apr 14 for manual testing				
						
			redirect('doc/confirm');					
		
		}
				
		
	}

	public function clear_s() // added for testing purpose by preeti on 28th mar 14
	{
		$this->session->sess_destroy();
	}
	
	
	// below code added on 2nd apr 14 by preeti for black-box testing
	
	function alpha_and_space($str) // only for alpha and space
	{
	    //if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    if( ! preg_match("/^([-a-z,_ ])+$/i", $str) ) // above line commented and this line added by preeti on 15th may 14
	    {
	    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	
	
	// below code added on 22nd may 14 by preeti
	
	function alpha_address($str) // for alpha-numeric with space, dash, underscore, forward slash
	{
	    if( ! preg_match("/^([-a-z0-9_,-\s\/])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_address', 'The %s field may only contain valid characters like alpha-numeric, dash, underscore, slash, comma etc.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	
	// 23rd may 14
	function send_sms_curl( $message, $mnumber )
	{
		
		$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$message."&mnumber=".$mnumber."&signature=NICSUP"; 
		
		//although we have used https, you can also use http
		
		$ch = curl_init();
		
		//initialize curl handle 
		
		curl_setopt($ch, CURLOPT_URL, $url); 
		
		//set the url
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		//return as a variable 
		
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//set POST method 
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
		
		//set the POST variables
		
		$response = curl_exec($ch); 
		
		//run the whole process and return the response
		
		curl_close($ch); 
		
		if(strpos($response, "Message Accepted") === FALSE )
		{
			return FALSE;	
		}
		else 
		{
			return TRUE;
		}
		
	}
	
	
	function sms_check()
	{
		/*//$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=hi&mnumber=9810469060&signature=NICSUP";
		
		$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=hi&mnumber=919810469060&signature=NICSUP"; 
 	
		$res_sms = file_get_contents( $url ); 
		
		echo " response is ".$res_sms;	
		
		if( $res_sms )
		{
			echo " sms sent ";
		}
		else 
		{
			echo " sms not sent ";	
		}
		
		
		
		$mnumber='9810469060';
		
		$username="nicsup.auth";
		
		$pin="Tp%24m7%23Rc8";
		
		$signature="NICSUP";
		
		$message="hi";
		
		

		$r = new HttpRequest('http://smsgw.nic.in/failsafe/HttpLink?', HttpRequest::METH_GET);
		
		exit;
		
		$r->addQueryData(array('username' => $username,"pin"=>$pin,"message"=>$message,"mnumber"=>$mnumber,"signature"=>$signature));
		
		echo $r->send();
		
		exit;

		try {
		    $r->send();
		    if ($r->getResponseCode() == 200) {
		        print $r->getResponseBody();
		    }
		} catch (HttpException $ex) {
		    echo $ex;
		}*/
		
		
		
		$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=hi&mnumber=919810469060&signature=NICSUP"; 
		
		//although we have used https, you can also use http
		
		$ch = curl_init();
		
		//initialize curl handle 
		
		curl_setopt($ch, CURLOPT_URL, $url); 
		
		//set the url
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		//return as a variable 
		
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//set POST method 
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
		
		//set the POST variables
		
		$response = curl_exec($ch); 
		
		echo $response;
		
		//run the whole process and return the response
		
		curl_close($ch); 
		
	}
	
	
}	