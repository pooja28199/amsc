$(document).ready(function() {
		
	$('.sbtn').live('click', function(e){

		var code=$('#code').val();
		var text=$('#label').val();
		var base_url = $('#base_url').val();
		
		$.post(base_url+'backend/managecenters/addCenter',
   		
	   		{ 'text': text,'code': code },
	   		
	   		function( result)
	   		{   			
	   			window.location.reload(true);
	   		}
	   		
	   	);
				
		return false; 
	
	});	

});


function del(id)
{
	var base_url = $('#base_url').val();
	
	$.post(base_url+'backend/managecenters/deleteCenter',
   		
	   		{ 'id': id },
	   		
	   		function( result)
	   		{   			
	   			$('#center_'+id).hide();
	   		}
	   		
	   	);
}