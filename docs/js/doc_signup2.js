$(document).ready(function() {

var bar = $('.bar');

var percent = $('.percent');

var status = $('#status');

var base_url = $('#base_url').val();

var reg_id = $('#reg_id').val();

var u_intern_complete = $('input[name=u_intern_complete]:checked').val();

var u_is_pg = $('input[name=u_is_pg]:checked').val();

var pg_count = $('#pg_count').val(); // number of pg qualifications added in the db 

var confirm_random = $('#confirm_random').val(); // added by preeti on 22nd apr 14 for manual testing

var sequence_token = $('#sequence_token').val(); // added by preeti on 23rd apr 14 for manual testing

/* 

$('#upload_form').ajaxForm({
	
    beforeSend: function() {
    
        status.empty();
    
        var percentVal = '0%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
		document.getElementById("loading").style.display="block";

    },
    
    uploadProgress: function(event, position, total, percentComplete) {
    
        var percentVal = percentComplete + '%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
    },
  
  complete: function(xhr) {
	  	  document.getElementById("loading").style.display="none";
	//  document.getElementById("status").style.display="block";
	
	   document.getElementById("status").style.border="1px solid #ddd";
	 document.getElementById("status").scrollTop;
	//  document.getElementById("fullright").scrollIntoView();
 
  
    status.html(xhr.responseText);// if there are validation errors then show then on the same screen     
    
  	if( xhr.responseText == 'exit' )
  	{
  		//document.location = base_url+'doc/doc_saved/'+reg_id;   // line commented by preeti on 23rd apr 14 for manual testing
  		
  		document.location = base_url+'doc/doc_saved/'+reg_id+'/'+sequence_token;// line added by preeti on 23rd apr 14 for manual testing
  		  				
  	}
  	else if( xhr.responseText == 'continue' )
  	{
  		//document.location = base_url+'doc/doc_signup3/'+reg_id;  // line commented by preeti on 23rd apr 14 for manual testing
  		
  		document.location = base_url+'doc/doc_signup3/'+reg_id+'/'+sequence_token; // line added by preeti on 23rd apr 14 for manual testing
  		  		
  	}
	else
	{
		
	//document.getElementById('status').scrollIntoView();
	  $("html, body").animate({scrollTop: $("#status").offset().top}, 1000); 	
	}
  
  },
	  
   error: function(response, status, err){
        
        
    }
	

}); */
	
    $('#foreign_grad').live('click', function(e) 
    {
    	if( $('#foreign_grad').is(':checked') )
    	{
    		$('#u_basic_qual').val('MBBS');	
    		$('#u_basic_qual_hidden').val('MBBS');	
			$('#u_basic_qual').attr('disabled',true);	
    		
    		alert("Please proceed only if you have Permanent Registration number issued by MCI");	
    	}    		
    	
    });
    
    
    // below code added by preeti on 2nd apr 14
    
    $('#indian_grad').live('click', function(e) 
    {   	
    	if( $('#indian_grad').is(':checked') )
    	{
    		$('#u_basic_qual').val('MBBS');
			$('#u_basic_qual_hidden').val('MBBS');	
			$('#u_basic_qual').attr('disabled',true);	
    	}  	
    	
    });
    
    
    
    if( u_intern_complete == 'y' )
	{
		$('#intern_like_div').hide();
		
		$('#intern_div').show();			
			
	}
	else if( u_intern_complete == 'n' )
	{
		$('#intern_div').hide();	
		
		$('#intern_like_div').show();	
		
	}
	else
	{
		$('#intern_div').hide();	
		
		$('#intern_like_div').hide();
	}	
    
    
    $('input[name=u_intern_complete]:checked').live('click', function(e)
    {
    	var value = $('input[name=u_intern_complete]:checked').val();
		
		if( value == 'y' )
		{
			// show the div
			
			$('#intern_div').show();
			
			$('#intern_like_div').hide();
			
			// below lines are added by preeti on 3rd mar 14
		
			// clear all the no related fields
			
			$('#u_intern_date_likely').val('');
		}
		else if( value == 'n' )
		{
			// hide the div
			
			$('#intern_div').hide();
			
			$('#intern_like_div').show();
			
			// below lines are added by preeti on 3rd mar 14
		
			// clear all the yes related fields
			
			$('#u_intern_date').val('');
			
			$('#u_intern_cert').val('');
			
			$('#intern_upload').hide();	
		}
    });
    
    
    
    if( u_is_pg == 'y' )
	{
		$('#pg_div').show();	
	}
	else
	{
		$('#pg_div').hide();	
	}	
    
    
    $('input[name=u_is_pg]:checked').live('click', function(e)
    {
    	var value = $('input[name=u_is_pg]:checked').val();
		
		if( value == 'y' )
		{
			// show the div
			
			$('#pg_div').show();
		}
		else if( value == 'n' )
		{
			// hide the div
			
			$('#pg_div').hide();
			
			// clear all the fields inside pg_div div
			
			$('#pg_degree').val('');
			
			$('#pg_subject').val('');
			
			$('#pg_college').val('');
			
			$('#pg_univ').val('');
			
			$('#pg_year').val('');
			
			$('input[name=pg_mci_recog]:checked').each(function()
			{				
				$(this)[0].checked = false;
    		
    		});
    		
    		$('#pg_cert').val('');	
    		
    		$('#pg_upload').hide();	
    		
    		
    		$('input[name=add_pg1]:checked').each(function()
			{				
				$(this)[0].checked = false;
    		
    		});
    		
    		
		}
    });
    
    // hide the add more pg blocks by default
    
    if(pg_count >= 2  )
    {
    	$('#add_pg1_div').show();	
    }
    else
    {
    	$('#add_pg1_div').hide();
    }
    
    if( pg_count == 3  )
    {
    	$('#add_pg2_div').show();	
    }
    else
    {
    	$('#add_pg2_div').hide();
    }
    
    $('input[name=add_pg1]:checked').live('click', function(e)
    {
    	var value = $('input[name=add_pg1]:checked').val();
		
		if( value == 'y' )
		{
			$('#add_pg1_div').show();
		}
		else if( value == 'n' )
		{
			$('#add_pg1_div').hide();
			
			// below lines added by preeti n 27th feb 14
			
			// make all the values empty for the fields inside this block
			
			$('#pg_degree1').val('');
			
			$('#pg_subject1').val('');
			
			$('#pg_college1').val('');
			
			$('#pg_year1').val('');
			
			$('#pg_univ1').val('');	
			
			$('input[name=pg_mci_recog1]:checked').each(function()
			{				
				$(this)[0].checked = false;
    		
    		});	
    		
    		$('#pg_upload1').hide();
			
			
		}
    });
    
   	// #remove_pg1 click removed from here by preeti on 27th feb 14
    
    $('input[name=add_pg2]:checked').live('click', function(e)
	{
	   	var value = $('input[name=add_pg2]:checked').val();
			
		if( value == 'y' )
		{
			$('#add_pg2_div').show();
		}
		else if( value == 'n' )
		{
			$('#add_pg2_div').hide();
			
			// below lines added by preeti n 27th feb 14
			
			// make all the values empty for the fields inside this block
			
			$('#pg_degree2').val('');
			
			$('#pg_subject2').val('');
			
			$('#pg_college2').val('');
			
			$('#pg_year2').val('');
			
			$('#pg_univ2').val('');
			
			
			$('input[name=pg_mci_recog2]:checked').each(function()
			{				
				$(this)[0].checked = false;
    		
    		});
    		
    		$('#pg_upload2').hide();					
			
		}
	});
	
	// #remove_pg2 click removed from here by preeti on 27th feb 14
           

} );