<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/left_user_out'); ?>
<div id="fullright">

	<h1>ALL NOTIFICATIONS</h1>

    <div class="contentsquaresmall">
		
		<ul>
		<?php foreach( $records as $record ){ 
		
		if(strtoupper($record->not_author)=='FLASH')
		continue;
		
		if( $record->not_type == 'c' )
		$url = base_url().'doc/not/'.$record->not_id;
		else if( $record->not_type == 'l' )
		$url = base_url().'uploads/not/'.$record->not_link;
		
		?>
		<li>
			<h2>
				<a class="alink" href="<?php echo $url; ?>">
				<?php echo strtoupper( $record->not_title ).' updated on '.db_to_calen( $record->not_date ) ; ?>
				</a>
			</h2>     				
		</li>
		<?php } ?>
		</ul>
		
    </div>      

    </div>

<?php $this->load->view('template/footer'); ?>
