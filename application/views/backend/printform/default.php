<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Print Form</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />



<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">
<div class="listsquaresmall2">
     

          <p><h2>Print Form</h2></p>
          
          <?php
          
          
         
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>         
          
           		<form action="<?php echo base_url(); ?>backend/printform/downloadform" method="post">
          		
				<table cellspacing="5" cellpadding="5" align="center">
				
                
                <tr >
				<td>Range From </td>
				<td>
				<input type="text"  name="from" value=""  />
				</td>
				</tr>
				
				<tr >
				<td>Range To </td>
				<td>
				<input type="text" name="to" value=""    />
				</td>
				</tr>
				
				
				
				
                
                
                
                
                <tr>
				<td></td>
				<td><input type="submit" value="submit" name="submit" /></td>
				</tr>
				</table>
				
          			
          		
          	</form>         
          
        
          				
          
           		
          

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#idate').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			//$('#to').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>