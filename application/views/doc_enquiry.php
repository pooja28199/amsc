 <?php $this->load->view('template/header'); ?>
 <?php $this->load->view('template/left_user_out'); ?>
 
 <style>
 .contact{font-size:13px;}
 .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
	border:1px solid #993300;
	color:#fff;
}
.button:hover{
	background: #663333 none repeat scroll 0 0;
	color:#fff;
}
div.error{
	margin-bottom:20px;
	border:none !important;
	text-align:left;
}
div.error p{
	display: block;
	font-weight:bold;
    font-size: 14px;
    line-height: 14px;
    margin: 0;
	color:red;
}
div.error p.green{color:green;}
 </style>
 
 <div id="fullright">
 <div class="contentsquaresmall">

	<p><h1>GET IN TOUCH / FFEDBACK</h1></p>
   
	<p><strong><?php echo strtoupper("We welcome your feedback and suggestions about the Portal to help us improve further and serve you better."); ?></strong></p>
   
	<div class="error">
				
	<?php 
	
		if( validation_errors() )
		echo (validation_errors());
		else 
		echo '<p class="green">'.urldecode($errmsg).'</p>';	
	
	?>
	
	</div>
   
    <form method="post" action="sendquery">
	<table class="contact" width="500" cellspacing="0" cellpadding="3">
	<tbody>
		<tr>
			<th align="left">
				<label>FULL NAME<span class="error">*</span></label>
			</th>
			<td colspan="2"><input type="text" name="name"></td>
		</tr>
		<tr>
			<th align="left">
				<label>EMAIL<span class="error">*</span></label>
			</th>
			<td colspan="2"><input type="email" name="email"></td>
		</tr>
		<tr>
			<th align="left">
				<label>PHONE<span class="error">*</span></label>
			</th>
			<td colspan="2"><input type="text" name="mobile"></td>
		</tr>
		<tr>
			<th align="left">
				<label>MESSAGE<span class="error">*</span></label>
			</th>
			<td colspan="2"><textarea name="msg" cols="40" rows="5"></textarea></td>
		</tr>
		<tr>
			<th align="left">
				<label>CAPTCHA<span class="error">*</span></label>
			</th>
			<td>
				<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
			</td>
			<td><?php echo $cap_img; ?></td>
		</tr>
		<tr>
			<th align="left">&nbsp;</th>
			<td colspan="2"><input class="button" type="submit" value="Send Enquiry" name="subm" /></td>
		</tr>
	</tbody>
	</table>
	</form>
	
 </div>      
 </div>
 
 <?php $this->load->view('template/footer'); ?>
 