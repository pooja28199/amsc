<?php

$url = '';

$url_sort = '';

		if( isset( $keyword ) &&  $keyword != '' )
		{
			$url .= "/keyword/".$keyword;
		}
		
		
		
		
		if( isset( $gender ) && $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		 
		
		
		if(  isset( $state ) && $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		
		if( isset( $start_date ) && $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		
		if( isset( $end_date ) && $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		
		if( isset( $start_age ) && $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		
		if( isset( $end_age ) && $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		
		if( isset( $sort_by ) && $sort_by != '' ) // block added by preeti on 27th feb 14
		{
			$url_sort .= "/sort_by/".$sort_by;
		}   
		
		
		
		if( isset( $sort_order ) && $sort_order != '' ) // block added by preeti on 27th feb 14
		{
			$url_sort .= "/sort_order/".$sort_order;
		}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: User's List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />



<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">
<div class="listsquaresmall2">
     

          <p><h2>List of Users</h2></p>
          
          <?php
          
          
          if( is_array($records)  && COUNT( $records ) == 0  )
		  {
		  ?>	
		  	<span>No Users Found !</span>
		  <?php
		  }
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>         
          
          <form action="<?php echo base_url(); ?>admin/doc_list" method="post">
          
          		<div class="s-bind-col" >
          			
          			<div class="s-left-col"><span class="txt-label">Keyword</span></div>
          			
          			<div class="s-right-col">
          				
          				<!-- below line modified by preeti on 21st apr for the manual testing -->
          				
          				<input style="width:130px;" type="text" <?php echo 'autocomplete="off"'; ?> name="keyword" value="<?php if(  $this->input->post('keyword') != '' ){ echo $this->input->post('keyword'); }else{ echo $keyword; }?>" id="keyword" />
          				         				
          			</div>         			
          			
          			<div class="s-right-col"><span class="small">( Name, Reg No., Email, Mobile )</span></div>		        			
          			
          		</div>   
          		
          		
          		<div class="s-bind-col" >
          			
          			<div class="s-left-col"><span class="txt-label">State</span></div>
          			
          			<div class="s-right-col">
          				
          				<select style="width: 130px;" name="state" id="state">
          						
          					  <option value="">Select</option>
			      				
			      				<?php
			      				$res_st = $this->db->get('state');
								
								foreach( $res_st->result() as $val )
								{						
			      				?>
			      				
			      					<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
			      				
			      					<option <?php if( $this->input->post('state')  != ''  && $this->input->post('state') == $val->state_id  ){ echo "selected"; }else if( $state == $val->state_id ){ echo "selected"; }  ?> value="<?php echo $val->state_id; ?>"><?php echo $val->state_name; ?></option>
			      				
			      				<?php
								}
			      				?>      				
			      				      				
	      			
          						
          					</select>
          				
          			</div>
          			
          			<div class="s-left-col"><span class="txt-label">Gender</span></div>
          			
          			<div class="s-right-col" >
          				          				
          				<select style="width: 130px;" name="gender" id="gender">
          						
	          				<option value="">Select</option>
	          						
	          				<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->		
	          						
	          				<option <?php if(  $this->input->post('gender')  != ''  && $this->input->post('gender') == 'm'  ){ echo "selected"; }else if( $keyword == 'm' ){ echo "selected"; } ?> value="m">Male</option>
	          						
	          				<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->		
	          						
	          				<option <?php if( $this->input->post('gender')  != ''  && $this->input->post('gender') == 'f'  ){ echo "selected"; }else if( $keyword == 'f' ){ echo "selected"; }  ?> value="f">Female</option>
	          						
          				</select>
          				
          		    </div>         			
          			
          		</div>   
          		    		
          		
          		
          		<div class="s-bind-col" >
          			
          			<div class="s-left-col"><span class="txt-label">Reg. Start</span></div>
          			
          			<div class="s-right-col">
          				
          				<!-- below line modified by preeti on 15th apr 14 for the white box bug fixing -->
          				
          				<input  style="width:130px;" readonly="readonly" name="start_date" id="start_date" value="<?php if(  $this->input->post('start_date')  != '' ){ echo $this->input->post('start_date'); }else { echo db_to_calen( $start_date ) ; }?>" type="text" />
          				
          					<span  style="display: none;">
				
								<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
							</span>	
         				
          			</div>
          			
          			<div class="s-left-col"><span class="txt-label">Reg. End</span></div>
          			
          			<div class="s-right-col" >         				
          				
          					<!-- below line modified by preeti on 15th apr 14 for the white box bug fixing -->
          					
          					<input  style="width:130px;" readonly="readonly" name="end_date" value="<?php if( $this->input->post('end_date')  != '' ){ echo $this->input->post('end_date'); }else { echo db_to_calen( $end_date ); }?>" id="end_date" type="text" />
          					
          					<span  style="display: none;">
				
								<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
							</span>
          				
          				
          		    </div>         			
          			
          		</div>  
          		
          		
          		
          		<div class="s-bind-col" >
          			
          			<div class="s-left-col"><span class="txt-label">Start Age</span></div>
          			
          			<div class="s-right-col">
          				
          				<!-- below line modified by preeti on 21st apr 14 for the manual testing -->
          				
          				<input maxlength="3" <?php echo 'autocomplete="off"'; ?>  style="width:130px;" name="start_age" id="start_age" type="text" value="<?php if( $this->input->post('start_age')  != '' ){ echo $this->input->post('start_age'); }else{ echo $start_age; }?>" />	
         				
          			</div>
          			
          			<div class="s-left-col"><span class="txt-label">End Age</span></div>
          			
          			<div class="s-right-col" >         				
          				
          					<!-- below line modified by preeti on 21st apr 14 for the manual testing -->
          				
          					<input maxlength="3" <?php echo 'autocomplete="off"'; ?>  style="width:130px;" name="end_age" id="end_age" type="text" value="<?php if( $this->input->post('end_age')  != '' ){ echo $this->input->post('end_age'); }else{ echo $end_age; }?>" />          				
          				
          		    </div>         			
          			
          		</div> 
          		
          		
          		
          		<div class="s-bind-col" >
          			
          			<div class="s-left-col">&nbsp;</div>
          			
          			<div class="s-right-col">&nbsp;</div>
          			
          			<div class="s-left-col"><input type="submit" name="sub" value="Search" /></div>
          			         			
          		</div>     
          		
          		      		
          		
          	</form>         
          
          <?php
          
          $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
          $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$i = $uri_arr['offset'] + 1;
			
			$offset = $uri_arr['offset'] ;
				
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	  // add a link to download as excel	
			  
			  $files=array();	 
				
			  $keyword = $this->input->post('keyword');
			
			  if( $this->input->post( 'keyword' ) )
			  {
				$keyword = $this->input->post( 'keyword' );	
					
			  }
			  else
			  {
			  	$keyword =0;
			  }
			 
			  array_push($files,$keyword);  
		
		      $gender = '';
		
			  if( $this->input->post( 'gender' ) )
			  {
				$gender = $this->input->post(  'gender' );
							
			  }
			  else
			  {
			  	$gender =0;
					
			  }
		 
		      array_push($files,$gender);
			  
			  $state = '';
				
			  if( $this->input->post( 'state' ) )
			  {
				$state = $this->input->post( 'state' );
							
			  }
			  else
			  {
			  	$state =0;
					
			  }
		      
		    array_push($files,$state);
			  
			$start_date = '';
			
			if( $this->input->post( 'start_date' ) )
			{
				$start_date = $this->input->post( 'start_date' );
				$start_date=date("m.d.Y", strtotime($start_date));						
			}
			else
			{
				$start_date ='0000.00.00';
					
			}
		 
		 	array_push($files,$start_date);
		
			$end_date = '';
		
			if( $this->input->post( 'end_date' ) )
			{
				$end_date = $this->input->post( 'end_date' );
				$end_date=date("m.d.Y", strtotime($end_date));		
			}
			else {
				$end_date ='0000.00.00';
			}
			
			array_push($files,$end_date);
			
			$start_age = '';
		
			if( $this->input->post( 'start_age' ) )
			{
				$start_age = $this->input->post( 'start_age' );
						
			}
			else
			{
				$start_age =0;
					
			}
		
			array_push($files,$start_age);
			
			$end_age = '';
			
			if( $this->input->post( 'end_age' ) )
			{
				$end_age = $this->input->post( 'end_age' );
						
			}
			else
			{
				$end_age =0;
					
			}
		
			array_push($files,$end_age);
		
		
		
			$url1 = base_url().'admin/file_pdf';	
			  
			  
			$co = implode("-", $files);
			  
			  
			if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			{
			  $encoded = urlencode( $keyword ); // encoded keyword
				
			  $url1 = base_url().'admin/file_pdf/'.$co; 
			}
			
			$url31 = base_url().'admin/file_xls';	
			  
			if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			{
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url31 = base_url().'admin/file_xls/'.$co;
				 
			}
			
			$url21 = base_url().'admin/file_pdf1';	
			  
			if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			{
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url21 = base_url().'admin/file_pdf1/'.$co;
				 
			}
			
			$url221 = base_url().'admin/file_pdf4';	
			  
			if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			{
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url221 = base_url().'admin/file_pdf4/'.$co;
				 
			}
			
			//$url211 = base_url().'admin/dozipall';
			
			// above line commented and below added by preeti on 3rd jul 14
			
			$url211 = base_url().'admin/dozipall/offset/'.$offset.'/limit/'.$per_page.$url_sort;
						
			$this->table->add_row(
			
			'<a class="link" href="'.$url1.'">Print</a>', 
			
			'<a class="link" href="'.$url21.'">Export</a>',
			
			
			
			'<a class="link" href="'.base_url().'admin/file_xls'.$url.$url_sort.'">Export Excel</a>', // above line commented and this line added by preeti on 27th feb 14 
			
			'<a class="link" href="'.$url211.'">zipall</a>', 
			
			'&nbsp;',
			
			'&nbsp;',
			
			'&nbsp;'
			  
			  );
			
  			// add the list heading for columns	
				
			$this->table->add_row(
			
			'<span class="col-label">S.No</span>', 
			
			//'<strong>Select</strong>', // commented by preeti on 26th feb 14
			
			'<a href="'.base_url().'admin/doc_list/sort_by/u_fname/sort_order/ASC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/asc.png" /></a><span class="col-label">Name</span><a href="'.base_url().'admin/doc_list/sort_by/u_fname/sort_order/DESC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/desc.png" /></a>', 
			  
			'<a href="'.base_url().'admin/doc_list/sort_by/u_regno/sort_order/ASC/offset/'.$offset.$url.'"><img  border="0" src="'.base_url().'images/asc.png" /></a><span class="col-label">Reg No.</span><a href="'.base_url().'admin/doc_list/sort_by/u_regno/sort_order/DESC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/desc.png" /></a>', 
			
			//'<strong>Mobile</strong>', // commented by preeti on 26th feb 14
			
			'<a href="'.base_url().'admin/doc_list/sort_by/u_father_fname/sort_order/ASC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/asc.png" /></a><span class="col-label">Father</span><a href="'.base_url().'admin/doc_list/sort_by/u_father_fname/sort_order/DESC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/desc.png" /></a>',// added by preeti on 26th feb 14
			
			'<a href="'.base_url().'admin/doc_list/sort_by/u_dob/sort_order/ASC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/asc.png" /></a><span class="col-label">Dob</span><a href="'.base_url().'admin/doc_list/sort_by/u_dob/sort_order/DESC/offset/'.$offset.$url.'"><img border="0" src="'.base_url().'images/desc.png" /></a>',// added by preeti on 26th feb 14

			'&nbsp', 
			  
			'<input type = "checkbox" id="check_all" name = "check_all" value = "check_all" />'
			
			);
			
			foreach( $records as $row )
	        {          	
				
				if( $row->u_is_name_change == 'y' )
				{
					$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
				}
				else 
				{
					$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
				}
				
	          	$this->table->add_row(
	          	
					"<input type='radio' name='Typ' id='Typ' value='".$row->reg_regno."'>".'<span class="col-data">'.$i.'</span>',
					
					'<span class="col-data">'.strtoupper( $name ).'</span>',
					
					'<span class="col-data">'.$row->reg_regno.'</span>',
					
					'<span class="col-data">'.strtoupper( $row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname).'</span>'	,
					
					'<span class="col-data">'.db_to_calen( $row->u_dob ).'</span>',
					
					"<a class='link' href='".base_url()."admin/doc_password/".$row->reg_id."'>Edit Password</a>" ,
					
					"<input type = 'checkbox' name = 'doc_ids[]' value = '".$row->reg_id."' />"					
					
				);
				
				$i++;
	        }
$attribute=array("target"=>"_blank");
			echo form_open('admin/submit',$attribute); 
	
			// below line added by preeti on 4th mar 14
			
			echo  $this->pagination->create_links();	
			
			?>
			
			<!-- below div added by preeti on 4th mar 14 -->
			
			<div class="clear"></div>
	
			<?php
	
			echo $this->table->generate();
	 ?>
	  
	  <table>
	  	
	  	<tr>
	  		
	  		<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
				
	  		
	  		<td><input type="submit" name="sbm" id="pn" value="Print all Docs" /></td>
	 	
	 		<td><input type="submit" name="sbm" id="dz"  value="Download Zip" /></td>
	 		
	 		<td><input type="submit" name="sbm" id="pp" value="Print Preview" /></td> 
	 		
	 		<td><input type="submit" name="sbm" id="gp" value="Generate Pdf" /></td>
	 		
	 		<td><!--<input type="submit" name="sbm" id="del" value="Delete" />--></td><!-- delete commented by preeti on 25th jun 14 -->
	 		
	 	</tr>
	 	
	 </table> 				
	
				<?php
	
				echo  $this->pagination->create_links();			
				
				echo form_close();
		}
				  
		?>         

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#start_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#end_date').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>