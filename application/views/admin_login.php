<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Login</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- below scripts are added by preeti on 26th mar 14 for black-box testing -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the password typed 
			
			var pass = $('#admin_password').val();
			
			if( pass != '' )
			{
				//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
			
				var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
			
				var result = md5( md5( pass ) + salt );
				
				// set the value of the hidden field
				
				$('#admin_password_encode').val(result);
				
				// clear the field
				
				$('#admin_password').val('');	
			}
			
			
		});
		
		$('#admin_password').blur(function()
		{
			$.post('/ajax/ajax_update_otp',
	   		{},
	   		
	   		function( result)
	   		{
				$('#div_otp').show();	
			}
	   		
	   		);
					
		});
		
	});
	
</script>

<style>
.regsquaresmall{width:450px;}
.regsquaresmall p span{font-size:10px;font-weight:bold;display:block;margin-left:100px;text-align: left;}
</style>

</head>

<body>

<div id="container">

  <?php $this->load->view('includes/header_admin'); ?>

  <div id="main">

    <div id="mainleft">

      <div id="contmenu1">

        <div id="menu">

          <!--<ul>

            <li><a href="<?php echo base_url(); ?>admin/index">Login Here</a></li>

          </ul>-->

        </div>

      </div>

    </div>

    <div id="changing">

      <div class="regsquaresmall">
		<br>
		<div class="heading">::Admin Login::</div> <!-- Line added by preeti on 21st apr 14 -->

          <p>
          	<span class="error">
          		
          		<!-- below lines modified by preeti on 26th mar 14 for black-box testing  -->
          		
          		<?php 
          		
          		if( validation_errors() )
				{
					echo strip_tags( validation_errors() ) ;
				}
				else 
				{
					$errors=explode('.',$errmsg);
					foreach( $errors as $val )
					{
						if($val)
						echo $val.'.<br>';
					}	
					//echo $errmsg;	
				}          		 
          		
          		?>	
          		
          		
          		</span>
          	</p>
			
          	<?php
          	
          		echo form_open('admin/validate');
			?>	
				
				<!--echo form_label('Username', 'admin_username');
				
				// below code added by preeti on 3rd apr 14
				
				$arr = array(
				
				'name' => 'admin_username',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);
				
				// below code commented by preeti on 3rd apr 14
				
				//echo form_input('admin_username', $this->input->post('admin_username')); // modified by preeti on 26th mar 14 
				
				// below code added by preeti on 3rd apr 14
				
				echo form_input( $arr ); 
				
				
				echo form_label('Password', 'admin_password');
				
					// below code modified by preeti on 26th mar 14
				
				$pass_att = array(
				
					'name' => 'admin_password',
					
					'id' => 'admin_password',
					
					'autocomplete' => 'off'
				
				);
				
				-->	
				
			<table align="center">
			<tbody>
				
				<tr>		
					<th><label>Username<span class="star">*</span><label/></th>					
					<td>						
						<input style="text-transform: none;" type="text" <?php echo 'autocomplete="off"'; ?> name="admin_username" id="admin_username" value="" />						
					</td>	
				</tr>
					
				<tr>		
					<th><label>Password<span class="star">*</span><label/></th>				
					<td>
						<input type="password" <?php echo 'autocomplete="off"'; ?> name="admin_password" id="admin_password" value="" />
					</td>	
				</tr>
			
				<tr>
					<td>&nbsp;</td>
					<td>						
						<?php echo $cap_img; ?>							
					</td>		
				</tr>			
			
				<!-- <tr>	
					<th><label for="captcha">Captcha<span class="star">*</span></label></th>	
					<td>						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
					</td>	
				</tr>
				
				<tr id="div_otp" style="display:none;">
			
					<th><label>One Time Password (OTP)<span class="star">*</span><label/></th>				
					<td>					
						<input type="text" autocomplete="off" name="otp" id="otp" value="" />
						<br><span class="star">OTP sent to XXXXXXXX26</span>
					</td>
				</tr> -->	
				
				<!--echo form_password( $pass_att ); // code modified by preeti on 24th mar 14 
				
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Login');// code added by preeti on 26th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 26th mar 14 for black-box testing
				
				-->	
				
				<tr>	
					<td>&nbsp;</td>
					<td>	
						<br>
						<input type="hidden" name="admin_password_encode" id="admin_password_encode" />
						<input type="submit" name="sub" id="sub" value="Login" />						
					</td>
				</tr>
				
			</tbody>
			</table>
			</br>
			</br>
			<?php
								
				echo form_close();
          	
          	?> 
          	          	
        </div>

      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>