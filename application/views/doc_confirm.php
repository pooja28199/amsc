 <?php $this->load->view('template/header'); ?>

<style>
.basic-grey .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
	text-shadow:none;
	text-decoration:none;
}
.basic-grey .link.button{padding: 7px 10px;display:inline-block;}

.basic-grey input[type="password"] {
    border: 1px solid #dadada;
    color: #888;
    font: 12px/12px Georgia,"Times New Roman",Times,serif;
    height: 24px;
    margin-bottom: 16px;
    margin-right: 6px;
    margin-top: 2px;
    outline: 0 none;
    padding: 3px 3px 3px 5px;
    width: 100%;
}

.basic-grey input[type="text"], .basic-grey input[type="email"], .basic-grey textarea, .basic-grey select
{
	width:100%;
	max-width: 245px;
	margin-bottom: 5px;
}

.collect-signup label {font-weight:bold;}

div.warnings{
	margin-bottom:20px;
	text-align:left;
}
div.warnings span{
	display:block;
	font-size: 11px;
}
td{width: 33.3%;}
.basic-grey label
{
	font-size: 13px;
	font-weight:bold;
}
</style>


<?php $this->load->view('template/left_user_out'); ?>




<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<!-- below scripts are added by preeti on 26th mar 14 for black-box testing -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the password typed 
			
			var pass = $('#reg_otp').val();
			
			if( pass != '' )
			{
				//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
				
				var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
				
				pass = md5( pass );
				
				var result = md5( pass + salt );
			
				// set the value of the hidden field
				
				$('#reg_otp_encode').val(result);
				
				// clear the field
				
				$('#reg_otp').val('');	
			}
			
			
		});
		
	});
	
</script>


<div id="fullright">

<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<div class="regsquaresmall">
<?php
          	$attributes = array('class' => 'basic-grey');

          		echo form_open_multipart('doc/validate_otp',$attributes);		
					
		?>	
<h1>Confirm Registration</h1>
<strong>Read the form filling instructions and keep scanned soft copies of documents/certificates ready for uploading.</strong>
<br />

<br />

 	<div class="error warnings">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					$explode=explode('.',$errmsg);
					
					foreach($explode as $val)
					{
						if($val)
						echo '<span>* '.$val.'.</span>';
					}
					//echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
			
			
			 
			
			
			
			
			
			  	<!--         		
          	
          		
				
				echo form_label('Registration Number', 'reg_regno');
				
				
				
				// below code added by preeti on 3rd apr 14
				
				$arr = array(
				
				'name' => 'reg_regno',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);
				
				
				echo form_input( $arr ); 
				
				
				
				
				//echo form_input('reg_regno', $this->input->post('reg_regno')); // commented by preeti on 3rd apr 14
							
				echo form_label('One Time Password<br/>(OTP)', 'reg_otp');
				
				
				// below code modified by preeti on 26th mar 14 for black-box testing
				
				$pass_att = array(
				
					'name' => 'reg_otp',
					
					'id' => 'reg_otp',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 25th mar 14 for black-box testing 
				
				-->
				
				<table style="width:100%;">
				<tr>
			
					<td><label for="reg_regno">Registration Number <span class="star">*</span></label></td>
					
					<td>
						
						<input type="text" placeholder="XXXXXXSSCXXXX" <?php echo 'autocomplete="off"'; ?> name="reg_regno" id="reg_regno" value="" />
						
					</td>
					<td>&nbsp;</td>
				</tr>
				
				
				
				<tr>
			
					<td><label for="reg_otp">One Time Password (OTP) <span class="star">*</span></label></td>
					
					<td>
						
						<input type="password" <?php echo 'autocomplete="off"'; ?> name="reg_otp" id="reg_otp" value="" />
						
					</td>
					<td>&nbsp;</td>
				</tr>
				
				
				<tr>						
			
					<td>&nbsp;</td>
					<td>
						
						<?php
						
						
						 echo $cap_img; ?>		
						
					</td>
					<td>&nbsp;</td>
				</tr>			
				
				
				<tr>
			
					<td><label for="captcha">Captcha <span class="star">*</span></label></td>
					
					<td>
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</td>
					<td>&nbsp;</td>
				</tr>
				
				
				<!-- below code added by preeti on 26th mar 14 for black-box testing -->
				
				
				
				<!-- below code added by preeti on 28th mar 14 for black-box testing -->
				
				
								
				<!--
				
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Submit');// code added by preeti on 25th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 25th mar 14 for black-box testing
				
				echo anchor(base_url().'doc/new_otp', 'Generate New OTP', array( 'class' => 'link') );
				
				-->
				
				
				<tr>
					
					<td>&nbsp;</td>
						
					
					<td>	
						<input type="hidden" value="" name="reg_otp_encode" id="reg_otp_encode" />
						<input type="submit" class="button" name="sub" id="sub" value="Submit" />						
					</td>
					<td>&nbsp;</td>
				</tr>	
				
				<tr>
				<td colspan="3" align="right">
					
					<?php echo anchor(base_url().'doc/new_otp', 'Generate New OTP', array( 'class' => 'link button') ); ?>
					
	
				</td>	
				</tr>
				</table>
			
	
			
			
			
			
			
			
			
	
			
			
	
		
				
			
				
			
			 
					
				
			         	
          
          <?php							
				echo form_close();
          	
          	?>
			</div>
</div>




 <?php $this->load->view('template/footer'); ?>


