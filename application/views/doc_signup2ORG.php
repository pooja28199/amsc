<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/left_user_out'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.form.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/doc_signup2.js"></script> 



<div id="fullright">

      <div class="regsquaresmall">
      	
      	<!-- below form modified by preeti on 11th mar 14 -->
      	
      	<div class="heading">REGISTRATION STEP - 2</div>
			
			<div class="error" id="status">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
      	
      	<form  id="upload_form" action="<?php echo base_url(); ?>doc/save2" class="basic-grey" method="post" enctype="multipart/form-data">
      	
      	<div class="form">
      		
      		<!-- commented by preeti on 23rd apr 14 -->
			
			<!--<div class="left-col"><a class="link" href="<?php echo base_url(); ?>doc/signup1/<?php echo $reg_id; ?>">Back to Step 1</a></div>-->
			
			<div class="sub-form">EDUCATIONAL DETAILS</div>
			
			<div class="subsub-form">GRADUATION DETAILS</div>			

			<div class="bind-col">
			
			<div class="left-col"><label>Graduated from<span class="error">*</span></label></div>
			
			<div class="right-col">

      				Foreign<input type="radio" id="foreign_grad" name="u_grad_from" <?php if( $record->u_grad_from == 'f' ){ echo "checked"; } ?> value="f" />&nbsp;
      				
      				India<input type="radio" id="indian_grad" name="u_grad_from" <?php if( $record->u_grad_from == 'i' ){ echo "checked"; } ?> value="i"  />

					<!-- id added for the above radio by preeti on 2nd apr 14 -->

			</div>
			
			</div>

			

			<div class="bind-col">
			
			<div class="left-col"><label>Basic Qualification<span class="error">*</span></label></div>
			
			<div class="right-col">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->

     			<input <?php echo 'autocomplete="off"'; ?> name="u_basic_qual" value="<?php echo $record->u_basic_qual; ?>"  id="u_basic_qual" type="text" />
     				
     				<span class="small">Minimum MBBS/Equivalent is Mandatory</span>
      				      				
			</div>

			</div>	
					
			
			<div class="bind-col">
			
			<div class="left-col"><label>Date Of Admission<span class="error">*</span></label></div>
			
			<div class="right-col">

     				<input name="u_adm_date" readonly="readonly" value="<?php if( $record->u_adm_date != '0000-00-00' ){ echo db_to_calen( $record->u_adm_date ) ; } ?>"  id="u_adm_date" type="text" />
      				
      				<span  style="display: none;">
				
						<img id="calImg1" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>

			</div>

			</div>			
			
			<div class="bind-col">
			
			<!-- below label modified by preeti on 3rd apr 14 -->
			
			<div class="left-col"><label>Date Of Passing Final MBBS<span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input type="text" readonly="readonly" value="<?php if( $record->u_passing_date != '0000-00-00' ){ echo db_to_calen( $record->u_passing_date ) ; } ?>" name="u_passing_date" id="u_passing_date" />
      			
      				<span  style="display: none;">
				
						<img id="calImg2" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	

			</div>
			
			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>No. Of Attempts for passing Final MBBS Exam (Part I)<span class="error">*</span></label></div>
			
			<div class="right-col">

      				<select style="width: 150px;" name="u_num_attempt" id="u_num_attempt">
      					
      					<option value="">Select</option>
      					
      					<?php
      						// below line modified by preeti on 2nd apr 14 for black-box testing 
      						for( $i = 1; $i <=2 ; $i++ )
							{
						?>		
								
								<option <?php if( $record->u_num_attempt == $i ){ echo "selected";  } ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
								
						<?php
							}
      					?>
      					
      				</select>
      				
      				<!--below code added by preeti on 2nd apr 14 for black-box testing -->
      				
      				<span class="small">Max Allowed Attempt: 2</span>
	

			</div>

			</div>
			
      		<?php
      		
      		if( $record->u_attempt_cert != '' )
			{
      			// below line modified by preeti on 7th mar 14	
					
      			$url = base_url().'uploads/attempt1/'.$record->u_attempt_cert;
      		?>

			<div class="bind-col">

			<div class="left-col"><label>Uploaded Attempt Certificate (Part I)</label></div>
			
			<div class="right-col">

      				<!-- below file modified by preeti on 7th mar 14 -->
      				
      				<a target="_BLANK" title="Click Here" class="" href="<?php echo $url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a></td>	

			</div>

			</div>
				      		
      		<?php
			}
      		?>
      		
      		<div class="bind-col">
      		
      		<div class="left-col"><label>Upload Attempt Certificate (Part I) <span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input name="u_attempt_cert" id="u_attempt_cert" type="file" /><br />
      				
      				<span class="small">Type: pdf / Size: 200KB</span>	

			</div>

			</div>


						<div class="bind-col">
			
			<div class="left-col"><label>No. Of Attempts for passing Final MBBS Exam (Part II)<span class="error">*</span></label></div>
			
			<div class="right-col">

      				<select style="width: 150px;" name="u_num_attempt1" id="u_num_attempt1">
      					
      					<option value="">Select</option>
      					
      					<?php
      						// below line modified by preeti on 2nd apr 14 for black-box testing
      						for( $i = 1; $i <=2 ; $i++ )
							{
						?>		
								
								<option <?php if( $record->u_num_attempt1 == $i ){ echo "selected";  } ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
								
						<?php
							}
      					?>
      					
      				</select>
      				
      				
      				<!--below code added by preeti on 2nd apr 14 for black-box testing -->
      				
      				<span class="small">Max Allowed Attempt: 2</span>
	

			</div>

			</div>
			
      		<?php
      		
      		if( $record->u_attempt_cert1 != '' )
			{
				// below file modified by preeti on 7th mar 14 
				
      			$url = base_url().'uploads/attempt2/'.$record->u_attempt_cert1;
      		?>

			<div class="bind-col">

			<div class="left-col"><label>Uploaded Attempt Certificate (Part II)</label></div>
			
			<div class="right-col">

      				<!-- below file modified by preeti on 7th mar 14 -->
      				
      				<a target="_BLANK" title="Click Here" class="" href="<?php echo $url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a></td>	

			</div>

			</div>
				      		
      		<?php
			}
      		?>
      		
      		<div class="bind-col">
      		
      		<div class="left-col"><label>Upload Attempt Certificate (Part II)<span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input name="u_attempt_cert1" id="u_attempt_cert1" type="file" /><br />
      				
      				<span class="small">Type: pdf / Size: 200KB</span>	

			</div>

			</div>



			<div class="bind-col">

			<div class="left-col"><label>Medical College<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_college; ?>" name="u_college" id="u_college" />	

			</div>

			</div>
			
			<div class="bind-col">

			<div class="left-col"><label>University<span class="error">*</span></label></div>
			
			<div class="right-col">

				<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      			<input <?php echo 'autocomplete="off"'; ?> type="text" value="<?php echo $record->u_univ; ?>" name="u_univ" id="u_univ" />	

			</div>

			</div>
			
			
			
			<div class="bind-col">

			<div class="left-col"><label>Have you completed your Internship?<span class="error">*</span></label></div>
			
			<div class="right-col">

      				Yes<input type="radio" name="u_intern_complete" value="y" <?php if( $record->u_intern_complete == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="u_intern_complete" value="n" <?php if( $record->u_intern_complete == 'n' ){ echo "checked"; } ?> />
      				      				
			</div> 

			</div>
			
			
			<div id="intern_div">			
			
				<div class="bind-col">
	
				<div class="left-col"><label>Internship Completion Date<span class="error">*</span></label></div>
				
				<div class="right-col">
	
	      				<input type="text" readonly="readonly" value="<?php if( $record->u_intern_date != '0000-00-00' ){ echo db_to_calen( $record->u_intern_date ) ; } ?>" name="u_intern_date" id="u_intern_date" />
	      				
	      				<span  style="display: none;">
					
							<img id="calImg3" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
									
						</span>	
	      			
	
				</div>
	
				</div>
				
				 <?php
	      			
	      			if( $record->u_intern_cert != '' )
					{
						$intern_url = base_url().'uploads/intern/'.$record->u_intern_cert;
				
				?>      
				
				<!-- label changed by preeti on 28th feb 14 -->
				
					<!-- id added to below div by preeti on 3rd mar 14 -->
					
					<div class="bind-col" id="intern_upload">
					
						<div class="left-col"><label>Uploaded Copy Of Internship Completion Certificate </label></div>
						
						<div class="right-col">
			
			      				<!-- below file modified by preeti on 7th mar 14 -->
			      				
			      				<a class="" title="Click Here" target="_blank" href="<?php echo $intern_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>
						
						</div>
					
					</div>
				
				<?php			
					}
	      		
	      		?> 
	      			<!-- label changed by preeti on 28th feb 14 -->
	      		
		      		<div class="bind-col">
		      		
			      		<div class="left-col"><label>Upload a Copy Of Internship Completion Certificate <span class="error">*</span></label></div>
						
						<div class="right-col">
			
			      				<input type="file" name="u_intern_cert" id="u_intern_cert" /><br />
			      				
			      				<span class="small">Type: pdf / Size: 200KB</span>
						</div>
		      		 
		
					</div>			
			
			
			</div> <!-- intern_div ends here -->
			
			
			
			
			<div id="intern_like_div">			
			
				<div class="bind-col">
	
				<div class="left-col"><label>Internship Completion Date (Likely)<span class="error">*</span></label></div>
				
				<div class="right-col">
	
	      				<input type="text" readonly="readonly" value="<?php if( $record->u_intern_date_likely != '0000-00-00' ){ echo db_to_calen( $record->u_intern_date_likely ) ; } ?>" name="u_intern_date_likely" size="15" id="u_intern_date_likely" />
	      				
	      				<span  style="display: none;">
					
							<img id="calImg5" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
									
						</span>	
	      			
	
				</div>
	
				</div>			
			
			</div> <!-- intern_like_div ends here -->
			
			
			

			<div class="bind-col">

			<div class="left-col"><label>College Recognized by MCI<span class="error">*</span></label></div>
			
			<div class="right-col">

      				Yes<input type="radio" name="u_college_recog" value="y" <?php if( $record->u_college_recog == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="u_college_recog" value="n" <?php if( $record->u_college_recog == 'n' ){ echo "checked"; } ?> />
      
			</div>
      		
      		</div> 

			<?php
      			
      			if( $record->u_mbbs_cert != '' )
				{
					$u_mbbs_cert_url = base_url().'uploads/mbbs/'.$record->u_mbbs_cert;
			
			?>      


			<div class="bind-col">

			<div class="left-col"><label>Uploaded MBBS Degree/Passing Certificate </label></div>
			
			<div class="right-col">

      				<!-- below file modified by preeti on 7th mar 14 -->
      				
      				<a class="" title="Click Here" target="_blank" href="<?php echo $u_mbbs_cert_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>
      				
			</div>
      		
      		
      		</div> 

			<?php			
				}
      		
      		?>
      		
      		<div class="bind-col">
      		
      		<div class="left-col"><label>Upload a Copy Of MBBS Degree/Passing Certificate <span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input type="file" name="u_mbbs_cert" id="u_mbbs_cert" /><br />
      				
      				<span class="small">Type: pdf / Size: 200KB</span>
      				
			</div> 		

			</div>
			
			
			<!-- start, lines moved from above to here by preeti on 12th mar 14 -->
			
			<div class="clear"></div>
			
			<div class="subsub-form">PMRC DETAILS</div>	

			<div class="bind-col">

			<div class="left-col"><label>Permanent Medical Registration Number<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> name="u_pmrc_num" value="<?php if( $record->u_pmrc_num != '0' ){ echo $record->u_pmrc_num; } ?>" id="u_pmrc_num" type="text" />

			</div>

			</div>

			<div class="bind-col">

			<div class="left-col"><label>Issuing Office<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> type="text" name="u_pmrc_issue_off" value="<?php if( $record->u_pmrc_issue_off != '0' ){ echo $record->u_pmrc_issue_off; } ?>" id="u_pmrc_issue_off" />

			</div>

			</div>

			<div class="bind-col">

			<div class="left-col"><label>Date Of Registration<span class="error">*</span></label></div>
			
			<div class="right-col">

   				<input type="text" readonly="readonly" name="u_pmrc_reg_date" value="<?php if( $record->u_pmrc_reg_date != '0000-00-00' ){ echo db_to_calen( $record->u_pmrc_reg_date ) ; } ?>" id="u_pmrc_reg_date" />

				<span  style="display: none;">
				
						<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>

			</div>

			</div>

			<?php
      		
      		if( $record->u_pmrc_copy != '' )
			{
				$pmrc_url = base_url().'uploads/pmrc/'.$record->u_pmrc_copy;
      		?>

			<!-- label changed by preeti on 28th feb 14 -->

			<div class="bind-col">

			<div class="left-col"><label>Uploaded Copy of Permanent Medical Registration Certificate</label></div>
			
			<div class="right-col">

   				<!-- below file modified by preeti on 7th mar 14 -->
   				
   				<a class="" target="_blank" title="Click Here" href="<?php echo $pmrc_url; ?>" ><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>
      				
			</div>

			</div>			
			
			<?php
      		}
      		?>     

			<!-- label changed by preeti on 28th feb 14 -->

			<div class="bind-col">	
			
			<div class="left-col"><label>Upload a Copy of Permanent Registration Certificate / Provisional Certificate <span class="error">*</span></label></div>
			
			<div class="right-col">

   				<input type="file" name="u_pmrc_copy" id="u_pmrc_copy" value="f" />
   				<br />
   				<span class="small">Type: pdf / Size: 200KB</span>

			</div>
			
			</div>
			
			
			
			
			
			<!-- end, lines moved from above to here by preeti on 12th mar 14 -->
			
			<div class="clear"></div> 
			
			<div class="subsub-form">POST GRADUATION DETAILS</div>

			<div class="bind-col">

			<div class="left-col"><label>Are you a Post Graduate ?<span class="error">*</span></label></div>
			
			<div class="right-col">

      				Yes<input type="radio" name="u_is_pg" value="y" <?php if( $record->u_is_pg == 'y' ){ echo "checked"; } ?> />&nbsp;
      				
      				No<input type="radio" name="u_is_pg" value="n" <?php if( $record->u_is_pg == 'n' ){ echo "checked"; } ?> />
      				      				
			</div> 

			</div>

			<div id="pg_div">


			<div class="subsub-form">FIRST</div>
			
			<div class="bind-col">	

			<div class="left-col"><label>PG Qualification<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> name="pg_degree" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' )
      				
      				{echo $record_pg[0]->pg_degree;} ?>" id="pg_degree" type="text" />      				
			</div> 
	
			</div>

			<div class="bind-col">
	
			<div class="left-col"><label>Subject<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> name="pg_subject" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' )
      				
      				{echo $record_pg[0]->pg_subject;} ?>" id="pg_subject" type="text" />      				
			</div> 

			</div>

			<div class="bind-col">

			<div class="left-col"><label>College<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> name="pg_college" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' )
      				
      				{echo $record_pg[0]->pg_college;} ?>" id="pg_college" type="text" />      				
			</div> 

			</div>

			<div class="bind-col">

			<div class="left-col"><label>University<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != '' )
      				
      				{echo $record_pg[0]->pg_univ;} ?>" id="pg_univ" />      				
			</div>

			</div>
			
			<div class="bind-col">

			<div class="left-col"><label>Year Of Passing<span class="error">*</span></label></div>
			
			<div class="right-col">

      				
      				 <select style="width: 150px;" name="pg_year" id="pg_year">
      					
      					<option value="">Select</option>
      					
      					<?php
      						
      						//for( $i = 2014; $i >=1950 ; $i-- )
      						for( $i = 2018; $i >= 1950 ; $i-- ) // modified by preeti on 4th apr 14
							{
						?>		
								
								<option <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
								
						<?php
							}
      					?>
      					
      				</select>   				
      				      				
			</div>
			
			</div>
			
			<div class="bind-col">
			
				<div class="left-col"><label>College Recognized by MCI<span class="error">*</span></label></div>
				
				<div class="right-col">
	
	      				Yes<input type="radio" name="pg_mci_recog" value="y" <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
	      				
	      				No<input type="radio" name="pg_mci_recog" value="n" <?php if( isset( $record_pg[0] ) && $record_pg[0] != '' ){ if( $record_pg[0]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> />
	      				      				
				</div>
			
			</div>
			
			<?php
      			
      			if( isset( $record_pg[0] ) && $record_pg[0] != '' &&  $record_pg[0]->pg_cert != '' )
				{
					$u_pg_cert_url = base_url().'uploads/pg/pg1/'.$record_pg[0]->pg_cert;
			
			?> 
			
			<!-- id added to the below div by preeti on 3rd mar 14 -->     
			
			<div class="bind-col" id="pg_upload">
			
			<div class="left-col"><label>Uploaded Copy Of PG Degree/Passing Certificate</label></div>
			
			<div class="right-col">

      				<!-- below file modified by preeti on 7th mar 14 -->
      				
      				<a class="" title="Click Here" target="_blank" href="<?php echo $u_pg_cert_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>      				
			</div>
			
			</div>
			
			<?php			
				}
      		
      		?> 
      		
      		<div class="bind-col">
      		
      		<div class="left-col"><label>Upload a Copy Of PG Degree/Passing Certificate <span class="error">*</span></label></div>
			
			<div class="right-col">

     			<input type="file" name="pg_cert" id="pg_cert" /> <br />
     			
     			<span class="small">Type: pdf / Size: 200KB</span>
     
     		</div> 
     		
     		</div>
     		
     		
     		<div id="add_pg1_addmore" class="bind-col">
      		
      		<div class="left-col"><label>If you have any other MCI recognized PG Degree / Diploma</label></div>
			
			<div class="right-col">
				
				<!--<a class="link" href="javascript:void(0);" id="add_pg1" >Click Here To Add</a>-->
				
				Yes<input <?php if(  $cnt_pg >1 ){ echo "checked"; } ?> type="radio" value="y" name="add_pg1" />
						
				No<input type="radio" value="n" name="add_pg1" />
				
				<br />
				
				<span class="small"> You can add max Two more PG Qualifications </span>
				
			</div> 
     		
     		</div>
     		
     		<div class="clear"></div>
     		
     		<div id="add_pg1_div"> <!-- first innner add_pg1_div starts here -->

				<div class="subsub-form">SECOND</div>

				
				<!-- field "Click Here to Remove" removed from here by preeti on 27th feb 14 -->
     			
     			<div class="bind-col">	

					<div class="left-col"><label>PG Qualification<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_degree1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' )
      				
      				{echo $record_pg[1]->pg_degree;} ?>" 
		      				
		      				id="pg_degree1" type="text" />      				
					</div> 
			
				</div>
	
				<div class="bind-col">
		
					<div class="left-col"><label>Subject<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_subject1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' )
      				
      				{echo $record_pg[1]->pg_subject;} ?>" 
		      				
		      				id="pg_subject1" type="text" />      				
					</div> 
	
				</div>
	
				<div class="bind-col">
	
					<div class="left-col"><label>College<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_college1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' )
      				
      				{echo $record_pg[1]->pg_college;} ?>" id="pg_college1" type="text" />      				
					</div> 
	
				</div>
	
				<div class="bind-col">
	
					<div class="left-col"><label>University<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ1" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != '' )
      				
      				{echo $record_pg[1]->pg_univ;} ?>" id="pg_univ1" />      				
					</div>
	
				</div>
				
				<div class="bind-col">
	
					<div class="left-col"><label>Year Of Passing<span class="error">*</span></label></div>
					
					<div class="right-col">
		
		      				
		      				 <select style="width: 150px;" name="pg_year1" id="pg_year1">
		      					
		      					<option value="">Select</option>
		      					
		      					<?php
		      						
		      						//for( $i = 2014; $i >=1950 ; $i-- )
		      						for( $i = 2018; $i >=1950 ; $i-- )// modified by preeti on 4th apr 14
									{
								?>		
										
										<option <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
										
								<?php
									}
		      					?>
		      					
		      				</select>   				
		      				      				
					</div>
				
				</div>
				
				<div class="bind-col">
				
					<div class="left-col"><label>College Recognized by MCI<span class="error">*</span></label></div>
					
					<div class="right-col">
		
		      				Yes<input type="radio" name="pg_mci_recog1" value="y" <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
		      				
		      				No<input type="radio" name="pg_mci_recog1" value="n" <?php if( isset( $record_pg[1] ) && $record_pg[1] != '' ){ if( $record_pg[1]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> />
		      				      				
					</div>
				
				</div>
				
				<?php
	      			
	      		if( isset( $record_pg[1] ) && $record_pg[1] != '' &&  $record_pg[1]->pg_cert != '' )
				{
					$u_pg_cert_url = base_url().'uploads/pg/pg2/'.$record_pg[1]->pg_cert;
				?>      
				
				<div class="bind-col" id="pg_upload1">
				
					<div class="left-col"><label>Uploaded Copy Of PG Degree/Passing Certificate</label></div>
					
					<div class="right-col">
		
		      				<!-- below file modified by preeti on 7th mar 14 -->
		      				
		      				<a class="" title="Click Here" target="_blank" href="<?php echo $u_pg_cert_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>      				
					</div>
				
				</div>
				
				<?php			
					}
	      		
	      		?> 
	      		
	      		<div class="bind-col">
	      		
		      		<div class="left-col"><label>Upload a Copy Of PG Degree/Passing Certificate <span class="error">*</span></label></div>
					
					<div class="right-col">
		
		     			<input type="file" name="pg_cert1" id="pg_cert1" /> <br />
		     			
		     			<span class="small">Type: pdf / Size: 200KB</span>
		     
		     		</div> 
	     		
	     		</div>      		
	     		
	     		
	     		<div id="add_pg2_addmore" class="bind-col">
      		
      				<div class="left-col"><label>If you have any other MCI recognized PG Degree / Diploma</label></div>
			
					<div class="right-col">
						
						<!--<a class="link" href="javascript:void(0);" id="add_pg2" >Click Here To Add</a>-->
						
						Yes<input <?php if(  $cnt_pg >2 ){ echo "checked"; } ?> type="radio" value="y" name="add_pg2" />
						
						No<input type="radio" value="n" name="add_pg2" />
						
					</div>      		
	     		
	     		</div>   			
     			
     			
     		</div> <!-- inner first add_pg1_div ends here -->
     		
     		<div class="clear"></div>
     		
     		
     		<div id="add_pg2_div"> <!-- second innner add_pg2_div starts here -->

				<div class="subsub-form">THIRD</div>	
				
			
				<!-- field "Click Here to Remove" removed from here by preeti on 27th feb 14 -->
				   			
     			
     			<div class="bind-col">	

					<div class="left-col"><label>PG Qualification<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_degree2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' )
      				
      				{echo $record_pg[2]->pg_degree;} ?>" id="pg_degree2" type="text" />      				
					</div> 
			
				</div>
	
				<div class="bind-col">
		
					<div class="left-col"><label>Subject<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_subject2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' )
      				
      				{echo $record_pg[2]->pg_subject;} ?>" id="pg_subject2" type="text" />      				
					</div> 
	
				</div>
	
				<div class="bind-col">
	
					<div class="left-col"><label>College<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> name="pg_college2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' )
      				
      				{echo $record_pg[2]->pg_college;} ?>" id="pg_college2" type="text" />      				
					</div> 
	
				</div>
	
				<div class="bind-col">
	
					<div class="left-col"><label>University<span class="error">*</span></label></div>
					
					<div class="right-col">
		
							<!-- below line modified by preeti on 21st apr 14 for manual testing -->
		
		      				<input <?php echo 'autocomplete="off"'; ?> type="text" name="pg_univ2" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != '' )
      				
      				{echo $record_pg[2]->pg_univ;} ?>" id="pg_univ2" />      				
					</div>
	
				</div>
				
				<div class="bind-col">
	
					<div class="left-col"><label>Year Of Passing<span class="error">*</span></label></div>
					
					<div class="right-col">
		
		      				
		      				 <select style="width: 150px;" name="pg_year2" id="pg_year2">
		      					
		      					<option value="">Select</option>
		      					
		      					<?php
		      						
		      						//for( $i = 2014; $i >=1950 ; $i-- )
		      						for( $i = 2018; $i >=1950 ; $i-- )// modified by preeti on 4th apr 14
									{
								?>											
										<option <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_year == $i ){ echo "selected";  }} ?>  value="<?php echo $i; ?>"><?php echo $i; ?></option>
										
								<?php
									}
		      					?>
		      					
		      				</select>   				
		      				      				
					</div>
				
				</div>
				
				<div class="bind-col">
				
					<div class="left-col"><label>College Recognized by MCI<span class="error">*</span></label></div>
					
					<div class="right-col">
		
		      				Yes<input type="radio" name="pg_mci_recog2" value="y" <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_mci_recog == 'y' ){ echo "checked"; } }?> />&nbsp;
		      				
		      				No<input type="radio" name="pg_mci_recog2" value="n" <?php if( isset( $record_pg[2] ) && $record_pg[2] != '' ){ if( $record_pg[2]->pg_mci_recog == 'n' ){ echo "checked"; }} ?> />
		      				      				
					</div>
				
				</div>
				
				<?php
	      			
	      			if( isset( $record_pg[2] ) && $record_pg[2] != '' &&  $record_pg[2]->pg_cert != '' )
					{
						$u_pg_cert_url = base_url().'uploads/pg/pg3/'.$record_pg[2]->pg_cert;
				
				?>      
				
				<div class="bind-col"  id="pg_upload2">
				
					<div class="left-col"><label>Uploaded Copy Of PG Degree/Passing Certificate</label></div>
					
					<div class="right-col">
		
		      				<!-- below file modified by preeti on 7th mar 14 -->
		      				
		      				<a class="" title="Click Here" target="_blank" href="<?php echo $u_pg_cert_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>      				
					</div>
				
				</div>
				
				<?php			
					}
	      		
	      		?> 
	      		
	      		<div class="bind-col">
	      		
		      		<div class="left-col"><label>Upload a Copy Of PG Degree/Passing Certificate <span class="error">*</span></label></div>
					
					<div class="right-col">
		
		     			<input type="file" name="pg_cert2" id="pg_cert2" /> <br />
		     			
		     			<span class="small">Type: pdf / Size: 200KB</span>
		     
		     		</div> 
	     		
	     		</div>
	     		
	     		<div class="clear"></div>    			
     			
     			
     		</div> <!-- inner second add_pg2_div ends here -->
     		
     		
     		
     		
     		
     		
     		
   </div><!-- pg_div ends-->
     		
     		<div class="clear"></div>
     		
     		<div class="sub-form">FEES DETAILS</div>

			<div class="bind-col">

			<div class="left-col"><label>DD Number<span class="error">*</span></label></div>
			
			<div class="right-col">

				<!-- below line modified by preeti on 21st apr 14 for manual testing -->

     			<input <?php echo 'autocomplete="off"'; ?> name="u_dd_num" maxlength="6" value="<?php echo $record->u_dd_num ; ?>" id="u_dd_num" type="text" />
     
     		</div> 

			</div>
			
			<div class="bind-col">
			
			<div class="left-col"><label>Date<span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input name="u_dd_date" readonly="readonly" value="<?php if( $record->u_dd_date != '0000-00-00' ){ echo db_to_calen( $record->u_dd_date ) ; } ?>" id="u_dd_date" type="text" />
      			
      				<span  style="display: none;">
				
						<img id="calImg4" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
					</span>	

     
     		</div> 

			</div>

			<div class="bind-col">

			<div class="left-col"><label>Payable Bank<span class="error">*</span></label></div>
			
			<div class="right-col">

					<!-- below line modified by preeti on 21st apr 14 for manual testing -->

      				<input <?php echo 'autocomplete="off"'; ?> name="u_dd_bank" value="<?php echo $record->u_dd_bank ; ?>" id="u_dd_bank" type="text" />
     
     		</div> 

			</div>	

			<?php
      			
      			if( $record->u_dd_copy != '' )
				{
					$dd_url = base_url().'uploads/dd/'.$record->u_dd_copy;
			
			?>      

			<div class="bind-col">

			<div class="left-col"><label>Uploaded Copy Of DD</label></div>
			
			<div class="right-col">

      				<!-- below file modified by preeti on 7th mar 14 -->
      				
      				<a class="" title="Click Here" target="_blank" href="<?php echo $dd_url; ?>"><img width="30" height="30" src="<?php echo base_url(); ?>images/file.gif" /></a>
     
     		</div> 

			</div>

			<?php			
				}
      		
      		?>  

			<div class="bind-col">

			<div class="left-col"><label>Upload a Copy Of DD <span class="error">*</span></label></div>
			
			<div class="right-col">

      				<input type="file" name="u_dd_copy" id="u_dd_copy" /> <br />
      				
      				<span class="small">Type: pdf / Size: 200KB</span>
     
     		</div>
     		
     		</div>
     		
     		<div class="bind-col">
     		
     		<div class="left-col">
				
				<input type="hidden" name="reg_id" id="reg_id" value="<?php echo $reg_id; ?>" />

					<input type="hidden" name="u_pmrc_copy_hid" value="<?php echo $record->u_pmrc_copy; ?>" id="u_pmrc_copy_hid" />

					<input type="hidden" name="u_attempt_cert_hid" value="<?php echo $record->u_attempt_cert; ?>" id="u_attempt_cert_hid" />

					<input type="hidden" name="u_attempt_cert1_hid" value="<?php echo $record->u_attempt_cert1; ?>" id="u_attempt_cert1_hid" />					

					<input type="hidden" name="u_intern_cert_hid" value="<?php echo $record->u_intern_cert; ?>" id="u_intern_cert_hid" />
      				
      				<input type="hidden" name="u_mbbs_cert_hid" value="<?php echo $record->u_mbbs_cert; ?>" id="u_mbbs_cert_hid" />
      				
      				<!-- start, added by preeti on 24th feb 14 -->
      				
      				<input type="hidden" name="pg_cert1_hid" value="<?php if( isset( $record_pg[0] ) && $record_pg[0] != ''  ){ echo $record_pg[0]->pg_cert; }?>" id="pg_cert1_hid" />
      				
      				<input type="hidden" name="pg_cert2_hid" value="<?php if( isset( $record_pg[1] ) && $record_pg[1] != ''  ){ echo $record_pg[1]->pg_cert; }?>" id="pg_cert2_hid" />
      				
      				<input type="hidden" name="pg_cert3_hid" value="<?php if( isset( $record_pg[2] ) && $record_pg[2] != ''  ){ echo $record_pg[2]->pg_cert; }?>" id="pg_cert3_hid" />
      				
      				
      				<!-- end, added by preeti on 24th feb 14 -->
      				
      				<input type="hidden" name="u_dd_copy_hid" value="<?php echo $record->u_dd_copy; ?>" id="u_dd_copy_hid" />
      				
      				
      				
      				<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />

				
					<!-- start, fields added by preeti on 25th feb 14 -->
					
					<input type="hidden" name="pg_count" id="pg_count" value="<?php echo $cnt_pg; ?>" />
					
					<!-- end, fields added by preeti on 25th feb 14 -->
					
					
					<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
					<input type="hidden" name="confirm_random" id="confirm_random" value="<?php echo $confirm_random; ?>" />
				
				
					<!-- below line added by preeti on 23rd apr 14 for manual testing -->
				
					<input type="hidden" name="sequence_token" id="sequence_token" value="<?php echo $sequence_token; ?>" /> 
					
				
					<input type="submit" class="button" name="save_e" value="Save & Exit" />
				
			</div>
			
			<div class="right-col">
								
				<input type="submit" class="button" name="save_c" id="save_c" value="Save & Continue" />
				
			</div>
     		
     		</div> 


		</div>	
		
		
		<!-- form div close here -->		
      	
      	 </form>
		 
      </div>     


<script type="text/javascript">

			$('#u_pmrc_reg_date').datepick({showOnFocus: false, showTrigger: '#calImg'});

			$('#u_adm_date').datepick({showOnFocus: false, showTrigger: '#calImg1'});
			
			$('#u_passing_date').datepick({showOnFocus: false, showTrigger: '#calImg2'});
			
			$('#u_intern_date').datepick({showOnFocus: false, showTrigger: '#calImg3'});
			
			$('#u_dd_date').datepick({showOnFocus: false, showTrigger: '#calImg4'});
			
			
			
			$('#u_intern_date_likely').datepick({showOnFocus: false, showTrigger: '#calImg5'});
			
</script>

</div>
<?php $this->load->view('template/footer'); ?>