<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/left_user_out'); ?>


<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<style>
.regsquaresmall .left { width:140px;}
.basic-grey .button{
	background: #993300 none repeat scroll 0 0;
    border-radius: 4px;
    padding: 5px 10px;
    text-align: center;	
	cursor:pointer;
	text-shadow:none;
	text-decoration:none;
}
div.warnings{
	margin-bottom:20px;
	margin-top:20px;
	text-align:left;
}
div.warnings span{
	display:block;
	font-size: 11px;
}
.basic-grey label
{
	font-size: 14px;
}
</style>

	<div id="fullright">

		<div class="regsquaresmall">

		<div class="heading">New OTP</div>

			<div class="error warnings">
          		
          	<?php 
          	     	
			if( validation_errors() )
			{
				echo strip_tags( validation_errors() ) ;
			}
			else 
			{
				$explode=explode('.',$errmsg);
					
					foreach($explode as $val)
					{
						if($val)
						echo '<span>* '.$val.'.</span>';
					}
				//echo strip_tags( $errmsg );	
			}			 
          	
          	?>
          	
          	</div>

          <p>
          	         	
          	<?php
          	
				$attributes=array('class'=>'basic-grey');
          		echo form_open('doc/create_otp',$attributes);
				
				?>
				
				<!--echo form_label('Registration No.', 'reg_regno');
				
				
				// below code added by preeti on 21st apr 14 for manual testing
				
				$arr = array(
				
				'name' => 'reg_regno',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);				
				
				echo form_input( $arr ); // code added by preeti on 21st apr 14 for manual testing
							
				
				//echo form_input('reg_regno', ''); // code commented by preeti on 21st apr 14 for manual testing
								
				echo $cap_img;	// code added by preeti on 21st apr 14 for manual testing	
				
				echo form_submit('sub', 'Generate OTP');-->
					
				
				
				<div class="collect-signup" >
			
				<div class="left"><label for="reg_regno">Registration No.<span class="star">*</span></label></div>
				
				<div class="right" >
					
					<input type="text" name="reg_regno" id="reg_regno" <?php echo 'autocomplete="off"'; ?> />
					
				</div>		
				
				</div>				
					
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
	
				
				
				<div class="collect-signup">					
				
					<div class="right">
						
						<input class="button" type="submit" name="sub" value="Generate OTP" />	
						
					</div>
		
				</div>
					
							
				<?php
				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>        
              
    </div>	
    
  
  <?php $this->load->view('template/footer'); ?>
