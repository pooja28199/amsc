<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Registration</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>
		
<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
	
	<div id="changing">

      <div class="regsquaresmall">
      	
      	<?php
          	
          		echo form_open_multipart('doc/add_doc');		
					
		?>			
		
			
			<div class="heading">New Registration</div><!-- Line modified by preeti on 25th mar 14 -->
			
			<div class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
			
			<div class="collect-signup">
				
				<div class="left"><span class="star">*</span>Mandatory Field</div>
			
				<div class="right">&nbsp;</div>		
		
			</div> 
			
			<div class="collect-signup">
			
			<div class="left"><label for="reg_fname">First Name<span class="star">*</span></label></div>
			
			<div class="right" >
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_fname" 
				
				id="reg_fname" value="<?php echo $reg_fname; ?>" />
									
			</div>		
			
			</div>
			
			
			
			<div class="collect-signup" >
			
			<div class="left"><label for="reg_mname">Middle Name</label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_mname" 
				
				value="<?php echo $reg_mname; ?>" />
				
			</div>
	
			</div>
			
			<div class="collect-signup">
			
			<!--  below line modified by preeti on 2nd apr 14 for black-box testing -->
			
			<div class="left"><label for="reg_lname">Last Name</label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_lname" 
				
				value="<?php echo $reg_lname; ?>" />
				
			</div>
	
			</div>
	
			<div class="collect-signup">
			
			<div class="left"><label for="reg_mobile">Mobile<span class="star">*</span></label>
				
				<!-- below line added by preeti on 2nd apr 14 for black-box testing -->
				
				<span class="small">(+91)</span>
				
				
			</div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 2nd apr 14 -->
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" maxlength="10" name="reg_mobile" <?php echo 'autocomplete="off"'; ?> 
				
				id="reg_mobile" value="<?php echo $reg_mobile; ?>" />
				
				<!-- below line added by preeti on 2nd apr 14 for black-box testing -->
				
				<span class="small">( mobile no. should be of 10-digit )</span>
				
			</div>
	
			</div>
			
			
			
			<div id="div_mobile" class="collect-signup" >
			
			<div class="left">&nbsp;</div>
			
			<div class="right" >
				
				<span class="ajax-msg" id="msg_span_mobile"></span>
								
			</div>		
			
			</div>
			
			
	
			
			<div class="collect-signup">
			
			<div class="left"><label for="reg_email">Email<span class="star">*</span></label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 2nd apr 14 -->
				
				<!-- below line modified by preeti on 28th apr 14 for manual testing -->
				
				<input type="text" name="reg_email" id="reg_email" <?php echo 'autocomplete="off"'; ?> 
				
				value="<?php echo $reg_email; ?>" />
				
			</div>
	
			</div>
	
		
			<div id="div_email" class="collect-signup" >
			
			<div class="left">&nbsp;</div>
			
			<div class="right" >
				
				<span class="ajax-msg" id="msg_span_email"></span>
								
			</div>		
			
			</div>	
			
			<!-- start, code added by preeti on 3rd apr 14 -->
			
			
			<div class="collect-signup">		
			
			<div class="right">
				
				<?php echo $cap_img; ?>		
				
			</div>
	
			</div>
			
			<div class="collect-signup">
			
			<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
			
			<div class="right">
				
				<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
				
			</div>
	
			</div>
			
			
			<!-- end, code added by preeti on 3rd apr 14 -->	
				
			
			<div class="collect-signup">
				
				<div class="left">&nbsp;</div>
			
			<div class="right">&nbsp;</div>		
		
			</div> 
					
				
			<div class="collect-signup">
				
				<div class="left">
					
					<!-- below line added by preeti on 12th mar 14 -->
					
					<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
					
					<?php echo form_submit('sub', 'Submit'); ?> </div>
			
			<div class="right">&nbsp;</div>		
		
			</div>         	
          
          <?php							
				echo form_close();
          	
          	?>

        </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

 </body>

</html>