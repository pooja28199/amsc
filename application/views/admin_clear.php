<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title><?php echo $title; ?></title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>

    <div id="changing">

      <div class="loginsquaresmall">

          <p><h2><?php echo $errmsg; ?></h2></p>
          
          <?php
          echo form_open('admin/del_all');
          ?>
          
          <!-- below line added by preeti on 28th apr 14 for manual testing -->
				
		  <input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
					  			
          
          <input type="submit" id="proceed" name="proceed" value="Proceed" />

		  <?php
          echo form_close();
          ?> 	

        </div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script>
	
	$('#proceed').click(function(){
		
		var choice = confirm("Are you sure you want to clear the complete User related Information from Database ? ") ;
		
		if( choice )
		{
			return true;
		}
		
		return false;
				
	});
	
</script>

</body>

</html>