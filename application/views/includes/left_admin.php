<?php

$current_page = $this->uri->segment(2);

?>
<div id="mainleft">

      <div id="contmenu">

        <div id="menu">

          <ul>

            <li  <?php if( $current_page == 'home' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/home">Home</a></li>
            
            <li <?php if( $current_page == 'doc_list' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/doc_list">Users</a></li>
            
            <li <?php if( $current_page == 'doc_list1' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/doc_list1">Approve or Reject</a></li>
            
            <li <?php if( $current_page == 'doc_list2' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/doc_list2">Approved Users</a></li>
			
			<!--<li <?php if( $current_page == 'dak_list' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/dak_list">Dak approved Users</a></li>-->
			
			<li <?php if( $current_page == 'managecall' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>backend/managecall">Manage Call-letter</a></li>
			
			<li <?php if( $current_page == 'managecenters' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>backend/managecenters">Manage Centers</a></li>			
			
			<li <?php if( $current_page == 'printform' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>backend/printform">Print Form</a></li> 
			
			
            	<li <?php if( $current_page == 'iplist' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>backend/iplist">IP whitelist</a></li> 
				
            <li <?php if( $current_page == 'doc_list3' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/doc_list3">Rejected Users</a></li>
            
            <!--<li <?php if( $current_page == 'doc_list21' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/doc_list21">Marking Sheet</a></li>-->
            
            <li <?php if( $current_page == 'not_list' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/not_list">Notifications</a></li>
            
            <li <?php if( $current_page == 'res_list' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/res_list">Result Upload</a></li>
            
            <li <?php if( $current_page == 'log_list' || $current_page == 'send_email' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/log_list">Send Email</a></li>
            
            <li <?php if( $current_page == 'form_list' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/form_list">Email Format</a></li>
                        
            <li <?php if( $current_page == 'set_date' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/set_date">Set Last Date</a></li>
            
            <!-- Below line added by preeti on 25th mar 14 -->
            
            <li <?php if( $current_page == 'admin_db_password' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/admin_db_password">Change&nbsp;DB&nbsp;Password</a></li>
            
            <!-- Below text modified by preeti on 25th mar 14 -->
            
            <li <?php if( $current_page == 'admin_password' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/admin_password">Change&nbsp;Password</a></li>
            
            <li <?php if( $current_page == 'admin_email' || $current_page == 'update_admin_email'  ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/admin_email">Change Email</a></li>
            
            <li <?php if( $current_page == 'clear_pass' || $current_page == 'clear_verify_pass' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/clear_pass">Clear DB</a></li>
            
             <!-- below line added by preeti on 26th apr 14  -->
             
             <li <?php if( $current_page == 'audit_user' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/audit_user">User Audit Trail</a></li>
            
            
            <!-- below line added by preeti on 26th apr 14  -->
             
             <li <?php if( $current_page == 'audit_admin' ){ echo "style='background-color: #787878;'";  } ?> ><a href="<?php echo base_url(); ?>admin/audit_admin">Admin Audit Trail</a></li>
            
            
            
            <!-- below line modified by preeti on 22nd apr 14 for manual testing -->
            
            <li><a href="<?php echo base_url(); ?>admin/logout/<?php echo $this->session->userdata('admin_random'); ?>">Log Out</a></li>

          </ul>
          
        </div>

      </div>

</div>