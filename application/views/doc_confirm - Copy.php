<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Confirm Registration</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<!-- below scripts are added by preeti on 26th mar 14 for black-box testing -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the password typed 
			
			var pass = $('#reg_otp').val();
			
			if( pass != '' )
			{
				//var salt = $('#salt').val(); // commented by preeti on 22nd apr 14 for manual testing
				
				var salt = '<?php echo $salt; ?>'; // added by preeti on 22nd apr 14 for manual testing
				
				pass = md5( pass );
				
				var result = md5( pass + salt );
			
				// set the value of the hidden field
				
				$('#reg_otp_encode').val(result);
				
				// clear the field
				
				$('#reg_otp').val('');	
			}
			
			
		});
		
	});
	
</script>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>

    <div id="changing">

      <div class="regsquaresmall">
      	
      	<div class="heading">Confirm Registration</div> <!-- Line added by preeti on 25th mar 14 -->

          <p>
          	
          	<div class="error">
          		<?php 
          		
          		if( validation_errors() )
				{
					echo validation_errors();
				}
				else 
				{
					echo $errmsg;	
				}          		 
          		
          		?>
          		
          	</div>
          	
          	<?php
                    	
          	echo form_open('doc/validate_otp');
          	
          	?>
          	
          	<!--         		
          	
          		
				
				echo form_label('Registration Number', 'reg_regno');
				
				
				
				// below code added by preeti on 3rd apr 14
				
				$arr = array(
				
				'name' => 'reg_regno',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);
				
				
				echo form_input( $arr ); 
				
				
				
				
				//echo form_input('reg_regno', $this->input->post('reg_regno')); // commented by preeti on 3rd apr 14
							
				echo form_label('One Time Password<br/>(OTP)', 'reg_otp');
				
				
				// below code modified by preeti on 26th mar 14 for black-box testing
				
				$pass_att = array(
				
					'name' => 'reg_otp',
					
					'id' => 'reg_otp',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 25th mar 14 for black-box testing 
				
				-->
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="reg_regno">Registration Number<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" <?php echo 'autocomplete="off"'; ?> name="reg_regno" id="reg_regno" value="" />
						
					</div>
	
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="reg_otp">One Time Password (OTP)<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="password" <?php echo 'autocomplete="off"'; ?> name="reg_otp" id="reg_otp" value="" />
						
					</div>
	
				</div>
				
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>			
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
				
				
				<!-- below code added by preeti on 26th mar 14 for black-box testing -->
				
				<input type="hidden" value="" name="reg_otp_encode" id="reg_otp_encode" />
				
				<!-- below code added by preeti on 28th mar 14 for black-box testing -->
				
				
								
				<!--
				
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Submit');// code added by preeti on 25th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 25th mar 14 for black-box testing
				
				echo anchor(base_url().'doc/new_otp', 'Generate New OTP', array( 'class' => 'link') );
				
				-->
				
				
				<div class="collect-signup">
					
					<div class="left">
						
						<?php echo anchor(base_url().'doc/new_otp', 'Generate New OTP', array( 'class' => 'link') ); ?>
											
					</div>
						
					
					<div class="right">
						
						<input type="submit" name="sub" id="sub" value="Submit" />
						
					</div>
	
				</div>	
				
				
				
			<?php	
				echo form_close();
          	
          	?> 
          	          	
          	
          </p>

        </div>      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>