<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: User's List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />


<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

     <div class="listsquaresmall2">

			<!-- below heading changed by preeti on 7th apr 14 -->

          <p><h2>Approve or Reject User(s)</h2></p>
          
          <?php
          
          if( is_array($records)  && COUNT( $records ) == 0  )
		  {
		  ?>	
		  	<span>No Users Found !</span>
		  <?php
		  }
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
		  
		  ?>         
          	<form id="f1" action="<?php echo base_url(); ?>admin/searchnew3" method="post">
          		
          		<table>
          			
          			<tr>
          				
          				<!--<td align="center" width="250"><span class="txt-label">Range of Registration No.</span></td>-->
          			</tr>
          			
          			<tr>
          				
          				<td><span class="txt-label">From (Reg No.)</span>
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="from"  id="from" /></td>
          				
          				<td><span class="txt-label">To (Reg No.)</span>
          					
          					<!-- below line modified by preeti on 21st apr 14 for manual testing -->
          					
          					<input <?php echo 'autocomplete="off"'; ?> type="text" name="to"  id="to" /></td>
          			          				          			
          			</tr>
          			
          			<tr>
          				
          				<td valign="top" align="center" colspan="4">
          					
          					<input style="margin-left: 205px;margin-top:20px; " type="submit" name="sub" value="Search" />
          					
          				</td>
          				          				
          			</tr>          			
          			 
          			
          			</table></form>
          
           	
          <?php
           $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
         
         $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$i = $uri_arr['offset'] + 1;
			
			$offset = $uri_arr['offset'] ;
				
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	  // add a link to download as excel	
		$files=array();	 
						  $keyword = $this->input->post('keyword');
			  if( $this->input->post( 'keyword' ) )
		{
			$keyword = $this->input->post( 'keyword' );	
			
		}
		else
		{
			$keyword =0;
					
		}
		
		array_push($files,$keyword);  
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) )
		{
			$gender = $this->input->post(  'gender' );
					
		}
		else
		{
			$gender =0;
					
		}
		
		array_push($files,$gender);
			  
		$state = '';
		
		if( $this->input->post( 'state' ) )
		{
			$state = $this->input->post( 'state' );
					
		}
		else
		{
			$state =0;
					
		}
		
		array_push($files,$state);
			  
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) )
		{
			$start_date = $this->input->post( 'start_date' );
					
		}
		else
		{
			$start_date ='0000.00.00';
					
		}
		
		array_push($files,$start_date);
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) )
		{
			$end_date = $this->input->post('end_date' );
					
		}
		else 
		{
			$end_date ='0000.00.00';
		}
		
		array_push($files,$end_date);
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) )
		{
			$start_age = $this->input->post( 'start_age' );
					
		}
		else
		{
			$start_age =0;
					
		}
		
		array_push($files,$start_age);
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) )
		{
			$end_age = $this->input->post('end_age' );
					
		}
		else
		{
			$end_age =0;
					
		}
		
		array_push($files,$end_age);
		
  		// add the list heading for columns	
		
		$ar="";
		
		$this->table->add_row(
		
		'<span class="col-label">S.No</span>' ,
		
		'<span class="col-label">Name</span>', 
			  
		'<span class="col-label">Reg No.</span>',  
		
		'<span class="col-label">Reason</span>', 
		
		'<span class="col-label">Status</span>',
			  
		'<input type = "checkbox" id="check_all" name = "check_all" value = "check_all" />'
		
		);	
			  
	    foreach( $records as $row )
	    {          	
			if($row->u_status=='appr')
			{
				$ar='Approved';						
			}
			else if($row->u_status=='rej')
			{
				$ar='Rejected';
			}
			
			if( $row->u_is_name_change == 'y' )
			{
				$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
			}
			else 
			{
				$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
			}
				
	        $this->table->add_row(
	          	
			'<span class="col-data">'.$i.'</span>',
				
			'<span class="col-data">'.strtoupper($name).'</span>',// modified by preeti on 28th feb 14
					
			'<span class="col-data">'.$row->reg_regno.'</span>',
				
			// below line modified by preeti on 23rd apr 14 for manual testing	
				
			"<textarea name = 'reason_".$row->u_regno."' id='reason_".$row->u_regno."' rows='6' cols='15'>".$row->u_reason."</textarea>",
			
			'<span class="col-data">'.strtoupper( $row->u_status ) .'</span>',
			
			//anchor(base_url().'admin/doc_del/'.$row->reg_id, 'Delete', array('class'=>'del-link')
					
			"<input type = 'checkbox' name = 'doc_ids[]' value = '".$row->u_regno."' />"					
					
			);
				
			$i++;
	    }
	
		echo form_open('admin/submit'); 
	
		// below line added by preeti on 4th mar 14
			
		echo  $this->pagination->create_links();	
			
	?>
			
	<!-- below div added by preeti on 4th mar 14 -->
			
	<div class="clear"></div>
	
	<?php
	
	echo $this->table->generate();
	
	 ?>
	 
	 <!-- below line added by preeti on 21st apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
	 
	  <table><tr><td><input type="submit" name="sbm" id="app"  value="Approve" /></td>
	 	<td><input type="submit" name="sbm" id="rej"  value="Reject" /></td></tr></table> 
	
	
	
				
	
				<?php
	
				echo  $this->pagination->create_links();			
				
				echo form_close();
		}
				  
		?>         

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#start_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#end_date').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>