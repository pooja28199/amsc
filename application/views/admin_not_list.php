<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Notification List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>-->


<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>



</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

      <div class="listsquaresmall">

          <p><h2>List Of Notification</h2></p>
          
          <?php
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
          
          
          if($this->uri->segment(3) != '' )
		  {
		  	$i = $this->uri->segment(3) + 1;	
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
         
		 $this->table->add_row('', '', '', '', '','', '<a href="'.base_url().'admin/not_add/'.'" class="link">Add</a>');
		 
		 $this->table->add_row(
		 
		 '<font class="col-label">S.No</font>', 
		 
		 '<font class="col-label">Title</font>', 
		 
		 '<font class="col-label">Date</font>', 
		 
		 '<font class="col-label">Type</font>',
		 
		 '<font class="col-label">File</font>', 
		 
		 '<font class="col-label">Edit</font>', 
		 
		 '<input type = "checkbox" id="check_all" name = "check_all" value = "check_all" />'
		 
		 );
		 
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		  
          foreach( $records as $row )
          {          	
			
          	$this->table->add_row(
          	
				'<font class="col-data">'.$i.'</font>',
			
				'<font class="col-data">'.strtoupper( substr($row->not_title, 0, 55) ).'</font>' , // modified by preeti on 28th feb 14
				
				'<font class="col-data">'.db_to_calen( $row->not_date ).'</font>' ,
				
				( $row->not_type == 'c' )?'<font class="col-data">CONTENT</font>':'<font class="col-data">FILE</font>',// modified by preeti on 28th feb 14
				
				( $row->not_type == 'l' && $row->not_link !='' )?anchor(base_url().'uploads/not/'.$row->not_link, 'File', array('class'=>'link', 'target'=>'_blank')):'-',
				
				anchor(base_url().'admin/not_edit/'.$row->not_id, 'Edit', array('class'=>'link')),				
				
				//anchor(base_url().'admin/not_del/'.$row->not_id, 'Delete', array('class'=>'del-link'))
				
				"<input type = 'checkbox' name = 'doc_ids[]' value = '".$row->not_id."' />"
									
			);
			
			$i++;
          }

          	echo form_open('admin/not_del');
			
			// below line added by preeti on 5th mar 14
			
			echo  $this->pagination->create_links();	
					
			?>
					
			<!-- below div added by preeti on 5th mar 14 -->
					
			<div class="clear"></div>
			
			<?php
          
			echo $this->table->generate();
		?>	
			<div class="clear"></div>
			
			<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
			
			<input type="submit" name="del" id="del" value="Delete" />
			
			<div class="clear"></div>
			
		<?php	
			echo  $this->pagination->create_links();
			
			echo form_close();
		}
		else
		{
		?>	
			<p style="float: right;clear: both;"><a href="<?php echo base_url(); ?>admin/not_add/" class="link">Add</a></p>
			
			<p><span>No Record Found !</span></p>		
		
		<?php
		}		
		  
		?>         

        </div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script>
	
	$('#check_all').click( function()
    {    
    	if($(this).is(':checked'))
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', true);
				
				//$(this).attr('checked','checked');
						
			});			
		}
		else
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', false);
				
				//$(this).removeAttr('checked');
						
			});
		}
  			
  	});
  	
  	$('#del').click( function()
    {
    	var ischecked = false;
    	
    	$(':checkbox').each(function()
		{
			if( $(this).is(':checked') )
			{
				if( !ischecked )
				{
					ischecked = true;	
				}		
							
			}
			
		});
		
		if( !ischecked )
		{
			// below text changed by preeti on 5th mar 14
			
			alert(" Please select at least one checkbox to delete ");
						
			return false;
		}
		else
		{
			return true;
		}
    	
   	});
	
</script>


</body>

</html>