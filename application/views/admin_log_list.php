<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Send Email</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

<script language="JavaScript" src="<?php echo base_url(); ?>js/admin_log_list.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

      <div class="listsquaresmall">

          <p><h2>Send Email</h2></p>
          
          <?php
          
          if( !empty( $errmsg ) )
          {
          ?>
          
          	<span><?php echo $errmsg; ?></span>
          
          <?php
          }
          ?>
          
          <p>
          	
          	<div >
          		
          		<div>
          			
          			<select id="email" name="email">
          				
          				<option value="">Select Email Format</option>
          				
          				<?php
          				if( $formats != '' )
						{
							foreach( $formats as $obj )
							{
          				?>    
          						<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          				      				
          						<option <?php if( $this->input->post('form_id') != '' ){ if($this->input->post('form_id') == $obj->form_id ){ echo "selected"; }} ?>  value="<?php echo $obj->form_id ; ?>"><?php echo $obj->form_title ; ?></option>
          				
          				<?php
							}
						}
          				?>
          				
          			</select>
          			
          		</div>
          		
          		<div>
          			
          			<!--<textarea readonly="readonly" style="margin-left: 10px;" name="form_content" id="form_content" rows="23" cols="60"></textarea>-->
          			
          			<textarea readonly="readonly" style="margin-left: 10px;margin-right: 10px;" name="form_content" id="form_content" rows="23" cols="60"></textarea>
          			
          			<!-- above code commented and below line added by preeti on 3rd apr 14 -->
          			
          			<!--<div style="margin-left: 10px;overflow: scroll;width: inherit;height:300px;clear: both;border: 1px solid grey;margin: 10px;padding: 5px;" name="form_content" id="form_content" ></div>-->
          			
          		</div>
          		
          	</div>         	
          	          	          	          	
          </p>
          
          <!-- below div modified by preeti on 5th mar 14 -->
          
          <div style="height: 50px;margin:5px;">
          	
          	<div id="excel_id" style="float: left;padding: 5px;cursor: pointer;background-color:#993300;color:#FFFFFF;margin:10px;">Upload Excel</div>
          
          	<div id="list_id" style="float: right;padding: 5px;cursor: pointer;background-color:#993300;color:#FFFFFF;margin:10px;">Choose From a List</div>
          
          </div>
          
          <div id="up_excel">
          	
          	<?php
          	
          	$attributes = array('name' => 'frm_excel', 'id' => 'frm_excel');
          
          	echo  form_open_multipart('admin/send_excel_email', $attributes); 
          	
			?>
			
          	<label>Upload Excel</label>
          	
          	<input type="file" name="excel_file" id="excel_file" />
          	
          	<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->
          	
          	<input type="hidden" name="form_id" value="<?php if( $this->input->post('form_id') != '' ){ echo $this->input->post('form_id'); } ?>" id="form_id" /><!-- its value is set by AJAX response -->	
	
			<!-- below line added by preeti on 25th apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
	
			<?php  echo form_submit('send', 'Send Mail'); 
			
			 echo  form_close(); 
			
			?>
	
          	
          </div>         
          
          <div id="show_list">
          
          <p>
          	
          	<?php echo form_open('admin/user_search'); ?>
          	
          	<?php echo form_label('Keyword', 'keyword'); ?>
          	
          	<?php
          	
          	//below line added by preeti on 21st apr 14 for manual testing
          	
          	$key_att = array(
				
					'name' => 'keyword',
					
					'id' => 'keyword',
					
					'value' => $keyword,
					
					'autocomplete' => 'off'
				
				);
          	
          	
          	echo form_input( $key_att ); // line modified by preeti on 21st apr 14 for manual testing ?> 
          	
          	&nbsp;<font class="col-data">( Reg no., Email, Name )</font>
          	
          	<?php echo form_submit('sub', 'Search'); ?>
          	
          	<?php echo form_close(); ?>
          	
          </p>
          
          <?php  
          
          $attributes = array('name' => 'frm', 'id' => 'frm');
          
          echo  form_open('admin/send_email', $attributes); 
		  
          if($this->uri->segment(3) != '' )
		  {
		  	$i = $this->uri->segment(3) + 1;	
		  }
		  else 
		  {
			$i = 1;	  
		  } 	  
		  
         
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		 	
			$this->table->add_row(
          	
				'<font class="col-label">S.No</font> <input type = "checkbox" id="check_all" name = "check_all" value = "check_all" />', 
				
				'<font class="col-label">Name</font>', 
				
				'<font class="col-label">Reg No.</font>', 
				
				'<font class="col-label">Email</font>'  
				
			);
		
		  foreach( $records as $row )
          {
          	
			$email =  str_replace( '@', '[at]', $row->reg_email );// code added on 27th mar 14 by preeti for black-box testing
			
			$email =  str_replace( '.', '[dot]', $email );		// code added on 27th mar 14 by preeti for black-box testing
			 
			$regid=$row->reg_id;
			
          	$this->table->add_row(
          	
				'<font class="col-data">'.$i.' <input type = "checkbox" name = "doc_ids[]" value = "'.$row->reg_id.'" /></font>',
			
				'<font class="col-data">'.strtoupper( $row->name ).'</font>', // modified by preeti on 28th feb 14
				
				'<font class="col-data">'.$row->reg_regno.'</font>',
				
				'<font class="col-data">'.strtoupper( $email ).'</font>'// modified by preeti on 27th mar 14 for black-box testing
				
							
			);
			
			$i++;
          }			

			echo $this->table->generate();

			echo  $this->pagination->create_links();
		}	
		  
		?>

			<!-- below line modified by preeti on 18th mar 14 for the white box bug fixing -->

			<input type="hidden" name="form_id_hid" 
					
			value="<?php if( $this->input->post('form_id') != '') { echo $this->input->post('form_id'); } ?>" 
			
			id="form_id_hid" /><!-- its value is set by AJAX response -->	
	
			<!-- below line added by preeti on 12th mar 14 -->	
	
			<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
	
			<!-- below line added by preeti on 25th apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
				
				
	
			<input type="submit" name="send" value="Send Mail" />
	
			<?php  echo  form_close(); ?>
			
		</div>	         
		
		</div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>