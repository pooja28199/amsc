<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Forgot Password</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
	
	<div id="changing">

		<div class="regsquaresmall">
			
		<div class="heading">Forgot Password</div>

		
			<span  >
          		
          	<?php 
          	     	
			if( validation_errors() )
			{
				echo strip_tags( validation_errors() ) ;
			}
			else 
			{
				echo strip_tags( $errmsg );	
			}			 
          	
          	?>
          	
          	</span>

          <p>
          	         	
          	<?php
          	
          		echo form_open('doc/send_password');
				
				?>
				
				<!--echo form_label('Email', 'reg_email');
				
				
				
				// below code added by preeti on 21st apr 14 for manual testing
				
				$arr = array(
				
				'name' => 'reg_email',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);				
				
				echo form_input( $arr ); // code added by preeti on 21st apr 14 for manual testing
				
				//echo form_input('reg_email', ''); // code commented by preeti on 21st apr 14 for manual testing
				
				echo form_submit('sub', 'Submit');-->
				
				
				
				<div class="collect-signup" >
			
				<div class="left"><label for="reg_email">Email<span class="star">*</span></label></div>
				
				<div class="right" >
					
					<input type="text" name="reg_email" id="reg_email" <?php echo 'autocomplete="off"'; ?> />
					
				</div>		
				
				</div>	
				
				
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
				
				
				<div class="collect-signup">
			
					<div class="left">&nbsp;</div>
					
					<div class="right">
						
						<input type="submit" value="Submit" name="sub" />
						
					</div>
	
				</div>
				
							
				<?php
				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>      

      
    </div>

	
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>