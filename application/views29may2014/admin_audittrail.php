<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: User's List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->

<!--<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>-->

<script src="<?php echo base_url(); ?>calendar/jquery.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />


<script language="JavaScript" src="<?php echo base_url(); ?>js/signup.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

     <div class="listsquaresmall2">

         
          <B><center>LIST OF AUDIT TRAIL</center></B>
          
           	
          <?php
          $i=1;
       
		//array_push($files,$file_path11);
		
		//print_r($files);
			  /* $url1 = base_url().'admin/file_pdf';	
			  
			  
			  $co = implode("-", $files);
			  
			  
			 // echo $co;
			  if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			  {
			  	$encoded = urlencode( $keyword ); // encoded keyword
				
			  	//$url1 = base_url().'admin/file_pdf/'.$keyword.'/'.$gender.'/'.$state.'/'.$start_date.'/'.$end_date.'/'.$start_age.'/'.$end_age;
				//$url1 = base_url().'admin/file_pdf4('.$keyword.','.$gender.','.$state.','.$start_date.','.$end_date.','.$start_age.','.$end_age.')';
				$url1 = base_url().'admin/file_pdf/'.$co; 
			  }
			  $url31 = base_url().'admin/file_xls';	
			  
			  if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			  {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url31 = base_url().'admin/file_xls/'.$co;
				 
			  }
			 	$url21 = base_url().'admin/file_pdf1';	
			  
			  if( $keyword||$gender||$state||$start_date||$end_date||$start_age||$end_age )
			  {
			  	$encoded = urlencode( $keyword); // encoded keyword
				
			  	$url21 = base_url().'admin/file_pdf1/'.$co;
				 
			  }
				$url211 = base_url().'admin/dozipall';
				//$url2=base_url().'Member_con/topdf1';
			 $this->table->add_row('<a class="link" href="'.$url1.'">Print</a>', '<a class="link" href="'.$url21.'">Export</a>',
			  '<a class="link" href="'.$url31.'">Export Excel</a>', '<a class="link" href="'.$url211.'">zipall</a>', '&nbsp;','&nbsp;', '&nbsp;'
			  
			  );
			   * */
			/*  			  $this->table->add_row('<input type="submit" name="sbm" value="Print all Docs" />',
			   '<input type="submit" name="sbm"  value="Download Zip" />',
			  '<input type="submit" name="sbm" value="Print Preview" />', 
			  '<input type="submit" name="sbm" value="Generate Pdf" />', '<input type="submit" name="sbm" id="del" value="Delete" />','&nbsp;', '&nbsp;'
			  
			  );
			  */
  // add the list heading for columns	
				$ar="";
			  $this->table->add_row('<strong>S.No</strong>' ,'<strong>Name</strong>', 
			  
			  '<strong>Reg No.</strong>', '<strong>Login Time</strong>','<strong>Logout Time</strong>');	
			  
			 // print_r($records);
	          foreach( $records as $row )
	          {          	
		
				if( $row->u_is_name_change == 'y' )
				{
					$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
				}
				else 
				{
					$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
				}
				
	          	$this->table->add_row(
	          	
					$i,
				
					strtoupper( $name ),// modified by preeti on 28th feb 14
					
					$row->reg_regno,
					
					$row->logon_date,$row->logoff_date					
					
				);
				
				$i++;
	          }
	
	
	// below line added by preeti on 5th mar 14
			
	
			
	 
	
	echo  $this->pagination->create_links();	
			echo $this->table->generate();
	?>
			
	<!-- below div added by preeti on 5th mar 14 -->
			
	<div class="clear"></div>
	
	

        </div>     </div>

    </div>

   

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


<script type="text/javascript">

			$('#start_date').datepick({showOnFocus: false, showTrigger: '#calImg'});
			
			$('#end_date').datepick({showOnFocus: false, showTrigger: '#calImg'});		

</script>

</body>

</html>