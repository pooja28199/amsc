<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Email Format List</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script language="JavaScript" src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

<style type="text/css" media="screen">
	
	#pagination a, #pagination strong {
	 background: #e3e3e3;
	 padding: 4px 7px;
	 text-decoration: none;
	border: 1px solid #cac9c9;
	color: #292929;
	font-size: 13px;
	}

	#pagination strong, #pagination a:hover {
	 font-weight: normal;
	 background: #cac9c9;
	}	
	
</style>

</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_admin'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_admin'); ?>
    
    <div id="changing">

      <div class="listsquaresmall">

          <p><h2>List of Email Formats</h2></p>
          
          <!-- below code modified by preeti on 27th mar 14 for black-box testing -->
          
          <span>
         
          <?php 
				
		  if( validation_errors() )
		  {
		  	
			 echo (validation_errors());
		  }
		  else 
		  {
		  	
			 echo ( $errmsg );	
		  }				 
				
		  ?>
          
          </span>
          
          <?php
          
          if($this->uri->segment(3) != '' )
		  {
		  	$i = $this->uri->segment(3) + 1;	
		  }
		  else 
		  {
			$i = 1;	  
		  }         
          
         
		 $this->table->add_row('', '', '', '', '', '<a href="'.base_url().'admin/form_add/'.'" class="link">Add</a>');
		 
		 if( is_array($records)  && COUNT( $records ) > 0  )
		 {
		  
		  $this->table->add_row('<strong>S.No</strong>', '<strong>Title</strong>', '<strong>Date</strong>', 
		 
		 '<strong>Subject</strong>', '<strong>Edit</strong>', 
		 
		 
		 
		 '<input type = "checkbox" id="check_all" name = "check_all" value = "check_all" />'
		 
		 );
		 		  
          foreach( $records as $row )
          {          	
			
          	$this->table->add_row(
          	
				$i,
			
				strtoupper( substr($row->form_title, 0, 25) ),  // modified by preeti on 28th feb 14
			
				db_to_calen( $row->form_createdon ) ,
				
				strtoupper( substr($row->form_subject.'..', 0, 20) ),// modified by preeti on 28th feb 14
				
				anchor(base_url().'admin/form_edit/'.$row->form_id, 'Edit', array('class'=>'link')),				
				
				
				
				"<input type = 'checkbox' name = 'doc_ids[]' value = '".$row->form_id."' />"
									
			);
			
			$i++;
          }
          
            echo form_open('admin/form_del'); 
			
			// below line added by preeti on 5th mar 14
			
			echo  $this->pagination->create_links();	
					
			?>
					
			<!-- below div added by preeti on 5th mar 14 -->
					
			<div class="clear"></div>
			
			<?php 

			echo $this->table->generate();
			
			?>	
			
			<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
			<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
			
			<input type="submit" name="del" id="del" value="Delete" />
			
			<div class="clear"></div>
			
			<?php

			echo  $this->pagination->create_links();
			
			echo form_close();
		}
		else
		{
		?>	
			
			<p style="float: right;clear: both;"><a href="<?php echo base_url(); ?>admin/form_add/" class="link">Add</a></p>
			
			<p>No Record Found !</p>
			
		<?php
			
		} 	 	
		  
		?>         

        </div>     

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script>
	
	$('#check_all').click( function()
    {    
    	if($(this).is(':checked'))
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', true);
				
				//$(this).attr('checked','checked');
						
			});			
		}
		else
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', false);
				
				//$(this).removeAttr('checked');
						
			});
		}
  			
  	});
  	
  	$('#del').click( function()
    {
    	var ischecked = false;
    	
    	$(':checkbox').each(function()
		{
			if( $(this).is(':checked') )
			{
				if( !ischecked )
				{
					ischecked = true;	
				}		
							
			}
			
		});
		
		if( !ischecked )
		{
			// below text changed by preeti on 5th mar 14
			
			alert(" Please select at least one checkbox to delete ");
			
			return false;
		}
		else
		{
			return true;
		}
    	
   	});
	
</script>


</body>

</html>