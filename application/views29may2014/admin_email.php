<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Change Admin Email</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin'); ?>
    
	
	<div id="changing">

		<div class="emailsquaresmall">

		<h2>Change Admin Email</h2>

			<span class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo strip_tags( $errmsg );	
				}				 
				
				?>
				
			</span>

          <p>         	
          	
          	<?php          	
          		          	
          		echo form_open('admin/update_admin_email');
			
				/*echo form_label('Email', 'reg_email');
				
				echo form_input('reg_email', '');	
				
				echo form_submit('sub', 'Submit');*/
							
			?>
			
					
								
				<label for="admin_email">Email</label>
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input type="text" <?php echo 'autocomplete="off"'; ?> size="30" name="admin_email" value="<?php echo $admin_email; ?>" />
				
				
				<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
				<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
				
				
				
				
				<input type="submit" name="sub" value="Submit" />
				
				
				
			<?php				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div> 
              
    </div>
        
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>