<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>Admin :: Edit Notification</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>js/signup.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>asset/ckeditor/ckeditor.js"></script>

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin'); ?>
	
	<div id="changing">

      <div class="cksquaresmall">
      	
      	<?php
          	
          		echo form_open_multipart('admin/not_update');
					
		?>
			
			<div class="heading">Edit Notification</div>
			
			<div class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo ( $errmsg );	
				}				 
				
				?>
				
			</div>
			
			<div class="collect-signup">
				
				<div class="left"><span class="star">*</span>Mandatory Field</div>
			
				<div class="right">&nbsp;</div>		
		
			</div> 
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_title">Title<span class="star">*</span>
								
			</label></div>
			
			<div class="right" >
				
				<!--<input type="text" maxlength="50" name="not_title" id="not_title" 
				
				value="<?php echo $record->not_title; ?>" />-->
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
									
				<input <?php echo 'autocomplete="off"'; ?> type="text" name="not_title" id="not_title" 
				
				value="<?php echo $record->not_title; ?>" />					
									
			</div>		
			
			</div>			
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_date">Date<span class="star">*</span></label></div>
			
			<div class="right">
				
				<input type="text" readonly="readonly" name="not_date" id="not_date" 
				
				value="<?php echo db_to_calen( $record->not_date ) ; ?>" />
				
				<span  style="display: none;">
				
					<img id="calImg" src="<?php echo base_url(); ?>calendar/calendar.gif" alt="Popup" style="margin-left:5px;" class="trigger">
								
				</span>
		
			</div>
			
			</div>
			
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_author">Author<span class="star">*</span></label></div>
			
			<div class="right">
				
				<!-- below line modified by preeti on 21st apr 14 for manual testing -->
				
				<input <?php echo 'autocomplete="off"'; ?> type="text" name="not_author" id="not_author" 
				
				value="<?php echo $record->not_author; ?>" /></div>
			
			</div>
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_type">Type<span class="star">*</span></label></div>
			
			<div class="right">
				
				<select name="not_type" id="not_type">
					
					<option value="">Select</option>
					
					<option value="c" <?php if( isset( $_REQUEST['not_type'] ) &&  $_REQUEST['not_type'] == 'c' ){ echo "selected"; }else if( $record->not_type == 'c' ){ echo "selected"; } ?> >Add Content</option>
					
					<option value="l" <?php if( isset( $_REQUEST['not_type'] )){ if( $_REQUEST['not_type'] == 'l' ){ echo "selected"; }}else if( $record->not_type == 'l' ){ echo "selected"; } ?> >Add File</option>
					
				</select>
				
			</div>
			
			</div>
			
			
			<div id="con_div">
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_content">Content<span class="star">*</span></label></div>
			
			<div class="right">&nbsp;</div>
			
			</div>
			
			
			<div >
			
			<div style="width: 500px;clear: both;" >
				
				<textarea cols="80" id="not_content" name="not_content" rows="10"><?php echo $record->not_content; ?></textarea>
                
                <script>

                    CKEDITOR.replace('not_content');

                </script>
				
			</div>
			
			</div>	
			
			</div>
			
			
			<div id="link_div">
			
			
			<?php
			if( $record->not_link != '' )
			{
				$not_url = base_url().'uploads/not/'.$record->not_link;
			?>
			
				<div class="collect-signup">
			
					<div class="left"><label for="not_link">Uploaded File<span class="star">*</span></label></div>
					
					<div class="right">
						
						<a target="_blank" class="link"  href="<?php echo $not_url; ?>">File</a>
						
					</div>
				
				</div>
			
			<?php
			}
			?>
			
			<div class="collect-signup">
			
			<div class="left"><label for="not_link">File ( Type: pdf / Size: 200KB )<span class="star">*</span></label></div>
			
			<div class="right">
				
				<input type="file" name="not_link" id="not_link"  />
				
			</div>
			
			</div>
			
			</div>

			
			
			<!--<div class="collect-signup">
			
			<div class="left"><label for="not_content">Content<span class="star">*</span></label></div>
			
			<div class="right">&nbsp;</div>
			
			</div>
			
			
			<div >
			
			<div style="width: 500px;clear: both;" >
				
				<textarea cols="80" id="not_content" name="not_content" rows="10">
                
                <?php echo $record->not_content; ?>
                
                </textarea>
                
                <script>

                    CKEDITOR.replace('not_content');

                </script>
				
			</div>
			
			</div>	-->
			
			
			<div class="collect-signup">
				
				<div class="left">&nbsp;</div>
			
			<div class="right">
				
				<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
				<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
				
				<input type="hidden" name="not_id" value="<?php echo $record->not_id; ?>" />
					
					<?php echo form_submit('sub', 'Submit'); ?> </div>		
		
			</div>         	
          
          <?php						
				echo form_close();
          	
          	?>

        </div>     

    </div>
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<script type="text/javascript">

			$('#not_date').datepick({showOnFocus: false, showTrigger: '#calImg'});

</script>
		

</body>

</html>