<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: New OTP</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_user'); ?>
  
   <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>
	
	<div id="changing">

		<div class="regsquaresmall">

		<div class="heading">New OTP</div>

			<span>
          		
          	<?php 
          	     	
			if( validation_errors() )
			{
				echo strip_tags( validation_errors() ) ;
			}
			else 
			{
				echo strip_tags( $errmsg );	
			}			 
          	
          	?>
          	
          	</span>

          <p>
          	         	
          	<?php
          	
          		echo form_open('doc/create_otp');
				
				?>
				
				<!--echo form_label('Registration No.', 'reg_regno');
				
				
				// below code added by preeti on 21st apr 14 for manual testing
				
				$arr = array(
				
				'name' => 'reg_regno',
				
				'value' => '',
				
				'autocomplete' => 'off'
				
				);				
				
				echo form_input( $arr ); // code added by preeti on 21st apr 14 for manual testing
							
				
				//echo form_input('reg_regno', ''); // code commented by preeti on 21st apr 14 for manual testing
								
				echo $cap_img;	// code added by preeti on 21st apr 14 for manual testing	
				
				echo form_submit('sub', 'Generate OTP');-->
					
				
				
				<div class="collect-signup" >
			
				<div class="left"><label for="reg_regno">Registration No.<span class="star">*</span></label></div>
				
				<div class="right" >
					
					<input type="text" name="reg_regno" id="reg_regno" <?php echo 'autocomplete="off"'; ?> />
					
				</div>		
				
				</div>				
					
				
				<div class="collect-signup">						
				
					<div class="right">
						
						<?php echo $cap_img; ?>		
						
					</div>
		
				</div>
				
				
				
				<div class="collect-signup">
			
					<div class="left"><label for="captcha">Captcha<span class="star">*</span></label></div>
					
					<div class="right">
						
						<input type="text" autocomplete="off" name="captcha" id="captcha" value="" />
						
					</div>
	
				</div>
	
				
				
				<div class="collect-signup">					
				
					<div class="right">
						
						<input type="submit" name="sub" value="Generate OTP" />	
						
					</div>
		
				</div>
					
							
				<?php
				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>        
              
    </div>	
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

</body>

</html>