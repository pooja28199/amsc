<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Results</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />

<script src="<?php echo base_url(); ?>js/jquery.min.js"></script> <!-- added by preeti on 20th feb 14 -->

<script src="<?php echo base_url(); ?>js/marquee.js"></script> <!-- added by preeti on 20th feb 14 -->


</head>

<body>

<div id="container">

	<?php $this->load->view('includes/header_user'); ?>
  
  <div id="main">

    <?php $this->load->view('includes/left_user_out'); ?>

    <div id="changing">

      <div class="contentsquaresmall">

		  <p>
          	
          	<h2>RESULTS</h2>     	
          
          </p>
          
          <!--<marquee behavior="scroll" direction="up" scrollamount="2" height="100" width="350">-->
         
         <!-- above line commented below line added by preeti on 20th feb 14 -->
         
         <marquee behavior="scroll" direction="up" scrollamount="2">
         
          <?php
          foreach( $record as $val )
		  {
		  	
			$url = base_url().'uploads/result/'.$val->res_file_path;
          ?>
          
          	<!-- below line modified by preeti on 3rd mar 14 -->
          
          	<div class="not_left"> <a target="_BLANK" href="<?php echo $url; ?>"><?php echo strtoupper( $val->res_title ) ; ?> - <?php echo db_to_calen( $val->res_date ); ?></a> </div>

		  <?php
		  }		  
		  ?>	

		</marquee>

        </div>      

    </div>

  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>

<!-- script tag added by preeti on 20th feb 14 -->

<script>
	
	$('marquee').marquee('pointer').mouseover(function () {
  $(this).trigger('stop');
}).mouseout(function () {
  $(this).trigger('start');
}).mousemove(function (event) {
  if ($(this).data('drag') == true) {
    this.scrollLeft = $(this).data('scrollX') + ($(this).data('x') - event.clientX);
  }
}).mousedown(function (event) {
  $(this).data('drag', true).data('x', event.clientX).data('scrollX', this.scrollLeft);
}).mouseup(function () {
  $(this).data('drag', false);
});
	
</script>

</body>

</html>