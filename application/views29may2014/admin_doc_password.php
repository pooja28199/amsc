<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">

<head>

<title>User :: Edit User Password</title>

<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>calendar/jquery.datepick.css" id="theme"><!-- for calender -->
	
<script src="<?php echo base_url(); ?>calendar/jquery-1.4.2.min.js"></script><!-- for calender -->

<script type="text/javascript" src="<?php echo base_url(); ?>calendar/jquery.datepick.js"></script><!-- for calender -->


<link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type="text/css" media="all" />


<!-- below scripts added by preeti on 26th mar 14 for black-box testing -->


<script type="text/javascript" src="<?php echo base_url(); ?>js/md5.js"></script>

<script>
	
	$(document).ready(function(){
		
		$('#sub').click(function()
		{
			// retrieve the value of the old password typed 
			
			var pass;
			
			var result;
			
			
			
			// retrieve the value of the new password typed 
			
			pass = $('#reg_pass').val();
			
			if( pass != '' )
			{
			
				// check if the password matches the regex
				
				// if,  added by preeti on 3rd apr 14
				
				if( pass.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z@#$%]{8,}$/) )
				{
			
					result = md5( pass );
					
					// set the value of the hidden field
					
					$('#reg_pass_encode').val(result);
					
					// clear the field
					
					$('#reg_pass').val('');
				
				}
				else // else  added by preeti on 3rd apr 14
				{
					alert("Password is not Valid");
					
					$('#reg_pass').val('');
					
					$('#cpassword').val('');	
					
					return false;
				}
				
			}
			
			
			// retrieve the value of the confirm password typed 
			
			pass = $('#cpassword').val();
			
			if( pass != '' )
			{			
				result = md5( pass );
				
				// set the value of the hidden field
				
				$('#cpassword_encode').val(result);
				
				// clear the field
				
				$('#cpassword').val('');
			
			}
			
		});
		
	});
	
</script>



</head>

<body>

<div id="container">

<?php $this->load->view('includes/header_admin'); ?>
  
   <div id="main">

    
    <?php $this->load->view('includes/left_admin'); ?>
    
	
	<div id="changing">

		<div class="passsquaresmall">

		<h2>Edit Password - <?php echo $name.'('.$reg_regno.')'; ?></h2>

			<span class="error">
				
				<?php 
				
				if( validation_errors() )
				{
					echo (validation_errors());
				}
				else 
				{
					echo strip_tags( $errmsg );	
				}				 
				
				?>
				
			</span>

          <p>
             	
          	
          	<?php          	
          		          	
          		echo form_open('admin/update_doc_password');
				
				echo form_label('New Password', 'reg_pass');
				
				
				// below code modified by preeti on 26th mar 14 for black-box testing
				
				$pass_att = array(
				
					'name' => 'reg_pass',
					
					'id' => 'reg_pass',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 26th mar 14 for black-box testing 
							
										
				echo form_label('Confirm Password', 'cpassword');
				
				
				// below code modified by preeti on 26th mar 14 for black-box testing
				
				$pass_att = array(
				
					'name' => 'cpassword',
					
					'id' => 'cpassword',
					
					'autocomplete' => 'off'
				
				);
				
				echo form_password( $pass_att ); // code modified by preeti on 26th mar 14 for black-box testing 
				
				echo form_hidden('reg_id', $reg_id);
				
				?>
				
				<!--  hidden field added by preeti on 26th mar 14 for black-box testing -->
				
				<input type="hidden" name="reg_pass_encode" id="reg_pass_encode" />
				
				<input type="hidden" name="cpassword_encode" id="cpassword_encode" />
				
				
				<!-- below line added by preeti on 21st apr 14 for manual testing -->
				
				<input type="hidden" name="admin_random"  value="<?php echo $admin_random; ?>"/>
			
				
				<?php	
				
				
				$sub_att = array('name' => 'sub', 'id' => 'sub', 'value' => 'Submit');// code added by preeti on 26th mar 14 for black-box testing
				
				echo form_submit( $sub_att );// code modified by preeti on 26th mar 14 for black-box testing
				
				
				echo form_close();
          	
          	?>           	          	
          	
          </p>

        </div>      

      
    </div>

	
    
  </div>

  <?php $this->load->view('includes/footer'); ?>

</div>

<?php $this->load->view('includes/footer_bottom'); ?>


</body>

</html>