<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	public function Ajax()
	{
		parent::__CONSTRUCT();
		
		$this->load->model('doc_model');
		
		$this->load->model('admin_model');
	}
	
	public function ajax_check_username()
	{
		$res = $this->doc_model->ajax_check();
		
		echo $res;		
	}
	
	public function ajax_check_mobile()
	{
		$res = $this->doc_model->ajax_check_mobile();
		
		echo $res;		
	}
	
	public function ajax_check_email()
	{
		$res = $this->doc_model->ajax_check_email();
		
		echo $res;		
	}
	
	public function ajax_check_aadhar()
	{
		$res = $this->doc_model->ajax_check_aadhar();
		
		echo $res;		
	}
	
	public function ajax_check_pan()
	{
		$res = $this->doc_model->ajax_check_pan();
		
		echo $res;		
	}
	
	// code commented by preeti on 11th apr 14 for black-box testing
	
	/*public function ajax_email_format()
	{
		$res = $this->admin_model->ajax_email_format();
		
		$data = str_replace('<br />', '\n', $res);
		
		$data = str_replace('\n', '', $data);
		
		echo trim( $data );		
	}*/	
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	public function ajax_email_format()
	{
		$res = $this->admin_model->ajax_email_format();
		
		if($res)
		{
			$data = str_replace('<br />', '\n', $res);
		
			$data = str_replace('\n', '', $data);
			
			echo trim( $data );	
		}
		else 
		{
			echo "No Format Found !";
		}
		
				
	}
	
	public function ajax_update_otp()
	{
		$rand=$this->generate_random_num();
		$res = $this->admin_model->update_admin_otp($rand);
		$this->admin_model->con_login_session();
		echo $res;
		
	}
	
	public function generate_random_num( $num = 8 ) // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, $num);	
		return 	$random;
	}
	
}
