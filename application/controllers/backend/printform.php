<?php

class printform extends CI_Controller
{
public $vjdb;

public function __CONSTRUCT()
{


parent::__CONSTRUCT();

$this->load->model('backend/managecall_model');
$this->load->model('cletter_model');
$this->load->model('admin_model');
$this->load->library('vj'); 
$this->vjdb=$this->vj->loadclass('db'); 
//$this->load->helper('vj');
}


public function index($errmsg='')
{

$data=array();
    $data['errmsg'] = urldecode( $errmsg );
	
$data['records']=$this->managecall_model->loadrecords(); 
 
$this->load->view('backend/printform/default',$data);
}


public function downloadform()
{
	
$res=$this->admin_model->get_last_date();
$icenter=$res->icenter;	
	
$this->form_validation->set_rules('from','From','required');
$this->form_validation->set_rules('to','To','required');
if($this->form_validation->run()!=TRUE)
{
$err="All fields are required.";
$er=urlencode($err);
redirect('backend/printform/index/'.$err);
}
else
{
$from=$this->input->post('from');
$to=$this->input->post('to');
$limitstart=$from-1;

$limit=$to-$limitstart;
$html='';

$sql="SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname ) name FROM register reg JOIN user u ON reg.reg_regno = u.u_regno WHERE reg_status = 'c' AND pay_status=1 AND icenter='$icenter' ORDER BY appr_dt asc,u_id asc limit $limitstart,$limit ";
$query =$this->db->query($sql);
$rows=$query->result();



//$ct=2521;
foreach($rows as $row)
{
$regno=$row->reg_regno;
//echo $ct.'-'.$regno."<br />";
//$ct++;
$res = $this->admin_model->get_user_detail( $regno );   
$html.= $this->get_pdf_html( $res); // second parameter added by preeti on 13th may 14
$html.='<div style="page-break-after: always"></div>';	  				    
	


}


echo $html;




//	$this->_gen_pdf($html,'A4');  

}




}


public function getUserCenters($pan,$aadhar)
	{
		$string='';
		$qry='';
		
		$res=$this->admin_model->get_last_date();
		$cIcenter=$res->icenter;
		
		if($pan && empty($aadhar))
		$qry.=" AND reg_pan='$pan'";
		elseif($aadhar && empty($pan))
		$qry.=" AND reg_aadhar='$aadhar'";
		elseif(!empty($aadhar) && !empty($pan))
		$qry.=" AND (reg_aadhar='$aadhar' OR reg_pan='$pan')";	
		
		$sql="select * from register where icenter!='$cIcenter' $qry";
		
		$query=$this->db->query($sql);
		$icenter=array();
		$results=$query->result();
		if ($query->num_rows() > 0)
		{
			foreach($results as $result)
			{
				$code=$result->icenter;
				if($code)
				$icenter[$code]=$this->getIcenterInfo($code).' ( '.$result->reg_regno.' ) ';
			}		
		} 	

		if(!empty($icenter))
		$string=implode('<br>',$icenter);
		
		return $string;
		
	}
	
	public function getIcenterInfo($code)
	{
		$sql="select * from icenters where code='$code'";
		$query=$this->db->query($sql);
		$result=$query->result();
		return $result[0]->label;
	}
	
	public function getStateNameFromId($state_id){
		
		$state_name='';
		$query = $this->db->query("SELECT * FROM state WHERE state_id=$state_id");
		$result=$query->result();
		if($query->num_rows>0)
		$state_name=$result[0]->state_name;
		
		return $state_name;
		
	}

public function get_pdf_html( $res, $where = '' ) // parameter added by preeti on 13th may 14
	{
		//$image_src = base_url()."uploads/photo/".$res->u_photo;
	
		
		// above line commented and below lines added by preeti on 13th may 14
		
		if( $where == 'pdf' )
		{
			$image_src = FCPATH."uploads/photo/".$res->u_photo; 	
		}
		else 
		{
			$image_src = base_url()."uploads/photo/".$res->u_photo; 	
		}
		
		// .image-col property modified by preeti on 25th feb 14
		
		$html = "<style>
		
		.pdf{height:auto;width:100%;font-size:12px;font-family: 'Calibri', Arial, Verdana;}
		
		.left-col{float:left;width:40%; }
		
		.right-col{float:left;width:40%;}
		
		.image-col{float:right;width:20%;}
		#profimg{position:absolute;margin-left:500px;}
		</style>
		
		<div class='pdf'>
		
		<h3 align='center'><u>Application Form Details</u></h3>
		
		<div class='left-col' >Registration No.</div>
		
		<div class='right-col' >".$res->reg_regno."</div>
		
		<div id='profimg' class='image-col' ><img src='".$image_src."' /></div>
		
		
		<div class='left-col' >Registration Date</div>
		
		<div class='right-col' >".db_to_calen( $res->u_updatedon )."</div>";
		
		if( $res->u_is_name_change == 'y' )
		{
			$html .= "<div class='left-col' >Name</div>
				
			<div class='right-col' >".strtoupper( $res->new_fname.' '.$res->new_mname.' '.$res->new_lname ) ."</div>";		
		}
		else 
		{
			$html .= "<div class='left-col' >Name (as in Matric Certificate)</div>
		
			<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";
		}
		
		$html .= "<div class='left-col' >Name Changed</div>
				
		<div class='right-col' >".( ($res->u_is_name_change == 'y')?'YES':'NO' )."</div>";
				
			
		if( $res->u_is_name_change == 'y' )
		{
				
				$html .= "<div class='left-col' >Old Name (as in Matric Certificate)</div>
				
				<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";		
				
				
				$html .= "<div class='left-col' >Name Change Certificate</div>
				
				<div class='right-col' >".strtoupper( $res->name_cft_no.' '.$res->name_cft_office.' '.db_to_calen($res->name_cft_date) ) ."</div>";		
				
				
		}
			
			if( $res->u_sex == 'm' )
			{
				$gender = 'MALE';
			}
			else
			{
				$gender = 'FEMALE';
			} 
			
			$html .= "<div class='left-col' >Gender</div>
				
				<div class='right-col' >".$gender."</div>";	
			
			$u_dob = $res->u_dob;
						
			
			$html .= " <div class='left-col' >Date Of Birth</div>
				
				<div class='right-col' >".db_to_calen( $u_dob ) ."</div>";	
			
			$html .= " <div class='left-col' >Age (As on 31st Dec)</div>
				
				<div class='right-col' >".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</div>";
			
			
			// below lines added by preeti on 19th feb 14
			
			$html .= " <div class='left-col' >Father's Name</div>
				
				<div class='right-col' >".strtoupper( $res->u_father_fname.' '.$res->u_father_mname.' '.$res->u_father_lname )."</div>";
			
			$html .= " <div class='left-col' >Mobile</div>
				
				<div class='right-col' >".$res->reg_mobile."</div>";
			
			$html .= "<div class='left-col' >Email</div>
				
				<div class='right-col' >".strtoupper( $res->reg_email ) ."</div>";		

			if($res->reg_pan)
			{
				$html .= "<div class='left-col' >PAN Number</div>			
				<div class='right-col' >".$res->reg_pan."</div>";
			}
			
			if($res->reg_aadhar)
			{
				$html .= "<div class='left-col' >AADHAR Number</div>			
				<div class='right-col' >".$res->reg_aadhar."</div>";
			}
				
		
			$html .= " <div class='left-col' >Nationality</div>
				
				<div class='right-col' >".strtoupper( $res->u_nationality ) ."</div>";
			
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
	
						
			
			$html .= "<div class='left-col' >Are you Married ?</div>
				
				<div class='right-col' >".$u_is_married."</div>";
			
			if( $res->u_is_married == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Marriage</div>
				
				<div class='right-col' >".db_to_calen( $res->sp_mar_date ) ."</div>";
			
				
				$html .="<div class='left-col' >Spouse Name</div>
				

				<div class='right-col' >".strtoupper( $res->sp_name ) ."</div>";
				
				
				$html .= "<div class='left-col' >Occupation</div>
				
				<div class='right-col' >".strtoupper( $res->sp_occup ) ."</div>";
				
				if( $res->sp_nation == 'f' )
				{
					$sp_nation = 'FOREIGN';
				}
				else
				{
					$sp_nation = 'INDIAN';
				}	
				
				$html .= "<div class='left-col' >Nationality</div>
				
				<div class='right-col' >".$sp_nation ."</div>";
				
				if( $res->sp_citizenship_date != '0000-00-00' )
				{
				
					$html .= "<div class='left-col' >Citizenship acquiring Date(if foreigner)</div>
					
					<div class='right-col' >".db_to_calen( $res->sp_citizenship_date )."</div>";
				
				}
				
				if( $res->sp_ssc_applied == 'y' )
				{
					$sp_ssc_applied = 'YES';
				}
				else
				{
					$sp_ssc_applied = 'NO';
				}	
				
				$html .= "<div class='left-col' >Applied for grant of SSC in AMC</div>
					
					<div class='right-col' >".$sp_ssc_applied."</div>					
					";	
				
			}

			if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIA';
			}
			
			$html .= "<div class='left-col' >Graduated From</div>
					
					<div class='right-col' >".$u_grad_from."</div>";					
					
					$html .= "<div class='left-col' >PMRC Medical Registration Number</div>
					
					<div class='right-col' >".strtoupper( $res->u_pmrc_num )."</div>";
					
					$html .= "<div class='left-col' >Date of PMRC Registration</div>
					
					<div class='right-col' >". db_to_calen( $res->u_pmrc_reg_date ) ."</div>";
					
					
					$html .= "<div class='left-col' >PMRC Issuing Office</div>
					
					<div class='right-col' >". strtoupper( $res->u_pmrc_issue_off ) ."</div>";
					
					
					$html .= "<div class='left-col' >Basic Educational Qualification</div>
					
					<div class='right-col' >".strtoupper( $res->u_basic_qual )."</div>					
					";	
					
				
					$html .= "<div class='left-col' >Date of Admission to MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_adm_date ) ."</div>";
				
					// below line modified by preeti on 25th feb 14, text corrected
				
					$html .= "<div class='left-col' >Date Of Passing MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_passing_date ) ."</div>";
					
					// below lines modified by preeti on 25th feb 14
				
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part I)</div>
					
					<div class='right-col' >". $res->u_num_attempt ."</div>";
					
					$html .= "<div class='left-col' >Certificate (MBBS Part I)</div>
					
					<div class='right-col' >". $res->u_attempt_cert1_no." ".$res->u_attempt_cert1_office." ".db_to_calen($res->u_attempt_cert1_date) ."</div>";
					
					// below lines added by preeti on 25th feb 14
					
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part II)</div>
					
					<div class='right-col' >". $res->u_num_attempt1 ."</div>";
					
					$html .= "<div class='left-col' >Certificate (MBBS Part II)</div>
					
					<div class='right-col' >". $res->u_attempt_cert2_no." ".$res->u_attempt_cert2_office." ".db_to_calen($res->u_attempt_cert2_date) ."</div>";
					
					$mstate=$this->getStateNameFromId($res->medical_state);
					
				
					$html .= "<div class='left-col' >MBBS College/University/State</div>
					
					<div class='right-col' >". strtoupper( $res->u_college.' / '.$res->u_univ.' / '.$mstate ) ."</div>";
					
					
					if( $res->u_intern_complete == 'y' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date ) ;
					
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >YES</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date</div>
					
						<div class='right-col' >".$intern_date."</div>";
						
						
						
							$html .= "<div class='left-col' >Certificate Internship</div>
					
					<div class='right-col' >". $res->intern_ctf_no." ".$res->intern_ctf_office." ".db_to_calen($res->intern_ctf_date) ."</div>";
					
						
						
						
					}
					else if( $res->u_intern_complete == 'n' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date_likely ) ;
						
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >NO</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date (Likely)</div>
					
						<div class='right-col' >".$intern_date."</div>";
					}
					
					if( $res->u_college_recog == 'y' )
					{
						$u_college_recog = 'YES';
					}
					else
					{
						$u_college_recog = 'NO';
					}
			
					
					$html .= "<div class='left-col' >MBBS college Recg. by MCI</div>
					
					<div class='right-col' >". $u_college_recog ."</div>
					
					";
					
					
					
					
					
					
						$html .= "<div class='left-col' >Certificate MBBS</div>
					
					<div class='right-col' >". $res->mbbs_ctf_no." ".$res->mbbs_ctf_office." ".db_to_calen($res->mbbs_ctf_date) ."</div>";
					
					
					
					
					
					
					
					
					
			
			if( $res->u_is_pg == 'y' )
			{
				$u_is_pg = 'YES';
			}
			else
			{
				$u_is_pg = 'NO';
			}
			
			
			$html .= "<div class='left-col' >Are you a Post Graduate ?</div>
					
					<div class='right-col' >". $u_is_pg ."</div> ";
					
			if( $res->u_is_pg == 'y' )
			{
				
				$regno = $res->u_regno; // line added by preeti on 25th feb 14
		
				// get post_grad details
				
				$res_pg = $this->admin_model->get_pg_data( $regno );
				
				$pg_qual = '';
				
				$pg_arr = array();
				
				if( $res_pg )
				{
					if( is_array( $res_pg ) && COUNT( $res_pg ) > 0 )
					{
						// below lines added by preeti on 27th feb 14
							
						$pg_cnt = '';
						
						if(COUNT( $res_pg ) > 1)
						{
							$pg_cnt = 1;
						}
						
						foreach( $res_pg as $val )
						{
							
							$state_id='';
							
							if($pg_cnt=='' || $pg_cnt==1)
							$state_id=$res->pg_medical_state1;
							elseif($pg_cnt==2)
							$state_id=$res->pg_medical_state2;
							elseif($pg_cnt==3)
							$state_id=$res->pg_medical_state3;
							
							if($state_id)
							$pgmstate=$this->getStateNameFromId($state_id); 
							else
							$pgmstate='-'; 
							
							$html .= "<div class='left-col' >PG Qualification$pg_cnt (Subject) </div>
					
							<div class='right-col' >". strtoupper( $val->pg_degree.'('.$val->pg_subject.')') ."</div>
					
							";
					
							$html .= "<div class='left-col' >College / University /State</div>
					
							<div class='right-col' >". strtoupper( $val->pg_college.'/'.$val->pg_univ.'/'.$pgmstate ) ."</div>
					
							";
					
							$html .= "<div class='left-col' >Year Of Passing</div>
					
							<div class='right-col' >". $val->pg_year ."</div>
					
							";
					
					
					if($pg_cnt==1)
					{
					
						$html .= "<div class='left-col' >Certificate PG 1</div>
					
					<div class='right-col' >". $res->firstpg_ctf_no." ".$res->firstpg_ctf_office." ".db_to_calen($res->firstpg_ctf_date) ."</div>";
					
					}
					else if($pg_cnt==2)
					{
					
						$html .= "<div class='left-col' >Certificate PG 2</div>
					
					<div class='right-col' >". $res->secondpg_ctf_no." ".$res->secondpg_ctf_office." ".db_to_calen($res->secondpg_ctf_date) ."</div>";
					
					}
					else if($pg_cnt==3)
					{
						$html .= "<div class='left-col' >Certificate PG 3</div>
					
					<div class='right-col' >". $res->thirdpg_ctf_no." ".$res->thirdpg_ctf_office." ".db_to_calen($res->thirdpg_ctf_date) ."</div>";
					
					
					}
					
					
					
							if( $val->pg_mci_recog == 'y' )
							{
								$pg_mci_recog = 'YES';
							}
							else
							{
								$pg_mci_recog = 'NO';
							}	
					
							$html .= "<div class='left-col' >PG College Recognized by MCI</div>
								
								<div class='right-col' >". $pg_mci_recog ."</div>
								
								";
								
							if( is_numeric( $pg_cnt ) )
							{
								$pg_cnt++;
							}		
								
								
						}
									
									
					}
				}				
							
			}
					
					
					$html .= "<div class='left-col' >Fees </div>
					
					<div class='right-col' >Rs. 200 Only. Txn Id ". $res->txn_id ."  ".db_to_calen($res->txn_date)."</div>
					
					";	
					
					
			/*$html .= "<div class='left-col' >DD Number</div>
					
					<div class='right-col' >". $res->u_dd_num ."</div>
					
					";	
			
			
			$html .= "<div class='left-col' >DD Date</div>
					
					<div class='right-col' >". db_to_calen( $res->u_dd_date )  ."</div>
					
					";	
			
			$html .= "<div class='left-col' >Payable Bank</div>
					
					<div class='right-col' >". strtoupper( $res->u_dd_bank )  ."</div>
					
					";*/
					
			if( $res->u_current_emp == 'y' )
			{
				$u_current_emp = 'YES';
			}
			else
			{
				$u_current_emp = 'NO';
			}		
			
			$html .= "<div class='left-col' >Are you Employed ?</div>
					
					<div class='right-col' >". $u_current_emp  ."</div>
					
					";
					
			if( $res->u_current_emp == 'y'  )
			{
				$html .= "<tr>
				
					<div class='left-col' >Current Employer</div>
					
					<div class='right-col' >".strtoupper( $res->ce_employer )  ."</div>
					
					";
					
					
					
					$html .= "<div class='left-col' >Certificate NOC</div>
					
					<div class='right-col' >". $res->noc_cft_no." ".$res->noc_cft_office." ".db_to_calen($res->noc_cft_date) ."</div>";
					
					
					
					
			}
			
			// below if else added by preeti on 3rd mar 14
			
			if( $res->u_ncc_cert == '' )
			{
				$u_ncc_cert = 'NONE';
			}		
			else
			{
				$u_ncc_cert = strtoupper( $res->u_ncc_cert );
			}
			
			// below code modified by preeti on 3rd mar 14		
			
			$html .= "<div class='left-col' >NCC Training Certificate</div>
					
					<div class='right-col' >". $u_ncc_cert  ."</div>
					
					";
			
			$html .= "<div class='left-col' >Hobbies</div>
					
					<div class='right-col' >". strtoupper( $res->u_hobbies )  ."</div>
					
					";
					
			if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >First Preference of Service</div>
					
					<div class='right-col' >". $u_pref1  ."</div>
					
					";
			
			
			
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Second Preference of Service</div>
					
					<div class='right-col' >". $u_pref2  ."</div>
					
					";
			
			
			if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Third Preference of Service</div>
					
					<div class='right-col' >". $u_pref3  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				$u_previous_ssc = 'YES';
			}
			else
			{
				$u_previous_ssc = 'NO';
			}		
					
			$html .= "<div class='left-col' >Have you worked in AMC service before ? </div>
					
					<div class='right-col' >". $u_previous_ssc  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Commission</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_com_date )  ."</div>
					
					";
				
				$html .= "<div class='left-col' >Date Of Release</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_rel_date )  ."</div>
					
					";
					
					
						$html .= "<div class='left-col' >Certificate Release</div>
					
					<div class='right-col' >". $res->rel_cft_no." ".$res->rel_cft_office." ".db_to_calen($res->rel_cft_date) ."</div>";
					
				
			}			
			
			if( $res->u_previous_unfit == 'y' )
			{
				$unfit_year=$res->u_previous_unfit_year;
				$u_previous_ssc = 'YES in '.$unfit_year;
			}
			else
			{
				$u_previous_ssc = 'NO';
			}		
			
			$icenters=$this->getUserCenters($res->reg_pan,$res->reg_aadhar);
			$html .= "<div class='left-col' >Have you attended AMC SSC interview previously ?</div>
					
					<div class='right-col' >". $u_previous_ssc." ";

			if($icenters)
			$html .= $icenters;
		
			$html .= "</div>";
					
			$_r=$this->admin_model->get_last_date();
			$cicentr=$_r->icenter;
			$curr_icenter=$this->getIcenterInfo($cicentr);
			
			$html .= "<div class='left-col'>Appearing Center</div>
					
					<div class='right-col' >". $curr_icenter  ."</div>
					
					";
			
			$state_post = $this->admin_model->get_state_name( $res->post_state );
			
			$state_perm = $this->admin_model->get_state_name( $res->perm_state );		
									
			
			/*$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. 'Tel(Landline) - '.$res->post_tel) ."</div>
					
					";				
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. 'Tel(Landline) - '. $res->perm_tel ) ."</div>
					 
					";	*/ //lines commented by preeti on 28th feb 14		 
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->post_tel != '' )
			{
				$post_tel = $res->post_tel;	
			}
			else 
			{
				$post_tel = 'NONE';
			}
			
			
			$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. ' Alt. Tel - '.$post_tel) ."</div>
					
					";									
			
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->perm_tel != '' )
			{
				$perm_tel = $res->perm_tel;	
			}
			else 
			{
				$perm_tel = 'NONE';
			}
			
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. ' Alt. Tel - '. $perm_tel ) ."</div>
					
					";	
					
			//$u_sign_url = base_url().'uploads/sign/'.$res->u_sign;
			
			
			
			// above line commented and below lines added by preeti on 13th may 14
		
			if( $where == 'pdf' )
			{
				$u_sign_url = FCPATH.'uploads/sign/'.$res->u_sign;	
			}
			else 
			{
				$u_sign_url = base_url().'uploads/sign/'.$res->u_sign; 	
			}		
				
					
					
			/*$html .= "<div class='left-col' >Declaration</div>
					
					<div class='right-col' >I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.</div>
					
					<div class='image-col' ><img src='".$u_sign_url."' /></div>
					
					";	*/
					
						$html .= "<div class='left-col' >Declaration</div>
					
					<div class='right-col' >I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.</div>
					
					<div class='image-col' ></div>
					
					";		
		
		$html .= "</div>
		
		";
		
		return $html;
	}
















}


?>


