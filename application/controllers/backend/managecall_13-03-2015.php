<?php

class managecall extends CI_Controller
{
public $vjdb;

public function __CONSTRUCT()
{


parent::__CONSTRUCT();

$this->load->model('backend/managecall_model');
$this->load->model('cletter_model');

$this->load->library('vj'); 
$this->vjdb=$this->vj->loadclass('db'); 
//$this->load->helper('vj');
}


public function index($errmsg='')
{

$data=array();
    $data['errmsg'] = urldecode( $errmsg );
	
$data['records']=$this->managecall_model->loadrecords(); 
 
$this->load->view('backend/managecall/default',$data);
}


public function downloadlist($id)
{
 


 $sql="select * from managecall order by `idate` asc";

$rows=$this->vjdb->loadrecords($sql);
$start=0;
$noc=0;
$idate='';
$ct=0;

foreach($rows as $row)
{
	 
	 if($row->id==$id)
   {
$noc=$row->noc;


$idate=$row->idate;

break;	
  }
	 $noc=$row->noc;
	 $idate=$row->idate;
	
	$start=$start+$row->noc;


	
}



$file="list-".$idate.'.pdf';

$this->load->library('dompdf');
$this->load->helper('dompdf');




$html = '
<style>
body {font-family:Arial, Helvetica, sans-serif; font-size:11px;} 
</style>
';
 
$sql="select * from user left join postal on user.u_regno=postal.post_u_regno left join state on postal.post_state=state.state_id where user.u_status ='appr' order by user.appr_dt asc  limit $start,$noc";

  //$sql="select * from user where u_status ='appr'  ";

$query =$this->db->query($sql);
$rows=$user=$query->result();
 

$html.='
<h4>Interview date : '.$idate.'</h4>
<table border="1"  width="100%" style="border-collapse:collapse" border="1">
<tr>
<th>Regno</th>
<th>Name</th>
<th>Address</th>
<th>Remark</th>
</tr>
';

foreach($rows as $user)
{
	$regno=$user->u_regno;
	
	$dob=$user->u_dob;
	$name=$user->u_fname.' '.$user->u_mname.' '.$user->u_lname;
	$state=$user->state_name;
	$pin=$user->post_pin;
$address=$user->post_address.' '.$state.' '.$pin;
	$sql="update user set u_idate='$id' where u_regno='$regno' ";
	
	//$this->vjdb->execute($sql); // line commented by preeti on 28th jul 14
	//$this->cletter_model->setivdate($regno,$idate);

//$html.=$this->cletter_model->getsinglecalll($regno,$dob,$idate);
$html.='
<tr>
<td>'.$regno.'</td><td>'.$name.'</td><td>'.$address.'</td><td></td>
</tr>
';


}

$html.='
</table>
';

//ENDHTML;
 //echo $html;
// $file=$from."-".$to.".pdf";
 
 $res = gendompdf($html,$file);

 //echo " pdf result is ".$res;





}

public function downloadrange($id)
{ 


 $sql="select * from managecall order by `idate` asc";

$rows=$this->vjdb->loadrecords($sql);
$start=0;
$noc=0;
$idate='';
$ct=0;

foreach($rows as $row)
{
	 
	 if($row->id==$id)
   {
$noc=$row->noc;


$idate=$row->idate;

break;	
  }
	 $noc=$row->noc;
	 $idate=$row->idate;
	
	$start=$start+$row->noc;


	
}





/*$sql="set @rownum=-1;
SELECT *
FROM (

SELECT @rownum := @rownum +1 AS rownum,`id`,`noc`
FROM `managecall` order by `idate` asc
) AS
VIEW WHERE `id` ='$id' 
";
*/
 
//$rows=$this->vjdb->loadrecords($sql);
$file="call-letter-".$idate.'.pdf';

/*$sql="select * from managecall where id ='$id' order by idate asc ";
	
//$sql="select * from managecall where id ='$id'  ";
$query =$this->db->query($sql);
$rows=$query->result();
$row=$rows[0];

$from=$row->from;
$to=$row->to;
$noc=$row->noc;
*/
$this->load->library('dompdf');
$this->load->helper('dompdf');




$html = '
<style>
body {font-family:Arial, Helvetica, sans-serif; font-size:13px;} 
</style>
';
 
  $sql="select * from user where u_status ='appr' order by appr_dt asc  limit $start,$noc";
  //$sql="select * from user where u_status ='appr'  ";

$query =$this->db->query($sql);
$rows=$user=$query->result();
 

foreach($rows as $user)
{
	$regno=$user->u_regno;
	
	$dob=$user->u_dob;

	$sql="update user set u_idate='$id' where u_regno='$regno' ";
	
	//$this->vjdb->execute($sql); // line commented by preeti on 28th jul 14
	//$this->cletter_model->setivdate($regno,$idate);

$html.=$this->cletter_model->getsinglecall($regno,$dob,$idate);


}

//ENDHTML;
 //echo $html;
// $file=$from."-".$to.".pdf";
 
 $res = gendompdf($html,$file);

 //echo " pdf result is ".$res;




}


public function deleterange($id)
{

$res=$this->managecall_model->delete($id);

redirect('backend/managecall/index');
}

public function setinterviewdate()
{

//$this->form_validation->set_rules('from','From','required');
//$this->form_validation->set_rules('to','To','required');
$this->form_validation->set_rules('idate','Interview Date','required');
$this->form_validation->set_rules('noc','Number of candidate','required');



if($this->form_validation->run()!=TRUE)
{

$err="All fields are required.";
$er=urlencode($err);
redirect('backend/managecall/index/'.$err);

}
else
{

$from=$this->input->post('from');
$to=$this->input->post('to');
$idate=$this->input->post('idate');
$noc=$this->input->post('noc');
$ds=explode('/',$idate);
$idate=$ds[2].'-'.$ds[1].'-'.$ds[0];

$this->managecall_model->saverecord($from,$to,$idate,$noc);
redirect('backend/managecall/index');


}







}




}


?>


