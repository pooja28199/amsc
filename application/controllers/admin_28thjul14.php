<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '10.26.48.135')
{
	
}
else
{
$ip=$_SERVER['REMOTE_ADDR'];
$time=date('Y-m-d h:i:s');

$file = 'security.txt';
// Open the file to get existing content
$current = file_get_contents($file);
// Append a new person to the file
$current = $ip."  ".$time."\n".$current;
// Write the contents back to the file
file_put_contents($file, $current);
echo "You are trying to breach the security system.Please contact system administrator";
die;

}*/

class Admin extends CI_Controller
{
	public $vjdb;
	
	//code added by Pavan on 31-03-2014
	function stopXSS() 
	{
		// prevent XSS on $_GET, $_POST and $_COOKIE
		foreach ($_GET as $gkey => &$gval) {
		if(is_array($gval)) { // allow for checkboxes!
		foreach ($gval as $gkey2 => &$gval2) {
		$gval2 = htmlspecialchars($gval2);
		}
		} else {
		$gval = htmlspecialchars($gval);
		}
		}
		foreach ($_POST as $pkey => &$pval) {
		if(is_array($pval)) { // allow for checkboxes!
		foreach ($pval as $pkey2 => &$pval2) {
		$pval2 = htmlspecialchars($pval2);
		}
		} else {
		$pval = htmlspecialchars($pval);
		}
		}
		foreach ($_COOKIE as $ckey => &$cval) {
		$cval = htmlspecialchars($cval);
		}
	}
	
	public function __CONSTRUCT()
	{
		parent::__CONSTRUCT();
		
		$this->load->model('admin_model');
		$this->load->model('cletter_model');
		
		// below code added by preeti on 25th mar 14 for black-box testing 
		
		$this->load->helper('my_helper');
    	$this->load->library('vj');
		$this->vjdb=$this->vj->loadclass('db');
    	no_cache();
		
		
		// below code written by preeti on 31st mar 14 for testing
		
		/*echo "<pre>";
		
		print_r( $this->session->all_userdata() );
		
		echo "</pre>";*/
	}
	
	// below function modified by preeti on 5th mar 14
	 
	public function del_all()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		if( $this->input->post('proceed') == 'Proceed'  )// to chcek if it has been come from the proceed button click page only
		{
			// below if added by preeti on 28th apr 14 	
				
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{			
				$errmsg = '';
			
				$res = $this->admin_model->clear_db( );
						
				if( $res )
				{
					$errmsg = "Database cleared successfully";	
				}			
				
				// clear the regnum table as well
						
				$this->admin_model->empty_regnum();
				
				// clear the register table as well
				
				$this->admin_model->empty_register();// added by preeti on 27th feb 14
				
				$data['errmsg']	= $errmsg;
				
				$data['title']	= 'Admin :: Database Clear';  
				
				$this->load->view('admin_msg', $data);
				
			}
		}
		else 
		{
			redirect('admin/clear_pass');	
		}

		redirect('admin/clear_pass'); // line added by preeti on 28th apr 14	
		
	}
		
	// parameter added by preeti on 26th apr 14	
		
	public function index( $errmsg = '' )
	{
		
		if( !($this->check_session()) )
		{
			
			// start, code added by preeti on 21st apr 14
			
			$this->load->helper('captcha'); 
			
			$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
				 );
		  		
		  	$cap = create_captcha($vals);
				
		  		
		  	/*$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		     );*/ 
		     
		    // above code commented and below added by preeti on 24th apr 14 for manual testing
		    
		    $data['captcha_time'] = $cap['time'];
			
			$data['ip_address'] = $this->input->ip_address();
		     	
		    $data['word'] = $cap['word']; 
		     
		  		
		  	$this->session->set_userdata($data);
		  		
		  	$data['cap_img']=$cap['image'];	
			
			// end, code added by preeti on 21st apr 14
			
			
			// added by preeti on 26th apr 14 for manual testing		
			
			if( $errmsg )
			{
				$errmsg = urldecode( $errmsg );			
			}
			
			//$data['errsmg'] = '';
			
			$data['errmsg'] = $errmsg; // above line commented and this added by preeti on 26th apr 14
			
			$data['salt'] = $this->random_num();	// code added by preeti on 28th mar 14 for black-box testing	
			
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
				
			$sess_arr = array(
				
				'admin_salt' => $data['salt'] );
				
			$this->session->set_userdata( $sess_arr );
						
			// end, code added by preeti on 22nd apr 14 for manual testing		
					
			
			
			$this->load->view('admin_login', $data);	
		}
		else 
		{
			$this->home();	
		}			
		
	}
	// below function added by preeti on 3rd mar 14

	public function clear_pass( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		$data['title'] = "Admin::Clear DB Password";
		
		$data['errmsg'] = $errmsg;
		
		$data['salt'] = $this->random_num();
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'admin_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_clear_pass', $data);
		
	}
	
	
	// below function added by preeti on 3rd mar 14

	public function clear_verify_pass()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		
		$this->form_validation->set_rules('clear_pass_encode', 'Password', 'required');// code added for black-box testing by preeti on 26th mar 14
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
			
		
				if( !$this->check_session() )
				{
					redirect('admin/index');
					
					exit;
				}
				
				// verify the password if it is correct then redirect to the clear function 
				
				// else redirect to the same password asking page via clear_pass function
				
				// to let the user type the password again
				
				$res = $this->admin_model->clear_verify_pass();
				
				if( $res )
				{			
					$data['title'] = "Admin::Clear DB";
				
					$data['errmsg'] = "Have you taken a backup of your Database to proceed with clearing of the Database";
				
					// below line added by preeti on 28th apr 14
				
					$data['admin_random'] = $this->session->userdata('admin_random');
					
					$this->load->view('admin_clear', $data);			
				}
				else 
				{
					$errmsg = " Password entered is Incorrect ";
					
					$this->clear_pass( $errmsg );
				}
				
			}
			else
			{
				//below line added by preeti on 21st apr 14 for manual testing
				
				$data['admin_random'] = $this->session->userdata('admin_random');
				
				$data['salt'] = $this->random_num();
				
				// start, code added by preeti on 22nd apr 14 for manual testing
				
				// add the same value in the session variable
								
				$sess_arr = array(
								
					'admin_salt' => $data['salt'] );
								
					$this->session->set_userdata( $sess_arr );
										
				// end, code added by preeti on 22nd apr 14 for manual testing	
				
			
				$this->load->view('admin_clear_pass', $data);		
			}		
		
		}
		else 
		{
			//below line added by preeti on 21st apr 14 for manual testing
				
			$data['admin_random'] = $this->session->userdata('admin_random');
				
			
			$data['salt'] = $this->random_num();
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
							
			$sess_arr = array(
							
				'admin_salt' => $data['salt'] );
							
				$this->session->set_userdata( $sess_arr );
									
			// end, code added by preeti on 22nd apr 14 for manual testing	
			
			
			$this->load->view('admin_clear_pass', $data);		
		}		
		
	}
	
	public function update_db_password()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		// start, code modified for black-box testing by preeti on 26th mar 14
		
		$this->form_validation->set_rules('opassword_encode', 'Old Password', 'required');
		
		$this->form_validation->set_rules('admin_password_encode', 'Password', 'required');
		
		$this->form_validation->set_rules('cpassword_encode', 'Confirm Password', 'required|matches[admin_password_encode]'); 
		
		// end, code modified for black-box testing by preeti on 26th mar 14
		
		$this->form_validation->set_message('matches', 'The two passwords do not match');
		
		if( $this->form_validation->run() == TRUE )
		{
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
				// shoot an update query here
		
				if( $this->admin_model->check_old_db_password() )
				{					
					// check if the new password is the same as the old password
					
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					$old_password = $this -> input -> post('opassword_encode');// hashed md5 value
		
					$new_password = $this -> input -> post('admin_password_encode'); // only md5 value
		
					
					if( !$this->admin_model->check_old_new_pass_same( $old_password, $new_password ) )// end, code added by preeti on 22nd apr 14 for manual testing
					{
					
						$res = $this->admin_model->update_db_password();	
					
						if($res)
						{
							$errmsg = urlencode( "Clear DB Password Updated Successfully " );
							
							$title = urlencode( "Clear DB Password" );
							
							redirect('admin/show_message/Clear DB Password/'.$errmsg);
						}
						else 
						{
							$errmsg = urlencode( "Clear DB Password couldn't be Updated" );
							
							$title = urlencode( "Clear DB Password" );
							
							redirect('admin/show_message/Clear DB Password/'.$errmsg);
						}	
					
					}
					else 
					{
						$errmsg = "New Password cannot be same as the Old Password";
												
						// below code added by preeti on 24th apr 14 for manual testing
						
						$errmsg = urlencode( $errmsg );
						
						redirect('admin/admin_db_password/'.$errmsg);
					}		
					
				}
				else 
				{
					$errmsg = "Old Password is Incorrect";
					
					// below code added by preeti on 24th apr 14 for manual testing
						
					$errmsg = urlencode( $errmsg );
						
					redirect('admin/admin_db_password/'.$errmsg);
				}
			
			}
			else
			{
				$errmsg = "Clear DB Password couldn't be Updated";
				
				$errmsg = urlencode( $errmsg );
							
				$title = urlencode( "Clear DB Password" );
							
				redirect('admin/show_message/Clear DB Password/'.$errmsg);
			}						
			
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
			
			$errmsg = $this->form_validation->error_array();
					
			$errmsg = implode( ' ', $errmsg );
			
			$errmsg = urlencode($errmsg);
			
			redirect('admin/admin_db_password/'.$errmsg);
				
			exit;					
		}		
		
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$data['errmsg'] = $errmsg;	
		
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
		
		$data['salt'] = $this->random_num();
						
		$sess_arr = array(
						
			'admin_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		
		$this->load->view('admin_db_password', $data);
	}

	// below function is added by preeti on 22nd apr 14 for manual testing
	
	public function show_message( $title = '',  $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}	
		
		$data['title'] = urldecode( $title );
		
		$data['errmsg'] = urldecode( $errmsg );
		
		$this->load->view('admin_msg', $data);
	}	
	
	// parameter added by preeti on 24th apr 14 
	
	public function admin_db_password( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		// added by preeti on 24th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );
		
			$data['errmsg'] = $errmsg;
		}	
		else 
		{
			$data['errmsg'] = '';
		}
			
		//$data['errmsg'] = ''; // commented by preeti on 24th apr 14
		
		$data['salt'] = $this->random_num();
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'admin_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_db_password', $data);
	}
	
	
	
	public function get_pdf_html( $res, $where = '' ) // parameter added by preeti on 13th may 14
	{
		//$image_src = base_url()."uploads/photo/".$res->u_photo;
	
		
		// above line commented and below lines added by preeti on 13th may 14
		
		if( $where == 'pdf' )
		{
			$image_src = FCPATH."uploads/photo/".$res->u_photo; 	
		}
		else 
		{
			$image_src = base_url()."uploads/photo/".$res->u_photo; 	
		}
		
		// .image-col property modified by preeti on 25th feb 14
		
		$html = "<style>
		
		.pdf{height:auto;width:100%;font-size:10px;}
		
		.left-col{float:left;width:40%;}
		
		.right-col{float:left;width:40%;}
		
		.image-col{float:right;width:20%;}
		#profimg{position:absolute;margin-left:380px;}
		</style>
		
		<div class='pdf'>
		
		<h3>Form Details</h3>
		
		<div class='left-col' >Registration No.</div>
		
		<div class='right-col' >".$res->reg_regno."</div>
		
		<div id='profimg' class='image-col' ><img src='".$image_src."' /></div>
		
		
		<div class='left-col' >Registration Date</div>
		
		<div class='right-col' >".db_to_calen( $res->u_updatedon )."</div>";
		
		if( $res->u_is_name_change == 'y' )
		{
			$html .= "<div class='left-col' >Name</div>
				
			<div class='right-col' >".strtoupper( $res->new_fname.' '.$res->new_mname.' '.$res->new_lname ) ."</div>";		
		}
		else 
		{
			$html .= "<div class='left-col' >Name (as in Matric Certificate)</div>
		
			<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";
		}
		
		$html .= "<div class='left-col' >Name (in Hindi)</div>
		
		<div class='right-col' >".$res->u_hindi_name."</div>
		
		
		
		<div class='left-col' >Name Changed</div>
				
		<div class='right-col' >".( ($res->u_is_name_change == 'y')?'YES':'NO' )."</div>";
				
			
		if( $res->u_is_name_change == 'y' )
		{
				
				$html .= "<div class='left-col' >Old Name (as in Matric Certificate)</div>
				
				<div class='right-col' >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</div>";		
				
		}
			
			if( $res->u_sex == 'm' )
			{
				$gender = 'MALE';
			}
			else
			{
				$gender = 'FEMALE';
			} 
			
			$html .= "<div class='left-col' >Gender</div>
				
				<div class='right-col' >".$gender."</div>";	
			
			$u_dob = $res->u_dob;
						
			
			$html .= " <div class='left-col' >Date Of Birth</div>
				
				<div class='right-col' >".db_to_calen( $u_dob ) ."</div>";	
			
			$html .= " <div class='left-col' >Age (As on 31st Dec)</div>
				
				<div class='right-col' >".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</div>";
			
			
			// below lines added by preeti on 19th feb 14
			
			$html .= " <div class='left-col' >Father's Name</div>
				
				<div class='right-col' >".strtoupper( $res->u_father_fname.' '.$res->u_father_mname.' '.$res->u_father_lname )."</div>";
			
			$html .= " <div class='left-col' >Mobile</div>
				
				<div class='right-col' >".$res->reg_mobile."</div>";
			
			$html .= "<div class='left-col' >Email</div>
				
				<div class='right-col' >".strtoupper( $res->reg_email ) ."</div>";			
		
			$html .= " <div class='left-col' >Nationality</div>
				
				<div class='right-col' >".strtoupper( $res->u_nationality ) ."</div>";
			
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
	
						
			
			$html .= "<div class='left-col' >Are you Married ?</div>
				
				<div class='right-col' >".$u_is_married."</div>";
			
			if( $res->u_is_married == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Marriage</div>
				
				<div class='right-col' >".db_to_calen( $res->sp_mar_date ) ."</div>";
			
				
				$html .="<div class='left-col' >Spouse Name</div>
				
				<div class='right-col' >".strtoupper( $res->sp_name ) ."</div>";
				
				
				$html .= "<div class='left-col' >Occupation</div>
				
				<div class='right-col' >".strtoupper( $res->sp_occup ) ."</div>";
				
				if( $res->sp_nation == 'f' )
				{
					$sp_nation = 'FOREIGN';
				}
				else
				{
					$sp_nation = 'INDIAN';
				}	
				
				$html .= "<div class='left-col' >Nationality</div>
				
				<div class='right-col' >".$sp_nation ."</div>";
				
				if( $res->sp_citizenship_date != '0000-00-00' )
				{
				
					$html .= "<div class='left-col' >Citizenship acquiring Date(if foreigner)</div>
					
					<div class='right-col' >".db_to_calen( $res->sp_citizenship_date )."</div>";
				
				}
				
				if( $res->sp_ssc_applied == 'y' )
				{
					$sp_ssc_applied = 'YES';
				}
				else
				{
					$sp_ssc_applied = 'NO';
				}	
				
				$html .= "<div class='left-col' >Applied for grant of SSC in AMC</div>
					
					<div class='right-col' >".$sp_ssc_applied."</div>					
					";	
				
			}

			if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIA';
			}
			
			$html .= "<div class='left-col' >Graduated From</div>
					
					<div class='right-col' >".$u_grad_from."</div>";					
					
					$html .= "<div class='left-col' >PMRC Medical Registration Number</div>
					
					<div class='right-col' >".strtoupper( $res->u_pmrc_num )."</div>";
					
					$html .= "<div class='left-col' >Date of PMRC Registration</div>
					
					<div class='right-col' >". db_to_calen( $res->u_pmrc_reg_date ) ."</div>";
					
					
					$html .= "<div class='left-col' >PMRC Issuing Office</div>
					
					<div class='right-col' >". strtoupper( $res->u_pmrc_issue_off ) ."</div>";
					
					
					$html .= "<div class='left-col' >Basic Educational Qualification</div>
					
					<div class='right-col' >".strtoupper( $res->u_basic_qual )."</div>					
					";	
					
				
					$html .= "<div class='left-col' >Date of Admission to MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_adm_date ) ."</div>";
				
					// below line modified by preeti on 25th feb 14, text corrected
				
					$html .= "<div class='left-col' >Date Of Passing MBBS</div>
					
					<div class='right-col' >". db_to_calen( $res->u_passing_date ) ."</div>";
					
					// below lines modified by preeti on 25th feb 14
				
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part I)</div>
					
					<div class='right-col' >". $res->u_num_attempt ."</div>";
					
					// below lines added by preeti on 25th feb 14
					
					$html .= "<div class='left-col' >No. Of Attempts (MBBS Part II)</div>
					
					<div class='right-col' >". $res->u_num_attempt1 ."</div>";
				
					$html .= "<div class='left-col' >MBBS College/University</div>
					
					<div class='right-col' >". strtoupper( $res->u_college.' / '.$res->u_univ ) ."</div>";
					
					
					if( $res->u_intern_complete == 'y' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date ) ;
					
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >YES</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date</div>
					
						<div class='right-col' >".$intern_date."</div>";
					}
					else if( $res->u_intern_complete == 'n' )
					{
						$intern_date = 	db_to_calen( $res->u_intern_date_likely ) ;
						
						$html .= "<div class='left-col' >Internship Completed</div>
					
						<div class='right-col' >NO</div>";
						
						$html .= "<div class='left-col' >Internship Completion Date (Likely)</div>
					
						<div class='right-col' >".$intern_date."</div>";
					}
					
					if( $res->u_college_recog == 'y' )
					{
						$u_college_recog = 'YES';
					}
					else
					{
						$u_college_recog = 'NO';
					}
			
					
					$html .= "<div class='left-col' >MBBS college Recg. by MCI</div>
					
					<div class='right-col' >". $u_college_recog ."</div>
					
					";
			
			if( $res->u_is_pg == 'y' )
			{
				$u_is_pg = 'YES';
			}
			else
			{
				$u_is_pg = 'NO';
			}
			
			
			$html .= "<div class='left-col' >Are you a Post Graduate ?</div>
					
					<div class='right-col' >". $u_is_pg ."</div> ";
					
			if( $res->u_is_pg == 'y' )
			{
				
				$regno = $res->u_regno; // line added by preeti on 25th feb 14
		
				// get post_grad details
				
				$res_pg = $this->admin_model->get_pg_data( $regno );
				
				$pg_qual = '';
				
				$pg_arr = array();
				
				if( $res_pg )
				{
					if( is_array( $res_pg ) && COUNT( $res_pg ) > 0 )
					{
						// below lines added by preeti on 27th feb 14
							
						$pg_cnt = '';
						
						if(COUNT( $res_pg ) > 1)
						{
							$pg_cnt = 1;
						}
						
						foreach( $res_pg as $val )
						{
							$html .= "<div class='left-col' >PG Qualification$pg_cnt (Subject) </div>
					
							<div class='right-col' >". strtoupper( $val->pg_degree.'('.$val->pg_subject.')') ."</div>
					
							";
					
							$html .= "<div class='left-col' >College / University </div>
					
							<div class='right-col' >". strtoupper( $val->pg_college.'/'.$val->pg_univ ) ."</div>
					
							";
					
							$html .= "<div class='left-col' >Year Of Passing</div>
					
							<div class='right-col' >". $val->pg_year ."</div>
					
							";
					
							if( $val->pg_mci_recog == 'y' )
							{
								$pg_mci_recog = 'YES';
							}
							else
							{
								$pg_mci_recog = 'NO';
							}	
					
							$html .= "<div class='left-col' >PG College Recognized by MCI</div>
								
								<div class='right-col' >". $pg_mci_recog ."</div>
								
								";
								
							if( is_numeric( $pg_cnt ) )
							{
								$pg_cnt++;
							}		
								
								
						}
									
									
					}
				}				
							
			}
					
			$html .= "<div class='left-col' >DD Number</div>
					
					<div class='right-col' >". $res->u_dd_num ."</div>
					
					";	
			
			
			$html .= "<div class='left-col' >DD Date</div>
					
					<div class='right-col' >". db_to_calen( $res->u_dd_date )  ."</div>
					
					";	
			
			$html .= "<div class='left-col' >Payable Bank</div>
					
					<div class='right-col' >". strtoupper( $res->u_dd_bank )  ."</div>
					
					";
					
			if( $res->u_current_emp == 'y' )
			{
				$u_current_emp = 'YES';
			}
			else
			{
				$u_current_emp = 'NO';
			}		
			
			$html .= "<div class='left-col' >Are you Employed ?</div>
					
					<div class='right-col' >". $u_current_emp  ."</div>
					
					";
					
			if( $res->u_current_emp == 'y'  )
			{
				$html .= "<tr>
				
					<div class='left-col' >Current Employer</div>
					
					<div class='right-col' >".strtoupper( $res->ce_employer )  ."</div>
					
					";
			}
			
			// below if else added by preeti on 3rd mar 14
			
			if( $res->u_ncc_cert == '' )
			{
				$u_ncc_cert = 'NONE';
			}		
			else
			{
				$u_ncc_cert = strtoupper( $res->u_ncc_cert );
			}
			
			// below code modified by preeti on 3rd mar 14		
			
			$html .= "<div class='left-col' >NCC Training Certificate</div>
					
					<div class='right-col' >". $u_ncc_cert  ."</div>
					
					";
			
			$html .= "<div class='left-col' >Hobbies</div>
					
					<div class='right-col' >". strtoupper( $res->u_hobbies )  ."</div>
					
					";
					
			if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >First Preference of Service</div>
					
					<div class='right-col' >". $u_pref1  ."</div>
					
					";
			
			
			
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Second Preference of Service</div>
					
					<div class='right-col' >". $u_pref2  ."</div>
					
					";
			
			
			if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
			
			$html .= "<div class='left-col' >Third Preference of Service</div>
					
					<div class='right-col' >". $u_pref3  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				$u_previous_ssc = 'YES';
			}
			else
			{
				$u_previous_ssc = 'NO';
			}		
					
			$html .= "<div class='left-col' >Have you worked in AMC service before ? </div>
					
					<div class='right-col' >". $u_previous_ssc  ."</div>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				
				$html .= "<div class='left-col' >Date Of Commission</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_com_date )  ."</div>
					
					";
				
				$html .= "<div class='left-col' >Date Of Release</div>
					
					<div class='right-col' >". db_to_calen( $res->ps_rel_date )  ."</div>
					
					";
				
			}			
			
			$state_post = $this->admin_model->get_state_name( $res->post_state );
			
			$state_perm = $this->admin_model->get_state_name( $res->perm_state );		
									
			
			/*$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. 'Tel(Landline) - '.$res->post_tel) ."</div>
					
					";				
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. 'Tel(Landline) - '. $res->perm_tel ) ."</div>
					 
					";	*/ //lines commented by preeti on 28th feb 14		 
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->post_tel != '' )
			{
				$post_tel = $res->post_tel;	
			}
			else 
			{
				$post_tel = 'NONE';
			}
			
			
			$html .= "<div class='left-col' >Address (Postal)</div>
					
					<div class='right-col' >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. ' Alt. Tel - '.$post_tel) ."</div>
					
					";									
			
			
			// below lines added and modified by preeti on 11th mar 14
			
			if( $res->perm_tel != '' )
			{
				$perm_tel = $res->perm_tel;	
			}
			else 
			{
				$perm_tel = 'NONE';
			}
			
			
			
			$html .= "<div class='left-col' >Address (Permanent)</div>
					
					<div class='right-col' >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. ' Alt. Tel - '. $perm_tel ) ."</div>
					
					";	
					
			//$u_sign_url = base_url().'uploads/sign/'.$res->u_sign;
			
			
			
			// above line commented and below lines added by preeti on 13th may 14
		
			if( $where == 'pdf' )
			{
				$u_sign_url = FCPATH.'uploads/sign/'.$res->u_sign;	
			}
			else 
			{
				$u_sign_url = base_url().'uploads/sign/'.$res->u_sign; 	
			}		
				
					
					
			$html .= "<div class='left-col' >Declaration</div>
					
					<div class='right-col' >I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.</div>
					
					<div class='image-col' ><img src='".$u_sign_url."' /></div>
					
					";		
		
		$html .= "</div>
		
		";
		
		return $html;
	}

	// below code added by preeti on 24th apr 14
	 	
	 public function check_captcha()
	 {
	  	
		  $expiration = time()-7200; // Two hour limit
		  
		  $cap=$this->input->post('captcha');
		  
		  if( strtoupper( $this->session->userdata('word') ) == strtoupper( $cap ) 
		  
		   AND $this->session->userdata('ip_address')== $this->input->ip_address()
		  
		   AND $this->session->userdata('captcha_time')> $expiration)
		  {
		  		  
		   	return true;
		  
		  }
		  
		  else
		  {  
			   $this->form_validation->set_message('check_captcha', 'Security number does not match.');
			  
			   return false;
		  }
	 
	 }
	
	
	// function modified by preeti on 26th mar 14 for black-box testing
				
	public function validate()
	{
		$this->form_validation->set_rules('admin_username', 'Username', 'required|alpha_dash');
		
		$this->form_validation->set_rules('admin_password_encode', 'Password', 'required');
		
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_check_captcha');
		
		if( $this->form_validation->run() == TRUE )
		{
			$result = $this->admin_model->validate();
		
			
			if($result)
			{
				// first set the session variables and then	
					
				// go to the home page
				
				$admin_username = $this->input->post('admin_username');
				
				$admin_id = $this->admin_model->get_admin_id( $admin_username );
				
				
				/*$data = array(
				
				'admin_id' => $admin_id,
				
				'username' => $admin_username,
				
				'is_logged_in' => TRUE, 
				
				);*/ // line commented by preeti on 31st mar 14 for black-box testing
				
				
				
				// start, code added by preeti on 31st mar 14 for black-box testing
		
				$sessid = '';
				
				while (strlen($sessid) < 32)
				{
					$sessid .= mt_rand(0, mt_getrandmax());
				}	
				
				// To make the session ID even more secure we'll combine it with the user's IP
				
				$sessid .= $this->input->ip_address();
		
				$session_id = md5(uniqid($sessid, TRUE)) ; 
				
				
				$data = array(
				
				'admin_id' => $admin_id,
				
				'username' => $admin_username,
				
				'is_logged_in' => TRUE,
				
				'admin_random' => $this->random_num(), // line added by preeti on 21st apr 14 for manual testing  
				
				'session_id' => $session_id,
				
				);		
					
				
				// end, code added by preeti on 31st mar 14 for black-box testing			
				
			
				$this->session->set_userdata( $data );
				
				// below line added by preeti on 26th apr 14
				
				$this->admin_model->add_login( );
			
				redirect('admin/home');
			}
			else 
			{
				
				// start, code added by preeti on 21st apr 14 for manual testing
			
				$this->load->helper('captcha'); 
				
				$vals = array(
				     
				     'img_path' => './captcha/',
				     
				     'img_url' => base_url().'captcha/'
				     
				 );
			  		
			  	$cap = create_captcha($vals);
					
			  		
			  	$data = array(
			     	
			     	'captcha_time' => $cap['time'],
			     	
			     	'ip_address' => $this->input->ip_address(),
			     	
			     	'word' => $cap['word']
			    );
			  		
			  	$this->session->set_userdata($data);
			  		
			  	$data['cap_img']=$cap['image'];	
				
				// end code added by preeti on 21st apr 14 for manual testing
		
				
				// below line added by preeti on 26th apr 14
				
				$this->admin_model->add_logfail( $this->input->post('admin_username') );
				
				
				// show the error message	
			
				$data['errmsg'] = 'Login Credentials are Incorrect';
				
				$data['salt'] = $this->random_num();	// code added by preeti on 22nd apr 14 for manual testing	
				
				// start, code added by preeti on 22nd apr 14 for manual testing
				
				// add the same value in the session variable
				
				$sess_arr = array(
				
				'admin_salt' => $data['salt'] );
				
				$this->session->set_userdata( $sess_arr );
						
				// end, code added by preeti on 22nd apr 14 for manual testing		
						
				$this->load->view('admin_login', $data);
			}			
		
		}
		/*else 
		{
			// start, code added by preeti on 21st apr 14 for manual testing
			
			$this->load->helper('captcha'); 
			
			$vals = array(
			     
			     'img_path' => './captcha/',
			     
			     'img_url' => base_url().'captcha/'
			     
			 );
		  		
		  	$cap = create_captcha($vals);
				
		  		
		  	$data = array(
		     	
		     	'captcha_time' => $cap['time'],
		     	
		     	'ip_address' => $this->input->ip_address(),
		     	
		     	'word' => $cap['word']
		    );
		  		
		  	$this->session->set_userdata($data);
		  		
		  	$data['cap_img']=$cap['image'];	
			
			// end code added by preeti on 21st apr 14 for manual testing
			
			
			$data['salt'] = $this->random_num();	// code added by preeti on 22nd apr 14 for manual testing
			
			
			// start, code added by preeti on 22nd apr 14 for manual testing
				
			// add the same value in the session variable
				
			$sess_arr = array(
				
				'admin_salt' => $data['salt'] );
				
			$this->session->set_userdata( $sess_arr );
						
			// end, code added by preeti on 22nd apr 14 for manual testing		
						
			
			$this->load->view( 'admin_login', $data );
		}*/ 	
		else if( validation_errors( ) )// above code commented and below added by preeti on 26th apr 14 
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('admin/index/'.$errmsg);
						
			exit;					
		}
			
	}
//  added by pavan 22-04-14
public function doc_list11( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list11'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list11'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_numb();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14 
				
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result34( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		//print_r($data);
		$this->load->view('admin_audittrail', $data);
		
		
	} 
	
	public function home()
	{
		if( $this->check_session() )
		{
			$data = array(
		
			'username' => $this->session->userdata('username'),
			
			'is_logged_in' => $this->session->userdata('is_logged_in'),
			
			);
			
			
			$sql="select * from register where reg_status='i'";
			
			
			$xyz=$this->vjdb->numrows($sql);
			$data['pending']=$xyz;
			
			$sql="select * from register where reg_status='c'";
			$xyz=$this->vjdb->numrows($sql);
		    $data['confirm']=$xyz;
				
			$sql="select * from register";
			$xyz=$this->vjdb->numrows($sql);
		    $data['total']=$xyz;
				
						$today=date('Y-m-d');
			$sql="select * from register where reg_createdon like '$today%'";
			$xyz=$this->vjdb->numrows($sql);
		    $data['today_total']=$xyz;
			
		
			$sql="select * from register where reg_createdon like '$today%' and reg_status='i'";
			$xyz=$this->vjdb->numrows($sql);
		    $data['today_pending']=$xyz;
		
		
	 		$sql="select * from register where reg_createdon like '$today%' and reg_status='c'";
		
			$xyz=$this->vjdb->numrows($sql);
		    $data['today_confirmed']=$xyz;
			
				
			$this->load->view('admin_home', $data);	
		}
		else 
		{
			//$this->index();
			
			redirect('admin/index');
		}			
		
	}
	
	public function check_session()
	{
		if(  $this->session->userdata('username') != ''  && $this->session->userdata('is_logged_in') == TRUE )
		{
			return TRUE;
		}
		else 
		{
			return FALSE;	
		}
	}
	
	/*public function doc_list( )// function modified by preeti on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
		}
		
		if( $keyword != '' )
		{
			$keyword = urlencode( $keyword ); // to be decoded at the time of DB query fetching 
			
			$url .= "/keyword/".$keyword;
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		if( $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		if( $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		if( $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		if( $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		if( $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}
		
		if( $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		$url .= "/offset/";

		$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		$config['num_links'] = 20;
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list', $data);
		
	} */  // code commented on 27th mar 14 by preeti for black-box testing
	
	
	// below function added by preeti on 10th apr 14
	
	function only_num($str) // for numeric
    {
        return ( ! preg_match("/^([0-9])+$/i", $str))? FALSE : TRUE;    
	}
	
	
	
	function alpha_num_dash($str) // for alpha-numeric with space, dash, underscore, forward slash
    {
        return ( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $str))? FALSE : TRUE;    
	}
	
	
	// below function added by preeti on 12th apr 14
	
	function date_validate($str) // date format
    {    	
		if( ! preg_match("/^([-0-9\/])+$/i", $str) )
		{
		   	$this->form_validation->set_message('date_validate', 'The %s field should only contain date.');	
					
		   	return FALSE;
		}
		else 
		{
			return TRUE;	
		}          
	}
	
	
	public function doc_list( )// function modified by preeti on 27th mar 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_num_at_space( $keyword ) )
			{
				$keyword = '';
			}
		}
		
		if( $keyword != '' )
		{
			$keyword = urlencode( $keyword ); // to be decoded at the time of DB query fetching 
			
			$url .= "/keyword/".$keyword;
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		
		if( $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		if( $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		if( $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		if( $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		if( $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}

		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}

		
		if( $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		$sort_by = 'reg_updatedon';
		
		$sort_order = 'DESC';
		
			
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		// start below code added by preeti on 11th apr 14 for black-box testing
		
		if( $offset )
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}		
		
		
		
		if( !$this->alpha_and_space(  $sort_by ) )
		{
			$sort_by = 'u_id';
		}
		
		if( !$this->alpha_and_space( $sort_order ) )
		{
			$sort_order = 'DESC';
		}
		
		// end below code added by preeti on 11th apr 14 for black-box testing
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
		
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$data['admin_random'] = $this->session->userdata('admin_random');

		$this->load->view('admin_doc_list', $data);
		
	} 
	
	
	
	public function doc_list1( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
		}
		
		if( $keyword != '' )
		{
			$keyword = urlencode( $keyword ); // to be decoded at the time of DB query fetching 
			
			$url .= "/keyword/".$keyword;
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		if( $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		if( $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		if( $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		if( $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		if( $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}
		
		if( $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list1'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list1'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_doc_list1', $data);
		
		
	} 
	public function doc_list2( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) ) // line modified by preeti on 9th apr 14 for black-box testing
			{
				$keyword = '';
			}
		}
		
		if( $keyword != '' )
		{
			$keyword = urlencode( $keyword ); // to be decoded at the time of DB query fetching 
			
			$url .= "/keyword/".$keyword;
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		
		
		if( $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		if( $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		if( $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		if( $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		if( $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}


		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}

		
		if( $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list2'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list2'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_appnum();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'appr_dt';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result2( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list2', $data);
		
	} 

	
	public function doc_list21( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$from = '';
		
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		else if( isset( $uri_arr['from'] ) &&  $uri_arr['from'] != '' )
		{
			$from = $uri_arr['from'];
		}
		$to = '';
		
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
		else if( isset( $uri_arr['to'] ) &&  $uri_arr['to'] != '' )
		{
			$to = $uri_arr['to'];
		}
		
				
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list21'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list21'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_appnum();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['from'] = $from;
		
		$data['to'] = $to;
		
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result2( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list21', $data);
		
	} 

	public function doc_list3( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2; 
		
		
		
		// add all the search parameters if they exist in the uri
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) )
			{
				$keyword = '';
			}
		}
		
		if( $keyword != '' )
		{
			$keyword = urlencode( $keyword ); // to be decoded at the time of DB query fetching 
			
			$url .= "/keyword/".$keyword;
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		if( $gender != '' )
		{
			$url .= "/gender/".$gender;
		} 
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		if( $state != '' )
		{
			$url .= "/state/".$state;
		}  
		
		
		 
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		if( $start_date != '' )
		{
			$url .= "/start_date/".$start_date;
		}   
		 
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		if( $end_date != '' )
		{
			$url .= "/end_date/".$end_date;
		}   
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		if( $start_age != '' )
		{
			$url .= "/start_age/".$start_age;
		}   
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		if( $end_age != '' )
		{
			$url .= "/end_age/".$end_age;
		}
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list3'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list3'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_rejnum();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		
		// pass all the parameters to the view file 
		
		$data['keyword'] = $keyword;
		
		$data['gender'] = $gender;
		
		$data['state'] = $state;
		
		$data['start_date'] = $start_date;
		
		$data['end_date'] = $end_date;
		
		$data['start_age'] = $start_age;
		
		$data['end_age'] = $end_age;
		
				
		$data['sort_by'] = $sort_by ; // line added by preeti on 27th feb 14
		
		$data['sort_order'] = $sort_order ;	// line added by preeti on 27th feb 14	
		
		
		
		$data['per_page'] = $config['per_page'];
		
		// BELOW LINE COMMENTED BY PREETI ON 26TH FEB 14 , TO INCLUDE SEARCH IN THE SAME FUNCTION
		
		//$data['records'] = $this->admin_model->get_user_limit( $config['per_page'], $offset , $sort_by, $sort_order );
		
		$data['records'] = $this->admin_model->get_search_result3( $config['per_page'], $offset , $sort_by, $sort_order );
		
				
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list3', $data);
	} 

	/*public function doc_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$reg_id = $this->uri->segment(3);
		
		$res = $this->admin_model->del_record( $reg_id );
		
		redirect('admin/doc_list');
		
	}*/
	public function file_pdf1( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 $decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list');//added code Pavan 11-4-14
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decodede;
			$s=array();
	  	$s=explode("-",$decodede);
		//print_r($s);
	  		if($s[0]!='')
	  		$decoded=$s[0];
	  		if($s[1]!='')
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   $this->admin_model->delete_detail();
	   $data1 = $this->admin_model->get_reg_ids();
	   $i=1;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->u_regno;
	  	 
	  	 //echo $i;
		 //$res = $this->admin_model->get_detail4($a1);
		 $res21=array();$res22=array();
		 	if(is_array($res21)){
					unset($res21);
						$res21=array();
					}
						if(is_array($res22)){
					unset($res22);
							$res22=array();
					}
				$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
					 $res21=array();
					 $pg_degree="";$pg_subject="";
					 //echo $doc_id;
		 $res21 = $this->admin_model->get_detail4($a1);
		 //print_r($res21);
		    
			$cnt=count($res21);
			//echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
				
				$pg_degree=$t;
			}
			else if($cnt==2){
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
								
				
			   $t3=$res21[2];
			   $pg_degree=$t.",".$t2.",".$t3;
			}
		//$ae1=$t2;
		
		/* if($t!=''&&$t2!=''&&$t3!='')
				$pg_degree=$t.",".$t2.",".$t3;
				else if($t!=''&&$t2==''&&$t3!='')
				$pg_degree=$t.",".$t3;
				else if($t!=''&&$t2!=''&&$t3=='')
				$pg_degree=$t.",".$t2;
				else if($t!=''&&$t2==''&&$t3=='')
				$pg_degree=$t;
			*/	
				//$pg_degree=$t.",".$t2.",".$t3;
				//echo $pg_degree;
			
			$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		 $cnt2=count($res22);
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
				
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }
	   $this->load->library('mpdf');
	  	   $data = $this->admin_model->get_search_result1($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "<div>";
	   $html.="<table border='1'><tr><th>SNo.</th>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th></tr>";
foreach ($data as $res) {
	$b=$res->u_photo;
	$doc_id=$res->u_regno;
	
	
	
	//echo $doc_id;
				 //$image_src = base_url().'/uploads/photo/'.$b;
				 
				  $image_src = FCPATH.'uploads/photo/'.$b; // above line commented and this line added by preeti on 15th may 14
	
				 
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
						$u_is_pg2="";
					$u_is_pg2=strtoupper($res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				
				if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
					//echo $name;
					//exit();
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}
				
$html.="<tr style='height:150px;'><td>".$i."</td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$name."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
</tr>";
$i++;

}
$html.="</table></div>";
	   
			   //echo $html;
        $this->mpdf->setTitle('Posts');
		$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    $this->mpdf->writeHTML($html);
        $this->mpdf->output($pdfFilePath, 'F');
	 $this->download_zip24();
	    //$this->_gen_pdf7($html,'A3');
	    
	}
	
	public function file_pdf5( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 
 		$decoded='';
 		
 		$decoded1='';
 		
 		$decoded2='';
 		
 		$decoded3='';
 		
 		$decoded4='';
 		
 		$decoded5='';
 		
 		$decoded6='';
	    
	    if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   	{
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list2');//added code Pavan 11-4-14
	   	}
		else 
		{
			$decodede='';
	  	
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
		
			$s=array();
	  		
	  		$s=explode("-",$decodede);
	  		
			if($s[0]!='')
	  		
	  		$decoded=$s[0];
	  		
			if($s[1]!='')
	  		
	  		$decoded1=$s[1];
	  		
	  		if($s[2]!=0)
	  		
	  		$decoded2=$s[2];
	  		
	  		if($s[3]!='0000.00.00')
	  		
	  		$decoded3=$s[3];
	  		
	  		if($s[4]!='0000.00.00')
	  		
	  		$decoded4=$s[4];
	  		
	  		if($s[5]!=0)
	  		
	  		$decoded5=$s[5];
	  		
	  		if($s[6]!=0)
	  		
	  		$decoded6=$s[6];
	  					
		}
		  
		//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->admin_model->delete_detail();
	   
	   $data1 = $this->admin_model->get_reg_ids();
	   
	   $i=1;
	  	
	   foreach ($data1 as $res1) 
	   {
	   	
		   	$a1=$res1->u_regno;
		  	
		  	//echo $i;
			//$res = $this->admin_model->get_detail4($a1);
			
			$res21=array();$res22=array();
			
			if(is_array($res21))
			{
				unset($res21);
			
				$res21=array();
			}
			
			if(is_array($res22))
			{
				unset($res22);
			
				$res22=array();
			}
			
			$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
			
			$res21=array();
			
			$pg_degree="";
			
			$pg_subject="";
			
			//echo $doc_id;
			
			$res21 = $this->admin_model->get_detail4($a1);
			
			//print_r($res21);
			    
			$cnt=count($res21);
			
			//echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
					
				$pg_degree=$t;
			}
			else if($cnt==2)
			{
				$t=$res21[0];
					
				$t2=$res21[1];
				
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
			   $t=$res21[0];
					
			   $t2=$res21[1];
									
			   $t3=$res21[2];
			
			   $pg_degree=$t.",".$t2.",".$t3;
			}
			//$ae1=$t2;
			
			/* if($t!=''&&$t2!=''&&$t3!='')
					$pg_degree=$t.",".$t2.",".$t3;
					else if($t!=''&&$t2==''&&$t3!='')
					$pg_degree=$t.",".$t3;
					else if($t!=''&&$t2!=''&&$t3=='')
					$pg_degree=$t.",".$t2;
					else if($t!=''&&$t2==''&&$t3=='')
					$pg_degree=$t;
				*/	
					//$pg_degree=$t.",".$t2.",".$t3;
					//echo $pg_degree;
				
			$res22=array();
			
			$res22 = $this->admin_model->get_detail5($a1);
			
			 //print_r($res22);
			$cnt2=count($res22);
			
			$t5=$t6=$t7="";
			
			if($cnt2==1)
			{
				$t1=$res22[0];
					
				$t5=$t."(".$t1.")";
					
			}
			else if($cnt2==2)
			{
				$t1=$res22[0];
					
				$t5=$t."(".$t1.")";
					
			    $t21=$res22[1];
					//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
					
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				$t1=$res22[0];
					
				$t5=$t."(".$t1.")";
					
				$t21=$res22[1];
					//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
					
				$t6=$t5.",".$t6;
				
				$t31=$res22[2];
				
				$t7=$t3."(".$t31.")";
					
				$t7=$t7.",".$t6;
					
				   
			}
			if($t31!="")
			{
				$pg_subject=$t7;
			}
			else if($t21!="")
			{
				$pg_subject=$t6;
			}
			else 
			{
				$pg_subject=$t5;
			}
				
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }
	   
	  $this->load->library('mpdf');
	  
	  $data = $this->admin_model->get_search_result121($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 
	  $html = "<div>";$i=1;
	  
	  $html.="<table border='1'><tr><th>SNo</th>
                         <th>Registration No.</th>
	<th>Photo</th>
	<th>Name</th>
	<th>Father Name</th>
	<th>DOB</th> 
	<th>AGE</th>
	<th>GENDER</th>
	<th>NATIONALITY</th>
	<th>COLLEGE</th>
	<th>MBBS/PG</th>
	<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
	<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
	<th>MARRIED</th>
	<th>NATLITY. OF SPOUSE</th>
	<th>PERMANENT MEDICAL REGNO.</th>
	<th>FOREIGN MEDICAL GRADUATE</th>
	<th>DDNO., DATE</th>
	<th>CH.OFSERVICE PREF1</th>
	<th>CH.OFSERVICE PREF2</th>
	<th>CH.OFSERVICE PREF3</th></tr>";

	foreach ($data as $res) 
	{
		$b=$res->u_photo;
		
		$doc_id=$res->u_regno;
		//echo $doc_id;
		
		//$image_src = base_url().'/uploads/photo/'.$b;
				 
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 16th may 14
	
	
		if( $res->u_pref1 == 'arm' )
		{
				$u_pref1 = 'ARMY';
		}
		else if( $res->u_pref1 == 'air' )
		{
				$u_pref1 = 'AIR FORCE';
		}		
		else if( $res->u_pref1 == 'nav' )
		{
				$u_pref1 = 'NAVY';
		}
		if( $res->u_pref2 == 'arm' )
		{
				$u_pref2 = 'ARMY';
		}
		else if( $res->u_pref2 == 'air' )
		{
				$u_pref2 = 'AIR FORCE';
		}		
		else if( $res->u_pref2 == 'nav' )
		{
				$u_pref2 = 'NAVY';
		}
		if( $res->u_pref3 == 'arm' )
		{
				$u_pref3 = 'ARMY';
		}
		else if( $res->u_pref3 == 'air' )
		{
				$u_pref3 = 'AIR FORCE';
		}		
		else if( $res->u_pref3 == 'nav' )
		{
				$u_pref3 = 'NAVY';
		}
		if( $res->u_college_recog == 'y' )
		{
					$u_college_recog = 'YES';
		}
		else
		{
					$u_college_recog= 'NO';
		}	
		if( $res->u_is_pg == 'y' )
		{
			$u_is_pg1='MBBS,';
			
			$u_is_pg2="";
			
			$u_is_pg2=strtoupper($res->pg_subject);
			
			$u_is_pg=$u_is_pg1.$u_is_pg2;							
				
		}	
		else 
		{
			$u_is_pg='MBBS';
		}

		if( $res->u_grad_from == 'f' )
		{
			$u_grad_from = 'FOREIGN';
		}
		else
		{
				$u_grad_from = 'INDIAN';
		}
		if($res->u_is_married == 'y')
		{
				$u_is_married = 'YES';
		}
		else
		{
				$u_is_married = 'NO';		
		}
		if( $res->sp_nation == 'f' )
		{
				$sp_nation = 'FOREIGN';
		}
		else
		{
				$sp_nation = 'INDIAN';
		}
		 	
		if( $res->u_sex == 'F' )
		{
					$u_sex = 'FEMALE';
		}
		else
		{
					$u_sex= 'MALE';
		}	
		if( $res->u_is_name_change == 'y' )
		{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
		}
		else 
		{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
		}
								
		$html.="<tr style='height:150px;'><td>".$i."</td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
		
		<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
		
		<td><font style='text-transform: uppercase;'>".$name." </font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
		
		<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
		
		</tr>";
		
		$i++;
		}
		
		$html.="</table></div>";
	   
		//echo $html;
        
        $this->mpdf->setTitle('Posts');
		
		$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    
	    $this->mpdf->writeHTML($html);
        
        $this->mpdf->output($pdfFilePath, 'F');
		
		$this->download_zip24();
	    //$this->_gen_pdf7($html,'A3');
	    
	}

	public function file_pdf6( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 $decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list2');//added code Pavan 11-4-14
	   		}
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  			if($s[0]!='')
	  		$decoded=$s[0];
	  			if($s[1]!='')
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   $this->admin_model->delete_detail();
	   $data1 = $this->admin_model->get_reg_ids();
	   $i=1;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->u_regno;
	  	 
	  	 //echo $i;
		 //$res = $this->admin_model->get_detail4($a1);
		 $res21=array();$res22=array();
		 	if(is_array($res21)){
					unset($res21);
						$res21=array();
					}
						if(is_array($res22)){
					unset($res22);
							$res22=array();
					}
				$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
					 $res21=array();
					 $pg_degree="";$pg_subject="";
					 //echo $doc_id;
		 $res21 = $this->admin_model->get_detail4($a1);
		 //print_r($res21);
		    
			$cnt=count($res21);
			//echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
				
				$pg_degree=$t;
			}
			else if($cnt==2){
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
								
				
			   $t3=$res21[2];
			   $pg_degree=$t.",".$t2.",".$t3;
			}
		//$ae1=$t2;
		
		/* if($t!=''&&$t2!=''&&$t3!='')
				$pg_degree=$t.",".$t2.",".$t3;
				else if($t!=''&&$t2==''&&$t3!='')
				$pg_degree=$t.",".$t3;
				else if($t!=''&&$t2!=''&&$t3=='')
				$pg_degree=$t.",".$t2;
				else if($t!=''&&$t2==''&&$t3=='')
				$pg_degree=$t;
			*/	
				//$pg_degree=$t.",".$t2.",".$t3;
				//echo $pg_degree;
			
			$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		 $cnt2=count($res22);
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
				
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }
	   $this->load->library('mpdf');
	  	   $data = $this->admin_model->get_search_result121($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "<div>";$i=1;
	   $html.="<table border='1'><tr><th>SNo</th>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th></tr>";
foreach ($data as $res) {
	$b=$res->u_photo;
	$doc_id=$res->u_regno;
	//echo $doc_id;
				//$image_src = base_url().'/uploads/photo/'.$b;
				 
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 16th may 14
	
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
						$u_is_pg2="";
					$u_is_pg2=strtoupper($res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}
				
$html.="<tr style='height:150px;'><td>".$i."</td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$name."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
</tr>";
$i++;
}
$html.="</table></div>";


	   //echo $html;
		 $this->_gen_pdf37($html,'A3');
		 //$this->_gen_pdf7($html,'A3');
	    
	}
public function file_pdf7( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 $decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list3');//added code Pavan 11-4-14
	   		}
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  			if($s[0]!='')
	  		$decoded=$s[0];
	  			if($s[1]!='')
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   $this->admin_model->delete_detail();
	   $data1 = $this->admin_model->get_reg_ids();
	   $i=1;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->u_regno;
	  	 
	  	 //echo $i;
		 //$res = $this->admin_model->get_detail4($a1);
		 $res21=array();$res22=array();
		 	if(is_array($res21)){
					unset($res21);
						$res21=array();
					}
						if(is_array($res22)){
					unset($res22);
							$res22=array();
					}
				$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
					 $res21=array();
					 $pg_degree="";$pg_subject="";
					 //echo $doc_id;
		 $res21 = $this->admin_model->get_detail4($a1);
		 //print_r($res21);
		    
			$cnt=count($res21);
			//echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
				
				$pg_degree=$t;
			}
			else if($cnt==2){
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
								
				
			   $t3=$res21[2];
			   $pg_degree=$t.",".$t2.",".$t3;
			}
		//$ae1=$t2;
		
		/* if($t!=''&&$t2!=''&&$t3!='')
				$pg_degree=$t.",".$t2.",".$t3;
				else if($t!=''&&$t2==''&&$t3!='')
				$pg_degree=$t.",".$t3;
				else if($t!=''&&$t2!=''&&$t3=='')
				$pg_degree=$t.",".$t2;
				else if($t!=''&&$t2==''&&$t3=='')
				$pg_degree=$t;
			*/	
				//$pg_degree=$t.",".$t2.",".$t3;
				//echo $pg_degree;
			
			$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		$cnt2=count($res22);
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
				
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }
	   $this->load->library('mpdf');
	   $i=1;
	  	   $data = $this->admin_model->get_search_result122($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
		   $html ="<style>
		.pdf{width: 300px;white-space:normal;}
       
		</style>
		
		<div>";
		
	  
	   $html.="<table border='1'><tr><th>SNo</th>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th>
<th>REJECT REASON</th>
</tr>";
foreach ($data as $res) {
	$b=$res->u_photo;
	$doc_id=$res->u_regno;
	//echo $doc_id;
				 //$image_src = base_url().'/uploads/photo/'.$b;
				 
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 16th may 14
	
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
						$u_is_pg2="";
					$u_is_pg2=strtoupper($res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}
				
$html.="<tr style='height:150px;'><td>".$i."</td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$name."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
<td style='width:300px;' ><font style='text-transform: uppercase;'><div class='pdf'>".$res->u_reason."</div></font></td>
</tr>";
$i++;
}
$html.="</table></div>";
	   
			   //echo $html;
        $this->mpdf->setTitle('Posts');
		$pdfFilePath = FCPATH."uploads/form/REJECTED.pdf";
	    $this->mpdf->writeHTML($html);
        $this->mpdf->output($pdfFilePath, 'F');
		 $this->download_zip23();
	    //$this->_gen_pdf7($html,'A3');
	    
	}
public function file_pdf8( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 $decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	    if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list3');//added code Pavan 11-4-14
	   		}
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  			if($s[0]!='')
	  		$decoded=$s[0];
	  			if($s[1]!='')
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   $this->admin_model->delete_detail();
	   $data1 = $this->admin_model->get_reg_ids();
	   $i=1;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->u_regno;
	  	 
	  	 //echo $i;
		 //$res = $this->admin_model->get_detail4($a1);
		 $res21=array();$res22=array();
		 	if(is_array($res21)){
					unset($res21);
						$res21=array();
					}
						if(is_array($res22)){
					unset($res22);
							$res22=array();
					}
				$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
					 $res21=array();
					 $pg_degree="";$pg_subject="";
					 //echo $doc_id;
		 $res21 = $this->admin_model->get_detail4($a1);
		 //print_r($res21);
		    
			$cnt=count($res21);
			//echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
				
				$pg_degree=$t;
			}
			else if($cnt==2){
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
								
				
			   $t3=$res21[2];
			   $pg_degree=$t.",".$t2.",".$t3;
			}
		//$ae1=$t2;
		
		/* if($t!=''&&$t2!=''&&$t3!='')
				$pg_degree=$t.",".$t2.",".$t3;
				else if($t!=''&&$t2==''&&$t3!='')
				$pg_degree=$t.",".$t3;
				else if($t!=''&&$t2!=''&&$t3=='')
				$pg_degree=$t.",".$t2;
				else if($t!=''&&$t2==''&&$t3=='')
				$pg_degree=$t;
			*/	
				//$pg_degree=$t.",".$t2.",".$t3;
				//echo $pg_degree;
			
			$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		$cnt2=count($res22);
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
				
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }
	   $this->load->library('mpdf');
	   $i=1;
	  	   $data = $this->admin_model->get_search_result122($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	    $html ="<style>
		.pdf{width: 300px;white-space:normal;}
       
		</style>
		
		<div>";
		
	   $html.="<table border='1'><tr><th>SNo</th>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th>
<th>REJECT REASON</th>
</tr>";
foreach ($data as $res) {
	$b=$res->u_photo;
	$doc_id=$res->u_regno;
	//echo $doc_id;
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 26th may 14
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
						$u_is_pg2="";
					$u_is_pg2=strtoupper($res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}
				
$html.="<tr style='height:150px;'><td>".$i."</td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$name."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
<td style='width:300px;' ><font style='text-transform: uppercase;'><div class='pdf'>".$res->u_reason."</div></font></td>
</tr>";
$i++;
}
$html.="</table></div>";
	   
			   //echo $html;
       
	    $this->_gen_pdf37($html,'A3');
	    
	}

	public function file_pdf10( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
 	     $this->load->library('zip');
	   $this->load->library('mpdf');
	   $data1 = $this->admin_model->get_reg_id();
	  foreach ($data1 as $res) {
	  	 $a1=$res->reg_regno;
		 $data = $this->admin_model->get_search_result13($a1);
	   $html = "<div>";
	   $html.="<table border='1'><tr>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th></tr>";
foreach ($data as $res) {
	$b=$res->photo;
				 $image_src = base_url().'/uploads/photo/'.$b;
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
					$u_is_pg2=strtoupper( $res->pg_degree.'('.$res->pg_subject.')');
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				
$html.="<tr style='height:150px;'>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_fname."&nbsp;".$res->u_mname."&nbsp;".$res->u_lname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
</tr>";
}
$html.="</table></div>";
	   
	    foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
	  
	  
		$this->zip->archive('D:\zip111.zip');
		
        $this->mpdf->setTitle('Posts');
		$a2=$a1.".pdf";
		$pdfFilePath = FCPATH."uploads/result/".$a2;
	    $this->mpdf->writeHTML($html);
        $this->mpdf->output($pdfFilePath, 'F');
		 $this->download_zip23();
	    //$this->_gen_pdf7($html,'A3');
	    
	}
	}
public function file_pdf42( $decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6 )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			 
			redirect('admin/index');
			
			exit;
		}
	 
		$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		
	   }
		else 
		{
	  		$decoded = $this->uri->segment(3);
	  		
		}
		 if($this->uri->segment(4) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded1 = $this->uri->segment(4);
	  		
		}
		 if($this->uri->segment(5) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded2 = $this->uri->segment(5);
	  		
		}
		 if($this->uri->segment(6) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded3 = $this->uri->segment(6);
	  		
		}
		 if($this->uri->segment(7) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded4 = $this->uri->segment(7);
	  		
		}
		 if($this->uri->segment(8) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded5 = $this->uri->segment(8);
	  		
		}
		 if($this->uri->segment(9) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   }
		else 
		{
	  		$decoded6 = $this->uri->segment(9);
	  		
		}
		 
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->load->library('mpdf');
	   $data = $this->admin_model->get_search_result1($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "<div>";
	   $html.="<table border='1'><tr>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>MBBS/PG</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th></tr>";
foreach ($data as $res) {
	$b=$res->u_photo;
				 $image_src = base_url().'/uploads/photo/'.$b;
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
					$u_is_pg2=strtoupper( $res->pg_degree.'('.$res->pg_subject.')');
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				
$html.="<tr style='height:150px;'>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_fname."&nbsp;".$res->u_mname."&nbsp;".$res->u_lname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."&nbsp;".$res->u_father_lname."&nbsp;".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</font></td>
<td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$u_pref3."</font></td>
</tr>";
}
$html.="</table></div>";
	   
		   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	     $this->_gen_pdf7($html,'A3');
	    
	}

public function file_pdf( $test='')
	{
		
		//print_r($s);
		
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			 
			redirect('admin/index');
			
			exit;
		}
	 
		$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == ''||$this->uri->segment(3) == 'test')//added code Pavan 11-4-14
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		redirect('admin/doc_list');//added code Pavan 11-4-14
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
	  		 
			//echo $decoded;
			$s=array();
			
	  	$s=explode("-",$decodede);
		//print_r($s);
		
	  		if($s[0]!=''||$s[0]!='')
	  		$decoded=$s[0];
				//echo $decoded;}
				//echo $s[1];
	  		if($s[1]!='')
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   $this->admin_model->delete_detail();
	   $data1 = $this->admin_model->get_reg_ids();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->u_regno;
	  	 $i++;
	  	 //echo $i;
		 //$res = $this->admin_model->get_detail4($a1);
		 $res21=array();$res22=array();
		 	if(is_array($res21)){
					unset($res21);
						$res21=array();
					}
						if(is_array($res22)){
					unset($res22);
							$res22=array();
					}
				$t="";$t2="";$t3="";$t1="";$t21="";$t31="";
					 $res21=array();
					 $pg_degree="";$pg_subject="";
					 //echo $doc_id;
		 $res21 = $this->admin_model->get_detail4($a1);
		 //print_r($res21);
		    
			$cnt=count($res21);
		//	echo $cnt;
			if($cnt==1)
			{
				$t=$res21[0];
				
				$pg_degree=$t;
			}
			else if($cnt==2){
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
				$pg_degree=$t.",".$t2;
			}
			else if($cnt==3)
			{
				 
		       $t=$res21[0];
				
			   $t2=$res21[1];
								
				
			   $t3=$res21[2];
			   $pg_degree=$t.",".$t2.",".$t3;
			}
		//$ae1=$t2;
		
		/* if($t!=''&&$t2!=''&&$t3!='')
				$pg_degree=$t.",".$t2.",".$t3;
				else if($t!=''&&$t2==''&&$t3!='')
				$pg_degree=$t.",".$t3;
				else if($t!=''&&$t2!=''&&$t3=='')
				$pg_degree=$t.",".$t2;
				else if($t!=''&&$t2==''&&$t3=='')
				$pg_degree=$t;
			*/	
				//$pg_degree=$t.",".$t2.",".$t3;
			//	echo $pg_degree;
			
			$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		 $cnt2=count($res22);
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
			//echo $pg_subject;
				/*if($t1!=''&&$t21!=''&&$t31!='')
				$pg_subject=$t1.",".$t21.",".$t31;
				else if($t1!=''&&$t21==''&&$t31!='')
				$pg_subject=$t1.",".$t31;
				else if($t1!=''&&$t21!=''&&$t31=='')
				$pg_subject=$t1.",".$t21;
				else if($t1!=''&&$t21==''&&$t31=='')
				$pg_subject=$t1;
				*/
			//echo $pg_subject;
			
			$res23 = $this->admin_model->insert_detail($a1,$pg_degree,$pg_subject);
	  }

	   //$this->load->library('mpdf');
	   $this->load->library('dompdf');
	   $this->load->helper('dompdf');
	   
	   $i=1;
	  // echo $decoded;
	  	   $data = $this->admin_model->get_search_result1($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "
	 
	 ";
	   $html.="<table border='1' width='100%' style='border-collapse:collapse;'><tr>
	   
	   
	   <th width='5'>SNo.</th>
                         <th width='24'>REGNO</th>

<th>Name</th>

<th width='40'>DOB</th>
<th  width='60'>AGE</th>
<th  width='3'>M/F</th>
<th  width='50'>NLTY</th>

<th>MBBS/PG</th>
<th  width='25'>ATMPTS</th>
<th  width='25'>MCI<br />RECG</th>

<th>NLTY<br />SPOUSE</th>
<th>PMRC</th>
<th  width='50'>GRAD<br />FROM</th>
<th  width='70'>DDNO., DATE</th>
<th  width='60'>INTERN</th>
<th  width='3'>NCC</a>
</tr>";
$i=0;
$lastreg='';
$firstreg='';
foreach ($data as $res) {
	$b=$res->u_photo;
	
	$doc_id=$res->u_regno;
	$i++;
	if($i==1)
	$firstreg=$doc_id;
	$lastreg=$doc_id;
	
	//echo $doc_id;
				 //$image_src = base_url().'/uploads/photo/'.$b;
				 
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 16th may 14
	
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
						$u_is_pg2="";
					$u_is_pg2=strtoupper($res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			
			$sp_nation="NA";
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
				
				if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
			}
			else
			{
				$u_is_married = 'NO';		
			}
			
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'f';
				}
				else
				{
					$u_sex= 'm';
				}	
					if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
					//echo $name;
					//exit();
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}
				$dob=$res->u_dob;
				$dobs=explode('-',$dob);
				$dob=$dobs[2].'-'.$dobs[1].'-'.$dobs[0];
				$intern=$res->u_intern_complete;
				$dts=explode('-',$res->u_intern_date);
				$dt=$dts[2].'-'.$dts[1].'-'.$dts[0];
				
				$internstring='Y-'.$dt;	
				
				if($intern!='y')
				{
				$dts=explode('-',$res->u_intern_date_likely);
				$dt=$dts[2].'-'.$dts[1].'-'.$dts[0];
				
				$internstring='N-'.$dt;	
				
				}
				
			
$html.="<tr >
<td>".$i."</td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>

<td align='center'><font style='text-transform: uppercase;'>".$name."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$dob."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_age_year.' y,'.$res->u_age_month.' m, '.$res->u_age_day.' d '."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$u_sex."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>

<td align='center'><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_num_attempt."/".$res->u_num_attempt1."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$u_college_recog."</font></td>

<td align='center'><font style='text-transform: uppercase;'>".$sp_nation."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$u_grad_from."</font></td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_dd_num.",".$res->u_dd_date."</font></td>
<td align='center'><font style='text-transform: uppercase;'></font>".$internstring."</td>
<td align='center'><font style='text-transform: uppercase;'>".$res->u_ncc_cert."</font></td>

</tr>";
$i++;
}
$html.="</table>";
	   
		   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	    
		
		// $this->_gen_pdf7($html,'A3');
		$file="userlist-".$firstreg."-".$lastreg.".pdf";
	    gendompdf($html,$file,'landscape','A3'); 
	}
		
	public function file_pdf14( $test='')
	{
			 
		
		//print_r($s);
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
		$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  		if($s[0]!=0)
	  		$decoded=$s[0];
	  		if($s[1]!=0)
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->load->library('mpdf');
	   $data = $this->admin_model->get_search_result122($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "<div>";
	   $html.="<table border='1'><tr><td align='center'><h2>SSC INTERVIEW</h2></td></tr>
	   <tr><td align='center'>ARMY MEDICAL CORPS</td></tr>
	   <tr><td align='left'><h4>DATE OF INTERVIEW</h4></td><td align='right'><h4>BOARD NO.</h4></td><td border='1' style='width:80px;'></td> </tr>
	   <tr>
<th>SNo.</th>
<th>Photo</th>
	   	   
                         <th>Registration No.</th>
<th>Name</th>
<th>GENDER</th>
<th>MBBS/PG</th>
<th>NCC</th>
<th>VIVA MARKS</th>
<th>TOTAL</th>
<th>REMARKS</th>
<th>REASON FOR REJECTION</th>
</tr>";
$i=1;
foreach ($data as $res) {
	$b=$res->u_photo;
				 $image_src = base_url().'/uploads/photo/'.$b;
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
					$u_is_pg2=strtoupper( $res->pg_degree.'('.$res->pg_subject.')');
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				
$html.="<tr style='height:150px;'>
<td>".$i."</td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
 
 
<td><font style='text-transform: uppercase;'>".$res->u_fname."&nbsp;".$res->u_mname."&nbsp;".$res->u_lname."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td></td><td></td><td></td><td style='width:180px;></td><td></td>
</tr>";
}
$html.="</table></div>";
	   
		   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	     $this->_gen_pdf127($html,'A3');
	    
	}

public function file_pdf4( $test='')
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
		//print_r($s);
		
		$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
	  	$s=explode("-",$decodede);
	  		if($s[0]!=0)
	  		$decoded=$s[0];
	  		if($s[1]!=0)
	  		$decoded1=$s[1];
	  	 	
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->load->library('mpdf');
	   $data = $this->admin_model->get_search_result1211($decoded,$decoded1);
	 $html = "<div>";
	   $html.="<table><tr><td><table align='center'><tr><td></td><td></td><td></td><td align='center'><h2>SSC INTERVIEW</h2></td>
	   <td></td><td></td><td></td><td></td></tr>
	   <tr><td></td><td></td><td></td><td align='center'><h2>ARMY MEDICAL CORPS</h2></td><td></td><td></td><td></td><td></td></tr></table></td></tr>
	   <tr><td><table><tr><td width='150'><h4>DATE OF INTERVIEW</h4></td><td>__________</td><td></td><td></td><td></td><td width='420'></td><td  width='100' align='right'><h4>BOARD NO.</h4></td><td>________</td></tr></table></td></tr>
	   <tr><td><table border='1'><tr>
<th>SNo.</th>
<th>Photo</th>
	   	   
                         <th>Registration No.</th>
<th>Name</th>
<th>GENDER</th>
<th>MBBS/PG</th>
<th>NCC</th>
<th>VIVA MARKS</th>
<th>TOTAL</th>
<th>REMARKS</th>
</tr>";
$i=1;
foreach ($data as $res) {
	$b=$res->u_photo;
	$a1=$res->u_regno;
	$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		 $cnt2=count($res22);
	$cnt2=count($res22);
	
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
		
				  //$image_src = base_url().'/uploads/photo/'.$b;
				 
				 $image_src = FCPATH.'/uploads/photo/'.$b; // above line commented and this line added by pavan on 16th may 14
	
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
					$u_is_pg2=strtoupper( $res->pg_subject);
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				if( $res->u_is_name_change == 'y' )
				{
					$name = $res->new_fname.' '.$res->new_mname.' '.$res->new_lname;
				}
				else 
				{
					$name = $res->u_fname.' '.$res->u_mname.' '.$res->u_lname;
				}

$html.="<tr style='height:150px;'>
<td>".$i."</td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
 
 
<td><font style='text-transform: uppercase;'>".$name."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td></td><td></td><td></td><td width='160'></td>
</tr>";
$i++;
}
$html.="</table></td></tr></table></div>";
	   
		   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	     $this->_gen_pdf17($html,'A3');
	    
	}

public function file_pdf432( $test='')
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
		//print_r($s);
		
		$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  		if($s[0]!=0)
	  		$decoded=$s[0];
	  		if($s[1]!=0)
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->load->library('mpdf');
	   $data = $this->admin_model->get_search_result122($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	 $html = "<div>";
	   $html.="<table><tr><td><table align='center'><tr><td></td><td></td><td></td><td align='center'><h2>SSC INTERVIEW</h2></td>
	   <td></td><td></td><td></td><td></td></tr>
	   <tr><td></td><td></td><td></td><td align='center'>ARMY MEDICAL CORPS</td><td></td><td></td><td></td><td></td></tr></table></td></tr>
	   <tr><td><table><tr><td width='150'><h4>DATE OF INTERVIEW</h4></td><td>__________</td><td></td><td></td><td></td><td width='420'></td><td  width='100' align='right'><h4>BOARD NO.</h4></td><td>________</td></tr></table></td></tr>
	   <tr><td><table border='1'><tr>
<th>SNo.</th>
<th>Photo</th>
	   	   
                         <th>Registration No.</th>
<th>Name</th>
<th>GENDER</th>
<th>MBBS/PG</th>
<th>NCC</th>
<th>VIVA MARKS</th>
<th>TOTAL</th>
<th>Reason of reject</th>
</tr>";
$i=1;
foreach ($data as $res) {
	$b=$res->u_photo;
				 $image_src = base_url().'/uploads/photo/'.$b;
	if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
	if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
		if( $res->u_college_recog == 'y' )
				{
					$u_college_recog = 'YES';
				}
				else
				{
					$u_college_recog= 'NO';
				}	
	
			if( $res->u_is_pg == 'y' )
			{
					
					$u_is_pg1='MBBS,';
					$u_is_pg2=strtoupper( $res->pg_degree.'('.$res->pg_subject.')');
					$u_is_pg=$u_is_pg1.$u_is_pg2;
					
															
				
			}	
else {
	$u_is_pg='MBBS';
}

if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
			if( $res->sp_nation == 'f' )
			{
				$sp_nation = 'FOREIGN';
			}
			else
			{
				$sp_nation = 'INDIAN';
			}
		 	
	if( $res->u_sex == 'F' )
				{
					$u_sex = 'FEMALE';
				}
				else
				{
					$u_sex= 'MALE';
				}	
				
$html.="<tr style='height:150px;'>
<td>".$i."</td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
 
 
<td><font style='text-transform: uppercase;'>".$res->u_fname."&nbsp;".$res->u_mname."&nbsp;".$res->u_lname."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_sex."</font></td>
 <td><font style='text-transform: uppercase;'>".$u_is_pg."</font></td>
<td></td><td></td><td></td><td width='160'>".$u_reason."</td>
</tr>";
$i++;
}
$html.="</table></td></tr></table></div>";
	   
		   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	     $this->_gen_pdf17($html,'A3');
	    
	}

	public function file_pdf3( $test = '' )
	{
		if( !$this->check_session() )//added session code Pavan 09-4-14
		{
			redirect('admin/index');
			
			exit;
		}
	   if( $test != '' )
	   {
	   		$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   }
		else 
		{
	  		$decoded = '';
		}	   	 
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $this->load->library('mpdf');
	   $data = $this->admin_model->get_search_result1($decoded );
	   $html = "<div>";
	   $html.="<table border='1'><tr>
                         <th>Registration No.</th>
<th>Photo</th>
<th>Name</th>
<th>Father Name</th>
<th>DOB</th>
<th>AGE</th>
<th>GENDER</th>
<th>NATIONALITY</th>
<th>COLLEGE</th>
<th>EDU QUAL(MBBS/PG)</th>
<th>NO. OF ATTEMPTS FOR PASSING MBBS</th>
<th>MEDICAL COLLEGE RECOGNIZED BY MCI</th>
<th>MARRIED</th>
<th>NATLITY. OF SPOUSE</th>
<th>PERMANENT MEDICAL REGNO.</th>
<th>FOREIGN MEDICAL GRADUATE</th>
<th>DDNO., DATE</th>
<th>CH.OFSERVICE PREF1</th>
<th>CH.OFSERVICE PREF2</th>
<th>CH.OFSERVICE PREF3</th></tr>";
foreach ($data as $res) {
	$b=$res->photo;
	$a1=$res->u_regno;
	$res22=array();
		 $res22 = $this->admin_model->get_detail5($a1);
		 //print_r($res22);
		 $cnt2=count($res22);
	$cnt2=count($res22);
	
		 $t5=$t6=$t7="";
		if($cnt2==1)
			{
				$t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
			}
			else if($cnt2==2){
				 
		    $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			}
			else if($cnt2==3)
			{
				 
		       $t1=$res22[0];
				
				$t5=$t."(".$t1.")";
				
				
			   $t21=$res22[1];
				//$t6=$t2."-".$t21;
				$t6=$t2."(".$t21.")";
				
				
				$t6=$t5.",".$t6;
			   $t31=$res22[2];
			   $t7=$t3."(".$t31.")";
				
				$t7=$t7.",".$t6;
				
			   
			}
			if($t31!=""){
				$pg_subject=$t7;
			}
			
			else if($t21!=""){
				$pg_subject=$t6;
			}
			
			else {
				$pg_subject=$t5;
			}
	
				 $image_src = base_url().'/uploads/photo/'.$b;
	
$html.="<tr style='height:150px;'>
<td><font style='text-transform: uppercase;'>".$res->u_regno."</font></td>
<td><img src='".$image_src."' alt='Photo' width='50' height='50'  /></td>
<td><font style='text-transform: uppercase;'>".$res->u_fname."".$res->u_mname."".$res->u_lname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_father_fname."".$res->u_father_lname."".$res->u_father_mname."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dob."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_age_year."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_sex."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_nationality."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_num_attempt."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college_recog."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_college."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_is_married."</font></td>
<td><font style='text-transform: uppercase;'>".$res->nation."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pmrc_num."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_num."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_dd_date."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pref1."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pref2."</font></td>
<td><font style='text-transform: uppercase;'>".$res->u_pref3."</font></td>
</tr>";
}
$html.="</table></div>";
	   
	   
        //$this->mpdf->setTitle('Posts');
		//$pdfFilePath = FCPATH."uploads/form/list2.pdf";
	    //$this->mpdf->writeHTML($html);
       // $this->mpdf->output($pdfFilePath, 'F');
		// $this->download_zip23();
	     $this->_gen_pdf7($html,'A3');
	    
	}
private function _gen_pdf($html,$paper='A4')
    {
       $this->load->library('mpdf/mpdf');                
        
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
        $pdfFilePath = FCPATH."uploads/form/form.pdf";
        
	 //$html = $this->load->view('doc_print2', $data, true);
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
        //d$mpdf->Output('profile.pdf', 'D');
     $this->download_zip25();
    }
    
		private function _gen_pdf7($html,$paper='A3')
    {
        $this->load->library('mpdf/mpdf');                
        $pdfFilePath = FCPATH."uploads/form/list1.pdf";
       // $mpdf=new mPDF('hi',$paper,0,'',15,15,16,16,9,9,'L');
        
        
        $mpdf=new mPDF('hi',$paper,0,'',15,15,16,16,9,9,'L');
        $mpdf->SetJS('this.print();');
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();
		
    }
	private function _gen_pdf37($html,$paper='A3')
    {
        $this->load->library('mpdf/mpdf');                
        $pdfFilePath = FCPATH."uploads/form/rejected.pdf";
        //$mpdf=new mPDF('hi',$paper,0,'',15,15,16,16,9,9,'L');
        
		$mpdf=new mPDF('hi',$paper,0,'',15,15,16,16,9,9,'L');
        $mpdf->SetJS('this.print();');
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();
		
        //$mpdf->WriteHTML($html);
        
        //$mpdf->Output($pdfFilePath, 'F');
		
		//$this->load->view('doc_print25', $data);
    }
	
	private function _gen_pdf17($html,$paper='A3')
    {
        $this->load->library('mpdf/mpdf');     
		//$this->load->helper('PDF_AutoPrint');
		
		 
		 //$pdfFilePath = FCPATH."uploads/result/APPROVED.pdf"; 
		//$mpdf = new PDF_AutoPrint();
		/*$mpdf = new PDF_AutoPrint();
// $mpdf->Open();
//$mpdf->AddPage();
		
$file = file_get_contents($pdfFilePath);
$mpdf->WriteHTML($file,4);
//$mpdf->WriteHTML($message,2);
//$mpdf->Text($pdfFilePath);
//$mpdf->AutoPrint(true);

$mpdf->Output();
		  */         
        $pdfFilePath = FCPATH."uploads/form/APPROVED.pdf";
        $mpdf=new mPDF('hi',$paper,0,'',15,15,16,16,9,9,'L');
        $mpdf->SetJS('this.print();');
        $mpdf->WriteHTML($html);
        
        $mpdf->Output();
		
		//$this->load->view('doc_print12', $data);
    }
    
    //CODE CHANGED BY PAVAN 13-02-13 END_PDF
	//CODE CHANGED BY PAVAN 13-02-13
		public function doprint()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		  $regno = $this->uri->segment(3);   
		  		    
		 //$doc_id = $this->session->userdata('regno');
		 //echo $doc_id;
			//exit();
		 	   
		// $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 // $a = base_url().'\\images\\'.$res->photo;
//			  echo $a;

					//$regno = $this->session->userdata('regno');   
		    
		 	$res = $this->admin_model->get_user_detail( $regno );   
		 //echo $res->photo;
		 
		 $html = $this->get_pdf_html( $res, 'pdf' ); // second parameter added by preeti on 13th may 14
		 	  				    
		$this->_gen_pdf($html,'A4');                  
    }
 
 	/*public function submit()
    {
 		if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
	if($this->input->post('sbm') == "Print all Docs") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download Zip") {
			$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint34/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Print all AppDocs") {
		$res_id = $this->input->post('Typr');
		//echo $res_id;
		redirect('admin/doprinta5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download AppZip") {
			$res_id = $this->input->post('Typr');
		//echo $res_id;
		redirect('admin/doprinta34/'.$res_id);
		
		}
	if($this->input->post('sbm') == "Print all RejDocs") {
		$res_id = $this->input->post('Typo');
		//echo $res_id;
		redirect('admin/doprintr5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download RejZip") {
			$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprintr34/'.$res_id);
		
		}
	
		// below block is modified by preeti on 3rd apr 14
		
		if($this->input->post('sbm') == "Approve") 
		{
			$reg_id_arr = $this->input->post('doc_ids');
			
			$res_id_arr = $this->input->post('reason_ids');			
			
			if( COUNT( $reg_id_arr ) > 0 )
			{
				$res = $this->admin_model->update_statusapp( $reg_id_arr,$res_id_arr );	
				
				
				// start, below code added by preeti on 3rd apr 14
				
				// add a code to send email and smss to all the approved users
				
				$obj_arr = $this->admin_model->get_all_email($reg_id_arr);
				
				$email_arr = array();
				
				$mob_arr = array();  
				
				foreach( $obj_arr as $obj )
				{
					$email_arr[] = $obj->reg_email;
					
					$mob_arr[] = $obj->reg_mobile;  
				}
				
				$config = array(
				
				'protocol' => 'smtp',
										
				'smtp_host' => 'relay.nic.in',
										
				'smtp_port' => 25,
										
				'mailtype' => 'html',
					        
				'newline' => '\r\n',
					        
				'charset' => 'utf-8' //default charset				
									
				);
									
				$this->load->library('email', $config);
									
				$this->email->set_newline("\r\n");
									
				$admin_email = $this->admin_model->get_admin_email();
										
				if( !$admin_email || $admin_email == '' )
				{
					$admin_email = "preeti.nhq@nic.in";	
				}
									
				$this->email->from($admin_email, 'AMC');
				
				
				//$this->email->cc( $email_arr ); 
									
				//$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
				
				$this->email->bcc( $email_arr );
				
				$sub = "SSC Entry Application";
									
				$this->email->subject( $sub ); // email format subject here		
									
				$msg = $format_obj->form_content; // html email format body here
									
				$msg = "Dear Applicant, Your application and Demand Draft for SSC Entry has been recieved. For details on the interview dates, please visit the website and check your registered email ID after the last date of application ";
				
				$this->email->message($msg);
					
				//$res_send = $this->email->send();	// commented by preeti on 20th mar 14 for automatic testing
								
				// send sms
				
				$msg = "Dear Applicant, Your application and Demand Draft for SSC Entry has been recieved.";
				
				$msg_encoded = urlencode($msg);			
											
				foreach( $mob_arr as $mob )
				{
					$dest_num = $mob;
					
					$url = "http://164.100.14.211/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_encoded."&mnumber=".$dest_num."&signature=NICSUP";

					//$res_sms = file_get_contents( $url );// code commented for black-box testing
				}						
				
				// end, below code added by preeti on 3rd apr 14
				
			}
		
			redirect('admin/doc_list1');
		
		}
		if($this->input->post('sbm') == "Reject") {
		$reg_id_arr = $this->input->post('doc_ids');
		$res_id_arr = $this->input->post('reason_ids');
		
		   //print_r($reg_id_arr);
		if( COUNT( $reg_id_arr ) > 0 )
		{
			$res = $this->admin_model->update_statusrej( $reg_id_arr,$res_id_arr );	
		}

		redirect('admin/doc_list1');
		
		}
	if($this->input->post('sbm') == "Print Preview") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/preview/'.$res_id);
		
		}
	if($this->input->post('sbm') == "Generate Pdf") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Delete") {
	$reg_id_arr = $this->input->post('doc_ids');
		  
		if( COUNT( $reg_id_arr ) > 0 )
		{
			$res = $this->admin_model->del_record( $reg_id_arr );	
		}
		redirect('admin/doc_list');
		}
		
	}*/ // code commented by preeti on 11th apr 14 for black-box testing
	
	// code added by preeti on 11th apr 14 for black-box testing
	public function displayprintall()
	{
		if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		 $this->load->view('doc_p');
	}
	public function submit()
    {
 		if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		 
		 if($this->input->post('sbm') == "Print all") {
			 
			 $this->load->helper('a');     
        $pdf = new a();
		 
		      $file_path = FCPATH."/uploads/result/";
        //$files = scandir($file_path);
$file_path4 = FCPATH."/uploads/rs.pdf";
$file_path5 = FCPATH."/uploads/merged/rs.pdf";

copy($file_path4, $file_path5);
		
		$res_id = $this->input->post('regno');
	$res_id=array_reverse($res_id);	
		
		foreach($res_id as $docid)
		{
			
			
			
			$pdf=$this->doprintallvj($docid,$pdf);
			
			}
		
$pdf->merge('file', FCPATH.'/uploads/merged/rs.pdf');		
	//$this->load->view('doc_p');
	redirect('admin/displayprintall');	
		
		}
		 
	if($this->input->post('sbm') == "Print all Docs") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download Zip") {
			$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint34/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Print all AppDocs") {
		$res_id = $this->input->post('Typr');
		//echo $res_id;
		redirect('admin/doprinta5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download AppZip") {
			$res_id = $this->input->post('Typr');
		//echo $res_id;
		redirect('admin/doprinta34/'.$res_id);
		
		}
	if($this->input->post('sbm') == "Print all RejDocs") {
		$res_id = $this->input->post('Typo');
		//echo $res_id;
		redirect('admin/doprintr5/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Download RejZip") {
			$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprintr34/'.$res_id);
		
		}
	
		// below block is modified by preeti on 3rd apr 14
		
		if($this->input->post('sbm') == "Approve") 
		{
			$reg_id_arr = $this->input->post('doc_ids');
			
			$res_id_arr = $this->input->post('reason_ids');	
							
			
			if( COUNT( $reg_id_arr ) > 0 )
			{
				
				//below line added by preeti on 21st apr 14 for manual testing	
				
				if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
				
				&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
				{
					$res = $this->admin_model->update_statusapp( $reg_id_arr,$res_id_arr );	
					
					// start, below code added by preeti on 3rd apr 14
					
					// add a code to send email and smss to all the approved users
					
					$obj_arr = $this->admin_model->get_all_email($reg_id_arr);
					
					if( $obj_arr )
					{
						$email_arr = array();
					
						$mob_arr = array();  
						
						foreach( $obj_arr as $obj )
						{
							$email_arr[] = $obj->reg_email;
							
							$mob_arr[] = $obj->reg_mobile;  
						}
						
						$config = array(
						
						'protocol' => 'smtp',
												
						'smtp_host' => 'relay.nic.in',
												
						'smtp_port' => 25,
												
						'mailtype' => 'html',
							        
						'newline' => '\r\n',
							        
						'charset' => 'utf-8' //default charset				
											
						); 
						
						
														
						$this->load->library('email', $config);
											
						$this->email->set_newline("\r\n");
											
						$admin_email = $this->admin_model->get_admin_email();
												
						if( !$admin_email || $admin_email == '' )
						{
							$admin_email = "dirafmsp-mod@nic.in"; // changed by preeti on 16th may 14	
						}
											
						$this->email->from($admin_email, 'AMC');
						
						
						//$this->email->cc( $email_arr ); 
											
						//$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
						
						$this->email->bcc( $email_arr );
						
						$sub = "AMC SSC Entry Application";
											
						$this->email->subject( $sub ); // email format subject here		
											
						$msg = $format_obj->form_content; // html email format body here
											
						$msg = "Dear Applicant, Your application and Demand Draft for AMC SSC Entry has been recieved. For details on the interview dates, please visit the website and check your registered email ID after the last date of application ";
						
						$this->email->message($msg);
							
						$res_send = $this->email->send();	
										
						// send sms
						
						$msg = "Dear Applicant, Your application and Demand Draft for AMC SSC Entry has been recieved.";
						
						$msg_encoded = urlencode($msg);			
													
						foreach( $mob_arr as $mob )
						{
							$dest_num = $mob;
														
							$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$msg_encoded."&mnumber=".$dest_num."&signature=NICSUP";
										
							$res_sms = $this->send_sms_curl( $msg_encoded, $dest_num );	
						}						
						
						// end, below code added by preeti on 3rd apr 14					
						
					}

				}			
				
				redirect('admin/doc_list1');				
			}
			else
			{
				redirect('admin/doc_list');
			}
		

		
		}
		
		if($this->input->post('sbm') == "Reject") 
		{
		$reg_id_arr = $this->input->post('doc_ids');
		$res_id_arr = $this->input->post('reason_ids');
		
		   //print_r($reg_id_arr);
		if( COUNT( $reg_id_arr ) > 0 )
		{
			//below line added by preeti on 21st apr 14 for manual testing	
				
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	

				$res = $this->admin_model->update_statusrej( $reg_id_arr,$res_id_arr );	
		
			}	
		
		}

		redirect('admin/doc_list1');
		
		}
	if($this->input->post('sbm') == "Print Preview") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/preview/'.$res_id);
		
		}
	if($this->input->post('sbm') == "Generate Pdf") {
		$res_id = $this->input->post('Typ');
		//echo $res_id;
		redirect('admin/doprint/'.$res_id);
		
		}
		if($this->input->post('sbm') == "Delete") {
	$reg_id_arr = $this->input->post('doc_ids');
		  
		if( COUNT( $reg_id_arr ) > 0 )
		{
			// below if added by preeti on 21st apr 14 for manual testing	
				
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
							
				$res = $this->admin_model->del_record( $reg_id_arr );
			}
					
		}
			
		redirect('admin/doc_list');
		
		}
		
	}
	
		
		public function doprintallvj($regno,$pdf)
		{
			
			
			
	$res = $this->admin_model->get_user_detail( $regno );   
	$html = $this->get_pdf_html( $res,'pdf' );// second parameter added by preeti on 13th may 14
	$pdf=$this->_genvj_pdf1($html,'A4',$regno,$pdf);  	
		return $pdf;	
	}
    public function doprint5()
    {
 if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		
	$regno = $this->uri->segment(3);
	$res = $this->admin_model->get_user_detail( $regno );   
	
	$html = $this->get_pdf_html( $res,'pdf' );// second parameter added by preeti on 13th may 14
	$this->_gen_pdf1($html,'A4',$regno);  
	//$this->_genvj_allpdf($html,'A4',$regno);                 
    
	                  
	
	}
public function doprintr5()
    {
 if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		// $regno = $this->input->post('Type');
		 //echo $regno;
		  $regno = $this->uri->segment(3);
		  //echo $regno;   		    
		 //$doc_id = $this->session->userdata('regno');
		 //echo $doc_id;
			//exit();
		 	   
		// $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 // $a = base_url().'\\images\\'.$res->photo;
//			  echo $a;

					//$regno = $this->session->userdata('regno');   
		    
		 		$res = $this->admin_model->get_user_detailr( $regno );   
		 //echo $res->photo;
		 
		 $html = $this->get_pdf_html( $res, 'pdf' );// second parameter added by preeti on 13th may 14
		    
		 //echo $res->photo;
		 					
			  				    
		$this->_gen_pdf1($html,'A4',$regno);                  
    }
public function doprinta5()
    {
 if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		// $regno = $this->input->post('Type');
		 //echo $regno;
		  $regno = $this->uri->segment(3);
		  //echo $regno;   		    
		 //$doc_id = $this->session->userdata('regno');
		 //echo $doc_id;
			//exit();
		 	   
		// $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 // $a = base_url().'\\images\\'.$res->photo;
//			  echo $a;

					//$regno = $this->session->userdata('regno');   
		    
		 		$res = $this->admin_model->get_user_detaila( $regno );   
		 //echo $res->photo;
		 
		 $html = $this->get_pdf_html( $res , 'pdf' );// second parameter added by preeti on 13th may 14
		    
		 //echo $res->photo;
		 					
			  				    
		$this->_gen_pdf1($html,'A4',$regno);                  
    }
    
    public function doprint2()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		 $doc_id = $this->uri->segment(3);
		 //echo $doc_id;
			//exit();
		 	   
		 $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 $a = base_url()."/images/logo_left.jpg";
			 	// $b=base_url()."/images/logo_right.jpg";
			 	 $b=$res->photo;
				 $image_src = base_url()."uploads/photo/".$b;
		    		 $html = "
			
			<div>
			
			<h3>Form Details</h3>
			
			<table width='100%' style='font-size:10px;'>";
			
			$html .= "<tr>
			
				<td>Registration No.</td>
				
				<td >".$res->u_regno."</td>
				
				<td rowspan='5'><img src='".$image_src."' /></td>
						
			</tr>";
									
			$html .= "<tr>
			
				<td>Name</td>
				
				<td >".strtoupper( $res->u_fname.' '.$res->u_mname.' '.$res->u_lname ) ."</td>
				
				<td rowspan='5'>&nbsp;</td>
						
			</tr>";
			
			$html .= "<tr>
			
				<td>Name (in Hindi)</td>
				
				<td >".$res->u_hindi_name."</td>
				
				<td rowspan='5'>&nbsp;</td>
						
			</tr>";
			
			$html .= "<tr>
			
				<td>Name Changed</td>
				
				<td >".( ($res->u_is_name_change == 'y')?'YES':'NO' )."</td>
				
				<td rowspan='5'>&nbsp;</td>
						
			</tr>";
			
			if( $res->u_is_name_change == 'y' )
			{
				
				$html .= "<tr>
			
				<td>Old Name</td>
				
				<td >".strtoupper( $res->new_fname.' '.$res->new_mname.' '.$res->new_lname ) ."</td>
														
				</tr>";		
				
			}
			
			if( $res->u_sex == 'm' )
			{
				$gender = 'MALE';
			}
			else
			{
				$gender = 'FEMALE';
			} 
			
			$html .= "<tr>
			
				<td>Gender</td>
				
				<td >".$gender."</td>
														
				</tr>";	
			
			$u_dob = $res->u_dob;
						
			
			$html .= " <tr>
			
				<td>Date Of Birth</td>
				
				<td >".db_to_calen( $u_dob ) ."</td>
														
			</tr>";	
			
			$html .= " <tr>
			
				<td>Age</td>
				
				<td >".$res->u_age_year.' years,'.$res->u_age_month.' Months, '.$res->u_age_day.' Days '."</td>
														
			</tr>";
			
			$html .= " <tr>
			
				<td>Mobile</td>
				
				<td >".$res->reg_mobile."</td>
														
			</tr> ";
			
			$html .= "<tr>
			
				<td>Email</td>
				
				<td >".strtoupper( $res->reg_email ) ."</td>
														
			</tr>";			
		
			$html .= " <tr>
			
				<td>Nationality</td>
				
				<td >".strtoupper( $res->u_nationality ) ."</td>
														
			</tr>";
			
			if($res->u_is_married == 'y')
			{
				$u_is_married = 'YES';
			}
			else
			{
				$u_is_married = 'NO';		
			}
	
						
			
			$html .= "<tr>
			
				<td>Are you Married ?</td>
				
				<td >".$u_is_married."</td>
														
			</tr> ";
			
			if( $res->u_is_married == 'y' )
			{
				
				$html .= "<tr>
			
				<td>Date Of Marriage</td>
				
				<td >".db_to_calen( $res->sp_mar_date ) ."</td>
														
				</tr>
				
				<tr>";
			
				$html .="<td>Spouse Name</td>
				
				<td >".strtoupper( $res->sp_name ) ."</td>
														
				</tr>";
				
				
				$html .= "<tr>
			
				<td>Occupation</td>
				
				<td >".strtoupper( $res->sp_occup ) ."</td>
														
				</tr>";
				
				if( $res->sp_nation == 'f' )
				{
					$sp_nation = 'FOREIGN';
				}
				else
				{
					$sp_nation = 'INDIAN';
				}	
				
				$html .= "<tr>
			
				<td>Nationality</td>
				
				<td >".$sp_nation ."</td>
														
				</tr> ";
				
				if( $res->u_citizenship_date != '0000-00-00' )
				{
				
					$html .= "<tr>
				
					<td>Citizenship acquiring Date(if foreigner)</td>
					
					<td >".db_to_calen( $res->u_citizenship_date )."</td>
															
					</tr>";
				
				}
				
				if( $res->sp_ssc_applied == 'y' )
				{
					$sp_ssc_applied = 'YES';
				}
				else
				{
					$sp_ssc_applied = 'NO';
				}	
				
				$html .= "<tr>
				
					<td>Applied for grant of SSC in AMC</td>
					
					<td >".$sp_ssc_applied."</td>
															
					</tr>					
					";	
				
			}
			
			if( $res->u_grad_from == 'f' )
			{
				$u_grad_from = 'FOREIGN';
			}
			else
			{
				$u_grad_from = 'INDIAN';
			}
			
			$html .= "<tr>
				
					<td>Graduated From</td>
					
					<td >".$u_grad_from."</td>
															
					</tr>";					
					
					$html .= "<tr>
				
					<td>PMRC Medical Registration Number</td>
					
					<td >".strtoupper( $res->u_pmrc_num )."</td>
															
					</tr>";
					
					$html .= "<tr>
				
					<td>Date of PMRC Registration</td>
					
					<td >". db_to_calen( $res->u_pmrc_reg_date ) ."</td>
															
					</tr>";
					
				
					$html .= "<tr>
				
					<td>Date of Admission to MBBS</td>
					
					<td >". db_to_calen( $res->u_adm_date ) ."</td>
															
					</tr>";
				
				
					$html .= "<tr>
				
					<td>Date of Date Of Passing MBBS</td>
					
					<td >". db_to_calen( $res->u_passing_date ) ."</td>
															
					</tr>";
				
				
					$html .= "<tr>
				
					<td>MBBS College/University</td>
					
					<td >". strtoupper( $res->u_college.' / '.$res->u_univ ) ."</td>
															
					</tr>";
					
					
					$html .= "<tr>
				
					<td>Internship Completion Date</td>
					
					<td >". db_to_calen( $res->u_intern_date ) ."</td>
															
					</tr>";
					
					
					if( $res->u_college_recog == 'y' )
					{
						$u_college_recog = 'YES';
					}
					else
					{
						$u_college_recog = 'NO';
					}
			
					
					$html .= "<tr>
				
					<td>MBBS college Recg. by MCI</td>
					
					<td >". $u_college_recog ."</td>
															
					</tr>
					
					";
			
			if( $res->u_is_pg == 'y' )
			{
				$u_is_pg = 'YES';
			}
			else
			{
				$u_is_pg = 'NO';
			}
			
			
			$html .= "<tr>
				
					<td>Are you a Post Graduate ?</td>
					
					<td >". $u_is_pg ."</td>
															
					</tr>
					
					";
					
			if( $res->u_is_pg == 'y' )
			{
				$html .= "<tr>
				
					<td>PG Qualification (Subject)</td>
					
					<td >". strtoupper( $res->pg_degree.'('.$res->pg_subject.')') ."</td>
															
					</tr>
					
					";
					
				$html .= "<tr>
				
					<td> College / University </td>
					
					<td >". strtoupper( $res->pg_college.'/'.$res->pg_univ ) ."</td>
															
					</tr>
					
					";
					
				$html .= "<tr>
				
					<td>Year Of Passing</td>
					
					<td >". $res->pg_year ."</td>
															
					</tr>
					
					";
					
				if( $res->pg_mci_recog == 'y' )
				{
					$pg_mci_recog = 'YES';
				}
				else
				{
					$pg_mci_recog = 'NO';
				}	
					
				$html .= "<tr>
				
					<td>PG College Recognized by MCI</td>
					
					<td >". $pg_mci_recog ."</td>
															
					</tr>
					
					";				
							
			}
					
			$html .= "<tr>
				
					<td>DD Number</td>
					
					<td >". $res->u_dd_num ."</td>
															
					</tr>
					
					";	
			
			
			$html .= "<tr>
				
					<td>DD Date</td>
					
					<td >". db_to_calen( $res->u_dd_date )  ."</td>
															
					</tr>
					
					";	
			
			$html .= "<tr>
				
					<td>Payable Bank</td>
					
					<td >". strtoupper( $res->u_dd_bank )  ."</td>
															
					</tr>
					
					";
					
			if( $res->u_current_emp == 'y' )
			{
				$u_current_emp = 'YES';
			}
			else
			{
				$u_current_emp = 'NO';
			}		
			
			$html .= "<tr>
				
					<td>Are you Employed ?</td>
					
					<td >". $u_current_emp  ."</td>
															
					</tr>
					
					";
					
			if( $res->u_current_emp == 'y'  )
			{
				$html .= "<tr>
				
					<td>Current Employer</td>
					
					<td >".strtoupper( $res->ce_employer )  ."</td>
															
					</tr>
					
					";
			}		
			
			$html .= "<tr>
				
					<td>NCC Training Certificate</td>
					
					<td >". strtoupper( $res->u_ncc_cert )  ."</td>
															
					</tr>
					
					";
			
			$html .= "<tr>
				
					<td>Hobbies</td>
					
					<td >". strtoupper( $res->u_hobbies )  ."</td>
															
					</tr>
					
					";
					
			if( $res->u_pref1 == 'arm' )
			{
				$u_pref1 = 'ARMY';
			}
			else if( $res->u_pref1 == 'air' )
			{
				$u_pref1 = 'AIR FORCE';
			}		
			else if( $res->u_pref1 == 'nav' )
			{
				$u_pref1 = 'NAVY';
			}
			
			$html .= "<tr>
				
					<td>First Preference of Service</td>
					
					<td >". $u_pref1  ."</td>
															
					</tr>
					
					";
			
			
			
			if( $res->u_pref2 == 'arm' )
			{
				$u_pref2 = 'ARMY';
			}
			else if( $res->u_pref2 == 'air' )
			{
				$u_pref2 = 'AIR FORCE';
			}		
			else if( $res->u_pref2 == 'nav' )
			{
				$u_pref2 = 'NAVY';
			}
			
			$html .= "<tr>
				
					<td>Second Preference of Service</td>
					
					<td >". $u_pref2  ."</td>
															
					</tr>
					
					";
			
			
			if( $res->u_pref3 == 'arm' )
			{
				$u_pref3 = 'ARMY';
			}
			else if( $res->u_pref3 == 'air' )
			{
				$u_pref3 = 'AIR FORCE';
			}		
			else if( $res->u_pref3 == 'nav' )
			{
				$u_pref3 = 'NAVY';
			}
			
			$html .= "<tr>
				
					<td>Third Preference of Service</td>
					
					<td >". $u_pref3  ."</td>
															
					</tr>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				$u_previous_ssc = 'YES';
			}
			else
			{
				$u_previous_ssc = 'NO';
			}		
					
			$html .= "<tr>
				
					<td>Have you worked in AMC service before ? </td>
					
					<td >". $u_previous_ssc  ."</td>
															
					</tr>
					
					";
			
			if( $res->u_previous_ssc == 'y' )
			{
				
				$html .= "<tr>
				
					<td>Date Of Commission</td>
					
					<td >". db_to_calen( $res->ps_com_date )  ."</td>
															
					</tr>
					
					";
				
				$html .= "<tr>
				
					<td>Date Of Release</td>
					
					<td >". db_to_calen( $res->ps_rel_date )  ."</td>
															
					</tr>
					
					";
				
			}
			
			
			
			$state_post = $this->admin_model->get_state_name( $res->post_state );
			
			$state_perm = $this->admin_model->get_state_name( $res->perm_state );		
									
			
			$html .= "<tr>
				
					<td>Address (Postal)</td>
					
					<td >". strtoupper( $res->post_address.','.$state_post.','.$res->post_pin.','. 'Tel(Landline) - '.$res->post_tel) ."</td>
															
					</tr>
					
					";				
			
			
			$html .= "<tr>
				
					<td>Address (Permanent)</td>
					
					<td >". strtoupper( $res->perm_address.','.$state_perm.','. $res->perm_pin.','. 'Tel(Landline) - '. $res->perm_tel ) ."</td>
															
					</tr>
					
					";			
					
			//$str = "I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.";		
			
			$html .= "<tr>
				
					<td >Declaration</td>
					
					<td >I hereby solemnly declare that all statements made by me in the application are true and correct to the best of my knowledge and belief. At any stage, if information furnished by me is found to be false or incorrect I will be liable for disciplinary action or termination of service as deemed fit.</td>
															
					</tr> ";
								
			$html .= "		
			
			</table>
			
			</div>
			";	
			$this->_gen_pdf1($html,'A4');	                     
    }
  public function dozipall()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		
		// start, added by preeti on 3rd jul 14
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
          
          $offset = 0 ;
          
          if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != '' )
		  {
		  	$offset = $uri_arr['offset'] ;
				
		  }
		  
		  $limit = 10;
		  
		  if( isset( $uri_arr['limit'] ) && $uri_arr['limit'] != '' )
		  {
		  	$limit = $uri_arr['limit'] ;
				
		  } 
		  
		  $sort_by = 'reg_id';
		  
		  if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != '' )
		  {
		  	$sort_by = $uri_arr['sort_by'] ;
				
		  } 
		  
		  $sort_order = 'DESC';

		  if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != '' )
		  {
		  	$sort_order = $uri_arr['sort_order'] ;
				
		  } 
		  		
		  // end, added by preeti on 3rd jul 14  		    
		    		    
		$this->load->library('zip');
	   $this->load->library('mpdf');
	   
	   //$data1 = $this->admin_model->get_reg_id();
	   
	   // above line commented and below added by preeti on 3rd jul 14
	   
	   $data1 = $this->admin_model->get_doc_zip( $offset , $limit, $sort_by, $sort_order);
	   
	   
	   
	   $i=0;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->reg_regno;
	  		
	  	 $i++;
	  	 //echo $i;
		 $res = $this->admin_model->get_user_detail($a1);
		// foreach ($res21 as $res) {
					//$regno = $this->session->us4erdata('regno');   
		    //echo $res->u_regno;echo $res->u_fname;
		 	//$res = $this->admin_model->get_detail2( $regno );   
		 
		 	//$i = $res->u_photo;
		 	
		 	// echo $i;
		 	//exit();
		 	//$image_src = base_url()."uploads/photo/".$res->u_photo;
		 	
		 	$image_src = FCPATH."uploads/photo/".$res->u_photo; // above line commented and this line added by preeti on 19th may 14
		 	
		//print_r($res);
		    $html = $this->get_pdf_html( $res , 'pdf' ); // second parameter added by preeti on 19th may 14
		     //echo $image_src;
		 	//exit();
		
		 	 $paper='A4';
		 $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form-".$a1.".pdf";
        $pdfFilePath = FCPATH."uploads/form/".$a4;
        
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
     
				 $this->load->library('zip');
	   
	  		
			    //$file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		//print_r($files_array);
		if(is_array($files_array))
		{
		unset($files_array);
		}
		
		$files_array = array();
    
		//$file_path11 = FCPATH."uploads/result/".$a4;
		
		
		$file_path11 = FCPATH."uploads/form/".$a4;// above line commented and this line added by preeti on 19th may 14
		
		array_push($files_array,$file_path11);
		$file_path1 = FCPATH."uploads/pmrc/";
		$file_path2 = FCPATH."uploads/dob/";
		$file_path3 = FCPATH."uploads/dd/";
		$file_path4 = FCPATH."uploads/attempt1/";
		$file_path41 = FCPATH."uploads/attempt2/";
		$file_path5 = FCPATH."uploads/new_name/";
		$file_path6 = FCPATH."uploads/emp_noc/";
		$file_path7 = FCPATH."uploads/prev_rel_order/";
		$file_path8 = FCPATH."uploads/result/";
		
		$file_path16 = FCPATH."uploads/mbbs/";
		$file_path17 = FCPATH."uploads/intern/";
		$file_path18 = FCPATH."uploads/pg/pg1/";
		$file_path19 = FCPATH."uploads/pg/pg2/";
		$file_path20 = FCPATH."uploads/pg/pg3/";
		
		//echo $file_path.$file_name1;
			    //array_push($files_array,$file_path1);
		//$data = $this->admin_model->get_detail( $a1 );
		$res2=array();
		 $res2 = $this->admin_model->get_detail3($a1);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
			 
         $res = $this->admin_model->get_detail($a1);
		    
          $file_name1 = $res->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path1.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name2 = $res->dob;
        
//echo $file_name2;
            $file_name2 = trim($file_name2);

            if(!empty($file_name2)) {
         
                    array_push($files_array,$file_path2.$file_name2);
                
		 }
        
			$file_name3 = $res->u_dd_copy;
         

            $file_name3 = trim($file_name3);

            if(!empty($file_name3)) {
         
                    array_push($files_array,$file_path3.$file_name3);
                
		  }
        
			/* $file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if(!empty($file_name)) {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			 * */
			 $file_name342 = $res->u_attempt_cert;
        

            $file_name342 = trim($file_name342);

            if(!empty($file_name342)) {
         
                    array_push($files_array,$file_path4.$file_name342);
                
           }
			$file_name3421 = $res->u_attempt_cert1;
        

            $file_name3421 = trim($file_name3421);

            if(!empty($file_name3421)) {
         
                    array_push($files_array,$file_path41.$file_name3421);
                
           }
			

            if(!empty($t)) {
         
                    array_push($files_array,$file_path18.$t);
                
           }
            if(!empty($t2)) {
         
                    array_push($files_array,$file_path19.$t2);
                
           }
            if(!empty($t3)) {
         
                    array_push($files_array,$file_path20.$t3);
                
           }
			
 $file_name35 = $res->mbbs;
        

            $file_name35 = trim($file_name35);

            if(!empty($file_name35)) {
         
                    array_push($files_array,$file_path16.$file_name35);
                
           }
			 
			$file_name36 = $res->intern;
        

            $file_name36 = trim($file_name36);

            if(!empty($file_name36)) {
         
                    array_push($files_array,$file_path17.$file_name36);
                
           }
			
			$file_name6 =$res->cemp;
        

            $file_name6 = trim($file_name6);

            if(!empty($file_name6)) {
          
                    array_push($files_array,$file_path6.$file_name6);
                
		 }
        
			$file_name5 =$res->nma;
        

            $file_name5 = trim($file_name5);

            if(!empty($file_name5)) {
           
                    array_push($files_array,$file_path5.$file_name5);
                
		  }
        
			$file_name7 =$res->relorder;
        

            $file_name7 = trim($file_name7);

            if(!empty($file_name7)) {
         
           
                    array_push($files_array,$file_path7.$file_name7);
                
				}
        	$this->zip->clear_data();
			
			//print_r($files_array);
        foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
	//$pdfFilePath = FCPATH."uploads/result/";	
//$dirPath2 = "C:\\zipalldocs\\";//removed this line 15-05-2014 Pavan
$dirPath2 = FCPATH."uploads/temp/zipalldocs/";//Pavan 15-05
	//echo $a1;
	//rmdir($dirPath2);
	if (!is_dir($dirPath2)) {
    mkdir($dirPath2, 0777, TRUE);

}
		  //$dirPath = "C:\\zipalldocs\\".$a1;//Pavan removed line 15-05-14
		  $dirPath = FCPATH."uploads/temp/zipalldocs/".$a1;
	//echo $a1;
	if (!is_dir($dirPath)) {
    mkdir($dirPath, 0777, TRUE);

}
	  $s1=$a1.".zip";
	  //$s2=$dirPath.'\\';
	  //$s23="C:\\zipalldocs\\".$a1.'\\'.$s1;//Pavan 15-05
	  
	  $s23=FCPATH."uploads/temp/zipalldocs/".$a1.'/'.$s1;
	//  echo $s23;
		$this->zip->archive($s23);
		//$this->zip->download($s2);		    
		//$this->_gen_pdf1($html,'A4',$doc_id);
		}     
//$s1="files.zip";
	//	$dirPath = "D:\\zip\\".$a1;
	$this->zip->clear_data();
	$a2="lk"; 
		//$dirPath3 = "C:\\zipalldocs\\";//Pavan 15-05
		$dirPath3 = FCPATH."uploads/temp/zipalldocs/";
		//$this->zip->add_dir($dirPath3);
		$this->zip->read_dir($dirPath3);
		  
		//$this->zip->clear_data();
		//$this->zip->clear_data(); 
		//$dirPath31 = "D:\\";
		 // Creates a folder called "myfolder"$this->zip->add_dir($dirPath); // Creates a folder called "myfolder"
		//$dirPath1="C:\\zipalldocs\\zip12.zip";//Pabvan 15-05
		
		$dirPath1=FCPATH."uploads/temp/zipalldocs/zip12.zip";
		//$dirPath11="C:\\fil.zip";
		$this->zip->archive($dirPath1);  
		//$file_path = 'custom/uploads/project/'.$file_name;
		$file_name='zip12.zip';
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=$file_name");
    ob_clean();
    flush();
    readfile($dirPath1);
		$data1 = $this->admin_model->get_reg_id();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	$a1=$res1->reg_regno;
	  	//$dirPath221 =  "C:\\zipalldocs\\".$a1.'\\';//Pavan
	  	$dirPath221 =  FCPATH."uploads/temp/zipalldocs/".$a1.'/';
		  $file=$a1.".zip";
		  $r=$dirPath221.$file;
		  if($r!='.'||$r!='..')
						{
		unlink($r);
		rmdir($dirPath221);
						}
	  }	 
	  
		unlink($dirPath1);
		$dirPath221 = FCPATH."uploads/temp/zipalldocs/";//"C:\\zipalldocs\\"; Pavan 15-05
		rmdir($dirPath221);
   //header('Content-Type: application/octet-stream');
   //header("Content-Dsposition: attachment; filename=$file_name");
    //flush();
    //readfile($dirPath1);
		 
//$this->zip->archive($dirPath);
//$path="D:\zip";
//$this->zip->read_dir($path,TRUE);         
    }
  public function dozipallapp()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		    		    
		$this->load->library('zip');
	   $this->load->library('mpdf');
	   $data1 = $this->admin_model->get_reg_id1();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->reg_regno;
	  	 $i++;
	  	 //echo $a1;
		 $res = $this->admin_model->get_user_detaila($a1);
		// foreach ($res21 as $res) {
					//$regno = $this->session->userdata('regno');   
		    //echo $res->u_regno;echo $res->u_fname;
		 	//$res = $this->admin_model->get_detail2( $regno );   
		 
		 	$image_src = base_url()."uploads/photo/".$res->u_photo;
		    $html = $this->get_pdf_html( $res);
		 	//echo $html;
			 $paper='A4';
		 $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form-".$a1.".pdf";
        $pdfFilePath = FCPATH."uploads/result/".$a4;
        
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
     
				 $this->load->library('zip');
	   
	  		
			    //$file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		//print_r($files_array);
		if(is_array($files_array))
		{
		unset($files_array);
		}
		
		$files_array = array();
    
		$file_path11 = FCPATH."uploads/result/".$a4;
		array_push($files_array,$file_path11);
		$file_path1 = FCPATH."uploads/pmrc/";
		$file_path2 = FCPATH."uploads/dob/";
		$file_path3 = FCPATH."uploads/dd/";
		$file_path4 = FCPATH."uploads/attempt1/";
		$file_path41 = FCPATH."uploads/attempt2/";
		$file_path5 = FCPATH."uploads/new_name/";
		$file_path6 = FCPATH."uploads/emp_noc/";
		$file_path7 = FCPATH."uploads/prev_rel_order/";
		$file_path8 = FCPATH."uploads/result/";
		
		$file_path16 = FCPATH."uploads/mbbs/";
		$file_path17 = FCPATH."uploads/intern/";
		$file_path18 = FCPATH."uploads/pg/pg1/";
		$file_path19 = FCPATH."uploads/pg/pg2/";
		$file_path20 = FCPATH."uploads/pg/pg3/";
		
		//echo $file_path.$file_name1;
			    //array_push($files_array,$file_path1);
		//$data = $this->admin_model->get_detail( $a1 );
		$res2=array();
		 $res2 = $this->admin_model->get_detail3($a1);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
			 
         $res = $this->admin_model->get_detail($a1);
		    
          $file_name1 = $res->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path1.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name2 = $res->dob;
        
//echo $file_name2;
            $file_name2 = trim($file_name2);

            if(!empty($file_name2)) {
         
                    array_push($files_array,$file_path2.$file_name2);
                
		 }
        
			$file_name3 = $res->u_dd_copy;
         

            $file_name3 = trim($file_name3);

            if(!empty($file_name3)) {
         
                    array_push($files_array,$file_path3.$file_name3);
                
		  }
        
			/* $file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if(!empty($file_name)) {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			 * */
			 $file_name342 = $res->u_attempt_cert;
        

            $file_name342 = trim($file_name342);

            if(!empty($file_name342)) {
         
                    array_push($files_array,$file_path4.$file_name342);
                
           }
			$file_name3421 = $res->u_attempt_cert1;
        

            $file_name3421 = trim($file_name3421);

            if(!empty($file_name3421)) {
         
                    array_push($files_array,$file_path41.$file_name3421);
                
           }
			

            if(!empty($t)) {
         
                    array_push($files_array,$file_path18.$t);
                
           }
            if(!empty($t2)) {
         
                    array_push($files_array,$file_path19.$t2);
                
           }
            if(!empty($t3)) {
         
                    array_push($files_array,$file_path20.$t3);
                
           }
			
 $file_name35 = $res->mbbs;
        

            $file_name35 = trim($file_name35);

            if(!empty($file_name35)) {
         
                    array_push($files_array,$file_path16.$file_name35);
                
           }
			 
			$file_name36 = $res->intern;
        

            $file_name36 = trim($file_name36);

            if(!empty($file_name36)) {
         
                    array_push($files_array,$file_path17.$file_name36);
                
           }
			
			$file_name6 =$res->cemp;
        

            $file_name6 = trim($file_name6);

            if(!empty($file_name6)) {
          
                    array_push($files_array,$file_path6.$file_name6);
                
		 }
        
			$file_name5 =$res->nma;
        

            $file_name5 = trim($file_name5);

            if(!empty($file_name5)) {
           
                    array_push($files_array,$file_path5.$file_name5);
                
		  }
        
			$file_name7 =$res->relorder;
        

            $file_name7 = trim($file_name7);

            if(!empty($file_name7)) {
         
           
                    array_push($files_array,$file_path7.$file_name7);
                
				}
        	$this->zip->clear_data();
			
			//print_r($files_array);
        foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
		
$dirPath2 = FCPATH."uploads/temp/zip6/"; // 15-05 Pavan "D:\\zip6\\";
	//echo $a1;
	//rmdir($dirPath2);
	if (!is_dir($dirPath2)) {
    mkdir($dirPath2, 0777, TRUE);

}
		  $dirPath = FCPATH."uploads/temp/zip6/".$a1;// 15-05 Pavan "D:\\zip6\\".$a1;
	//echo $a1;
	if (!is_dir($dirPath)) {
    mkdir($dirPath, 0777, TRUE);

}
	  $s1=$a1.".zip";
	  //$s2=$dirPath.'\\';
	  $s23=FCPATH."uploads/temp/zip6/".$a1.'/'.$s1;//15-05 pavan "D:\\zip6\\".$a1.'\\'.$s1;
	//  echo $s23;
		$this->zip->archive($s23);
		//$this->zip->download($s2);		    
		//$this->_gen_pdf1($html,'A4',$doc_id);
		}     
//$s1="files.zip";
	//	$dirPath = "D:\\zip\\".$a1;
	$this->zip->clear_data();
	$a2="lk"; 
		$dirPath3 =FCPATH."uploads/temp/zip6/"; //15-05 Pavan "D:\\zip6\\";
		//$this->zip->add_dir($dirPath3);
		$this->zip->read_dir($dirPath3);
		  
		//$this->zip->clear_data();
		//$this->zip->clear_data(); 
		//$dirPath31 = "D:\\";
		 // Creates a folder called "myfolder"$this->zip->add_dir($dirPath); // Creates a folder called "myfolder"
		$dirPath1=FCPATH."uploads/temp/zip6/APPROVED.zip";
		$dirPath11="C:\\fil.zip";
		$this->zip->archive($dirPath1);  
		//$file_path = 'custom/uploads/project/'.$file_name;
		$file_name='APPROVED.zip';
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=$file_name");
    ob_clean();
    flush();
    readfile($dirPath1);
		$data1 = $this->admin_model->get_reg_id();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	$a1=$res1->reg_regno;
	  	$dirPath221 = FCPATH."uploads/temp/zip6/".$a1.'/';
		  $file=$a1.".zip";
		  $r=$dirPath221.$file;
		  if($r!='.'||$r!='..')
						{
		unlink($r);
		rmdir($dirPath221);
						}
	  }	 
	  
		unlink($dirPath1);
		$dirPath221 = FCPATH."uploads/temp/zip6/";//15-05 Pavan "D:\\zip6\\";
		rmdir($dirPath221);

   //header('Content-Type: application/octet-stream');
   //header("Content-Disposition: attachment; filename=$file_name");
    //flush();
    //readfile($dirPath1);
		 
//$this->zip->archive($dirPath);
//$path="D:\zip";
//$this->zip->read_dir($path,TRUE);         
    }
public function dozipallrej()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		    		    
		$this->load->library('zip');
	   $this->load->library('mpdf');
	   $data1 = $this->admin_model->get_reg_id2();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	 $a1=$res1->reg_regno;
	  	 $i++;
	  	 //echo $i;
		 $res = $this->admin_model->get_user_detailr($a1);
		// foreach ($res21 as $res) {
					//$regno = $this->session->userdata('regno');   
		    //echo $res->u_regno;echo $res->u_fname;
		 	//$res = $this->admin_model->get_detail2( $regno );   
		 
		 	$image_src = base_url()."uploads/photo/".$res->u_photo;
		    $html = $this->get_pdf_html( $res);
		 	//echo $html;
			 $paper='A4';
		 $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form-".$a1.".pdf";
        $pdfFilePath = FCPATH."uploads/form/".$a4;
        
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
     
				 $this->load->library('zip');
	   
	  		
			    //$file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		//print_r($files_array);
		if(is_array($files_array))
		{
		unset($files_array);
		}
		
		$files_array = array();
    
		$file_path11 = FCPATH."uploads/form/".$a4;
		array_push($files_array,$file_path11);
		$file_path1 = FCPATH."uploads/pmrc/";
		$file_path2 = FCPATH."uploads/dob/";
		$file_path3 = FCPATH."uploads/dd/";
		$file_path4 = FCPATH."uploads/attempt1/";
		$file_path41 = FCPATH."uploads/attempt2/";
		$file_path5 = FCPATH."uploads/new_name/";
		$file_path6 = FCPATH."uploads/emp_noc/";
		$file_path7 = FCPATH."uploads/prev_rel_order/";
		$file_path8 = FCPATH."uploads/result/";
		
		$file_path16 = FCPATH."uploads/mbbs/";
		$file_path17 = FCPATH."uploads/intern/";
		$file_path18 = FCPATH."uploads/pg/pg1/";
		$file_path19 = FCPATH."uploads/pg/pg2/";
		$file_path20 = FCPATH."uploads/pg/pg3/";
		
		//echo $file_path.$file_name1;
			    //array_push($files_array,$file_path1);
		//$data = $this->admin_model->get_detail( $a1 );
		$res2=array();
		 $res2 = $this->admin_model->get_detail3($a1);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
			 
         $res = $this->admin_model->get_detail($a1);
		    
          $file_name1 = $res->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path1.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name2 = $res->dob;
        
//echo $file_name2;
            $file_name2 = trim($file_name2);

            if(!empty($file_name2)) {
         
                    array_push($files_array,$file_path2.$file_name2);
                
		 }
        
			$file_name3 = $res->u_dd_copy;
         

            $file_name3 = trim($file_name3);

            if(!empty($file_name3)) {
         
                    array_push($files_array,$file_path3.$file_name3);
                
		  }
        
			/* $file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if(!empty($file_name)) {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			 * */
			 $file_name342 = $res->u_attempt_cert;
        

            $file_name342 = trim($file_name342);

            if(!empty($file_name342)) {
         
                    array_push($files_array,$file_path4.$file_name342);
                
           }
			$file_name3421 = $res->u_attempt_cert1;
        

            $file_name3421 = trim($file_name3421);

            if(!empty($file_name3421)) {
         
                    array_push($files_array,$file_path41.$file_name3421);
                
           }
			

            if(!empty($t)) {
         
                    array_push($files_array,$file_path18.$t);
                
           }
            if(!empty($t2)) {
         
                    array_push($files_array,$file_path19.$t2);
                
           }
            if(!empty($t3)) {
         
                    array_push($files_array,$file_path20.$t3);
                
           }
			
 $file_name35 = $res->mbbs;
        

            $file_name35 = trim($file_name35);

            if(!empty($file_name35)) {
         
                    array_push($files_array,$file_path16.$file_name35);
                
           }
			 
			$file_name36 = $res->intern;
        

            $file_name36 = trim($file_name36);

            if(!empty($file_name36)) {
         
                    array_push($files_array,$file_path17.$file_name36);
                
           }
			
			$file_name6 =$res->cemp;
        

            $file_name6 = trim($file_name6);

            if(!empty($file_name6)) {
          
                    array_push($files_array,$file_path6.$file_name6);
                
		 }
        
			$file_name5 =$res->nma;
        

            $file_name5 = trim($file_name5);

            if(!empty($file_name5)) {
           
                    array_push($files_array,$file_path5.$file_name5);
                
		  }
        
			$file_name7 =$res->relorder;
        

            $file_name7 = trim($file_name7);

            if(!empty($file_name7)) {
         
           
                    array_push($files_array,$file_path7.$file_name7);
                
				}
        	$this->zip->clear_data();
			
			//print_r($files_array);
        foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
		
$dirPath2 = FCPATH."uploads/temp/REJECTED/";//15-05 Pavan "D:\\REJECTED\\";
	//echo $a1;
	//rmdir($dirPath2);
	if (!is_dir($dirPath2)) {
    mkdir($dirPath2, 0777, TRUE);

}
		  $dirPath = FCPATH."uploads/temp/REJECTED/".$a1;//"D:\\REJECTED\\".$a1; 15-05 Pavan
	//echo $a1;
	if (!is_dir($dirPath)) {
    mkdir($dirPath, 0777, TRUE);

}
	  $s1=$a1.".zip";
	  //$s2=$dirPath.'\\';
	  $s23=FCPATH."uploads/temp/REJECTED/".$a1.'/'.$s1;//15-05 Pavan "D:\\REJECTED\\".$a1.'\\'.$s1;
	//  echo $s23;
		$this->zip->archive($s23);
		//$this->zip->download($s2);		    
		//$this->_gen_pdf1($html,'A4',$doc_id);
		}     
//$s1="files.zip";
	//	$dirPath = "D:\\zip\\".$a1;
	$this->zip->clear_data();
	$a2="lk"; 
		$dirPath3 = FCPATH."uploads/temp/REJECTED/";//15-05 pavan "D:\\REJECTED\\"
		//$this->zip->add_dir($dirPath3);
		$this->zip->read_dir($dirPath3);
		  
		//$this->zip->clear_data();
		//$this->zip->clear_data(); 
		//$dirPath31 = "D:\\";
		 // Creates a folder called "myfolder"$this->zip->add_dir($dirPath); // Creates a folder called "myfolder"
		$dirPath1=FCPATH."uploads/temp/REJECTED/REJECTED.zip";//15-05 pavan "D:\\REJECTED\\REJECTED.zip"
		$dirPath11="C:\\fil.zip";
		$this->zip->archive($dirPath1);  
		//$file_path = 'custom/uploads/project/'.$file_name;
		$file_name='REJECTED.zip';
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=$file_name");
    ob_clean();
    flush();
    readfile($dirPath1);
		$data1 = $this->admin_model->get_reg_id();
	   $i=0;
	  foreach ($data1 as $res1) {
	  	$a1=$res1->reg_regno;
	  	$dirPath221 = FCPATH."uploads/temp/REJECTED/".$a1.'/';//15-05 pavan "D:\\REJECTED\\".$a1.'\\'
		  $file=$a1.".zip";
		  $r=$dirPath221.$file;
		  if($r!='.'||$r!='..')
						{
		unlink($r);
		rmdir($dirPath221);
						}
	  }	 
	  
		unlink($dirPath1);
		$dirPath221 = FCPATH."uploads/temp/REJECTED/";//15-05 pavan
		rmdir($dirPath221);
   //header('Content-Type: application/octet-stream');
   //header("Content-Disposition: attachment; filename=$file_name");
    //flush();
    //readfile($dirPath1);
		 
//$this->zip->archive($dirPath);
//$path="D:\zip";
//$this->zip->read_dir($path,TRUE);         
    }
        
        
        
     public function deletedir($path)
    {
    	
 }
 public function doprint34()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		 $doc_id = $this->uri->segment(3);
		 		    
		 $res = $this->admin_model->get_user_detail( $doc_id );   
		
		 $image_src = FCPATH."uploads/photo/".$res->u_photo;
		 
		 $html = $this->get_pdf_html( $res , 'pdf');// second parameter added by preeti on 19th may 14 again
		 
		    	    
		 $this->_gen_pdf11($html,'A4',$doc_id);                  
    }
public function doprinta34()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		 $doc_id = $this->uri->segment(3);
		 //echo $doc_id;
			//exit();
		 	   //$regno = $this->session->userdata('regno');   
		    
		 	$res = $this->admin_model->get_user_detaila( $doc_id );   
		 //echo $res->photo;
		 
		
		    
		 
		 //$res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	// $a = base_url()."/images/logo_left.jpg";
			 //	 $b=$res->photo;
				// $image_src = base_url()."uploads\\photo\\".$b;
		$image_src = base_url()."uploads/photo/".$res->u_photo;
		 $html = $this->get_pdf_html( $res, 'pdf' );// second parameter added by preeti on 13th may 14
		    //$html="";
		    	    
		$this->_gen_pdf11($html,'A4',$doc_id);                  
    }
public function doprintr34()
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		 $doc_id = $this->uri->segment(3);
		 //echo $doc_id;
			//exit();
		 	   //$regno = $this->session->userdata('regno');   
		    
		 	$res = $this->admin_model->get_user_detailr( $doc_id );   
		 //echo $res->photo;
		 
		
		    
		 
		 //$res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	// $a = base_url()."/images/logo_left.jpg";
			 //	 $b=$res->photo;
				// $image_src = base_url()."uploads\\photo\\".$b;
		$image_src = base_url()."uploads/photo/".$res->u_photo;
		 $html = $this->get_pdf_html( $res, 'pdf' );// second parameter added by preeti on 13th may 14
		    //$html="";
		    	    
		$this->_gen_pdf11($html,'A4',$doc_id);                  
    }
    
    public function doprint42($doc_id)
    {
         if( !$this->check_session() )
		 {
			redirect('doc/index');
			
			exit;
		 }   
		    		    
		 $doc_id = $this->uri->segment(3);
		 //echo $doc_id;
			//exit();
		 	   
		 $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 //$a = base_url()."/images/logo_left.jpg";
			 	 //$b=base_url()."/images/logo_right.jpg";
			 	  $b=$res->photo;
				 $image_src = base_url()."uploads/photo/".$b;
		    	echo $image_src;
		    		 $html = "
						<div><table border=1>
<tr style='height:100px;'><td>Photo: <img src= '".$image_src."' alt='Photo' width='50' height='50 />     
</td></tr>
		</table></div>";   
		//echo $html;
	//	  $this->load->library('mpdf/mpdf');
		//    $pdfFilePath = FCPATH."uploads/result/form3.pdf";
        
	 //$html = $this->load->view('doc_print2', $data, true);
	 //$paper='A4';
	 //$mpdf=new mPDF('hi',$paper);
	 //$mpdf->WriteHTML($html);
        //$mpdf->Output($pdfFilePath, 'F');
     
		$this->_gen_pdf51($html,'A4',$doc_id);                  
    }
    	private function _gen_pdf51($html,$paper='A4',$doc_id)
    {
        $this->load->library('mpdf/mpdf');                
        
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
        $pdfFilePath = FCPATH."uploads/form/form3.pdf";
        
	 //$html = $this->load->view('doc_print2', $data, true);
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
      //  echo $doc_id;
        //$this->download_zip22($doc_id);
        }
    
    //CODE CHANGED BY PAVAN 13-02-13 END        
	//CODE CHANGED BY PAVAN 13-02-13 
    private function merge_pdf()
    {
    $this->load->library('mpdf/PDFMerger');

$pdf = new PDFMerger;

$pdf->addPDF('samplepdfs/one.pdf', '1, 3, 4')
      ->addPDF('samplepdfs/two.pdf', '1-2')
      ->addPDF('samplepdfs/three.pdf', 'all')
      ->merge('file', 'samplepdfs/TEST2.pdf');
	}
    	private function _gen_pdf1($html,$paper='A4',$doc_id)
    {
		
      
	  
	          $this->load->library('mpdf/mpdf');                
        
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form1-".$doc_id.".pdf";
        $pdfFilePath = FCPATH."/uploads/form/".$a4;
        
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
		
        //$this->download_zip22($doc_id,$a4);
		$this->load->helper('a');     
        $pdf = new a();
          $this->tesa($doc_id,$a4,$pdf);// function  name changed Pavan 09-04-14
        }
		
		
		private function _genvj_pdf1($html,$paper='A4',$doc_id,$pdf)
    {
		
      
	  
	          $this->load->library('mpdf/mpdf');                
        
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form1-".$doc_id.".pdf";
        $pdfFilePath = FCPATH."/uploads/form/".$a4;
        
		
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
		
        //$this->download_zip22($doc_id,$a4);
		
          $pdf=$this->vjtesa($doc_id,$a4,$pdf);// function  name changed Pavan 09-04-14
		  
		  return $pdf;
        }
	
		
		
		
		
		
		
		
		
	
    
    	
	
	
	
		public function vjtesa($doc_id,$a4,$pdf)// function  name changed Pavan 09-04-14
{
	  if( !$this->check_session() )   //SESSION CHECK Pavan 09-04-14
		{
			redirect('admin/index');
			
			exit;
		}
		  
		//$doc_id = $this->uri->segment(3);
		$res2=array();
		$ans=$this->admin_model->get_detail22($doc_id);
		//if($ans=='y'){
			
		 $res2 = $this->admin_model->get_detail3($doc_id);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
		//}
		//else {
			//echo "sd";
		//}
		
		//	$data2 = $this->admin_model->get_detail3( $doc_id );
		$data = $this->admin_model->get_detail( $doc_id );        
        $files = array();    
		    
          //$file_name1 = "mpr.pdf";
         
    $url =  FCPATH.'/uploads/pmrc/'.$data->doc;
			$url1 =  FCPATH.'uploads/dob/'.$data->dob;
			//$url2 =  '/iamc/uploads/result/'.$data->gradproof;
			$url3 =  FCPATH.'/uploads/dd/'.$data->u_dd_copy;
			$urla =  FCPATH.'/uploads/attempt1/'.$data->u_attempt_cert;
			$urlm =  FCPATH.'/uploads/attempt2/'.$data->u_attempt_cert1;
			//$a4=""'form-'.$doc_id.'.pdf;
			$urln =  FCPATH.'/uploads/form/'.$a4;
			$a=$data->nma;
			if(!empty($a))
			{
				
			$url4 =FCPATH.'/uploads/new_name/'.$data->nma;
			array_push($files,$url4);	
			}
			$ans=$this->admin_model->get_detail22($doc_id);
			$ae=$ae1=$ae2="";
		//if($ans=='y'){
		//echo "ll";
			$ae=$t;
			if(!empty($ae))
			{
				
			$url7 =FCPATH.'/uploads/pg/pg1/'.$ae;
			array_push($files,$url7);	
			}
			$ae1=$t2;
			if(!empty($ae1))
			{
				
			$url9 =FCPATH.'/uploads/pg/pg2/'.$t2;
			array_push($files,$url9);	
			}
			$ae2=$t3;
			if(!empty($ae2))
			{
				
			$url10 =FCPATH.'/uploads/pg/pg3/'.$t3;
			array_push($files,$url10);	
			}
		///}
			$are=$data->intern;
			if(!empty($are))
			{
				
			$url8 =FCPATH.'/uploads/intern/'.$data->intern;
			array_push($files,$url8);	
			}
			$ace=$data->mbbs;
				
			$url424 =FCPATH.'/uploads/mbbs/'.$data->mbbs;
			array_push($files,$url424);	
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
			$url5 = FCPATH.'/uploads/emp_noc/'.$data->cemp;
			array_push($files,$url5);	
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				
			$url6 = FCPATH.'/uploads/prev_rel_order/'.$data->relorder;
				array_push($files,$url6);
			}    
//echo $file_name1;
           // $url7 = FCPATH.'/uploads/merged/rs.pdf';

           
    // $files = array(<file full paths here>);
   // echo "tww";
	//$this->load->helper('PDFMerger');
	
//$a=$pdf->zp();
//echo $a;
//exit();
 // $this->load->helper('PDFMerger');
	
//$pdf = new PDFMerger;
//echo "tf";
//$pdf = new PDFMerger();
 //echo "paww";

	//		exit();
	

if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		
		->addPDF($urlm, 'all',$doc_id)	
				
		
		;//
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
//echo "as";
//exit();
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
	}
 
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{

	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			 
			;//			
	}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			
			;// 
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			 
			
			;// 
			
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			 
			;// 
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			 
			->addPDF($url9, 'all',$doc_id)
			
			;// 
	
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;// 
	
}

else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			 
			
			;//
			
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			 
			->addPDF($url6, 'all',$doc_id)
			 
			;//
			
	
	
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			;//
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			 
			
			;//
		
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			 
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			 
			;// 
			
	
}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			 
			->addPDF($url5, 'all',$doc_id)
			 
			->addPDF($url7, 'all',$doc_id)
		 	
			;//
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
		
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			 
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			 
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			 
			;//
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			 
			;//
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			 
			
			;//
			
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
	
			;// 
	
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}

else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
						
			;// 
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
			
	
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
	
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			;//
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			
	
}



else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;// 
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;// 
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			
			;//
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
	
}

else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			;//
			

	
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}
//
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
			

}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
			
	
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
	
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

}


 else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
			

	
	
}
	else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
		 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			
}
	else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2)){
		 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
			

	}
//
	else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
			
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//

}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			
			->addPDF($url10, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url8, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;// 
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}

else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
		
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
		
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
		
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			
			->addPDF($url9, 'all',$doc_id)
			
			;//
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}

else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
			
			->addPDF($url1, 'all',$doc_id)
			
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($url8, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
			->addPDF($url10, 'all',$doc_id)
			
			->addPDF($url7, 'all',$doc_id)
			
			;//
}
//
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
	
		->addPDF($url1, 'all',$doc_id)
	
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all',$doc_id)
			->addPDF($url, 'all',$doc_id)
	
			->addPDF($url1, 'all',$doc_id)
	
			->addPDF($url3, 'all',$doc_id)
			->addPDF($urlm, 'all',$doc_id)
			->addPDF($urla, 'all',$doc_id)	
	
	
	 
			->addPDF($url4, 'all',$doc_id)
			->addPDF($url5, 'all',$doc_id)
			->addPDF($url6, 'all',$doc_id)
			->addPDF($url7, 'all',$doc_id)
			->addPDF($url9, 'all',$doc_id)
	
			;//
	
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		
		;//
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;// 
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
			 
		->addPDF($url4, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;//
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		 
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		
		;//
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		 
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;//
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		->addPDF($url10, 'all',$doc_id)
		
		;// 
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all',$doc_id)
		->addPDF($url, 'all',$doc_id)
		
		->addPDF($url1, 'all',$doc_id)
		
		->addPDF($url3, 'all',$doc_id)
		->addPDF($urla, 'all',$doc_id)	
		->addPDF($urlm, 'all',$doc_id)
		
		->addPDF($url4, 'all',$doc_id) 
		->addPDF($url5, 'all',$doc_id)
		->addPDF($url6, 'all',$doc_id)
		->addPDF($url7, 'all',$doc_id)
		->addPDF($url8, 'all',$doc_id)
		->addPDF($url9, 'all',$doc_id)
		
		;//
}
 
//shell_exec("'C:\\Program Files (x86)\\kgsAdobe\\Reader 9.0\\Reader\\AcroRd32.exe' /p /h c:\\xampp\\htdocs\\iamc\\uploads\\result\\rs.pdf \\\\hnurenfp01\\Accounts_FS-1128MFP");
return $pdf;
    }
	
	
	
		public function tesa($doc_id,$a4)// function  name changed Pavan 09-04-14
{
	  if( !$this->check_session() )   //SESSION CHECK Pavan 09-04-14
		{
			redirect('admin/index');
			
			exit;
		}
		        $file_path = FCPATH."/uploads/result/";
        //$files = scandir($file_path);
$file_path4 = FCPATH."/uploads/rs.pdf";
$file_path5 = FCPATH."/uploads/merged/rs.pdf";

copy($file_path4, $file_path5);
		//$doc_id = $this->uri->segment(3);
		$res2=array();
		$ans=$this->admin_model->get_detail22($doc_id);
		//if($ans=='y'){
			
		 $res2 = $this->admin_model->get_detail3($doc_id);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
		//}
		//else {
			//echo "sd";
		//}
		
		//	$data2 = $this->admin_model->get_detail3( $doc_id );
		$data = $this->admin_model->get_detail( $doc_id );        
        $files = array();    
		    
          //$file_name1 = "mpr.pdf";
         
    $url =  FCPATH.'/uploads/pmrc/'.$data->doc;
			$url1 =  FCPATH.'uploads/dob/'.$data->dob;
			//$url2 =  '/iamc/uploads/result/'.$data->gradproof;
			$url3 =  FCPATH.'/uploads/dd/'.$data->u_dd_copy;
			$urla =  FCPATH.'/uploads/attempt1/'.$data->u_attempt_cert;
			$urlm =  FCPATH.'/uploads/attempt2/'.$data->u_attempt_cert1;
			//$a4=""'form-'.$doc_id.'.pdf;
			$urln =  FCPATH.'/uploads/form/'.$a4;
			$a=$data->nma;
			if(!empty($a))
			{
				
			$url4 =FCPATH.'/uploads/new_name/'.$data->nma;
			array_push($files,$url4);	
			}
			$ans=$this->admin_model->get_detail22($doc_id);
			$ae=$ae1=$ae2="";
		//if($ans=='y'){
		//echo "ll";
			$ae=$t;
			if(!empty($ae))
			{
				
			$url7 =FCPATH.'/uploads/pg/pg1/'.$ae;
			array_push($files,$url7);	
			}
			$ae1=$t2;
			if(!empty($ae1))
			{
				
			$url9 =FCPATH.'/uploads/pg/pg2/'.$t2;
			array_push($files,$url9);	
			}
			$ae2=$t3;
			if(!empty($ae2))
			{
				
			$url10 =FCPATH.'/uploads/pg/pg3/'.$t3;
			array_push($files,$url10);	
			}
		///}
			$are=$data->intern;
			if(!empty($are))
			{
				
			$url8 =FCPATH.'/uploads/intern/'.$data->intern;
			array_push($files,$url8);	
			}
			$ace=$data->mbbs;
				
			$url424 =FCPATH.'/uploads/mbbs/'.$data->mbbs;
			array_push($files,$url424);	
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
			$url5 = FCPATH.'/uploads/emp_noc/'.$data->cemp;
			array_push($files,$url5);	
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				
			$url6 = FCPATH.'/uploads/prev_rel_order/'.$data->relorder;
				array_push($files,$url6);
			}    
//echo $file_name1;
           // $url7 = FCPATH.'/uploads/merged/rs.pdf';

           
    // $files = array(<file full paths here>);
   // echo "tww";
	//$this->load->helper('PDFMerger');
	$this->load->helper('a');     
	//include '../system/helpers/a_helper.php';
//echo "twwfa";
$pdf = new a();
//$a=$pdf->zp();
//echo $a;
//exit();
 // $this->load->helper('PDFMerger');
	
//$pdf = new PDFMerger;
//echo "tf";
//$pdf = new PDFMerger();
 //echo "paww";

	//		exit();
if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		
		->addPDF($urlm, 'all')	
				
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
//echo "as";
//exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	}
 
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{

	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');			
	}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
	
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
	
}

else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
	
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
		
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			
	
}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			 
			->addPDF($url7, 'all')
		 	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
		
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
	
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}

else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
						
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}



else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}

else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
//
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url9, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url10, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}


 else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	
	
}
	else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
		 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
	else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2)){
		 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	}
//
	else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');

}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	 
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}

else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
		
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
		
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
		
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}

else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
//
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
	
		->addPDF($url1, 'all')
	
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
	
			->addPDF($url1, 'all')
	
			->addPDF($url3, 'all')
			->addPDF($urlm, 'all')
			->addPDF($urla, 'all')	
	
	
	 
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url9, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
			 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf'); 
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		->addPDF($url4, 'all') 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
 
//shell_exec("'C:\\Program Files (x86)\\kgsAdobe\\Reader 9.0\\Reader\\AcroRd32.exe' /p /h c:\\xampp\\htdocs\\iamc\\uploads\\result\\rs.pdf \\\\hnurenfp01\\Accounts_FS-1128MFP");

$this->load->view('doc_p');
    }
	
	
	
		
	private function _gen_pdf11($html,$paper='A4',$doc_id)
    {
        $this->load->library('mpdf/mpdf');                
        
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
		$a4="form-".$doc_id.".pdf";
        $pdfFilePath = FCPATH."uploads/form/".$a4;
     //EG   $pdfFilePath = "/home/contents/uploads/result/".$a4;
	 
	 //$html = $this->load->view('doc_print2', $data, true);t2()
	 
	 $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, 'F');
      //  echo $doc_id;
        $this->download_zip22($doc_id,$a4);
        //$this->test($doc_id,$a4);
        }
    
    private function _gen_pdf2($html,$paper='A4',$doc_id)
    {
        $this->load->library('mpdf/mpdf');                
        
		
        $mpdf=new mPDF('hi',$paper);
        
        $data=array();
          //  echo $doc_id;
        $this->tesa($doc_id);// function  name changed Pavan 09-04-14
        }
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    	public function testapp($doc_id,$a4)
{
	  
		        $file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);
$file_path4 = FCPATH."uploads/rs.pdf";
$file_path5 = FCPATH."uploads/result/rs.pdf";
copy($file_path4, $file_path5);
		//$doc_id = $this->uri->segment(3);
		$res2=array();
		$ans=$this->admin_model->get_detail22($doc_id);
		//if($ans=='y'){
			
		 $res2 = $this->admin_model->get_detail3($doc_id);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
		//}
		//else {
			//echo "sd";
		//}
		
		//	$data2 = $this->admin_model->get_detail3( $doc_id );
		$data = $this->admin_model->get_detailapp( $doc_id );        
        $files = array();    
		    
          //$file_name1 = "mpr.pdf";
         
    $url =  '/iamc/uploads/pmrc/'.$data->doc;
			$url1 =  '/iamc/uploads/dob/'.$data->dob;
			//$url2 =  '/iamc/uploads/result/'.$data->gradproof;
			$url3 =  '/iamc/uploads/dd/'.$data->u_dd_copy;
			$urla =  '/iamc/uploads/attempt1/'.$data->u_attempt_cert;
			$urlm =  '/iamc/uploads/attempt2/'.$data->u_attempt_cert1;
			//$a4=""'form-'.$doc_id.'.pdf;
			$urln =  '/iamc/uploads/result/'.$a4;
			$a=$data->nma;
			if(!empty($a))
			{
				
			$url4 ='/iamc/uploads/new_name/'.$data->nma;
			array_push($files,$url4);	
			}
			$ans=$this->admin_model->get_detail22($doc_id);
			$ae=$ae1=$ae2="";
		//if($ans=='y'){
		//echo "ll";
			$ae=$t;
			if(!empty($ae))
			{
				
			$url7 ='/iamc/uploads/pg/pg1/'.$ae;
			array_push($files,$url7);	
			}
			$ae1=$t2;
			if(!empty($ae1))
			{
				
			$url9 ='/iamc/uploads/pg/pg2/'.$t2;
			array_push($files,$url9);	
			}
			$ae2=$t3;
			if(!empty($ae2))
			{
				
			$url10 ='/iamc/uploads/pg/pg3/'.$t3;
			array_push($files,$url10);	
			}
		///}
			$are=$data->intern;
			if(!empty($are))
			{
				
			$url8 ='/iamc/uploads/intern/'.$data->intern;
			array_push($files,$url8);	
			}
			$ace=$data->mbbs;
				
			$url424 ='/iamc/uploads/mbbs/'.$data->mbbs;
			array_push($files,$url424);	
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
			$url5 = '/iamc/uploads/emp_noc/'.$data->cemp;
			array_push($files,$url5);	
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				
			$url6 = '/iamc/uploads/prev_rel_order/'.$data->relorder;
				array_push($files,$url6);
			}    
//echo $file_name1;
            $url7 = '/iamc/uploads/result/rs.pdf';

            	//print_r($files);
				
    // $files = array(<file full paths here>);
    $this->load->helper('PDFMerger');
$pdf = new PDFMerger();
if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		
		->addPDF($urlm, 'all')	
				
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{

	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	}
 
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	
}

else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
	
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
		
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			 
			->addPDF($url7, 'all')
		 	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
		
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}

else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
						
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}



else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}

else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

	
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
//
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
	
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url9, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url10, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

}


 else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

	
	
}
	else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
	else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2)){
		$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			

	}
//
	else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
			
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();

}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}

else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}

else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
//
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
	
		->addPDF($url1, 'all')
	
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
	
			->addPDF($url1, 'all')
	
			->addPDF($url3, 'all')
			->addPDF($urlm, 'all')
			->addPDF($urla, 'all')	
	
	
	 
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
	
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url10, 'all')
		
	->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url9, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
			 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		->addPDF($url4, 'all') 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');echo "1";exit();
}
 




 $this->load->view('doc_print', $data);
//$pdf = new FPDI();
//if ($data->password)
//{
 //   $pdf->setProtection(array(),$data->password);
//}
/*
for ($i = 0; $i < count($files); $i++ )
{
    $pagecount = $pdf->setSourceFile($files[$i]);
    for($j = 0; $j < $pagecount ; $j++)
    {
        $tplidx = $pdf->importPage(($j +1), '/MediaBox'); // template index.
       // $pdf->addPage('P','A4');// orientation can be P|L
        $size = $pdf->getTemplateSize($tplidx);
		 $orientation = $size['h'] > $size['w'] ? 'P':'L';
            $pdf->AddPage($orientation);
        $pdf->useTemplate($tplidx, null, null, $size['w'], $size['h'], true);

            //$pdf->SetXY(5, 5);
            //$pdf->SetTextColor(150);
            //$pdf->SetFont('Arial','',8);
            //$pdf->Cell(0,0,$text,0,1,'R');
                           
    }
	//unlink($files[$i]); // you may not want to unlink the files!
}
*/
 
 //$file = "/iamc/uploads/result/list2.pdf";
	// $pdf->Output($file, 'F');
	 //$url=base_url()."pdfs/lorem.pdf";
  //$pdf->setFiles(array($ur));
// add another file
//$pdf->addFile($url1);
//$pdf->addFile($url2);
//$pdf->addFile($url1);
/*$a=$data->nma;
			if(!empty($a))
			{
				
				$pdf->addFile($url4);
			}
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
				$pdf->addFile($url5);
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				$pdf->addFile($url6);
			}

*/
// concatenate the files
//$pdf->concat();

// return the new pdf as a download
//$pdf->Output('/iamc/uploads/result/newpdf.pdf', 'F');
    
    // set the metadata.
    /*
$pdf->SetAuthor($data->user->user_name);
$pdf->SetCreator('website ame!');
 $a="zz";
$pdf->SetSubject('PDF subject !');
$pdf->SetKeywords('website name!'.", keywords! ".$data->user->user_name);
$output = $pdf->Output('', 'S');
$name = "PDF".'-'.$a.'.pdf';

$this->output
    ->set_header("Content-Disposition: filename=$name;")
    ->set_content_type('Application/pdf')
    ->set_output($output);
	 
	 */
    }
  public function testrej($doc_id,$a4)
{
	  
		        $file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);
$file_path4 = FCPATH."/uploads/rs.pdf";
$file_path5 = FCPATH."/uploads/result/rs.pdf";
copy($file_path4, $file_path5);
		//$doc_id = $this->uri->segment(3);
		$res2=array();
		$ans=$this->admin_model->get_detail22($doc_id);
		//if($ans=='y'){
			
		 $res2 = $this->admin_model->get_detail3($doc_id);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
		//}
		//else {
			//echo "sd";
		//}
		
		//	$data2 = $this->admin_model->get_detail3( $doc_id );
		$data = $this->admin_model->get_detailrej( $doc_id );        
        $files = array();    
		    
          //$file_name1 = "mpr.pdf";
         
    $url =  '/iamc/uploads/pmrc/'.$data->doc;
			$url1 =  '/iamc/uploads/dob/'.$data->dob;
			//$url2 =  '/iamc/uploads/result/'.$data->gradproof;
			$url3 =  '/iamc/uploads/dd/'.$data->u_dd_copy;
			$urla =  '/iamc/uploads/attempt1/'.$data->u_attempt_cert;
			$urlm =  '/iamc/uploads/attempt2/'.$data->u_attempt_cert1;
			//$a4=""'form-'.$doc_id.'.pdf;
			$urln =  '/iamc/uploads/result/'.$a4;
			$a=$data->nma;
			if(!empty($a))
			{
				
			$url4 ='/iamc/uploads/new_name/'.$data->nma;
			array_push($files,$url4);	
			}
			$ans=$this->admin_model->get_detail22($doc_id);
			$ae=$ae1=$ae2="";
		//if($ans=='y'){
		//echo "ll";
			$ae=$t;
			if(!empty($ae))
			{
				
			$url7 ='/iamc/uploads/pg/pg1/'.$ae;
			array_push($files,$url7);	
			}
			$ae1=$t2;
			if(!empty($ae1))
			{
				
			$url9 ='/iamc/uploads/pg/pg2/'.$t2;
			array_push($files,$url9);	
			}
			$ae2=$t3;
			if(!empty($ae2))
			{
				
			$url10 ='/iamc/uploads/pg/pg3/'.$t3;
			array_push($files,$url10);	
			}
		///}
			$are=$data->intern;
			if(!empty($are))
			{
				
			$url8 ='/iamc/uploads/intern/'.$data->intern;
			array_push($files,$url8);	
			}
			$ace=$data->mbbs;
				
			$url424 ='/iamc/uploads/mbbs/'.$data->mbbs;
			array_push($files,$url424);	
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
			$url5 = '/iamc/uploads/emp_noc/'.$data->cemp;
			array_push($files,$url5);	
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				
			$url6 = '/iamc/uploads/prev_rel_order/'.$data->relorder;
				array_push($files,$url6);
			}    
//echo $file_name1;
            $url7 = '/iamc/uploads/result/rs.pdf';

            	//print_r($files);
				
    // $files = array(<file full paths here>);
    $this->load->helper('PDFMerger');
$pdf = new PDFMerger();
if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		
		->addPDF($urlm, 'all')	
				
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{echo "11";exit();

	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	}
 
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{echo "12";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "13";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "14";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "15";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "16";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "17";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "18";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}

else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "19";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "120";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			 
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
	
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "121";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	
	echo "122";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "123";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
		
}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "124";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "125";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "126";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			 
			->addPDF($url7, 'all')
		 	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
		echo "127";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
		
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "128";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "129";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			 
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "130";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
echo "131";exit();	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			 
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "132";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			 
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "133";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "134";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
echo "135";exit();	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "141";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "142";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "144";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	echo "145";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}

else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "146";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
						
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "147";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "148";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "149";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "151";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "152";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
echo "153";exit();	
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}



else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "160";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "161";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "171";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "172";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "181";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "190";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "167";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}

else if(!empty($a)&&empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	echo "113";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2)){
	echo "1131";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	
}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "1511";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "191";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "189";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
//
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "176";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "156";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}

else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "143";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "101";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "109";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	echo "155";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url5, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2)){
	echo "1341";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "123";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "1333";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
	
}

else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2)){
	echo "174";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url9, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
	echo "1711";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url10, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}
else if(empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&!empty($ae2)){
	echo "1067";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url6, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

}


 else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{echo "1890";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	
	
}
	else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2)){
		echo "1891";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
	else if(empty($a)&&empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2)){
		echo "1992";exit();
		$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			

	}
//
	else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "1200";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
			
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "1310";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');

}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "1331";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "1998";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			
			->addPDF($url10, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "1999";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "17780";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "18821";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "12318";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url8, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "16661";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "17779";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url6, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "17756";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "123456";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url4, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}

else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "19234";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "190890";exit();	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "123410";exit();	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "1786789";exit();	
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url5, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			
			->addPDF($url7, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "123123454";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	echo "81";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url5, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url9, 'all')
			
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "91";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url8, 'all')
			
			->addPDF($url9, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	
	echo "101";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url6, 'all')
			->addPDF($url8, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}

else if(empty($a)&&empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	echo "3111";exit();
$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
			
			->addPDF($url1, 'all')
			
			->addPDF($url3, 'all')
			->addPDF($urla, 'all')	
			->addPDF($urlm, 'all')
			->addPDF($url8, 'all')
			->addPDF($url9, 'all')
			->addPDF($url10, 'all')
			
			->addPDF($url7, 'all')
			
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
//
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&empty($ae2))
{
	echo "4111";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
	
		->addPDF($url1, 'all')
	
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "5671";exit();
	$pdf->addPDF($urln, 'all')
			->addPDF($url, 'all')
	
			->addPDF($url1, 'all')
	
			->addPDF($url3, 'all')
			->addPDF($urlm, 'all')
			->addPDF($urla, 'all')	
	
	
	 
			->addPDF($url4, 'all')
			->addPDF($url5, 'all')
			->addPDF($url6, 'all')
			->addPDF($url7, 'all')
			->addPDF($url9, 'all')
	
			->merge('file', FCPATH.'/uploads/merged/rs.pdf');
	
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "1978";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url10, 'all')
		
	->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "1995";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "1567";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	echo "123454";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url5, 'all')
		->addPDF($url9, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "16543221";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "19000";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&empty($b)&&!empty($c)&&empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	echo "189736";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
			 
		->addPDF($url4, 'all')
		->addPDF($url6, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "134237";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&empty($ae1)&&!empty($ae2))
{
	echo "11234567";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(empty($a)&&empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&!empty($ae2))
{
	echo "1567892";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		->addPDF($url10, 'all')
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
else if(!empty($a)&&!empty($b)&&!empty($c)&&!empty($ae)&&!empty($are)&&!empty($ae1)&&empty($ae2))
{
	echo "156789";exit();
$pdf->addPDF($urln, 'all')
		->addPDF($url, 'all')
		
		->addPDF($url1, 'all')
		
		->addPDF($url3, 'all')
		->addPDF($urla, 'all')	
		->addPDF($urlm, 'all')
		
		->addPDF($url4, 'all') 
		->addPDF($url5, 'all')
		->addPDF($url6, 'all')
		->addPDF($url7, 'all')
		->addPDF($url8, 'all')
		->addPDF($url9, 'all')
		
		
		->merge('file', FCPATH.'/uploads/merged/rs.pdf');
}
 




 $this->load->view('doc_print22', $data);
//$pdf = new FPDI();
//if ($data->password)
//{
 //   $pdf->setProtection(array(),$data->password);
//}
/*
for ($i = 0; $i < count($files); $i++ )
{
    $pagecount = $pdf->setSourceFile($files[$i]);
    for($j = 0; $j < $pagecount ; $j++)
    {
        $tplidx = $pdf->importPage(($j +1), '/MediaBox'); // template index.
       // $pdf->addPage('P','A4');// orientation can be P|L
        $size = $pdf->getTemplateSize($tplidx);
		 $orientation = $size['h'] > $size['w'] ? 'P':'L';
            $pdf->AddPage($orientation);
        $pdf->useTemplate($tplidx, null, null, $size['w'], $size['h'], true);

            //$pdf->SetXY(5, 5);
            //$pdf->SetTextColor(150);
            //$pdf->SetFont('Arial','',8);
            //$pdf->Cell(0,0,$text,0,1,'R');
                           
    }
	//unlink($files[$i]); // you may not want to unlink the files!
}
*/
 
 //$file = "/iamc/uploads/result/list2.pdf";
	// $pdf->Output($file, 'F');
	 //$url=base_url()."pdfs/lorem.pdf";
  //$pdf->setFiles(array($ur));
// add another file
//$pdf->addFile($url1);
//$pdf->addFile($url2);
//$pdf->addFile($url1);
/*$a=$data->nma;
			if(!empty($a))
			{
				
				$pdf->addFile($url4);
			}
			
			
			$b=$data->cemp;
			if(!empty($b))
			{
				
				$pdf->addFile($url5);
			}
			$c=$data->relorder;
			if(!empty($c))
			{
				$pdf->addFile($url6);
			}

*/
// concatenate the files
//$pdf->concat();

// return the new pdf as a download
//$pdf->Output('/iamc/uploads/result/newpdf.pdf', 'F');
    
    // set the metadata.
    /*
$pdf->SetAuthor($data->user->user_name);
$pdf->SetCreator('website ame!');
 $a="zz";
$pdf->SetSubject('PDF subject !');
$pdf->SetKeywords('website name!'.", keywords! ".$data->user->user_name);
$output = $pdf->Output('', 'S');
$name = "PDF".'-'.$a.'.pdf';

$this->output
    ->set_header("Content-Disposition: filename=$name;")
    ->set_content_type('Application/pdf')
    ->set_output($output);
	 
	 */
    }
  
		
	function download_zip() {
       if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
       $this->load->library('zip');
		        $file_path = FCPATH."uploads/form/";
        //$files = scandir($file_path);

		$doc_id = $this->uri->segment(3);
		
			
		$data = $this->admin_model->get_detail( $doc_id );        
        $files_array = array();    
		    
          $file_name1 = $data->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name = $data->dob;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
         
                    array_push($files_array,$file_path.$file_name);
                
		 }
        
			$file_name = $data->photo;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
         
                    array_push($files_array,$file_path.$file_name);
                
		  }
        
			$file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			$file_name = $data->u_attempt_cert;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
         
                    array_push($files_array,$file_path.$file_name);
                
           }
			
			$file_name =$data->cemp;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
          
                    array_push($files_array,$file_path.$file_name);
                
		 }
        
			$file_name =$data->nma;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
           
                    array_push($files_array,$file_path.$file_name);
                
		  }
        
			$file_name =$data->relorder;
        

            $file_name = trim($file_name);

            if($file_name != '.' || $file_name != '..') {
         
           
                    array_push($files_array,$file_path.$file_name);
                
				}
        
			$zip_file_name='ak.zip';
        foreach($files_array as $file){
        	echo $file;
			//echo $file_path.$file;
            $this->zip->read_file($file);
        }
		$this->zip->archive('D:\zip1.zip');
        $this->zip->download('zip1.zip');

    }
	function download_zip222($doc_id,$a4) {
       if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
       $this->load->library('zip');
	    //$this->load->library('CreateZipFile.inc');
	  		
			    //$file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		$files_array = array();    
		$file_path11 = FCPATH."uploads/result/".$a4;
		 array_push($files_array,$file_path11);
		$file_path1 = FCPATH."uploads/pmrc\\";
		$file_path2 = FCPATH."uploads/dob\\";
		$file_path3 = FCPATH."uploads/dd\\";
		$file_path4 = FCPATH."uploads/attempt\\";
		$file_path5 = FCPATH."uploads/new_name\\";
		$file_path6 = FCPATH."uploads/emp_noc\\";
		$file_path7 = FCPATH."uploads/prev_rel_order\\";
		$file_path8 = FCPATH."uploads/result/";
		
		
		
		//echo $file_path11;
			    //array_push($files_array,$file_path1);
		$data = $this->admin_model->get_detail( $doc_id );        
        
		    
          $file_name1 = $data->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path1.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name2 = $data->dob;
        

            $file_name2 = trim($file_name2);

            if(!empty($file_name2)) {
         
                    array_push($files_array,$file_path2.$file_name2);
                
		 }
        
			$file_name3 = $data->u_dd_copy;
         

            $file_name3 = trim($file_name3);

            if(!empty($file_name3)) {
         
                    array_push($files_array,$file_path3.$file_name3);
                
		  }
        
			/* $file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if(!empty($file_name)) {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			 * */
			$file_name4 = $data->u_attempt_cert;
        

            $file_name4 = trim($file_name4);

            if(!empty($file_name4)) {
         
                    array_push($files_array,$file_path4.$file_name4);
                
           }
			
			$file_name6 =$data->cemp;
        

            $file_name6 = trim($file_name6);

            if(!empty($file_name6)) {
          
                    array_push($files_array,$file_path6.$file_name6);
                
		 }
        
			$file_name5 =$data->nma;
        

            $file_name5 = trim($file_name5);

            if(!empty($file_name5)) {
           
                    array_push($files_array,$file_path5.$file_name5);
                
		  }
        
			$file_name7 =$data->relorder;
        

            $file_name7 = trim($file_name7);

            if(!empty($file_name7)) {
         
           
                    array_push($files_array,$file_path7.$file_name7);
                
				}
        
			
        foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
		  $s1="a.zip";
	 $s2="D://a.zip";
	 if(file_exists($s2))
	  {
	  	//unlink($s2);
	  }
	 //$this->zip->clear_data();
	 // $path1 = FCPATH."uploads/result/".$s1;
		$this->zip->archive($s2);
		//$this->download12($s2)
		//$this->zip->read_file($s2);
		//$name="/iamc/uploads/".$s1;
		$this->zip->download($s2);
		//$this->force_download1('zip111.zip','D:\zip111.zip');
		//$path="D:\zip111.zip";
		//$filename=$s1.'(1).zip';
		//unlink($filename);
		//$this->load->helper('downloads');
		//$name=".$s1;
		$path="zip111.zip";
		//echo $s2;
		$zipname = 'filename.zip';
  //  $zip = new ZipArchive();
    //$zip->open($zipname, ZipArchive::CREATE);
    //foreach ($files as $file) {
      //$zip->addFile($file);
    //}
   // $zip->close();
		 //header('Content-Type: application/zip');
    //header('Content-disposition: attachment; filename=filename.zip');
    //header('Content-Length: ' . filesize($zipfilename));
    //readfile($zipname);
		//$this->_push_file($s2, $s1);
	//	 $s2="C:\\Users\\pavan\\Downloads\\a.zip";
	 if(file_exists($s2))
	 {
	  	//unlink($s2);
	  }
	 //$this->zip->clear_data();
	 // $path1 = FCPATH."uploads/result/".$s1;
		//$this->zip->archive($s2);
	//$this->load->view('download_zip');
	  
						}
function download_zip22($doc_id,$a4) {
       
       $this->load->library('zip');
	    //$this->load->library('CreateZipFile.inc');
	  		
			    //$file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		    
		$files_array = array();
    
		//$file_path11 = FCPATH."uploads/result/".$a4;
		
		$file_path11 = FCPATH."uploads/form/".$a4; // above line commented and this added by preeti on 19th may 14
		 array_push($files_array,$file_path11);
		$file_path1 = FCPATH."uploads/pmrc/";
		$file_path2 = FCPATH."uploads/dob/";
		$file_path3 = FCPATH."uploads/dd/";
		$file_path4 = FCPATH."uploads/attempt1/";
		$file_path41 = FCPATH."uploads/attempt2/";
		$file_path5 = FCPATH."uploads/new_name/";
		$file_path6 = FCPATH."uploads/emp_noc/";
		$file_path7 = FCPATH."uploads/prev_rel_order/";
		$file_path8 = FCPATH."uploads/result/";
		
		$file_path16 = FCPATH."uploads/mbbs/";
		$file_path17 = FCPATH."uploads/intern/";
		$file_path18 = FCPATH."uploads/pg/pg1/";
		$file_path19 = FCPATH."uploads/pg/pg2/";
		$file_path20 = FCPATH."uploads/pg/pg3/";
		
		//echo $file_path.$file_name1;
			    //array_push($files_array,$file_path1);
		//$data = $this->admin_model->get_detail( $a1 );
		$res2=array();
		 $res2 = $this->admin_model->get_detail3($doc_id);
		 //print_r($res2);
		 
		       $t=$res2[0];
			   $t2=$res2[1];
			   $t3=$res2[2];
		         
			 
         $res = $this->admin_model->get_detail($doc_id);
		    
          $file_name1 = $res->doc;
        

            $file_name1 = trim($file_name1);

            if($file_name1 != '.' || $file_name1 != '..') {
              
                	//echo $file_path.$file_name1;
                    array_push($files_array,$file_path1.$file_name1);
                
           
        
			}
			//print_r($files_array);
			$file_name2 = $res->dob;
        
//echo $file_name2;
            $file_name2 = trim($file_name2);

            if(!empty($file_name2)) {
         
                    array_push($files_array,$file_path2.$file_name2);
                
		 }
        
			$file_name3 = $res->u_dd_copy;
         

            $file_name3 = trim($file_name3);

            if(!empty($file_name3)) {
         
                    array_push($files_array,$file_path3.$file_name3);
                
		  }
        
			/* $file_name = $data->gradproof;
        

            $file_name = trim($file_name);

            if(!empty($file_name)) {
         
                    array_push($files_array,$file_path.$file_name);
                
           
			}
			 * */
			 $file_name342 = $res->u_attempt_cert;
        

            $file_name342 = trim($file_name342);

            if(!empty($file_name342)) {
         
                    array_push($files_array,$file_path4.$file_name342);
                
           }
			$file_name3421 = $res->u_attempt_cert1;
        

            $file_name3421 = trim($file_name3421);

            if(!empty($file_name3421)) {
         
                    array_push($files_array,$file_path41.$file_name3421);
                
           }
			

            if(!empty($t)) {
         
                    array_push($files_array,$file_path18.$t);
                
           }
            if(!empty($t2)) {
         
                    array_push($files_array,$file_path19.$t2);
                
           }
            if(!empty($t3)) {
         
                    array_push($files_array,$file_path20.$t3);
                
           }
			
 $file_name35 = $res->mbbs;
        

            $file_name35 = trim($file_name35);

            if(!empty($file_name35)) {
         
                    array_push($files_array,$file_path16.$file_name35);
                
           }
			 
			$file_name36 = $res->intern;
        

            $file_name36 = trim($file_name36);

            if(!empty($file_name36)) {
         
                    array_push($files_array,$file_path17.$file_name36);
                
           }
			
			$file_name6 =$res->cemp;
        

            $file_name6 = trim($file_name6);

            if(!empty($file_name6)) {
          
                    array_push($files_array,$file_path6.$file_name6);
                
		 }
        
			$file_name5 =$res->nma;
        

            $file_name5 = trim($file_name5);

            if(!empty($file_name5)) {
           
                    array_push($files_array,$file_path5.$file_name5);
                
		  }
        
			$file_name7 =$res->relorder;
        

            $file_name7 = trim($file_name7);

            if(!empty($file_name7)) {
         
           
                    array_push($files_array,$file_path7.$file_name7);
                
				}
        	$this->zip->clear_data();
			
			//print_r($files_array);
        foreach($files_array as $file){
        	//echo $file;
			
			
            $this->zip->read_file($file);
        }
		
		$dirPath2 =FCPATH."uploads/temp/zip31/";// 15-5 Pavans FCPATH."zip31//";
	//echo $a1;
	if (!is_dir($dirPath2)) {
    mkdir($dirPath2, 0777, TRUE);

}
/*	  $dirPath = "D:\\zip31\\".$doc_id;
	//echo $a1;
	if (!is_dir($diPath)) {
    mkdir($dirPath, 0777, TRUE);

}
  */
	//$this->zip->add_dir($dirPath); // Creates a folder called "myfolder"
	 $s1=$doc_id.".zip";
	  //$s2=$dirPath.'\\';
	  $s23=FCPATH."uploads/temp/zip31/".$s1;//15-5 Pavan "D:\\zip31\\".$s1;
	//  echo $s23;
		$this->zip->archive($s23);
		//$this->zip->download($s2);		    
		//$this->_gen_pdf1($html,'A4',$doc_id);
		
//$s1="files.zip";
	//	$dirPath = "D:\\zip\\".$a1;
	//$this->zip->clear_data();
	$a2="lk"; 
		
		//$dirPath3 = "D:\\zip31\\";
		
		//$this->zip->add_dir($dirPath3);
		
		//$this->zip->read_dir($dirPath3);
		  
		//$this->zip->clear_data();
		//$this->zip->clear_data(); 
		//$dirPath31 = "D:\\";
		 // Creates a folder called "myfolder"$this->zip->add_dir($dirPath); // Creates a folder called "myfolder"
		 $s23=FCPATH."uploads/temp/zip31/".$s1;// 15-5 Pavan FCPATH."zip31//".$s1;
		//$dirPath1="D:\\zip31.zip";
		//$dirPath11="C:\\fil.zip";
		//$this->zip->archive($dirPath1);  
		//$file_path = 'custom/uploads/project/'.$file_name;
		$file_name=$s1;
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=$file_name");
    ob_clean();
    flush();
    readfile($s23);
 unlink($s23);
	
	//	$s1=$doc_id.".zip";
	  //$s2=$dirPath.'\\';
	  //$s23="D:\\zip3\\".$s1;
	//  echo $s23;
		//$this->zip->archive($s23);
		}
						function download12($fullPath)
	{
if ($fd = fopen ($fullPath, "r")) {
    $fsize = filesize($fullPath);
    $path_parts = pathinfo($fullPath);
    $ext = strtolower($path_parts["extension"]);
    switch ($ext) {
        case "zip":
        header("Content-type: application/zip"); // add here more headers for diff. extensions
        header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a download
        break;
        default;
        header("Content-type: application/octet-stream");
        header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
    }
    header("Content-length: $fsize");
    header("Cache-control: private"); //use this to open files directly
    while(!feof($fd)) {
        $buffer = fread($fd, 2048);
        echo $buffer;
    }
}
	}
function force_download1($filename = '', $path = '')
	{
		
		// Try to determine if the filename includes a file extension.
		// We need it in order to set the MIME type
		
echo "sssa";
		// Grab the file extension
		$this->load->helper('file');

    /**
     * This uses a pre-built list of mime types compiled by Codeigniter found at
     * /system/application/config/mimes.php 
     * Codeigniter says this is prone to errors and should not be dependant upon
     * However it has worked for me so far. 
     * You can also add more mime types as needed.
     */
    $mime = get_mime_by_extension($path);
		if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") !== FALSE)
		{
			header('Content-Type: "'.$mime.'"');
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
			header('Content-Length: '.filesize($path)); // provide file size
		}
		else
		{
header('Pragma: public');     // required
    header('Expires: 0');         // no cache
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
    header('Cache-Control: private',false);
    header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
    header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($path)); // provide file size
    header('Connection: close');
    readfile($path); // push it out
    		}

		//exit($data);
	}

function download_zip23() {
       
        $this->load->library('zip');
	   
		        $file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		$path = FCPATH."uploads/form/REJECTED.pdf";
		$files_array = array();    
		//echo $file_path.$file_name1;
			    array_push($files_array,$file_path1);
		        foreach($files_array as $file){
        //	echo $file;
        
			
			
            $this->zip->read_file($file);
        }
		//$this->load->helper('downloads');
		//$path = FCPATH."uploads/result/list.pdf";
		$name=FCPATH."uploads/form/REJECTED.pdf";
		//$path1="D:/zip1.zip";
		$this->_push_file($path, $name);
				
				
		}
function download_zip24() {
       if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
        $this->load->library('zip');
	   
		        $file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		$path = FCPATH."uploads/form/list2.pdf";
		$files_array = array();    
		//echo $file_path.$file_name1;
			    array_push($files_array,$file_path1);
		        foreach($files_array as $file){
        //	echo $file;
        
			
			
            $this->zip->read_file($file);
        }
		//$this->load->helper('downloads');
		//$path = FCPATH."uploads/result/list.pdf";
		$name=FCPATH."uploads/form/list2.pdf";
		//$path1="D:/zip1.zip";
		$this->_push_file($path, $name);
				
				
		}

function download_zip25() {
       if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
        $this->load->library('zip');
	   
		        $file_path = FCPATH."uploads/result/";
        //$files = scandir($file_path);

		//$doc_id = $this->uri->segment(3);
		$path = FCPATH."uploads/form/form.pdf";
		$files_array = array();    
		//echo $file_path.$file_name1;
			    array_push($files_array,$file_path1);
		        foreach($files_array as $file){
        //	echo $file;
			
			
            $this->zip->read_file($file);
        }
		//$this->load->helper('downloads');
		//$this->zip->archive($path1);  
		//$file_path = 'custom/uploads/project/'.$file_name;
		$file_name='form.pdf';
    header('Content-Type: application/octet-stream');
    header("Content-Disposition: attachment; filename=$file_name");
    ob_clean();
    flush();
    readfile($path);
				
				
		}

    function _push_file($path, $name)
{
  // make sure it's a file before doing anything!
  //echo $path;
  if(is_file($path))
  {
    // required for IE
   // echo $path;
    if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

    // get the file mime type using the file extension
    $this->load->helper('file');

    /**
     * This uses a pre-built list of mime types compiled by Codeigniter found at
     * /system/application/config/mimes.php 
     * Codeigniter says this is prone to errors and should not be dependant upon
     * However it has worked for me so far. 
     * You can also add more mime types as needed.
     */
    $mime = get_mime_by_extension($path);

    // Build the headers to push out the file properly.
    header('Pragma: public');     // required
    header('Expires: 0');         // no cache
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
    header('Cache-Control: private',false);
    header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
    header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($path)); // provide file size
    header('Connection: close');
    readfile($path); // push it out
    unlink($path);
   // exit();
}
}
//CODE CHANGED BY PAVAN 13-02-13 END
	//CODE CHANGED BY PAVAN 13-02-13 STRT
	public function preview()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		     if( $this->check_session() )
		 {
			 
		    		    
		 $regno = $this->uri->segment(3);   		    
		 //$doc_id = $this->session->userdata('regno');
		 //echo $doc_id;
			//exit();
		 	   
		// $res = $this->admin_model->get_detail( $doc_id );   
		 
		 
		 	 // $a = base_url().'\\images\\'.$res->photo;
//			  echo $a;

					//$regno = $this->session->userdata('regno');   
		    
		 		$res = $this->admin_model->get_user_detail( $regno );   
		 //echo $res->photo;
		 
		 $html = $this->get_pdf_html( $res );
		    
		 		
			$data['regno'] = $res->u_regno;
						
			$data['html'] = $html;
			
			$this->load->view('admin_preview', $data);	
		}
		else 
		{
			redirect('doc/index');
		}	
		
	} 
	//CODE CHANGED BY PAVAN 13-02-13 END
	
	
	public function doc_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$reg_id_arr = $this->input->post('doc_ids');
		  
		if( COUNT( $reg_id_arr ) > 0 )
		{
			//$res = $this->admin_model->del_record( $reg_id_arr );	
		}
			
		redirect('admin/doc_list');
		
	}
	
		/*
	public function clear()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		$data['title'] = "Admin::Clear DB";
		
		$data['errmsg'] = "Have you taken a backup of your Database to proceed with clearing of the Database";
		
		$this->load->view('admin_clear', $data);
		
	}
	*/
	
	// below function is commented by preeti on 21st apr 14 as it is not being used in the project
	
	/*public function update_doc()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
			
		
		$errmsg = '';
		
		$doc_id = '';
		
		if( $this->uri->segment(3) != '' )
		{
			$doc_id = $this->uri->segment(3);	
		}
		else if( $this->input->post('doc_id') != '' )
		{
			$doc_id = $this->input->post('doc_id');	
		}
		
		
		
		$this->form_validation->set_rules('doc_fname', 'First Name', 'trim|required|xss_clean');
		
		$this->form_validation->set_rules('doc_lname', 'Last Name', 'trim|required|xss_clean'); 
		
		//$this->form_validation->set_rules('doc_mobile', 'Mobile', 'trim|required|integer|max_length[10]|xss_clean');
		
		//$this->form_validation->set_rules('doc_email', 'Email', 'trim|required|valid_email|xss_clean');
		
		$this->form_validation->set_rules('doc_dob', 'Date Of Birth', 'required');
		
		$this->form_validation->set_rules('doc_qual', 'Qualification', 'required');
		
		$this->form_validation->set_rules('doc_stream', 'Specialization', 'required');
		
		$this->form_validation->set_rules('doc_college', 'College', 'required');
		
		if( $this->form_validation->run() == TRUE )
		{
			
			$data = array();
		
			$photo_file_name = '';
			
			$resume_file_name = '';
			
			$previous_photo = ''; // in order to delete the previous files
		
			$previous_resume = '';// in order to delete the previous files
			
			
			
			
			$this->load->library('upload');
			
			
			if( isset($_FILES['doc_photo']) && $_FILES['doc_photo']['size'] > 0  )
			{
				
				$config['upload_path'] = './uploads/photo/';
				
				$config['allowed_types'] = 'gif|jpeg|jpg|png';
				
				$config['max_size'] = '100'; // in kb
				
				$config['max_width'] = '189'; // in pixel
				
				$config['max_height'] = '189'; // in pixel
				
				$config['min_width'] = '76'; // in pixel
			
				$config['min_height'] = '76'; // in pixel
				
				$this->upload->initialize($config);
				
				if(! $this->upload->do_upload('doc_photo') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$photo_upload_arr = $this->upload->data();
			
					$photo_file_name = $photo_upload_arr['file_name'];
										
				}
			}
			
			
			if( isset( $_FILES['doc_resume'] ) && $_FILES['doc_resume']['size'] > 0   )
			{
				$config['upload_path'] = './uploads/resume/';
			
				$config['allowed_types'] = 'doc|docx|pdf';
				
				$config['max_size'] = '100'; // in kb
				
				$config['max_width'] = '0'; // in pixel
				
				$config['max_height'] = '0'; // in pixel
				
				$this->upload->initialize($config);
				
				
				if(! $this->upload->do_upload('doc_resume') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$resume_upload_arr = $this->upload->data();
					
					$resume_file_name = $resume_upload_arr['file_name'];
				}	
				
			}
			
			if( $photo_file_name !='' )
			{
					// delete the previous photo from the uploads/photo folder
					
					$previous_photo = $this->admin_model->get_item_name( $doc_id, 'photo' );// get the previous file name
									
			}
				
			if( $resume_file_name !='' )
			{
					// delete the previous resume from the uploads/resume folder
					
					$previous_resume = $this->admin_model->get_item_name( $doc_id, 'resume' );// get the previous file name
									
			}

			// shoot an update query here
	
			$res = $this->admin_model->update_doc( $doc_id, $photo_file_name, $resume_file_name );	
			
			if($res)
			{
				if( !empty( $photo_file_name ) )
				{
				
					$photo_url = "uploads/photo/".$previous_photo; // path to the previous file
					
					if(file_exists($photo_url))
					{
						unlink($photo_url);
					}
				}
				
				if( !empty( $resume_file_name ) )
				{
				
					$resume_url = "uploads/resume/".$previous_resume; // path to the previous file
					
					if(file_exists($resume_url))
					{
						unlink($resume_url);
					}
				}
				
				$errmsg = "Profile Updated Successfully ";
			}
			else 
			{
				$errmsg = "Profile couldn't be Updated";
			}			
			
		}
		
		
		$data['errmsg'] = $errmsg;
		
		$data['doc_id'] = $doc_id;
		
		$data['record'] = $this->admin_model->get_detail( $data['doc_id'] );
		
		$data['qual_records'] = $this->admin_model->get_qual();
		
		$data['stream_records'] = $this->admin_model->get_stream();
		
		$data['college_records'] = $this->admin_model->get_college();
		
		$this->load->view('admin_doc_edit', $data);
	}*/
	
	
	public function doc_edit()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		$doc_id = $this->uri->segment(3);		
			
		// get all the qualification values from the DB
		
		$data = array();
		
		$data['errmsg'] = '';
		
		$data['doc_id'] = $doc_id;
		
		$data['record'] = $this->admin_model->get_detail( $doc_id );
		
		$data['qual_records'] = $this->admin_model->get_qual();
		
		$data['stream_records'] = $this->admin_model->get_stream();
		
		$data['college_records'] = $this->admin_model->get_college();
		
		$this->load->view('admin_doc_edit', $data);	
		
	}
	
	public function update_status()
	{
		$doc_id = $this->uri->segment(3);
		
		$current_status = $this->uri->segment(4);
		
		if( $current_status == 'a' )
		{
			$new_status = 'i';
		}
		else if( $current_status == 'i' ) 
		{
			$new_status = 'a';
		}		

		$qry = " UPDATE doctor
		
		SET doc_status = '".$new_status."'  
		
		WHERE doc_id = ? ";
		
		$res = $this->db->query( $qry , array( $doc_id )  );
		
		if( $res )
		{
			redirect('admin/doc_list');
		}
	}
public function update_status1()
	{
		$doc_id = $this->uri->segment(3);
		
		 
		
		
		$qry = " UPDATE user
		
		SET u_status = 'appr'  
		
		WHERE u_regno = ? ";
		
		$res = $this->db->query( $qry , array( $doc_id )  );
		
		if( $res )
		{
			redirect('admin/doc_list1');
		}
	}
	
	
	/*public function search()
	{
		if( $this->input->post('keyword') != '' )
		{
				
			$data['records'] = $this->admin_model->get_search_result( $this->input->post('keyword') );
					
			$data['keyword'] = $this->input->post('keyword');
			
			if( !empty( $data['records'] ) )
			{					
					
				$data['errmsg'] = "";	
				
			}
			else 
			{
				$data['errmsg'] = "No Search Results Found ! ";
			}
			
			$this->load->view('admin_doc_list', $data);
			
		}
		else 
		{
			redirect('admin/doc_list');
			
			return; 	
		}			
					
	}*/
	
	
	public function search( )// function modified by preeti on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		// start below code added by preeti on 11th apr 14 for black-box testing
		
		if( $offset )
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}		
		
		
		
		if( !$this->alpha_and_space(  $sort_by ) )
		{
			$sort_by = 'u_id';
		}
		
		if( !$this->alpha_and_space( $sort_order ) )
		{
			$sort_order = 'DESC';
		}
		
		// end below code added by preeti on 11th apr 14 for black-box testing
		
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_result( $config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list', $data);
		
	}

public function searchnew( )// function modified by pavan on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		$from = $this->input->post('from');
		$to=$this->input->post('to');
		
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list2'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list2'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_nums();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		
		// start, code added by preeti on 11th apr 14 for black-box testing
		
		if( $offset )
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}
		
		if( !$this->alpha_and_space( $sort_by ) )
		{
			$sort_by = 'u_id';
		}
		
		if( !$this->alpha_and_space( $sort_order ) )
		{
			$sort_order = 'DESC';
		}		
		
		// end, code added by preeti on 11th apr 14 for black-box testing
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_resultnew($config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list2', $data);
		
	}

	public function searchnew3( )// function modified by pavan on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		$from = $this->input->post('from');
		$to=$this->input->post('to');
		
		if( $from != '' )
		{
			if(  !$this->alpha_num_dash( $from ) )
			{
				$from = '';
			}
		}
		if( $to != '' )
		{
			if(  !$this->alpha_num_dash( $to ) )
			{
				$to = '';
			}
		}
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list1'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list1'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_numsp();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_resultnews($config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		// below line added by preeti on 11th jul 14
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_doc_list1', $data);
		
	}

	public function searchnew1( )// function modified by pavan on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		$from = $this->input->post('from');
		$to=$this->input->post('to');
		
		
		if( $from != '' )
		{
			if(  !$this->alpha_num_dash( $from ) )
			{
				$from = '';
			}
		}
		if( $to != '' )
		{
			if(  !$this->alpha_num_dash( $to ) )
			{
				$to = '';
			}
		}
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list21'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list21'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_numsy();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_resultnew($config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list21', $data);
		
	}
public function searchnew2( )// function modified by pavan on 26th feb 14
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		$from = $this->input->post('from');
		$to=$this->input->post('to');
		
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list1'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list1'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_result($config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list1', $data);
		
	}

public function search1()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$url = ''; // to create the base url with all the current uri elements
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// get the segment of the offset in the array with key as offset
		
		$index_offset = array_search('offset', array_keys( $uri_arr ) );
		
		// set the segment value for the offset , added by preeti on 26th feb 14
		
		$config['uri_segment'] = (( intval( $index_offset ) + 1 ) * 2 ) + 2;  
		
		
		$url .= "/offset/";

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/doc_list1'.$url;
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/doc_list1'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_appnum();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		
		$this->pagination->initialize($config);
		
		
		$sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		
		if( isset( $uri_arr['offset'] ) && $uri_arr['offset'] != ''  )
		{
			$offset = $uri_arr['offset'];
		}
		else 
		{
			$offset = 0;	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_search_resultap( $config['per_page'], $offset , $sort_by, $sort_order );
		
		/*if( $errmsg != '' )
		{
			$data['errmsg'] = $errmsg;
		}*/
		
		if( isset( $uri_arr['msg'] ) && $uri_arr['msg'] != '')// passed from the update_doc_password page
		{
			$errmsg = urldecode( $uri_arr['msg'] );	
				
			$data['errmsg'] = $errmsg;
		}
		else 
		{
			$data['errmsg'] = '';	
		}
		
		$this->load->view('admin_doc_list1', $data);
	}
	
	// code commented by preeti on 11th apr 14 for black-box testing
	
	/*public function doc_password()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
			
		$reg_id = $this->uri->segment(3);
		
		
		
		$obj = $this->admin_model->get_name( $reg_id );		
			
		$data['errmsg'] = '';
		
		$data['reg_id'] = $reg_id;	
			//27th
		if( $obj->u_is_name_change == 'y' )
		{
			$data['name'] = $obj->new_fname.' '.$obj->new_mname.' '.$obj->new_lname;	
		}
		else 
		{
			$data['name'] = $obj->u_fname.' '.$obj->u_mname.' '.$obj->u_lname;	
		}	
			
		//$data['name'] = $obj->name;// line commented by preeti on 27th feb 14
		
		$data['reg_regno'] = $obj->reg_regno;
			
		$this->load->view('admin_doc_password', $data);
	}*/
	
	// parameter added by preeti on 24th apr 14 for black-box testing
		
	public function doc_password( )
	{
		
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
			
		$reg_id = $this->uri->segment(3);
		
		$errmsg = $this->uri->segment(4); // added by preeti on 24th apr 14 for manual testing
			
			
		$obj = $this->admin_model->get_name( $reg_id );		
		
		if( $obj )
		{
			// added by preeti on 24th apr 14 for manual testing		
			
			//$data['errmsg'] = '';
			
			if( $errmsg )
			{
				$errmsg = urldecode( $errmsg );
					
				if( !is_numeric( $errmsg ) )
				{
					$data['errmsg'] = $errmsg; // above line commented and this added by preeti on 24th apr 14
				}					
				
			}
			
			
		
			$data['reg_id'] = $reg_id;	
				//27th
			if( $obj->u_is_name_change == 'y' )
			{
				$data['name'] = $obj->new_fname.' '.$obj->new_mname.' '.$obj->new_lname;	
			}
			else 
			{
				$data['name'] = $obj->u_fname.' '.$obj->u_mname.' '.$obj->u_lname;	
			}	
				
			//$data['name'] = $obj->name;// line commented by preeti on 27th feb 14
			
			$data['reg_regno'] = $obj->reg_regno;
			
			// below line added by preeti on 21st apr 14 for manual testing
			
			$data['admin_random'] = $this->session->userdata('admin_random');
				
			$this->load->view('admin_doc_password', $data);			
			
		}
		else
		{
			redirect('admin/doc_list');	
		}
			
		
	}
	
	
	public function update_doc_password()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$reg_id = '';
		
		$flag_redirect = FALSE;
		
		/*if( $this->uri->segment(3) )
		{
			$reg_id = $this->uri->segment(3);
		}
		else*/ // commented by preeti on 24th apr 14 for manual testing
		{
			$reg_id = $this->input->post('reg_id');
		}
		
			
			
		
		$obj = $this->admin_model->get_name( $reg_id );	
		
		if( $obj )
		{
			$reg_regno = $obj->reg_regno;	
			
			if( $obj->u_is_name_change == 'y' )
			{
				$name = $obj->new_fname.' '.$obj->new_mname.' '.$obj->new_lname;
			}
			else 
			{
				$name = $obj->u_fname.' '.$obj->u_mname.' '.$obj->u_lname;
			}
			
		}
		else 
		{
			
			$name = '';
		
			$reg_regno = '';	
			
		}					
		
		
		//$name = $obj->name; // line commented by preeti on 5th mar 14
		
		$reg_regno = $obj->reg_regno;	
							
		// start, code modified for black-box testing by preeti on 26th mar 14
		
		$this->form_validation->set_rules('reg_pass_encode', 'Password', 'required');
		
		$this->form_validation->set_rules('cpassword_encode', 'Confirm Password', 'required|matches[reg_pass_encode]'); 
		
		// end, code modified for black-box testing by preeti on 26th mar 14
		
		$this->form_validation->set_message('matches', 'The two passwords do not match');
		
		
		
		if( $this->form_validation->run() == TRUE )
		{	
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
				// shoot an update query here
		
				$res = $this->admin_model->update_doc_password();	
				
				if($res)
				{
					$errmsg = "Password Updated Successfully ";
					
					//$flag_redirect = TRUE;
					
					// above line commented and below code added by preeti on 24th apr 14 for manual testing
						
					$errmsg = urlencode( $errmsg );
											
					redirect('admin/doc_password/'.$reg_id.'/'.$errmsg);
				}
				else 
				{
					$errmsg = "Password couldn't be Updated";
					
					
					// below code added by preeti on 22nd apr 14 for manual testing
									
					$errmsg = urlencode( $errmsg );
																		
					$title = urlencode( "Change Password" );
																		
					redirect('admin/show_message/'.$title.'/'.$errmsg);					
				}
				
			}
			else 
			{
				$errmsg = "Password couldn't be Updated";
				
				// below code added by preeti on 22nd apr 14 for manual testing
									
				$errmsg = urlencode( $errmsg );
																		
				$title = urlencode( "Change Password" );
																		
				redirect('admin/show_message/'.$title.'/'.$errmsg);	
			}				
			
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('admin/doc_password/'.$reg_id.'/'.$errmsg);
						
			exit;					
		}		
		
		
		$data['errmsg'] = $errmsg;
		
		$data['reg_id'] = $reg_id;
		
		$data['name'] = $name;
		
		$data['reg_regno'] = $reg_regno;
		
		$data['admin_random'] = $this->session->userdata('admin_random');	
					
		if( $flag_redirect )
		{
						
			$this->doc_list( $errmsg );	
		}
		else 
		{	
			
			
			$this->load->view('admin_doc_password', $data);	
		}		
		
	}
	
	
	public function update_admin_password()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		// start, below code modified by preeti on 26th mar 14 for black-box testing
		
		$this->form_validation->set_rules('opassword_encode', 'Old Password', 'trim|required');
		
		$this->form_validation->set_rules('admin_password_encode', 'Password', 'trim|required');
		
		$this->form_validation->set_rules('cpassword_encode', 'Confirm Password', 'trim|required|matches[admin_password_encode]'); 
		
		// end,  code modified by preeti on 26th mar 14 for black-box testing
		
		$this->form_validation->set_message('matches', 'The two passwords do not match');
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
				
				// shoot an update query here
		
				if( $this->admin_model->check_old_password() )
				{
					
					// start, code added by preeti on 22nd apr 14 for manual testing
					
					$old_password = $this -> input -> post('opassword_encode');// hashed md5 value
							
					$new_password = $this -> input -> post('admin_password_encode'); // only md5 value
							
										
					if( !$this->admin_model->check_old_new_pass_same( $old_password, $new_password ) )// end, code added by preeti on 22nd apr 14 for manual testing
					{
					
						$res = $this->admin_model->update_admin_password();	
					
						if($res)
						{
							// below line added by preeti on 26th apr 14
							
							$this->admin_model->add_pass( 'passchange' );
							
							$errmsg = "Admin Password Updated Successfully ";
							
							// below code added by preeti on 22nd apr 14 for manual testing
									
							$errmsg = urlencode( $errmsg );
													
							$title = urlencode( "Change Password" );
													
							redirect('admin/show_message/'.$title.'/'.$errmsg);
							
						}
						else 
						{
							// below line added by preeti on 26th apr 14
							
							$this->admin_model->add_pass( 'passchangefail' );
														
							$errmsg = "Admin Password couldn't be Updated";
							
							// below code added by preeti on 22nd apr 14 for manual testing
									
							$errmsg = urlencode( $errmsg );
													
							$title = urlencode( "Change Password" );
													
							redirect('admin/show_message/'.$title.'/'.$errmsg);
						}
					
					}
					else// below code added by preeti on 22nd apr 14 for manual testing
					{
						$errmsg = "New Password cannot be same as the Old Password";
						
						// below code added by preeti on 24th apr 14 for manual testing
						
						$errmsg = urlencode( $errmsg );
												
						redirect('admin/admin_password/'.$errmsg);
					}			
					
				}
				else 
				{
					$errmsg = "Old Password is Incorrect";
					
					// below code added by preeti on 24th apr 14 for manual testing
						
					$errmsg = urlencode( $errmsg );
												
					redirect('admin/admin_password/'.$errmsg);					
					
				}
				
			}
			else 
			{
				$errmsg = "Admin Password couldn't be Updated";
				
				// below code added by preeti on 22nd apr 14 for manual testing
								
				$errmsg = urlencode( $errmsg );
												
				$title = urlencode( "Change Password" );
												
				redirect('admin/show_message/'.$title.'/'.$errmsg);
			}					
				
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('admin/admin_db_password/'.$errmsg);
						
			exit;					
		}		
		
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$data['errmsg'] = $errmsg;
		
		$data['salt'] = $this->random_num();// code added by preeti on 28th mar 14 for black-box testing
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'admin_salt' => $data['salt'] );
						
			$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		$this->load->view('admin_password', $data);
	}

	public function update_admin_email()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$this->form_validation->set_rules('admin_email', 'Email', 'trim|required|valid_email');
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
				// shoot an update query here
		
				$res = $this->admin_model->update_admin_email();	
				
				if($res)
				{
					$errmsg = "Admin Email Updated Successfully ";
					
					// below code added by preeti on 22nd apr 14 for manual testing
								
					$errmsg = urlencode( $errmsg );
											
					$title = urlencode( "Change Admin Email" );
											
					redirect('admin/show_message/'.$title.'/'.$errmsg);
					
				}
				else 
				{
					$errmsg = "Admin Email couldn't be Updated";
					
					// below code added by preeti on 22nd apr 14 for manual testing
								
					$errmsg = urlencode( $errmsg );
											
					$title = urlencode( "Change Admin Email" );
											
					redirect('admin/show_message/'.$title.'/'.$errmsg);
					
				}			
			
			}
			else 
			{
				$errmsg = "Admin Email couldn't be Updated";
				
				// below code added by preeti on 22nd apr 14 for manual testing
								
				$errmsg = urlencode( $errmsg );
											
				$title = urlencode( "Change Admin Email" );
											
				redirect('admin/show_message/'.$title.'/'.$errmsg);
					
			}
			
		}
		else if( validation_errors( ) ) // else added by preeti on 24th apr 14 for manual testing
		{		
					
			$errmsg = $this->form_validation->error_array();
							
			$errmsg = implode( ' ', $errmsg );
					
			$errmsg = urlencode($errmsg);
					
			redirect('admin/admin_email/'.$errmsg);
						
			exit;					
		}
		
		
		
		$email = $this->admin_model->get_admin_email();		
		
		if( $email )
		{
			$data['admin_email'] = $email;
			
		}
		else 
		{
			$data['admin_email'] = '';			
		}
				
		
		$data['errmsg'] = $errmsg;
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_email', $data);
	}
	
	public function admin_password( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		
		// added by preeti on 24th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );
				
			$data['errmsg'] = $errmsg;
		}	
		else 
		{
			$data['errmsg'] = '';
		}
				
			
		//$data['errmsg'] = ''; // commented by preeti on 24th apr 14 for manual testing
		
		$data['salt'] = $this->random_num();// code added by preeti on 28th mar 14 for black-box testing
		
		// start, code added by preeti on 22nd apr 14 for manual testing
				
		// add the same value in the session variable
						
		$sess_arr = array(
						
			'admin_salt' => $data['salt'] );
						
		$this->session->set_userdata( $sess_arr );
								
		// end, code added by preeti on 22nd apr 14 for manual testing	
		
		
		// below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_password', $data);
	}
	
	// parameter added by preeti on 24th apr 14 for manual testing
	
	public function admin_email( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		// get the current email id if any
		
		$email = $this->admin_model->get_admin_email();	
		
		// added by preeti on 24th apr 14 for manual testing		
			
		if( $errmsg )
		{
			$errmsg = urldecode( $errmsg );
				
			
		}	
			
		
		if( $email )
		{
			$data['admin_email'] = $email;	
		
			//$data['errmsg'] = '';
			
			$data['errmsg'] = $errmsg; // above line commented and this added by preeti on 24th apr 14
		}
		else 
		{
			$data['admin_email'] = '';
			
			$data['errmsg'] = 'Currently No Admin Email is Set';	
		}
		
		$data['admin_random'] = $this->session->userdata('admin_random');		
		
		$this->load->view('admin_email', $data);
	}
			
	
	public function logout()
	{
		// below if added by preeti on 22nd apr 14 for manual testing
		
		if( $this->uri->segment(3) == $this->session->userdata('admin_random') )
		{
			
			// below code added by preeti on 26th apr 14
		
			$this->admin_model->add_logout( );

			$data = array(
				
				'username' => '',
				
				'is_logged_in' => '',
				
				'admin_random' => '', // added by preeti on 21st apr 14 for manual testing
				
				'ci_session' => '', // line added by preeti on 22nd apr 14 for manual testing
				
				'admin_salt' => '' // line added by preeti on 22nd apr 14 for manual testing
				
				);
				
				$this->session->unset_userdata($data);
				
				// delet cookie ci_session
				
				// line added by preeti on 22nd apr 14 for manual testing
				
				setcookie( "ci_session", '', time()-(60*60*24*30) );
				
				
			// start, code added by preeti on 31st mar 14 for black-box testing
			
			$sessid = '';
			
			while (strlen($sessid) < 32)
			{
				$sessid .= mt_rand(0, mt_getrandmax());
			}	
			
			// To make the session ID even more secure we'll combine it with the user's IP
			$sessid .= $this->input->ip_address();
	
			$data = array(
								'session_id'	=> md5(uniqid($sessid, TRUE))							
								); 
			
			$this->session->set_userdata($data);
			
			// end, code added by preeti on 31st mar 14 for black-box testing	
			
			$this->session->sess_destroy(); // added by preeti on 24th apr 14 for manual testing	
				
			redirect('admin/index');
			
		}		
		
	}
	
	public function not_list( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$this->table->set_heading('S.No', 'Title', 'Date', 'Author', 'Edit', 'Delete');
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/not_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/not_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_num_not();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('not_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		// below code added by preeti on 11th apr 14
		
		if( $offset )  
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}
		
		$data['keyword'] = '';
		
		$data['doc_status'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_not_limit( $config['per_page'], $offset);
		
		// below lines modified by preeti on 6th mar 14
		
		if( $errmsg != '' && !is_numeric( $errmsg ) && $this->alpha_and_space( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;
		}
		else
		{
			$data['errmsg'] = '';
		}
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_not_list', $data);
	} 
	
	public function ck()
	{
		$this->load->view('admin_ck');

	}

	/*public function not_save()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_title', 'Title', 'required|callback__alpha_dash_space');
		
		$this->form_validation->set_rules('not_date', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('not_author', 'Author', 'trim|required|callback_alpha_and_space'); // code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_type', 'Type', 'required');
		
		if( $this->input->post('not_type') == 'c' )
		{
			$this->form_validation->set_rules('not_content', 'Content', 'trim|required');
		}
		
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('not_type') == 'c' ) // for only content
			{
				$res = $this->admin_model->save_not( );
			
				if( $res )
				{
					$errmsg = " Notification saved successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = " Notification couldnot be saved ";	
				}	
			}
			else // for only file
			{
				$config['upload_path'] = './uploads/not/';
			
				$config['allowed_types'] = 'pdf';
					
				$config['max_size'] = '500'; // in kb
					
				//$config['max_width'] = '0'; // in pixel
					
				//$config['max_height'] = '0'; // in pixel
					
				$this->upload->initialize($config);				
					
				if(! $this->upload->do_upload('not_link') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$result_upload_arr = $this->upload->data();
						
					$file_name = $result_upload_arr['file_name'];
						
					$res = $this->admin_model->save_not( $file_name );
				
					if( $res )
					{
						$errmsg = " Notification saved successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = " Notification couldnot be saved ";	
					}	
						
				}				
					
			}		
				
		}
		
		$data['errmsg'] = $errmsg;
		
		if( $flag_redirect )
		{
			$this->not_list( $errmsg );					
		}
		else
		{
			$this->load->view('admin_not_add', $data);	
		}		
	}*/ // code commented by preeti on 1st apr 14 for black-box testing		

	// code added by preeti on 1st apr 14 for black-box testing
	
	public function not_save()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_title', 'Title', 'trim|required');
		
		$this->form_validation->set_rules('not_date', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('not_author', 'Author', 'trim|required|callback_alpha_and_space'); // code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_type', 'Type', 'required');
		
		if( $this->input->post('not_type') == 'c' )
		{
			$this->form_validation->set_rules('not_content', 'Content', 'trim|required');
		}
		
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			//below line added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
			
			
				if( $this->input->post('not_type') == 'c' ) // for only content
				{
					$res = $this->admin_model->save_not( );
				
					if( $res )
					{
						$errmsg = " Notification saved successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = " Notification couldnot be saved ";	
					}	
				}
				else // for only file
				{
					$config['upload_path'] = './uploads/not/';
				
					$config['allowed_types'] = 'pdf';
						
					$config['max_size'] = '500'; // in kb
						
					//$config['max_width'] = '0'; // in pixel
						
					//$config['max_height'] = '0'; // in pixel
						
					$this->upload->initialize($config);				
						
					if(! $this->upload->do_upload('not_link') )
					{
						$errmsg = $this->upload->display_errors();
					}
					else
					{
						$result_upload_arr = $this->upload->data();
							
						$file_name = $result_upload_arr['file_name'];
						
						
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
							
							// read the file and verify if it is actually a pdf file
							
							// if it is not a pdf file then delete the file and show the message as file not pdf
							
							//$path = './uploads/not/'.$file_name;
							
							$path = 'uploads/not/'.$file_name;
							
							$this->load->helper('file');
							
							$string = read_file( $path );
							
							if( $string )
							{
								if( strpos( $string , '%PDF' ) !== FALSE )
								{
									// proceed with the saving of the data
									
									$res = $this->admin_model->save_not( $file_name );
					
									if( $res )
									{
										$errmsg = " Notification saved successfully ";
										
										$flag_redirect = TRUE;
									}
									else 
									{
										$errmsg = " Notification couldnot be saved ";	
									}						
									
								}
								else 
								{
									// delete the uploaded file, dont proceed and show the error message
									
									$file_path = "uploads/not/" . $file_name;
									
									if(file_exists( $file_path )) 
									{						
										unlink( $file_path );
									}
									
									$errmsg = "Uploaded File is not valid ";
									
								}
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
									
								$file_path = "uploads/not/" . $file_name;
									
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
									
								$errmsg = "Uploaded File is not valid ";		
							}						
							
							
							// end, code added by preeti on 1st apr 14 for black-box testing	
					}				
						
				}

			}
			else
			{
				$errmsg = " Notification couldnot be saved ";	
			}		
				
		}
		
		$data['errmsg'] = $errmsg;
		
		// below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		if( $flag_redirect )
		{
			$this->not_list( $errmsg );					
		}
		else
		{
			$this->load->view('admin_not_add', $data);	
		}		
	}
	
	
	public function not_add()
	{
		$data['errmsg'] = '';
		
		
		// below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		$this->load->view('admin_not_add', $data);
	}
	
	// below function modified on 27th mar 14 by preeti for black-box testing
	
	public function not_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('doc_ids[]', 'Notification ID', 'required|integer|max_length[11]');// code added for black-box testing by preeti on 27th mar 14
		
		if( $this->form_validation->run() == TRUE )// code added for black-box testing by preeti on 27th mar 14
		{
			// below line added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
		
				$not_id_arr = $this->input->post('doc_ids');
				
				$res = $this->admin_model->del_not( $not_id_arr );
			}
			
		
		}
		
		redirect('admin/not_list');
		
	}
	
	// below code added on 27th mar 14 by preeti for black-box testing
	
	function alpha_dash_space($str) // only for alpha and space
	{
	    return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
	}
	
	
	/*public function not_update()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_title', 'Title', 'trim|required|callback_alpha_and_space'); // line modified by preeti on 1st apr 14 for black-box testing
		
		$this->form_validation->set_rules('not_date', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('not_author', 'Author', 'trim|required|callback_alpha_and_space'); // code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_type', 'Type', 'required');
		
		if( $this->input->post('not_type') == 'c' )
		{
			$this->form_validation->set_rules('not_content', 'Content', 'trim|required');
		}
	
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('not_type') == 'c' ) // for only content
			{
				$res = $this->admin_model->update_not();
			
				if( $res )
				{
					$errmsg = " Notification updated successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = " Notification couldnot be updated ";					
				}	
			}
			else 
			{
			 	if( isset( $_FILES['not_link'] ) && $_FILES['not_link']['size'] > 0   )
				{ 
					$config['upload_path'] = './uploads/not/';
				
					$config['allowed_types'] = 'pdf';
						
					$config['max_size'] = '500'; // in kb
						
					//$config['max_width'] = '0'; // in pixel
						
					//$config['max_height'] = '0'; // in pixel
						
					$this->upload->initialize($config);				
						
					if(! $this->upload->do_upload('not_link') )
					{
						$errmsg = $this->upload->display_errors();
					}
					else
					{
						$result_upload_arr = $this->upload->data();
							
						$file_name = $result_upload_arr['file_name'];												
					}
				}	
				
				$res = $this->admin_model->update_not( $file_name );
					
				if( $res )
				{
					$errmsg = " Notification updated successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = " Notification couldnot be updated ";	
				}			
					
			}		
				
		}
		
		$data['record'] = $this->admin_model->not_detail( $this->input->post('not_id') );
		
		$data['errmsg'] = $errmsg;
		
		if( $flag_redirect )
		{
			$this->not_list( $errmsg );					
		}
		else 
		{
			$this->load->view('admin_not_edit', $data);
		}				
		
	}*/	// code commented by preeti on 1st apr 14 for black-box testing

	// below code added by preeti on 1st apr 14 for black-box testing
	
	public function not_update()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_title', 'Title', 'trim|required|alpha_dash_space'); // line modified by preeti on 1st apr 14 for black-box testing
		
		$this->form_validation->set_rules('not_date', 'Date', 'trim|required');
		
		$this->form_validation->set_rules('not_author', 'Author', 'trim|required|callback_alpha_and_space'); // code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('not_type', 'Type', 'required');
		
		if( $this->input->post('not_type') == 'c' )
		{
			$this->form_validation->set_rules('not_content', 'Content', 'trim|required');
		}
	
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			//below if added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
			
				if( $this->input->post('not_type') == 'c' ) // for only content
				{
					$res = $this->admin_model->update_not();
				
					if( $res )
					{
						$errmsg = " Notification updated successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = " Notification couldnot be updated ";					
					}	
				}
				else 
				{
				 	if( isset( $_FILES['not_link'] ) && $_FILES['not_link']['size'] > 0   )
					{ 
						$config['upload_path'] = './uploads/not/';
					
						$config['allowed_types'] = 'pdf';
							
						$config['max_size'] = '500'; // in kb
							
						//$config['max_width'] = '0'; // in pixel
							
						//$config['max_height'] = '0'; // in pixel
							
						$this->upload->initialize($config);				
							
						if(! $this->upload->do_upload('not_link') )
						{
							$errmsg = $this->upload->display_errors();
						}
						else
						{
							$result_upload_arr = $this->upload->data();
								
							$file_name = $result_upload_arr['file_name'];	
							
							// start, code added by preeti on 1st apr 14 for black-box testing
							
							// read the file and verify if it is actually a pdf file
							
							// if it is not a pdf file then delete the file and show the message as file not pdf
							
							//$path = './uploads/not/'.$file_name;
							
							$path = 'uploads/not/'.$file_name;
							
							$this->load->helper('file');
							
							$string = read_file( $path );
							
							if( $string )
							{
								if( strpos( $string , '%PDF' ) !== FALSE )
								{
									// proceed with the saving of the data
									
									$res = $this->admin_model->update_not( $file_name );
									
									if( $res )
									{
										$errmsg = " Notification updated successfully ";
										
										$flag_redirect = TRUE;
									}
									else 
									{
										$errmsg = " Notification couldnot be updated ";	
									}						
									
								}
								else 
								{
									// delete the uploaded file, dont proceed and show the error message
									
									$file_path = "uploads/not/" . $file_name;
									
									if(file_exists( $file_path )) 
									{						
										unlink( $file_path );
									}
									
									$errmsg = "Uploaded File is not valid ";
									
								}
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
									
								$file_path = "uploads/not/" . $file_name;
									
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
									
								$errmsg = "Uploaded File is not valid ";		
							}						
							
							
							// end, code added by preeti on 1st apr 14 for black-box testing
													
						}
					}
					else 
					{
		
						$res = $this->admin_model->update_not( );
						
						if( $res )
						{
							$errmsg = " Notification updated successfully ";
							
							$flag_redirect = TRUE;
						}
						else 
						{
							$errmsg = " Notification couldnot be updated ";	
						}
		
					}						
						
				}

			}
			else
			{
				$errmsg = " Notification couldnot be updated ";	
			}			
				
		}
		
		$data['record'] = $this->admin_model->not_detail( $this->input->post('not_id') );
		
		$data['errmsg'] = $errmsg;
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		if( $flag_redirect )
		{
			$this->not_list( $errmsg );					
		}
		else 
		{
			$this->load->view('admin_not_edit', $data);
		}				
		
	}	
	

	public function not_edit()
	{
		$not_id = $this->uri->segment(3);
		
		$data['record'] = $this->admin_model->not_detail( $not_id );
		
		$data['errmsg'] = '';
		
		$data['admin_random'] = $this->session->userdata('admin_random');
			
		$this->load->view('admin_not_edit', $data);
	}
	
	public function res_list( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/res_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/res_list'.$url;
		
		
		$config['total_rows'] = $this->admin_model->get_num_res();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('res_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		if( $offset )
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}
		
		$data['keyword'] = '';
		
		$data['doc_status'] = '';
		
		// below lines modified by preeti on 6th mar 14
		
		if( $errmsg != '' && !is_numeric( $errmsg ) && $this->alpha_and_space( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;
		}
		else
		{
			$data['errmsg'] = '';
		}
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_res_limit( $config['per_page'], $offset);
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_res_list', $data);
	}
	
	// below code commented by preeti on 1st apr 14 for black-box testing
	
	/*public function res_save()
	{		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		$this->form_validation->set_rules('res_title', 'Title', 'required|callback__alpha_dash_space');// code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('res_date', 'Date', 'trim|required');
		
		if( $this->form_validation->run() == TRUE )
		{
			// save here
					
			$this->load->library('upload');			
			
			if( isset( $_FILES['res_file_path'] ) && $_FILES['res_file_path']['size'] > 0   )
			{
				$config['upload_path'] = './uploads/result/';
			
				$config['allowed_types'] = 'pdf';
				
				$config['max_size'] = '500'; // in kb
				
				$config['max_width'] = '0'; // in pixel
				
				$config['max_height'] = '0'; // in pixel
				
				$this->upload->initialize($config);				
				
				if(! $this->upload->do_upload('res_file_path') )
				{
					$errmsg = $this->upload->display_errors();
				}
				else
				{
					$result_upload_arr = $this->upload->data();
					
					$result_file_name = $result_upload_arr['file_name'];
					
					
					$res = $this->admin_model->save_res( $result_file_name );
			
					if( $res )
					{
						$errmsg = " Result saved successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = " Result couldnot be saved ";	
					}	
					
				}			
				
			}
			else 
			{
					$errmsg = " Please upload the result file ";
			}		
					
				
		}
		
		$data['errmsg'] = $errmsg;
		
		if( $flag_redirect )
		{
			$this->res_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_res_add', $data);	
		}	
		
	}*/	
	
	// below code added by preeti on 1st apr 14 for black-box testing
	
	public function res_save()
	{		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		$this->form_validation->set_rules('res_title', 'Title', 'required|callback__alpha_dash_space');// code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('res_date', 'Date', 'trim|required|callback_date_validate');// code modified on 12th apr 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{		
			
				// save here
						
				$this->load->library('upload');			
				
				if( isset( $_FILES['res_file_path'] ) && $_FILES['res_file_path']['size'] > 0   )
				{
					$config['upload_path'] = './uploads/result/';
				
					$config['allowed_types'] = 'pdf';
					
					$config['max_size'] = '500'; // in kb
					
					$config['max_width'] = '0'; // in pixel
					
					$config['max_height'] = '0'; // in pixel
					
					$this->upload->initialize($config);				
					
					if(! $this->upload->do_upload('res_file_path') )
					{
						$errmsg = $this->upload->display_errors();
					}
					else
					{
						$result_upload_arr = $this->upload->data();
						
						$result_file_name = $result_upload_arr['file_name'];
						
					
					
						// start, code added by preeti on 1st apr 14 for black-box testing
							
						// read the file and verify if it is actually a pdf file
							
						// if it is not a pdf file then delete the file and show the message as file not pdf
							
						$path = 'uploads/result/'.$result_file_name;
							
						$this->load->helper('file');
							
						$string = read_file( $path );
					
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																
								// shoot an update query here
						
								
								$res = $this->admin_model->save_res( $result_file_name );
				
								if( $res )
								{
									$errmsg = " Result saved successfully ";
									
									$flag_redirect = TRUE;
								}
								else 
								{
									$errmsg = " Result couldnot be saved ";	
								}	
																		
									
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
									
								$file_path = "uploads/not/" . $file_name;
									
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
									
								$errmsg = "Uploaded File is not valid ";
									
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
									
							$file_path = "uploads/not/" . $file_name;
									
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
									
							$errmsg = "Uploaded File is not valid ";		
						}						
							
							
						// end, code added by preeti on 1st apr 14 for black-box testing	
						
					}			
					
				}
				else 
				{
						$errmsg = " Please upload the result file ";
				}		
			
			}
			else 
			{
	
			}
				
		}
		
		$data['errmsg'] = $errmsg;
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		if( $flag_redirect )
		{
			$this->res_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_res_add', $data);	
		}	
		
	}
		

	public function res_add()
	{
		$data['errmsg'] = '';
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		$this->load->view('admin_res_add', $data);
	}
	
	public function res_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		//$res_id = $this->uri->segment(3);
		
		// below code added by preeti on 11th apr 14 for black-box testing
		
		if( $this->input->post( 'del' ) != '' && $this->input->post( 'del' ) == 'Delete'  )
		{
			//below line added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
			
				$res_id_arr = $this->input->post('doc_ids');
			
				$res = $this->admin_model->del_res( $res_id_arr );
				
			}		
		}
			
		redirect('admin/res_list');
		
	}
		
	
	// below code commented by preeti on 1st apr 14 for black-box testing
	
	/*public function res_update()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
			
		
		$errmsg = '';
		
		$res_id = '';
		
		$flag_redirect = FALSE;
		
		if( $this->uri->segment(3) != '' )
		{
			$res_id = $this->uri->segment(3);	
		}
		else if( $this->input->post('res_id') != '' )
		{
			$res_id = $this->input->post('res_id');	
		}
		
		$this->form_validation->set_rules('res_title', 'Title', 'trim|alpha_numeric|required');// code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('res_date', 'Date', 'trim|required');
		
		if( $this->form_validation->run() == TRUE )
		{
			
			$data = array();
		
			$result_file_name = '';
			
			$this->load->library('upload');
			
			
			if( isset( $_FILES['res_file_path'] ) && $_FILES['res_file_path']['size'] > 0   )
			{
				$config['upload_path'] = './uploads/result/';
			
				$config['allowed_types'] = 'pdf';
				
				$config['max_size'] = '500'; // in kb
				
				//$config['max_width'] = '0'; // in pixel
				
				//$config['max_height'] = '0'; // in pixel
				
				$this->upload->initialize($config);
				
				$previous_file = $this->admin_model->get_prev_result( $res_id );// get the previous file name
				
				
				if(! $this->upload->do_upload('res_file_path') )
				{
					$errmsg = $this->upload->display_errors();					
					
				}
				else
				{
					$resume_upload_arr = $this->upload->data();
					
					$result_file_name = $resume_upload_arr['file_name'];
					
					
					// shoot an update query here
			
					$res = $this->admin_model->update_res( $res_id, $result_file_name );	
					
					if($res)
					{
						if( !empty( $result_file_name ) )
						{
						
							$url = "uploads/result/".$previous_file; // path to the previous file
							
							if(file_exists( $url ))
							{
								unlink( $url );
							}
						}
						
						$errmsg = "Result Updated Successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = "Result couldn't be Updated";
					}								
						
				}			
				
				
			}
			else 
			{
				$res = $this->admin_model->update_res( $res_id, $result_file_name );
				
				if($res)
				{
					$errmsg = "Result Updated Successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = "Result couldn't be Updated";
				}
			}					
			
		}
		
		
		$data['errmsg'] = $errmsg;
		
		$data['res_id'] = $res_id;
		
		$data['record'] = $this->admin_model->res_detail( $res_id );
		
		if( $flag_redirect )
		{
			$this->res_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_res_edit', $data);	
		}
		
		
	}*/

	// below code added by preeti on 1st apr 14 for black-box testing

	public function res_update()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
			
		
		$errmsg = '';
		
		$res_id = '';
		
		$flag_redirect = FALSE;
		
		if( $this->uri->segment(3) != '' )
		{
			$res_id = $this->uri->segment(3);	
		}
		else if( $this->input->post('res_id') != '' )
		{
			$res_id = $this->input->post('res_id');	
		}
		
		$this->form_validation->set_rules('res_title', 'Title', 'trim|required|callback__alpha_dash_space');// code modified on 1st apr 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('res_date', 'Date', 'trim|required|callback_date_validate'); // code modified by preeti on 12th apr 14
		
		if( $this->form_validation->run() == TRUE )
		{
			//below line added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
			
			
				$data = array();
			
				$result_file_name = '';
				
				$this->load->library('upload');
				
				
				if( isset( $_FILES['res_file_path'] ) && $_FILES['res_file_path']['size'] > 0   )
				{
					$config['upload_path'] = './uploads/result/';
				
					$config['allowed_types'] = 'pdf';
					
					$config['max_size'] = '500'; // in kb
					
					//$config['max_width'] = '0'; // in pixel
					
					//$config['max_height'] = '0'; // in pixel
					
					$this->upload->initialize($config);
					
					$previous_file = $this->admin_model->get_prev_result( $res_id );// get the previous file name
					
					
					if(! $this->upload->do_upload('res_file_path') )
					{
						$errmsg = $this->upload->display_errors();					
						
					}
					else
					{
						$resume_upload_arr = $this->upload->data();
						
						$result_file_name = $resume_upload_arr['file_name'];					
						
						
						// start, code added by preeti on 1st apr 14 for black-box testing
							
						// read the file and verify if it is actually a pdf file
							
						// if it is not a pdf file then delete the file and show the message as file not pdf
							
						$path = 'uploads/result/'.$result_file_name;
							
						$this->load->helper('file');
							
						$string = read_file( $path );
					
						if( $string )
						{
							if( strpos( $string , '%PDF' ) !== FALSE )
							{
								// proceed with the saving of the data
																
								// shoot an update query here
						
								$res = $this->admin_model->update_res( $res_id, $result_file_name );	
								
								if($res)
								{
									if( !empty( $result_file_name ) )
									{
									
										$url = "uploads/result/".$previous_file; // path to the previous file
										
										if(file_exists( $url ))
										{
											unlink( $url );
										}
									}
									
									$errmsg = "Result Updated Successfully ";
									
									$flag_redirect = TRUE;
								}
								else 
								{
									$errmsg = "Result couldn't be Updated";
								}											
									
							}
							else 
							{
								// delete the uploaded file, dont proceed and show the error message
									
								$file_path = "uploads/not/" . $file_name;
									
								if(file_exists( $file_path )) 
								{						
									unlink( $file_path );
								}
									
								$errmsg = "Uploaded File is not valid ";
									
							}
						}
						else 
						{
							// delete the uploaded file, dont proceed and show the error message
									
							$file_path = "uploads/not/" . $file_name;
									
							if(file_exists( $file_path )) 
							{						
								unlink( $file_path );
							}
									
							$errmsg = "Uploaded File is not valid ";		
						}						
							
							
						// end, code added by preeti on 1st apr 14 for black-box testing										
							
					}			
					
					
				}
				else 
				{
					$res = $this->admin_model->update_res( $res_id, $result_file_name );
					
					if($res)
					{
						$errmsg = "Result Updated Successfully ";
						
						$flag_redirect = TRUE;
					}
					else 
					{
						$errmsg = "Result couldn't be Updated";
					}
				}
				
			}
			else
			{
				$errmsg = "Result couldn't be Updated";	
			}						
			
		}
		
		
		$data['errmsg'] = $errmsg;
		
		$data['res_id'] = $res_id;
		
		$data['record'] = $this->admin_model->res_detail( $res_id );
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		if( $flag_redirect )
		{
			$this->res_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_res_edit', $data);	
		}
		
		
	}	
		
		


	public function res_edit()
	{
		$res_id = $this->uri->segment(3);
		
		$data['record'] = $this->admin_model->res_detail( $res_id );
		
		$data['errmsg'] = '';
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
			
		$this->load->view('admin_res_edit', $data);
	}
	
	public function form_list( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$this->table->set_heading('S.No', 'Title', 'Date', 'Author', 'Edit', 'Delete');
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/form_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/form_list'.$url;
		
		
		$config['total_rows'] = $this->admin_model->get_num_form();// line modified by preeti on 5th mar 14
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		// below code added by preeti on 11th apr 14
		
		if( $offset )
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}			

		}
		
		$data['per_page'] = $config['per_page'];
		
		// below if condition modified by preeti on 11th apr 14
		
		if( $errmsg != '' && $this->alpha_and_space( $errmsg )  )
		{
			$data['errmsg'] = $errmsg;
		}
		
		$data['records'] = $this->admin_model->get_form_limit( $config['per_page'], $offset);
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_form_list', $data);
	} 

	public function form_save()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('form_title', 'Title', 'trim|required|callback__alpha_dash_space');
		
		$this->form_validation->set_rules('form_subject', 'Email Subject', 'trim|required|callback__alpha_dash_space');
		
		$this->form_validation->set_rules('form_content', 'Email Body', 'trim|required|callback__alpha_dash_space'); 
		
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	
			
			
				// save here
				
				$res = $this->admin_model->save_form();
				
				if( $res )
				{
					$errmsg = " Format saved successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = " Format couldnot be saved ";	
				}
			
			}
			else 
			{
				$errmsg = " Format couldnot be saved ";
			}	
		}
		
		$data['errmsg'] = $errmsg;
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		if( $flag_redirect )
		{
			$this->form_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_form_add', $data);	
		}
		
		
	}		

	public function form_add()
	{
		$data['errmsg'] = '';
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		$this->load->view('admin_form_add', $data);
	}
	
	// below function modified on 27th mar 14 by preeti for black-box testing
	
	public function form_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('doc_ids[]', 'Format ID', 'required|integer|max_length[11]');// code added for black-box testing by preeti on 27th mar 14
		
		if( $this->form_validation->run() == TRUE )// code added for black-box testing by preeti on 27th mar 14
		{
			//below line added by preeti on 21st apr 14 for manual testing
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{		
		
				$form_id_arr = $this->input->post('doc_ids');
				
				$res = $this->admin_model->del_form( $form_id_arr );
			}
		}
		
		redirect('admin/form_list');			
		
	}
	
	
	public function form_update()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		$errmsg = '';
		
		$flag_redirect = FALSE;
		
		
		
		// start, code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('form_title', 'Title', 'trim|required|callback__alpha_dash_space');
		
		$this->form_validation->set_rules('form_subject', 'Email Subject', 'trim|required|callback__alpha_dash_space');
		
		$this->form_validation->set_rules('form_content', 'Email Body', 'trim|required|callback__alpha_dash_space'); 
		
		// end, code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{
			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{	

			
				// save here
				
				$res = $this->admin_model->update_form();
				
				if( $res )
				{
					$errmsg = " Format updated successfully ";
					
					$flag_redirect = TRUE;
				}
				else 
				{
					$errmsg = " Format couldnot be updated ";	
				}
				
			}
			else 
			{
				$errmsg = " Format couldnot be updated ";	
			}	
				
		}
		
		$data['record'] = $this->admin_model->form_detail( $this->input->post('form_id') );
		
		$data['errmsg'] = $errmsg;
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		
		if( $flag_redirect )
		{
			$this->form_list( $errmsg );
		}
		else 
		{
			$this->load->view('admin_form_edit', $data);	
		}		
		
	}		


	public function form_edit()
	{
		$form_id = $this->uri->segment(3);
		
		$data['record'] = $this->admin_model->form_detail( $form_id );
		
		$data['errmsg'] = '';
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
			
		$this->load->view('admin_form_edit', $data);
	}
	
	public function log_list()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$this->table->set_heading('S.No', 'Name', 'Reg No.', 'Email', 'View Logs', '<input type = "checkbox" name = "check_all" value = "check_all" />');
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/log_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		if( !$this->only_num( $offset ) )
		{
			$offset = 0;
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		// below line added by preeti on 25th apr 14
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_log_list', $data);
	}

	// below code commented by preeti on 7th apr 14	

	/*public function send_excel_email()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('form_id', 'Email Format', 'trim|required|xss_clean');
		
		$errmsg = '';
		
		if( $this->form_validation->run() == TRUE )
		{
			// if selected file is valid then read it
			// else show a message to select the correct file type 
			
			$form_id = $this->input->post('form_id');
					
			if( isset($_FILES['excel_file']) && $_FILES['excel_file']['size'] > 0  )
			{
				$name = $_FILES['excel_file']['name'];
				
				if( strpos($name, '.xls') === FALSE )
				{
					$errmsg = "Please upload an excel file";		
				}
				else 
				{
					// start, code added by preeti on 4th apr 14
					
					//$valid = false;

					//$types = array('Excel2007', 'Excel5');

					//foreach ($types as $type) 
					{    	
	    				//$reader = PHPExcel_IOFactory::createReader($type);
						
						//$file_path = $_FILES['excel_file']['tmp_name'] ;
	    
	    				//if ($reader->canRead( $file_path )) 
	    				{        	
		        			//$valid = true;
		        			
		        			//break;
	    				}
					}

					//if ($valid) 
					{
						//echo "excel file is valid";
						
						//exit;  
					} 
					//else 
					{
						//echo "excel file is INVALID";
						
						//exit;
					}
					
					// end, code added by preeti on 4th apr 14	
						
					// do the coding here to send emails after reading from the excel file
					
					//cell1 ( first column ) starting from second row
					
					$dom = DOMDocument::load( $_FILES['excel_file']['tmp_name'] );
						  
					$rows = $dom->getElementsByTagName( 'Row' );
					
					$first_row = true;
						
					$regnum_arr = array(); 
						  
					foreach ($rows as $row)
					{							
						if ( !$first_row )
						{
							$cells = $row->getElementsByTagName( 'Cell' );
							  
							$ind = 1;
							  
							foreach( $cells as $cell )
							{
							   //$ind = $cell->getAttribute( 'Index' );
								
							   if ( $ind == 2 ) // line added by preeti on 3rd mar 14 
							   {
								 $regnum_arr[] = $cell->nodeValue;
										
								 break;
							   }	
								  						
							   $ind++;
							   
							}							  
							  
						 }
						  
						$first_row = false;
					}
					
					if( COUNT( $regnum_arr ) > 0 )
					{
						// send the email to all the given regnums
						
						
						// get the email id for the corresponding doc_id
			
						// $doc_ids array contains all the selected users
						
						$emails = $this->admin_model->get_regnum_email( $regnum_arr ); 
						
						$format_obj = $this->admin_model->form_detail( $form_id );
						
						$email_arr = array();
						
						foreach( $emails as $obj )
						{
							$email_arr[] = $obj->reg_email;
						}
												
						$config = array(
						
						'protocol' => 'smtp',
												
						'smtp_host' => 'relay.nic.in',
												
						'smtp_port' => 25,
												
						'mailtype' => 'html',
							        
						'newline' => '\r\n',
							        
						'charset' => 'utf-8' //default charset				
											
						);
											
						$this->load->library('email', $config);
											
						$this->email->set_newline("\r\n");
						
						// get the admin email id to set the from email id
						
						$admin_email = $this->admin_model->get_admin_email();
												
						if( !$admin_email || $admin_email == '' )
						{
							$admin_email = "preeti.nhq@nic.in";	
						}
											
						$this->email->from($admin_email, 'AMC');
						
						//$this->email->cc( $email_arr ); 
											
						$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
											
						$this->email->subject( $format_obj->form_subject ); // email format subject here		
											
						$msg = $format_obj->form_content; // html email format body here
											
						$this->email->message($msg);
							
						//$res_send = $this->email->send();	// commented by preeti on 20th mar 14 for automatic testing
									
						$res_send = true; // added by preeti on 20th mar 14 for automatic testing			
											
						if( $res_send )
						{
							$errmsg = "Mail Sent Successfully";
							
							// add record in the elog table for all the doc_id that are there in the 
							
							// $doc_ids array
							
							//$res_log = $this->admin_model->add_email_log( $doc_ids, $form_id );
							
							//if( $res_log )
							{
								//$errmsg = "Mail Sent Successfully";
							}			
						}
						else 
						{
							$errmsg = "Mail couldnot be Sent";	
						}						
						
					}				    					
					 
				}	
			
			}
			else 
			{
				$errmsg = "Please upload a file";
			}
			 
		}
		else 
		{
			$errmsg = strip_tags( validation_errors() );	
		}
		
		$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		$config['num_links'] = 20;
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		$data['keyword'] = '';
		
		$data['errmsg'] = $errmsg;
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		$this->load->view('admin_log_list', $data);			
	}*/
	
	
	
	// below code added by preeti on 7th apr 14	
	
	public function send_excel_email()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('form_id', 'Email Format', 'trim|required|integer');// line modified by preeti on 12th apr 14
		
		$errmsg = '';
		
		if( $this->form_validation->run() == TRUE )
		{
			// if selected file is valid then read it
			// else show a message to select the correct file type 
			
			$form_id = $this->input->post('form_id');
					
			if( isset($_FILES['excel_file']) && $_FILES['excel_file']['size'] > 0  )
			{
				$name = $_FILES['excel_file']['name'];
				
				if( strpos($name, '.xls') === FALSE )
				{
					$errmsg = "Please upload an excel file";		
				}
				else 
				{
					require_once dirname(__FILE__) . '/../third_party/PHPExcel/IOFactory.php';
					
					// start, code added by preeti on 4th apr 14
					
					$valid = false;

					$types = array('Excel2007', 'Excel5');

					foreach ($types as $type) 
					{    	
	    				$reader = PHPExcel_IOFactory::createReader($type);
						
						$file_path = $_FILES['excel_file']['tmp_name'] ;
	    
	    				if ($reader->canRead( $file_path )) 
	    				{        	
		        			$valid = true;
		        			
		        			break;
	    				}
					}

					if ($valid) 
					{						
						
						// do the coding here to send emails after reading from the excel file
					
						//cell1 ( first column ) starting from second row
						
						/*$dom = DOMDocument::load( $_FILES['excel_file']['tmp_name'] );
							  
						$rows = $dom->getElementsByTagName( 'Row' );
						
						$first_row = true;
							
						$regnum_arr = array(); 
							  
						foreach ($rows as $row)
						{							
							if ( !$first_row )
							{
								$cells = $row->getElementsByTagName( 'Cell' );
								  
								$ind = 1;
								  
								foreach( $cells as $cell )
								{
								   //$ind = $cell->getAttribute( 'Index' );
									
								   if ( $ind == 2 ) // line added by preeti on 3rd mar 14 
								   {
									 $regnum_arr[] = $cell->nodeValue;
											
									 break;
								   }	
									  						
								   $ind++;
								   
								}							  
								  
							 }
							  
							$first_row = false;
						}*/
						
					
						
						$tmp_filename = $_FILES['excel_file']['tmp_name'];
							
						$objPHPExcel = PHPExcel_IOFactory::load( $tmp_filename );
							
							
						$objPHPExcel->setActiveSheetIndex(0); 					
						
						
						// get total no. of rows in the excel sheet
						
						// put a for loop to fetch each reg no as Bn whene n is a number	
						
						
						$lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();
						
						$arr = array();
						
						for( $i = 2; $i <= $lastRow; $i++  )
						{
							$regnum_arr[] = $objPHPExcel->getActiveSheet()->getCell('B'.$i);
							
							$var = 	$objPHPExcel->getActiveSheet()->getCell('B'.$i);
							
							if( $var != '' )
							{
								$arr[] = strval( $var );	
							}					
							
						}	
						
						$regnum_arr = array();
						
						if( COUNT( $arr ) > 0 )
						{
							 for( $j = 0; $j < COUNT( $arr ); $j++ )
							 {
							 	$regnum_arr[] =  strval( $arr[$j] ) ;
							 }
						}				
						
						if( COUNT( $regnum_arr ) > 0 )
						{
							// send the email to all the given regnums
							
							
							// get the email id for the corresponding doc_id
				
							// $doc_ids array contains all the selected users registration numbers
							
							$emails = $this->admin_model->get_regnum_email( $regnum_arr ); 
							
							$format_obj = $this->admin_model->form_detail( $form_id );
							
							$email_arr = array();
							
							foreach( $emails as $obj )
							{
								$email_arr[] = $obj->reg_email;
							}
																				
							$config = array(
							
							'protocol' => 'smtp',
													
							'smtp_host' => 'relay.nic.in',
													
							'smtp_port' => 25,
													
							'mailtype' => 'html',
								        
							'newline' => '\r\n',
								        
							'charset' => 'utf-8' //default charset				
												
							);
							
							/*$config = Array(
		    
						    'protocol' => 'smtp',
						    
						    'smtp_host' => 'ssl://smtp.googlemail.com',
						    
						    'smtp_port' => 465,
						    
						    'smtp_user' => 'rahejapreetiemail@gmail.com',
						    
						    'smtp_pass' => '',
						    
						    'mailtype'  => 'html', 
						    
						    'charset'   => 'iso-8859-1'
							
							);*/ // commented by preeti on 16th may 14
												
							$this->load->library('email', $config);
												
							$this->email->set_newline("\r\n");
							
							// get the admin email id to set the from email id
							
							$admin_email = $this->admin_model->get_admin_email();
													
							if( !$admin_email || $admin_email == '' )
							{
								$admin_email = "dirafmsp-mod@nic.in"; // changed by preeti on 16th may 14	
							}
												
							$this->email->from($admin_email, 'AMC');
							
							//$this->email->cc( $email_arr ); 
												
							$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
												
							$this->email->subject( $format_obj->form_subject ); // email format subject here		
												
							$msg = $format_obj->form_content; // html email format body here
												
							$this->email->message($msg);
								
							
							// if added by preeti on 25th apr 14
							if( $this->input->post('admin_random') == $this->session->userdata('admin_random') )// if block added by preeti on 25th apr 14 
							{
							
								$res_send = $this->email->send();											
													
								if( $res_send )
								{
									$errmsg = "Mail Sent Successfully";
									
									// below code added by preeti on 22nd apr 14 for manual testing
									
									$errmsg = urlencode( $errmsg );
							
									$title = urlencode( "Send Email" );
							
									redirect('admin/show_message/'.$title.'/'.$errmsg);
									
									
									// add record in the elog table for all the doc_id that are there in the 
									
									// $doc_ids array
									
									//$res_log = $this->admin_model->add_email_log( $doc_ids, $form_id );
									
									//if( $res_log )
									{
										//$errmsg = "Mail Sent Successfully";
									}			
								}
								else 
								{
									$errmsg = "Mail couldnot be Sent";	
									
									// below code added by preeti on 22nd apr 14 for manual testing
									
									$errmsg = urlencode( $errmsg );
							
									$title = urlencode( "Send Email" );
							
									redirect('admin/show_message/'.$title.'/'.$errmsg);
								}
								
							}
							else // else block added by preeti on 25th apr 14
							{								
								$errmsg = "Mail couldnot be Sent";	
									
								// below code added by preeti on 22nd apr 14 for manual testing
									
								$errmsg = urlencode( $errmsg );
							
								$title = urlencode( "Send Email" );
							
								redirect('admin/show_message/'.$title.'/'.$errmsg);
							}			
							
						}	
							
							
					} 
					else 
					{
						$errmsg = "Please upload a Valid Excel File";	
					}
						
					// end, code added by preeti on 4th apr 14				    					
					 
				}	
			
			}
			else 
			{
				$errmsg = "Please upload a file";
			}
			 
		}
		else 
		{
			$errmsg = strip_tags( validation_errors() );	
		}
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/log_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		$data['keyword'] = '';
		
		$data['errmsg'] = $errmsg;
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		// below line added by preeti on 25th apr 14
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_log_list', $data);			
	}	
	
	
	// code commented by preeti on 11th apr 14 for black-box testing

	/*public function send_email()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		//$form_id = $this->input->post('form_id'); 
		
		$form_id = $this->input->post('form_id_hid');
		
		$doc_ids = $this->input->post('doc_ids');
		
		$errmsg = '';
		
		if( $form_id == '' )
		{
			$errmsg .= 'Please select an email format <br />';			
		}
		
		if( $doc_ids == '' )
		{
			$errmsg .= 'Please select a user ';			
		}
		
		if( !empty( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;					
		}
		else 
		{
			// get the email id for the corresponding doc_id
			
			// $doc_ids array contains all the selected users
			
			$reg_ids = $doc_ids;
			
			$emails = $this->admin_model->get_email( $reg_ids ); 
			
			$format_obj = $this->admin_model->form_detail( $form_id );
			
			$email_arr = array();
			
			foreach( $emails as $obj )
			{
				$email_arr[] = $obj->reg_email;
			}
				
			$config = array(
			
			'protocol' => 'smtp',
									
			'smtp_host' => 'relay.nic.in',
									
			'smtp_port' => 25,
									
			'mailtype' => 'html',
				        
			'newline' => '\r\n',
				        
			'charset' => 'utf-8' //default charset				
								
			);
								
			$this->load->library('email', $config);
								
			$this->email->set_newline("\r\n");
								
			$admin_email = $this->admin_model->get_admin_email();
									
			if( !$admin_email || $admin_email == '' )
			{
				$admin_email = "preeti.nhq@nic.in";	
			}
								
			$this->email->from($admin_email, 'AMC');
			
			
			//$this->email->cc( $email_arr ); 
								
			//$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
			
			$this->email->bcc( $email_arr );
								
			$this->email->subject( $format_obj->form_subject ); // email format subject here		
								
			$msg = $format_obj->form_content; // html email format body here
								
			$this->email->message($msg);
				
			//$res_send = $this->email->send();	// commented by preeti on 20th mar 14 for automatic testing
						
			$res_send = true; // added by preeti on 20th mar 14 for automatic testing			
								
			if( $res_send )
			{
				$data['errmsg'] = "Mail Sent Successfully";
				
				// add record in the elog table for all the doc_id that are there in the 
				
				// $doc_ids array
				
				$res_log = $this->admin_model->add_email_log( $doc_ids, $form_id );
				
				if( $res_log )
				{
					$data['errmsg'] = "Mail Sent Successfully";
				}				
			}
			else 
			{
				$data['errmsg'] = "Mail couldnot be Sent";	
			}		
				
		}


		$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		$config['num_links'] = 20;
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		

		
		$this->load->view('admin_log_list', $data);
		
	}*/

	// below code added by preeti on 11th apr 14 for black-box testing
	
	public function send_email()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		//$form_id = $this->input->post('form_id'); 
		
		$form_id = $this->input->post('form_id_hid');
		
		$doc_ids = $this->input->post('doc_ids');
		
		$errmsg = '';
		
		if( $form_id == '' )
		{
			$errmsg .= 'Please select an email format <br />';			
		}
		
		if( $doc_ids == '' )
		{
			$errmsg .= 'Please select a user ';			
		}
		
		if( !empty( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;					
		}
		else 
		{
			// get the email id for the corresponding doc_id
			
			// $doc_ids array contains all the selected users
			
			$reg_ids = $doc_ids;
			
			$emails = $this->admin_model->get_email( $reg_ids ); 
					
			$format_obj = $this->admin_model->form_detail( $form_id );
			
			if( $emails != '' && $format_obj != '' )
			{
				$email_arr = array();
			
				foreach( $emails as $obj )
				{
					$email_arr[] = $obj->reg_email;
				}
				
				
					
				$config = array(
				
				'protocol' => 'smtp',
										
				'smtp_host' => 'relay.nic.in',
										
				'smtp_port' => 25,
										
				'mailtype' => 'html',
					        
				'newline' => '\r\n',
					        
				'charset' => 'utf-8' //default charset				
									
				);
				
				
				
				/*$config = Array(
		    
			    'protocol' => 'smtp',
			    
			    'smtp_host' => 'ssl://smtp.googlemail.com',
			    
			    'smtp_port' => 465,
			    
			    'smtp_user' => 'rahejapreetiemail@gmail.com',
			    
			    'smtp_pass' => '',
			    
			    'mailtype'  => 'html', 
			    
			    'charset'   => 'iso-8859-1'
				
				);*/ // commented by preeti on 16th may 14
				
									
				$this->load->library('email', $config);
									
				$this->email->set_newline("\r\n");
									
				$admin_email = $this->admin_model->get_admin_email();
										
				if( !$admin_email || $admin_email == '' )
				{
					$admin_email = "dirafmsp-mod@nic.in"; // mail id changed by preeti on 16th may 14	
				}
									
				$this->email->from($admin_email, 'AMC');
				
				
				//$this->email->cc( $email_arr ); 
									
				//$this->email->to( $email_arr );	// email will go to all the users selected with the same email format	
				
				$this->email->bcc( $email_arr );
									
				$this->email->subject( $format_obj->form_subject ); // email format subject here		
									
				$msg = $format_obj->form_content; // html email format body here
									
				$this->email->message($msg);
					
				// if added by preeti on 25th apr 14

				if( $this->input->post('admin_random') == $this->session->userdata('admin_random') )// if block added by preeti on 25th apr 14 
				{
				
					$res_send = $this->email->send();
						
					if( $res_send )
					{					
						$data['errmsg'] = "Mail Sent Successfully";
							
						// below code added by preeti on 22nd apr 14 for manual testing
							
						$errmsg = $data['errmsg'];
										
						$errmsg = urlencode( $errmsg );
													
						$title = urlencode( "Send Email" );
													
						redirect('admin/show_message/'.$title.'/'.$errmsg);
												
							
						/*// add record in the elog table for all the doc_id that are there in the 
							
						$res_log = $this->admin_model->add_email_log( $doc_ids, $form_id );
							
						if( $res_log )
						{
							$data['errmsg'] = "Mail Sent Successfully";
						}*/				
					}
					else 
					{
						$data['errmsg'] = "Mail couldnot be Sent";	
							
						// below code added by preeti on 22nd apr 14 for manual testing
							
						$errmsg = $data['errmsg'];
										
						$errmsg = urlencode( $errmsg );
													
						$title = urlencode( "Send Email" );
													
						redirect('admin/show_message/'.$title.'/'.$errmsg);
					}
				}
				else 
				{
					$data['errmsg'] = "Mail couldnot be Sent";	
							
					// below code added by preeti on 22nd apr 14 for manual testing
							
					$errmsg = $data['errmsg'];
										
					$errmsg = urlencode( $errmsg );
													
					$title = urlencode( "Send Email" );
													
					redirect('admin/show_message/'.$title.'/'.$errmsg);
				}					
				
			}
			else 
			{
				
				$data['errmsg'] = "Mail couldnot be Sent";	
				
				// below code added by preeti on 22nd apr 14 for manual testing
					
				$errmsg = $data['errmsg'];
								
				$errmsg = urlencode( $errmsg );
											
				$title = urlencode( "Send Email" );
											
				redirect('admin/show_message/'.$title.'/'.$errmsg);
			}			
				
		}

		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/log_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		// below line added by preeti on 25th apr 14
		
		$data['admin_random'] = $this->session->userdata('admin_random');

		
		$this->load->view('admin_log_list', $data);
		
	}
	
	public function user_search()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}		
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/log_list/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/log_list'.$url;
		
		$config['total_rows'] = $this->admin_model->get_user_num();
		
		$config['per_page'] = 10;
		
		//$config['per_page'] = 4;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('form_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		$data['keyword'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_records_limit( $config['per_page'], $offset);
		
		$data['formats'] = $this->admin_model->get_email_format();
		
		
		
		
		
		
		
		if( $this->input->post('keyword') != '')
		{
				
			$data['records'] = $this->admin_model->get_user_search();
					
			// below if else added by preeti on 10th apr 14
			
			if( $this->alpha_at_space( $this->input->post('keyword') ) )
			{
				$keyword = $this->input->post('keyword');
			}
			else 
			{
				$keyword = '';
			}
			
			$data['keyword'] = $keyword; // line modified by preeti on 10th apr 14
			
			if( !empty( $data['records'] ) )
			{					
				$data['errmsg'] = "";	
			}
			else 
			{
				$data['errmsg'] = "No Search Results Found ! ";
			}	
			
		}
		
		// below line added by preeti on 25th apr 14
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_log_list', $data);		
					
	}

	// to generate an Excel sheet from DB data
	
	/* public function file_xls( $test = '' )
	{
	$decoded='';$decoded1='';$decoded2='';$decoded3='';$decoded4='';$decoded5='';$decoded6='';
	   if($this->uri->segment(3) == '')
	   {
	   		//$decoded = urldecode( $test );	// this is encoded before pasing in the url in admin_doc_list
	   		//echo $decoded;
	   		
	   }
		else 
		{
			$decodede='';
	  		$decodede = $this->uri->segment(3);
			//echo $decoded;
			$s=array();
	  	$s=explode("-",$decodede);
	  		if($s[0]!=0)
	  		$decoded=$s[0];
	  		if($s[1]!=0)
	  		$decoded1=$s[1];
	  		if($s[2]!=0)
	  		$decoded2=$s[2];
	  		if($s[3]!='0000.00.00')
	  		$decoded3=$s[3];
	  		if($s[4]!='0000.00.00')
	  		$decoded4=$s[4];
	  		if($s[5]!=0)
	  		$decoded5=$s[5];
	  		if($s[6]!=0)
	  		$decoded6=$s[6];
	  		
		}
		  
	//   $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	 //  $this->load->library('mpdf');
	   
	    $this->load->helper('php-excel');
	   
	   //$query = $this->db->get('register');
	   
	   $fields = (
	   
	   //$field_array[] = array ("Reg. No.", "Name", "Gender", "DOB", "Age", "State", "Email", "Mobile", "Reg. Date" ) 
	   
	   $field_array[] = array ("Reg No", "Name", "Fathr Name", "DOB", "Age", 
	   
	   "Gender", "Natnlty","Qual", "Atmpts"
	   
	   , "MCI Recg", "Married", "Spouse Natnlty", 
	   
	   "PMR No", "Foreign Grad", "DD No.", "DD Date", 
	   
	   "Reg Date" )
	   
	   );
	   $records_arr = $this->admin_model->get_search_result1($decoded,$decoded1,$decoded2,$decoded3,$decoded4,$decoded5,$decoded6);
	   //$records_arr = $this->admin_model->get_search_result($decoded );
	   
	   foreach ($records_arr as $row)
	   {
	   	   if( $row->u_is_pg == 'y' )
		   {
		   		$edu_qual = 'MBBS, '.$row->pg_degree.'('.$row->pg_subject.')';
		   }
		   else 
		   {
				$edu_qual = 'MBBS';	   
		   }
		   
		   if( $row->u_college_recog == 'y' )
		   {
		   		$u_college_recog = "Yes";
		   }
		   else 
		   {
				$u_college_recog = "No";   
		   }
		   
		   $state_name = $this->admin_model->get_state_name( $row->post_state );
				
		
	       $data_array[] = array( 
	       
	       $row->reg_regno, 
	       
	       strtoupper($row->reg_fname.' '.$row->reg_mname.' '.$row->reg_lname), 
	       
		   strtoupper($row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname)	, 
		   
		   db_to_calen( $row->u_dob ) ,
		   
		   $row->u_age_year,
		   
		   strtoupper($row->u_sex),
		   
		   strtoupper($row->u_nationality),
		   
		   strtoupper($edu_qual),
	       
	       $row->u_num_attempt, 
	       
		   strtoupper($row->u_college_recog),
		   
		   strtoupper($row->u_is_married),
	       
	       strtoupper($row->sp_nation),
	       
		   strtoupper($row->u_pmrc_num),
		   
		   strtoupper($row->u_grad_from),
	       
		   $row->u_dd_num,
		   
		   db_to_calen($row->u_dd_date),
		   
		   db_to_calen( $row->reg_createdon ) ,
		   
		   );
	   }
	   
	   
	   $xls = new Excel_XML;
	   
	   $xls->addArray ($field_array);
	   
	   $xls->addArray ($data_array);
	   
	   $file_name = date('Y-m-d');
	   
	   $xls->generateXML ( $file_name );
	}  */
	
	
	// below function added by preeti on 7th apr 14
	
	function create_excel( ) 
	{
		//load our new PHPExcel library
	
		$this->load->library('excel');
		
		//activate worksheet number 1
	
		$this->excel->setActiveSheetIndex(0);
		
		//name the worksheet
	
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		
		//set cell A1 content with some text
	
		$this->excel->getActiveSheet()->setCellValue('A1', 'first cell');
		
		$this->excel->getActiveSheet()->setCellValue('A3', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('A5', 'third cell');							  
		
		$this->excel->getActiveSheet()->setCellValue('B1', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('B2', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('B3', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('C1', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('C2', 'second cell');
		
		$this->excel->getActiveSheet()->setCellValue('C3', 'second cell');
		
									  
		
		
		//change the font size
	
		//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		
		//make the font become bold
	
		//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		//merge cell A1 until D1
	
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		
		//set aligment to center for that merged cell (A1 to D1)
	
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
	
		header('Content-Type: application/vnd.ms-excel'); //mime type
	
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	
		header('Cache-Control: max-age=0'); //no cache
		             
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		
		//if you want to save it as .XLSX Excel 2007 format
	
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		
		//force user to download the Excel file without writing it to server's HD
	
		$objWriter->save('php://output');
	}

	// below function added by preeti on 7th apr 14

	function read_excel()
	{
		/** Include PHPExcel_IOFactory */
			
		require_once dirname(__FILE__) . '/../third_party/PHPExcel/IOFactory.php';
			
			
		if (!file_exists("test.xls")) 
		{
			
			exit("Please run test.php first.\n");
			
		}
			
		$objPHPExcel = PHPExcel_IOFactory::load("2014-04-04.xls");
			
			
		$objPHPExcel->setActiveSheetIndex(0); 
			
			
		$var = $objPHPExcel->getActiveSheet()->getCell('A1');
			
		echo " cell value is ".$var; 
			
		$var = $objPHPExcel->getActiveSheet()->getCell('B1');
			
		echo " cell value is ".$var; 
			
		$var = $objPHPExcel->getActiveSheet()->getCell('C1');
			
		echo " cell value is ".$var; 
			 
		$var = $objPHPExcel->getActiveSheet()->getCell('A2');
			
		echo " cell value is ".$var; 
			
		$var = $objPHPExcel->getActiveSheet()->getCell('B2');
			
		echo " cell value is ".$var; 
			
		$var = $objPHPExcel->getActiveSheet()->getCell('C2');
			
		echo " cell value is ".$var; 			
		
	}		
	
	// below function commented by preeti on 7th apr 14
	
	/*public function file_xls( $test = '' ) 
	{			
	   
	    $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
	
	    $sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
	   
	    $this->load->helper('php-excel');
	   
	   
	   $fields = (
	   
	   // sno added by preeti on 28th feb 14
	   
	   $field_array[] = array ("S.No.", "Reg No", "Name", "Fathr Name", "DOB", "Age", 
	   
	   "Gender", "Natnlty","Qual", "Atmpt(I)", "Atmpt(II)"
	   
	   , "MCI Recg", "Married", "Spouse Natnlty", 
	   
	   "PMR No", "Foreign Grad", "DD No.", "DD Date", 
	   
	   "Reg Date" )
	   
	   );
	   
	   $records_arr = $this->admin_model->get_search_result(  '', '' , $sort_by, $sort_order );
	   
	   $i = 1;// line added by preeti on 28th feb 14
	   
	   foreach ($records_arr as $row)
	   {
	   	
		   // below lines added by preeti on 27th feb 14
				
		   if( $row->u_is_name_change == 'y' )
		   {
				$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
		   }
		   else 
		   {
				$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
		   }
	   	
		   if( $row->u_is_pg == 'y' )
		   {
		   	
				$res_pg = $this->admin_model->get_pg_data( $row->reg_regno ); // returns an array of post_grad objects
			
		   		$edu_qual = strtoupper( $row->u_basic_qual );
				
				if( is_array( $res_pg ) &&  COUNT( $res_pg ) > 0 )
				{
					foreach( $res_pg as $obj_pg )
					{
						$edu_qual .= ', '.$obj_pg->pg_degree.'('.$obj_pg->pg_subject.')';
					}
				}
		   		
		   		$edu_qual = rtrim( $edu_qual, ',' );
		   }
		   else 
		   {
				$edu_qual = strtoupper( $row->u_basic_qual );   
		   }
		   
		   if( $row->u_college_recog == 'y' )
		   {
		   		$u_college_recog = "Yes";
		   }
		   else 
		   {
				$u_college_recog = "No";   
		   }
		   
		   $state_name = $this->admin_model->get_state_name( $row->post_state );
				
			// sno added by preeti on 28th feb 14
		
	       $data_array[] = array( 
	       
		   $i,
		   
	       $row->reg_regno, 
	       
	       //strtoupper($row->reg_fname.' '.$row->reg_mname.' '.$row->reg_lname),
	       
	       strtoupper($name), // above line commented and this line added by preeti on 27th feb 14 
	       
		   strtoupper($row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname)	, 
		   
		   db_to_calen( $row->u_dob ) ,
		   
		   $row->u_age_year.' yr,'.$row->u_age_month.' mnth,'.$row->u_age_day.' days',
		   
		   strtoupper($row->u_sex),
		   
		   strtoupper($row->u_nationality),
		   
		   strtoupper($edu_qual),
	       
	       $row->u_num_attempt,
	       
		   $row->u_num_attempt,  
	       
		   strtoupper($row->u_college_recog),
		   
		   strtoupper($row->u_is_married),
	       
	       strtoupper($row->sp_nation),
	       
		   strtoupper($row->u_pmrc_num),
		   
		   strtoupper($row->u_grad_from),
	       
		   $row->u_dd_num,
		   
		   db_to_calen($row->u_dd_date),
		   
		   db_to_calen( $row->reg_createdon ) ,
		   
		   );
		   
		    $i++;// line added by preeti on 28th feb 14
	   }
	   
	   
	   $xls = new Excel_XML;
	   
	   $xls->addArray ($field_array);
	   
	   $xls->addArray ($data_array);
	   
	   $file_name = date('Y-m-d');
	   
	   $xls->generateXML ( $file_name );
	}*/

	
	// below function added by preeti on 7th apr 14
	
	public function file_xls( $test = '' ) 
	{			
	   
	    $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
	
	    $sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
		
		//start, code added by preeti on 11th apr 14 for black-box testing
		
		if( !$this->alpha_and_space( $sort_by  ) )
		{
			$sort_by = 'u_id';
		}
		
		
		if( !$this->alpha_and_space( $sort_order  ) )
		{
			$sort_order = 'DESC';
		}
		
		//end, code added by preeti on 11th apr 14 for black-box testing
	   
	    $this->load->helper('php-excel');
	   
	   
	   $fields = (
	   
	   // sno added by preeti on 28th feb 14
	   
	   $field_array[] = array ("S.No.", "Reg No", "Name", "Fathr Name", "DOB", "Age", 
	   
	   "Gender", "Natnlty","Qual", "Atmpt(I)", "Atmpt(II)"
	   
	   , "MCI Recg", "Married", "Spouse Natnlty", 
	   
	   "PMR No", "Foreign Grad", "DD No.", "DD Date", 
	   
	   "Reg Date" )
	   
	   );
	   
	   $records_arr = $this->admin_model->get_search_result(  '', '' , $sort_by, $sort_order );
	   
	   $i = 1;// line added by preeti on 28th feb 14
	   
	   
	   
	    //load our new PHPExcel library
	
	   $this->load->library('excel');
			
	  //activate worksheet number 1
		
	   $this->excel->setActiveSheetIndex(0);
			
	   //name the worksheet
		
	   $this->excel->getActiveSheet()->setTitle('User List');
		 	
	   $this->excel->getActiveSheet()->setCellValue('A1', 'S.No.');
		
	   $this->excel->getActiveSheet()->setCellValue('B1', 'Reg No');
			
	   $this->excel->getActiveSheet()->setCellValue('C1', 'Name');							  
			
	   $this->excel->getActiveSheet()->setCellValue('D1', 'Fathr Name');
			
	   $this->excel->getActiveSheet()->setCellValue('E1', 'DOB');
			
	   $this->excel->getActiveSheet()->setCellValue('F1', 'Age');
			
	   $this->excel->getActiveSheet()->setCellValue('G1', 'Gender');
			
	   $this->excel->getActiveSheet()->setCellValue('H1', 'Natnlty');
			
	   $this->excel->getActiveSheet()->setCellValue('I1', 'Qual');
		   
	   $this->excel->getActiveSheet()->setCellValue('J1', 'Atmpt(I)');  	
		   
	   $this->excel->getActiveSheet()->setCellValue('K1', 'Atmpt(II)');
		   
	   $this->excel->getActiveSheet()->setCellValue('L1', 'MCI Recg');
		   
	   $this->excel->getActiveSheet()->setCellValue('M1', 'Married');
		   
	   $this->excel->getActiveSheet()->setCellValue('N1', 'Spouse Natnlty');
		   
	   $this->excel->getActiveSheet()->setCellValue('O1', 'PMR No');
		   
	   $this->excel->getActiveSheet()->setCellValue('P1', 'Foreign Grad');
		   
	   $this->excel->getActiveSheet()->setCellValue('Q1', 'DD No.');
		   
	   $this->excel->getActiveSheet()->setCellValue('R1', 'DD Date');
		   
	   $this->excel->getActiveSheet()->setCellValue('S1', 'Reg Date');	   
	   
	   
	   foreach ($records_arr as $row)
	   {
	   	
		   // below lines added by preeti on 27th feb 14
				
		   if( $row->u_is_name_change == 'y' )
		   {
				$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
		   }
		   else 
		   {
				$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
		   }
	   	
		   if( $row->u_is_pg == 'y' )
		   {
		   	
				$res_pg = $this->admin_model->get_pg_data( $row->reg_regno ); // returns an array of post_grad objects
			
		   		$edu_qual = strtoupper( $row->u_basic_qual );
				
				if( is_array( $res_pg ) &&  COUNT( $res_pg ) > 0 )
				{
					foreach( $res_pg as $obj_pg )
					{
						$edu_qual .= ', '.$obj_pg->pg_degree.'('.$obj_pg->pg_subject.')';
					}
				}
		   		
		   		$edu_qual = rtrim( $edu_qual, ',' );
		   }
		   else 
		   {
				$edu_qual = strtoupper( $row->u_basic_qual );   
		   }
		   
		   if( $row->u_college_recog == 'y' )
		   {
		   		$u_college_recog = "Yes";
		   }
		   else 
		   {
				$u_college_recog = "No";   
		   }
		   
		   $state_name = $this->admin_model->get_state_name( $row->post_state );		
			
		  
		   $this->excel->getActiveSheet()->setCellValue('A'.($i+1), $i);
		
		   $this->excel->getActiveSheet()->setCellValue('B'.($i+1), $row->reg_regno);
				
		   $this->excel->getActiveSheet()->setCellValue('C'.($i+1),  strtoupper($name));							  
				
		   $this->excel->getActiveSheet()->setCellValue('D'.($i+1), strtoupper($row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname));
				
		   $this->excel->getActiveSheet()->setCellValue('E'.($i+1), db_to_calen( $row->u_dob ));
				
		   $this->excel->getActiveSheet()->setCellValue('F'.($i+1), $row->u_age_year.' yr,'.$row->u_age_month.' mnth,'.$row->u_age_day.' days');
				
		   $this->excel->getActiveSheet()->setCellValue('G'.($i+1), strtoupper($row->u_sex));
				
		   $this->excel->getActiveSheet()->setCellValue('H'.($i+1), strtoupper($row->u_nationality));
				
		   $this->excel->getActiveSheet()->setCellValue('I'.($i+1), strtoupper($edu_qual));
			   
		   $this->excel->getActiveSheet()->setCellValue('J'.($i+1), $row->u_num_attempt);  	
			   
		   $this->excel->getActiveSheet()->setCellValue('K'.($i+1), $row->u_num_attempt1);
			   
		   $this->excel->getActiveSheet()->setCellValue('L'.($i+1), strtoupper($row->u_college_recog));
			   
		   $this->excel->getActiveSheet()->setCellValue('M'.($i+1),  strtoupper($row->u_is_married));
			   
		   $this->excel->getActiveSheet()->setCellValue('N'.($i+1), strtoupper($row->sp_nation));
			   
		   $this->excel->getActiveSheet()->setCellValue('O'.($i+1), strtoupper($row->u_pmrc_num));
			   
		   $this->excel->getActiveSheet()->setCellValue('P'.($i+1), strtoupper($row->u_grad_from));
			   
		   $this->excel->getActiveSheet()->setCellValue('Q'.($i+1), $row->u_dd_num);
			   
		   $this->excel->getActiveSheet()->setCellValue('R'.($i+1), db_to_calen($row->u_dd_date));
			   
		   $this->excel->getActiveSheet()->setCellValue('S'.($i+1), db_to_calen( $row->reg_createdon ));
		  
		   $i++;// line added by preeti on 28th feb 14
	   }
	   
	   $filename = date('Y-m-d').'.xls';
	
	   header('Content-Type: application/vnd.ms-excel'); //mime type
	
	   header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	
	   header('Cache-Control: max-age=0'); //no cache
		             
	   //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		
	   //if you want to save it as .XLSX Excel 2007 format
	
	   $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		
	   //force user to download the Excel file without writing it to server's HD
	
	   $objWriter->save('php://output');   
	   
	}	
	
	
	public function file_xls12( $test = '' ) 
	{			
	   
	    $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
	
	    $sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
	   
	    $this->load->helper('php-excel');
	   
	   
	   $fields = (
	   
	   $field_array[] = array ("SNo","Reg No", "Name", "Fathr Name", "DOB", "Age", 
	   
	   "Gender", "Natnlty","Qual", "Atmpt(I)", "Atmpt(II)"
	   
	   , "MCI Recg", "Married", "Spouse Natnlty", 
	   
	   "PMR No", "Foreign Grad", "DD No.", "DD Date", 
	   
	   "Reg Date","idate","ncc" )
	   
	   );
	   
	  
	   $i = 1;// line added by preeti on 28th feb 14
	   $records_arr = $this->admin_model->get_search_resultap(  '', '' , $sort_by, $sort_order );
	   
	   foreach ($records_arr as $row)
	   {
	   	$regno=$row->u_regno;
	$ncc=$row->u_ncc_cert;
		$idate=$this->cletter_model->getivdate($regno);
			
		//	$this->cletter_model->
		   // below lines added by preeti on 16th may 14
				
		   if( $row->u_is_name_change == 'y' )
		   {
				$name = $row->new_fname.' '.$row->new_mname.' '.$row->new_lname;
		   }
		   else 
		   {
				$name = $row->u_fname.' '.$row->u_mname.' '.$row->u_lname;
		   }
	   	
		   if( $row->u_is_pg == 'y' )
		   {
		   	
				$res_pg = $this->admin_model->get_pg_data( $row->reg_regno ); // returns an array of post_grad objects
			
		   		$edu_qual = strtoupper( $row->u_basic_qual );
				
				if( is_array( $res_pg ) &&  COUNT( $res_pg ) > 0 )
				{
					foreach( $res_pg as $obj_pg )
					{
						$edu_qual .= ', '.$obj_pg->pg_degree.'('.$obj_pg->pg_subject.')';
					}
				}
		   		
		   		$edu_qual = rtrim( $edu_qual, ',' );
		   }
		   else 
		   {
				$edu_qual = strtoupper( $row->u_basic_qual );   
		   }
		   
		   if( $row->u_college_recog == 'y' )
		   {
		   		$u_college_recog = "Yes";
		   }
		   else 
		   {
				$u_college_recog = "No";   
		   }
		   
		   $state_name = $this->admin_model->get_state_name( $row->post_state );
				
		
	       $data_array[] = array( 
	       $i,
		   
	       $row->reg_regno, 
	       
	       //strtoupper($row->reg_fname.' '.$row->reg_mname.' '.$row->reg_lname),
	       
	       strtoupper( $name ), 
	       
		   strtoupper($row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname)	, 
		   
		  // db_to_calen( $row->u_dob ) ,
		   
		    $row->u_dob,
		   
		   $row->u_age_year.' yr,'.$row->u_age_month.' mnth,'.$row->u_age_day.' days',
		   
		   strtoupper($row->u_sex),
		   
		   strtoupper($row->u_nationality),
		   
		   strtoupper($edu_qual),
	       
	       $row->u_num_attempt,
	       
		   $row->u_num_attempt,  
	       
		   strtoupper($row->u_college_recog),
		   
		   strtoupper($row->u_is_married),
	       
	       strtoupper($row->sp_nation),
	       
		   strtoupper($row->u_pmrc_num),
		   
		   strtoupper($row->u_grad_from),
	       
		   $row->u_dd_num,
		   
		   db_to_calen($row->u_dd_date),
		   
		   db_to_calen( $row->reg_createdon ) ,$idate,$ncc
		   
		   );
		   $i++;// line added by preeti on 28th feb 14
	   }
	   
	   
	   $xls = new Excel_XML;
	   
	   $xls->addArray ($field_array);
	   
	   $xls->addArray ($data_array);
	   
	   $file_name = date('Y-m-d');
	   
	   $xls->generateXML ( $file_name );
	}
public function file_xls13( $test = '' ) 
	{			
	   
	    $uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
	
	    $sort_by = 'u_id';
		
		$sort_order = 'DESC';
		
		if( isset( $uri_arr['sort_by'] ) && $uri_arr['sort_by'] != ''  )
		{
			$sort_by = $uri_arr['sort_by'];
		}
		
		if( isset( $uri_arr['sort_order'] ) && $uri_arr['sort_order'] != ''  )
		{
			$sort_order = $uri_arr['sort_order'];
		}
	   
	    $this->load->helper('php-excel');
	   
	   $i=1;
	   $fields = (
	   
	   $field_array[] = array ("SNo","Reg No", "Name", "Fathr Name", "DOB", "Age", 
	   
	   "Gender", "Natnlty","Qual", "Atmpt(I)", "Atmpt(II)"
	   
	   , "MCI Recg", "Married", "Spouse Natnlty", 
	   
	   "PMR No", "Foreign Grad", "DD No.", "DD Date", 
	   
	   "Reg Date" )
	   
	   );
	   
	   $records_arr = $this->admin_model->get_search_resultre(  '', '' , $sort_by, $sort_order );
	   
	   foreach ($records_arr as $row)
	   {
	   	
		   if( $row->u_is_pg == 'y' )
		   {
		   	
				$res_pg = $this->admin_model->get_pg_data( $row->reg_regno ); // returns an array of post_grad objects
			
		   		$edu_qual = strtoupper( $row->u_basic_qual );
				
				if( is_array( $res_pg ) &&  COUNT( $res_pg ) > 0 )
				{
					foreach( $res_pg as $obj_pg )
					{
						$edu_qual .= ', '.$obj_pg->pg_degree.'('.$obj_pg->pg_subject.')';
					}
				}
		   		
		   		$edu_qual = rtrim( $edu_qual, ',' );
		   }
		   else 
		   {
				$edu_qual = strtoupper( $row->u_basic_qual );   
		   }
		   
		   if( $row->u_college_recog == 'y' )
		   {
		   		$u_college_recog = "Yes";
		   }
		   else 
		   {
				$u_college_recog = "No";   
		   }
		   
		   $state_name = $this->admin_model->get_state_name( $row->post_state );
				
		
	       $data_array[] = array( 
	       $i,
	       $row->reg_regno, 
	       
	       strtoupper($row->reg_fname.' '.$row->reg_mname.' '.$row->reg_lname), 
	       
		   strtoupper($row->u_father_fname.' '.$row->u_father_mname.' '.$row->u_father_lname)	, 
		   
		   db_to_calen( $row->u_dob ) ,
		   
		   $row->u_age_year.' yr,'.$row->u_age_month.' mnth,'.$row->u_age_day.' days',
		   
		   strtoupper($row->u_sex),
		   
		   strtoupper($row->u_nationality),
		   
		   strtoupper($edu_qual),
	       
	       $row->u_num_attempt,
	       
		   $row->u_num_attempt,  
	       
		   strtoupper($row->u_college_recog),
		   
		   strtoupper($row->u_is_married),
	       
	       strtoupper($row->sp_nation),
	       
		   strtoupper($row->u_pmrc_num),
		   
		   strtoupper($row->u_grad_from),
	       
		   $row->u_dd_num,
		   
		   db_to_calen($row->u_dd_date),
		   
		   db_to_calen( $row->reg_createdon ) ,
		   
		   );
		   $i++;// line added by preeti on 28th feb 14
	   }
	   
	   
	   $xls = new Excel_XML;
	   
	   $xls->addArray ($field_array);
	   
	   $xls->addArray ($data_array);
	   
	   $file_name = date('Y-m-d');
	   
	   $xls->generateXML ( $file_name );
	}
	
	public function save_date()
	{
		
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$data['errmsg']  = '';
		
		// below code added by preeti on 2nd apr 14 for black-box testing 
		
		$this->form_validation->set_rules('ld_start', 'Opening Date', 'trim|required|callback_date_validate'); // code modified on 12th apr 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('ld_date', 'Last Date', 'trim|required|callback_date_validate'); // code modified on 12th apr 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('ld_validity', 'Validity Days', 'trim|required|integer|xss_clean'); // code modified on 27th mar 14 by preeti for black-box testing
		
		$this->form_validation->set_rules('ld_otp_hours', 'OTP Validity(Hours)', 'trim|required|integer|xss_clean');// code modified on 27th mar 14 by preeti for black-box testing
		
		if( $this->form_validation->run() == TRUE )
		{			
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) )
			{
			
				$res = $this->admin_model->save_date();
			
				if( $res )
				{
					$data['errmsg'] = "Values Saved Successfully";
					
					// below code added by preeti on 22nd apr 14 for manual testing
								
					$errmsg = $data['errmsg'];			
								
					$errmsg = urlencode( $errmsg );
											
					$title = urlencode( "Set Last Date" );
											
					redirect('admin/show_message/'.$title.'/'.$errmsg);
					
				}
				else 
				{
					$data['errmsg'] = "Values could not be Saved";	
					
					// below code added by preeti on 22nd apr 14 for manual testing
								
					$errmsg = $data['errmsg'];			
								
					$errmsg = urlencode( $errmsg );
											
					$title = urlencode( "Set Last Date" );
											
					redirect('admin/show_message/'.$title.'/'.$errmsg);
				}
				
			}
			else 
			{
				$data['errmsg'] = "Values could not be Saved";	
				
				// below code added by preeti on 22nd apr 14 for manual testing
								
				$errmsg = $data['errmsg'];			
								
				$errmsg = urlencode( $errmsg );
											
				$title = urlencode( "Set Last Date" );
											
				redirect('admin/show_message/'.$title.'/'.$errmsg);
			}		
			
		}	
		
		$res = $this->admin_model->get_last_date();
		
		if( $res )
		{
			// below code added by preeti on 2nd apr 14 for black-box testing
			
			if($res->ld_start == '0000-00-00' )
			{
				$data['ld_start'] = '';	
			}
			else
			{
				$data['ld_start'] = db_to_calen( $res->ld_start ) ;		
			}	
				
			
			
			//$data['ld_date'] = db_to_calen( $res->ld_date ) ; // commented by preeti on 2nd apr 14
			
			// below code added by preeti on 2nd apr 14 for black-box testing
			
			if($res->ld_date == '0000-00-00' )
			{
				$data['ld_date'] = '';	
			}
			else
			{
				$data['ld_date'] = db_to_calen( $res->ld_date ) ;		
			}	
			
			$data['ld_validity'] = $res->ld_validity;
			
			$data['ld_otp_hours'] = $res->ld_otp_hours;						
					
		}
		else
		{
			$data['ld_start'] = '' ;// code added by preeti on 2nd apr 14 for black-box testing
			
			$data['ld_date'] = '';
			
			$data['ld_validity'] = '';	
			
			$data['ld_otp_hours'] = '';		
		}
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_set_date', $data);		
		
	}
	
	
	public function set_date() 
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		// get the row value from the last_date table if it exists
		
		$res = $this->admin_model->get_last_date();
		
		if( $res )
		{
			$data['ld_start'] = db_to_calen( $res->ld_start ) ;//  code added by preeti on 3rd apr 14
			
			
			$data['ld_date'] = db_to_calen( $res->ld_date ) ;
			
			$data['ld_validity'] = $res->ld_validity;
			
			$data['ld_otp_hours'] = $res->ld_otp_hours;	
		}
		else
		{
			$data['ld_start'] = '' ;//  code added by preeti on 3rd apr 14
			
			$data['ld_date'] = '';
			
			$data['ld_validity'] = '';
			
			$data['ld_otp_hours'] = '';	
		}	
		
		$data['errmsg'] = ''; 
		
		// below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_set_date', $data);
	}
	
	// below function added on 27th mar 14 by pavan for black-box testing
	
		 /** 
	 *    Method is used to validate strings to allow alpha
	 *    numeric spaces underscores and dashes ONLY.
	 *    @param $str    String    The item to be validated.
	 *    @return BOOLEAN   True if passed validation false if otherwise.
	 */
	function _alpha_dash_space($str_in = '')
	{
	    if (! preg_match("/^([-a-z0-9_ ])+$/i", $str_in))
	    {
	        $this->form_validation->set_message('_alpha_dash_space', 'The %s field may only contain alpha-numeric characters, spaces, underscores, and dashes.');
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}
	
	
	// below code added on 27th mar 14 by preeti for black-box testing
	
	function alpha_and_space($str) // only for alpha and space
	{
	    if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters and space.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	function alpha_at_space($str) // only for alpha and space and @
	{
		 //if( ! preg_match("/^([-a-z@_ ])+$/i", $str) )
		 if( ! preg_match("/^([-a-z_@\. ])+$/i", $str) )
		 {
		    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
					
		    	return FALSE;
		 }
		else 
		{
			return TRUE;	
		}	
			
	}
	
	// below code added by preeti on 28th mar 14 for black-box testing
	
	/*function random_num() // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, 32);
		
		return 	$random;
	}*/
	
	
	// above commented below added by preeti on 21st apr 14 for manual testing
	
	function random_num( $num = 32 ) // returns a 32-bit alpha-numeric number
	{
		$random = substr(md5(rand()), 0, $num);
		
		return 	$random;
	}
	
	// below function added by preeti on 3rd apr 14
	
	function random_only_num( $length ) 
	{ 
		$random= "";
	
		srand((double)microtime()*1000000);
		
		$data = "0123456789";
	
		for($i = 0; $i < $length; $i++) 
		{ 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}
		
		$random = substr($random, 0, $length);
	
		return $random; 
	}
	
	
	
	
	function mail_test()
	{
		$config = array(
				
				'protocol' => 'smtp',
										
				'smtp_host' => 'relay.nic.in',
										
				'smtp_port' => 25,
										
				'mailtype' => 'html',
					        
				'newline' => '\r\n',
					        
				'charset' => 'utf-8' //default charset				
									
				);	
				
				
			/*$config = Array(
		    
		    'protocol' => 'smtp',
		    
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    
		    'smtp_port' => 465,
		    
		    'smtp_user' => 'rahejapreetiemail@gmail.com',
		    
		    'smtp_pass' => '',
		    
		    'mailtype'  => 'html', 
		    
		    'charset'   => 'iso-8859-1'
			
			);*/ // 
			
			$this->load->library('email', $config);
			
			$this->email->set_newline("\r\n");
			
			// Set to, from, message, etc.
			
			$this->email->from('preeti.nhq@nic.in', 'IAMC');
    
    		$this->email->to('rahejapreeti@gmail.com'); 

    		$this->email->subject('Email Test');
    
       		$this->email->message('Testing the email class.'); 
			        
			$res_send = $this->email->send();				
				
				
			if( $res_send )
			{
				echo "mail sent";
			}
			else 
			{
				echo $this->email->print_debugger();
					
				echo "mail not sent";
			}
	}

	// below function added by preeti on 17th apr 14
	
	function alpha_num_at_space($str) // only for alpha and space and @ and numerics
	{
		 //if( ! preg_match("/^([-a-z@_ ])+$/i", $str) )
		 if( ! preg_match("/^([-0-9a-z_@\. ])+$/i", $str) )
		 {
		    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
					
		    	return FALSE;
		 }
		 else 
		 {
			return TRUE;	
		 }	
			
	}	
	
	// below function added by preeti on 24th apr 14 to redirect fatal error page
	
	function fatal() 
	{	
		$data['errmsg'] = 'There is something that didnt go well Here...'; 
			
		$data['title'] = "Something didnt go well";
		
		$this->load->view('admin_msg', $data);
	}
	
	// below function added by preeti on 26th apr 14
	
	public function audit_user( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$this->table->set_heading('S.No', 'Title', 'Date', 'Author', 'Edit', 'Delete');
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/audit_user/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/audit_user'.$url;
		
		$config['total_rows'] = $this->admin_model->get_num_audit_user();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('logu_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		// below code added by preeti on 11th apr 14
		
		if( $offset )  
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}
		
		$data['keyword'] = '';
		
		$data['doc_status'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_audit_user_limit( $config['per_page'], $offset);
		
		// below lines modified by preeti on 6th mar 14
		
		if( $errmsg != '' && !is_numeric( $errmsg ) && $this->alpha_and_space( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;
		}
		else
		{
			$data['errmsg'] = '';
		}
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_audituser_list', $data);
	} 

	// below function added by preeti on 26th apr 14
	
	public function audit_admin( $errmsg = '' )
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}

		//$this->table->set_heading('S.No', 'Title', 'Date', 'Author', 'Edit', 'Delete');
		
		//$config['base_url']	= 'http://'.$_SERVER['SERVER_NAME'].'/iamc/admin/audit_admin/';
		
		// above line commented and below line added by preeti on 16th may 14
		
		$config['base_url']	= base_url().'admin/audit_admin'.$url;
		
		$config['total_rows'] = $this->admin_model->get_num_audit_admin();
		
		$config['per_page'] = 10;
		
		//$config['num_links'] = 20;
		
		$config['num_links'] = 10; // above line commented and this line added by preeti on 2nd jun 14 
		
		$config['first_link'] = '&laquo; First';
		
		$config['last_link'] = 'Last &raquo;';
		
		
		$config['full_tag_open'] = '<div id="pagination">';
		
		$config['full_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('loga_id', 'desc');
		
		if($this->uri->segment(3) == '')
		{
			$offset = 0;
		}
		else 
		{
			$offset = $this->uri->segment(3);	
		}
		
		// below code added by preeti on 11th apr 14
		
		if( $offset )  
		{
			if( !$this->only_num( $offset ) )
			{
				$offset = 0;
			}
		}
		
		$data['keyword'] = '';
		
		$data['doc_status'] = '';
		
		$data['per_page'] = $config['per_page'];
		
		$data['records'] = $this->admin_model->get_audit_admin_limit( $config['per_page'], $offset);
		
		// below lines modified by preeti on 6th mar 14
		
		if( $errmsg != '' && !is_numeric( $errmsg ) && $this->alpha_and_space( $errmsg ) )
		{
			$data['errmsg'] = $errmsg;
		}
		else
		{
			$data['errmsg'] = '';
		}
		
		//below line added by preeti on 21st apr 14 for manual testing
		
		$data['admin_random'] = $this->session->userdata('admin_random');
		
		$this->load->view('admin_auditadmin_list', $data);
	} 

	// below function added by preeti on 26th apr 14
	
	public function audit_user_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('logu_ids[]', 'Log ID', 'required|integer|max_length[11]');
		
		if( $this->form_validation->run() == TRUE )
		{
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
		
				$id_arr = $this->input->post('logu_ids');
				
				$res = $this->admin_model->audit_user_del( $id_arr );
			}
			
		
		}
		
		redirect('admin/audit_user');
		
	}
	
	// below function added by preeti on 26th apr 14
	
	public function audit_admin_del()
	{
		if( !$this->check_session() )
		{
			redirect('admin/index');
			
			exit;
		}
		
		$this->form_validation->set_rules('loga_ids[]', 'Log ID', 'required|integer|max_length[11]');
		
		if( $this->form_validation->run() == TRUE )
		{
			if( $this->input->post('admin_random') != ''  && strlen( $this->input->post('admin_random') ) == 32   
			
			&& $this->session->userdata('admin_random') != ''   && ( $this->input->post('admin_random') == $this->session->userdata('admin_random') ) ) 
			{
		
				$id_arr = $this->input->post('loga_ids');
				
				$res = $this->admin_model->audit_admin_del( $id_arr );
			}
			
		
		}
		
		redirect('admin/audit_admin');
		
	}
	
	
	/*function create_pdf()// added by preeti on 19th may 14 for testing
	{
		 //$image_src = getenv("DOCUMENT_ROOT").'/uploads/photo/img6.png';
		 
		 $image_src = FCPATH.'uploads/photo/img6.png';
		
		
		
		 $html = "<style>
		
		.pdf{height:auto;width:100%;font-size:10px;}
		
		.left-col{float:left;width:40%;}
		
		.right-col{float:left;width:40%;}
		
		.image-col{float:right;width:20%;}
		
		</style>
		
		<div class='pdf'>
		
		<h3>Form Details</h3>
		
		<div class='left-col' >Registration No.</div>
		
		<div class='right-col' >201405SSC1050</div>
		
		<div class='image-col' ><img src='".$image_src."' /></div>";
	
		
		
		$this->load->library('mpdf/mpdf'); 
	
		 $paper='A4';
		
		 $mpdf=new mPDF('hi',$paper);
		        
		 $data=array();
		
		 $a4="preeti_form.pdf";
		 
		 //$pdfFilePath = FCPATH.$a4;
		 
		 $pdfFilePath = FCPATH."uploads/form/preeti_form.pdf";
		 
		 echo " pdf file path is ".$pdfFilePath;
		 
		        
		 $res_write = $mpdf->WriteHTML($html);
		 
		 echo " write function return ".$res_write;
		 
		 $res_op = $mpdf->Output($pdfFilePath, 'F');     
		 
		 echo " output function return ".$res_op;
		
	}*/
	
	// 26th may 14
	function send_sms_curl( $message, $mnumber )
	{
		
		$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$message."&mnumber=".$mnumber."&signature=NICSUP"; 
		
		//although we have used https, you can also use http
		
		$ch = curl_init();
		
		//initialize curl handle 
		
		curl_setopt($ch, CURLOPT_URL, $url); 
		
		//set the url
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		//return as a variable 
		
		curl_setopt($ch, CURLOPT_POST, 1);
		
		//set POST method 
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
		
		//set the POST variables
		
		$response = curl_exec($ch); 
		
		//run the whole process and return the response
		
		curl_close($ch); 
		
		if(strpos($response, "Message Accepted") === FALSE )
		{
			return FALSE;	
		}
		else 
		{
			return TRUE;
		}
		   
	}
	  
}
