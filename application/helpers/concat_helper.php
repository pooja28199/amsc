<?php

/* required files and their download locations:
 * FPDF 1.6: http://www.fpdf.org/ (then go to downloads)
 * FPDI 1.3.1: http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/
 * FPDI_TPL: http://www.setasign.de/products/pdf-php-solutions/fpdi/downloads/
 */

require_once('fpdf/fpdf.php');
require_once('FPDI_1.3.1/fpdi.php');

class concat_pdf extends FPDI {

	var $files = array();
	
	function setFiles($files){
		$this->files = $files;
	}
	
	function addFile($file){
		$this->files []= $file;
	}
	
	function concat(){
		foreach($this->files as $file){
			$pagecount = $this->setSourceFile($file);
			for ($i = 1; $i <= $pagecount; $i++){
				$tplidx = $this->ImportPage($i);
				$s = $this->getTemplatesize($tplidx);
				$this->AddPage('P', array($s['w'], $s['h']));
				$this->useTemplate($tplidx);
			}
		}
	}

}


?>