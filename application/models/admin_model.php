<?php
class Admin_model extends CI_Model 
{
	/*function validate() 
	{

		$this -> db -> where('admin_username', $this -> input -> post('admin_username'));

		// below code commented for black-box testing by preeti on 26th mar 14

		//$this -> db -> where('admin_password', md5($this -> input -> post('admin_password')));

		// below code added for black-box testing by preeti on 26th mar 14

		$this -> db -> where('admin_password', $this -> input -> post('admin_password_encode') );
		
		
		$result = $this -> db -> get('admin');

		if ($result -> num_rows == 1) {
			return true;
		}

	}*/ //  code commented by preeti on 28th mar 14 for black-box testing
	
	// code added by preeti on 28th mar 14 for black-box testing
	
	function validate() 
	{
		$this -> db -> where('admin_username', $this -> input -> post('admin_username'));
		//$this -> db -> where('admin_otp', $this -> input -> post('otp'));

		$result = $this -> db -> get('admin');

		if ($result -> num_rows  > 0 ) 
		{
			$res_arr = $result->result();
			
			$admin_password = $res_arr[0]->admin_password;
			
			//$salt = $this->input->post('salt'); // commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('admin_salt'); // added by preeti on 22nd apr 14 for manual testing
			
			$admin_password_encode = $this->input->post('admin_password_encode'); // saled md5
			
			$result_pass = md5( $admin_password.$salt );
			
			if( $admin_password_encode === $result_pass  )
			{
				return true;
			}
			else 
			{
				return true;	
			}			
			
		}
		else 
		{
			return false;	
		}

	}
	//pavan 22-04 
	function get_search_result34( $limit, $offset , $sort_by, $sort_order ) 
	{
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name,log.logon_date as logon_date,
		log.logoff_date as logoff_date
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN logondetails1 log ON reg.reg_regno = log.u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' ";

				
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		
        $res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	
	function clear_db() 
	{
		$this->db->empty_table('current_employer');
			
		$this->db->empty_table('new_name');
			
		$this -> db -> empty_table('permanent');

		$this -> db -> empty_table('postal');

		$this -> db -> empty_table('post_grad');

		$this -> db -> empty_table('previous_ssc');

		$this -> db -> empty_table('spouse');

		$this -> db -> empty_table('user');
		
		$this->db->empty_table('managecall');
		$this->db->empty_table('login_user');
		//$this->db->empty_table('login_admin');

		$res = $this -> db -> empty_table('register');

		

		
		$path = "uploads/photo/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}
		
		
		$path = "uploads/dd/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	} 
		
		$path = "uploads/attempt1/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	} 
		
		$path = "uploads/attempt2/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	} 
		
		
		$path = "uploads/pmrc/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	} 
		
		
		$path = "uploads/emp_noc/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	} 
		  
		$path = "uploads/new_name/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}  
		
		
		$path = "uploads/prev_rel_order/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}   
		  
		$path = "uploads/dob/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}

		$path = "uploads/intern/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}
		
		
		$path = "uploads/mbbs/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}	
		
		
		$path = "uploads/pg/pg1/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}		


		$path = "uploads/pg/pg2/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}		
		
		$path = "uploads/pg/pg3/";		
		
		$files = glob($path.'*'); // get all file names
		
    	foreach($files as $file)
    	{       
      		if(is_file($file))
        
        	unlink($file);       	
    	}
		
		return $res; // line added by preeti on 5th mar 14	
	}
	
	
	/*function check_old_db_password() 
	{
		//$old_password = $this -> input -> post('opassword');// code commented for black-box testing by preeti on 26th mar 14
		
		$old_password = $this -> input -> post('opassword_encode');// code added for black-box testing by preeti on 26th mar 14
		
		$this->db->select('clear_id');
		
		$this->db->from('clear');
		
		//$this->db->where('clear_pass', MD5($old_password));// code commented for black-box testing by preeti on 26th mar 14
		
		$this->db->where('clear_pass', $old_password );// code added for black-box testing by preeti on 26th mar 14

		$res = $this->db->get();
		
		if ($res -> num_rows() == 1) 
		{
			return TRUE;
		}
		else 
		{
			return FALSE;
		}

	}*/ //  code commented by preeti on 28th mar 14 for black-box testing
	
	// code added by preeti on 28th mar 14 for black-box testing
	
	
	
	// below function added by preeti on 26th apr 14	
		
	function add_login( ) 
	{
		$data=array();
		
		$data['loga_action'] = 'login';
		
		$data['loga_time'] = date('Y-m-d H:i:s');
		
		//$data['loga_ip'] = $this->input->ip_address();
		
		$data['loga_user']= $this->session->userdata('username') ;
							
		$this->db->insert('login_admin', $data);
			
	}
	
	// below function added by preeti on 26th apr 14	
			
	function add_logout(  )
	{
		$data=array();
		
		$data['loga_action'] = 'logout';
		
		$data['loga_time'] = date('Y-m-d H:i:s');
		
		$data['loga_ip'] = $this->input->ip_address();
		
		$data['loga_user']= $this->session->userdata('username') ;
							
		$this->db->insert('login_admin', $data);
	}


	// below function added by preeti on 26th apr 14	
		
	function add_pass( $action ) 
	{
		$data=array();
		
		$data['loga_action'] = $action;
		
		$data['loga_time'] = date('Y-m-d H:i:s');
		
		$data['loga_ip'] = $this->input->ip_address();
		
		$data['loga_user']= $this->session->userdata('username') ;
							
		$this->db->insert('login_admin', $data);
			
	}
	
	
	
	
	// below function added by preeti on 26th apr 14	
		
	function add_logfail( $username ) 
	{
		$data=array();
		
		$data['loga_action'] = 'logfail';
		
		$data['loga_time'] = date('Y-m-d H:i:s');
		
		$data['loga_ip'] = $this->input->ip_address();
		
		$data['loga_user']= $username ;
							
		$this->db->insert('login_admin', $data);
			
	}
	
	
	function get_user_numb() 
	{
		
		//$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN logondetails log ON reg.reg_regno = log.logondetails_u_regno
		
		 
		
		
			
		WHERE reg_status = 'c' ";

		
		
		$res = $this -> db -> query($qry);

		//return $res -> num_rows();
	}
	
	
	
	function get_dak_list($status)
	{
	
	$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		" ;
		
		
			
		$qry.="WHERE u.u_status = 'appr' and reg.dak_status='$status'  ";
	
	$res = $this -> db -> query($qry);

		
			$res_arr = $res -> result();

			return $res_arr;
		
	
	
	}
	
	
	
	function check_old_db_password() 
	{
		$old_password = $this -> input -> post('opassword_encode');// code added for black-box testing by preeti on 26th mar 14
			
		$this->db->select('clear_id, clear_pass');
		
		$this->db->from('clear');
		
		$res = $this->db->get();
		
		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res->result();
			
			$clear_pass = $res_arr[0]->clear_pass;
			
			//$salt = $this->input->post('salt');// commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('admin_salt');// added by preeti on 22nd apr 14 for manual testing			
			
			$result_pass = md5( $clear_pass.$salt );
			
			if( $result_pass === $old_password )
			{
				return TRUE;	
			}
			else 
			{
				return FALSE;	
			}			
		
		}
		else 
		{
			return FALSE;
		}

	}
	
	
	// code added by preeti on 22nd apr 14 for manual testing
	
	function check_old_new_pass_same( $old_password, $new_password )// old pass is salted md5 value and password is md5 value 
	{
		$salt = $this->session->userdata('admin_salt');			
			
		$result_pass = md5( $new_password.$salt );
			
		if( $result_pass === $old_password )
		{
			return TRUE; 	
		}
		else 
		{
			return FALSE;	
		}
	}
	
	
	// below function added by preeti on 3rd mar 14
	
	function update_db_password() 
	{
		$data = array();

		
		//$data['clear_pass'] = md5($this -> input -> post('admin_password'));// code commented for black-box testing by preeti on 26th mar 14
		
		$data['clear_pass'] = $this -> input -> post('admin_password_encode');// code added for black-box testing by preeti on 26th mar 14

		$res = $this -> db -> update('clear', $data);

		if ($res) 
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}
	}
	
	// below function added by preeti on 3rd mar 14	
	
	/*function clear_verify_pass()
	{
		//$old_password = $this -> input -> post('clear_pass'); // code commented for black-box testing by preeti on 26th mar 14
		
		$old_password = $this -> input -> post('clear_pass_encode'); // code added for black-box testing by preeti on 26th mar 14
		
		$this->db->select('clear_id');
		
		$this->db->from('clear');
		
		//$this->db->where('clear_pass', MD5($old_password));// code commented for black-box testing by preeti on 26th mar 14
		
		$this->db->where('clear_pass', $old_password );// code added for black-box testing by preeti on 26th mar 14

		$res = $this->db->get();
		
		if ($res -> num_rows() == 1) 
		{
			return TRUE;
		}
		else 
		{
			return FALSE;
		}
	}*/ //  code commented by preeti on 28th mar 14 for black-box testing
	
	// code added by preeti on 28th mar 14 for black-box testing
	
	function clear_verify_pass()
	{
		$old_password = $this -> input -> post('clear_pass_encode'); // code added for black-box testing by preeti on 26th mar 14
		
		$this->db->select('clear_id, clear_pass');
		
		$this->db->from('clear');
		
		$res = $this->db->get();
		
		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res->result();
			
			$clear_pass = $res_arr[0]->clear_pass;
			
			//$salt = $this->input->post('salt');// commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('admin_salt');// added by preeti on 22nd apr 14 for manual testing
			
			$result_pass = md5( $clear_pass.$salt );
			
			if( $result_pass == $old_password )
			{
				return TRUE;	
			}
			else 
			{
				return FALSE;	
			}
			
		}
		else 
		{
			return FALSE;
		}
	}
	

	//CODE CHANGED BY PAVAN 13-02-13
	function get_search_result1($keyword,$gender,$state,$start_date,$end_date,$start_age,$end_age) {
		//echo "ss";
		//echo $keyword;
				$qry = "SELECT * 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN temp t ON reg.reg_regno = t.pg_u_regno
			
		WHERE reg_status = 'c' ";

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
			
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$start_date=date("d/m/Y", strtotime($start_date));
				$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$end_date=date("d/m/Y", strtotime($end_date));
				$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
	//echo $qry;
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
function get_search_result121($keyword,$gender,$state,$start_date,$end_date,$start_age,$end_age) {
		//echo "ss";
		//echo $state;
				$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN temp t ON reg.reg_regno = t.pg_u_regno
			
		WHERE reg_status = 'c' AND u_status='appr'";

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
			
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$start_date=date("d/m/Y", strtotime($start_date));
				$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$end_date=date("d/m/Y", strtotime($end_date));
				$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}


		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		//echo $qry;
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
function get_search_result1211($from,$to) {
		//echo "ss";
		//echo $state;
				$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN temp t ON reg.reg_regno = t.pg_u_regno
			
		WHERE reg_status = 'c' AND u_status='appr'";

				//echo $qry;
				if (!empty($from) || !empty($to)) {
					
			$qry .= " AND u.u_regno BETWEEN '".$from."' AND '".$to."'";
			 }
			
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();
			return $res_arr;
		} else {
			return FALSE;
		}
	}
	
function get_search_result122($keyword,$gender,$state,$start_date,$end_date,$start_age,$end_age) {
		//echo "ss";
		//echo $state;
				$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN temp t ON reg.reg_regno = t.pg_u_regno
			
		WHERE reg_status = 'c' AND u_status='rej'";

if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
			
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$start_date=date("d/m/Y", strtotime($start_date));
				$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$end_date=date("d/m/Y", strtotime($end_date));
				$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		//echo $qry;
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
		
	function get_search_result12($reg_no) {
		$qry = " SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email,
reg.reg_fname as fname,reg.reg_lname as lname,reg.reg_mname as u_mname,reg.reg_mobile as mobile,reg_regno as rno, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
 u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,u_sign as u_sign,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
st.new_fname as new_fname,st.new_mname as new_mname,st.new_lname as new_lname,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,sp_nation as sp_nation,u.u_grad_from as u_grad_from,
pg.pg_subject as pg_subject,reg.reg_mobile as reg_mobile,reg.reg_email as reg_email,
sp.sp_mar_date as sp_mar_date,sp.sp_citizenship_date as u_citizenship_date,sp.sp_nation as sp_nation,sp.sp_name as sp_name,
sp.sp_occup as sp_occup,sp.sp_ssc_applied as sp_ssc_applied,u.u_pmrc_reg_date as u_pmrc_reg_date,pg.pg_college as pg_college,  
 pg.pg_univ as pg_univ,pg.pg_year as pg_year,pg.pg_mci_recog as pg_mci_recog,ce.ce_employer as ce_employer,ps.ps_com_date as ps_com_date,
 ps.ps_rel_date as ps_rel_date,poll.perm_address as perm_address,poll.perm_state as perm_state,poll.perm_tel as perm_tel,poll.perm_pin as perm_pin,
		        pm.post_address as post_address,pm.post_state as post_state,pm.post_tel as post_tel,pm.post_pin as post_pin
		        
		
		FROM user u,register reg,spouse sp,post_grad pg,new_name st,current_employer ce,previous_ssc ps,permanent poll,postal pm
		
		
		WHERE u.u_regno=reg.reg_regno AND u.u_regno=sp.sp_u_regno AND pm.post_u_regno=u.u_regno AND u.u_regno=pg.pg_u_regno AND u.u_regno=poll.perm_u_regno AND 
		
		u.u_regno= '" . $reg_no . "'";

		//echo $qry;

		$res = $this -> db -> query($qry);

		//print $res;
		if ($res -> num_rows() > 012) {
			foreach ($res->result() as $obj) {
				return $obj;
			}
		}

	}

	function get_search_result13($reg_no) {
		$qry = " SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email,
reg.reg_fname as fname,reg.reg_lname as lname,reg.reg_mname as u_mname,reg.reg_mobile as mobile,
reg_regno as rno, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
 u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,pm.post_state as posst,sp_nation as sp_nation,u.u_grad_from as u_grad_from,
pg.pg_degree as pg_degree,pg.pg_subject as pg_subject,psgm.ps_rel_order as relorder,st.new_name_proof as nma
 
		
		FROM user u 
		
		
		LEFT JOIN current_employer ce ON u.u_regno= ce.ce_u_regno
		
		LEFT JOIN new_name st ON u.u_regno = st.new_u_regno
		
		LEFT JOIN permanent poll ON u.u_regno = poll.perm_u_regno
		
			LEFT JOIN register reg ON u.u_regno = reg.reg_regno
		LEFT JOIN postal pm ON u.u_regno= pm.post_u_regno
		
		LEFT JOIN post_grad pg ON u.u_regno = pg.pg_u_regno
		
		LEFT JOIN previous_ssc psgm ON u.u_regno = psgm.ps_u_regno
		
		LEFT JOIN spouse sm ON u.u_regno = sm.sp_u_regno
		
		
		WHERE u.u_regno='" . $reg_no . "'";

		//echo $qry;
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}

	function get_detail2($doc_id) {
		//echo "pris";

		$sql = "SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, u.u_grad_from as u_grad_from, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
   u.u_pmrc_reg_date as u_pmrc_reg_date, u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,
		        ce.ce_emp_noc as cemp,ce.ce_employer as ce_employer,st.new_fname as new_fname,
		        st.new_mname as new_mname,st.new_lname as new_lname,st.new_name_proof as nma,
		        poll.perm_address as perm_address,poll.perm_state as perm_state,poll.perm_tel as perm_tel,poll.perm_pin as perm_pin,
		        pm.post_address as post_address,pm.post_state as post_state,pm.post_tel as post_tel,pm.post_pin as post_pin,
		        pgm.pg_degree as pg_degree,pgm.pg_subject as pg_subject,pgm.pg_college as pg_college,pgm.pg_univ as pg_univ,pgm.pg_year as pg_year,
		        pgm.pg_mci_recog as pg_mci_recog,
		        psgm.ps_com_date as ps_com_date,psgm.ps_rel_date as ps_rel_date,psgm.ps_rel_order as ps_rel_order,
		        sm.sp_mar_date as sp_mar_date,sm.sp_citizenship_date as u_citizenship_date,sm.sp_nation as sp_nation,sm.sp_name as sp_name,
		        sm.sp_occup as sp_occup,sm.sp_ssc_applied as sp_ssc_applied,reg.reg_mobile as reg_mobile,reg.reg_email as reg_email
		         FROM user u 
		LEFT JOIN current_employer ce ON u.u_regno= ce.ce_u_regno
		
		LEFT JOIN new_name st ON u.u_regno = st.new_u_regno
		
		LEFT JOIN permanent poll ON u.u_regno = poll.perm_u_regno
		
			LEFT JOIN register reg ON u.u_regno = reg.reg_regno
		LEFT JOIN postal pm ON u.u_regno= pm.post_u_regno
		
		LEFT JOIN post_grad pgm ON u.u_regno = pgm.pg_u_regno
		
		LEFT JOIN previous_ssc psgm ON u.u_regno = psgm.ps_u_regno
		
		LEFT JOIN spouse sm ON u.u_regno = sm.sp_u_regno
		
		
		WHERE u.u_regno= ?";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $obj) {
				return $obj;
			}
		}

		/*$res_arr =  $res->result();

		 return $res_arr[0];	*/

	}

	function get_admin_id($admin_username) {
		$qry = " SELECT admin_id FROM admin 
		
		WHERE admin_username = ?  ";

		$res = $this -> db -> query($qry, array($admin_username));

		$res_arr = $res -> result();

		return $res_arr[0] -> admin_id;
	}

	// below function modified by preeti on 3rd apr 14
	
	function get_records_limit($limit, $offset) 
	{
		$qry = " SELECT *, CONCAT( u.u_fname, ' ', u.u_mname, ' ', u.u_lname ) name
		
		FROM user u
		
		JOIN register reg  ON u.u_regno = reg.reg_regno 
		
		WHERE reg.reg_status = 'c'
		
		AND u.u_status = 'appr'
		
		ORDER BY u.u_id DESC  
		
		LIMIT " . $offset . ", " . $limit;
		
		$res = $this -> db -> query($qry);

		return $res -> result();
	}
	
	function get_user_limit($limit, $offset , $sort_by, $sort_order )  // modified by preeti on 26th feb 14
	{
		$qry = " SELECT *, CONCAT( u.u_fname, ' ', u.u_mname, ' ', u.u_lname ) as name		
		
		FROM user u
		
		JOIN register reg  ON u.u_regno = reg.reg_regno 
		
		WHERE reg.reg_status = 'c'
		
		ORDER BY ".$sort_by." ".$sort_order."  
		
		LIMIT " . $offset . ", " . $limit;

		$res = $this -> db -> query($qry);

		return $res -> result();
	}

	/*function del_record($reg_id_arr) {
		$reg_ids = implode(',', $reg_id_arr);
		//echo $reg_ids;

		$qry = " SELECT u.u_dd_copy , u.u_attempt_cert, 
		
		ce.ce_emp_noc, new.new_name_proof, ps.ps_rel_order, reg.reg_regno		
		
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN current_employer ce ON  u.u_regno = ce.ce_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		WHERE reg.reg_id IN ( " . $reg_ids . " ) ";

		//$res = $this->db->query($qry, $reg_id);

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		//$row = $res_arr[0];

		foreach ($res_arr as $row) {
			$reg_regno = $row -> reg_regno;

			$dd_path = '';

			$u_photo = '';

			$u_attempt_cert = '';

			$ce_emp_noc = '';

			$new_name_proof = '';

			$ps_rel_order = '';

			$u_dob_proof = '';

			$u_pmrc_copy = '';

			if ($row -> u_photo != '') {
				$u_photo = "uploads/photo/" . $row -> u_photo;
			}

			if ($row -> u_dd_copy != '') {
				$dd_path = "uploads/dd/" . $row -> u_dd_copy;
			}

			if ($row -> u_attempt_cert != '') {
				$u_attempt_cert = "uploads/attempt/" . $row -> u_attempt_cert;
			}

			if ($row -> ce_emp_noc != '') {
				$ce_emp_noc = "uploads/emp_noc/" . $row -> ce_emp_noc;
			}

			if ($row -> new_name_proof != '') {
				$new_name_proof = "uploads/new_name/" . $row -> new_name_proof;
			}

			if ($row -> ps_rel_order != '') {
				$ps_rel_order = "uploads/prev_rel_order/" . $row -> ps_rel_order;
			}

			if ($row -> u_dob_proof != '') {
				$u_dob_proof = "uploads/dob/" . $row -> u_dob_proof;
			}

			if ($row -> u_pmrc_copy != '') {
				$u_pmrc_copy = "uploads/dob/" . $row -> u_pmrc_copy;
			}

			// delete from current_employer table

			$this -> db -> where('ce_u_regno', $reg_regno);

			$this -> db -> delete('current_employer');

			// delete from new_name table

			$this -> db -> where('new_u_regno', $reg_regno);

			$this -> db -> delete('new_name');

			// delete from permanent table

			$this -> db -> where('perm_u_regno', $reg_regno);

			$this -> db -> delete('permanent');

			// delete from postal table

			$this -> db -> where('post_u_regno', $reg_regno);

			$this -> db -> delete('postal');

			// delete from post_grad table

			$this -> db -> where('pg_u_regno', $reg_regno);

			$this -> db -> delete('post_grad');

			// delete from previous_ssc table

			$this -> db -> where('ps_u_regno', $reg_regno);

			$this -> db -> delete('previous_ssc');

			// delete from spouse table

			$this -> db -> where('sp_u_regno', $reg_regno);

			$this -> db -> delete('spouse');

			// delete from user table

			$this -> db -> where('u_regno', $reg_regno);

			$this -> db -> delete('user');

			// delete from register table

			$this -> db -> where('reg_regno', $reg_regno);

			$res = $this -> db -> delete('register');

			if ($dd_path != '') {
				if (file_exists($dd_path)) {
					unlink($dd_path);
				}
			}

			if ($u_photo != '') {
				if (file_exists($u_photo)) {
					unlink($u_photo);
				}
			}

			if ($u_attempt_cert != '') {
				if (file_exists($u_attempt_cert)) {
					unlink($u_attempt_cert);
				}
			}

			if ($ce_emp_noc != '') {
				if (file_exists($ce_emp_noc)) {
					unlink($ce_emp_noc);
				}
			}

			if ($new_name_proof != '') {
				if (file_exists($new_name_proof)) {
					unlink($new_name_proof);
				}
			}

			if ($ps_rel_order != '') {
				if (file_exists($ps_rel_order)) {
					unlink($ps_rel_order);
				}
			}

			if ($u_dob_proof != '') {
				if (file_exists($u_dob_proof)) {
					unlink($u_dob_proof);
				}
			}

			if ($u_pmrc_copy != '') {
				if (file_exists($u_pmrc_copy)) {
					unlink($u_pmrc_copy);
				}
			}

		}

	}*/
	function get_user_limit1($limit, $offset , $sort_by, $sort_order )  // modified by preeti on 26th feb 14
	{
		$qry = " SELECT *, CONCAT( u.u_fname, ' ', u.u_mname, ' ', u.u_lname ) as name		
		
		FROM user u
		
		JOIN register reg  ON u.u_regno = reg.reg_regno 
		
		WHERE reg.reg_status = 'c' AND u.u_status='appr'
		
		ORDER BY ".$sort_by." ".$sort_order."  
		
		LIMIT " . $offset . ", " . $limit;

		$res = $this -> db -> query($qry);

		return $res -> result();
	}
	//mod pavan 27th
	function get_detail4($doc_id) {
		//echo "pris";
$i=0;
		$sql = "SELECT pg_degree FROM post_grad 
				
		WHERE pg_u_regno= ?";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		$db_array=array();
		$dba=array();
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $row) {
			//	$res_arr = $res->result();
		foreach ($row as $key => $value)
            {
                $db_array[$key] = $value;
                //$i++;
                //echo $value;
                $dba[$i]=$value;
                $i++;
            }
			
			}
			
		}
return $dba;
		/*res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
	function get_reg_ids() {
		$qry = " SELECT u_regno FROM user WHERE u_is_pg = 'y'";
	

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
	
function get_detail5($doc_id) {
		//echo "pris";
$i=0;
		$sql = "SELECT pg_subject FROM post_grad 
				
		WHERE pg_u_regno= ?";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		$db_array=array();
		$dba=array();
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $row) {
			//	$res_arr = $res->result();
		foreach ($row as $key => $value)
            {
                $db_array[$key] = $value;
                //$i++;
                //echo $value;
                $dba[$i]=$value;
                $i++;
            }
			
			}
			
		}
return $dba;
		/*res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
function get_detail22($doc_id) {
$sql = "SELECT u_is_pg FROM user 
				
		WHERE u_regno= ?";

		$res = $this -> db -> query($sql, array($doc_id));
		$res_arr = $res -> result();

		return $res_arr[0] -> u_is_pg;
		
	
}
	function get_detail3($doc_id) {
		//echo "pris";
$i=0;
		$sql = "SELECT pg_cert FROM post_grad 
				
		WHERE pg_u_regno= ? order by pg_id";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		$db_array=array();
		$dba=array();
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $row) {
			//	$res_arr = $res->result();
		foreach ($row as $key => $value)
            {
                $db_array[$key] = $value;
                //$i++;
                //echo $value;
                $dba[$i]=$value;
                $i++;
            }
			
			}
			
		}
return $dba;
		/*res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
	
	//
function get_user_limit2($limit, $offset , $sort_by, $sort_order )  // modified by preeti on 26th feb 14
	{
		$qry = " SELECT *, CONCAT( u.u_fname, ' ', u.u_mname, ' ', u.u_lname ) as name		
		
		FROM user u
		
		JOIN register reg  ON u.u_regno = reg.reg_regno 
		
		WHERE reg.reg_status = 'c' AND u.u_status='rej'
		
		ORDER BY ".$sort_by." ".$sort_order."  
		
		LIMIT " . $offset . ", " . $limit;

		$res = $this -> db -> query($qry);

		return $res -> result();
	}

	// below function modified by preeti on 5th mar 14

	function del_record( $reg_id_arr ) 
	{
		$reg_ids = implode(',', $reg_id_arr);
		//echo $reg_ids;

		// below query is modified by preeti on 20th feb 14
		
		$qry = " SELECT *		
		
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN current_employer ce ON  u.u_regno = ce.ce_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		WHERE reg.reg_id IN ( " . $reg_ids . " ) ";
		
		//$res = $this->db->query($qry, $reg_id);

		$res = $this -> db -> query($qry);

		

		$res_arr = $res -> result();

		//$row = $res_arr[0];

		foreach ($res_arr as $row) {
			
			$reg_regno = $row -> reg_regno;

			$dd_path = '';

			$u_photo = '';

			$u_attempt_cert = '';
			
			$u_attempt_cert1 = '';

			$ce_emp_noc = '';

			$new_name_proof = '';

			$ps_rel_order = '';

			$u_dob_proof = '';

			$u_pmrc_copy = '';
			
			
			
			$u_intern_cert = '';// added by preeti on 20th feb 14
			
			$u_mbbs_cert = '';// added by preeti on 20th feb 14
			
			$pg_cert1 = '';
			
			$pg_cert2 = '';
			
			$pg_cert3 = '';
			
			
			// get the pg data
			
			$res_pg = $this->get_pg_data( $reg_regno );
			
			if( $res_pg )
			{
				foreach( $res_pg as $val )
				{
					if( $val->pg_order == 1 )
					{
						$pg_cert1 = $val->pg_cert;	
					}	
					else if( $val->pg_order == 1 )
					{
						$pg_cert2 = $val->pg_cert;	
					}
					else if( $val->pg_order == 1 )
					{
						$pg_cert3 = $val->pg_cert;	
					}
						
				}				
			}
			

			if ($row -> u_photo != '') {
				$u_photo = "uploads/photo/" . $row -> u_photo;
			}

			if ($row -> u_dd_copy != '') {
				$dd_path = "uploads/dd/" . $row -> u_dd_copy;
			}

			if ($row -> u_attempt_cert != '') {
				$u_attempt_cert = "uploads/attempt1/" . $row -> u_attempt_cert;
			}
			
			if ($row -> u_attempt_cert1 != '') {
				$u_attempt_cert1 = "uploads/attempt2/" . $row -> u_attempt_cert1;
			}

			if ($row -> ce_emp_noc != '') {
				$ce_emp_noc = "uploads/emp_noc/" . $row -> ce_emp_noc;
			}

			if ($row -> new_name_proof != '') {
				$new_name_proof = "uploads/new_name/" . $row -> new_name_proof;
			}

			if ($row -> ps_rel_order != '') {
				$ps_rel_order = "uploads/prev_rel_order/" . $row -> ps_rel_order;
			}

			if ($row -> u_dob_proof != '') {
				$u_dob_proof = "uploads/dob/" . $row -> u_dob_proof;
			}

			if ($row -> u_pmrc_copy != '') {
				$u_pmrc_copy = "uploads/dob/" . $row -> u_pmrc_copy;
			}
			
			// below if added by preeti on 20th feb 14
			if ($row -> u_intern_cert != '') 
			{
				$u_intern_cert = "uploads/intern/" . $row -> u_intern_cert;
			}
			
			// below if added by preeti on 20th feb 14
			if ($row -> u_mbbs_cert != '') 
			{
				$u_mbbs_cert = "uploads/mbbs/" . $row -> u_mbbs_cert;
			}
			
			if ($pg_cert1 != '') 
			{
				$pg_cert1 = "uploads/pg/pg1/" . $row -> pg_cert;
			}
			
			if ($pg_cert2 != '') 
			{
				$pg_cert2 = "uploads/pg/pg2/" . $row -> pg_cert;
			}			
			
			if ($pg_cert3 != '') 
			{
				$pg_cert3 = "uploads/pg/pg3/" . $row -> pg_cert;
			}

			// delete from current_employer table

			$this -> db -> where('ce_u_regno', $reg_regno);

			$this -> db -> delete('current_employer');

			// delete from new_name table

			$this -> db -> where('new_u_regno', $reg_regno);

			$this -> db -> delete('new_name');

			// delete from permanent table

			$this -> db -> where('perm_u_regno', $reg_regno);

			$this -> db -> delete('permanent');

			// delete from postal table

			$this -> db -> where('post_u_regno', $reg_regno);

			$this -> db -> delete('postal');

			// delete from post_grad table

			$this -> db -> where('pg_u_regno', $reg_regno);

			$this -> db -> delete('post_grad');

			// delete from previous_ssc table

			$this -> db -> where('ps_u_regno', $reg_regno);

			$this -> db -> delete('previous_ssc');

			// delete from spouse table

			$this -> db -> where('sp_u_regno', $reg_regno);

			$this -> db -> delete('spouse');

			// delete from user table

			$this -> db -> where('u_regno', $reg_regno);

			$this -> db -> delete('user');

			// delete from register table

			$this -> db -> where('reg_regno', $reg_regno);

			$res = $this -> db -> delete('register');

			if ($dd_path != '') {
				if (file_exists($dd_path)) {
					unlink($dd_path);
				}
			}

			if ($u_photo != '') {
				if (file_exists($u_photo)) {
					unlink($u_photo);
				}
			}

			if ($u_attempt_cert != '') {
				if (file_exists($u_attempt_cert)) {
					unlink($u_attempt_cert);
				}
			}
			
			
			if ($u_attempt_cert1 != '') {
				if (file_exists($u_attempt_cert1)) {
					unlink($u_attempt_cert1);
				}
			}

			if ($ce_emp_noc != '') {
				if (file_exists($ce_emp_noc)) {
					unlink($ce_emp_noc);
				}
			}

			if ($new_name_proof != '') {
				if (file_exists($new_name_proof)) {
					unlink($new_name_proof);
				}
			}

			if ($ps_rel_order != '') {
				if (file_exists($ps_rel_order)) {
					unlink($ps_rel_order);
				}
			}

			if ($u_dob_proof != '') {
				if (file_exists($u_dob_proof)) {
					unlink($u_dob_proof);
				}
			}

			if ($u_pmrc_copy != '') {
				if (file_exists($u_pmrc_copy)) {
					unlink($u_pmrc_copy);
				}
			}
			
			
			// below if added by preeti on 20th feb 14
			
			if ($u_intern_cert != '') {
				if (file_exists($u_intern_cert)) {
					unlink($u_intern_cert);
				}
			}
			
			// below if added by preeti on 20th feb 14
			
			if ($u_mbbs_cert != '') {
				if (file_exists($u_mbbs_cert)) {
					unlink($u_mbbs_cert);
				}
			}
			
			if ($pg_cert1 != '') 
			{
				if (file_exists( $pg_cert1 )) 
				{
					unlink( $pg_cert1 );
				}
			}
			 
			if ($pg_cert2 != '') 
			{
				if (file_exists( $pg_cert2 )) 
				{
					unlink( $pg_cert2 );
				}
			} 
			
			
			if ($pg_cert3 != '') 
			{
				if (file_exists( $pg_cert3 )) 
				{
					unlink( $pg_cert3 );
				}
			}

		}

	}

	// code added on 27th mar 14 by preeti for black-box testing	

	function alpha_num_dash($str) // for alpha-numeric with space, dash, underscore
    {
        return ( ! preg_match("/^([-a-z0-9_-\s\/])+$/i", $str))? FALSE : TRUE;
    }
	
	
	// below code added by preeti on 9th apr 14 for black-box testing
	
	function alpha_at_space($str) // only for alpha and space and @
	{
		 if( ! preg_match("/^([-a-z_@\. ])+$/i", $str) )
		 {
			    //	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
						
		    	return FALSE;
		 }
		else 
		{
			return TRUE;	
		}	
				
	}
	

	function get_search_result( $limit, $offset , $sort_by, $sort_order ) 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_num_at_space( $keyword ) )
			{
				$keyword = '';
			}
		}	
		
			
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
				$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		

		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		// above code added by preeti on 26th feb 14 
		
		$res = $this->get_last_date();
		$icenter=$res->icenter;

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE (reg_status = 'c' or reg_status = 'i') AND reg.icenter='$icenter' ";

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
		
		    OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
			
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
	
		
        $res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	/*function get_search_resultnew($limit, $offset , $sort_by, $sort_order ) 
	{
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		else if( isset( $uri_arr['from'] ) &&  $uri_arr['from'] != '' )
		{
			$from = $uri_arr['from'];
			
			$from = urldecode( $from );
		}	
		
				if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
		else if( isset( $uri_arr['to'] ) &&  $uri_arr['to'] != '' )
		{
			$to = $uri_arr['to'];
			
			$to = urldecode( $to );
		}	
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
				
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' AND u.u_status='appr' AND u.u_regno BETWEEN '".$from."' AND '".$to."'";

			
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		


		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}*/  // code commented by preeti on 11th apr 14 for black-box testing
	
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	function get_search_resultnew($limit, $offset , $sort_by, $sort_order ) 
	{
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		else if( isset( $uri_arr['from'] ) &&  $uri_arr['from'] != '' )
		{
			$from = $uri_arr['from'];
			
			$from = urldecode( $from );
		}	
		
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
		else if( isset( $uri_arr['to'] ) &&  $uri_arr['to'] != '' )
		{
			$to = $uri_arr['to'];
			
			$to = urldecode( $to );
		}
		
		
		if( $from != '' )
		{
			if( !$this->alpha_num_dash( $from ) )
			{
				$from = '';
			}	
		}
		
		if( $to != '' )
		{
			if( !$this->alpha_num_dash( $to ) )
			{
				$to = '';
			}	
		}
			
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' AND u.u_status='appr'  ";
		
		
		if( $from != '' && $to != '' )
		{
			$qry .= " AND u.u_regno  BETWEEN '".$from."' AND '".$to."'";	
		}	

			
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		


		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}
	

	function get_search_resultnews($limit, $offset , $sort_by, $sort_order ) 
	{
		
		/*$keyword = '';
		
		if ($this -> input -> post('keyword')) {
			$keyword = $this -> input -> post('keyword');
		}

		$gender = '';

		if ($this -> input -> post('gender')) {
			$gender = $this -> input -> post('gender');
		}

		$state = '';

		if ($this -> input -> post('state')) {
			$state = $this -> input -> post('state');
		}

		$start_date = '';

		if ($this -> input -> post('start_date')) {
			$start_date = $this -> input -> post('start_date');
		}

		$end_date = '';

		if ($this -> input -> post('end_date')) {
			$end_date = $this -> input -> post('end_date');
		}

		$start_age = '';

		if ($this -> input -> post('start_age')) {
			$start_age = $this -> input -> post('start_age');
		}

		$end_age = '';

		if ($this -> input -> post('end_age')) {
			$end_age = $this -> input -> post('end_age');
		}*/ // commented by preeti on 26th feb 14
		if( $this->input->post( 'from' ) != '')
		{
			
			$from = $this->input->post( 'from' );
			
		}
		else if( isset( $uri_arr['from'] ) &&  $uri_arr['from'] != '' )
		{
			$from = $uri_arr['from'];
			
			$from = urldecode( $from );
		}	
		
				if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
		else if( isset( $uri_arr['to'] ) &&  $uri_arr['to'] != '' )
		{
			$to = $uri_arr['to'];
			
			$to = urldecode( $to );
		}	
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
				
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' ";

			if (!empty($from) || !empty($to)) {
					
			
			 	$qry.=" AND SUBSTRING(u.u_regno,-4) >='".$from."'  AND  SUBSTRING(u.u_regno,-4) <=  '".$to."' ";
			 }
				
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		
//echo $qry;

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	function get_search_resultap( $limit, $offset , $sort_by, $sort_order ) 
	{
		/*$keyword = '';
		
		if ($this -> input -> post('keyword')) {
			$keyword = $this -> input -> post('keyword');
		}

		$gender = '';

		if ($this -> input -> post('gender')) {
			$gender = $this -> input -> post('gender');
		}

		$state = '';

		if ($this -> input -> post('state')) {
			$state = $this -> input -> post('state');
		}

		$start_date = '';

		if ($this -> input -> post('start_date')) {
			$start_date = $this -> input -> post('start_date');
		}

		$end_date = '';

		if ($this -> input -> post('end_date')) {
			$end_date = $this -> input -> post('end_date');
		}

		$start_age = '';

		if ($this -> input -> post('start_age')) {
			$start_age = $this -> input -> post('start_age');
		}

		$end_age = '';

		if ($this -> input -> post('end_age')) {
			$end_age = $this -> input -> post('end_age');
		}*/ // commented by preeti on 26th feb 14
		
		$res = $this->get_last_date();
		$icenter=$res->icenter;
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		$start_date = '';
		
			$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
	$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg.icenter='$icenter' AND reg_status = 'c' AND ";
		
		$qry.=" u.u_status='appr' ";
		//$qry.=" reg.pay_status='1' ";
		
		

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		//$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		$qry .= " ORDER BY appr_dt asc,u_id asc ";
		
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		
//echo $qry;
//die;

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

function get_search_resultre( $limit, $offset , $sort_by, $sort_order ) 
	{
		/*$keyword = '';
		
		if ($this -> input -> post('keyword')) {
			$keyword = $this -> input -> post('keyword');
		}

		$gender = '';

		if ($this -> input -> post('gender')) {
			$gender = $this -> input -> post('gender');
		}

		$state = '';

		if ($this -> input -> post('state')) {
			$state = $this -> input -> post('state');
		}

		$start_date = '';

		if ($this -> input -> post('start_date')) {
			$start_date = $this -> input -> post('start_date');
		}

		$end_date = '';

		if ($this -> input -> post('end_date')) {
			$end_date = $this -> input -> post('end_date');
		}

		$start_age = '';

		if ($this -> input -> post('start_age')) {
			$start_age = $this -> input -> post('start_age');
		}

		$end_age = '';

		if ($this -> input -> post('end_age')) {
			$end_age = $this -> input -> post('end_age');
		}*/ // commented by preeti on 26th feb 14
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post('keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		$start_date = '';
		
				$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		

		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' AND u.u_status='rej'";

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	function get_search_result2( $limit, $offset , $sort_by, $sort_order ) 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}	
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) )
			{
				//$keyword = '';
			}
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		// above code added by preeti on 26th feb 14 
		
		
		// start, code added by preeti on 9th apr 14 for black-box testing 
		
		/*$keyword = $this->db->escape( $keyword );
		
		$gender = $this->db->escape( $gender );
		
		$state = $this->db->escape( $state );
		
		$start_date = $this->db->escape( $start_date );
		
		$end_date = $this->db->escape( $end_date );
		
		
		$start_age = $this->db->escape( $start_age );
		
		$end_age = $this->db->escape( $end_age );
		
		
		$sort_by = $this->db->escape( $sort_by );
		
		$sort_order = $this->db->escape( $sort_order );*/
		
		
		// end, code added by preeti on 9th apr 14 for black-box testing
		
		$res = $this->get_last_date();
		$icenter=$res->icenter;

		// below query modified by preeti on 26th feb 14
		$from='';
		$to='';
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
			
		WHERE reg_status = 'c' AND reg.icenter='$icenter' AND  ";
		
		$qry.="u.u_status='appr'    ";
		//$qry.="reg.pay_status='1'    ";
//$mystring="and  u.u_regno between '$from' and '$to'";

if($from!='')
$qry.=" and SUBSTRING(u.u_regno,-4) >= '$from' ";
if($to!='')
$qry.=" and SUBSTRING(u.u_regno,-4) <= '$to' ";

	
		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}
	
	
	
	function get_search_dak( $limit, $offset , $sort_by, $sort_order ) 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}	
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) )
			{
				//$keyword = '';
			}
		}
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		// above code added by preeti on 26th feb 14 
		
		
		// start, code added by preeti on 9th apr 14 for black-box testing 
		
		/*$keyword = $this->db->escape( $keyword );
		
		$gender = $this->db->escape( $gender );
		
		$state = $this->db->escape( $state );
		
		$start_date = $this->db->escape( $start_date );
		
		$end_date = $this->db->escape( $end_date );
		
		
		$start_age = $this->db->escape( $start_age );
		
		$end_age = $this->db->escape( $end_age );
		
		
		$sort_by = $this->db->escape( $sort_by );
		
		$sort_order = $this->db->escape( $sort_order );*/
		
		
		// end, code added by preeti on 9th apr 14 for black-box testing
		

		// below query modified by preeti on 26th feb 14
		$from='';
		$to='';
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
			
		WHERE reg_status = 'c' AND  ";
		
		$qry.="u.u_status='appr' and reg.dak_status='0'   ";
		//$qry.="reg.pay_status='1'    ";
//$mystring="and  u.u_regno between '$from' and '$to'";

if($from!='')
$qry.=" and SUBSTRING(u.u_regno,-4) >= '$from' ";
if($to!='')
$qry.=" and SUBSTRING(u.u_regno,-4) <= '$to' ";

	
		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}
	
	

	function get_search_result72( $limit, $offset , $sort_by, $sort_order ) 
	{
		/*$keyword = '';
		
		if ($this -> input -> post('keyword')) {
			$keyword = $this -> input -> post('keyword');
		}

		$gender = '';

		if ($this -> input -> post('gender')) {
			$gender = $this -> input -> post('gender');
		}

		$state = '';

		if ($this -> input -> post('state')) {
			$state = $this -> input -> post('state');
		}

		$start_date = '';

		if ($this -> input -> post('start_date')) {
			$start_date = $this -> input -> post('start_date');
		}

		$end_date = '';

		if ($this -> input -> post('end_date')) {
			$end_date = $this -> input -> post('end_date');
		}

		$start_age = '';

		if ($this -> input -> post('start_age')) {
			$start_age = $this -> input -> post('start_age');
		}

		$end_age = '';

		if ($this -> input -> post('end_age')) {
			$end_age = $this -> input -> post('end_age');
		}*/ // commented by preeti on 26th feb 14
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		else if( isset( $uri_arr['from'] ) &&  $uri_arr['from'] != '' )
		{
			$from = $uri_arr['from'];
			
			$from = urldecode( $from );
		}	
		
				if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
		else if( isset( $uri_arr['to'] ) &&  $uri_arr['to'] != '' )
		{
			$to = $uri_arr['to'];
			
			$to = urldecode( $to );
		}	
		
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
			
		WHERE reg_status = 'c' AND u.u_status='appr'";

		if (!empty($from) && !empty($to) ) {
			$qry .= " AND (  AND u.u_regno BETWEEN '".$from."' AND '".$to."' 
			) ";
		}

				
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		


		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	function get_search_result3( $limit, $offset , $sort_by, $sort_order ) 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		
		$keyword = '';
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword );
		}
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) )
			{
				$keyword = '';
			}
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'];
		}
		
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		} 
		
		
		// chcek if parameter contains any special character if yes then make it empty
		
		// below code added on 27th mar 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		// above code added by preeti on 26th feb 14 
		
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON reg.reg_regno = sp.sp_u_regno
		
			
		WHERE reg_status = 'c' AND u.u_status='rej'";

		if (!empty($keyword)) {
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) {
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) {
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) {
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}
		}

		if ($start_age != '' || $end_age != '') {
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		$qry .= " ORDER BY ".$sort_by." ".$sort_order;
		
		if( $limit != '' ) // line modified by preeti on 2nd mar 14
		{
			$qry .= " LIMIT ". $offset . ", " . $limit;	
		}
		
		
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}

	/*function get_user_search() {
		
		
		$keyword = $this -> input -> post('keyword');

		$qry = " SELECT *, CONCAT(reg_fname, ' ', reg_mname, ' ', reg_lname) name,  
		
		reg_id, reg_regno, reg_email
		
		FROM register 
		
		WHERE 1";

		if (!empty($keyword)) {
			$qry .= " AND ( reg_regno = '" . $keyword . "'  
		
			OR reg_fname LIKE '%" . $keyword . "%'
		
			OR reg_mname LIKE '%" . $keyword . "%'
			
			OR reg_lname LIKE '%" . $keyword . "%'
		
			OR reg_email LIKE '%" . $keyword . "%' ) ";
		}
		
		echo " query is ".$qry;

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}*/  // code commented by preeti on 11th apr 14 for black-box testing

	// code added by preeti on 11th apr 14 for black-box testing
	
	function get_user_search() 
	{
		$keyword = $this -> input -> post('keyword');

		if( !$this->alpha_at_space( $keyword ) )
		{
			$keyword = '';
		}

		$qry = " SELECT *, CONCAT(reg_fname, ' ', reg_mname, ' ', reg_lname) name,  
		
		reg_id, reg_regno, reg_email
		
		FROM register 
		
		WHERE 1";

		if (!empty($keyword)) {
			$qry .= " AND ( reg_regno = '" . $keyword . "'  
		
			OR reg_fname LIKE '%" . $keyword . "%'
		
			OR reg_mname LIKE '%" . $keyword . "%'
			
			OR reg_lname LIKE '%" . $keyword . "%'
		
			OR reg_email LIKE '%" . $keyword . "%' ) ";
		}
		
		

		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
	
	
	function update_doc($doc_id, $photo_path = '', $resume_path = '') {
		$data = array();

		$previous_photo = '';

		$previous_resume = '';

		$data['doc_fname'] = $this -> input -> post('doc_fname');

		$data['doc_lname'] = $this -> input -> post('doc_lname');

		$data['doc_lname'] = $this -> input -> post('doc_lname');

		//$data['doc_mobile'] = $this->input->post('doc_mobile');

		//$data['doc_email'] = $this->input->post('doc_email');

		$data['doc_dob'] = calen_to_db($this -> input -> post('doc_dob'));

		$data['doc_qual'] = $this -> input -> post('doc_qual');

		$data['doc_stream'] = $this -> input -> post('doc_stream');

		$data['doc_college'] = $this -> input -> post('doc_college');

		if (!empty($photo_path)) {
			$data['doc_photo'] = $photo_path;
		}

		if (!empty($resume_path)) {
			$data['doc_resume'] = $resume_path;
		}

		$data['doc_updatedon'] = date('Y-m-d H:i:s');

		$this -> db -> where('doc_id', $doc_id);

		$res = $this -> db -> update('doctor', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_pg_data( $u_regno )// function added by preeti on 25th feb 14
	{
		$this->db->select('*');
		
		$this->db->from('post_grad');
		
		$this->db->where('pg_u_regno', $u_regno);
		
		$this->db->order_by('pg_order', 'ASC');
		
		$res = $this->db->get();
		
		if( $res->num_rows > 0 )
		{
			return $res->result();
		}
		else
		{
			return FALSE;	
		}
	}
		


	function get_user_detail( $regno )
	{
		$this->db->select('reg_email, reg_mobile');
		
		$this->db->from('register');
		
		$res = $this->db->get();
		
		/*$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN post_grad grad ON u.u_regno = grad.pg_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? ";*/
		
		// above lines commented and below lines added by preeti on 25h feb 14
		
		$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? ";
		
		$res = $this->db->query( $qry, array( $regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}
		else 
		{
			return ;	
		}		
		
	}
	function get_user_detaila( $regno )
	{
		$this->db->select('reg_email, reg_mobile');
		
		$this->db->from('register');
		
		$res = $this->db->get();
		
		/*$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN post_grad grad ON u.u_regno = grad.pg_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? ";*/
		
		// above lines commented and below lines added by preeti on 25h feb 14
		
		$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ?  AND u_status='appr'";
		
		$res = $this->db->query( $qry, array( $regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}
		else 
		{
			return ;	
		}		
		
	}
	function get_user_detailr( $regno )
	{
		$this->db->select('reg_email, reg_mobile');
		
		$this->db->from('register');
		
		$res = $this->db->get();
		
		/*$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN post_grad grad ON u.u_regno = grad.pg_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? ";*/
		
		// above lines commented and below lines added by preeti on 25h feb 14
		
		$qry = " SELECT * FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		JOIN postal post ON u.u_regno = post.post_u_regno
		
		JOIN permanent perm ON u.u_regno = perm.perm_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN new_name new ON u.u_regno = new.new_u_regno
		
		LEFT JOIN current_employer cur ON u.u_regno = cur.ce_u_regno 
		
		WHERE u.u_regno = ? AND u_status='rej'";
		
		$res = $this->db->query( $qry, array( $regno ) );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res->result();
		
			return $res_arr[0];	
		}
		else 
		{
			return ;	
		}		
		
	}
	
//27th feb added 
	function get_detail($doc_id) {
		//echo "pris";

		$sql = "SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, 
		
		u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, 
		 
		 u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, 

u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email,
reg.reg_fname as fname,reg.reg_lname as lname,reg.reg_mname as u_mname,

reg.reg_mobile as mobile,reg_regno as rno, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert,u.u_attempt_cert1 as u_attempt_cert1, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
 u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,u_sign as u_sign,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
st.new_fname as new_fname,st.new_mname as new_mname,st.new_lname as new_lname,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,sp_nation as sp_nation,u.u_grad_from as u_grad_from,
pgm.pg_degree as pg_degree,pgm.pg_subject as pg_subject,reg.reg_mobile as reg_mobile,reg.reg_email as reg_email,
sp.sp_mar_date as sp_mar_date,sp.sp_citizenship_date as sp_citizenship_date,sp.sp_nation as sp_nation,sp.sp_name as sp_name,
sp.sp_occup as sp_occup,sp.sp_ssc_applied as sp_ssc_applied,u.u_pmrc_reg_date as u_pmrc_reg_date,pgm.pg_college as pg_college,  
 pgm.pg_univ as pg_univ,pgm.pg_year as pg_year,pgm.pg_mci_recog as pg_mci_recog,ce.ce_employer as ce_employer,ps.ps_com_date as ps_com_date,
 ps.ps_rel_date as ps_rel_date,poll.perm_address as perm_address,poll.perm_state as perm_state,poll.perm_tel as perm_tel,poll.perm_pin as perm_pin,
		        pm.post_address as post_address,pm.post_state as post_state,pm.post_tel as post_tel,pm.post_pin as post_pin,
		        ps.ps_rel_order as relorder,st.new_name_proof as nma,ce.ce_emp_noc as cemp,
		        u.u_mbbs_cert as mbbs,u.u_intern_cert as intern
		        
		
		
		
		 FROM user u 
		LEFT JOIN current_employer ce ON u.u_regno= ce.ce_u_regno
		
		LEFT JOIN new_name st ON u.u_regno = st.new_u_regno
		
		LEFT JOIN permanent poll ON u.u_regno = poll.perm_u_regno
		
			LEFT JOIN register reg ON u.u_regno = reg.reg_regno
		LEFT JOIN postal pm ON u.u_regno= pm.post_u_regno
		
		LEFT JOIN post_grad pgm ON u.u_regno = pgm.pg_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
		WHERE u.u_regno= ?";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $obj) {
				return $obj;
			}
		}

		/*$res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
function get_detailapp($doc_id) {
		//echo "pris";

		$sql = "SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, 
		
		u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, 
		 
		 u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, 

u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email,
reg.reg_fname as fname,reg.reg_lname as lname,reg.reg_mname as u_mname,

reg.reg_mobile as mobile,reg_regno as rno, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert,u.u_attempt_cert1 as u_attempt_cert1, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
 u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,u_sign as u_sign,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
st.new_fname as new_fname,st.new_mname as new_mname,st.new_lname as new_lname,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,sp_nation as sp_nation,u.u_grad_from as u_grad_from,
pgm.pg_degree as pg_degree,pgm.pg_subject as pg_subject,reg.reg_mobile as reg_mobile,reg.reg_email as reg_email,
sp.sp_mar_date as sp_mar_date,sp.sp_citizenship_date as sp_citizenship_date,sp.sp_nation as sp_nation,sp.sp_name as sp_name,
sp.sp_occup as sp_occup,sp.sp_ssc_applied as sp_ssc_applied,u.u_pmrc_reg_date as u_pmrc_reg_date,pgm.pg_college as pg_college,  
 pgm.pg_univ as pg_univ,pgm.pg_year as pg_year,pgm.pg_mci_recog as pg_mci_recog,ce.ce_employer as ce_employer,ps.ps_com_date as ps_com_date,
 ps.ps_rel_date as ps_rel_date,poll.perm_address as perm_address,poll.perm_state as perm_state,poll.perm_tel as perm_tel,poll.perm_pin as perm_pin,
		        pm.post_address as post_address,pm.post_state as post_state,pm.post_tel as post_tel,pm.post_pin as post_pin,
		        ps.ps_rel_order as relorder,st.new_name_proof as nma,ce.ce_emp_noc as cemp,
		        u.u_mbbs_cert as mbbs,u.u_intern_cert as intern
		        
		
		
		
		 FROM user u 
		LEFT JOIN current_employer ce ON u.u_regno= ce.ce_u_regno
		
		LEFT JOIN new_name st ON u.u_regno = st.new_u_regno
		
		LEFT JOIN permanent poll ON u.u_regno = poll.perm_u_regno
		
			LEFT JOIN register reg ON u.u_regno = reg.reg_regno
		LEFT JOIN postal pm ON u.u_regno= pm.post_u_regno
		
		LEFT JOIN post_grad pgm ON u.u_regno = pgm.pg_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
		WHERE u.u_regno= ? AND u.u_status='appr'";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $obj) {
				return $obj;
			}
		}

		/*$res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
function get_detailrej($doc_id) {
		//echo "pris";

		$sql = "SELECT u.u_id as u_id, u.u_regno as u_regno, u.u_fname as u_fname, 
		
		u.u_mname as u_mname, u.u_lname as u_lname,
		  u.u_hindi_name as u_hindi_name, 
		 u.u_is_name_change as u_is_name_change, u.u_father_fname as u_father_fname, 
		 
		 u.u_father_mname as u_father_mname,
u.u_father_lname as u_father_lname, u.u_is_married as u_is_married, 

u.u_nationality as u_nationality, 
 u.u_dob as u_dob, u.u_sex as u_sex, u.u_age_year as u_age_year, 
u.u_age_month as u_age_month, u.u_age_day as u_age_day, reg.reg_email as u_email,
reg.reg_fname as fname,reg.reg_lname as lname,reg.reg_mname as u_mname,

reg.reg_mobile as mobile,reg_regno as rno, u.u_previous_ssc as u_previous_ssc,
u.u_dd_num as u_dd_num, u.u_dd_date as u_dd_date, u.u_dd_bank as u_dd_bank, u.u_dd_copy as u_dd_copy,
 u.u_college as u_college, u.u_univ as u_univ, u.u_mbbs_year as u_mbbs_year, u.u_adm_date as u_adm_date,
  u.u_passing_date as u_passing_date,   u.u_num_attempt as u_num_attempt, u.u_attempt_cert as u_attempt_cert,u.u_attempt_cert1 as u_attempt_cert1, 
  u.u_intern_date as u_intern_date,    u.u_college_recog as u_college_recog, u.u_is_pg as u_is_pg, u.u_pmrc_num as u_pmrc_num,
 u.u_pmrc_issue_off as u_pmrc_issue_off, u.u_current_emp as u_current_emp, u.u_ncc_cert as u_ncc_cert,u_sign as u_sign,
u.u_hobbies as u_hobbies, u.u_pref1 as u_pref1, u.u_pref2 as u_pref2, u.u_pref3 as u_pref3, u.u_updatedon as u_updatedon,
st.new_fname as new_fname,st.new_mname as new_mname,st.new_lname as new_lname,
u.u_pmrc_copy as doc,u.u_dob_proof as dob,u.u_photo as photo,sp_nation as sp_nation,u.u_grad_from as u_grad_from,
pgm.pg_degree as pg_degree,pgm.pg_subject as pg_subject,reg.reg_mobile as reg_mobile,reg.reg_email as reg_email,
sp.sp_mar_date as sp_mar_date,sp.sp_citizenship_date as sp_citizenship_date,sp.sp_nation as sp_nation,sp.sp_name as sp_name,
sp.sp_occup as sp_occup,sp.sp_ssc_applied as sp_ssc_applied,u.u_pmrc_reg_date as u_pmrc_reg_date,pgm.pg_college as pg_college,  
 pgm.pg_univ as pg_univ,pgm.pg_year as pg_year,pgm.pg_mci_recog as pg_mci_recog,ce.ce_employer as ce_employer,ps.ps_com_date as ps_com_date,
 ps.ps_rel_date as ps_rel_date,poll.perm_address as perm_address,poll.perm_state as perm_state,poll.perm_tel as perm_tel,poll.perm_pin as perm_pin,
		        pm.post_address as post_address,pm.post_state as post_state,pm.post_tel as post_tel,pm.post_pin as post_pin,
		        ps.ps_rel_order as relorder,st.new_name_proof as nma,ce.ce_emp_noc as cemp,
		        u.u_mbbs_cert as mbbs,u.u_intern_cert as intern
		        
		
		
		
		 FROM user u 
		LEFT JOIN current_employer ce ON u.u_regno= ce.ce_u_regno
		
		LEFT JOIN new_name st ON u.u_regno = st.new_u_regno
		
		LEFT JOIN permanent poll ON u.u_regno = poll.perm_u_regno
		
			LEFT JOIN register reg ON u.u_regno = reg.reg_regno
		LEFT JOIN postal pm ON u.u_regno= pm.post_u_regno
		
		LEFT JOIN post_grad pgm ON u.u_regno = pgm.pg_u_regno
		
		LEFT JOIN previous_ssc ps ON u.u_regno = ps.ps_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
		WHERE u.u_regno= ? AND u.u_status='rej'";

		$res = $this -> db -> query($sql, array($doc_id));

		//print $res;
		if ($res -> num_rows() > 0) {
			foreach ($res->result() as $obj) {
				return $obj;
			}
		}

		/*$res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
		
	function get_reg_id() {
		$qry = " SELECT reg_regno FROM register WHERE reg_status = 'c' ";

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
function get_reg_id1() {
		$qry = " SELECT r.reg_regno FROM register r,user u WHERE r.reg_regno=u.u_regno AND reg_status = 'c' AND u.u_status='appr'";

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}

function get_reg_id2() {
		$qry = " SELECT r.reg_regno FROM register r,user u WHERE r.reg_regno=u.u_regno AND reg_status = 'c' AND u.u_status='rej'";

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}

	
	function get_qual() {
		$res = $this -> db -> query("SELECT qual_id, qual_name 
		
		FROM qualification ORDER BY qual_name ASC ");

		return $res -> result();
	}

	function get_stream() {
		$res = $this -> db -> query("SELECT stream_id, stream_name 
		
		FROM stream ORDER BY stream_name ASC ");

		return $res -> result();
	}

	function get_college() {
		$res = $this -> db -> query("SELECT coll_id, coll_name	 
		
		FROM college ORDER BY coll_name	 ASC ");

		return $res -> result();
	}

	function get_num_records() {
		$res = $this -> db -> get('doctor');

		return $res -> num_rows();

	}

	/*function get_user_num() {
		$res = $this -> db -> where('reg_status', 'c');

		$res = $this -> db -> get('register');

		return $res -> num_rows();
	}*/ // commented by preeti on 26th feb 14
	
	
	function only_num($str) // function added by preeti on 10th apr 14
    {
        return ( ! preg_match("/^([0-9])+$/i", $str))? FALSE : TRUE;    
	}
	
	
	function get_user_num() 
	{
		
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword ); 
		}
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $keyword != '' )
		{
			if( !$this->alpha_num_at_space( $keyword) )
			{
				$keyword = '';
			} 
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $gender != '' )
		{
			if( !$this->alpha_at_space( $gender) )
			{
				$gender = '';
			} 
		}	
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'] ;
		}
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $state != '' )
		{
			if( !$this->only_num( $state) )
			{
				$state = '';
			} 
		}	
		 
		
		$start_date = '';
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $start_date != '' )
		{
			if( !$this->alpha_num_dash( $start_date) )
			{
				$start_date = '';
			} 
		}	
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $end_date != '' )
		{
			if( !$this->alpha_num_dash( $end_date) )
			{
				$start_date = '';
			} 
		}	
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $start_age != '' )
		{
			if( !$this->only_num( $start_age) )
			{
				$start_age = '';
			} 
		}	
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}


		// if block added by preeti on 10th apr 14 for black-box testing
		
		if( $end_age != '' )
		{
			if( !$this->only_num( $end_age) )
			{
				$end_age = '';
			} 
		}
		
		// above code added by preeti on 26th feb 14 
		
		$res=$this->get_last_date();
		$icenter=$res->icenter;

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
			
		WHERE (reg_status = 'i' or reg_status = 'c') AND reg.icenter='$icenter' ";

		if (!empty($keyword)) 
		{
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) 
		{
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) 
		{
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) 
		{
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') 
		{
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		
		
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}
//pavan 27 feb
function get_user_nums() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' AND u.u_status='appr'";
	if (!empty($from) && !empty($to)) 
		{
			$qry .= " AND (u.u_regno BETWEEN '".$from."' AND '".$to."')";
		}
		
			//echo $qry;
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}

	/*function get_user_numsy() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		$from=$to='';
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' AND u.u_status='appr' AND u.u_regno BETWEEN '".$from."' AND '".$to."'";

		/*	if (!empty($from) && !empty($to)) 
		//{
			//$qry .= " AND (u.u_regno BETWEEN '".$from."' AND '".$to."')";
		//}
			
			//echo $qry;
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}*/ // code commented by preeti on 11th apr 14 for black-box testing
	
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	function get_user_numsy() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		$from=$to='';
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		
		
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
				
		// start code added by preeti on 11th apr 14 for black-box testing
		
		if( $from != '' )
		{
			if( !$this->alpha_num_dash( $from ) )
			{
				$from = '';
			}
		}
		
		if( $to != '' )
		{
			if( !$this->alpha_num_dash( $to ) )
			{
				$to = '';
			}
		}
		
		
		// end code added by preeti on 11th apr 14 for black-box testing
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno	
		
			
		WHERE reg_status = 'c' AND u.u_status='appr' "; 
		
		if( $from != '' && $to != '' )
		{
			$qry .= "AND u.u_regno BETWEEN '".$from."' AND '".$to."'";
		}

		
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}
	


function get_user_numsp() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		$from=$to='';
		if( $this->input->post( 'from' ) != '' )
		{
			$from = $this->input->post( 'from' );	
		}
		if( $this->input->post( 'to' ) != '' )
		{
			$to = $this->input->post( 'to' );	
		}
				
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
			
		WHERE reg_status = 'c' ";

			if (!empty($from) && !empty($to)) 
		{
			$qry.=" AND SUBSTRING(u.u_regno,-4) >='".$from."'  AND  SUBSTRING(u.u_regno,-4) <=  '".$to."' ";
			//$qry .= " AND (u.u_regno BETWEEN '".$from."' AND '".$to."')";
		}
		
			//echo $qry;
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}

	function get_user_appnum() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword ); 
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) ) // line modified by preeti on 9th apr 14 for black-box testing
			{
				//$keyword = '';
			}
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'] ;
		}
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		// above code added by preeti on 26th feb 14 
			
		$res = $this->get_last_date();
		$icenter=$res->icenter;
			
		// below query modified by preeti on 26th feb 14
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		" ;
		
		
			
		$qry.="WHERE u.u_status = 'appr' AND reg.icenter='$icenter' ";
		
		if($from!='')
$qry.=" and SUBSTRING(u.u_regno,-4) >= '$from' ";
if($to!='')
$qry.=" and SUBSTRING(u.u_regno,-4) <= '$to' ";
		

		if (!empty($keyword)) 
		{
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) 
		{
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) 
		{
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) 
		{
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') 
		{
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}	
		
		
		//echo $qry;
		
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}




function get_user_daknum() 
	{
		$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword ); 
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) ) // line modified by preeti on 9th apr 14 for black-box testing
			{
				//$keyword = '';
			}
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'] ;
		}
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		$start_date = '';
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		
		
		
		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$end_age = $uri_arr['end_age'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		
		
		// above code added by preeti on 26th feb 14 
			

		// below query modified by preeti on 26th feb 14
		$from=$this->input->post('from');
		$to=$this->input->post('to');
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		" ;
		
		
			
		$qry.="WHERE u.u_status = 'appr' and reg.dak_status='0'  ";
		
		if($from!='')
$qry.=" and SUBSTRING(u.u_regno,-4) >= '$from' ";
if($to!='')
$qry.=" and SUBSTRING(u.u_regno,-4) <= '$to' ";
		

		if (!empty($keyword)) 
		{
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) 
		{
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) 
		{
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) 
		{
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') 
		{
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}	
		
		
		//echo $qry;
		//die;
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}




	function get_user_rejnum() {
		
	$uri_arr = $this->uri->uri_to_assoc(3); // a uri is converted into key value pair of an array
		
		$keyword = '';
		
		
		if( $this->input->post( 'keyword' ) != '' )
		{
			$keyword = $this->input->post( 'keyword' );	
		}
		else if( isset( $uri_arr['keyword'] ) &&  $uri_arr['keyword'] != '' )
		{
			$keyword = $uri_arr['keyword'];
			
			$keyword = urldecode( $keyword ); 
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $keyword != '' )
		{
			if(  !$this->alpha_at_space( $keyword ) )
			{
				$keyword = '';
			}
		}	
		
		
		$gender = '';
		
		if( $this->input->post( 'gender' ) != '' )
		{
			$gender = $this->input->post( 'gender' );	
		}
		else if( isset( $uri_arr['gender'] ) &&  $uri_arr['gender'] != '' )
		{
			$gender = $uri_arr['gender'];
		}
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $gender != '' )
		{
			if(  !$this->alpha_num_dash( $gender ) )
			{
				$gender = '';
			}
		}
		 
		
		$state = '';
		
		if( $this->input->post( 'state' ) != '' )
		{
			$state = $this->input->post( 'state' );	
		}
		else if( isset( $uri_arr['state'] ) &&  $uri_arr['state'] != '' )
		{
			$state = $uri_arr['state'] ;
		}

		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $state != '' )
		{
			if(  !$this->alpha_num_dash( $state ) )
			{
				$state = '';
			}
		}
		
		
		$start_date = '';
		
		
		
		if( $this->input->post( 'start_date' ) != '' )
		{
			$start_date = $this->input->post( 'start_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$start_date = calen_to_db( $start_date );	
		}
		else if( isset( $uri_arr['start_date'] ) &&  $uri_arr['start_date'] != '' )
		{
			$start_date = $uri_arr['start_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_date != '' )
		{
			if(  !$this->alpha_num_dash( $start_date ) )
			{
				$start_date = '';
			}
		}
		
		$end_date = '';
		
		if( $this->input->post( 'end_date' ) != '' )
		{
			$end_date = $this->input->post( 'end_date' );
			
			// below line added by preeti on 3rd mar 14
			
			$end_date = calen_to_db( $end_date );	
		}
		else if( isset( $uri_arr['end_date'] ) &&  $uri_arr['end_date'] != '' )
		{
			$end_date = $uri_arr['end_date'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_date != '' )
		{
			if(  !$this->alpha_num_dash( $end_date ) )
			{
				$end_date = '';
			}
		}
		

		$start_age = '';
		
		if( $this->input->post( 'start_age' ) != '' )
		{
			$start_age = $this->input->post( 'start_age' );	
		}
		else if( isset( $uri_arr['start_age'] ) &&  $uri_arr['start_age'] != '' )
		{
			$start_age = $uri_arr['start_age'];
		}
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $start_age != '' )
		{
			if(  !$this->alpha_num_dash( $start_age ) )
			{
				$start_age = '';
			}
		}
		
		
		$end_age = '';
		
		if( $this->input->post( 'end_age' ) != '' )
		{
			$end_age = $this->input->post( 'end_age' );	
		}
		else if( isset( $uri_arr['end_age'] ) &&  $uri_arr['end_age'] != '' )
		{
			$start_age = $uri_arr['end_age'];
		}
		
		// above code added by preeti on 26th feb 14 
		
		
		// below code added on 9th apr 14 by preeti for black-box testing
		
		if( $end_age != '' )
		{
			if(  !$this->alpha_num_dash( $end_age ) )
			{
				$end_age = '';
			}
		}
		

		// below query modified by preeti on 26th feb 14
		
		$qry = " SELECT *, CONCAT(reg.reg_fname, ' ',reg.reg_mname, ' ', reg.reg_lname  ) name 
		
		FROM register reg
		
		JOIN user u ON reg.reg_regno = u.u_regno
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		LEFT JOIN postal post ON reg.reg_regno = post.post_u_regno
		
		LEFT JOIN spouse sp ON u.u_regno = sp.sp_u_regno
		
		
			
		WHERE u.u_status = 'rej' ";

		if (!empty($keyword)) 
		{
			$qry .= " AND ( reg.reg_regno = '" . $keyword . "'  
		
			OR reg.reg_fname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mname LIKE '%" . $keyword . "%'
			
			OR reg.reg_lname LIKE '%" . $keyword . "%'
			
			OR reg.reg_mobile LIKE '%" . $keyword . "%'
			
			OR reg.reg_email LIKE '%" . $keyword . "%'
			OR new.new_fname LIKE '%" . $keyword . "%'
			
			OR new.new_mname LIKE '%" . $keyword . "%'
			
			OR new.new_lname LIKE '%" . $keyword . "%'
		
		
			) ";
		}

		if (!empty($gender)) 
		{
			$qry .= " AND ( u.u_sex = '" . $gender . "' )";
		}

		if (!empty($state)) 
		{
			$qry .= " AND ( post.post_state = '" . $state . "' )";
		}

		if (!empty($start_date) || !empty($end_date)) 
		{
			if (!empty($start_date)) {
				$qry .= " AND ( reg.reg_createdon >= '" . $start_date . "' )";
				//$qry .= " AND ( reg.reg_createdon >= '" . calen_to_db($start_date) . "' )";
			}

			if (!empty($end_date)) {
				$qry .= " AND ( reg.reg_createdon <= '" . $end_date . "' )";
				//$qry .= " AND ( reg.reg_createdon <= '" . calen_to_db($end_date) . "' )";
			}

		}

		if ($start_age != '' || $end_age != '') 
		{
			if ($start_age != '') {
				$qry .= " AND ( u.u_age_year >= '" . $start_age . "' )";
			}

			if ($end_age != '') {
				$qry .= " AND ( u.u_age_year <= '" . $end_age . "' )";
			}

		}
		
		$res = $this -> db -> query($qry);

		return $res -> num_rows();
	}
function delete_detail() {
		//echo "pris";
$i=0;
$sql = "TRUNCATE TABLE temp";

		$res = $this -> db -> query($sql);
	}
	function insert_detail($doc_id,$w,$q) {
		//echo "pris";
$i=0;
//$sql = "truncate table temp";

	//	$res = $this -> db -> query($sql);
//$res_arr = $res -> result();

		$sql = "INSERT INTO temp(pg_u_regno,pg_degree,pg_subject) VALUES('".$doc_id."','".$w."','".$q."')";

		$res = $this -> db -> query($sql);
//$res_arr = $res -> result();

	//	if ($res -> num_rows() > 0) {
		//	$res_arr = $res -> result();

			return TRUE;
		//} else {
			//return FALSE;
		//}
		/*res_arr =  $res->result();

		 return $res_arr[0];	*/

	}
function update_statusapp1($reg_id_arr,$res_id_arr) 
{
	$reg_ids = implode(',', $reg_id_arr);
	$i=0;
	$cnt=count($res_id_arr);
	//echo $cnt;
	//print_r($res_id_arr);print_r($reg_id_arr);
	$i=0;$j=0;
	$resid=array();
	//foreach ($res_id_arr as $reg) {
		for($i=0;$i<$cnt;$i++){
		
		if($res_id_arr[$i]!='')
		{
		$resid[$j]=$res_id_arr[$i];
		$j++;
		
		}
		
		
			}
			$cnt1=count($resid);
	//print_r($resid);
	for($i=0;$i<$cnt1;$i++)
	{
	//foreach ($reg_ids as $reg) {
			//asak how to solve tis from preetii
			
			
			//echo $i;
		$resid1=$resid[$i];$regid=$reg_id_arr[$i];
		//echo $resid;echo $regid; 
		
		$qry = " UPDATE user SET u_status ='rej',u_reason='".$resid1."' WHERE u_regno ='" . $regid . "'";
//echo $qry;
		$res = $this -> db -> query($qry);
			
		}
	
		
		//$res_arr = $res -> result();

		return TRUE;
	}
	

	/*function update_statusapp($reg_id_arr,$res_id_arr) 
	{
		// start , code added by preeti on 11th apr 14 for black-box testing	
				
		$valid_reg = array();
				
		foreach( $reg_id_arr as $val )
		{
			if( $this->alpha_num_dash( $val ) )
			{
				$valid_reg[] = $val;
			}	
		}
		
		// end , code added by preeti on 11th apr 14 for black-box testing
		
		if( COUNT( $valid_reg ) > 0 )// if else block added by preeti on 11th apr 14 for black-box testing
		{
				
			$reg_id_arr = $valid_reg;	
			
			$reg_ids = implode(',', $reg_id_arr);
			
			$i=0;
			
			$cnt2=count($reg_id_arr);
			
			$cnt=count($res_id_arr);
			
			$i=0;$j=0;
			
			$resid=array();
			
			for($i = 0; $i < $cnt; $i++)
			{
				
				if($res_id_arr[$i]!='')
				{
					$resid[$j]=$res_id_arr[$i];
					$j++;
				}
				
			}
			
			//3rd march pavan mod
			
			$cnt1=count($resid);
			
			for($i=0;$i<$cnt2;$i++)
			{
				
				$resid1=$resid[$i];
							
				$regid=$reg_id_arr[$i];
							
				$qry = " UPDATE user SET u_status ='appr',u_reason='' WHERE u_regno ='" . $regid . "'";
		
				$res = $this -> db -> query($qry);
					
			}
			
			return TRUE;
				
		}
		else 
		{
			return FALSE;
		}
		
		
	}*/ //function commented by preeti on 23rd apr 14 for manual testing
	
	// below code added by preeti on 11th apr 14 for black-box testing
	
	function update_statusapp($reg_id_arr,$res_id_arr) 
	{
		// start , code added by preeti on 11th apr 14 for black-box testing	
				
		$valid_reg = array();
				
		foreach( $reg_id_arr as $val )
		{
			if( $this->alpha_num_dash( $val ) )
			{
				$valid_reg[] = $val;
			}	
		}
		
		// end , code added by preeti on 11th apr 14 for black-box testing
		
		if( COUNT( $valid_reg ) > 0 )// if else block added by preeti on 11th apr 14 for black-box testing
		{
				
			$reg_id_arr = $valid_reg;	
			
			$reg_ids = implode(',', $reg_id_arr);
			
			$i=0;
			
			$cnt2=count($reg_id_arr);
			
			$cnt=count($res_id_arr);
			
			$i=0;$j=0;
			
			$resid=array();
			
			for($i = 0; $i < $cnt; $i++)
			{
				
				if($res_id_arr[$i]!='')
				{
					$resid[$j]=$res_id_arr[$i];
					
					$j++;
				}
				
			}
			
			//3rd march pavan mod
			
			$cnt1=count($resid);
			
			for( $i=0; $i<$cnt2; $i++ )
			{				
				$resid1=$resid[$i];
							
				$regid=$reg_id_arr[$i];
				
				// below if block added by preeti on 23rd apr 14 for manual testing
				
				if( $this->alpha_num_dash( $regid ) )
				{
					$qry = " UPDATE user SET u_status ='appr',u_reason='',appr_dt=CURRENT_TIMESTAMP
				
					WHERE u_regno ='" . $regid . "'";
			
					$res = $this -> db -> query($qry);
				}			
					
			}
			
			return TRUE;
				
		}
		else 
		{
			return FALSE;
		}		
		
	} 
	
	
	//3rd march pavan
	
	// below function commented by preeti on 23rd apr 14 for manual testing
	
	/*function update_statusrej($reg_id_arr,$res_id_arr) {
	$reg_ids = implode(',', $reg_id_arr);
	$i=0;
	$cnt2=count($reg_id_arr);
	$cnt=count($res_id_arr);
	//echo $cnt;
	//print_r($res_id_arr);print_r($reg_id_arr);
	$i=0;$j=0;
	$resid=array();
	//foreach ($res_id_arr as $reg) {
		for($i=0;$i<$cnt;$i++){
		
		if($res_id_arr[$i]!='')
		{
		$resid[$j]=$res_id_arr[$i];
		$j++;
		
		}
		
		
			}
		//3rd march pavan mod
			$cnt1=count($resid);
			//echo $cnt1;
	//print_r($resid);
	for($i=0;$i<$cnt2;$i++)
	{
	//foreach ($reg_ids as $reg) {
			//asak how to solve tis from preetii
			
			
			//echo $i;
		$resid1=$resid[$i];
		
		
		$regid=$reg_id_arr[$i];
		//echo $resid;echo $regid; 
		
		$qry = " UPDATE user SET u_status ='rej',u_reason='".$resid1."' WHERE u_regno ='" . $regid . "'";
//echo $qry;
		$res = $this -> db -> query($qry);
			
		}
	
		
		//$res_arr = $res -> result();

		return TRUE;
	}*/
	
	
	// below function added by preeti on 23rd apr 14 for manual testing
	
	function update_statusrej($reg_id_arr,$res_id_arr) 
	{
		$reg_ids = implode(',', $reg_id_arr);
		
		$i=0;
		
		$cnt2=count($reg_id_arr); // doc_id array
		
		$cnt=count($res_id_arr); // reasons array		
		
		$i=0;
		
		$j=0;
		
		$resid=array(); // this will store all the non-empty values
		
		for( $i=0; $i<$cnt; $i++ )
		{			
			if($res_id_arr[$i]!='')
			{				
				$resid[$j]=$res_id_arr[$i];
				
				$j++;			
			}			
			
		}
		
		$cnt1=count( $resid );			
		
		for( $i=0; $i<$cnt2; $i++ )
		{		
			//$resid1 = $resid[$i];	// commented by preeti on 23rd apr 14 for manual testing
			
			if( $this->alpha_num_dash( $reg_id_arr[$i] ) )
			{
				$regid = $reg_id_arr[$i];
			
				$resid1 = $this->input->post( 'reason_'.$regid ) ;	// commented by preeti on 23rd apr 14 for manual testing	
			
				if( !$this->alpha_num_at_space( $resid1 ) )
				{
					$resid1 = '';
				}
			
			
				$qry = " UPDATE user SET u_status ='rej',u_reason='".$resid1."' 
			
				WHERE u_regno ='" . $regid . "'";
	
				$res = $this -> db -> query($qry);	
					
			}
			
				
		}
		
		return TRUE;
	}
	

	function get_item_name($doc_id, $item_name) {
		$this -> load -> helper('file');

		if ($item_name == 'photo') {
			$sql = " SELECT doc_photo FROM doctor WHERE doc_id = ? ";

			$res = $this -> db -> query($sql, array($doc_id));

			if ($res -> num_rows() > 0) {
				$res_arr = $res -> result();

				return $res_arr[0] -> doc_photo;
			}

		} else if ($item_name == 'resume') {
			$sql = " SELECT doc_resume FROM doctor WHERE doc_id = ? ";

			$res = $this -> db -> query($sql, array($doc_id));

			$res_arr = $res -> result();

			return $res_arr[0] -> doc_resume;

		}

	}

	// below code commented by preeti on 11th apr 14 for black-box testing

	/*function update_doc_password() 
	{
		$data = array();

		$reg_id = $this -> input -> post('reg_id');

		//$data['reg_pass'] = md5($this -> input -> post('reg_pass'));// code commented for black-box testing by preeti on 26th mar 14
		
		$data['reg_pass'] = $this -> input -> post('reg_pass_encode');// code added for black-box testing by preeti on 26th mar 14

		$this -> db -> where('reg_id', $reg_id);

		$res = $this -> db -> update('register', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}*/

	
	// below code added by preeti on 11th apr 14 for black-box testing

 	function update_doc_password() 
	{
		$data = array();

		$flag = TRUE;
		
		if( $this->only_num( $this -> input -> post('reg_id') ) )
		{
			$reg_id = $this -> input -> post('reg_id');	
		}
		else 
		{
			$flag = FALSE;
		}

		if( $this->alpha_num_dash( $this -> input -> post('reg_pass_encode') ) )
		{
			$data['reg_pass'] = $this -> input -> post('reg_pass_encode');	
		}
		else 
		{
			$flag = FALSE;
		}

		
		if( $flag )
		{
			
			$this -> db -> where('reg_id', $reg_id);

			$res = $this -> db -> update('register', $data);
	
			if ($res) 
			{
				return TRUE;
			} 
			else 
			{
				return FALSE;
			}			
			
		}
		else 
		{
			return FALSE;
		}		
	}
	
	
	
	function update_admin_password() 
	{
		$data = array();

		$admin_id = $this -> session -> userdata('admin_id');

		//$data['admin_password'] = md5($this -> input -> post('admin_password'));// code commented for black-box testing by preeti on 26th mar 14
		
		$data['admin_password'] = $this -> input -> post('admin_password_encode');// code added for black-box testing by preeti on 26th mar 14

		$data['admin_updatedon'] = date('Y-m-d H:i:s');

		$this -> db -> where('admin_id', $admin_id);

		$res = $this -> db -> update('admin', $data);

		if ($res) 
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}
	}

	function update_admin_email() {
		$data = array();

		$admin_id = $this -> session -> userdata('admin_id');

		$data['admin_email'] = $this -> input -> post('admin_email');

		$data['admin_updatedon'] = date('Y-m-d H:i:s');

		$this -> db -> where('admin_id', $admin_id);

		$res = $this -> db -> update('admin', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*function check_old_password() 
	{
		// code commented for black-box testing by preeti on 26th mar 14
		
		//$old_password = $this -> input -> post('opassword');
		
		// code added for black-box testing by preeti on 26th mar 14
		
		$old_password = $this -> input -> post('opassword_encode');
		

		$admin_id = $this -> session -> userdata('admin_id');

		$qry = " SELECT admin_id FROM admin 
		
		WHERE admin_id = ? AND admin_password = ? ";

		// code commented for black-box testing by preeti on 26th mar 14

		//$res = $this -> db -> query($qry, array($admin_id, md5($old_password)));

		// code added for black-box testing by preeti on 26th mar 14
		
		$res = $this -> db -> query($qry, array($admin_id, $old_password ) );
		
		if ($res -> num_rows() == 1) 
		{
			return TRUE;
		}
		else 
		{
			return FALSE;
		}

	}*/ //  code commented by preeti on 28th mar 14 for black-box testing
	
	
	// below code added by preeti on 28th mar 14 for black-box testing
	
	function check_old_password() 
	{
		$old_password = $this -> input -> post('opassword_encode'); // salted md5 value
		

		$admin_id = $this -> session -> userdata('admin_id');

		$qry = " SELECT admin_id, admin_password FROM admin 
		
		WHERE admin_id = ? ";

		$res = $this -> db -> query($qry, array($admin_id) );
		
		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res->result();
			
			$admin_password = $res_arr[0]->admin_password ;
				
			//$salt = $this->input->post('salt'); // commented by preeti on 22nd apr 14 for manual testing
			
			$salt = $this->session->userdata('admin_salt'); // added by preeti on 22nd apr 14 for manual testing
			
			$result_pass = md5( $admin_password.$salt );
						
			if( $result_pass === $old_password )
			{
				return TRUE;	
			}
			else 
			{
				return FALSE;	
			}
						
		}
		else 
		{
			return FALSE;
		}

	}
	
	
	function empty_register() // function added by preeti on 27th feb 14
	{
		$res = $this -> db -> empty_table('register');

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}	

	// code commented by preeti on 11th apr 14 for black-box testing

	/*function get_name($reg_id) 
	{
		$qry = " SELECT reg.reg_regno, u.u_is_name_change , u.u_fname, u.u_mname, u.u_lname,
		
		new.new_fname , new.new_mname, new.new_lname
		
		FROM register reg 
		
		JOIN user u ON reg.reg_regno = u. u_regno
		
		LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
		
		WHERE reg.reg_id = ".$reg_id;
		
		$res = $this -> db -> query( $qry );
		
		if( $res->num_rows() > 0 )
		{
			$res_arr = $res -> result();

			return $res_arr[0];
				
		}	
		else
		{
			return;
		}
		
	}*/
	
	// code added by preeti on 11th apr 14 for black-box testing
	
	function get_name($reg_id) 
	{
		if( $this->only_num( $reg_id ) )
		{
			
			$qry = " SELECT reg.reg_regno, u.u_is_name_change , u.u_fname, u.u_mname, u.u_lname,
		
			new.new_fname , new.new_mname, new.new_lname
			
			FROM register reg 
			
			JOIN user u ON reg.reg_regno = u. u_regno
			
			LEFT JOIN new_name new ON reg.reg_regno = new.new_u_regno
			
			WHERE reg.reg_id = ".$reg_id;
			
			$res = $this -> db -> query( $qry );
			
			if( $res->num_rows() > 0 )
			{
				$res_arr = $res -> result();
	
				return $res_arr[0];
					
			}	
			else
			{
				return;
			}			
			
		}
		else
		{
			return FALSE ;
		}	
		
	}
	
	// below function added by preeti on 26th apr 14
	
	function get_num_audit_user() 
	{
		$res = $this -> db -> get('login_user');

		return $res -> num_rows();

	}
	
	// below function added by preeti on 26th apr 14
	
	function get_audit_user_limit($limit, $offset) 
	{
			
		$this->db->select('*');
				
		$this->db->order_by('logu_time', 'DESC');
		
		$this->db->limit( $limit, $offset);

		$res = $this->db->get('login_user');
		
		return $res -> result();
		// this is an array that is returned by this function
	}
	
	
	
	// below function added by preeti on 26th apr 14
	
	function get_num_audit_admin() 
	{
		$res = $this -> db -> get('login_admin');

		return $res -> num_rows();

	}
	
	// below function added by preeti on 26th apr 14
	
	function get_audit_admin_limit($limit, $offset) 
	{
			
		$this->db->select('*');
				
		$this->db->order_by('loga_time', 'DESC');
		
		$this->db->limit( $limit, $offset);

		$res = $this->db->get('login_admin');
		
		return $res -> result();
		
	}
	
	

	function get_num_not() 
	{
		$res = $this -> db -> get('notification');

		return $res -> num_rows();

	}

	function get_not_limit($limit, $offset) {
		$qry = " SELECT *
		
		FROM notification ORDER BY not_id DESC  
		
		LIMIT " . $offset . ", " . $limit;

		$res = $this -> db -> query($qry);

		return $res -> result();
		// this is an array that is returned by this function
	}

	function save_not($file_name = '') {
		$data = array();

		$data['not_title'] = $this -> input -> post('not_title');

		$data['not_date'] = calen_to_db($this -> input -> post('not_date'));

		$data['not_author'] = $this -> input -> post('not_author');

		$data['not_type'] = $this -> input -> post('not_type');
			


		if ($data['not_type'] == 'c') {
			$data['not_content'] = $this -> input -> post('not_content');
		} else if ($data['not_type'] == 'l') {
			$data['not_link'] = $file_name;
		}

		$data['not_createdon'] = date("Y-m-d H:i:s");

		$data['not_updatedon'] = date("Y-m-d H:i:s");

		$x=$this->input->post('flashtype');

$y=0;

if($data['not_author']=='AMC')
$y=1;

	$data['flashtype'] =$y;

  
		//var_dump($data);
		
		
		$res = $this -> db -> insert('notification', $data);
 
		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function not_detail($not_id) {
		$qry = " SELECT * FROM notification 
		
		WHERE not_id = ?  ";

		$res = $this -> db -> query($qry, array($not_id));

		$res_arr = $res -> result();

		return $res_arr[0];
	}

	function get_not_filename($not_id) {
		$this -> db -> select('not_link');

		$this -> db -> from('notification');

		$this -> db -> where('not_id', $not_id);

		$res = $this -> db -> get();

		$res_arr = $res -> result();

		return $res_arr[0] -> not_link;
	}

	/*function update_not($file_name = '') {

		$data = array();

		$prev_file = '';

		$data['not_id'] = $this -> input -> post('not_id');

		$data['not_title'] = $this -> input -> post('not_title');

		$data['not_date'] = calen_to_db($this -> input -> post('not_date'));

		$data['not_author'] = $this -> input -> post('not_author');

		$data['not_type'] = $this -> input -> post('not_type');

		if ($data['not_type'] == 'c') {
			$data['not_content'] = $this -> input -> post('not_content');

			$data['not_link'] = '';

			// check if file uploaded before then get the name and delete from folder

			$prev_file = $this -> get_not_filename($data['not_id']);

		} else if ($data['not_type'] == 'l') {
			if ($file_name != '') {
				$data['not_link'] = $file_name;

				$data['not_content'] = '';

				// check if file uploaded before then get the name and delete from folder

				$prev_file = $this -> get_not_filename($data['not_id']);
			}

		}

		$data['not_updatedon'] = date("Y-m-d H:i:s");

		$res = $this -> db -> where('not_id', $this -> input -> post('not_id'));

		$res = $this -> db -> update('notification', $data);

		if ($res) {
			// delete previous files in case of file updation

			if ($prev_file != '') {
				$prev_file_path = "uploads/not/" . $prev_file;

				if (file_exists($prev_file_path)) {
					unlink($prev_file_path);
				}
			}

			return TRUE;
		} else {
			return FALSE;
		}

	}*/// function commented by preeti on 11th apr 14

	
	// function added by preeti on 11th apr 14
	
	function update_not($file_name = '') {

		$data = array();

		$prev_file = '';
		
		$flag = TRUE;
		
		if( $this->only_num( $this -> input -> post('not_id') ) )
		{
			$data['not_id'] = $this -> input -> post('not_id');	
		}
		else
		{
			$flag = FALSE;
		}
	

		if( $flag && $this->alpha_num_dash( $this -> input -> post('not_title') ) )
		{
			$data['not_title'] = $this -> input -> post('not_title');
		}
		else
		{
			$flag = FALSE;
		}
			
		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('not_date') ) )
		{
			$data['not_date'] = calen_to_db($this -> input -> post('not_date'));
		}
		else
		{
			$flag = FALSE;
		}
		

		
		if( $flag && $this->alpha_and_space( $this -> input -> post('not_author') ) )
		{
			$data['not_author'] = $this -> input -> post('not_author');
		}
		else
		{
			$flag = FALSE;
		}	

		$data['not_type'] = $this -> input -> post('not_type');

		if ($data['not_type'] == 'c') 
		{
			$data['not_content'] = $this -> input -> post('not_content');
					

			$data['not_link'] = '';

			// check if file uploaded before then get the name and delete from folder

			$prev_file = $this -> get_not_filename($data['not_id']);

		} 
		else if ($data['not_type'] == 'l') 
		{
			if ($file_name != '') 
			{
				$data['not_link'] = $file_name;

				$data['not_content'] = '';

				// check if file uploaded before then get the name and delete from folder

				$prev_file = $this -> get_not_filename($data['not_id']);
			}

		}
		
		if( $flag )
		{
			
			$data['not_updatedon'] = date("Y-m-d H:i:s");

			$res = $this -> db -> where('not_id', $this -> input -> post('not_id'));
	
			$res = $this -> db -> update('notification', $data);
	
			if ($res)
			{
				// delete previous files in case of file updation
	
				if ($prev_file != '') {
					$prev_file_path = "uploads/not/" . $prev_file;
	
					if (file_exists($prev_file_path)) {
						unlink($prev_file_path);
					}
				}
	
				return TRUE;
			} 
			else 
			{
				return FALSE;
			}		
			
		}
		else 
		{
			return FALSE;	
		}	

	}
	
	
	/*function del_not( $not_id )
	 {
	 $filename = $this->get_not_filename($not_id);

	 $qry = " DELETE FROM notification WHERE not_id = ? ";

	 $res = $this->db->query( $qry, array( $not_id ) );

	 if($res)
	 {
	 if( $filename != '' )
	 {
	 $prev_file_path = "uploads/not/".$filename;

	 if(file_exists( $prev_file_path ))
	 {
	 unlink( $prev_file_path );
	 }
	 }

	 return TRUE;
	 }
	 else
	 {
	 return FALSE;
	 }

	 }*/

	function del_not($not_id_arr) {

		$not_ids = implode(',', $not_id_arr);

		$qry = " SELECT not_id, not_type, not_link 
		
		FROM notification 
		
		WHERE not_id IN (" . $not_ids . ") ";

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		$res = '';

		foreach ($res_arr as $row) {
			$filename = '';

			if ($row -> not_type == 'l') {
				$filename = 'uploads/not/' . $row -> not_link;
			}

			$res = $this -> db -> delete('notification', array('not_id' => $row -> not_id));

			if ($res) {
				if ($filename != '' && file_exists($filename)) {
					unlink($filename);
				}
			}

		}

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function get_num_res() {
		$res = $this -> db -> get('result');

		return $res -> num_rows();

	}

	function get_res_limit($limit, $offset) {
		$qry = " SELECT *
		
		FROM result ORDER BY res_id DESC  
		
		LIMIT " . $offset . ", " . $limit; // change in query by preeti on 19th feb 14

		$res = $this -> db -> query($qry);

		return $res -> result();
		// this is an array that is returned by this function
	}

	
	
	/*function save_res($result_file_name) {

		$data = array();

		$data['res_title'] = $this -> input -> post('res_title');

		$data['res_date'] = calen_to_db($this -> input -> post('res_date'));

		$data['res_file_path'] = $result_file_name;

		$res = $this -> db -> insert('result', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}*/  // function commented by preeti on 11th apr 14 for black-box testing
	
	// below code added by preeti on 11th apr 14 for black-box testing
	
	function save_res($result_file_name) {

		$data = array();
		
		$flag = TRUE;
		
		if( $this->alpha_and_space( $this -> input -> post('res_title') ) )
		{
			$data['res_title'] = $this -> input -> post('res_title');	
		}
		else
		{
			$flag = FALSE;
		}

		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('res_date') ) )
		{
			$data['res_date'] = calen_to_db($this -> input -> post('res_date'));
		}
		else
		{
			$flag = FALSE;
		}
		

		if( $flag )
		{
			$data['res_file_path'] = $result_file_name;

			$res = $this -> db -> insert('result', $data);
	
			if ($res) 
			{
				return TRUE;
			} 
			else 
			{
				return FALSE;
			}	
		}
		else 
		{
			return FALSE;
		}	

	}

	function res_detail($res_id) {
		$qry = " SELECT * FROM result 
		
		WHERE res_id = ?  ";

		$res = $this -> db -> query($qry, array($res_id));

		$res_arr = $res -> result();

		return $res_arr[0];
	}

	function get_prev_result($res_id) {
		//$this->load->helper('file');

		$sql = " SELECT res_file_path 
		
		FROM result 
		
		WHERE res_id = ? ";

		$res = $this -> db -> query($sql, array($res_id));

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr[0] -> res_file_path;
		}

	}

	/*function update_res($res_id, $res_file_path = '') 
	{
		
		$data = array();

		$data['res_title'] = $this -> input -> post('res_title');

		$data['res_date'] = calen_to_db($this -> input -> post('res_date'));

		if (!empty($res_file_path)) {
			$data['res_file_path'] = $res_file_path;
		}

		$this -> db -> where('res_id', $res_id);

		$res = $this -> db -> update('result', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}*/ // code commented by preeti on 11th apr 14 for black-box testing

	// below code added by preeti on 11th apr 14 for black-box testing
	
	function update_res($res_id, $res_file_path = '') 
	{
		
		$data = array();
		
		$flag = TRUE;
		
		if( !$this->only_num( $res_id ) )
		{
			$flag = FALSE;
		}

		if( $flag && $this->alpha_and_space( $this -> input -> post('res_title') ) )
		{
			$data['res_title'] = $this -> input -> post('res_title');	
		}
		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('res_date') ) )
		{
			$data['res_date'] = calen_to_db($this -> input -> post('res_date'));	
		}

		
		if( $flag )
		{
			if (!empty($res_file_path))
			{
				$data['res_file_path'] = $res_file_path;
			}
	
			$this -> db -> where('res_id', $res_id);
	
			$res = $this -> db -> update('result', $data);
	
			if ($res) 
			{
				return TRUE;
			}
			else 
			{
				return FALSE;
			}
		}
		else 
		{
			return FALSE;
		}
		
	}
	
	function get_num_form() {
		$res = $this -> db -> get('eformat');

		return $res -> num_rows();

	}

	function get_form_limit($limit, $offset) {
		
		$qry = " SELECT *
		
		FROM eformat ORDER BY form_id DESC  
		
		LIMIT " . $offset . ", " . $limit;
		
		$res = $this -> db -> query($qry);

		return $res -> result();
		// this is an array that is returned by this function
	}

	function save_form() {

		$data = array();

		$data['form_title'] = $this -> input -> post('form_title');

		$data['form_subject'] = $this -> input -> post('form_subject');

		$data['form_content'] = nl2br($this -> input -> post('form_content'));

		$data['form_createdon'] = date("Y-m-d H:i:s");

		$data['form_updatedon'] = date("Y-m-d H:i:s");

		$res = $this -> db -> insert('eformat', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function form_detail($form_id) 
	{
		if( $this->only_num( $form_id ) )// code added by preeti on 11th apr 14 for black-box testing
		{
			$qry = " SELECT * FROM eformat 
		
			WHERE form_id = ?  ";
	
			$res = $this -> db -> query($qry, array($form_id));
	
			// below if else code added by preeti on 11th apr 14 for black-box testing
	
			if( $res->num_rows()  > 0)
			{
				$res_arr = $res -> result();
	
				return $res_arr[0];	
			}
			else 
			{
				return FALSE;
			}
		}
		else 
		{
			return FALSE;
		}		
		
	}

	/*function update_form() 
	{

		$data = array();

		$data['form_id'] = $this -> input -> post('form_id');

		$data['form_title'] = $this -> input -> post('form_title');

		$data['form_subject'] = $this -> input -> post('form_subject');

		$data['form_content'] = nl2br($this -> input -> post('form_content'));

		$data['form_createdon'] = date("Y-m-d H:i:s");

		$data['form_updatedon'] = date("Y-m-d H:i:s");

		$res = $this -> db -> where('form_id', $this -> input -> post('form_id'));

		$res = $this -> db -> update('eformat', $data);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}*/  // function commented by preeti on 11th apr 14
	
	// below function added by preeti on 11th apr 14
	
	function update_form() 
	{

		$flag = TRUE;

		$data = array();


		if( $this->only_num( $this -> input -> post('form_id') ) )
		{
			$data['form_id'] = $this -> input -> post('form_id');	
		}
		else 
		{
			$flag = FALSE;
		}
		
		if( $flag &&  $this->alpha_and_space( $this -> input -> post('form_title') ) )
		{
			$data['form_title'] = $this -> input -> post('form_title');	
		}
		else 
		{
			$flag = FALSE;
		}
		
		if( $flag &&  $this->alpha_and_space( $this -> input -> post('form_subject') ) )
		{
			$data['form_subject'] = $this -> input -> post('form_subject');	
		}
		else 
		{
			$flag = FALSE;
		}
		
		if( $flag &&  $this->alpha_and_space( $this -> input -> post('form_content') ) )
		{
			$data['form_content'] = $this -> input -> post('form_content');	
		}
		else 
		{
			$flag = FALSE;
		}


		if( $flag )
		{
			$data['form_createdon'] = date("Y-m-d H:i:s");

			$data['form_updatedon'] = date("Y-m-d H:i:s");
	
			$res = $this -> db -> where('form_id', $this -> input -> post('form_id'));
	
			$res = $this -> db -> update('eformat', $data);
			
			if ($res) 
			{
				return TRUE;
			} 
			else 
			{
				return FALSE;
			}
		}
		else 
		{
			return FALSE;
		}	

	}
	

	/*function del_form( $form_id )
	 {
	 $qry = " DELETE FROM eformat WHERE form_id = ? ";

	 $res = $this->db->query( $qry, array( $form_id ) );

	 if($res)
	 {
	 return TRUE;
	 }
	 else
	 {
	 return FALSE;
	 }

	 }*/

	function del_form($form_id_arr) {
		$form_ids = implode(',', $form_id_arr);

		$qry = " DELETE FROM eformat WHERE form_id  IN (" . $form_ids . ") ";

		$res = $this -> db -> query($qry);

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}

	}

	function get_email_format() {
		$qry = " SELECT *
		
		FROM eformat 
		
		WHERE form_status = 'a'
		
		ORDER BY form_id DESC ";

		$res = $this -> db -> query($qry);

		return $res -> result();
		// this is an array that is returned by this function containing the objects
	}
	
	// code commented by preeti on 11th apr 14 for black-box testing

	/*function ajax_email_format()// show the email format in the text area using ajax call
	{
		
		$form_id = $this -> input -> post('form_id');

		$sql = " SELECT form_content FROM eformat 
		
		WHERE form_id = ? ";

		$res = $this -> db -> query($sql, array($form_id));

		$res_arr = $res -> result();

		return $res_arr[0] -> form_content;

	}*/

	// code added by preeti on 11th apr 14 for black-box testing
	
	function ajax_email_format()// show the email format in the text area using ajax call
	{
		$form_id = $this -> input -> post('form_id');

		if( $this->only_num( $form_id ) )
		{
			$sql = " SELECT form_content FROM eformat 
		
			WHERE form_id = ? ";
	
			$res = $this -> db -> query($sql, array($form_id));
	
			$res_arr = $res -> result();
	
			return $res_arr[0] -> form_content;
		}
		else 
		{
			return FALSE;		
		}	

	}
	
	function get_email($reg_id_arr)// an array of doc_id is passed
	{
		// start, code added by preeti on 11th apr 14 for black-box testing	
			
		$valid_arr = array();
		
		foreach( $reg_id_arr as $val )
		{
			if( $this->only_num( $val ) )
			{
				$valid_arr[] = $val;
			}
		}
		
		// end, code added by preeti on 11th apr 14 for black-box testing
		
		if( COUNT( $valid_arr ) > 0 ) // if else added by preeti on 11th apr 14 for black-box testing
		{
			$this -> db -> select('*');

			$this -> db -> where_in('reg_id', $reg_id_arr);
	
			$res = $this -> db -> get('register');
	
			$res_arr = $res -> result();
	
			return $res_arr;
		}
		else 
		{
			return FALSE;
		}		
		
	}
	
	// below code added by preeti on 3rd apr 14
	
	function get_all_email( $reg_id_arr )// an array of doc_id is passed
	{
		// start, code added by preeti on 11th apr 14 for black-box testing	
		
		$valid_arr = array();
		
		foreach( $reg_id_arr as $val )
		{
			if( $this->alpha_num_dash( $val ) )
			{
				$valid_arr[] = $val;		
			}
		}
		
		$reg_id_arr = $valid_arr;
		
		// end, code added by preeti on 11th apr 14 for black-box testing
		
		$this -> db -> select('reg_email, reg_mobile');

		$this -> db -> where_in('reg_regno', $reg_id_arr);

		$res = $this -> db -> get('register');

		$res_arr = $res -> result();

		return $res_arr;
	}

	function get_regnum_email($regnum_arr)// an array of doc_id is passed
	{
		$this -> db -> select('reg_email');

		$this -> db -> where_in('reg_regno', $regnum_arr);

		$res = $this -> db -> get('register');

		$res_arr = $res -> result();

		return $res_arr;
	}

	function add_email_log($doc_ids, $log_format) {
		$data = array();

		foreach ($doc_ids as $doc_id) {
			$data[] = array('log_user' => $doc_id, 'log_format' => $log_format, 'log_num' => 'log_num' + 1, 'log_date' => date('Y-m-d H:i:s'));
		}

		$res = $this -> db -> insert_batch('elog', $data);

		return $res;
	}

	function get_last_date() {
		$qry = " SELECT * FROM last_date  ORDER BY ld_updatedon DESC LIMIT 0,1 ";

		$res = $this -> db -> query($qry);

		if ($res) {
			$res_arr = $res -> result();

			if (is_array($res_arr) && COUNT($res_arr) > 0) {
				return $res_arr[0];
			}

			return;
		} else {
			return;
		}

	}

	/*function save_date() 
	{
		// delete the values first and then add

		$res = $this -> db -> empty_table('last_date');

		if ($res) 
		{
			$data['ld_start'] = calen_to_db($this -> input -> post('ld_start'));// code added by preeti on 2nd apr 14 for black-box testing
			
			$data['ld_date'] = calen_to_db($this -> input -> post('ld_date'));

			$data['ld_validity'] = $this -> input -> post('ld_validity');

			$data['ld_otp_hours'] = $this -> input -> post('ld_otp_hours');

			$data['ld_updatedon'] = date('Y-m-d H:i:s');

			$res_ins = $this -> db -> insert('last_date', $data);

			return $res_ins;
		}

		return $res;
	}*/ // code commented by preeti on 11th apr 14 for black-box testing
	
	// below code added by preeti on 11th apr 14 for black-box testing
	
	function save_date() 
	{
		// delete the values first and then add
	
		$flag = TRUE;
		
		if( $this->alpha_num_dash( $this -> input -> post('ld_start') ) )
		{
			$data['ld_start'] = calen_to_db($this -> input -> post('ld_start'));	
		}
		else 
		{
			$flag = FALSE;
		}

		if( $this->alpha_num_dash( $this -> input -> post('lintern_date') ) )
		{
			$data['lintern_date'] = calen_to_db($this -> input -> post('lintern_date'));	
		}
		else 
		{
			$flag = FALSE;
		}
		
		if( $this->alpha_num_dash( $this -> input -> post('age_check') ) )
		{
			$data['age_check'] = calen_to_db($this -> input -> post('age_check'));	
		}
		else 
		{
			$flag = FALSE;
		}
		
		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('ld_date') ) )
		{
			$data['ld_date'] = calen_to_db($this -> input -> post('ld_date'));	
		}
		else 
		{
			$flag = FALSE;
		}	
		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('ld_validity') ) )
		{
			$data['ld_validity'] = $this -> input -> post('ld_validity');	
		}
		else 
		{
			$flag = FALSE;
		}		
		
		if( $flag && $this->alpha_num_dash( $this -> input -> post('ld_otp_hours') ) )
		{
			$data['ld_otp_hours'] = $this -> input -> post('ld_otp_hours');	
		}
		else 
		{
			$flag = FALSE;
		}
		
		
		$data['cletter_date'] = calen_to_db($this -> input -> post('cletter_date'));
		$data['cnf_date'] = calen_to_db($this -> input -> post('cnf_date'));
			
		$data['showregister']=$this->input->post('showregister');
		$data['showconfirm']=$this->input->post('showconfirm');
		$data['showcletter']=$this->input->post('showcletter');
		$data['showoletter']=$this->input->post('showoletter');
		$data['cost']=$this->input->post('cost');
		$data['ivenue']=$this->input->post('ivenue');
		
		$data['icenter']=$this->input->post('icenter');
		
		$data['m_mode']=$this->input->post('m_mode');

		if( $flag )
		{
			$data['ld_updatedon'] = date('Y-m-d H:i:s');

			$res = $this -> db -> empty_table('last_date');
	
			if ($res) 
			{	
				$res_ins = $this -> db -> insert('last_date', $data);
		
				return $res_ins;
			}
	
			return $res;
		}
		else 
		{
				return FALSE;
		}
		
	}

	function get_state_name($state_id) {
		$this -> db -> where('state_id', $state_id);

		$res = $this -> db -> get('state');

		if ($res -> num_rows()) {
			$res_arr = $res -> result();

			return $res_arr[0] -> state_name;
		}

	}

	function get_res_filename($res_id) {
		$this -> db -> select('res_file_path');

		$this -> db -> from('result');

		$this -> db -> where('res_id', $res_id);

		$res = $this -> db -> get();

		$res_arr = $res -> result();

		return $res_arr[0] -> res_file_path;
	}

	/*function del_res( $res_id )
	 {
	 $filename = $this->get_res_filename( $res_id );

	 $this->db->where('res_id', $res_id);

	 $res = $this->db->delete('result');

	 if($res)
	 {
	 if( $filename != '' )
	 {
	 $prev_file_path = "uploads/result/".$filename;

	 if(file_exists( $prev_file_path ))
	 {
	 unlink( $prev_file_path );
	 }
	 }

	 return TRUE;
	 }
	 else
	 {
	 return FALSE;
	 }

	 }*/

	function del_res($res_id_arr) {
			
		// start, code added by preeti on 11th apr 14 for black-box testing	
		
		$valid_data_arr = array();
		
		foreach( $res_id_arr as $val )
		{
			if( $this->only_num( $val ) )
			{
				$valid_data_arr[] = $val;
			}
		}

		// end, code added by preeti on 11th apr 14 for black-box testing

		$res_ids = implode(',', $valid_data_arr); //code modified by preeti on 11th apr 14 for black-box testing 

		$qry = " SELECT res_id, res_file_path 
		
		FROM result 
		
		WHERE res_id IN (" . $res_ids . ") ";

		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		$res = '';

		foreach ($res_arr as $row) {
			$filename = '';

			//$filename = 'uploads/result/' . $row -> not_link; // commented by preeti on 24th mar 14

			$filename = 'uploads/result/' . $row -> res_file_path; // added by preeti on 24th mar 14
			
			$res = $this -> db -> delete('result', array('res_id' => $row -> res_id));

			if ($res) {
				if ($filename != '' && file_exists($filename)) {
					unlink($filename);
				}
			}

		}

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_admin_email() {
		$this -> db -> select('admin_email');

		$res = $this -> db -> get('admin', 1, 0);

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr[0] -> admin_email;
		} else {
			return FALSE;
		}
	}

	function get_all_reg() {
		$this -> db -> select('reg_id');

		$this -> db -> from('register');

		$res = $this -> db -> get();

		if ($res -> num_rows() > 0) {
			return $res;
		} else {
			return FALSE;
		}
	}

	function empty_regnum() {
		$res = $this -> db -> empty_table('regnum');

		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	// below function added by preeti on 11th apr 14
	
	function alpha_and_space($str) // only for alpha and space
	{
	    if( ! preg_match("/^([-a-z_ ])+$/i", $str) )
	    {
	    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
				
	    	return FALSE;
	    }
		else 
		{
			return TRUE;	
		}	
		
	}
	
	
	// below function added by preeti on 17th apr 14
	
	function alpha_num_at_space($str) // only for alpha and space and @ and numerics
	{
		 //if( ! preg_match("/^([-a-z@_ ])+$/i", $str) )
		 if( ! preg_match("/^([-0-9a-z_@\. ])+$/i", $str) )
		 {
		    	$this->form_validation->set_message('alpha_and_space', 'The %s field may only contain alphabet characters.');	
					
		    	return FALSE;
		 }
		else 
		{
			return TRUE;	
		}	
			
	}
	
	// below function added by preeti on 26th apr 14
	
	function audit_user_del( $id_arr ) 
	{
		$this->db->where_in('logu_id', $id_arr);

		$res = $this->db->delete('login_user');

		if ($res) 
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}

	}
	
	// below function added by preeti on 26th apr 14
	
	function audit_admin_del( $id_arr ) 
	{
		$this->db->where_in('loga_id', $id_arr);

		$res = $this->db->delete('login_admin');

		if ($res) 
		{
			return TRUE;
		} 
		else 
		{
			return FALSE;
		}

	}
	
	// below functions added by preeti on 3rd jul 14
	
	function get_doc_zip( $offset, $limit, $sort_by, $sort_order ) //  parameter added by preeti on 3rd jul 14 
	{
		$qry = " SELECT reg_regno FROM register 
		
		JOIN user ON reg_regno = u_regno
		
		WHERE reg_status = 'c' ";
		
		$qry .= " ORDER BY ".$sort_by.' '.$sort_order;
		
		$qry .= " LIMIT ".$offset." ,  ".$limit;
		
		$res = $this -> db -> query($qry);

		$res_arr = $res -> result();

		if ($res -> num_rows() > 0) {
			$res_arr = $res -> result();

			return $res_arr;
		} else {
			return FALSE;
		}
	}
	
	// function added by preeti on 4th jul 14
	
	function get_reg_id_zip()
	{
		$qry = " SELECT reg_regno FROM register 
		
		WHERE reg_status = 'c' ";
		
		$qry .= " ORDER BY reg_regno ASC ";
		
		$qry .= " LIMIT 1900, 100  ";
		
		$res = $this -> db -> query($qry);

		if ($res -> num_rows() > 0) 
		{
			$res_arr = $res -> result();

			return $res_arr;
		} 
		else 
		{
			return FALSE;
		}
	}
	
	function update_admin_otp($rand)
	{
		$data['admin_otp'] = $rand;
		$this -> db -> where('admin_id', 1);
		$res = $this -> db -> update('admin', $data);
		if ($res) 
		{
			$msg="$rand is your one time password for admin login.";
			$encoded_msg = urlencode($msg);
			$this->send_sms_curl($encoded_msg,'9818556726');
			$this->send_sms_curl($encoded_msg,'9868160310');
			//$this->send_sms_curl($encoded_msg,'9015714801');
			return TRUE;
		}
		else 
		return FALSE;
	}

	function send_sms_curl( $message, $mnumber )
	{
		
		$url = "http://smsgw.sms.gov.in/failsafe/HttpLink?username=nicsup.auth&pin=Tp%24m7%23Rc8&message=".$message."&mnumber=91".$mnumber."&signature=NICSUP"; 
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request); 
		$response = curl_exec($ch); 
		curl_close($ch); 
		
		if(strpos($response, "Message Accepted") === FALSE )
		return FALSE;	
		else 
		return TRUE;
	
	}
	

}

function con_login_session(){
	$data['session_id'] = $this->session->userdata['session_id'];
	$this->db->where('admin_id',1);
	$res = $this->db->update('admin',$data);
}