<?php
class oletter_model extends CI_Model
{

	public function getivdate($regno)
	{
	
	$idate='0000-00-00';
	$sql="select * from managecall where `from` <= '$regno' and `to` >='$regno'";
	$sql="select * from managecall where `id` = '$regno' ";
	
	$qry=$this->db->query($sql);
	$rows=$qry->result();
	foreach($rows as $row)
	{
	$idate=$row->idate;
	}	
	return $rows[0];
	
	//return $idate;
	
	}
	
	
	public function getsinglecall($regno='',$dob='',$fidate='')
	{
	
	$html='';
	$fdob=$dob;
	if($dob[2]=='/')
	{
	$dobs=explode('/',$dob);
	$fdob=$dobs[2].'-'.$dobs[1].'-'.$dobs[0];
	}

	$sql="select * from offer where regn='$regno' ";
	$query = $this->db->query($sql);
	$numrows=$query->num_rows();
	if($numrows==0)
	{
		redirect('oletter/index/Your registration number does not exists in the list. ');
		exit;
	}

$rowsoffer=$query->result();
$offer=$rowsoffer[0];
$service=$offer->service;


if($service=='2')
{
	
	//redirect('oletter/index/Your registration number is not exists in first list. ');
	//exit;
	
}
$reportunit=$offer->unit;
$nrs=$offer->nrs;

$sql="select * from user where u_regno='$regno' and u_dob='$fdob' ";
$query = $this->db->query($sql);
$numrows=$query->num_rows();
if($numrows==0)
{
	redirect('oletter/index/Your Registration number or date of birth is incorrect ');
	exit;
}

$cyear=date('Y');
$cletter_date='23/24 Jul 2015';

$sql="select * from user where u_regno='$regno'";
$qry=$this->db->query($sql);
$rows=$qry->result();
$res=$rows[0];
$fname=$res->u_fname;
$lname=$res->u_lname;
$mname=$res->u_mname;
$dob=$res->u_dob;
$regno=$res->u_regno;
$ad1=$res->post_address;
$ad2=$res->state_name.' '.$res->post_pin;

$servicetext="Army";
if($service=='2')
$servicetext="Navy";
else if($service=='3')
$servicetext="Air";

$amb=$offer->amb;

$cletter_date="27 Feb 2018";

if($amb==1)
{
	$cletter_date="13 Apr 2018";
	if($service==1)
	$cletter_date="13 Apr 2018";
}

// if($service=='2')
// $cletter_date="24 Jul 2015";
// else if($service=='3')
// $cletter_date="23 Jul 2015";

$authdate="27 Feb 2018";
if($amb==1)
$authdate="12 Apr 2018";
//$ldate='Jan-'.date('Y');
//if($service==1)
$ldate='Jan-'.date('Y');	

$html.='
<style>



</style>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>

<td valign="top" width="70%">
<div style="font-size:13px;">Tele: 23093740</div><br>

</td>

<td width="33%">


<div style="font-size:13px;">
<u><b>REGISTERED</b></u>
<br /><br />
Office of the Director General
<br />
Armed Forces Medical Services
<br />
Ministry of Defence
<br />
\'M\' Block, New Delhi - 110001
</div>
</td>
</tr>

<tr>
<td>
<br />
<div style="font-size:13px;">42190/SSC/Jul-'.$cyear.'/'.$servicetext.'/DGAFMS/DG-1A </div>
</td>

<td>
<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:14px;">'.$cletter_date.'</span> 
</td>
</tr>
</table>
<br />
<table width="450" cellpadding="0" cellspacing="0"> 
<tr>
<td width="30">
<div style="width:600px; font-size:13px">
Dr. '.strtoupper($fname).' '.strtoupper($mname).' '.strtoupper($lname).'


</div>
</td>

</tr>

<tr>
<td>
</td>
<td width="250">

</td>
</tr>
</table>


<table align="center">
<tr>
<td >
<h4 style="text-align:center;font-size:13px"><u>GRANT OF SHORT SERVICE COMMISSION IN <br />THE

<font> ARMY MEDICAL CORPS : BATCH-JUL '.$cyear.'</font> 
<br>

<font>REGISTRATION NO. '.strtoupper($regno).'</font>
</u></h4>
</td>
</tr>
</table>


<ul id="cltul"  style="list-style-type:none;text-align: justify;margin-left:-40px; font-size:13px">
<li style="margin-bottom:3px;">1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reference your application for grant of Short Service Commission in the Army Medical Corps.</li>
<p><b><u>Appointment and Tenure</u></b><p>
<li style="margin-bottom:3px;">2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You have been selected for grant of Short Service Commission in the Army Medical Corps under the terms and conditions of service laid down in Army Instructions (AI) 75/78, as amended, for a period of five years in the first instance.  The grant of Short Service Commission in the Army Medical Corps is extendable up to a total period of 14 years including previous AMC service, if any, through two further spells of 5 years and 4 years respectively, subject to overall performance and meeting the laid down eligibility criteria.  The tenure of Short Service Commission will not be extended beyond the age of 55 years or 14 yrs service whichever is earlier.  This commission will be provisional in the first year subject to receipt of positive verification of your character antecedents from the Civil/Police authorities, that you are fit for Commission in the Army Medical Corps with a subsequent notification issued in the Gazette of India.</li>
<p><b><u>Reporting and Commissioning</u></b></p>
<li style="margin-bottom:3px;">3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are requested to report to the Commandant / Commanding Officer of the unit mentioned against your name in the Appendix to this letter <b>on 21 Oct 2019 </b> If you fail to report <b>within the stipulated date, this appointment letter will automatically be cancelled and the next eligible candidates in the merit list would be offered the appointment. Your date of commission will be the next day of your reporting,</b> Provided you are found fit in medical inspection, Please note that:- 
<br />

<ol class="subol" style="list-style:none;margin-left:0px;">
<li>(a)&nbsp;&nbsp;&nbsp;&nbsp;<b>Your seniority for pay and promotion will be from the date of Commission.  The inter-se seniority for officers commissioned on the same day will, however, be determined by the merit in selection.</b> 
</li>
<li>(b)&nbsp;&nbsp;&nbsp;&nbsp;You will be on probation for a period of one year from the date of your Commission.  In case you are reported upon during your probationary period as unsuitable to retain your commission, your service may be terminated at any time before or after expiry of the probation period on one month\'s notice.
</li>
<li>(c)&nbsp;&nbsp;&nbsp;&nbsp;<u><b>Eligibility for Permanent Commission.</b>&nbsp;&nbsp;</u>You will be eligible for consideration of Permanent Commission after 2 years of SSC service as per extant rules and subject to fulfilling all the eligibility conditions as amended, provided you have not attained the age as applicable for the qualification you possessed before the grant of SSC as indicated below:-

<ul style="list-style:none">
<li>(i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><u>MBBS</u></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30 years as on 31 Dec of the year of application<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
for Departmental Permanent Commission.
</li>

<li>(ii)&nbsp;&nbsp;&nbsp;&nbsp;  <b><u>Diploma</u></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;31 years as on 31 Dec of the year of application<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
for Departmental Permanent Commission.
</li>

<li>(iii)&nbsp;&nbsp;&nbsp;  <u><b>Post Graduate</b></u>&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;35 years as on 31 Dec of the year of application<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
for Departmental Permanent Commission.
</li>

</ul>
</li>

</ol>

</li>
<p><u><b>Documents Required</b></u></p>
<li style="margin-bottom:3px;">4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At the time of joining, you are required to produce under mentioned certificates in original with a copy thereof for verification by Commandant /Commanding Officer:-
<br />
<ol class="subol" style="list-style:none;margin-left:0px;">
<li>(a)&nbsp;&nbsp;&nbsp;&nbsp;Higher Secondary / Sr. Secondary School Certificate for verification of date of birth.
</li>
<li>(b)&nbsp;&nbsp;&nbsp;&nbsp;Internship Completion Certificate.
</li>
<li>(c)&nbsp;&nbsp;&nbsp;&nbsp;Permanent Medical Registration Certificate.
</li>
<li>(d)&nbsp;&nbsp;&nbsp;&nbsp;MBBS Degree/Pass Certificate.
</li>
<li>(e)&nbsp;&nbsp;&nbsp;&nbsp;PG Diploma or/& PG Degree recognised by MCI, if applicable.
</li>
<li>(f)&nbsp;&nbsp;&nbsp;&nbsp;Attempt cum Transcript Certificate.
</li>
</ol>



</li>

<li>5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Without these certificates you will not be allowed to join service.</b><br /><br /></li>

<p><u><b>Ante-date Seniority</b></u></p>
<li style="margin-bottom:3px;">6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A candidate who has held an approved whole-time appointment in a recognised civil hospital for a continuous period of six months or more, <b>OR</b> is in possession of a Post Graduate Diploma in any branch of medical science recognised by the Medical Council of India, <b>OR</b> is in possession of a Post Graduate Degree such as Doctor of Medicine, Master of Surgery, or an equivalent or higer qualification, will be entitled for antedate of commssion in accordance with Para 9 of AI 75/78, as amended.</li>
<li style="margin-bottom:3px;">7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you are entitled for any antedate of commission in accordance with Para 9 of AI 75/78, as amended, you should submit your claim in accordance with the existing procedure soon after joining duty, OR latest within one year of joining service.  Any antedate seniority granted will count only towards seniority promotions and not for back pay & allowances.  You are advised to retain a copy of reference under which your antedate claim is processed till such time the claim is finalized.</li>

<p><u><b>Expected Time of Arrival (ETA)</b></u></p>
<li style="margin-bottom:3px;">8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please inform the Commandant/Commanding Officer fo the Unit where you have been posted of your expected time of arrival (ETA) so that necessary arrangement for your reception can be made. </li>

<li style="margin-bottom:3px;">9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are advised to obtain prior permission of the Commandant / Commanding Officer of the unit before considering bringing your family to your duty station.</li>
<p><u><b>Reporting</b></u></p>
<li style="margin-bottom:3px;">10.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>Current Medical Status.</b></u>&nbsp;&nbsp; On your initial/AMB/RMB medical examination you have been found fit by the Medical Board.  However, you are required to undergo medical inspection on reporting to the unit mentioned against your name to simply confirm that there is no deterioration in your health status.  In case more than 180 days have passed from the last medical examination, detailed medical examination (in AFMS-2) will be carried out again.</li>

<li style="margin-bottom:3px;">11.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>Documentation</b></u>&nbsp;&nbsp;You have to obtain a proforma regarding submission of your photographs and details of personal particulars from the Commandant/ Commanding Officer.  The same, duly competed is required to be handed over along with the photographs for onward submission to DGAFMS/DG-1X (CRD Cell).  You are also required to fill the proforma as laid down in Appendix ‘A’ to Army Order (AO) 3/2003 for release of your pay and allowances and submit the same to the Unit Office, for onward submission to the concerned CDA/paying office.  You have to apply to the authorities concerned through proper channel for the issue of the Identity Card after your reporting for duty. </li>

<u><b>Change of Service, Date of Reporting or Unit</b></u>
<li style="margin-bottom:3px;">12.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please note that request for change of service, place of posting and extension of date of joining duty will not be accepted except in exceptional cases with the approval of competent authority.  Non-reporting on the due date will result in cancellation of the candidature of the candidate.</li>
<li style="margin-bottom:3px;">13.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>Inter Services Transfer</b></u>&nbsp;&nbsp;Armed Forces Medical Services (AFMS) is a tri-services organization and you may be transferred from your parent service to any other service with change of uniform, based on organizational requirements whenever directed by the DGAFMS.  However, any request from your side for change of service on personal grounds will not be entertained in the first three years of your commissioned physical service.</li>
<br>
<br>
<p><u><b>Unwillingness</b></u></p>
<li style="margin-bottom:3px;">14.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In case you are unwilling to join the Armed Forces Medical Services (AFMS) on the basis of this offer letter, you are requested to intimate the same to the commandant/Commanding Officer under intimation to the office immediately through e-mail (dirafmsp-mod@nic.in) so that the next eligible candidate in the merit list could be offered appointment. </li>
<br>
<p><u><b>Resignation & Release</b></u></p>
<br>
<li style="margin-bottom:3px;">15.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The minimum tenure of Short Service Commission is 05 years.  A candidate may apply to resign from the service without completing minimum 05 years of service will not be eligible to apply for Short Service Commission in future.  Resignation from Service before completion of first tenure of 05, will result in forfeiture of all the service benefits, including the Ex-serviceman status.  However, the officer can seek release from service after completion of the first tenure of five years </li>
<br>
<p><u><b>Reserve Liability</b></u></p>
<br>
<li style="margin-bottom:3px;">16.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;On completion of the contractual service and release from service, you would be liable for reserve duty for a period of 5 yrs/after your date of release as per the provisions of the Army Instructions on the subject.  </li>

<li><h4 style="text-align:center;font-size:13px"><u>ATTENTION COMMANDANT/ UNIT CONCERNED</u></h4></li>
<p><b><u>Medical Examination</u></b></p>
<li style="margin-bottom:3px;">17.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The candidate on reporting for duty will be subjected to medical examination in unit/MI Room before being allowed to join to ensure that his/her medical category has not deteriorated since the initial medical board.  In case more than 180 days have passed since the last medical examination of the candidate, a detailed medical examination in AFMS-2 will be carried out.  The candidate will not be allowed to join if found medically unfit and report to this effect along with a certificate will be sent to the Office of DGAFMS/DG-1A immediately.  </li>


<p><b><u>Casualty Report</u></b></p>

<li style="margin-bottom:3px;">18.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reporting/Non-reporting of the candidates will be intimated by 24 Oct 2019 to concerned DGsMS immediately, who, in turn, will forward the same to DGAFMS/DG-1A in consolidated form.  Subsequently, Do Part II/Gen Form/POR in respect of the candidates commissioned be forwarded to concerned DGsMS & DGAFMS/DG-1A.  It may be noted that delay in receipt of DO Part II/Gen Form/POR results in delay in allotment of MS Number<br />
<br />
</li>
<li style="margin-bottom:3px;">19.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regarding submission of personal particulars and antedate claim of the newly commissioned officers, please refer to this office letters No 8571/DGAFMS/DG-1A dt 25 Jul 1975 and 8571/79/DGAFMS/DG-1A dt 16 Aug 1979 respectively forwarded to all concerned through proper channel.<br />
<br />
</li>

<li style="margin-bottom:3px;">20.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report to <b>'.$reportunit.'</b><br />
<br />
</li>
<li style="margin-bottom:3px;">21.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nearest Railway Station : <b>'.$nrs.'</b><br />
<br />
</li>




<li style="margin-bottom:3px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authority:  DGAFMS Sanction letter No.42190/SSC/'.$ldate.'/DGAFMS/DG-1A dated '.$authdate.'.
</li>


</ul>


';

$html.='<table align="right">
<tr>
<td>
<img width="150" src="images/vksingh.png"/><br>

<div font-size:13px;">(Virendra Kumar Singh)
<br>
Dy Dir/DG-1A
<br>
For DGAFMS
</div>
</td> 
</tr>
</table>
	';

/*if($service=='2')
{
	
$html.='<table align="right">
<tr>
<td>
<img src="images/vsign.jpg"/><br>

<div font-size:13px;">(Virendra Kumar Singh)
<br>
Dy Dir/DG-1A
<br>

For DGAFMS
</div>
</td> 
</tr>
</table>
	';
/*}
else
{
$html.='<table align="right" style="font-size:14px">
<tr>
<td>
<img src="images/ccsign.png"/><br>

<div font-size:13px;">(Chandana Chakravarti)
<br>
Director AFMS (P)
<br>

For DGAFMS
</div>
</td> 
</tr>
</table>
	';*/
	
	
/*}*/


$html.='

<br style="clear:both"  />
<br />
<br />
<br />
<br />
<br />

';

if($service=='1')
{
$html.='
<ul id="cltul"  style="list-style-type:none;text-align: justify;margin-left:-40px; font-size:13px">
<li style="margin-bottom:3px;"><u>Copy to :-</u><br /><br /></li>

<li style="margin-bottom:3px;"><b>MG (Med), HQ Southern Command, Pune</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>MG (Med), HQ Eastern Command, Kolkata </b><br /><br /></li>
<li style="margin-bottom:3px;"><b>MG (Med), HQ Central Command, Lucknow</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>MG (Med), HQ Western Command, Chandimandir</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>MG (Med), HQ Northern Command, Udhampur</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>MG (Med), HQ  South Western Command, Jaipur</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PCDA(O) Pune&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PCDA (Pension) Allahabad </b><br /><br />
<b>Unit Concerned </b><br /><br />
<u>Internal</u>
<br /><br />
</li>
<li style="margin-bottom:3px;"><b>DGMS-1B &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MPRS(O)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DG-1B (II)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DGMS/MS-1A&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CRD Cell</b><br /></li>
</ul>




';

}

else if($service=='2')
{
$html.='
<ul id="cltul"  style="list-style-type:none;text-align: justify;margin-left:-40px; font-size:13px">
<li style="margin-bottom:3px;"><u>Copy to :-</u><br /><br /></li>';
//<li style="margin-bottom:3px;"><b>CMO HQ Eastern Naval Command, Visakhapatnam</b><br /><br /></li>
$html.='<li style="margin-bottom:3px;"><b>CMO HQ Southern Naval Command, Cochin</b><br /><br /></li>';
//<li style="margin-bottom:3px;"><b>CMO HQ Western Naval Command, Mumbai</b><br /><br /></li>
$html.='<li style="margin-bottom:3px;"><b>PCDA (Navy) Mumbai&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PCDA (Pension) Allahabad</b><br />
<br />
<b>Naval Pay Office, Shahid Bhagat Singh Road, Mumbai-400023</b>
<br />
<br />


<b>Unit Concerned </b><br /><br />
<u>Internal</u>
<br /><br />
</li>
<li style="margin-bottom:3px;"><b>DGMS (Navy)/PDMS(P&M)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MPRS(O)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DG-1B (II)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CRD Cell</b><br /></li>
</ul>




';		
}
else if($service=='3')
{
$html.='
<ul id="cltul"  style="list-style-type:none;text-align: justify;margin-left:-40px; font-size:13px">
<li style="margin-bottom:3px;"><u>Copy to :-</u><br /><br /></li>

<li style="margin-bottom:3px;"><b>PMO HQ South Western Air Command, Gandhinagar</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PMO HQ Southern Air Command, Thiruvananthapuram</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PMO HQ Central Command, Allahabad</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PMO HQ Western Air Command, Subroto Park</b><br /><br /></li>';
/*<li style="margin-bottom:3px;"><b>PMO HQ Eastern Air Command, Shillong</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PMO HQ Training Command, Bangalore</b><br /><br /></li>
<li style="margin-bottom:3px;"><b>PMO HQ Maintenance Command, Nagpur</b><br /><br /></li>*/
//$html.='<li style="margin-bottom:3px;"><b>PCDA (AF) Dehradun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PCDA (O) Pune&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PCDA (Pension) Allahabad</b><br /><br />';
$html.='<li style="margin-bottom:3px;"><b>PCDA (AF) Dehradun&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PCDA (Pension) Allahabad</b><br /><br />';

$html.='<b>Unit Concerned </b><br /><br />
<u>Internal</u>
<br />
<br />
</li>
<li style="margin-bottom:3px;"><b>DGMS (Air)/Med-1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MPRSO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DG-1B (II)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CRD Cell</b><br /></li>
</ul>




';	
	
}


 
 
return $html;

}
public function generatecall($regno='',$dob='')
{



//require_once "asset/pdf/dompdf_config.inc.php";

   

$this->load->library('dompdf');
$this->load->helper('dompdf');




/*$dompdf = new DOMPDF();
$html = '
<html>
<body style="font-family:Arial, Helvetica, sans-serif; font-size:10px; ">
';*/

$html = '
<style>

ol li{text-align:justify;} 
</style>

';



$html.=$this->getsinglecall($regno,$dob);


$file=$regno.".pdf";
 gendompdf($html,$file,"portrait","A4");





//$filename="$day.".pdf";
//file_put_contents($filename, $output);







return true;

	
}








}