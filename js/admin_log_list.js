$(document).ready(function() {

  // whenever a select box option is chosen , show the content in the textarea
  
  $('#email').change(function(e){
  	
        $this = $(e.target); // select element 
    
    	if( $this.val() )
    	{
    		
    		var base_url = $('#base_url').val();
    		
    		  	$.ajax({
    
	            type: "POST",
	    
	            //url: "http://localhost/iamc/ajax/ajax_email_format",// commented to run on network
	            
	            url: base_url+"ajax/ajax_email_format", 
	    
	            data: { form_id: $this.val() },
	    
	            success:function(data){
	            	
	            	$('#form_content').html(data);
	    
	                $('#form_id').val($this.val());
	                
	                $('#form_id_hid').val($this.val());
	                
	                
	            }
    
        	});   		
    		
    	}
    	else
    	{
    		$('#form_content').html('');
	    
	        $('#form_id').val($this.val());
	        
	        $('#form_id_hid').val($this.val());
	        
	        
	        		
    		alert("Please select an email format");
    	}     
    
    });    
    
    
    $('#check_all').on('click', function()
    {
  		if($(this).is(':checked'))
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', true);
						
			});			
		}
		else
		{
			$(':checkbox').each(function()
			{
				$(this).prop('checked', false);
						
			});
		}
  			
  	});
    
    
    $('#frm').submit(function()
    {
		if($('#form_id').val() != '' )
		{
		
			var flag = false;	
	
			$(':checkbox').each(function()
			{
				if($(this).is(':checked'))
				{
					flag = true;
				}
						
			});			
			      		
	
			if( flag )
			{
				var didConfirm = confirm("Do you want to send an email to the selected User(s) ?");
	
				if (didConfirm == true) 
				{
				   return true;
				}
				else
				{
				   return false;
				}
	
			}
			else
			{
				alert("Please select atleast one User");
						
				return false;
			}
			
		}
		else  
		{
			alert("Please choose an email format");
						
			return false;
		}
								
     });
     
    $('#frm_excel').submit(function()
    {
		if($('#form_id').val() != '' )
		{
			var flag = false;	
	
			if( $('#excel_file').val() != '')
			{
				flag = true;					
			}			
			      		
	
			if( flag )
			{
				var didConfirm = confirm("Do you want to send an email to the User(s) specified in the Excel ?");
	
				if (didConfirm == true) 
				{
				   return true;
				}
				else
				{
				   return false;
				}
	
			}
			else
			{
				alert("Please select an excel file");
						
				return false;
			}
			
		}
		else  
		{
			alert("Please choose an email format");
						
			return false;
		}
								
     });
     
     
     $('#up_excel').hide();
     
     $('#show_list').hide();
     
    $('#excel_id').on('click', function()
    {
    	$('#show_list').hide();
    	
    	$('#up_excel').show();
    });
    
    $('#list_id').on('click', function()
    {
    	$('#up_excel').hide();
    	
    	$('#show_list').show();
    	    	
    });  
	
});