$(document).ready(function() {
	
	var base_url = $('#base_url').val(); // line added by preeti on 12th mar 14
	var icenter = $('#icenter').val(); // line added by preeti on 12th mar 14

  // show a warning on mobile existence
  
   $('#reg_mobile').blur(function()
  {
  		var mobile = $('#reg_mobile').val(); 		
   	
   		if( mobile != '' )
   		{   			
   			//$.post('http://localhost/iamc/ajax/ajax_check_mobile',
   		
   			// above line commented and below line added by preeti on 12th mar 14
   			
   			$.post(base_url+'ajax/ajax_check_mobile',
   		
	   		{ 'reg_mobile': mobile,'icenter': icenter },
	   		
	   		function( result)
	   		{   			
	   			if( result > 0 )
	   			{
	   				$('#div_mobile').show();
	   				
	   				$('#msg_span_mobile').html('(Mobile already exist)');   				
	   				
	   			}
	   			else if( result == 'invalid' ) // else if block added by preeti on 11th apr 14
	   			{
	   				$('#div_email').show();
	   				
	   				$('#msg_span_email').html('(Mobile Invalid)');
	   			}
	   			else
	   			{
	   				/*$('#div_mobile').show();
	   				
	   				$('#msg_span_mobile').html(''); */  
	   				
	   				$('#div_mobile').hide();				
	   				   								
	   			}
	   		}
	   		
	   		);
	   				
	   	}
   		
   		
  	
  } );
  
  
  // show a warning on email existence
  
   $('#reg_email').blur(function()
  {
   		var email = $('#reg_email').val();
   		
   		if( email != '' )
   		{
   			
			
			
   			//$.post('http://localhost/iamc/ajax/ajax_check_email',
   			
   			// above line commented and below line added by preeti on 12th mar 14
   			
   			$.post(base_url+'ajax/ajax_check_email',
   			   		
	   		{ 'reg_email': email,'icenter': icenter },
	   		
	   		function( result)
	   		{
				
	   			if( result > 0 )
	   			{
	   				$('#div_email').show();
	   				
	   				$('#msg_span_email').html('(Email already exist)');   				
	   				
	   			}
	   			else if( result == 'invalid' ) // else if block added by preeti on 11th apr 14
	   			{
	   				$('#div_email').show();
	   				
	   				$('#msg_span_email').html('(Email Invalid)');
	   			}
	   			else
	   			{
	   				/*$('#div_email').show();
	   				
	   				$('#msg_span_email').html(''); */ 
	   				
	   				$('#div_email').hide(); 				
	   				   								
	   			}
	   		}
	   		
	   		);
	   				
	   	}  		
  	
  } );
  
   
$('#reg_aadhar').blur(function()
  {
  		var reg_aadhar = $('#reg_aadhar').val(); 		
   	
   		if( reg_aadhar != '' )
   		{   			
   			$.post(base_url+'ajax/ajax_check_aadhar',
   		
	   		{ 'reg_aadhar': reg_aadhar,'icenter': icenter },
	   		
	   		function( result)
	   		{   			
	   			if( result > 0 )
	   			{
	   				$('#div_reg_aadhar').show();	   				
	   				$('#msg_span_reg_aadhar').html('(Aadhar Number already exist)');   					   				
	   			}
	   			else
	   			{
	   				$('#div_reg_aadhar').hide();					   				   								
	   			}
	   		}
	   		
	   		);	   				
	   	} 	
  } );
  
  $('#reg_pan').blur(function()
  {
  		var reg_pan = $('#reg_pan').val(); 		
   	
   		if( reg_pan != '' )
   		{   			   			
   			$.post(base_url+'ajax/ajax_check_pan',
   		
	   		{ 'reg_pan': reg_pan,'icenter': icenter },
	   		
	   		function( result)
	   		{   			
	   			if( result > 0 )
	   			{
	   				$('#div_reg_pan').show();
	   				
	   				$('#msg_span_reg_pan').html('(PAN Number already exist)');   				
	   				
	   			}
	   			else
	   			{
	   				$('#div_reg_pan').hide();					   				   								
	   			}
	   		}
	   		
	   		);
	   				
	   	}		
  } );
  
  // check if the del-link class element has been made a onclick event, first check if it exists on the page
  
  if($('.del-link').length )
  {
  		//$('.del-link').live('click', function(e) 
    	
    	//$('.del-link').on( "click", function()
    	
    	$('.del-link').click( function()
    	{
    		return confirm("Do you really want to delete this Item ?");
    	}); 	
  	
  } 
  
  if( $('#not_type').val() == 'c' )
  {
  		$('#link_div').hide();
  			
  		$('#con_div').show();  	
  }
  else if( $('#not_type').val() == 'l' )// show the file div
  {
  		$('#con_div').hide();
  			
  		$('#link_div').show();  			 			
  }
  else
  {
  		$('#con_div').hide();
  			
  		$('#link_div').hide();
  }
    
  
$("#f1").submit(function() {
  var textVal = $("#from").val();
  var textVal1 = $("#to").val();
  if(textVal == ""||textVal1=="") {
    alert('Enter From and To Reg Nos.');
    return false;
  }
});



  //$('#not_type').live('change', function(e)
  
  $('#not_type').change(function() 
  {
  	var value = $(this).val();
  	
  	if( value != '' )
  	{
  		if( value == 'c' ) // show the content div
  		{
  			$('#link_div').hide();
  			
  			$('#con_div').show();
  			 			
  		}		
  		else if( value == 'l' )// show the file div
  		{
  			$('#con_div').hide();
  			
  			$('#link_div').show();
  			 			
  		}
  	}
  
  });
  
  
   $('#check_all').click( function()
    {
  		if($(this).is(':checked'))
		{
			$(':checkbox').each(function()
			{
				//$(this).prop('checked', true);
				
				$(this).attr('checked','checked');
						
			});			
		}
		else
		{
			$(':checkbox').each(function()
			{
				//$(this).prop('checked', false);
				
				$(this).removeAttr('checked');
						
			});
		}
  			
  	});
  	
  	$('#del').click( function()
    {
    	var ischecked = false;
    	
    	$(':checkbox').each(function()
		{
			if( $(this).is(':checked') )
			{
				if( !ischecked )
				{
					ischecked = true;	
		
		
				}		
							
			}
			
		});
		
		if( !ischecked )
		{
			alert(" Please check at least one checkbox to delete user");
			
			return false;
		}
		else
		{
			return true;
		}
    	
   	});
   	$('#adel').click( function()
    {
    	var ischecked = false;
    	
    	$(':checkbox').each(function()
		{
			if( $(this).is(':checked') )
			{
				if( !ischecked )
				{
					ischecked = true;	
		
		
				}		
							
			}
			
		});
		
		if( !ischecked )
		{
			alert(" Please check at least one checkbox to delete user");
			
			return false;
		}
		else
		{
			return true;
		}
    	
   	});
$('#rej').click(function() {
    	if($('input[type=checkbox]:checked').length == 0)
      {
      	 alert('Please check a user to reject. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });
$('#app').click(function() {
    	if($('input[type=checkbox]:checked').length == 0)
      {
      	 alert('Please check a user to approve. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });
   	
$('#pn').click(function() {
    	if (!$("input[name='Typ']:checked").val())
      {
      	 alert('Please check a radio button to print docs. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });
 
$('#dz').click(function() {
    	if (!$("input[name='Typ']:checked").val())
      {
      	 alert('Please check a radio button to download user docs. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });
$('#pp').click(function() {
    	if (!$("input[name='Typ']:checked").val())
      {
      	 alert('Please check a radio button to print preview. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });
$('#gp').click(function() {
    	if (!$("input[name='Typ']:checked").val())
      {
      	 alert('Please check a radio button to print pdf. '); 
      	 return false;
      	  }
      	   else 
      	   {
      	   //	 alert('One of the radio buttons is checked!');
      	   	  }
      	   	   });

	
});