$(document).ready(function() {

var bar = $('.bar');

var percent = $('.percent');

var status = $('#status');

var base_url = $('#base_url').val();

var reg_id = $('#reg_id').val();

var u_current_emp = $('input[name=u_current_emp]:checked').val();

var u_previous_ssc = $('input[name=u_previous_ssc]:checked').val();
var u_previous_unfit = $('input[name=u_previous_unfit]:checked').val();

var confirm_random = $('#confirm_random').val(); // added by preeti on 22nd apr 14 for manual testing

var sequence_token = $('#sequence_token').val(); // added by preeti on 23rd apr 14 for manual testing

$('#upload_form').ajaxForm({
	
    beforeSend: function() {
    
        status.empty();
    
        var percentVal = '0%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
		$('#main').addClass('opaque');
		
		document.getElementById("loading").style.display="block";
    },
    
    uploadProgress: function(event, position, total, percentComplete) {
    
        var percentVal = percentComplete + '%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
    },
  
  complete: function(xhr) {
  
		$('#main').removeClass('opaque');
  	  document.getElementById("loading").style.display="none";
	//  document.getElementById("status").style.display="block";
	
	   document.getElementById("status").style.border="1px solid #ddd";
	 document.getElementById("status").scrollTop;
	//  document.getElementById("fullright").scrollIntoView();
  
    status.html(xhr.responseText);// if there are validation errors then show then on the same screen     
    
  	if( xhr.responseText == 'continue' )
  	{
  		//document.location = base_url+'doc/final_saved/'+reg_id; // line commented by preeti on 23rd apr 14 for manual testing
  		
  	//	document.location = base_url+'doc/final_saved/'+reg_id+'/'+sequence_token;// line added by preeti on 23rd apr 14 for manual testing
  
  		//document.location = base_url+'doc/final_saved/'+reg_id+'/'+sequence_token;// line added by preeti on 23rd apr 14 for manual testing
      
	  
	  
	  document.location = base_url+'doc/paynow/'+reg_id+'/'+sequence_token;// line added by preeti on 23rd apr 14 for manual testing
  
   
  }
  else
	{
	//document.getElementById('status').scrollIntoView();
	  $("html, body").animate({scrollTop: $("#status").offset().top}, 1000); 	
	}
  
  },
	  
   error: function(response, status, err){
        
        
    }
	

});

	$('#perm_as_post').live('click', function(e) 
	{ 
    	if( $('#perm_as_post').is(':checked') )
    	{
    		// show the field values same as postal address
    		
    		$('#perm_address').val( $('#post_address').val() );  
    		
    		$('#perm_state').val( $('#post_state').val() );
    		
    		$('#perm_pin').val( $('#post_pin').val() );
    		
    		$('#perm_tel').val( $('#post_tel').val() );
	    		
    		
    	}
    	else
    	{
    		// clear the fields
    		
    		$('#perm_address').val('');  
    		
    		$('#perm_state').val('');
    		
    		$('#perm_pin').val('');
    		
    		$('#perm_tel').val('');
	    	
    	}  	 	
    	
    
    });
    
    
    if( u_current_emp == 'y' )
	{
		$('#emp_div').show();	
	}
	else
	{
		$('#emp_div').hide();	
	}	
    
    
    $('input[name=u_current_emp]:checked').live('click', function(e)
    {
    	var value = $('input[name=u_current_emp]:checked').val();
		
		if( value == 'y' )
		{
			// show the div
			
			$('#emp_div').show();
		}
		else if( value == 'n' )
		{
			// hide the div
			
			$('#emp_div').hide();
			
			// below lines added by preeti on 3rd mar 14
			
			$('#ce_employer').val(''); 
			
			$('#ce_emp_noc').val('');  
			
			$('#noc_upload').hide();  
		}
    });
    
    if( u_previous_ssc == 'y')
    {
    	$('#ex_ssc').show();
    }
    else
    {
    	$('#ex_ssc').hide();
    }
    
    
    
    if( u_ex_ssc == 'y' )
	{
		$('#ssc_div').show();	
	}
	else
	{
		echo "<p style='color:red;'>***You are not eligible to proceed</p>";	
	}	



    
    $('input[name=u_previous_ssc]:checked').live('click', function(e)
    {
    	var value = $('input[name=u_previous_ssc]:checked').val();
		
		if( value == 'y' )
		{
			$('#ex_ssc').show();
		}
		else if( value == 'n' )
		{
			$('#ex_ssc').hide();
			$('#ssc_div').hide();
			
			// below lines added by preeti on 3rd mar 14
			
			$('#ps_com_date').val(''); 
			
			$('#ps_rel_date').val('');  
			
			$('#ps_rel_order').val('');  
			
			$('#rel_upload').hide(); 			
			
		}
    }); 
    
	
	if( u_previous_unfit == 'y' )
	$('#u_previous_unfit_y').show();	
	else
	$('#u_previous_unfit_y').hide();
	
	$('input[name=u_previous_unfit]:checked').live('click', function(e)
    {
    	var value = $('input[name=u_previous_unfit]:checked').val();
		
		if( value == 'y' )
		{
			$('#u_previous_unfit_y').show();
		}
		else if( value == 'n' )
		{
			$('#u_previous_unfit_y').hide();		
		}
    }); 
	
	
    $('#u_pref1').live('change', function()
    {
    	var u_pref1 = $(this).val();
    	
    	if( u_pref1 == 'air' )
    	{
    		var arr = [
		
				  {val : '', text: 'Select'},	
				
				  {val : 'arm', text: 'Army'},
				
				  {val : 'nav', text: 'Navy'}
		
				];
    	}
    	else if( u_pref1 == 'arm' )
    	{
    		var arr = [
		
				  {val : '', text: 'Select'},	
				
				  {val : 'air', text: 'Air Force'},
				
				  {val : 'nav', text: 'Navy'}
		
				];
    	}
    	else if( u_pref1 == 'nav' )
    	{
    		var arr = [
		
				  {val : '', text: 'Select'},	
				
				  {val : 'arm', text: 'Army'},
				
				  {val : 'air', text: 'Air Force'}
		
				];
    	}
    	   	
		
		var sel = $('#u_pref2');
		
		$("#u_pref2 option").remove(); // reset the previous values of second dropdown
		
		$("#u_pref3 option").remove(); // reset the previous values of third dropdown
		
		$("#u_pref3").append($("<option>").attr('value','').text('Select')); // append this null option
		
		$(arr).each(function() {
		
		 sel.append($("<option>").attr('value',this.val).text(this.text));
		
		});    	
    	
    });
    
    
    $('#u_pref2').live('change', function()
    {
    	var u_pref1 = $('#u_pref1').val();
    	
    	var u_pref2 = $(this).val();
    	
    	if( ( u_pref1 == 'air' && u_pref2 == 'arm' ) || ( u_pref1 == 'arm' && u_pref2 == 'air' ) )
    	{
    		var arr = [
		
				  {val : 'nav', text: 'Navy'}
		
				];
    	
    		
    	
    	}
    	else if( ( u_pref1 == 'arm' && u_pref2 == 'nav' ) || ( u_pref1 == 'nav' && u_pref2 == 'arm' ) )
    	{
    		var arr = [
		
				  {val : 'air', text: 'Air Force'}
		
				];
				
				
    	}
    	else if( ( u_pref1 == 'air' && u_pref2 == 'nav' ) || ( u_pref1 == 'nav' && u_pref2 == 'air' ) )
    	{
    		var arr = [
		
				  {val : 'arm', text: 'Army'}
		
				];
				
				
    	}
    	   	
		
		var sel = $('#u_pref3');
		
		$("#u_pref3 option").remove(); // reset the previous values
		
		$(arr).each(function() {
		
		 sel.append($("<option>").attr('value',this.val).text(this.text));
		
		});    	
    	
    });
    
    
    $('#save').click( function()
    {
    	//return confirm("If you save now, you will not be able to edit information. Are you sure you want to Save ?");
		var u_college_recog = $('input[name=u_college_recog]:checked').val();
		
		if(u_college_recog=='n')
		{
			alert("College must be Recognised By MCI to Continue");
			return false;
		}
		
		return confirm(" Are you sure you want to Save ?");
		
		
    });  

} );