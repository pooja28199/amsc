$(document).ready(function() {

var bar = $('.bar');

var percent = $('.percent');

var status = $('#status');

var base_url = $('#base_url').val();

var reg_id = $('#reg_id').val();

var u_is_name_change = $('input[name=u_is_name_change]:checked').val();

var u_is_married = $('input[name=u_is_married]:checked').val();

var confirm_random = $('#confirm_random').val(); // added by preeti on 22nd apr 14 for manual testing

var sequence_token = $('#sequence_token').val(); // added by preeti on 23rd apr 14 for manual testing

/*

$('#upload_form').ajaxForm({
	
    beforeSend: function() {
    
        status.empty();
    
        var percentVal = '0%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
		
	document.getElementById("loading").style.display="block";
    },
    
    uploadProgress: function(event, position, total, percentComplete) {
    
        var percentVal = percentComplete + '%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
    },
  
  complete: function(xhr) {
	  document.getElementById("loading").style.display="none";
	//  document.getElementById("status").style.display="block";
	
	   document.getElementById("status").style.border="1px solid #ddd";
	 document.getElementById("status").scrollTop;
	//  document.getElementById("fullright").scrollIntoView();
  
    status.html(xhr.responseText);// if there are validation errors then show then on the same screen     
    
  	if( xhr.responseText == 'exit' )
  	{
  		//document.location = base_url+'doc/doc_saved/'+reg_id; // line commented by preeti on 23rd apr 14 for manual testing 
  		
  		document.location = base_url+'doc/doc_saved/'+reg_id+'/'+sequence_token; // line added by preeti on 23rd apr 14 for manual testing
  		 		
  	}
  	else if( xhr.responseText == 'continue' )
  	{
  		//document.location = base_url+'doc/doc_signup2/'+reg_id; // line commented by preeti on 23rd apr 14 for manual testing		
  		
  		document.location = base_url+'doc/doc_signup2/'+reg_id+'/'+sequence_token;// line added by preeti on 23rd apr 14 for manual testing
  		 
  	}
	else
	{
	//document.getElementById('status').scrollIntoView();
	  $("html, body").animate({scrollTop: $("#status").offset().top}, 1000); 	
	}
	
	
	
  
  },
	  
   error: function(response, status, err){
        
        //alert(" error is "+err);
    }
	

}); */

	if( u_is_name_change == 'y' )
	{
		$('#name_div').show();	
	}
	else
	{
		$('#name_div').hide();	
	}	

	if( u_is_married == 'y' )
	{
		$('#mar_div').show();	
		
	}
	else
	{
		$('#mar_div').hide();
	}
	// $('#no_mbbs_cmp'.val() == 'n'
	// u_complete_mbbs == 'n'
	// if( u_complete_mbbs == 'n')
	// {
	// 	$('#likelydate').show();
		
	// }
	// else
	// {
	// 	$('#likelydate').hide();
	// }
	
	
	if( $('#sp_nation').val() == 'f' )
	{
		// show field
		
		$('#f_citi').show();
	}
	else 
	{
		// hide field
		
		$('#f_citi').hide();
	}
	

	$('input[name=u_is_name_change]').live('click', function(e) 
	{
		var value = $('input[name=u_is_name_change]:checked').val();
		
		if( value == 'y' )
		{
			// show the div
			
			$('#name_div').show();
		}
		else if( value == 'n' )
		{
			// hide the div
			
			$('#name_div').hide();
			
			// below lines added by preeti on 3rd mar 14
			
			// clear all the fields that are there in the name_div div
			
			$('#new_fname').val('');
			
			$('#new_mname').val('');
			
			$('#new_lname').val('');
			
			$('#new_name_proof').val('');						
			
		}
	});
	
	
	$('input[name=u_is_married]').live('click', function(e) 
	{
		var value = $('input[name=u_is_married]:checked').val();
		
		if( value == 'y' )
		{
			$('#mar_div').show();

		}
		else if( value == 'n' )
		{
			$('#mar_div').hide();
			
			// below lines added by preeti on 3rd mar 14
			
			// clear all the fields that are there in the name_div div
			
			$('#sp_mar_date').val('');
			
			$('#sp_name').val('');
			
			$('#sp_occup').val('');	
			
			$('#sp_nation').val('');	
			
			$('#sp_citizenship_date').val('');	
			
			$('input[name=sp_ssc_applied]:checked').each(function()
			{				
				$(this)[0].checked = false;
    		
    		});		
		}
	});
	
	// below lines modified by preeti on 27th feb 14
	
	/*$( "#u_fname" ).blur(function() 
	{
  		alert("Please enter Exactly the Same Name as it is in your class 10th/Matriculation Certificate");
	});*/
	
	// below lines added by preeti on 2nd apr 14
	
	
	$('#sp_nation').live('change', function(e) 
	{
		$value = $('#sp_nation').val();
		
		if( $value == 'f' )
		{
			$('#f_citi').show();
		}
		else if( $value == 'i' )
		{
			$('#sp_citizenship_date').val('');
			
			$('#f_citi').hide();
		}		
		
	});
	

} );