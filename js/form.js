$(document).ready(function() {

var bar = $('.bar');

var percent = $('.percent');

var status = $('#status');

var test_div = $('#test_div');

$('#upload_form').ajaxForm({
	
    beforeSend: function() {
    
        status.empty();
    
        var percentVal = '0%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
    },
    
    uploadProgress: function(event, position, total, percentComplete) {
    
        var percentVal = percentComplete + '%';
    
        bar.width(percentVal);
    
        percent.html(percentVal);
    },
  
  complete: function(xhr) {
  
    status.html(xhr.responseText);
    
    test_div.html(xhr.responseText);
  
  },
	  
   error: function(response, status, err){
        
        alert(" error is "+err);
    }
	

});

} );